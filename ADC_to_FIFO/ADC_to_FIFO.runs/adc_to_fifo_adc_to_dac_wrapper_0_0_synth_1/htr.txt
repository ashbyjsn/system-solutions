REM
REM Vivado(TM)
REM htr.txt: a Vivado-generated description of how-to-repeat the
REM          the basic steps of a run.  Note that runme.bat/sh needs
REM          to be invoked for Vivado to track run status.
REM Copyright 1986-2016 Xilinx, Inc. All Rights Reserved.
REM

vivado -log adc_to_fifo_adc_to_dac_wrapper_0_0.vds -m64 -product Vivado -mode batch -messageDb vivado.pb -notrace -source adc_to_fifo_adc_to_dac_wrapper_0_0.tcl
