# 
# Synthesis run script generated by Vivado
# 

set_msg_config -id {HDL 9-1061} -limit 100000
set_msg_config -id {HDL 9-1654} -limit 100000
create_project -in_memory -part xc7z010clg400-1

set_param project.singleFileAddWarning.threshold 0
set_param project.compositeFile.enableAutoGeneration 0
set_param synth.vivado.isSynthRun true
set_msg_config -source 4 -id {IP_Flow 19-2162} -severity warning -new_severity info
set_property webtalk.parent_dir {D:/ECE/CapstoneDesign/ECE 483/ADC_to_FIFO/ADC_to_FIFO.cache/wt} [current_project]
set_property parent.project_path {D:/ECE/CapstoneDesign/ECE 483/ADC_to_FIFO/ADC_to_FIFO.xpr} [current_project]
set_property XPM_LIBRARIES {XPM_CDC XPM_MEMORY} [current_project]
set_property default_lib xil_defaultlib [current_project]
set_property target_language Verilog [current_project]
set_property ip_repo_paths {{d:/ECE/CapstoneDesign/ECE 483/FPGA_Data_Acquisition_And_Control}} [current_project]
set_property ip_output_repo {d:/ECE/CapstoneDesign/ECE 483/ADC_to_FIFO/ADC_to_FIFO.cache/ip} [current_project]
set_property ip_cache_permissions {read write} [current_project]
read_verilog -library xil_defaultlib {{D:/ECE/CapstoneDesign/ECE 483/ADC_to_FIFO/ADC_to_FIFO.srcs/sources_1/bd/adc_to_fifo/hdl/adc_to_fifo_wrapper.v}}
add_files {{D:/ECE/CapstoneDesign/ECE 483/ADC_to_FIFO/ADC_to_FIFO.srcs/sources_1/bd/adc_to_fifo/adc_to_fifo.bd}}
set_property used_in_implementation false [get_files -all {{d:/ECE/CapstoneDesign/ECE 483/ADC_to_FIFO/ADC_to_FIFO.srcs/sources_1/bd/adc_to_fifo/ip/adc_to_fifo_axis_data_fifo_0_0/adc_to_fifo_axis_data_fifo_0_0_ooc.xdc}}]
set_property used_in_implementation false [get_files -all {{d:/ECE/CapstoneDesign/ECE 483/ADC_to_FIFO/ADC_to_FIFO.srcs/sources_1/bd/adc_to_fifo/ip/adc_to_fifo_axis_data_fifo_0_0/adc_to_fifo_axis_data_fifo_0_0/adc_to_fifo_axis_data_fifo_0_0.xdc}}]
set_property used_in_implementation false [get_files -all {{d:/ECE/CapstoneDesign/ECE 483/ADC_to_FIFO/ADC_to_FIFO.srcs/sources_1/bd/adc_to_fifo/ip/adc_to_fifo_axis_data_fifo_1_0/adc_to_fifo_axis_data_fifo_1_0_ooc.xdc}}]
set_property used_in_implementation false [get_files -all {{d:/ECE/CapstoneDesign/ECE 483/ADC_to_FIFO/ADC_to_FIFO.srcs/sources_1/bd/adc_to_fifo/ip/adc_to_fifo_axis_data_fifo_1_0/adc_to_fifo_axis_data_fifo_1_0/adc_to_fifo_axis_data_fifo_1_0.xdc}}]
set_property used_in_implementation false [get_files -all {{d:/ECE/CapstoneDesign/ECE 483/ADC_to_FIFO/ADC_to_FIFO.srcs/sources_1/bd/adc_to_fifo/ip/adc_to_fifo_adc_to_fifo_0_0/constrs_1/new/RedPitaya.xdc}}]
set_property used_in_implementation false [get_files -all {{d:/ECE/CapstoneDesign/ECE 483/ADC_to_FIFO/ADC_to_FIFO.srcs/sources_1/bd/adc_to_fifo/ip/adc_to_fifo_processing_system7_0_0/adc_to_fifo_processing_system7_0_0.xdc}}]
set_property used_in_implementation false [get_files -all {{d:/ECE/CapstoneDesign/ECE 483/ADC_to_FIFO/ADC_to_FIFO.srcs/sources_1/bd/adc_to_fifo/ip/adc_to_fifo_proc_sys_reset_0_0/adc_to_fifo_proc_sys_reset_0_0_board.xdc}}]
set_property used_in_implementation false [get_files -all {{d:/ECE/CapstoneDesign/ECE 483/ADC_to_FIFO/ADC_to_FIFO.srcs/sources_1/bd/adc_to_fifo/ip/adc_to_fifo_proc_sys_reset_0_0/adc_to_fifo_proc_sys_reset_0_0.xdc}}]
set_property used_in_implementation false [get_files -all {{d:/ECE/CapstoneDesign/ECE 483/ADC_to_FIFO/ADC_to_FIFO.srcs/sources_1/bd/adc_to_fifo/ip/adc_to_fifo_proc_sys_reset_0_0/adc_to_fifo_proc_sys_reset_0_0_ooc.xdc}}]
set_property used_in_implementation false [get_files -all {{d:/ECE/CapstoneDesign/ECE 483/ADC_to_FIFO/ADC_to_FIFO.srcs/sources_1/bd/adc_to_fifo/ip/adc_to_fifo_axi_gpio_0_0/adc_to_fifo_axi_gpio_0_0_board.xdc}}]
set_property used_in_implementation false [get_files -all {{d:/ECE/CapstoneDesign/ECE 483/ADC_to_FIFO/ADC_to_FIFO.srcs/sources_1/bd/adc_to_fifo/ip/adc_to_fifo_axi_gpio_0_0/adc_to_fifo_axi_gpio_0_0_ooc.xdc}}]
set_property used_in_implementation false [get_files -all {{d:/ECE/CapstoneDesign/ECE 483/ADC_to_FIFO/ADC_to_FIFO.srcs/sources_1/bd/adc_to_fifo/ip/adc_to_fifo_axi_gpio_0_0/adc_to_fifo_axi_gpio_0_0.xdc}}]
set_property used_in_implementation false [get_files -all {{d:/ECE/CapstoneDesign/ECE 483/ADC_to_FIFO/ADC_to_FIFO.srcs/sources_1/bd/adc_to_fifo/ip/adc_to_fifo_axi_gpio_1_0/adc_to_fifo_axi_gpio_1_0_board.xdc}}]
set_property used_in_implementation false [get_files -all {{d:/ECE/CapstoneDesign/ECE 483/ADC_to_FIFO/ADC_to_FIFO.srcs/sources_1/bd/adc_to_fifo/ip/adc_to_fifo_axi_gpio_1_0/adc_to_fifo_axi_gpio_1_0_ooc.xdc}}]
set_property used_in_implementation false [get_files -all {{d:/ECE/CapstoneDesign/ECE 483/ADC_to_FIFO/ADC_to_FIFO.srcs/sources_1/bd/adc_to_fifo/ip/adc_to_fifo_axi_gpio_1_0/adc_to_fifo_axi_gpio_1_0.xdc}}]
set_property used_in_implementation false [get_files -all {{D:/ECE/CapstoneDesign/ECE 483/ADC_to_FIFO/ADC_to_FIFO.srcs/sources_1/bd/adc_to_fifo/adc_to_fifo_ooc.xdc}}]
set_property is_locked true [get_files {{D:/ECE/CapstoneDesign/ECE 483/ADC_to_FIFO/ADC_to_FIFO.srcs/sources_1/bd/adc_to_fifo/adc_to_fifo.bd}}]

foreach dcp [get_files -quiet -all *.dcp] {
  set_property used_in_implementation false $dcp
}
read_xdc dont_touch.xdc
set_property used_in_implementation false [get_files dont_touch.xdc]

synth_design -top adc_to_fifo_wrapper -part xc7z010clg400-1


write_checkpoint -force -noxdef adc_to_fifo_wrapper.dcp

catch { report_utilization -file adc_to_fifo_wrapper_utilization_synth.rpt -pb adc_to_fifo_wrapper_utilization_synth.pb }
