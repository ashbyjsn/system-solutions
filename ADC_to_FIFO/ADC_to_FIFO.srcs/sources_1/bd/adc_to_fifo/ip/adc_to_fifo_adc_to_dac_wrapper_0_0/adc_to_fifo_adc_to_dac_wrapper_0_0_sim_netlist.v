// Copyright 1986-2016 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2016.4 (win64) Build 1756540 Mon Jan 23 19:11:23 MST 2017
// Date        : Tue Apr 28 14:58:15 2020
// Host        : DESKTOP-0KQQTOC running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim {D:/ECE/CapstoneDesign/ECE
//               483/ADC_to_FIFO/ADC_to_FIFO.srcs/sources_1/bd/adc_to_fifo/ip/adc_to_fifo_adc_to_dac_wrapper_0_0/adc_to_fifo_adc_to_dac_wrapper_0_0_sim_netlist.v}
// Design      : adc_to_fifo_adc_to_dac_wrapper_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z010clg400-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "adc_to_fifo_adc_to_dac_wrapper_0_0,adc_to_dac_wrapper,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "adc_to_dac_wrapper,Vivado 2016.4" *) 
(* NotValidForBitStream *)
module adc_to_fifo_adc_to_dac_wrapper_0_0
   (adc_clk_i,
    adc_dat_a,
    adc_dat_b,
    s_axis_clk,
    s_axis_resetn,
    s_axis_tready1,
    s_axis_tvalid1,
    s_axis_tdata1);
  input adc_clk_i;
  input [13:0]adc_dat_a;
  input [13:0]adc_dat_b;
  input s_axis_clk;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 s_axis_resetn RST" *) input s_axis_resetn;
  input s_axis_tready1;
  output s_axis_tvalid1;
  output [15:0]s_axis_tdata1;

  wire adc_clk_i;
  wire [13:0]adc_dat_a;
  wire [13:0]adc_dat_b;
  wire s_axis_clk;
  wire s_axis_resetn;
  wire [15:0]s_axis_tdata1;
  wire s_axis_tready1;
  wire s_axis_tvalid1;

  adc_to_fifo_adc_to_dac_wrapper_0_0_adc_to_dac_wrapper inst
       (.adc_clk_i(adc_clk_i),
        .adc_dat_a(adc_dat_a),
        .adc_dat_b(adc_dat_b),
        .s_axis_clk(s_axis_clk),
        .s_axis_resetn(s_axis_resetn),
        .s_axis_tdata1(s_axis_tdata1),
        .s_axis_tready1(s_axis_tready1),
        .s_axis_tvalid1(s_axis_tvalid1));
endmodule

(* ORIG_REF_NAME = "adc_to_dac_wrapper" *) 
module adc_to_fifo_adc_to_dac_wrapper_0_0_adc_to_dac_wrapper
   (adc_clk_i,
    adc_dat_a,
    adc_dat_b,
    s_axis_clk,
    s_axis_resetn,
    s_axis_tready1,
    s_axis_tvalid1,
    s_axis_tdata1);
  input adc_clk_i;
  input [13:0]adc_dat_a;
  input [13:0]adc_dat_b;
  input s_axis_clk;
  input s_axis_resetn;
  input s_axis_tready1;
  output s_axis_tvalid1;
  output [15:0]s_axis_tdata1;

  wire \<const0> ;

  assign s_axis_tdata1[15] = \<const0> ;
  assign s_axis_tdata1[14] = \<const0> ;
  assign s_axis_tdata1[13] = \<const0> ;
  assign s_axis_tdata1[12] = \<const0> ;
  assign s_axis_tdata1[11] = \<const0> ;
  assign s_axis_tdata1[10] = \<const0> ;
  assign s_axis_tdata1[9] = \<const0> ;
  assign s_axis_tdata1[8] = \<const0> ;
  assign s_axis_tdata1[7] = \<const0> ;
  assign s_axis_tdata1[6] = \<const0> ;
  assign s_axis_tdata1[5] = \<const0> ;
  assign s_axis_tdata1[4] = \<const0> ;
  assign s_axis_tdata1[3] = \<const0> ;
  assign s_axis_tdata1[2] = \<const0> ;
  assign s_axis_tdata1[1] = \<const0> ;
  assign s_axis_tdata1[0] = \<const0> ;
  assign s_axis_tvalid1 = \<const0> ;
  GND GND
       (.G(\<const0> ));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (weak1, weak0) GSR = GSR_int;
    assign (weak1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
