-- Copyright 1986-2016 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2016.4 (win64) Build 1756540 Mon Jan 23 19:11:23 MST 2017
-- Date        : Tue Apr 28 14:58:15 2020
-- Host        : DESKTOP-0KQQTOC running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim {D:/ECE/CapstoneDesign/ECE
--               483/ADC_to_FIFO/ADC_to_FIFO.srcs/sources_1/bd/adc_to_fifo/ip/adc_to_fifo_adc_to_dac_wrapper_0_0/adc_to_fifo_adc_to_dac_wrapper_0_0_sim_netlist.vhdl}
-- Design      : adc_to_fifo_adc_to_dac_wrapper_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z010clg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity adc_to_fifo_adc_to_dac_wrapper_0_0_adc_to_dac_wrapper is
  port (
    adc_clk_i : in STD_LOGIC;
    adc_dat_a : in STD_LOGIC_VECTOR ( 13 downto 0 );
    adc_dat_b : in STD_LOGIC_VECTOR ( 13 downto 0 );
    s_axis_clk : in STD_LOGIC;
    s_axis_resetn : in STD_LOGIC;
    s_axis_tready1 : in STD_LOGIC;
    s_axis_tvalid1 : out STD_LOGIC;
    s_axis_tdata1 : out STD_LOGIC_VECTOR ( 15 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of adc_to_fifo_adc_to_dac_wrapper_0_0_adc_to_dac_wrapper : entity is "adc_to_dac_wrapper";
end adc_to_fifo_adc_to_dac_wrapper_0_0_adc_to_dac_wrapper;

architecture STRUCTURE of adc_to_fifo_adc_to_dac_wrapper_0_0_adc_to_dac_wrapper is
  signal \<const0>\ : STD_LOGIC;
begin
  s_axis_tdata1(15) <= \<const0>\;
  s_axis_tdata1(14) <= \<const0>\;
  s_axis_tdata1(13) <= \<const0>\;
  s_axis_tdata1(12) <= \<const0>\;
  s_axis_tdata1(11) <= \<const0>\;
  s_axis_tdata1(10) <= \<const0>\;
  s_axis_tdata1(9) <= \<const0>\;
  s_axis_tdata1(8) <= \<const0>\;
  s_axis_tdata1(7) <= \<const0>\;
  s_axis_tdata1(6) <= \<const0>\;
  s_axis_tdata1(5) <= \<const0>\;
  s_axis_tdata1(4) <= \<const0>\;
  s_axis_tdata1(3) <= \<const0>\;
  s_axis_tdata1(2) <= \<const0>\;
  s_axis_tdata1(1) <= \<const0>\;
  s_axis_tdata1(0) <= \<const0>\;
  s_axis_tvalid1 <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity adc_to_fifo_adc_to_dac_wrapper_0_0 is
  port (
    adc_clk_i : in STD_LOGIC;
    adc_dat_a : in STD_LOGIC_VECTOR ( 13 downto 0 );
    adc_dat_b : in STD_LOGIC_VECTOR ( 13 downto 0 );
    s_axis_clk : in STD_LOGIC;
    s_axis_resetn : in STD_LOGIC;
    s_axis_tready1 : in STD_LOGIC;
    s_axis_tvalid1 : out STD_LOGIC;
    s_axis_tdata1 : out STD_LOGIC_VECTOR ( 15 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of adc_to_fifo_adc_to_dac_wrapper_0_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of adc_to_fifo_adc_to_dac_wrapper_0_0 : entity is "adc_to_fifo_adc_to_dac_wrapper_0_0,adc_to_dac_wrapper,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of adc_to_fifo_adc_to_dac_wrapper_0_0 : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of adc_to_fifo_adc_to_dac_wrapper_0_0 : entity is "adc_to_dac_wrapper,Vivado 2016.4";
end adc_to_fifo_adc_to_dac_wrapper_0_0;

architecture STRUCTURE of adc_to_fifo_adc_to_dac_wrapper_0_0 is
begin
inst: entity work.adc_to_fifo_adc_to_dac_wrapper_0_0_adc_to_dac_wrapper
     port map (
      adc_clk_i => adc_clk_i,
      adc_dat_a(13 downto 0) => adc_dat_a(13 downto 0),
      adc_dat_b(13 downto 0) => adc_dat_b(13 downto 0),
      s_axis_clk => s_axis_clk,
      s_axis_resetn => s_axis_resetn,
      s_axis_tdata1(15 downto 0) => s_axis_tdata1(15 downto 0),
      s_axis_tready1 => s_axis_tready1,
      s_axis_tvalid1 => s_axis_tvalid1
    );
end STRUCTURE;
