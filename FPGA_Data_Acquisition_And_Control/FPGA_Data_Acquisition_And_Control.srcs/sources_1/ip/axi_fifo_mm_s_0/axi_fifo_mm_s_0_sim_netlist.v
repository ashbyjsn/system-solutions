// Copyright 1986-2016 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2016.4 (win64) Build 1756540 Mon Jan 23 19:11:23 MST 2017
// Date        : Wed Apr 01 14:00:40 2020
// Host        : DESKTOP-0KQQTOC running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim {d:/ECE/CapstoneDesign/ECE
//               483/FPGA_Data_Acquisition_And_Control/FPGA_Data_Acquisition_And_Control.srcs/sources_1/ip/axi_fifo_mm_s_0/axi_fifo_mm_s_0_sim_netlist.v}
// Design      : axi_fifo_mm_s_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z010clg400-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "axi_fifo_mm_s_0,axi_fifo_mm_s,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "axi_fifo_mm_s,Vivado 2016.4" *) 
(* NotValidForBitStream *)
module axi_fifo_mm_s_0
   (interrupt,
    s_axi_aclk,
    s_axi_aresetn,
    s_axi_awaddr,
    s_axi_awvalid,
    s_axi_awready,
    s_axi_wdata,
    s_axi_wstrb,
    s_axi_wvalid,
    s_axi_wready,
    s_axi_bresp,
    s_axi_bvalid,
    s_axi_bready,
    s_axi_araddr,
    s_axi_arvalid,
    s_axi_arready,
    s_axi_rdata,
    s_axi_rresp,
    s_axi_rvalid,
    s_axi_rready,
    s_axi4_awid,
    s_axi4_awaddr,
    s_axi4_awlen,
    s_axi4_awsize,
    s_axi4_awburst,
    s_axi4_awlock,
    s_axi4_awcache,
    s_axi4_awprot,
    s_axi4_awvalid,
    s_axi4_awready,
    s_axi4_wdata,
    s_axi4_wstrb,
    s_axi4_wlast,
    s_axi4_wvalid,
    s_axi4_wready,
    s_axi4_bid,
    s_axi4_bresp,
    s_axi4_bvalid,
    s_axi4_bready,
    s_axi4_arid,
    s_axi4_araddr,
    s_axi4_arlen,
    s_axi4_arsize,
    s_axi4_arburst,
    s_axi4_arlock,
    s_axi4_arcache,
    s_axi4_arprot,
    s_axi4_arvalid,
    s_axi4_arready,
    s_axi4_rid,
    s_axi4_rdata,
    s_axi4_rresp,
    s_axi4_rlast,
    s_axi4_rvalid,
    s_axi4_rready,
    mm2s_prmry_reset_out_n,
    axi_str_txd_tvalid,
    axi_str_txd_tready,
    axi_str_txd_tlast,
    axi_str_txd_tdata,
    mm2s_cntrl_reset_out_n,
    axi_str_txc_tvalid,
    axi_str_txc_tready,
    axi_str_txc_tlast,
    axi_str_txc_tdata,
    s2mm_prmry_reset_out_n,
    axi_str_rxd_tvalid,
    axi_str_rxd_tready,
    axi_str_rxd_tlast,
    axi_str_rxd_tdata);
  (* x_interface_info = "xilinx.com:signal:interrupt:1.0 interrupt_intf INTERRUPT" *) output interrupt;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 aclk_s_axi CLK" *) input s_axi_aclk;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 rst_s_axi RST" *) input s_axi_aresetn;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI AWADDR" *) input [31:0]s_axi_awaddr;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI AWVALID" *) input s_axi_awvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI AWREADY" *) output s_axi_awready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI WDATA" *) input [31:0]s_axi_wdata;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI WSTRB" *) input [3:0]s_axi_wstrb;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI WVALID" *) input s_axi_wvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI WREADY" *) output s_axi_wready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI BRESP" *) output [1:0]s_axi_bresp;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI BVALID" *) output s_axi_bvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI BREADY" *) input s_axi_bready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI ARADDR" *) input [31:0]s_axi_araddr;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI ARVALID" *) input s_axi_arvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI ARREADY" *) output s_axi_arready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI RDATA" *) output [31:0]s_axi_rdata;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI RRESP" *) output [1:0]s_axi_rresp;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI RVALID" *) output s_axi_rvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI RREADY" *) input s_axi_rready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI_FULL AWID" *) input [3:0]s_axi4_awid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI_FULL AWADDR" *) input [31:0]s_axi4_awaddr;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI_FULL AWLEN" *) input [7:0]s_axi4_awlen;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI_FULL AWSIZE" *) input [2:0]s_axi4_awsize;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI_FULL AWBURST" *) input [1:0]s_axi4_awburst;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI_FULL AWLOCK" *) input s_axi4_awlock;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI_FULL AWCACHE" *) input [3:0]s_axi4_awcache;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI_FULL AWPROT" *) input [2:0]s_axi4_awprot;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI_FULL AWVALID" *) input s_axi4_awvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI_FULL AWREADY" *) output s_axi4_awready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI_FULL WDATA" *) input [31:0]s_axi4_wdata;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI_FULL WSTRB" *) input [3:0]s_axi4_wstrb;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI_FULL WLAST" *) input s_axi4_wlast;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI_FULL WVALID" *) input s_axi4_wvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI_FULL WREADY" *) output s_axi4_wready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI_FULL BID" *) output [3:0]s_axi4_bid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI_FULL BRESP" *) output [1:0]s_axi4_bresp;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI_FULL BVALID" *) output s_axi4_bvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI_FULL BREADY" *) input s_axi4_bready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI_FULL ARID" *) input [3:0]s_axi4_arid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI_FULL ARADDR" *) input [31:0]s_axi4_araddr;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI_FULL ARLEN" *) input [7:0]s_axi4_arlen;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI_FULL ARSIZE" *) input [2:0]s_axi4_arsize;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI_FULL ARBURST" *) input [1:0]s_axi4_arburst;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI_FULL ARLOCK" *) input s_axi4_arlock;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI_FULL ARCACHE" *) input [3:0]s_axi4_arcache;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI_FULL ARPROT" *) input [2:0]s_axi4_arprot;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI_FULL ARVALID" *) input s_axi4_arvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI_FULL ARREADY" *) output s_axi4_arready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI_FULL RID" *) output [3:0]s_axi4_rid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI_FULL RDATA" *) output [31:0]s_axi4_rdata;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI_FULL RRESP" *) output [1:0]s_axi4_rresp;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI_FULL RLAST" *) output s_axi4_rlast;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI_FULL RVALID" *) output s_axi4_rvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI_FULL RREADY" *) input s_axi4_rready;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 rst_axi_str_txd RST" *) output mm2s_prmry_reset_out_n;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 AXI_STR_TXD TVALID" *) output axi_str_txd_tvalid;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 AXI_STR_TXD TREADY" *) input axi_str_txd_tready;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 AXI_STR_TXD TLAST" *) output axi_str_txd_tlast;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 AXI_STR_TXD TDATA" *) output [31:0]axi_str_txd_tdata;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 rst_axi_str_txc RST" *) output mm2s_cntrl_reset_out_n;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 AXI_STR_TXC TVALID" *) output axi_str_txc_tvalid;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 AXI_STR_TXC TREADY" *) input axi_str_txc_tready;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 AXI_STR_TXC TLAST" *) output axi_str_txc_tlast;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 AXI_STR_TXC TDATA" *) output [31:0]axi_str_txc_tdata;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 rst_axi_str_rxd RST" *) output s2mm_prmry_reset_out_n;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 AXI_STR_RXD TVALID" *) input axi_str_rxd_tvalid;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 AXI_STR_RXD TREADY" *) output axi_str_rxd_tready;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 AXI_STR_RXD TLAST" *) input axi_str_rxd_tlast;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 AXI_STR_RXD TDATA" *) input [31:0]axi_str_rxd_tdata;

  wire [31:0]axi_str_rxd_tdata;
  wire axi_str_rxd_tlast;
  wire axi_str_rxd_tready;
  wire axi_str_rxd_tvalid;
  wire [31:0]axi_str_txc_tdata;
  wire axi_str_txc_tlast;
  wire axi_str_txc_tready;
  wire axi_str_txc_tvalid;
  wire [31:0]axi_str_txd_tdata;
  wire axi_str_txd_tlast;
  wire axi_str_txd_tready;
  wire axi_str_txd_tvalid;
  wire interrupt;
  wire mm2s_cntrl_reset_out_n;
  wire mm2s_prmry_reset_out_n;
  wire s2mm_prmry_reset_out_n;
  wire [31:0]s_axi4_araddr;
  wire [1:0]s_axi4_arburst;
  wire [3:0]s_axi4_arcache;
  wire [3:0]s_axi4_arid;
  wire [7:0]s_axi4_arlen;
  wire s_axi4_arlock;
  wire [2:0]s_axi4_arprot;
  wire s_axi4_arready;
  wire [2:0]s_axi4_arsize;
  wire s_axi4_arvalid;
  wire [31:0]s_axi4_awaddr;
  wire [1:0]s_axi4_awburst;
  wire [3:0]s_axi4_awcache;
  wire [3:0]s_axi4_awid;
  wire [7:0]s_axi4_awlen;
  wire s_axi4_awlock;
  wire [2:0]s_axi4_awprot;
  wire s_axi4_awready;
  wire [2:0]s_axi4_awsize;
  wire s_axi4_awvalid;
  wire [3:0]s_axi4_bid;
  wire s_axi4_bready;
  wire [1:0]s_axi4_bresp;
  wire s_axi4_bvalid;
  wire [31:0]s_axi4_rdata;
  wire [3:0]s_axi4_rid;
  wire s_axi4_rlast;
  wire s_axi4_rready;
  wire [1:0]s_axi4_rresp;
  wire s_axi4_rvalid;
  wire [31:0]s_axi4_wdata;
  wire s_axi4_wlast;
  wire s_axi4_wready;
  wire [3:0]s_axi4_wstrb;
  wire s_axi4_wvalid;
  wire s_axi_aclk;
  wire [31:0]s_axi_araddr;
  wire s_axi_aresetn;
  wire s_axi_arready;
  wire s_axi_arvalid;
  wire [31:0]s_axi_awaddr;
  wire s_axi_awready;
  wire s_axi_awvalid;
  wire s_axi_bready;
  wire [1:0]s_axi_bresp;
  wire s_axi_bvalid;
  wire [31:0]s_axi_rdata;
  wire s_axi_rready;
  wire [1:0]s_axi_rresp;
  wire s_axi_rvalid;
  wire [31:0]s_axi_wdata;
  wire s_axi_wready;
  wire [3:0]s_axi_wstrb;
  wire s_axi_wvalid;
  wire [3:0]NLW_U0_axi_str_txc_tdest_UNCONNECTED;
  wire [3:0]NLW_U0_axi_str_txc_tid_UNCONNECTED;
  wire [3:0]NLW_U0_axi_str_txc_tkeep_UNCONNECTED;
  wire [3:0]NLW_U0_axi_str_txc_tstrb_UNCONNECTED;
  wire [3:0]NLW_U0_axi_str_txc_tuser_UNCONNECTED;
  wire [3:0]NLW_U0_axi_str_txd_tdest_UNCONNECTED;
  wire [3:0]NLW_U0_axi_str_txd_tid_UNCONNECTED;
  wire [3:0]NLW_U0_axi_str_txd_tkeep_UNCONNECTED;
  wire [3:0]NLW_U0_axi_str_txd_tstrb_UNCONNECTED;
  wire [3:0]NLW_U0_axi_str_txd_tuser_UNCONNECTED;

  (* C_AXI4_BASEADDR = "-2147479552" *) 
  (* C_AXI4_HIGHADDR = "-2147471361" *) 
  (* C_AXIS_TDEST_WIDTH = "4" *) 
  (* C_AXIS_TID_WIDTH = "4" *) 
  (* C_AXIS_TUSER_WIDTH = "4" *) 
  (* C_BASEADDR = "-2147483648" *) 
  (* C_DATA_INTERFACE_TYPE = "1" *) 
  (* C_FAMILY = "zynq" *) 
  (* C_HAS_AXIS_TDEST = "0" *) 
  (* C_HAS_AXIS_TID = "0" *) 
  (* C_HAS_AXIS_TKEEP = "0" *) 
  (* C_HAS_AXIS_TSTRB = "0" *) 
  (* C_HAS_AXIS_TUSER = "0" *) 
  (* C_HIGHADDR = "-2147479553" *) 
  (* C_RX_FIFO_DEPTH = "512" *) 
  (* C_RX_FIFO_PE_THRESHOLD = "2" *) 
  (* C_RX_FIFO_PF_THRESHOLD = "507" *) 
  (* C_S_AXI4_DATA_WIDTH = "32" *) 
  (* C_S_AXI_ADDR_WIDTH = "32" *) 
  (* C_S_AXI_DATA_WIDTH = "32" *) 
  (* C_S_AXI_ID_WIDTH = "4" *) 
  (* C_TX_FIFO_DEPTH = "512" *) 
  (* C_TX_FIFO_PE_THRESHOLD = "2" *) 
  (* C_TX_FIFO_PF_THRESHOLD = "507" *) 
  (* C_USE_RX_CUT_THROUGH = "0" *) 
  (* C_USE_RX_DATA = "1" *) 
  (* C_USE_TX_CTRL = "1" *) 
  (* C_USE_TX_CUT_THROUGH = "0" *) 
  (* C_USE_TX_DATA = "1" *) 
  axi_fifo_mm_s_0_axi_fifo_mm_s U0
       (.axi_str_rxd_tdata(axi_str_rxd_tdata),
        .axi_str_rxd_tdest({1'b0,1'b0,1'b0,1'b0}),
        .axi_str_rxd_tid({1'b0,1'b0,1'b0,1'b0}),
        .axi_str_rxd_tkeep({1'b0,1'b0,1'b0,1'b0}),
        .axi_str_rxd_tlast(axi_str_rxd_tlast),
        .axi_str_rxd_tready(axi_str_rxd_tready),
        .axi_str_rxd_tstrb({1'b0,1'b0,1'b0,1'b0}),
        .axi_str_rxd_tuser({1'b0,1'b0,1'b0,1'b0}),
        .axi_str_rxd_tvalid(axi_str_rxd_tvalid),
        .axi_str_txc_tdata(axi_str_txc_tdata),
        .axi_str_txc_tdest(NLW_U0_axi_str_txc_tdest_UNCONNECTED[3:0]),
        .axi_str_txc_tid(NLW_U0_axi_str_txc_tid_UNCONNECTED[3:0]),
        .axi_str_txc_tkeep(NLW_U0_axi_str_txc_tkeep_UNCONNECTED[3:0]),
        .axi_str_txc_tlast(axi_str_txc_tlast),
        .axi_str_txc_tready(axi_str_txc_tready),
        .axi_str_txc_tstrb(NLW_U0_axi_str_txc_tstrb_UNCONNECTED[3:0]),
        .axi_str_txc_tuser(NLW_U0_axi_str_txc_tuser_UNCONNECTED[3:0]),
        .axi_str_txc_tvalid(axi_str_txc_tvalid),
        .axi_str_txd_tdata(axi_str_txd_tdata),
        .axi_str_txd_tdest(NLW_U0_axi_str_txd_tdest_UNCONNECTED[3:0]),
        .axi_str_txd_tid(NLW_U0_axi_str_txd_tid_UNCONNECTED[3:0]),
        .axi_str_txd_tkeep(NLW_U0_axi_str_txd_tkeep_UNCONNECTED[3:0]),
        .axi_str_txd_tlast(axi_str_txd_tlast),
        .axi_str_txd_tready(axi_str_txd_tready),
        .axi_str_txd_tstrb(NLW_U0_axi_str_txd_tstrb_UNCONNECTED[3:0]),
        .axi_str_txd_tuser(NLW_U0_axi_str_txd_tuser_UNCONNECTED[3:0]),
        .axi_str_txd_tvalid(axi_str_txd_tvalid),
        .interrupt(interrupt),
        .mm2s_cntrl_reset_out_n(mm2s_cntrl_reset_out_n),
        .mm2s_prmry_reset_out_n(mm2s_prmry_reset_out_n),
        .s2mm_prmry_reset_out_n(s2mm_prmry_reset_out_n),
        .s_axi4_araddr(s_axi4_araddr),
        .s_axi4_arburst(s_axi4_arburst),
        .s_axi4_arcache(s_axi4_arcache),
        .s_axi4_arid(s_axi4_arid),
        .s_axi4_arlen(s_axi4_arlen),
        .s_axi4_arlock(s_axi4_arlock),
        .s_axi4_arprot(s_axi4_arprot),
        .s_axi4_arready(s_axi4_arready),
        .s_axi4_arsize(s_axi4_arsize),
        .s_axi4_arvalid(s_axi4_arvalid),
        .s_axi4_awaddr(s_axi4_awaddr),
        .s_axi4_awburst(s_axi4_awburst),
        .s_axi4_awcache(s_axi4_awcache),
        .s_axi4_awid(s_axi4_awid),
        .s_axi4_awlen(s_axi4_awlen),
        .s_axi4_awlock(s_axi4_awlock),
        .s_axi4_awprot(s_axi4_awprot),
        .s_axi4_awready(s_axi4_awready),
        .s_axi4_awsize(s_axi4_awsize),
        .s_axi4_awvalid(s_axi4_awvalid),
        .s_axi4_bid(s_axi4_bid),
        .s_axi4_bready(s_axi4_bready),
        .s_axi4_bresp(s_axi4_bresp),
        .s_axi4_bvalid(s_axi4_bvalid),
        .s_axi4_rdata(s_axi4_rdata),
        .s_axi4_rid(s_axi4_rid),
        .s_axi4_rlast(s_axi4_rlast),
        .s_axi4_rready(s_axi4_rready),
        .s_axi4_rresp(s_axi4_rresp),
        .s_axi4_rvalid(s_axi4_rvalid),
        .s_axi4_wdata(s_axi4_wdata),
        .s_axi4_wlast(s_axi4_wlast),
        .s_axi4_wready(s_axi4_wready),
        .s_axi4_wstrb(s_axi4_wstrb),
        .s_axi4_wvalid(s_axi4_wvalid),
        .s_axi_aclk(s_axi_aclk),
        .s_axi_araddr(s_axi_araddr),
        .s_axi_aresetn(s_axi_aresetn),
        .s_axi_arready(s_axi_arready),
        .s_axi_arvalid(s_axi_arvalid),
        .s_axi_awaddr(s_axi_awaddr),
        .s_axi_awready(s_axi_awready),
        .s_axi_awvalid(s_axi_awvalid),
        .s_axi_bready(s_axi_bready),
        .s_axi_bresp(s_axi_bresp),
        .s_axi_bvalid(s_axi_bvalid),
        .s_axi_rdata(s_axi_rdata),
        .s_axi_rready(s_axi_rready),
        .s_axi_rresp(s_axi_rresp),
        .s_axi_rvalid(s_axi_rvalid),
        .s_axi_wdata(s_axi_wdata),
        .s_axi_wready(s_axi_wready),
        .s_axi_wstrb(s_axi_wstrb),
        .s_axi_wvalid(s_axi_wvalid));
endmodule

(* ORIG_REF_NAME = "address_decoder" *) 
module axi_fifo_mm_s_0_address_decoder
   (IPIC_STATE_reg,
    \sig_register_array_reg[0][0] ,
    \sig_register_array_reg[0][1] ,
    \sig_register_array_reg[0][2] ,
    \sig_register_array_reg[0][3] ,
    \sig_register_array_reg[0][4] ,
    \sig_register_array_reg[0][5] ,
    \sig_register_array_reg[0][6] ,
    \sig_register_array_reg[0][7] ,
    \sig_register_array_reg[0][8] ,
    \sig_register_array_reg[0][9] ,
    \sig_register_array_reg[0][10] ,
    \sig_register_array_reg[0][11] ,
    \sig_register_array_reg[0][12] ,
    sig_str_rst_reg,
    \sig_ip2bus_data_reg[31] ,
    \sig_txd_wr_data_reg[31] ,
    IP2Bus_RdAck_reg,
    \sig_register_array_reg[1][0] ,
    IP2Bus_Error,
    \sig_register_array_reg[0][2]_0 ,
    \sig_register_array_reg[0][1]_0 ,
    \sig_register_array_reg[1][0]_0 ,
    D,
    \sig_ip2bus_data_reg[30] ,
    \sig_ip2bus_data_reg[9] ,
    \sig_ip2bus_data_reg[9]_0 ,
    \sig_ip2bus_data_reg[22] ,
    sig_rx_channel_reset_reg,
    sig_tx_channel_reset_reg,
    sig_str_rst_reg_0,
    IP2Bus_WrAck_reg,
    sig_rd_rlen_reg,
    sig_txd_sb_wr_en_reg,
    cs_ce_clr,
    Q,
    s_axi_aclk,
    IPIC_STATE,
    \sig_register_array_reg[0][0]_0 ,
    \gaxi_full_sm.r_last_r_reg ,
    \gaxi_full_sm.r_last_r_reg_0 ,
    IP2Bus_Error1_in,
    \sig_register_array_reg[0][1]_1 ,
    \gaxi_full_sm.r_last_r_reg_1 ,
    sig_rx_channel_reset_reg_0,
    \sig_register_array_reg[0][2]_1 ,
    tx_fifo_or,
    s_axi_wdata,
    \sig_register_array_reg[0][3]_0 ,
    axi_str_txd_tready,
    \sig_register_array_reg[0][4]_0 ,
    p_13_in,
    \sig_register_array_reg[0][5]_0 ,
    \sig_register_array_reg[0][6]_0 ,
    sig_txd_reset,
    \sig_register_array_reg[0][7]_0 ,
    sig_rxd_reset,
    \sig_register_array_reg[0][8]_0 ,
    sig_txd_pf_event__1,
    \sig_register_array_reg[0][9]_0 ,
    sig_txd_pe_event__1,
    \sig_register_array_reg[0][10]_0 ,
    sig_rxd_pf_event__1,
    \sig_register_array_reg[0][11]_0 ,
    sig_rxd_pe_event__1,
    \sig_register_array_reg[0][12]_0 ,
    \goreg_bm.dout_i_reg[0] ,
    eqOp__6,
    s_axi_aresetn,
    sig_Bus2IP_RNW,
    sig_tx_channel_reset_reg_0,
    \gtxd.sig_txd_packet_size_reg[30] ,
    \goreg_dm.dout_i_reg[21] ,
    \gfifo_gen.gmm2s.vacancy_i_reg[1] ,
    \gfifo_gen.gmm2s.vacancy_i_reg[2] ,
    \count_reg[9] ,
    \gfifo_gen.gmm2s.vacancy_i_reg[3] ,
    \gfifo_gen.gmm2s.vacancy_i_reg[4] ,
    \gfifo_gen.gmm2s.vacancy_i_reg[5] ,
    \gfifo_gen.gmm2s.vacancy_i_reg[6] ,
    \gfifo_gen.gmm2s.vacancy_i_reg[7] ,
    \gfifo_gen.gmm2s.vacancy_i_reg[8] ,
    \gfifo_gen.gmm2s.vacancy_i_reg[9] ,
    out,
    sig_rx_channel_reset_reg_1,
    \sig_register_array_reg[1][10] ,
    sig_str_rst_reg_1,
    IP2Bus_RdAck_reg_0,
    IP2Bus_WrAck_reg_0,
    \bus2ip_addr_i_reg[5] );
  output IPIC_STATE_reg;
  output \sig_register_array_reg[0][0] ;
  output \sig_register_array_reg[0][1] ;
  output \sig_register_array_reg[0][2] ;
  output \sig_register_array_reg[0][3] ;
  output \sig_register_array_reg[0][4] ;
  output \sig_register_array_reg[0][5] ;
  output \sig_register_array_reg[0][6] ;
  output \sig_register_array_reg[0][7] ;
  output \sig_register_array_reg[0][8] ;
  output \sig_register_array_reg[0][9] ;
  output \sig_register_array_reg[0][10] ;
  output \sig_register_array_reg[0][11] ;
  output \sig_register_array_reg[0][12] ;
  output sig_str_rst_reg;
  output [0:0]\sig_ip2bus_data_reg[31] ;
  output [0:0]\sig_txd_wr_data_reg[31] ;
  output IP2Bus_RdAck_reg;
  output [0:0]\sig_register_array_reg[1][0] ;
  output IP2Bus_Error;
  output \sig_register_array_reg[0][2]_0 ;
  output \sig_register_array_reg[0][1]_0 ;
  output [12:0]\sig_register_array_reg[1][0]_0 ;
  output [21:0]D;
  output \sig_ip2bus_data_reg[30] ;
  output \sig_ip2bus_data_reg[9] ;
  output \sig_ip2bus_data_reg[9]_0 ;
  output \sig_ip2bus_data_reg[22] ;
  output sig_rx_channel_reset_reg;
  output sig_tx_channel_reset_reg;
  output sig_str_rst_reg_0;
  output IP2Bus_WrAck_reg;
  output sig_rd_rlen_reg;
  output sig_txd_sb_wr_en_reg;
  input cs_ce_clr;
  input Q;
  input s_axi_aclk;
  input IPIC_STATE;
  input \sig_register_array_reg[0][0]_0 ;
  input \gaxi_full_sm.r_last_r_reg ;
  input \gaxi_full_sm.r_last_r_reg_0 ;
  input IP2Bus_Error1_in;
  input \sig_register_array_reg[0][1]_1 ;
  input \gaxi_full_sm.r_last_r_reg_1 ;
  input sig_rx_channel_reset_reg_0;
  input \sig_register_array_reg[0][2]_1 ;
  input tx_fifo_or;
  input [12:0]s_axi_wdata;
  input \sig_register_array_reg[0][3]_0 ;
  input axi_str_txd_tready;
  input \sig_register_array_reg[0][4]_0 ;
  input p_13_in;
  input \sig_register_array_reg[0][5]_0 ;
  input \sig_register_array_reg[0][6]_0 ;
  input sig_txd_reset;
  input \sig_register_array_reg[0][7]_0 ;
  input sig_rxd_reset;
  input \sig_register_array_reg[0][8]_0 ;
  input sig_txd_pf_event__1;
  input \sig_register_array_reg[0][9]_0 ;
  input sig_txd_pe_event__1;
  input \sig_register_array_reg[0][10]_0 ;
  input sig_rxd_pf_event__1;
  input \sig_register_array_reg[0][11]_0 ;
  input sig_rxd_pe_event__1;
  input \sig_register_array_reg[0][12]_0 ;
  input \goreg_bm.dout_i_reg[0] ;
  input eqOp__6;
  input s_axi_aresetn;
  input sig_Bus2IP_RNW;
  input sig_tx_channel_reset_reg_0;
  input \gtxd.sig_txd_packet_size_reg[30] ;
  input [21:0]\goreg_dm.dout_i_reg[21] ;
  input [0:0]\gfifo_gen.gmm2s.vacancy_i_reg[1] ;
  input \gfifo_gen.gmm2s.vacancy_i_reg[2] ;
  input [9:0]\count_reg[9] ;
  input \gfifo_gen.gmm2s.vacancy_i_reg[3] ;
  input \gfifo_gen.gmm2s.vacancy_i_reg[4] ;
  input \gfifo_gen.gmm2s.vacancy_i_reg[5] ;
  input \gfifo_gen.gmm2s.vacancy_i_reg[6] ;
  input \gfifo_gen.gmm2s.vacancy_i_reg[7] ;
  input \gfifo_gen.gmm2s.vacancy_i_reg[8] ;
  input \gfifo_gen.gmm2s.vacancy_i_reg[9] ;
  input out;
  input sig_rx_channel_reset_reg_1;
  input [2:0]\sig_register_array_reg[1][10] ;
  input sig_str_rst_reg_1;
  input IP2Bus_RdAck_reg_0;
  input IP2Bus_WrAck_reg_0;
  input [3:0]\bus2ip_addr_i_reg[5] ;

  wire Bus_RNW_reg;
  wire Bus_RNW_reg_i_1_n_0;
  wire \COMP_IPIC2AXI_S/sig_rd_rlen ;
  wire [21:0]D;
  wire \GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg ;
  wire \GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ;
  wire \GEN_BKEND_CE_REGISTERS[11].ce_out_i_reg ;
  wire \GEN_BKEND_CE_REGISTERS[12].ce_out_i_reg ;
  wire \GEN_BKEND_CE_REGISTERS[1].ce_out_i_reg ;
  wire \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ;
  wire \GEN_BKEND_CE_REGISTERS[3].ce_out_i_reg ;
  wire \GEN_BKEND_CE_REGISTERS[4].ce_out_i_reg ;
  wire \GEN_BKEND_CE_REGISTERS[5].ce_out_i_reg ;
  wire \GEN_BKEND_CE_REGISTERS[6].ce_out_i_reg ;
  wire \GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg ;
  wire \GEN_BKEND_CE_REGISTERS[8].ce_out_i_reg ;
  wire \GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg ;
  wire IP2Bus_Error;
  wire IP2Bus_Error1_in;
  wire IP2Bus_RdAck_reg;
  wire IP2Bus_RdAck_reg_0;
  wire IP2Bus_WrAck_reg;
  wire IP2Bus_WrAck_reg_0;
  wire IPIC_STATE;
  wire IPIC_STATE_reg;
  wire \MEM_DECODE_GEN[0].cs_out_i[0]_i_1_n_0 ;
  wire Q;
  wire axi_str_txd_tready;
  wire [3:0]\bus2ip_addr_i_reg[5] ;
  wire ce_expnd_i_0;
  wire ce_expnd_i_1;
  wire ce_expnd_i_10;
  wire ce_expnd_i_11;
  wire ce_expnd_i_12;
  wire ce_expnd_i_2;
  wire ce_expnd_i_3;
  wire ce_expnd_i_4;
  wire ce_expnd_i_5;
  wire ce_expnd_i_6;
  wire ce_expnd_i_7;
  wire ce_expnd_i_8;
  wire ce_expnd_i_9;
  wire [9:0]\count_reg[9] ;
  wire cs_ce_clr;
  wire eqOp__6;
  wire \gaxi_full_sm.r_last_r_reg ;
  wire \gaxi_full_sm.r_last_r_reg_0 ;
  wire \gaxi_full_sm.r_last_r_reg_1 ;
  wire [0:0]\gfifo_gen.gmm2s.vacancy_i_reg[1] ;
  wire \gfifo_gen.gmm2s.vacancy_i_reg[2] ;
  wire \gfifo_gen.gmm2s.vacancy_i_reg[3] ;
  wire \gfifo_gen.gmm2s.vacancy_i_reg[4] ;
  wire \gfifo_gen.gmm2s.vacancy_i_reg[5] ;
  wire \gfifo_gen.gmm2s.vacancy_i_reg[6] ;
  wire \gfifo_gen.gmm2s.vacancy_i_reg[7] ;
  wire \gfifo_gen.gmm2s.vacancy_i_reg[8] ;
  wire \gfifo_gen.gmm2s.vacancy_i_reg[9] ;
  wire \goreg_bm.dout_i_reg[0] ;
  wire [21:0]\goreg_dm.dout_i_reg[21] ;
  wire \gtxd.sig_txd_packet_size_reg[30] ;
  wire out;
  wire p_13_in;
  wire s_axi_aclk;
  wire s_axi_aresetn;
  wire [12:0]s_axi_wdata;
  wire sig_Bus2IP_RNW;
  wire [7:7]sig_Bus2IP_RdCE;
  wire [0:12]sig_Bus2IP_WrCE;
  wire [5:5]sig_Bus2IP_WrCE__0;
  wire \sig_ip2bus_data[0]_i_5_n_0 ;
  wire \sig_ip2bus_data[0]_i_6_n_0 ;
  wire \sig_ip2bus_data[0]_i_7_n_0 ;
  wire \sig_ip2bus_data[10]_i_2_n_0 ;
  wire \sig_ip2bus_data[10]_i_3_n_0 ;
  wire \sig_ip2bus_data[10]_i_4_n_0 ;
  wire \sig_ip2bus_data[10]_i_5_n_0 ;
  wire \sig_ip2bus_data[10]_i_6_n_0 ;
  wire \sig_ip2bus_data[11]_i_2_n_0 ;
  wire \sig_ip2bus_data[12]_i_2_n_0 ;
  wire \sig_ip2bus_data[20]_i_2_n_0 ;
  wire \sig_ip2bus_data[20]_i_3_n_0 ;
  wire \sig_ip2bus_data[20]_i_4_n_0 ;
  wire \sig_ip2bus_data[20]_i_5_n_0 ;
  wire \sig_ip2bus_data[22]_i_2_n_0 ;
  wire \sig_ip2bus_data[22]_i_4_n_0 ;
  wire \sig_ip2bus_data[22]_i_5_n_0 ;
  wire \sig_ip2bus_data[22]_i_6_n_0 ;
  wire \sig_ip2bus_data[30]_i_3_n_0 ;
  wire \sig_ip2bus_data[31]_i_2_n_0 ;
  wire \sig_ip2bus_data[31]_i_3_n_0 ;
  wire \sig_ip2bus_data[31]_i_5_n_0 ;
  wire \sig_ip2bus_data_reg[22] ;
  wire \sig_ip2bus_data_reg[30] ;
  wire [0:0]\sig_ip2bus_data_reg[31] ;
  wire \sig_ip2bus_data_reg[9] ;
  wire \sig_ip2bus_data_reg[9]_0 ;
  wire sig_rd_rlen_reg;
  wire \sig_register_array[0][0]_i_2_n_0 ;
  wire \sig_register_array[0][0]_i_3_n_0 ;
  wire \sig_register_array[0][0]_i_4_n_0 ;
  wire \sig_register_array[0][0]_i_5_n_0 ;
  wire \sig_register_array[0][0]_i_6_n_0 ;
  wire \sig_register_array[0][0]_i_7_n_0 ;
  wire \sig_register_array[0][10]_i_3_n_0 ;
  wire \sig_register_array[0][11]_i_3_n_0 ;
  wire \sig_register_array[0][12]_i_3_n_0 ;
  wire \sig_register_array[0][1]_i_2_n_0 ;
  wire \sig_register_array[0][1]_i_3_n_0 ;
  wire \sig_register_array[0][1]_i_6_n_0 ;
  wire \sig_register_array[0][2]_i_2_n_0 ;
  wire \sig_register_array[0][3]_i_2_n_0 ;
  wire \sig_register_array[0][3]_i_5_n_0 ;
  wire \sig_register_array[0][4]_i_2_n_0 ;
  wire \sig_register_array[0][5]_i_3_n_0 ;
  wire \sig_register_array[0][6]_i_3_n_0 ;
  wire \sig_register_array[0][6]_i_4_n_0 ;
  wire \sig_register_array[0][6]_i_5_n_0 ;
  wire \sig_register_array[0][6]_i_6_n_0 ;
  wire \sig_register_array[0][7]_i_10_n_0 ;
  wire \sig_register_array[0][7]_i_11_n_0 ;
  wire \sig_register_array[0][7]_i_12_n_0 ;
  wire \sig_register_array[0][7]_i_2_n_0 ;
  wire \sig_register_array[0][7]_i_3_n_0 ;
  wire \sig_register_array[0][7]_i_4_n_0 ;
  wire \sig_register_array[0][7]_i_5_n_0 ;
  wire \sig_register_array[0][7]_i_6_n_0 ;
  wire \sig_register_array[0][7]_i_7_n_0 ;
  wire \sig_register_array[0][7]_i_8_n_0 ;
  wire \sig_register_array[0][7]_i_9_n_0 ;
  wire \sig_register_array[0][8]_i_2_n_0 ;
  wire \sig_register_array[0][8]_i_3_n_0 ;
  wire \sig_register_array[0][9]_i_3_n_0 ;
  wire \sig_register_array[1][0]_i_4_n_0 ;
  wire \sig_register_array[1][0]_i_5_n_0 ;
  wire \sig_register_array_reg[0][0] ;
  wire \sig_register_array_reg[0][0]_0 ;
  wire \sig_register_array_reg[0][10] ;
  wire \sig_register_array_reg[0][10]_0 ;
  wire \sig_register_array_reg[0][11] ;
  wire \sig_register_array_reg[0][11]_0 ;
  wire \sig_register_array_reg[0][12] ;
  wire \sig_register_array_reg[0][12]_0 ;
  wire \sig_register_array_reg[0][1] ;
  wire \sig_register_array_reg[0][1]_0 ;
  wire \sig_register_array_reg[0][1]_1 ;
  wire \sig_register_array_reg[0][2] ;
  wire \sig_register_array_reg[0][2]_0 ;
  wire \sig_register_array_reg[0][2]_1 ;
  wire \sig_register_array_reg[0][3] ;
  wire \sig_register_array_reg[0][3]_0 ;
  wire \sig_register_array_reg[0][4] ;
  wire \sig_register_array_reg[0][4]_0 ;
  wire \sig_register_array_reg[0][5] ;
  wire \sig_register_array_reg[0][5]_0 ;
  wire \sig_register_array_reg[0][6] ;
  wire \sig_register_array_reg[0][6]_0 ;
  wire \sig_register_array_reg[0][7] ;
  wire \sig_register_array_reg[0][7]_0 ;
  wire \sig_register_array_reg[0][8] ;
  wire \sig_register_array_reg[0][8]_0 ;
  wire \sig_register_array_reg[0][9] ;
  wire \sig_register_array_reg[0][9]_0 ;
  wire [0:0]\sig_register_array_reg[1][0] ;
  wire [12:0]\sig_register_array_reg[1][0]_0 ;
  wire [2:0]\sig_register_array_reg[1][10] ;
  wire sig_rx_channel_reset_i_2_n_0;
  wire sig_rx_channel_reset_i_3_n_0;
  wire sig_rx_channel_reset_i_4_n_0;
  wire sig_rx_channel_reset_i_5_n_0;
  wire sig_rx_channel_reset_reg;
  wire sig_rx_channel_reset_reg_0;
  wire sig_rx_channel_reset_reg_1;
  wire sig_rxd_pe_event__1;
  wire sig_rxd_pf_event__1;
  wire sig_rxd_reset;
  wire sig_str_rst_i_5_n_0;
  wire sig_str_rst_reg;
  wire sig_str_rst_reg_0;
  wire sig_str_rst_reg_1;
  wire sig_tx_channel_reset_i_2_n_0;
  wire sig_tx_channel_reset_i_3_n_0;
  wire sig_tx_channel_reset_i_4_n_0;
  wire sig_tx_channel_reset_reg;
  wire sig_tx_channel_reset_reg_0;
  wire sig_txd_pe_event__1;
  wire sig_txd_pf_event__1;
  wire sig_txd_reset;
  wire sig_txd_sb_wr_en_reg;
  wire \sig_txd_wr_data[31]_i_2_n_0 ;
  wire \sig_txd_wr_data[31]_i_3_n_0 ;
  wire \sig_txd_wr_data[31]_i_4_n_0 ;
  wire \sig_txd_wr_data[31]_i_6_n_0 ;
  wire \sig_txd_wr_data[31]_i_7_n_0 ;
  wire [0:0]\sig_txd_wr_data_reg[31] ;
  wire tx_fifo_or;

  (* SOFT_HLUTNM = "soft_lutpair97" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    Bus_RNW_reg_i_1
       (.I0(sig_Bus2IP_RNW),
        .I1(Q),
        .I2(Bus_RNW_reg),
        .O(Bus_RNW_reg_i_1_n_0));
  FDRE Bus_RNW_reg_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(Bus_RNW_reg_i_1_n_0),
        .Q(Bus_RNW_reg),
        .R(1'b0));
  FDRE \GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg[0] 
       (.C(s_axi_aclk),
        .CE(Q),
        .D(ce_expnd_i_12),
        .Q(\GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg ),
        .R(cs_ce_clr));
  FDRE \GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg[10] 
       (.C(s_axi_aclk),
        .CE(Q),
        .D(ce_expnd_i_2),
        .Q(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .R(cs_ce_clr));
  FDRE \GEN_BKEND_CE_REGISTERS[11].ce_out_i_reg[11] 
       (.C(s_axi_aclk),
        .CE(Q),
        .D(ce_expnd_i_1),
        .Q(\GEN_BKEND_CE_REGISTERS[11].ce_out_i_reg ),
        .R(cs_ce_clr));
  FDRE \GEN_BKEND_CE_REGISTERS[12].ce_out_i_reg[12] 
       (.C(s_axi_aclk),
        .CE(Q),
        .D(ce_expnd_i_0),
        .Q(\GEN_BKEND_CE_REGISTERS[12].ce_out_i_reg ),
        .R(cs_ce_clr));
  FDRE \GEN_BKEND_CE_REGISTERS[1].ce_out_i_reg[1] 
       (.C(s_axi_aclk),
        .CE(Q),
        .D(ce_expnd_i_11),
        .Q(\GEN_BKEND_CE_REGISTERS[1].ce_out_i_reg ),
        .R(cs_ce_clr));
  FDRE \GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg[2] 
       (.C(s_axi_aclk),
        .CE(Q),
        .D(ce_expnd_i_10),
        .Q(\GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ),
        .R(cs_ce_clr));
  LUT5 #(
    .INIT(32'h10000000)) 
    \GEN_BKEND_CE_REGISTERS[3].ce_out_i[3]_i_1 
       (.I0(\bus2ip_addr_i_reg[5] [3]),
        .I1(\bus2ip_addr_i_reg[5] [2]),
        .I2(Q),
        .I3(\bus2ip_addr_i_reg[5] [0]),
        .I4(\bus2ip_addr_i_reg[5] [1]),
        .O(ce_expnd_i_9));
  FDRE \GEN_BKEND_CE_REGISTERS[3].ce_out_i_reg[3] 
       (.C(s_axi_aclk),
        .CE(Q),
        .D(ce_expnd_i_9),
        .Q(\GEN_BKEND_CE_REGISTERS[3].ce_out_i_reg ),
        .R(cs_ce_clr));
  FDRE \GEN_BKEND_CE_REGISTERS[4].ce_out_i_reg[4] 
       (.C(s_axi_aclk),
        .CE(Q),
        .D(ce_expnd_i_8),
        .Q(\GEN_BKEND_CE_REGISTERS[4].ce_out_i_reg ),
        .R(cs_ce_clr));
  FDRE \GEN_BKEND_CE_REGISTERS[5].ce_out_i_reg[5] 
       (.C(s_axi_aclk),
        .CE(Q),
        .D(ce_expnd_i_7),
        .Q(\GEN_BKEND_CE_REGISTERS[5].ce_out_i_reg ),
        .R(cs_ce_clr));
  FDRE \GEN_BKEND_CE_REGISTERS[6].ce_out_i_reg[6] 
       (.C(s_axi_aclk),
        .CE(Q),
        .D(ce_expnd_i_6),
        .Q(\GEN_BKEND_CE_REGISTERS[6].ce_out_i_reg ),
        .R(cs_ce_clr));
  FDRE \GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg[7] 
       (.C(s_axi_aclk),
        .CE(Q),
        .D(ce_expnd_i_5),
        .Q(\GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg ),
        .R(cs_ce_clr));
  FDRE \GEN_BKEND_CE_REGISTERS[8].ce_out_i_reg[8] 
       (.C(s_axi_aclk),
        .CE(Q),
        .D(ce_expnd_i_4),
        .Q(\GEN_BKEND_CE_REGISTERS[8].ce_out_i_reg ),
        .R(cs_ce_clr));
  FDRE \GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg[9] 
       (.C(s_axi_aclk),
        .CE(Q),
        .D(ce_expnd_i_3),
        .Q(\GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg ),
        .R(cs_ce_clr));
  LUT6 #(
    .INIT(64'h0000000000004000)) 
    IP2Bus_Error_i_2
       (.I0(sig_rx_channel_reset_reg_1),
        .I1(Bus_RNW_reg),
        .I2(\GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg ),
        .I3(out),
        .I4(\sig_ip2bus_data[20]_i_2_n_0 ),
        .I5(\sig_ip2bus_data[20]_i_4_n_0 ),
        .O(IP2Bus_Error));
  (* SOFT_HLUTNM = "soft_lutpair87" *) 
  LUT3 #(
    .INIT(8'h08)) 
    IP2Bus_RdAck_i_2
       (.I0(sig_Bus2IP_RNW),
        .I1(IPIC_STATE_reg),
        .I2(IPIC_STATE),
        .O(IP2Bus_RdAck_reg));
  (* SOFT_HLUTNM = "soft_lutpair87" *) 
  LUT4 #(
    .INIT(16'h0400)) 
    IP2Bus_WrAck_i_1
       (.I0(sig_Bus2IP_RNW),
        .I1(s_axi_aresetn),
        .I2(IPIC_STATE),
        .I3(IPIC_STATE_reg),
        .O(IP2Bus_WrAck_reg));
  axi_fifo_mm_s_0_pselect_f \MEM_DECODE_GEN[0].PER_CE_GEN[0].MULTIPLE_CES_THIS_CS_GEN.CE_I 
       (.Q(Q),
        .\bus2ip_addr_i_reg[5] (\bus2ip_addr_i_reg[5] ),
        .ce_expnd_i_12(ce_expnd_i_12));
  axi_fifo_mm_s_0_pselect_f__parameterized9 \MEM_DECODE_GEN[0].PER_CE_GEN[10].MULTIPLE_CES_THIS_CS_GEN.CE_I 
       (.Q(Q),
        .\bus2ip_addr_i_reg[5] (\bus2ip_addr_i_reg[5] ),
        .ce_expnd_i_2(ce_expnd_i_2));
  axi_fifo_mm_s_0_pselect_f__parameterized10 \MEM_DECODE_GEN[0].PER_CE_GEN[11].MULTIPLE_CES_THIS_CS_GEN.CE_I 
       (.Q(Q),
        .\bus2ip_addr_i_reg[5] (\bus2ip_addr_i_reg[5] ),
        .ce_expnd_i_1(ce_expnd_i_1));
  axi_fifo_mm_s_0_pselect_f__parameterized11 \MEM_DECODE_GEN[0].PER_CE_GEN[12].MULTIPLE_CES_THIS_CS_GEN.CE_I 
       (.Q(Q),
        .\bus2ip_addr_i_reg[5] (\bus2ip_addr_i_reg[5] ),
        .ce_expnd_i_0(ce_expnd_i_0));
  axi_fifo_mm_s_0_pselect_f__parameterized0 \MEM_DECODE_GEN[0].PER_CE_GEN[1].MULTIPLE_CES_THIS_CS_GEN.CE_I 
       (.Q(Q),
        .\bus2ip_addr_i_reg[5] (\bus2ip_addr_i_reg[5] ),
        .ce_expnd_i_11(ce_expnd_i_11));
  axi_fifo_mm_s_0_pselect_f__parameterized1 \MEM_DECODE_GEN[0].PER_CE_GEN[2].MULTIPLE_CES_THIS_CS_GEN.CE_I 
       (.Q(Q),
        .\bus2ip_addr_i_reg[5] (\bus2ip_addr_i_reg[5] ),
        .ce_expnd_i_10(ce_expnd_i_10));
  axi_fifo_mm_s_0_pselect_f__parameterized3 \MEM_DECODE_GEN[0].PER_CE_GEN[4].MULTIPLE_CES_THIS_CS_GEN.CE_I 
       (.Q(Q),
        .\bus2ip_addr_i_reg[5] (\bus2ip_addr_i_reg[5] ),
        .ce_expnd_i_8(ce_expnd_i_8));
  axi_fifo_mm_s_0_pselect_f__parameterized4 \MEM_DECODE_GEN[0].PER_CE_GEN[5].MULTIPLE_CES_THIS_CS_GEN.CE_I 
       (.Q(Q),
        .\bus2ip_addr_i_reg[5] (\bus2ip_addr_i_reg[5] ),
        .ce_expnd_i_7(ce_expnd_i_7));
  axi_fifo_mm_s_0_pselect_f__parameterized5 \MEM_DECODE_GEN[0].PER_CE_GEN[6].MULTIPLE_CES_THIS_CS_GEN.CE_I 
       (.Q(Q),
        .\bus2ip_addr_i_reg[5] (\bus2ip_addr_i_reg[5] ),
        .ce_expnd_i_6(ce_expnd_i_6));
  axi_fifo_mm_s_0_pselect_f__parameterized6 \MEM_DECODE_GEN[0].PER_CE_GEN[7].MULTIPLE_CES_THIS_CS_GEN.CE_I 
       (.Q(Q),
        .\bus2ip_addr_i_reg[5] (\bus2ip_addr_i_reg[5] ),
        .ce_expnd_i_5(ce_expnd_i_5));
  axi_fifo_mm_s_0_pselect_f__parameterized7 \MEM_DECODE_GEN[0].PER_CE_GEN[8].MULTIPLE_CES_THIS_CS_GEN.CE_I 
       (.Q(Q),
        .\bus2ip_addr_i_reg[5] (\bus2ip_addr_i_reg[5] ),
        .ce_expnd_i_4(ce_expnd_i_4));
  axi_fifo_mm_s_0_pselect_f__parameterized8 \MEM_DECODE_GEN[0].PER_CE_GEN[9].MULTIPLE_CES_THIS_CS_GEN.CE_I 
       (.Q(Q),
        .\bus2ip_addr_i_reg[5] (\bus2ip_addr_i_reg[5] ),
        .ce_expnd_i_3(ce_expnd_i_3));
  LUT5 #(
    .INIT(32'h000000E0)) 
    \MEM_DECODE_GEN[0].cs_out_i[0]_i_1 
       (.I0(IPIC_STATE_reg),
        .I1(Q),
        .I2(s_axi_aresetn),
        .I3(IP2Bus_RdAck_reg_0),
        .I4(IP2Bus_WrAck_reg_0),
        .O(\MEM_DECODE_GEN[0].cs_out_i[0]_i_1_n_0 ));
  FDRE \MEM_DECODE_GEN[0].cs_out_i_reg[0] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(\MEM_DECODE_GEN[0].cs_out_i[0]_i_1_n_0 ),
        .Q(IPIC_STATE_reg),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair84" *) 
  LUT3 #(
    .INIT(8'hDF)) 
    \sig_ip2bus_data[0]_i_1 
       (.I0(IPIC_STATE_reg),
        .I1(IPIC_STATE),
        .I2(s_axi_aresetn),
        .O(\sig_ip2bus_data_reg[31] ));
  (* SOFT_HLUTNM = "soft_lutpair90" *) 
  LUT4 #(
    .INIT(16'h4000)) 
    \sig_ip2bus_data[0]_i_3 
       (.I0(\GEN_BKEND_CE_REGISTERS[1].ce_out_i_reg ),
        .I1(Bus_RNW_reg),
        .I2(\GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg ),
        .I3(\sig_ip2bus_data[0]_i_5_n_0 ),
        .O(\sig_ip2bus_data_reg[9] ));
  (* SOFT_HLUTNM = "soft_lutpair90" *) 
  LUT4 #(
    .INIT(16'h4000)) 
    \sig_ip2bus_data[0]_i_4 
       (.I0(\GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg ),
        .I1(Bus_RNW_reg),
        .I2(\GEN_BKEND_CE_REGISTERS[1].ce_out_i_reg ),
        .I3(\sig_ip2bus_data[0]_i_5_n_0 ),
        .O(\sig_ip2bus_data_reg[9]_0 ));
  LUT6 #(
    .INIT(64'h0000000000000515)) 
    \sig_ip2bus_data[0]_i_5 
       (.I0(\sig_ip2bus_data[31]_i_5_n_0 ),
        .I1(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I2(Bus_RNW_reg),
        .I3(\GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg ),
        .I4(\sig_ip2bus_data[0]_i_6_n_0 ),
        .I5(\sig_ip2bus_data[0]_i_7_n_0 ),
        .O(\sig_ip2bus_data[0]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair77" *) 
  LUT3 #(
    .INIT(8'hC8)) 
    \sig_ip2bus_data[0]_i_6 
       (.I0(\GEN_BKEND_CE_REGISTERS[12].ce_out_i_reg ),
        .I1(Bus_RNW_reg),
        .I2(\GEN_BKEND_CE_REGISTERS[11].ce_out_i_reg ),
        .O(\sig_ip2bus_data[0]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair76" *) 
  LUT5 #(
    .INIT(32'hF0F0F0E0)) 
    \sig_ip2bus_data[0]_i_7 
       (.I0(\GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg ),
        .I1(\GEN_BKEND_CE_REGISTERS[8].ce_out_i_reg ),
        .I2(Bus_RNW_reg),
        .I3(\GEN_BKEND_CE_REGISTERS[5].ce_out_i_reg ),
        .I4(\GEN_BKEND_CE_REGISTERS[6].ce_out_i_reg ),
        .O(\sig_ip2bus_data[0]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hAAAA0080)) 
    \sig_ip2bus_data[10]_i_1 
       (.I0(\sig_ip2bus_data[10]_i_2_n_0 ),
        .I1(\goreg_dm.dout_i_reg[21] [21]),
        .I2(\sig_ip2bus_data[10]_i_3_n_0 ),
        .I3(\sig_ip2bus_data[10]_i_4_n_0 ),
        .I4(\sig_ip2bus_data[10]_i_5_n_0 ),
        .O(D[21]));
  LUT6 #(
    .INIT(64'h0000000000FF01FF)) 
    \sig_ip2bus_data[10]_i_2 
       (.I0(\GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ),
        .I1(\GEN_BKEND_CE_REGISTERS[3].ce_out_i_reg ),
        .I2(\GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg ),
        .I3(Bus_RNW_reg),
        .I4(\GEN_BKEND_CE_REGISTERS[4].ce_out_i_reg ),
        .I5(\sig_ip2bus_data[10]_i_6_n_0 ),
        .O(\sig_ip2bus_data[10]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair88" *) 
  LUT4 #(
    .INIT(16'h0008)) 
    \sig_ip2bus_data[10]_i_3 
       (.I0(\GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg ),
        .I1(Bus_RNW_reg),
        .I2(sig_rx_channel_reset_reg_1),
        .I3(out),
        .O(\sig_ip2bus_data[10]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair94" *) 
  LUT3 #(
    .INIT(8'hC8)) 
    \sig_ip2bus_data[10]_i_4 
       (.I0(\GEN_BKEND_CE_REGISTERS[1].ce_out_i_reg ),
        .I1(Bus_RNW_reg),
        .I2(\GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg ),
        .O(\sig_ip2bus_data[10]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h0000000020C02000)) 
    \sig_ip2bus_data[10]_i_5 
       (.I0(\sig_register_array_reg[0][10]_0 ),
        .I1(\GEN_BKEND_CE_REGISTERS[1].ce_out_i_reg ),
        .I2(Bus_RNW_reg),
        .I3(\GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg ),
        .I4(\sig_register_array_reg[1][10] [2]),
        .I5(\GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg ),
        .O(\sig_ip2bus_data[10]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hFAFAFAFAFAFAFAEA)) 
    \sig_ip2bus_data[10]_i_6 
       (.I0(\sig_ip2bus_data[20]_i_5_n_0 ),
        .I1(\GEN_BKEND_CE_REGISTERS[12].ce_out_i_reg ),
        .I2(Bus_RNW_reg),
        .I3(\GEN_BKEND_CE_REGISTERS[11].ce_out_i_reg ),
        .I4(\GEN_BKEND_CE_REGISTERS[8].ce_out_i_reg ),
        .I5(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .O(\sig_ip2bus_data[10]_i_6_n_0 ));
  LUT5 #(
    .INIT(32'hAAAA0080)) 
    \sig_ip2bus_data[11]_i_1 
       (.I0(\sig_ip2bus_data[10]_i_2_n_0 ),
        .I1(\goreg_dm.dout_i_reg[21] [20]),
        .I2(\sig_ip2bus_data[10]_i_3_n_0 ),
        .I3(\sig_ip2bus_data[10]_i_4_n_0 ),
        .I4(\sig_ip2bus_data[11]_i_2_n_0 ),
        .O(D[20]));
  LUT6 #(
    .INIT(64'h0000000020C02000)) 
    \sig_ip2bus_data[11]_i_2 
       (.I0(\sig_register_array_reg[0][11]_0 ),
        .I1(\GEN_BKEND_CE_REGISTERS[1].ce_out_i_reg ),
        .I2(Bus_RNW_reg),
        .I3(\GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg ),
        .I4(\sig_register_array_reg[1][10] [1]),
        .I5(\GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg ),
        .O(\sig_ip2bus_data[11]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hAAAA0080)) 
    \sig_ip2bus_data[12]_i_1 
       (.I0(\sig_ip2bus_data[10]_i_2_n_0 ),
        .I1(\goreg_dm.dout_i_reg[21] [19]),
        .I2(\sig_ip2bus_data[10]_i_3_n_0 ),
        .I3(\sig_ip2bus_data[10]_i_4_n_0 ),
        .I4(\sig_ip2bus_data[12]_i_2_n_0 ),
        .O(D[19]));
  LUT6 #(
    .INIT(64'h0000000020C02000)) 
    \sig_ip2bus_data[12]_i_2 
       (.I0(\sig_register_array_reg[0][12]_0 ),
        .I1(\GEN_BKEND_CE_REGISTERS[1].ce_out_i_reg ),
        .I2(Bus_RNW_reg),
        .I3(\GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg ),
        .I4(\sig_register_array_reg[1][10] [0]),
        .I5(\GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg ),
        .O(\sig_ip2bus_data[12]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair102" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \sig_ip2bus_data[13]_i_1 
       (.I0(\COMP_IPIC2AXI_S/sig_rd_rlen ),
        .I1(\goreg_dm.dout_i_reg[21] [18]),
        .O(D[18]));
  LUT6 #(
    .INIT(64'h0001000000000000)) 
    \sig_ip2bus_data[13]_i_2 
       (.I0(\sig_ip2bus_data[20]_i_2_n_0 ),
        .I1(\sig_ip2bus_data[20]_i_4_n_0 ),
        .I2(out),
        .I3(sig_rx_channel_reset_reg_1),
        .I4(Bus_RNW_reg),
        .I5(\GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg ),
        .O(\COMP_IPIC2AXI_S/sig_rd_rlen ));
  (* SOFT_HLUTNM = "soft_lutpair103" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \sig_ip2bus_data[14]_i_1 
       (.I0(\COMP_IPIC2AXI_S/sig_rd_rlen ),
        .I1(\goreg_dm.dout_i_reg[21] [17]),
        .O(D[17]));
  (* SOFT_HLUTNM = "soft_lutpair102" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \sig_ip2bus_data[15]_i_1 
       (.I0(\COMP_IPIC2AXI_S/sig_rd_rlen ),
        .I1(\goreg_dm.dout_i_reg[21] [16]),
        .O(D[16]));
  (* SOFT_HLUTNM = "soft_lutpair103" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \sig_ip2bus_data[16]_i_1 
       (.I0(\COMP_IPIC2AXI_S/sig_rd_rlen ),
        .I1(\goreg_dm.dout_i_reg[21] [15]),
        .O(D[15]));
  (* SOFT_HLUTNM = "soft_lutpair104" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \sig_ip2bus_data[17]_i_1 
       (.I0(\COMP_IPIC2AXI_S/sig_rd_rlen ),
        .I1(\goreg_dm.dout_i_reg[21] [14]),
        .O(D[14]));
  (* SOFT_HLUTNM = "soft_lutpair104" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \sig_ip2bus_data[18]_i_1 
       (.I0(\COMP_IPIC2AXI_S/sig_rd_rlen ),
        .I1(\goreg_dm.dout_i_reg[21] [13]),
        .O(D[13]));
  LUT2 #(
    .INIT(4'h8)) 
    \sig_ip2bus_data[19]_i_1 
       (.I0(\COMP_IPIC2AXI_S/sig_rd_rlen ),
        .I1(\goreg_dm.dout_i_reg[21] [12]),
        .O(D[12]));
  LUT6 #(
    .INIT(64'h0000000000540010)) 
    \sig_ip2bus_data[20]_i_1 
       (.I0(\sig_ip2bus_data[20]_i_2_n_0 ),
        .I1(out),
        .I2(\goreg_dm.dout_i_reg[21] [11]),
        .I3(\sig_ip2bus_data[20]_i_3_n_0 ),
        .I4(\count_reg[9] [9]),
        .I5(\sig_ip2bus_data[20]_i_4_n_0 ),
        .O(D[11]));
  LUT6 #(
    .INIT(64'hFFAAFFAAFFAAFEAA)) 
    \sig_ip2bus_data[20]_i_2 
       (.I0(\sig_ip2bus_data[10]_i_4_n_0 ),
        .I1(\GEN_BKEND_CE_REGISTERS[11].ce_out_i_reg ),
        .I2(\GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ),
        .I3(Bus_RNW_reg),
        .I4(\GEN_BKEND_CE_REGISTERS[3].ce_out_i_reg ),
        .I5(\GEN_BKEND_CE_REGISTERS[4].ce_out_i_reg ),
        .O(\sig_ip2bus_data[20]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair88" *) 
  LUT3 #(
    .INIT(8'hBF)) 
    \sig_ip2bus_data[20]_i_3 
       (.I0(sig_rx_channel_reset_reg_1),
        .I1(Bus_RNW_reg),
        .I2(\GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg ),
        .O(\sig_ip2bus_data[20]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFAAAAFFFEAAAA)) 
    \sig_ip2bus_data[20]_i_4 
       (.I0(\sig_ip2bus_data[20]_i_5_n_0 ),
        .I1(\GEN_BKEND_CE_REGISTERS[8].ce_out_i_reg ),
        .I2(\GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg ),
        .I3(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I4(Bus_RNW_reg),
        .I5(\GEN_BKEND_CE_REGISTERS[12].ce_out_i_reg ),
        .O(\sig_ip2bus_data[20]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair85" *) 
  LUT3 #(
    .INIT(8'hE0)) 
    \sig_ip2bus_data[20]_i_5 
       (.I0(\GEN_BKEND_CE_REGISTERS[6].ce_out_i_reg ),
        .I1(\GEN_BKEND_CE_REGISTERS[5].ce_out_i_reg ),
        .I2(Bus_RNW_reg),
        .O(\sig_ip2bus_data[20]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000540010)) 
    \sig_ip2bus_data[21]_i_1 
       (.I0(\sig_ip2bus_data[20]_i_2_n_0 ),
        .I1(out),
        .I2(\goreg_dm.dout_i_reg[21] [10]),
        .I3(\sig_ip2bus_data[20]_i_3_n_0 ),
        .I4(\count_reg[9] [8]),
        .I5(\sig_ip2bus_data[20]_i_4_n_0 ),
        .O(D[10]));
  LUT6 #(
    .INIT(64'hAAAAA888A888A888)) 
    \sig_ip2bus_data[22]_i_1 
       (.I0(\sig_ip2bus_data[22]_i_2_n_0 ),
        .I1(\gfifo_gen.gmm2s.vacancy_i_reg[9] ),
        .I2(\count_reg[9] [7]),
        .I3(\sig_ip2bus_data[22]_i_4_n_0 ),
        .I4(\goreg_dm.dout_i_reg[21] [9]),
        .I5(\sig_ip2bus_data[22]_i_5_n_0 ),
        .O(D[9]));
  LUT6 #(
    .INIT(64'h0000000000000515)) 
    \sig_ip2bus_data[22]_i_2 
       (.I0(\sig_ip2bus_data[10]_i_4_n_0 ),
        .I1(\GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ),
        .I2(Bus_RNW_reg),
        .I3(\GEN_BKEND_CE_REGISTERS[4].ce_out_i_reg ),
        .I4(\sig_ip2bus_data[20]_i_5_n_0 ),
        .I5(\sig_ip2bus_data[22]_i_6_n_0 ),
        .O(\sig_ip2bus_data[22]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000400000)) 
    \sig_ip2bus_data[22]_i_4 
       (.I0(sig_rx_channel_reset_reg_1),
        .I1(\GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg ),
        .I2(out),
        .I3(\GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg ),
        .I4(Bus_RNW_reg),
        .I5(\GEN_BKEND_CE_REGISTERS[3].ce_out_i_reg ),
        .O(\sig_ip2bus_data[22]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000100000)) 
    \sig_ip2bus_data[22]_i_5 
       (.I0(out),
        .I1(sig_rx_channel_reset_reg_1),
        .I2(\GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg ),
        .I3(\GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg ),
        .I4(Bus_RNW_reg),
        .I5(\GEN_BKEND_CE_REGISTERS[3].ce_out_i_reg ),
        .O(\sig_ip2bus_data[22]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair82" *) 
  LUT5 #(
    .INIT(32'hFF00FE00)) 
    \sig_ip2bus_data[22]_i_6 
       (.I0(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I1(\GEN_BKEND_CE_REGISTERS[8].ce_out_i_reg ),
        .I2(\GEN_BKEND_CE_REGISTERS[11].ce_out_i_reg ),
        .I3(Bus_RNW_reg),
        .I4(\GEN_BKEND_CE_REGISTERS[12].ce_out_i_reg ),
        .O(\sig_ip2bus_data[22]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair81" *) 
  LUT5 #(
    .INIT(32'h00000200)) 
    \sig_ip2bus_data[22]_i_7 
       (.I0(\GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg ),
        .I1(\GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg ),
        .I2(out),
        .I3(Bus_RNW_reg),
        .I4(\GEN_BKEND_CE_REGISTERS[3].ce_out_i_reg ),
        .O(\sig_ip2bus_data_reg[22] ));
  LUT6 #(
    .INIT(64'hAAAAA888A888A888)) 
    \sig_ip2bus_data[23]_i_1 
       (.I0(\sig_ip2bus_data[22]_i_2_n_0 ),
        .I1(\gfifo_gen.gmm2s.vacancy_i_reg[8] ),
        .I2(\count_reg[9] [6]),
        .I3(\sig_ip2bus_data[22]_i_4_n_0 ),
        .I4(\goreg_dm.dout_i_reg[21] [8]),
        .I5(\sig_ip2bus_data[22]_i_5_n_0 ),
        .O(D[8]));
  LUT6 #(
    .INIT(64'hAAAAA888A888A888)) 
    \sig_ip2bus_data[24]_i_1 
       (.I0(\sig_ip2bus_data[22]_i_2_n_0 ),
        .I1(\gfifo_gen.gmm2s.vacancy_i_reg[7] ),
        .I2(\count_reg[9] [5]),
        .I3(\sig_ip2bus_data[22]_i_4_n_0 ),
        .I4(\goreg_dm.dout_i_reg[21] [7]),
        .I5(\sig_ip2bus_data[22]_i_5_n_0 ),
        .O(D[7]));
  LUT6 #(
    .INIT(64'hAAAAA888A888A888)) 
    \sig_ip2bus_data[25]_i_1 
       (.I0(\sig_ip2bus_data[22]_i_2_n_0 ),
        .I1(\gfifo_gen.gmm2s.vacancy_i_reg[6] ),
        .I2(\count_reg[9] [4]),
        .I3(\sig_ip2bus_data[22]_i_4_n_0 ),
        .I4(\goreg_dm.dout_i_reg[21] [6]),
        .I5(\sig_ip2bus_data[22]_i_5_n_0 ),
        .O(D[6]));
  LUT6 #(
    .INIT(64'hAAAAA888A888A888)) 
    \sig_ip2bus_data[26]_i_1 
       (.I0(\sig_ip2bus_data[22]_i_2_n_0 ),
        .I1(\gfifo_gen.gmm2s.vacancy_i_reg[5] ),
        .I2(\count_reg[9] [3]),
        .I3(\sig_ip2bus_data[22]_i_4_n_0 ),
        .I4(\goreg_dm.dout_i_reg[21] [5]),
        .I5(\sig_ip2bus_data[22]_i_5_n_0 ),
        .O(D[5]));
  LUT6 #(
    .INIT(64'hAAAAA888A888A888)) 
    \sig_ip2bus_data[27]_i_1 
       (.I0(\sig_ip2bus_data[22]_i_2_n_0 ),
        .I1(\gfifo_gen.gmm2s.vacancy_i_reg[4] ),
        .I2(\count_reg[9] [2]),
        .I3(\sig_ip2bus_data[22]_i_4_n_0 ),
        .I4(\goreg_dm.dout_i_reg[21] [4]),
        .I5(\sig_ip2bus_data[22]_i_5_n_0 ),
        .O(D[4]));
  LUT6 #(
    .INIT(64'hAAAAA888A888A888)) 
    \sig_ip2bus_data[28]_i_1 
       (.I0(\sig_ip2bus_data[22]_i_2_n_0 ),
        .I1(\gfifo_gen.gmm2s.vacancy_i_reg[3] ),
        .I2(\count_reg[9] [1]),
        .I3(\sig_ip2bus_data[22]_i_4_n_0 ),
        .I4(\goreg_dm.dout_i_reg[21] [3]),
        .I5(\sig_ip2bus_data[22]_i_5_n_0 ),
        .O(D[3]));
  LUT6 #(
    .INIT(64'hAAAAA888A888A888)) 
    \sig_ip2bus_data[29]_i_1 
       (.I0(\sig_ip2bus_data[22]_i_2_n_0 ),
        .I1(\gfifo_gen.gmm2s.vacancy_i_reg[2] ),
        .I2(\count_reg[9] [0]),
        .I3(\sig_ip2bus_data[22]_i_4_n_0 ),
        .I4(\goreg_dm.dout_i_reg[21] [2]),
        .I5(\sig_ip2bus_data[22]_i_5_n_0 ),
        .O(D[2]));
  LUT6 #(
    .INIT(64'hAAAAAAAAAA808080)) 
    \sig_ip2bus_data[30]_i_1 
       (.I0(\sig_ip2bus_data[22]_i_2_n_0 ),
        .I1(\sig_ip2bus_data[22]_i_5_n_0 ),
        .I2(\goreg_dm.dout_i_reg[21] [1]),
        .I3(\gfifo_gen.gmm2s.vacancy_i_reg[1] ),
        .I4(\sig_ip2bus_data_reg[30] ),
        .I5(\sig_ip2bus_data[30]_i_3_n_0 ),
        .O(D[1]));
  (* SOFT_HLUTNM = "soft_lutpair83" *) 
  LUT4 #(
    .INIT(16'h0040)) 
    \sig_ip2bus_data[30]_i_2 
       (.I0(\GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg ),
        .I1(\GEN_BKEND_CE_REGISTERS[3].ce_out_i_reg ),
        .I2(Bus_RNW_reg),
        .I3(\GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg ),
        .O(\sig_ip2bus_data_reg[30] ));
  LUT6 #(
    .INIT(64'h0004000000000000)) 
    \sig_ip2bus_data[30]_i_3 
       (.I0(\GEN_BKEND_CE_REGISTERS[3].ce_out_i_reg ),
        .I1(Bus_RNW_reg),
        .I2(out),
        .I3(\GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg ),
        .I4(\GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg ),
        .I5(\count_reg[9] [1]),
        .O(\sig_ip2bus_data[30]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h80F0808080808080)) 
    \sig_ip2bus_data[31]_i_1 
       (.I0(\sig_ip2bus_data[31]_i_2_n_0 ),
        .I1(\count_reg[9] [0]),
        .I2(\sig_ip2bus_data[31]_i_3_n_0 ),
        .I3(sig_Bus2IP_RdCE),
        .I4(\goreg_dm.dout_i_reg[21] [0]),
        .I5(\sig_ip2bus_data[10]_i_3_n_0 ),
        .O(D[0]));
  (* SOFT_HLUTNM = "soft_lutpair81" *) 
  LUT4 #(
    .INIT(16'h1000)) 
    \sig_ip2bus_data[31]_i_2 
       (.I0(out),
        .I1(\GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg ),
        .I2(Bus_RNW_reg),
        .I3(\GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg ),
        .O(\sig_ip2bus_data[31]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000001115)) 
    \sig_ip2bus_data[31]_i_3 
       (.I0(\sig_ip2bus_data[31]_i_5_n_0 ),
        .I1(Bus_RNW_reg),
        .I2(\GEN_BKEND_CE_REGISTERS[5].ce_out_i_reg ),
        .I3(\GEN_BKEND_CE_REGISTERS[6].ce_out_i_reg ),
        .I4(\sig_ip2bus_data[10]_i_4_n_0 ),
        .I5(\sig_ip2bus_data[22]_i_6_n_0 ),
        .O(\sig_ip2bus_data[31]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \sig_ip2bus_data[31]_i_4 
       (.I0(\GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg ),
        .I1(Bus_RNW_reg),
        .O(sig_Bus2IP_RdCE));
  (* SOFT_HLUTNM = "soft_lutpair80" *) 
  LUT4 #(
    .INIT(16'hF0E0)) 
    \sig_ip2bus_data[31]_i_5 
       (.I0(\GEN_BKEND_CE_REGISTERS[4].ce_out_i_reg ),
        .I1(\GEN_BKEND_CE_REGISTERS[3].ce_out_i_reg ),
        .I2(Bus_RNW_reg),
        .I3(\GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ),
        .O(\sig_ip2bus_data[31]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair84" *) 
  LUT4 #(
    .INIT(16'h0800)) 
    sig_rd_rlen_i_1
       (.I0(\COMP_IPIC2AXI_S/sig_rd_rlen ),
        .I1(s_axi_aresetn),
        .I2(IPIC_STATE),
        .I3(IPIC_STATE_reg),
        .O(sig_rd_rlen_reg));
  LUT4 #(
    .INIT(16'hFE02)) 
    \sig_register_array[0][0]_i_1 
       (.I0(\sig_register_array[0][0]_i_2_n_0 ),
        .I1(\sig_register_array[0][0]_i_3_n_0 ),
        .I2(IPIC_STATE),
        .I3(\sig_register_array_reg[0][0]_0 ),
        .O(\sig_register_array_reg[0][0] ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFF4)) 
    \sig_register_array[0][0]_i_2 
       (.I0(Bus_RNW_reg),
        .I1(\GEN_BKEND_CE_REGISTERS[6].ce_out_i_reg ),
        .I2(\sig_register_array[0][0]_i_4_n_0 ),
        .I3(\sig_register_array[0][0]_i_5_n_0 ),
        .I4(\sig_register_array[0][0]_i_6_n_0 ),
        .I5(\sig_register_array[0][0]_i_7_n_0 ),
        .O(\sig_register_array[0][0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h7777777757555757)) 
    \sig_register_array[0][0]_i_3 
       (.I0(IPIC_STATE_reg),
        .I1(IP2Bus_Error),
        .I2(sig_str_rst_reg),
        .I3(sig_Bus2IP_WrCE[10]),
        .I4(s_axi_wdata[12]),
        .I5(\sig_register_array[0][1]_i_3_n_0 ),
        .O(\sig_register_array[0][0]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair79" *) 
  LUT5 #(
    .INIT(32'h33FF33F1)) 
    \sig_register_array[0][0]_i_4 
       (.I0(\GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg ),
        .I1(eqOp__6),
        .I2(\GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ),
        .I3(Bus_RNW_reg),
        .I4(\GEN_BKEND_CE_REGISTERS[1].ce_out_i_reg ),
        .O(\sig_register_array[0][0]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair86" *) 
  LUT4 #(
    .INIT(16'hFFF9)) 
    \sig_register_array[0][0]_i_5 
       (.I0(\GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg ),
        .I1(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I2(Bus_RNW_reg),
        .I3(\GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg ),
        .O(\sig_register_array[0][0]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair77" *) 
  LUT5 #(
    .INIT(32'h00FF00FE)) 
    \sig_register_array[0][0]_i_6 
       (.I0(\GEN_BKEND_CE_REGISTERS[12].ce_out_i_reg ),
        .I1(\GEN_BKEND_CE_REGISTERS[11].ce_out_i_reg ),
        .I2(\GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg ),
        .I3(Bus_RNW_reg),
        .I4(\GEN_BKEND_CE_REGISTERS[8].ce_out_i_reg ),
        .O(\sig_register_array[0][0]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h00FF00FF00FF00F4)) 
    \sig_register_array[0][0]_i_7 
       (.I0(s_axi_wdata[12]),
        .I1(\GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg ),
        .I2(\GEN_BKEND_CE_REGISTERS[5].ce_out_i_reg ),
        .I3(Bus_RNW_reg),
        .I4(\GEN_BKEND_CE_REGISTERS[3].ce_out_i_reg ),
        .I5(\GEN_BKEND_CE_REGISTERS[4].ce_out_i_reg ),
        .O(\sig_register_array[0][0]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'h888CFFFF888C0000)) 
    \sig_register_array[0][10]_i_1 
       (.I0(\sig_register_array[0][3]_i_2_n_0 ),
        .I1(sig_txd_pe_event__1),
        .I2(sig_Bus2IP_WrCE[10]),
        .I3(s_axi_wdata[2]),
        .I4(\sig_register_array[0][10]_i_3_n_0 ),
        .I5(\sig_register_array_reg[0][10]_0 ),
        .O(\sig_register_array_reg[0][10] ));
  LUT6 #(
    .INIT(64'hCCCCCCCCDCDDDCDC)) 
    \sig_register_array[0][10]_i_3 
       (.I0(\sig_register_array[0][1]_i_3_n_0 ),
        .I1(sig_txd_pe_event__1),
        .I2(sig_str_rst_reg),
        .I3(sig_Bus2IP_WrCE[10]),
        .I4(s_axi_wdata[2]),
        .I5(IP2Bus_Error1_in),
        .O(\sig_register_array[0][10]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h888CFFFF888C0000)) 
    \sig_register_array[0][11]_i_1 
       (.I0(\sig_register_array[0][3]_i_2_n_0 ),
        .I1(sig_rxd_pf_event__1),
        .I2(sig_Bus2IP_WrCE[10]),
        .I3(s_axi_wdata[1]),
        .I4(\sig_register_array[0][11]_i_3_n_0 ),
        .I5(\sig_register_array_reg[0][11]_0 ),
        .O(\sig_register_array_reg[0][11] ));
  LUT6 #(
    .INIT(64'hCCCCCCCCDCDDDCDC)) 
    \sig_register_array[0][11]_i_3 
       (.I0(\sig_register_array[0][1]_i_3_n_0 ),
        .I1(sig_rxd_pf_event__1),
        .I2(sig_str_rst_reg),
        .I3(sig_Bus2IP_WrCE[10]),
        .I4(s_axi_wdata[1]),
        .I5(IP2Bus_Error1_in),
        .O(\sig_register_array[0][11]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h888CFFFF888C0000)) 
    \sig_register_array[0][12]_i_1 
       (.I0(\sig_register_array[0][3]_i_2_n_0 ),
        .I1(sig_rxd_pe_event__1),
        .I2(sig_Bus2IP_WrCE[10]),
        .I3(s_axi_wdata[0]),
        .I4(\sig_register_array[0][12]_i_3_n_0 ),
        .I5(\sig_register_array_reg[0][12]_0 ),
        .O(\sig_register_array_reg[0][12] ));
  LUT6 #(
    .INIT(64'hCCCCCCCCDCDDDCDC)) 
    \sig_register_array[0][12]_i_3 
       (.I0(\sig_register_array[0][1]_i_3_n_0 ),
        .I1(sig_rxd_pe_event__1),
        .I2(sig_str_rst_reg),
        .I3(sig_Bus2IP_WrCE[10]),
        .I4(s_axi_wdata[0]),
        .I5(IP2Bus_Error1_in),
        .O(\sig_register_array[0][12]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAAFFABEFAA00A820)) 
    \sig_register_array[0][1]_i_1 
       (.I0(\sig_register_array[0][1]_i_2_n_0 ),
        .I1(\sig_register_array[0][1]_i_3_n_0 ),
        .I2(\gaxi_full_sm.r_last_r_reg ),
        .I3(\gaxi_full_sm.r_last_r_reg_0 ),
        .I4(IP2Bus_Error1_in),
        .I5(\sig_register_array_reg[0][1]_1 ),
        .O(\sig_register_array_reg[0][1] ));
  (* SOFT_HLUTNM = "soft_lutpair72" *) 
  LUT5 #(
    .INIT(32'hFFFFAABA)) 
    \sig_register_array[0][1]_i_2 
       (.I0(\sig_register_array[0][0]_i_4_n_0 ),
        .I1(Bus_RNW_reg),
        .I2(\GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg ),
        .I3(s_axi_wdata[11]),
        .I4(\sig_register_array[0][1]_i_6_n_0 ),
        .O(\sig_register_array[0][1]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair78" *) 
  LUT5 #(
    .INIT(32'hFFFFFFF9)) 
    \sig_register_array[0][1]_i_3 
       (.I0(\GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg ),
        .I1(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I2(\sig_register_array[0][6]_i_3_n_0 ),
        .I3(Bus_RNW_reg),
        .I4(\GEN_BKEND_CE_REGISTERS[5].ce_out_i_reg ),
        .O(\sig_register_array[0][1]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \sig_register_array[0][1]_i_6 
       (.I0(sig_Bus2IP_WrCE[6]),
        .I1(sig_Bus2IP_WrCE__0),
        .I2(\sig_txd_wr_data[31]_i_7_n_0 ),
        .I3(\sig_register_array[0][0]_i_6_n_0 ),
        .I4(\sig_register_array[0][0]_i_5_n_0 ),
        .I5(IP2Bus_Error1_in),
        .O(\sig_register_array[0][1]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair92" *) 
  LUT3 #(
    .INIT(8'h8A)) 
    \sig_register_array[0][1]_i_8 
       (.I0(s_axi_wdata[11]),
        .I1(Bus_RNW_reg),
        .I2(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .O(\sig_register_array_reg[0][1]_0 ));
  LUT6 #(
    .INIT(64'hAAFFABEFAA00A820)) 
    \sig_register_array[0][2]_i_1 
       (.I0(\sig_register_array[0][2]_i_2_n_0 ),
        .I1(\sig_register_array[0][1]_i_3_n_0 ),
        .I2(\gaxi_full_sm.r_last_r_reg_1 ),
        .I3(sig_rx_channel_reset_reg_0),
        .I4(IP2Bus_Error1_in),
        .I5(\sig_register_array_reg[0][2]_1 ),
        .O(\sig_register_array_reg[0][2] ));
  LUT5 #(
    .INIT(32'hFFFFAABA)) 
    \sig_register_array[0][2]_i_2 
       (.I0(\sig_register_array[0][0]_i_4_n_0 ),
        .I1(Bus_RNW_reg),
        .I2(\GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg ),
        .I3(s_axi_wdata[10]),
        .I4(\sig_register_array[0][1]_i_6_n_0 ),
        .O(\sig_register_array[0][2]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair93" *) 
  LUT3 #(
    .INIT(8'h8A)) 
    \sig_register_array[0][2]_i_6 
       (.I0(s_axi_wdata[10]),
        .I1(Bus_RNW_reg),
        .I2(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .O(\sig_register_array_reg[0][2]_0 ));
  LUT6 #(
    .INIT(64'h888CFFFF888C0000)) 
    \sig_register_array[0][3]_i_1 
       (.I0(\sig_register_array[0][3]_i_2_n_0 ),
        .I1(tx_fifo_or),
        .I2(sig_Bus2IP_WrCE[10]),
        .I3(s_axi_wdata[9]),
        .I4(\sig_register_array[0][3]_i_5_n_0 ),
        .I5(\sig_register_array_reg[0][3]_0 ),
        .O(\sig_register_array_reg[0][3] ));
  LUT6 #(
    .INIT(64'hFFFFFFFF77777745)) 
    \sig_register_array[0][3]_i_2 
       (.I0(eqOp__6),
        .I1(Bus_RNW_reg),
        .I2(\GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg ),
        .I3(\GEN_BKEND_CE_REGISTERS[1].ce_out_i_reg ),
        .I4(\GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ),
        .I5(\sig_register_array[0][1]_i_6_n_0 ),
        .O(\sig_register_array[0][3]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair73" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \sig_register_array[0][3]_i_4 
       (.I0(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I1(Bus_RNW_reg),
        .O(sig_Bus2IP_WrCE[10]));
  LUT6 #(
    .INIT(64'hCCCCCCCCDCDDDCDC)) 
    \sig_register_array[0][3]_i_5 
       (.I0(\sig_register_array[0][1]_i_3_n_0 ),
        .I1(tx_fifo_or),
        .I2(sig_str_rst_reg),
        .I3(sig_Bus2IP_WrCE[10]),
        .I4(s_axi_wdata[9]),
        .I5(IP2Bus_Error1_in),
        .O(\sig_register_array[0][3]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hA0B0FFFFA0B00000)) 
    \sig_register_array[0][4]_i_1 
       (.I0(\sig_register_array[0][3]_i_2_n_0 ),
        .I1(sig_Bus2IP_WrCE[10]),
        .I2(axi_str_txd_tready),
        .I3(s_axi_wdata[8]),
        .I4(\sig_register_array[0][4]_i_2_n_0 ),
        .I5(\sig_register_array_reg[0][4]_0 ),
        .O(\sig_register_array_reg[0][4] ));
  LUT6 #(
    .INIT(64'hCCCCCCCCDCDDDCDC)) 
    \sig_register_array[0][4]_i_2 
       (.I0(\sig_register_array[0][1]_i_3_n_0 ),
        .I1(\goreg_bm.dout_i_reg[0] ),
        .I2(sig_str_rst_reg),
        .I3(sig_Bus2IP_WrCE[10]),
        .I4(s_axi_wdata[8]),
        .I5(IP2Bus_Error1_in),
        .O(\sig_register_array[0][4]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h888CFFFF888C0000)) 
    \sig_register_array[0][5]_i_1 
       (.I0(\sig_register_array[0][3]_i_2_n_0 ),
        .I1(p_13_in),
        .I2(sig_Bus2IP_WrCE[10]),
        .I3(s_axi_wdata[7]),
        .I4(\sig_register_array[0][5]_i_3_n_0 ),
        .I5(\sig_register_array_reg[0][5]_0 ),
        .O(\sig_register_array_reg[0][5] ));
  LUT6 #(
    .INIT(64'hCCCCCCCCDCDDDCDC)) 
    \sig_register_array[0][5]_i_3 
       (.I0(\sig_register_array[0][1]_i_3_n_0 ),
        .I1(p_13_in),
        .I2(sig_str_rst_reg),
        .I3(sig_Bus2IP_WrCE[10]),
        .I4(s_axi_wdata[7]),
        .I5(IP2Bus_Error1_in),
        .O(\sig_register_array[0][5]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFEFEFEFF02020200)) 
    \sig_register_array[0][6]_i_1 
       (.I0(sig_Bus2IP_WrCE__0),
        .I1(IP2Bus_Error1_in),
        .I2(\sig_register_array[0][6]_i_3_n_0 ),
        .I3(\sig_register_array[0][6]_i_4_n_0 ),
        .I4(\sig_register_array[0][6]_i_5_n_0 ),
        .I5(\sig_register_array_reg[0][6]_0 ),
        .O(\sig_register_array_reg[0][6] ));
  (* SOFT_HLUTNM = "soft_lutpair78" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \sig_register_array[0][6]_i_2 
       (.I0(\GEN_BKEND_CE_REGISTERS[5].ce_out_i_reg ),
        .I1(Bus_RNW_reg),
        .O(sig_Bus2IP_WrCE__0));
  LUT6 #(
    .INIT(64'hFFFFFFFFEEFFEEFE)) 
    \sig_register_array[0][6]_i_3 
       (.I0(\sig_register_array[1][0]_i_4_n_0 ),
        .I1(\sig_register_array[0][6]_i_6_n_0 ),
        .I2(\GEN_BKEND_CE_REGISTERS[6].ce_out_i_reg ),
        .I3(Bus_RNW_reg),
        .I4(\GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg ),
        .I5(\sig_txd_wr_data[31]_i_7_n_0 ),
        .O(\sig_register_array[0][6]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0000000026220400)) 
    \sig_register_array[0][6]_i_4 
       (.I0(sig_Bus2IP_WrCE[0]),
        .I1(sig_Bus2IP_WrCE__0),
        .I2(sig_tx_channel_reset_reg_0),
        .I3(\gtxd.sig_txd_packet_size_reg[30] ),
        .I4(s_axi_wdata[6]),
        .I5(sig_Bus2IP_WrCE[10]),
        .O(\sig_register_array[0][6]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair85" *) 
  LUT4 #(
    .INIT(16'hCD00)) 
    \sig_register_array[0][6]_i_5 
       (.I0(\GEN_BKEND_CE_REGISTERS[5].ce_out_i_reg ),
        .I1(Bus_RNW_reg),
        .I2(\GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg ),
        .I3(sig_str_rst_reg),
        .O(\sig_register_array[0][6]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair94" *) 
  LUT3 #(
    .INIT(8'h32)) 
    \sig_register_array[0][6]_i_6 
       (.I0(\GEN_BKEND_CE_REGISTERS[1].ce_out_i_reg ),
        .I1(Bus_RNW_reg),
        .I2(\GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ),
        .O(\sig_register_array[0][6]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair72" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \sig_register_array[0][6]_i_7 
       (.I0(\GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg ),
        .I1(Bus_RNW_reg),
        .O(sig_Bus2IP_WrCE[0]));
  LUT6 #(
    .INIT(64'h888FFFFF888F0000)) 
    \sig_register_array[0][7]_i_1 
       (.I0(sig_txd_reset),
        .I1(\sig_register_array[0][7]_i_2_n_0 ),
        .I2(\sig_register_array[0][7]_i_3_n_0 ),
        .I3(\sig_register_array[0][7]_i_4_n_0 ),
        .I4(\sig_register_array[0][7]_i_5_n_0 ),
        .I5(\sig_register_array_reg[0][7]_0 ),
        .O(\sig_register_array_reg[0][7] ));
  LUT6 #(
    .INIT(64'hA0A00000A0A20000)) 
    \sig_register_array[0][7]_i_10 
       (.I0(\sig_register_array[0][7]_i_11_n_0 ),
        .I1(\GEN_BKEND_CE_REGISTERS[3].ce_out_i_reg ),
        .I2(Bus_RNW_reg),
        .I3(\GEN_BKEND_CE_REGISTERS[4].ce_out_i_reg ),
        .I4(IPIC_STATE_reg),
        .I5(\GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg ),
        .O(\sig_register_array[0][7]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000005051)) 
    \sig_register_array[0][7]_i_11 
       (.I0(\sig_register_array[0][7]_i_7_n_0 ),
        .I1(\GEN_BKEND_CE_REGISTERS[6].ce_out_i_reg ),
        .I2(Bus_RNW_reg),
        .I3(\GEN_BKEND_CE_REGISTERS[5].ce_out_i_reg ),
        .I4(\sig_register_array[0][6]_i_6_n_0 ),
        .I5(\sig_register_array[0][7]_i_12_n_0 ),
        .O(\sig_register_array[0][7]_i_11_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair82" *) 
  LUT3 #(
    .INIT(8'h32)) 
    \sig_register_array[0][7]_i_12 
       (.I0(\GEN_BKEND_CE_REGISTERS[11].ce_out_i_reg ),
        .I1(Bus_RNW_reg),
        .I2(\GEN_BKEND_CE_REGISTERS[12].ce_out_i_reg ),
        .O(\sig_register_array[0][7]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFEFFFF)) 
    \sig_register_array[0][7]_i_2 
       (.I0(IPIC_STATE),
        .I1(\sig_register_array[0][6]_i_6_n_0 ),
        .I2(\sig_register_array[0][7]_i_6_n_0 ),
        .I3(\sig_register_array[0][7]_i_7_n_0 ),
        .I4(\sig_register_array[0][7]_i_8_n_0 ),
        .I5(\sig_register_array[0][7]_i_9_n_0 ),
        .O(\sig_register_array[0][7]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hFF00FFDF)) 
    \sig_register_array[0][7]_i_3 
       (.I0(sig_txd_reset),
        .I1(s_axi_wdata[5]),
        .I2(\GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg ),
        .I3(Bus_RNW_reg),
        .I4(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .O(\sig_register_array[0][7]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAAFBAAAAFFFFFFFF)) 
    \sig_register_array[0][7]_i_4 
       (.I0(IPIC_STATE),
        .I1(eqOp__6),
        .I2(\GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg ),
        .I3(Bus_RNW_reg),
        .I4(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I5(\sig_register_array[0][7]_i_10_n_0 ),
        .O(\sig_register_array[0][7]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hCCCCCCCCDCDDDCDC)) 
    \sig_register_array[0][7]_i_5 
       (.I0(\sig_register_array[0][1]_i_3_n_0 ),
        .I1(sig_txd_reset),
        .I2(sig_str_rst_reg),
        .I3(sig_Bus2IP_WrCE[10]),
        .I4(s_axi_wdata[5]),
        .I5(IP2Bus_Error1_in),
        .O(\sig_register_array[0][7]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair91" *) 
  LUT3 #(
    .INIT(8'h32)) 
    \sig_register_array[0][7]_i_6 
       (.I0(\GEN_BKEND_CE_REGISTERS[6].ce_out_i_reg ),
        .I1(Bus_RNW_reg),
        .I2(\GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg ),
        .O(\sig_register_array[0][7]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair75" *) 
  LUT3 #(
    .INIT(8'h32)) 
    \sig_register_array[0][7]_i_7 
       (.I0(\GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg ),
        .I1(Bus_RNW_reg),
        .I2(\GEN_BKEND_CE_REGISTERS[8].ce_out_i_reg ),
        .O(\sig_register_array[0][7]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000008)) 
    \sig_register_array[0][7]_i_8 
       (.I0(\GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg ),
        .I1(IPIC_STATE_reg),
        .I2(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I3(\GEN_BKEND_CE_REGISTERS[12].ce_out_i_reg ),
        .I4(Bus_RNW_reg),
        .I5(\GEN_BKEND_CE_REGISTERS[11].ce_out_i_reg ),
        .O(\sig_register_array[0][7]_i_8_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair89" *) 
  LUT4 #(
    .INIT(16'h0F0E)) 
    \sig_register_array[0][7]_i_9 
       (.I0(\GEN_BKEND_CE_REGISTERS[4].ce_out_i_reg ),
        .I1(\GEN_BKEND_CE_REGISTERS[3].ce_out_i_reg ),
        .I2(Bus_RNW_reg),
        .I3(\GEN_BKEND_CE_REGISTERS[5].ce_out_i_reg ),
        .O(\sig_register_array[0][7]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'h888FFFFF888F0000)) 
    \sig_register_array[0][8]_i_1 
       (.I0(sig_rxd_reset),
        .I1(\sig_register_array[0][7]_i_2_n_0 ),
        .I2(\sig_register_array[0][8]_i_2_n_0 ),
        .I3(\sig_register_array[0][7]_i_4_n_0 ),
        .I4(\sig_register_array[0][8]_i_3_n_0 ),
        .I5(\sig_register_array_reg[0][8]_0 ),
        .O(\sig_register_array_reg[0][8] ));
  (* SOFT_HLUTNM = "soft_lutpair73" *) 
  LUT5 #(
    .INIT(32'hFF00FFDF)) 
    \sig_register_array[0][8]_i_2 
       (.I0(sig_rxd_reset),
        .I1(s_axi_wdata[4]),
        .I2(\GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg ),
        .I3(Bus_RNW_reg),
        .I4(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .O(\sig_register_array[0][8]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hCCCCCCCCDCDDDCDC)) 
    \sig_register_array[0][8]_i_3 
       (.I0(\sig_register_array[0][1]_i_3_n_0 ),
        .I1(sig_rxd_reset),
        .I2(sig_str_rst_reg),
        .I3(sig_Bus2IP_WrCE[10]),
        .I4(s_axi_wdata[4]),
        .I5(IP2Bus_Error1_in),
        .O(\sig_register_array[0][8]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h888CFFFF888C0000)) 
    \sig_register_array[0][9]_i_1 
       (.I0(\sig_register_array[0][3]_i_2_n_0 ),
        .I1(sig_txd_pf_event__1),
        .I2(sig_Bus2IP_WrCE[10]),
        .I3(s_axi_wdata[3]),
        .I4(\sig_register_array[0][9]_i_3_n_0 ),
        .I5(\sig_register_array_reg[0][9]_0 ),
        .O(\sig_register_array_reg[0][9] ));
  LUT6 #(
    .INIT(64'hCCCCCCCCDCDDDCDC)) 
    \sig_register_array[0][9]_i_3 
       (.I0(\sig_register_array[0][1]_i_3_n_0 ),
        .I1(sig_txd_pf_event__1),
        .I2(sig_str_rst_reg),
        .I3(sig_Bus2IP_WrCE[10]),
        .I4(s_axi_wdata[3]),
        .I5(IP2Bus_Error1_in),
        .O(\sig_register_array[0][9]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0000151000000000)) 
    \sig_register_array[1][0]_i_1 
       (.I0(IP2Bus_Error1_in),
        .I1(sig_Bus2IP_WrCE[10]),
        .I2(sig_Bus2IP_WrCE[1]),
        .I3(sig_str_rst_reg),
        .I4(\sig_register_array[1][0]_i_4_n_0 ),
        .I5(\sig_register_array[1][0]_i_5_n_0 ),
        .O(\sig_register_array_reg[1][0] ));
  (* SOFT_HLUTNM = "soft_lutpair96" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \sig_register_array[1][0]_i_2 
       (.I0(Bus_RNW_reg),
        .I1(\GEN_BKEND_CE_REGISTERS[1].ce_out_i_reg ),
        .I2(s_axi_wdata[12]),
        .O(\sig_register_array_reg[1][0]_0 [12]));
  (* SOFT_HLUTNM = "soft_lutpair89" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \sig_register_array[1][0]_i_3 
       (.I0(\GEN_BKEND_CE_REGISTERS[1].ce_out_i_reg ),
        .I1(Bus_RNW_reg),
        .O(sig_Bus2IP_WrCE[1]));
  (* SOFT_HLUTNM = "soft_lutpair75" *) 
  LUT5 #(
    .INIT(32'h00FF00FE)) 
    \sig_register_array[1][0]_i_4 
       (.I0(\GEN_BKEND_CE_REGISTERS[8].ce_out_i_reg ),
        .I1(\GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg ),
        .I2(\GEN_BKEND_CE_REGISTERS[12].ce_out_i_reg ),
        .I3(Bus_RNW_reg),
        .I4(\GEN_BKEND_CE_REGISTERS[11].ce_out_i_reg ),
        .O(\sig_register_array[1][0]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h00000000CCCCCCCD)) 
    \sig_register_array[1][0]_i_5 
       (.I0(\GEN_BKEND_CE_REGISTERS[6].ce_out_i_reg ),
        .I1(Bus_RNW_reg),
        .I2(\GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg ),
        .I3(\GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ),
        .I4(\GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg ),
        .I5(\sig_register_array[0][7]_i_9_n_0 ),
        .O(\sig_register_array[1][0]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair96" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \sig_register_array[1][10]_i_1 
       (.I0(Bus_RNW_reg),
        .I1(\GEN_BKEND_CE_REGISTERS[1].ce_out_i_reg ),
        .I2(s_axi_wdata[2]),
        .O(\sig_register_array_reg[1][0]_0 [2]));
  (* SOFT_HLUTNM = "soft_lutpair95" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \sig_register_array[1][11]_i_1 
       (.I0(Bus_RNW_reg),
        .I1(\GEN_BKEND_CE_REGISTERS[1].ce_out_i_reg ),
        .I2(s_axi_wdata[1]),
        .O(\sig_register_array_reg[1][0]_0 [1]));
  (* SOFT_HLUTNM = "soft_lutpair95" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \sig_register_array[1][12]_i_1 
       (.I0(Bus_RNW_reg),
        .I1(\GEN_BKEND_CE_REGISTERS[1].ce_out_i_reg ),
        .I2(s_axi_wdata[0]),
        .O(\sig_register_array_reg[1][0]_0 [0]));
  (* SOFT_HLUTNM = "soft_lutpair92" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \sig_register_array[1][1]_i_1 
       (.I0(Bus_RNW_reg),
        .I1(\GEN_BKEND_CE_REGISTERS[1].ce_out_i_reg ),
        .I2(s_axi_wdata[11]),
        .O(\sig_register_array_reg[1][0]_0 [11]));
  (* SOFT_HLUTNM = "soft_lutpair93" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \sig_register_array[1][2]_i_1 
       (.I0(Bus_RNW_reg),
        .I1(\GEN_BKEND_CE_REGISTERS[1].ce_out_i_reg ),
        .I2(s_axi_wdata[10]),
        .O(\sig_register_array_reg[1][0]_0 [10]));
  (* SOFT_HLUTNM = "soft_lutpair98" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \sig_register_array[1][3]_i_1 
       (.I0(Bus_RNW_reg),
        .I1(\GEN_BKEND_CE_REGISTERS[1].ce_out_i_reg ),
        .I2(s_axi_wdata[9]),
        .O(\sig_register_array_reg[1][0]_0 [9]));
  (* SOFT_HLUTNM = "soft_lutpair99" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \sig_register_array[1][4]_i_1 
       (.I0(Bus_RNW_reg),
        .I1(\GEN_BKEND_CE_REGISTERS[1].ce_out_i_reg ),
        .I2(s_axi_wdata[8]),
        .O(\sig_register_array_reg[1][0]_0 [8]));
  (* SOFT_HLUTNM = "soft_lutpair100" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \sig_register_array[1][5]_i_1 
       (.I0(Bus_RNW_reg),
        .I1(\GEN_BKEND_CE_REGISTERS[1].ce_out_i_reg ),
        .I2(s_axi_wdata[7]),
        .O(\sig_register_array_reg[1][0]_0 [7]));
  (* SOFT_HLUTNM = "soft_lutpair101" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \sig_register_array[1][6]_i_1 
       (.I0(Bus_RNW_reg),
        .I1(\GEN_BKEND_CE_REGISTERS[1].ce_out_i_reg ),
        .I2(s_axi_wdata[6]),
        .O(\sig_register_array_reg[1][0]_0 [6]));
  (* SOFT_HLUTNM = "soft_lutpair100" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \sig_register_array[1][7]_i_1 
       (.I0(Bus_RNW_reg),
        .I1(\GEN_BKEND_CE_REGISTERS[1].ce_out_i_reg ),
        .I2(s_axi_wdata[5]),
        .O(\sig_register_array_reg[1][0]_0 [5]));
  (* SOFT_HLUTNM = "soft_lutpair99" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \sig_register_array[1][8]_i_1 
       (.I0(Bus_RNW_reg),
        .I1(\GEN_BKEND_CE_REGISTERS[1].ce_out_i_reg ),
        .I2(s_axi_wdata[4]),
        .O(\sig_register_array_reg[1][0]_0 [4]));
  (* SOFT_HLUTNM = "soft_lutpair98" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \sig_register_array[1][9]_i_1 
       (.I0(Bus_RNW_reg),
        .I1(\GEN_BKEND_CE_REGISTERS[1].ce_out_i_reg ),
        .I2(s_axi_wdata[3]),
        .O(\sig_register_array_reg[1][0]_0 [3]));
  LUT6 #(
    .INIT(64'h080808FF08080800)) 
    sig_rx_channel_reset_i_1
       (.I0(sig_rx_channel_reset_i_2_n_0),
        .I1(sig_rx_channel_reset_i_3_n_0),
        .I2(\sig_txd_wr_data[31]_i_3_n_0 ),
        .I3(sig_rx_channel_reset_i_4_n_0),
        .I4(sig_rxd_reset),
        .I5(sig_rx_channel_reset_reg_1),
        .O(sig_rx_channel_reset_reg));
  LUT6 #(
    .INIT(64'h00000000CD000000)) 
    sig_rx_channel_reset_i_2
       (.I0(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .I1(Bus_RNW_reg),
        .I2(\GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg ),
        .I3(eqOp__6),
        .I4(IPIC_STATE_reg),
        .I5(IPIC_STATE),
        .O(sig_rx_channel_reset_i_2_n_0));
  LUT6 #(
    .INIT(64'h0000000000000004)) 
    sig_rx_channel_reset_i_3
       (.I0(\GEN_BKEND_CE_REGISTERS[5].ce_out_i_reg ),
        .I1(\GEN_BKEND_CE_REGISTERS[6].ce_out_i_reg ),
        .I2(\GEN_BKEND_CE_REGISTERS[12].ce_out_i_reg ),
        .I3(\GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg ),
        .I4(Bus_RNW_reg),
        .I5(\GEN_BKEND_CE_REGISTERS[8].ce_out_i_reg ),
        .O(sig_rx_channel_reset_i_3_n_0));
  LUT6 #(
    .INIT(64'h0000000400000000)) 
    sig_rx_channel_reset_i_4
       (.I0(\sig_register_array[0][0]_i_6_n_0 ),
        .I1(sig_rx_channel_reset_i_2_n_0),
        .I2(sig_rx_channel_reset_i_5_n_0),
        .I3(sig_Bus2IP_WrCE[0]),
        .I4(sig_Bus2IP_WrCE__0),
        .I5(sig_Bus2IP_WrCE[6]),
        .O(sig_rx_channel_reset_i_4_n_0));
  (* SOFT_HLUTNM = "soft_lutpair80" *) 
  LUT5 #(
    .INIT(32'h00FF00FE)) 
    sig_rx_channel_reset_i_5
       (.I0(\GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ),
        .I1(\GEN_BKEND_CE_REGISTERS[1].ce_out_i_reg ),
        .I2(\GEN_BKEND_CE_REGISTERS[4].ce_out_i_reg ),
        .I3(Bus_RNW_reg),
        .I4(\GEN_BKEND_CE_REGISTERS[3].ce_out_i_reg ),
        .O(sig_rx_channel_reset_i_5_n_0));
  (* SOFT_HLUTNM = "soft_lutpair71" *) 
  LUT2 #(
    .INIT(4'h2)) 
    sig_rx_channel_reset_i_6
       (.I0(\GEN_BKEND_CE_REGISTERS[6].ce_out_i_reg ),
        .I1(Bus_RNW_reg),
        .O(sig_Bus2IP_WrCE[6]));
  LUT6 #(
    .INIT(64'h0000000000000004)) 
    sig_str_rst_i_1
       (.I0(\sig_txd_wr_data[31]_i_3_n_0 ),
        .I1(sig_str_rst_reg),
        .I2(sig_Bus2IP_WrCE[9]),
        .I3(sig_Bus2IP_WrCE[12]),
        .I4(sig_str_rst_i_5_n_0),
        .I5(IP2Bus_Error1_in),
        .O(sig_str_rst_reg_0));
  (* SOFT_HLUTNM = "soft_lutpair86" *) 
  LUT3 #(
    .INIT(8'h20)) 
    sig_str_rst_i_2
       (.I0(eqOp__6),
        .I1(Bus_RNW_reg),
        .I2(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .O(sig_str_rst_reg));
  (* SOFT_HLUTNM = "soft_lutpair97" *) 
  LUT2 #(
    .INIT(4'h2)) 
    sig_str_rst_i_3
       (.I0(\GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg ),
        .I1(Bus_RNW_reg),
        .O(sig_Bus2IP_WrCE[9]));
  (* SOFT_HLUTNM = "soft_lutpair101" *) 
  LUT2 #(
    .INIT(4'h2)) 
    sig_str_rst_i_4
       (.I0(\GEN_BKEND_CE_REGISTERS[12].ce_out_i_reg ),
        .I1(Bus_RNW_reg),
        .O(sig_Bus2IP_WrCE[12]));
  (* SOFT_HLUTNM = "soft_lutpair76" *) 
  LUT5 #(
    .INIT(32'h00FF00FE)) 
    sig_str_rst_i_5
       (.I0(\GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg ),
        .I1(\GEN_BKEND_CE_REGISTERS[8].ce_out_i_reg ),
        .I2(\GEN_BKEND_CE_REGISTERS[5].ce_out_i_reg ),
        .I3(Bus_RNW_reg),
        .I4(\GEN_BKEND_CE_REGISTERS[6].ce_out_i_reg ),
        .O(sig_str_rst_i_5_n_0));
  LUT6 #(
    .INIT(64'h8888888088888888)) 
    sig_tx_channel_reset_i_1
       (.I0(sig_tx_channel_reset_i_2_n_0),
        .I1(sig_rx_channel_reset_i_2_n_0),
        .I2(sig_tx_channel_reset_i_3_n_0),
        .I3(sig_tx_channel_reset_reg_0),
        .I4(sig_str_rst_reg_1),
        .I5(s_axi_aresetn),
        .O(sig_tx_channel_reset_reg));
  LUT6 #(
    .INIT(64'h1100110100000000)) 
    sig_tx_channel_reset_i_2
       (.I0(\sig_txd_wr_data[31]_i_7_n_0 ),
        .I1(sig_str_rst_i_5_n_0),
        .I2(\GEN_BKEND_CE_REGISTERS[11].ce_out_i_reg ),
        .I3(Bus_RNW_reg),
        .I4(\GEN_BKEND_CE_REGISTERS[12].ce_out_i_reg ),
        .I5(sig_tx_channel_reset_i_4_n_0),
        .O(sig_tx_channel_reset_i_2_n_0));
  LUT6 #(
    .INIT(64'h000000CD00000000)) 
    sig_tx_channel_reset_i_3
       (.I0(\GEN_BKEND_CE_REGISTERS[11].ce_out_i_reg ),
        .I1(Bus_RNW_reg),
        .I2(\GEN_BKEND_CE_REGISTERS[12].ce_out_i_reg ),
        .I3(sig_str_rst_i_5_n_0),
        .I4(\sig_txd_wr_data[31]_i_7_n_0 ),
        .I5(sig_tx_channel_reset_i_4_n_0),
        .O(sig_tx_channel_reset_i_3_n_0));
  (* SOFT_HLUTNM = "soft_lutpair79" *) 
  LUT4 #(
    .INIT(16'h0004)) 
    sig_tx_channel_reset_i_4
       (.I0(\GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg ),
        .I1(\GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ),
        .I2(Bus_RNW_reg),
        .I3(\GEN_BKEND_CE_REGISTERS[1].ce_out_i_reg ),
        .O(sig_tx_channel_reset_i_4_n_0));
  (* SOFT_HLUTNM = "soft_lutpair74" *) 
  LUT5 #(
    .INIT(32'h00100000)) 
    sig_txd_sb_wr_en_i_1
       (.I0(\sig_txd_wr_data[31]_i_2_n_0 ),
        .I1(\sig_txd_wr_data[31]_i_3_n_0 ),
        .I2(s_axi_aresetn),
        .I3(IPIC_STATE),
        .I4(IPIC_STATE_reg),
        .O(sig_txd_sb_wr_en_reg));
  (* SOFT_HLUTNM = "soft_lutpair74" *) 
  LUT4 #(
    .INIT(16'h0004)) 
    \sig_txd_wr_data[31]_i_1 
       (.I0(IPIC_STATE),
        .I1(IPIC_STATE_reg),
        .I2(\sig_txd_wr_data[31]_i_2_n_0 ),
        .I3(\sig_txd_wr_data[31]_i_3_n_0 ),
        .O(\sig_txd_wr_data_reg[31] ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFBFFFF)) 
    \sig_txd_wr_data[31]_i_2 
       (.I0(sig_tx_channel_reset_reg_0),
        .I1(\sig_txd_wr_data[31]_i_4_n_0 ),
        .I2(\gtxd.sig_txd_packet_size_reg[30] ),
        .I3(Bus_RNW_reg),
        .I4(\GEN_BKEND_CE_REGISTERS[5].ce_out_i_reg ),
        .I5(\sig_txd_wr_data[31]_i_6_n_0 ),
        .O(\sig_txd_wr_data[31]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hF0F0FFFFF0F0FFFE)) 
    \sig_txd_wr_data[31]_i_3 
       (.I0(\GEN_BKEND_CE_REGISTERS[11].ce_out_i_reg ),
        .I1(\GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg ),
        .I2(\sig_txd_wr_data[31]_i_7_n_0 ),
        .I3(\GEN_BKEND_CE_REGISTERS[1].ce_out_i_reg ),
        .I4(Bus_RNW_reg),
        .I5(\GEN_BKEND_CE_REGISTERS[2].ce_out_i_reg ),
        .O(\sig_txd_wr_data[31]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair91" *) 
  LUT3 #(
    .INIT(8'hCD)) 
    \sig_txd_wr_data[31]_i_4 
       (.I0(\GEN_BKEND_CE_REGISTERS[8].ce_out_i_reg ),
        .I1(Bus_RNW_reg),
        .I2(\GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg ),
        .O(\sig_txd_wr_data[31]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair71" *) 
  LUT5 #(
    .INIT(32'h00FF00FE)) 
    \sig_txd_wr_data[31]_i_6 
       (.I0(\GEN_BKEND_CE_REGISTERS[6].ce_out_i_reg ),
        .I1(\GEN_BKEND_CE_REGISTERS[12].ce_out_i_reg ),
        .I2(\GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg ),
        .I3(Bus_RNW_reg),
        .I4(\GEN_BKEND_CE_REGISTERS[10].ce_out_i_reg ),
        .O(\sig_txd_wr_data[31]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair83" *) 
  LUT3 #(
    .INIT(8'h32)) 
    \sig_txd_wr_data[31]_i_7 
       (.I0(\GEN_BKEND_CE_REGISTERS[3].ce_out_i_reg ),
        .I1(Bus_RNW_reg),
        .I2(\GEN_BKEND_CE_REGISTERS[4].ce_out_i_reg ),
        .O(\sig_txd_wr_data[31]_i_7_n_0 ));
endmodule

(* C_AXI4_BASEADDR = "-2147479552" *) (* C_AXI4_HIGHADDR = "-2147471361" *) (* C_AXIS_TDEST_WIDTH = "4" *) 
(* C_AXIS_TID_WIDTH = "4" *) (* C_AXIS_TUSER_WIDTH = "4" *) (* C_BASEADDR = "-2147483648" *) 
(* C_DATA_INTERFACE_TYPE = "1" *) (* C_FAMILY = "zynq" *) (* C_HAS_AXIS_TDEST = "0" *) 
(* C_HAS_AXIS_TID = "0" *) (* C_HAS_AXIS_TKEEP = "0" *) (* C_HAS_AXIS_TSTRB = "0" *) 
(* C_HAS_AXIS_TUSER = "0" *) (* C_HIGHADDR = "-2147479553" *) (* C_RX_FIFO_DEPTH = "512" *) 
(* C_RX_FIFO_PE_THRESHOLD = "2" *) (* C_RX_FIFO_PF_THRESHOLD = "507" *) (* C_S_AXI4_DATA_WIDTH = "32" *) 
(* C_S_AXI_ADDR_WIDTH = "32" *) (* C_S_AXI_DATA_WIDTH = "32" *) (* C_S_AXI_ID_WIDTH = "4" *) 
(* C_TX_FIFO_DEPTH = "512" *) (* C_TX_FIFO_PE_THRESHOLD = "2" *) (* C_TX_FIFO_PF_THRESHOLD = "507" *) 
(* C_USE_RX_CUT_THROUGH = "0" *) (* C_USE_RX_DATA = "1" *) (* C_USE_TX_CTRL = "1" *) 
(* C_USE_TX_CUT_THROUGH = "0" *) (* C_USE_TX_DATA = "1" *) (* ORIG_REF_NAME = "axi_fifo_mm_s" *) 
module axi_fifo_mm_s_0_axi_fifo_mm_s
   (interrupt,
    s_axi_aclk,
    s_axi_aresetn,
    s_axi_awaddr,
    s_axi_awvalid,
    s_axi_awready,
    s_axi_wdata,
    s_axi_wstrb,
    s_axi_wvalid,
    s_axi_wready,
    s_axi_bresp,
    s_axi_bvalid,
    s_axi_bready,
    s_axi_araddr,
    s_axi_arvalid,
    s_axi_arready,
    s_axi_rdata,
    s_axi_rresp,
    s_axi_rvalid,
    s_axi_rready,
    s_axi4_awid,
    s_axi4_awaddr,
    s_axi4_awlen,
    s_axi4_awsize,
    s_axi4_awburst,
    s_axi4_awlock,
    s_axi4_awcache,
    s_axi4_awprot,
    s_axi4_awvalid,
    s_axi4_awready,
    s_axi4_wdata,
    s_axi4_wstrb,
    s_axi4_wlast,
    s_axi4_wvalid,
    s_axi4_wready,
    s_axi4_bid,
    s_axi4_bresp,
    s_axi4_bvalid,
    s_axi4_bready,
    s_axi4_arid,
    s_axi4_araddr,
    s_axi4_arlen,
    s_axi4_arsize,
    s_axi4_arburst,
    s_axi4_arlock,
    s_axi4_arcache,
    s_axi4_arprot,
    s_axi4_arvalid,
    s_axi4_arready,
    s_axi4_rid,
    s_axi4_rdata,
    s_axi4_rresp,
    s_axi4_rlast,
    s_axi4_rvalid,
    s_axi4_rready,
    mm2s_prmry_reset_out_n,
    axi_str_txd_tvalid,
    axi_str_txd_tready,
    axi_str_txd_tlast,
    axi_str_txd_tkeep,
    axi_str_txd_tdata,
    axi_str_txd_tstrb,
    axi_str_txd_tdest,
    axi_str_txd_tid,
    axi_str_txd_tuser,
    mm2s_cntrl_reset_out_n,
    axi_str_txc_tvalid,
    axi_str_txc_tready,
    axi_str_txc_tlast,
    axi_str_txc_tkeep,
    axi_str_txc_tdata,
    axi_str_txc_tstrb,
    axi_str_txc_tdest,
    axi_str_txc_tid,
    axi_str_txc_tuser,
    s2mm_prmry_reset_out_n,
    axi_str_rxd_tvalid,
    axi_str_rxd_tready,
    axi_str_rxd_tlast,
    axi_str_rxd_tkeep,
    axi_str_rxd_tdata,
    axi_str_rxd_tstrb,
    axi_str_rxd_tdest,
    axi_str_rxd_tid,
    axi_str_rxd_tuser);
  output interrupt;
  input s_axi_aclk;
  input s_axi_aresetn;
  input [31:0]s_axi_awaddr;
  input s_axi_awvalid;
  output s_axi_awready;
  input [31:0]s_axi_wdata;
  input [3:0]s_axi_wstrb;
  input s_axi_wvalid;
  output s_axi_wready;
  output [1:0]s_axi_bresp;
  output s_axi_bvalid;
  input s_axi_bready;
  input [31:0]s_axi_araddr;
  input s_axi_arvalid;
  output s_axi_arready;
  output [31:0]s_axi_rdata;
  output [1:0]s_axi_rresp;
  output s_axi_rvalid;
  input s_axi_rready;
  input [3:0]s_axi4_awid;
  input [31:0]s_axi4_awaddr;
  input [7:0]s_axi4_awlen;
  input [2:0]s_axi4_awsize;
  input [1:0]s_axi4_awburst;
  input s_axi4_awlock;
  input [3:0]s_axi4_awcache;
  input [2:0]s_axi4_awprot;
  input s_axi4_awvalid;
  output s_axi4_awready;
  input [31:0]s_axi4_wdata;
  input [3:0]s_axi4_wstrb;
  input s_axi4_wlast;
  input s_axi4_wvalid;
  output s_axi4_wready;
  output [3:0]s_axi4_bid;
  output [1:0]s_axi4_bresp;
  output s_axi4_bvalid;
  input s_axi4_bready;
  input [3:0]s_axi4_arid;
  input [31:0]s_axi4_araddr;
  input [7:0]s_axi4_arlen;
  input [2:0]s_axi4_arsize;
  input [1:0]s_axi4_arburst;
  input s_axi4_arlock;
  input [3:0]s_axi4_arcache;
  input [2:0]s_axi4_arprot;
  input s_axi4_arvalid;
  output s_axi4_arready;
  output [3:0]s_axi4_rid;
  output [31:0]s_axi4_rdata;
  output [1:0]s_axi4_rresp;
  output s_axi4_rlast;
  output s_axi4_rvalid;
  input s_axi4_rready;
  output mm2s_prmry_reset_out_n;
  output axi_str_txd_tvalid;
  input axi_str_txd_tready;
  output axi_str_txd_tlast;
  output [3:0]axi_str_txd_tkeep;
  output [31:0]axi_str_txd_tdata;
  output [3:0]axi_str_txd_tstrb;
  output [3:0]axi_str_txd_tdest;
  output [3:0]axi_str_txd_tid;
  output [3:0]axi_str_txd_tuser;
  output mm2s_cntrl_reset_out_n;
  output axi_str_txc_tvalid;
  input axi_str_txc_tready;
  output axi_str_txc_tlast;
  output [3:0]axi_str_txc_tkeep;
  output [31:0]axi_str_txc_tdata;
  output [3:0]axi_str_txc_tstrb;
  output [3:0]axi_str_txc_tdest;
  output [3:0]axi_str_txc_tid;
  output [3:0]axi_str_txc_tuser;
  output s2mm_prmry_reset_out_n;
  input axi_str_rxd_tvalid;
  output axi_str_rxd_tready;
  input axi_str_rxd_tlast;
  input [3:0]axi_str_rxd_tkeep;
  input [31:0]axi_str_rxd_tdata;
  input [3:0]axi_str_rxd_tstrb;
  input [3:0]axi_str_rxd_tdest;
  input [3:0]axi_str_rxd_tid;
  input [3:0]axi_str_rxd_tuser;

  wire \<const0> ;
  wire \<const1> ;
  wire COMP_IPIC2AXI_S_n_0;
  wire COMP_IPIC2AXI_S_n_10;
  wire COMP_IPIC2AXI_S_n_104;
  wire COMP_IPIC2AXI_S_n_105;
  wire COMP_IPIC2AXI_S_n_106;
  wire COMP_IPIC2AXI_S_n_11;
  wire COMP_IPIC2AXI_S_n_12;
  wire COMP_IPIC2AXI_S_n_13;
  wire COMP_IPIC2AXI_S_n_139;
  wire COMP_IPIC2AXI_S_n_14;
  wire COMP_IPIC2AXI_S_n_140;
  wire COMP_IPIC2AXI_S_n_141;
  wire COMP_IPIC2AXI_S_n_142;
  wire COMP_IPIC2AXI_S_n_143;
  wire COMP_IPIC2AXI_S_n_144;
  wire COMP_IPIC2AXI_S_n_145;
  wire COMP_IPIC2AXI_S_n_146;
  wire COMP_IPIC2AXI_S_n_147;
  wire COMP_IPIC2AXI_S_n_148;
  wire COMP_IPIC2AXI_S_n_149;
  wire COMP_IPIC2AXI_S_n_15;
  wire COMP_IPIC2AXI_S_n_150;
  wire COMP_IPIC2AXI_S_n_151;
  wire COMP_IPIC2AXI_S_n_152;
  wire COMP_IPIC2AXI_S_n_153;
  wire COMP_IPIC2AXI_S_n_154;
  wire COMP_IPIC2AXI_S_n_155;
  wire COMP_IPIC2AXI_S_n_156;
  wire COMP_IPIC2AXI_S_n_157;
  wire COMP_IPIC2AXI_S_n_158;
  wire COMP_IPIC2AXI_S_n_159;
  wire COMP_IPIC2AXI_S_n_16;
  wire COMP_IPIC2AXI_S_n_160;
  wire COMP_IPIC2AXI_S_n_17;
  wire COMP_IPIC2AXI_S_n_18;
  wire COMP_IPIC2AXI_S_n_19;
  wire COMP_IPIC2AXI_S_n_33;
  wire COMP_IPIC2AXI_S_n_34;
  wire COMP_IPIC2AXI_S_n_5;
  wire COMP_IPIC2AXI_S_n_7;
  wire COMP_IPIC2AXI_S_n_73;
  wire COMP_IPIC2AXI_S_n_77;
  wire COMP_IPIC2AXI_S_n_78;
  wire COMP_IPIC2AXI_S_n_8;
  wire COMP_IPIC2AXI_S_n_84;
  wire COMP_IPIC2AXI_S_n_85;
  wire COMP_IPIC2AXI_S_n_87;
  wire COMP_IPIC2AXI_S_n_88;
  wire COMP_IPIC2AXI_S_n_89;
  wire COMP_IPIC2AXI_S_n_9;
  wire COMP_IPIC2AXI_S_n_90;
  wire COMP_IPIC2AXI_S_n_91;
  wire COMP_IPIC2AXI_S_n_92;
  wire COMP_IPIC2AXI_S_n_93;
  wire COMP_IPIC2AXI_S_n_95;
  wire COMP_IPIC2AXI_S_n_96;
  wire COMP_IPIC2AXI_S_n_97;
  wire COMP_IPIF_n_10;
  wire COMP_IPIF_n_11;
  wire COMP_IPIF_n_12;
  wire COMP_IPIF_n_13;
  wire COMP_IPIF_n_14;
  wire COMP_IPIF_n_15;
  wire COMP_IPIF_n_16;
  wire COMP_IPIF_n_17;
  wire COMP_IPIF_n_18;
  wire COMP_IPIF_n_19;
  wire COMP_IPIF_n_20;
  wire COMP_IPIF_n_21;
  wire COMP_IPIF_n_22;
  wire COMP_IPIF_n_24;
  wire COMP_IPIF_n_25;
  wire COMP_IPIF_n_5;
  wire COMP_IPIF_n_6;
  wire COMP_IPIF_n_61;
  wire COMP_IPIF_n_62;
  wire COMP_IPIF_n_63;
  wire COMP_IPIF_n_64;
  wire COMP_IPIF_n_65;
  wire COMP_IPIF_n_66;
  wire COMP_IPIF_n_67;
  wire COMP_IPIF_n_68;
  wire COMP_IPIF_n_69;
  wire COMP_IPIF_n_7;
  wire COMP_IPIF_n_70;
  wire COMP_IPIF_n_8;
  wire COMP_IPIF_n_9;
  wire IP2Bus_Error;
  wire IP2Bus_Error1_in;
  wire IPIC_STATE;
  wire \I_SLAVE_ATTACHMENT/I_DECODER/cs_ce_clr ;
  wire axi4_fifo_rd_en_i;
  wire axi4_fifo_readyn;
  wire [31:0]axi_str_rxd_tdata;
  wire axi_str_rxd_tlast;
  wire axi_str_rxd_tready;
  wire axi_str_rxd_tvalid;
  wire axi_str_txc_tlast;
  wire axi_str_txc_tready;
  wire axi_str_txc_tvalid;
  wire [31:0]axi_str_txd_tdata;
  wire axi_str_txd_tlast;
  wire axi_str_txd_tready;
  wire axi_str_txd_tvalid;
  wire empty_fwft_i;
  wire eqOp__6;
  wire \gaxif.COMP_AXI4_n_10 ;
  wire \gaxif.COMP_AXI4_n_11 ;
  wire \gaxif.COMP_AXI4_n_13 ;
  wire \gaxif.COMP_AXI4_n_15 ;
  wire \gaxif.COMP_AXI4_n_16 ;
  wire \gaxif.COMP_AXI4_n_17 ;
  wire \gaxif.COMP_AXI4_n_18 ;
  wire \gaxif.COMP_AXI4_n_21 ;
  wire \gaxif.COMP_AXI4_n_22 ;
  wire \gaxif.COMP_AXI4_n_7 ;
  wire \gaxif.COMP_AXI4_n_8 ;
  wire \gaxif.COMP_AXI4_n_9 ;
  wire \grxd.COMP_RX_FIFO/gfifo_gen.COMP_AXIS_FG_FIFO/COMP_FIFO/inst_fifo_gen/axis_rd_en__0 ;
  wire [9:0]\grxd.COMP_RX_FIFO/gfifo_gen.COMP_AXIS_FG_FIFO/COMP_FIFO/inst_fifo_gen/gaxis_fifo.gaxisf.axisf/grf.rf/gntv_or_sync_fifo.gl0.rd/gr1.gdcf.dc/dc/count_reg ;
  wire interrupt;
  wire mm2s_cntrl_reset_out_n;
  wire mm2s_prmry_reset_out_n;
  wire p_13_in;
  wire p_1_out;
  wire ram_full_i;
  wire rx_fg_len_empty;
  wire s2mm_prmry_reset_out_n;
  wire [31:0]s_axi4_araddr;
  wire [3:0]s_axi4_arid;
  wire [7:0]s_axi4_arlen;
  wire s_axi4_arready;
  wire s_axi4_arvalid;
  wire [31:0]s_axi4_awaddr;
  wire [3:0]s_axi4_awid;
  wire [7:0]s_axi4_awlen;
  wire s_axi4_awready;
  wire s_axi4_awvalid;
  wire [3:0]s_axi4_bid;
  wire s_axi4_bready;
  wire s_axi4_bvalid;
  wire [31:0]s_axi4_rdata;
  wire [3:0]s_axi4_rid;
  wire s_axi4_rlast;
  wire s_axi4_rready;
  wire s_axi4_rvalid;
  wire [31:0]s_axi4_wdata;
  wire s_axi4_wready;
  wire [3:0]s_axi4_wstrb;
  wire s_axi4_wvalid;
  wire s_axi_aclk;
  wire [31:0]s_axi_araddr;
  wire s_axi_aresetn;
  wire s_axi_arready;
  wire s_axi_arvalid;
  wire [31:0]s_axi_awaddr;
  wire s_axi_awready;
  wire s_axi_awvalid;
  wire s_axi_bready;
  wire [1:1]\^s_axi_bresp ;
  wire s_axi_bvalid;
  wire [31:0]s_axi_rdata;
  wire s_axi_rready;
  wire [1:1]\^s_axi_rresp ;
  wire s_axi_rvalid;
  wire [31:0]s_axi_wdata;
  wire s_axi_wvalid;
  wire sig_Bus2IP_CS;
  wire sig_Bus2IP_Reset;
  wire [0:31]sig_IP2Bus_Data;
  wire sig_IP2Bus_Error;
  wire sig_axi_rd_en;
  wire [10:31]sig_ip2bus_data;
  wire [0:12]\sig_register_array[1]_0 ;
  wire sig_rxd_pe_event__1;
  wire sig_rxd_pf_event__1;
  wire sig_rxd_reset;
  wire sig_txd_pe_event__1;
  wire sig_txd_pf_event__1;
  wire sig_txd_reset;
  wire tx_fifo_or;
  wire txd_wr_en;
  wire [1:1]vacancy_i;

  assign axi_str_txc_tdata[31] = \<const1> ;
  assign axi_str_txc_tdata[30] = \<const1> ;
  assign axi_str_txc_tdata[29] = \<const1> ;
  assign axi_str_txc_tdata[28] = \<const1> ;
  assign axi_str_txc_tdata[27] = \<const1> ;
  assign axi_str_txc_tdata[26] = \<const1> ;
  assign axi_str_txc_tdata[25] = \<const1> ;
  assign axi_str_txc_tdata[24] = \<const1> ;
  assign axi_str_txc_tdata[23] = \<const1> ;
  assign axi_str_txc_tdata[22] = \<const1> ;
  assign axi_str_txc_tdata[21] = \<const1> ;
  assign axi_str_txc_tdata[20] = \<const1> ;
  assign axi_str_txc_tdata[19] = \<const1> ;
  assign axi_str_txc_tdata[18] = \<const1> ;
  assign axi_str_txc_tdata[17] = \<const1> ;
  assign axi_str_txc_tdata[16] = \<const1> ;
  assign axi_str_txc_tdata[15] = \<const1> ;
  assign axi_str_txc_tdata[14] = \<const1> ;
  assign axi_str_txc_tdata[13] = \<const1> ;
  assign axi_str_txc_tdata[12] = \<const1> ;
  assign axi_str_txc_tdata[11] = \<const1> ;
  assign axi_str_txc_tdata[10] = \<const1> ;
  assign axi_str_txc_tdata[9] = \<const1> ;
  assign axi_str_txc_tdata[8] = \<const1> ;
  assign axi_str_txc_tdata[7] = \<const1> ;
  assign axi_str_txc_tdata[6] = \<const1> ;
  assign axi_str_txc_tdata[5] = \<const1> ;
  assign axi_str_txc_tdata[4] = \<const1> ;
  assign axi_str_txc_tdata[3] = \<const1> ;
  assign axi_str_txc_tdata[2] = \<const1> ;
  assign axi_str_txc_tdata[1] = \<const1> ;
  assign axi_str_txc_tdata[0] = \<const1> ;
  assign axi_str_txc_tdest[3] = \<const0> ;
  assign axi_str_txc_tdest[2] = \<const0> ;
  assign axi_str_txc_tdest[1] = \<const0> ;
  assign axi_str_txc_tdest[0] = \<const0> ;
  assign axi_str_txc_tid[3] = \<const0> ;
  assign axi_str_txc_tid[2] = \<const0> ;
  assign axi_str_txc_tid[1] = \<const0> ;
  assign axi_str_txc_tid[0] = \<const0> ;
  assign axi_str_txc_tkeep[3] = \<const1> ;
  assign axi_str_txc_tkeep[2] = \<const1> ;
  assign axi_str_txc_tkeep[1] = \<const1> ;
  assign axi_str_txc_tkeep[0] = \<const1> ;
  assign axi_str_txc_tstrb[3] = \<const0> ;
  assign axi_str_txc_tstrb[2] = \<const0> ;
  assign axi_str_txc_tstrb[1] = \<const0> ;
  assign axi_str_txc_tstrb[0] = \<const0> ;
  assign axi_str_txc_tuser[3] = \<const0> ;
  assign axi_str_txc_tuser[2] = \<const0> ;
  assign axi_str_txc_tuser[1] = \<const0> ;
  assign axi_str_txc_tuser[0] = \<const0> ;
  assign axi_str_txd_tdest[3] = \<const0> ;
  assign axi_str_txd_tdest[2] = \<const0> ;
  assign axi_str_txd_tdest[1] = \<const0> ;
  assign axi_str_txd_tdest[0] = \<const0> ;
  assign axi_str_txd_tid[3] = \<const0> ;
  assign axi_str_txd_tid[2] = \<const0> ;
  assign axi_str_txd_tid[1] = \<const0> ;
  assign axi_str_txd_tid[0] = \<const0> ;
  assign axi_str_txd_tkeep[3] = \<const1> ;
  assign axi_str_txd_tkeep[2] = \<const1> ;
  assign axi_str_txd_tkeep[1] = \<const1> ;
  assign axi_str_txd_tkeep[0] = \<const1> ;
  assign axi_str_txd_tstrb[3] = \<const0> ;
  assign axi_str_txd_tstrb[2] = \<const0> ;
  assign axi_str_txd_tstrb[1] = \<const0> ;
  assign axi_str_txd_tstrb[0] = \<const0> ;
  assign axi_str_txd_tuser[3] = \<const0> ;
  assign axi_str_txd_tuser[2] = \<const0> ;
  assign axi_str_txd_tuser[1] = \<const0> ;
  assign axi_str_txd_tuser[0] = \<const0> ;
  assign s_axi4_bresp[1] = \<const0> ;
  assign s_axi4_bresp[0] = \<const0> ;
  assign s_axi4_rresp[1] = \<const0> ;
  assign s_axi4_rresp[0] = \<const0> ;
  assign s_axi_bresp[1] = \^s_axi_bresp [1];
  assign s_axi_bresp[0] = \<const0> ;
  assign s_axi_rresp[1] = \^s_axi_rresp [1];
  assign s_axi_rresp[0] = \<const0> ;
  assign s_axi_wready = s_axi_awready;
  axi_fifo_mm_s_0_ipic2axi_s COMP_IPIC2AXI_S
       (.Bus_RNW_reg_reg({\sig_register_array[1]_0 [0],\sig_register_array[1]_0 [1],\sig_register_array[1]_0 [2],\sig_register_array[1]_0 [3],\sig_register_array[1]_0 [4],\sig_register_array[1]_0 [5],\sig_register_array[1]_0 [6],\sig_register_array[1]_0 [7],\sig_register_array[1]_0 [8],\sig_register_array[1]_0 [9],\sig_register_array[1]_0 [10],\sig_register_array[1]_0 [11],\sig_register_array[1]_0 [12]}),
        .D({sig_ip2bus_data[10],sig_ip2bus_data[11],sig_ip2bus_data[12],sig_ip2bus_data[13],sig_ip2bus_data[14],sig_ip2bus_data[15],sig_ip2bus_data[16],sig_ip2bus_data[17],sig_ip2bus_data[18],sig_ip2bus_data[19],sig_ip2bus_data[20],sig_ip2bus_data[21],sig_ip2bus_data[22],sig_ip2bus_data[23],sig_ip2bus_data[24],sig_ip2bus_data[25],sig_ip2bus_data[26],sig_ip2bus_data[27],sig_ip2bus_data[28],sig_ip2bus_data[29],sig_ip2bus_data[30],sig_ip2bus_data[31]}),
        .\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram (COMP_IPIC2AXI_S_n_0),
        .E(\gaxif.COMP_AXI4_n_7 ),
        .\GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg[0] (COMP_IPIF_n_63),
        .\GEN_BKEND_CE_REGISTERS[11].ce_out_i_reg[11] (COMP_IPIF_n_67),
        .\GEN_BKEND_CE_REGISTERS[1].ce_out_i_reg[1] (COMP_IPIF_n_62),
        .\GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg[7] (COMP_IPIF_n_64),
        .\GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg[9] (COMP_IPIF_n_61),
        .IP2Bus_Error(IP2Bus_Error),
        .IP2Bus_Error1_in(IP2Bus_Error1_in),
        .IPIC_STATE(IPIC_STATE),
        .IPIC_STATE_reg_0(COMP_IPIF_n_5),
        .IPIC_STATE_reg_1(COMP_IPIF_n_70),
        .IPIC_STATE_reg_2(COMP_IPIF_n_69),
        .IPIC_STATE_reg_3(COMP_IPIF_n_20),
        .IPIC_STATE_reg_4(COMP_IPIF_n_22),
        .\MEM_DECODE_GEN[0].cs_out_i_reg[0] (COMP_IPIF_n_19),
        .Q(\grxd.COMP_RX_FIFO/gfifo_gen.COMP_AXIS_FG_FIFO/COMP_FIFO/inst_fifo_gen/gaxis_fifo.gaxisf.axisf/grf.rf/gntv_or_sync_fifo.gl0.rd/gr1.gdcf.dc/dc/count_reg ),
        .SR(sig_Bus2IP_Reset),
        .axi4_fifo_rd_en_i(axi4_fifo_rd_en_i),
        .axi_str_rxd_tdata(axi_str_rxd_tdata),
        .axi_str_rxd_tlast(axi_str_rxd_tlast),
        .axi_str_rxd_tready(axi_str_rxd_tready),
        .axi_str_rxd_tvalid(axi_str_rxd_tvalid),
        .axi_str_txc_tlast(axi_str_txc_tlast),
        .axi_str_txc_tready(axi_str_txc_tready),
        .axi_str_txc_tvalid(axi_str_txc_tvalid),
        .\axi_str_txd_tdata[31] ({axi_str_txd_tdata,axi_str_txd_tlast}),
        .axi_str_txd_tready(axi_str_txd_tready),
        .axi_str_txd_tvalid(axi_str_txd_tvalid),
        .axis_rd_en__0(\grxd.COMP_RX_FIFO/gfifo_gen.COMP_AXIS_FG_FIFO/COMP_FIFO/inst_fifo_gen/axis_rd_en__0 ),
        .bus2ip_rnw_i_reg(COMP_IPIF_n_21),
        .bus2ip_rnw_i_reg_0(COMP_IPIF_n_68),
        .cs_ce_clr(\I_SLAVE_ATTACHMENT/I_DECODER/cs_ce_clr ),
        .empty_fwft_i(empty_fwft_i),
        .eqOp__6(eqOp__6),
        .\gaxi_bid_gen.S_AXI_BID_reg[0] (axi4_fifo_readyn),
        .\gaxi_full_sm.r_last_r_reg (s_axi4_rlast),
        .\gaxi_full_sm.r_valid_r_reg (\gaxif.COMP_AXI4_n_15 ),
        .\gc0.count_reg[0] (COMP_IPIC2AXI_S_n_77),
        .\gc0.count_reg[0]_0 (COMP_IPIC2AXI_S_n_78),
        .\gpr1.dout_i_reg[0] (COMP_IPIC2AXI_S_n_33),
        .interrupt(interrupt),
        .mm2s_cntrl_reset_out_n(mm2s_cntrl_reset_out_n),
        .mm2s_prmry_reset_out_n(mm2s_prmry_reset_out_n),
        .out(rx_fg_len_empty),
        .p_13_in(p_13_in),
        .p_1_out(p_1_out),
        .ram_full_i(ram_full_i),
        .s2mm_prmry_reset_out_n(s2mm_prmry_reset_out_n),
        .s_axi4_araddr({s_axi4_araddr[25:22],s_axi4_araddr[17:14],s_axi4_araddr[5:2]}),
        .\s_axi4_araddr[21] (\gaxif.COMP_AXI4_n_22 ),
        .\s_axi4_araddr[29] (\gaxif.COMP_AXI4_n_21 ),
        .s_axi4_rdata(s_axi4_rdata),
        .s_axi4_wdata(s_axi4_wdata),
        .s_axi_aclk(s_axi_aclk),
        .s_axi_aresetn(s_axi_aresetn),
        .s_axi_arready(s_axi_arready),
        .s_axi_awready(s_axi_awready),
        .\s_axi_rdata_i_reg[31] ({sig_IP2Bus_Data[0],sig_IP2Bus_Data[1],sig_IP2Bus_Data[2],sig_IP2Bus_Data[3],sig_IP2Bus_Data[4],sig_IP2Bus_Data[5],sig_IP2Bus_Data[6],sig_IP2Bus_Data[7],sig_IP2Bus_Data[8],sig_IP2Bus_Data[9],sig_IP2Bus_Data[10],sig_IP2Bus_Data[11],sig_IP2Bus_Data[12],sig_IP2Bus_Data[13],sig_IP2Bus_Data[14],sig_IP2Bus_Data[15],sig_IP2Bus_Data[16],sig_IP2Bus_Data[17],sig_IP2Bus_Data[18],sig_IP2Bus_Data[19],sig_IP2Bus_Data[20],sig_IP2Bus_Data[21],sig_IP2Bus_Data[22],sig_IP2Bus_Data[23],sig_IP2Bus_Data[24],sig_IP2Bus_Data[25],sig_IP2Bus_Data[26],sig_IP2Bus_Data[27],sig_IP2Bus_Data[28],sig_IP2Bus_Data[29],sig_IP2Bus_Data[30],sig_IP2Bus_Data[31]}),
        .s_axi_wdata(s_axi_wdata),
        .sig_Bus2IP_CS(sig_Bus2IP_CS),
        .sig_IP2Bus_Error(sig_IP2Bus_Error),
        .sig_axi_rd_en(sig_axi_rd_en),
        .\sig_ip2bus_data_reg[0]_0 (COMP_IPIC2AXI_S_n_7),
        .\sig_ip2bus_data_reg[10]_0 ({COMP_IPIC2AXI_S_n_104,COMP_IPIC2AXI_S_n_105,COMP_IPIC2AXI_S_n_106}),
        .\sig_ip2bus_data_reg[10]_1 ({COMP_IPIC2AXI_S_n_139,COMP_IPIC2AXI_S_n_140,COMP_IPIC2AXI_S_n_141,COMP_IPIC2AXI_S_n_142,COMP_IPIC2AXI_S_n_143,COMP_IPIC2AXI_S_n_144,COMP_IPIC2AXI_S_n_145,COMP_IPIC2AXI_S_n_146,COMP_IPIC2AXI_S_n_147,COMP_IPIC2AXI_S_n_148,COMP_IPIC2AXI_S_n_149,COMP_IPIC2AXI_S_n_150,COMP_IPIC2AXI_S_n_151,COMP_IPIC2AXI_S_n_152,COMP_IPIC2AXI_S_n_153,COMP_IPIC2AXI_S_n_154,COMP_IPIC2AXI_S_n_155,COMP_IPIC2AXI_S_n_156,COMP_IPIC2AXI_S_n_157,COMP_IPIC2AXI_S_n_158,COMP_IPIC2AXI_S_n_159,COMP_IPIC2AXI_S_n_160}),
        .\sig_ip2bus_data_reg[1]_0 (COMP_IPIC2AXI_S_n_8),
        .\sig_ip2bus_data_reg[22]_0 (COMP_IPIC2AXI_S_n_93),
        .\sig_ip2bus_data_reg[23]_0 (COMP_IPIC2AXI_S_n_92),
        .\sig_ip2bus_data_reg[24]_0 (COMP_IPIC2AXI_S_n_91),
        .\sig_ip2bus_data_reg[25]_0 (COMP_IPIC2AXI_S_n_90),
        .\sig_ip2bus_data_reg[26]_0 (COMP_IPIC2AXI_S_n_89),
        .\sig_ip2bus_data_reg[27]_0 (COMP_IPIC2AXI_S_n_88),
        .\sig_ip2bus_data_reg[28]_0 (COMP_IPIC2AXI_S_n_87),
        .\sig_ip2bus_data_reg[29]_0 (COMP_IPIC2AXI_S_n_85),
        .\sig_ip2bus_data_reg[2]_0 (COMP_IPIC2AXI_S_n_9),
        .\sig_ip2bus_data_reg[30]_0 (vacancy_i),
        .\sig_ip2bus_data_reg[3]_0 (COMP_IPIC2AXI_S_n_10),
        .\sig_ip2bus_data_reg[4]_0 (COMP_IPIC2AXI_S_n_11),
        .\sig_ip2bus_data_reg[5]_0 (COMP_IPIC2AXI_S_n_12),
        .\sig_ip2bus_data_reg[6]_0 (COMP_IPIC2AXI_S_n_13),
        .\sig_ip2bus_data_reg[7]_0 (COMP_IPIC2AXI_S_n_14),
        .\sig_ip2bus_data_reg[8]_0 (COMP_IPIC2AXI_S_n_15),
        .\sig_ip2bus_data_reg[9]_0 (COMP_IPIC2AXI_S_n_16),
        .\sig_register_array_reg[0][10]_0 (COMP_IPIC2AXI_S_n_17),
        .\sig_register_array_reg[0][10]_1 (COMP_IPIF_n_15),
        .\sig_register_array_reg[0][11]_0 (COMP_IPIC2AXI_S_n_18),
        .\sig_register_array_reg[0][11]_1 (COMP_IPIF_n_16),
        .\sig_register_array_reg[0][12]_0 (COMP_IPIC2AXI_S_n_19),
        .\sig_register_array_reg[0][12]_1 (COMP_IPIF_n_17),
        .\sig_register_array_reg[0][1]_0 (COMP_IPIC2AXI_S_n_96),
        .\sig_register_array_reg[0][1]_1 (COMP_IPIF_n_6),
        .\sig_register_array_reg[0][2]_0 (COMP_IPIC2AXI_S_n_95),
        .\sig_register_array_reg[0][2]_1 (COMP_IPIC2AXI_S_n_97),
        .\sig_register_array_reg[0][2]_2 (COMP_IPIF_n_7),
        .\sig_register_array_reg[0][3]_0 (COMP_IPIF_n_8),
        .\sig_register_array_reg[0][4]_0 (COMP_IPIC2AXI_S_n_73),
        .\sig_register_array_reg[0][4]_1 (COMP_IPIF_n_9),
        .\sig_register_array_reg[0][5]_0 (COMP_IPIF_n_10),
        .\sig_register_array_reg[0][6]_0 (COMP_IPIF_n_11),
        .\sig_register_array_reg[0][7]_0 (COMP_IPIF_n_12),
        .\sig_register_array_reg[0][8]_0 (COMP_IPIF_n_13),
        .\sig_register_array_reg[0][9]_0 (COMP_IPIF_n_14),
        .sig_rx_channel_reset_reg_0(COMP_IPIF_n_65),
        .sig_rxd_pe_event__1(sig_rxd_pe_event__1),
        .sig_rxd_pf_event__1(sig_rxd_pf_event__1),
        .sig_rxd_reset(sig_rxd_reset),
        .sig_tx_channel_reset_reg_0(COMP_IPIF_n_66),
        .sig_txd_pe_event__1(sig_txd_pe_event__1),
        .sig_txd_pf_event__1(sig_txd_pf_event__1),
        .sig_txd_reset(sig_txd_reset),
        .sig_txd_sb_wr_en_reg_0({\gaxif.COMP_AXI4_n_8 ,\gaxif.COMP_AXI4_n_9 ,\gaxif.COMP_AXI4_n_10 ,\gaxif.COMP_AXI4_n_11 }),
        .\sig_txd_wr_data_reg[31]_0 (COMP_IPIC2AXI_S_n_84),
        .sync_areset_n_reg_inv(COMP_IPIC2AXI_S_n_5),
        .sync_areset_n_reg_inv_0(COMP_IPIC2AXI_S_n_34),
        .tx_fifo_or(tx_fifo_or),
        .txd_wr_en(txd_wr_en));
  axi_fifo_mm_s_0_axi_lite_ipif COMP_IPIF
       (.D({sig_ip2bus_data[10],sig_ip2bus_data[11],sig_ip2bus_data[12],sig_ip2bus_data[13],sig_ip2bus_data[14],sig_ip2bus_data[15],sig_ip2bus_data[16],sig_ip2bus_data[17],sig_ip2bus_data[18],sig_ip2bus_data[19],sig_ip2bus_data[20],sig_ip2bus_data[21],sig_ip2bus_data[22],sig_ip2bus_data[23],sig_ip2bus_data[24],sig_ip2bus_data[25],sig_ip2bus_data[26],sig_ip2bus_data[27],sig_ip2bus_data[28],sig_ip2bus_data[29],sig_ip2bus_data[30],sig_ip2bus_data[31]}),
        .IP2Bus_Error(IP2Bus_Error),
        .IP2Bus_Error1_in(IP2Bus_Error1_in),
        .IP2Bus_RdAck_reg(COMP_IPIF_n_21),
        .IP2Bus_RdAck_reg_0(s_axi_arready),
        .IP2Bus_WrAck_reg(COMP_IPIF_n_68),
        .IP2Bus_WrAck_reg_0(s_axi_awready),
        .IPIC_STATE(IPIC_STATE),
        .Q(\grxd.COMP_RX_FIFO/gfifo_gen.COMP_AXIS_FG_FIFO/COMP_FIFO/inst_fifo_gen/gaxis_fifo.gaxisf.axisf/grf.rf/gntv_or_sync_fifo.gl0.rd/gr1.gdcf.dc/dc/count_reg ),
        .SR(sig_Bus2IP_Reset),
        .axi_str_txd_tready(axi_str_txd_tready),
        .cs_ce_clr(\I_SLAVE_ATTACHMENT/I_DECODER/cs_ce_clr ),
        .eqOp__6(eqOp__6),
        .\gaxi_full_sm.r_last_r_reg (\gaxif.COMP_AXI4_n_17 ),
        .\gaxi_full_sm.r_last_r_reg_0 (\gaxif.COMP_AXI4_n_18 ),
        .\gaxi_full_sm.r_last_r_reg_1 (\gaxif.COMP_AXI4_n_13 ),
        .\gfifo_gen.gmm2s.vacancy_i_reg[1] (vacancy_i),
        .\gfifo_gen.gmm2s.vacancy_i_reg[2] (COMP_IPIC2AXI_S_n_85),
        .\gfifo_gen.gmm2s.vacancy_i_reg[3] (COMP_IPIC2AXI_S_n_87),
        .\gfifo_gen.gmm2s.vacancy_i_reg[4] (COMP_IPIC2AXI_S_n_88),
        .\gfifo_gen.gmm2s.vacancy_i_reg[5] (COMP_IPIC2AXI_S_n_89),
        .\gfifo_gen.gmm2s.vacancy_i_reg[6] (COMP_IPIC2AXI_S_n_90),
        .\gfifo_gen.gmm2s.vacancy_i_reg[7] (COMP_IPIC2AXI_S_n_91),
        .\gfifo_gen.gmm2s.vacancy_i_reg[8] (COMP_IPIC2AXI_S_n_92),
        .\gfifo_gen.gmm2s.vacancy_i_reg[9] (COMP_IPIC2AXI_S_n_93),
        .\goreg_bm.dout_i_reg[0] (COMP_IPIC2AXI_S_n_73),
        .\goreg_dm.dout_i_reg[21] ({COMP_IPIC2AXI_S_n_139,COMP_IPIC2AXI_S_n_140,COMP_IPIC2AXI_S_n_141,COMP_IPIC2AXI_S_n_142,COMP_IPIC2AXI_S_n_143,COMP_IPIC2AXI_S_n_144,COMP_IPIC2AXI_S_n_145,COMP_IPIC2AXI_S_n_146,COMP_IPIC2AXI_S_n_147,COMP_IPIC2AXI_S_n_148,COMP_IPIC2AXI_S_n_149,COMP_IPIC2AXI_S_n_150,COMP_IPIC2AXI_S_n_151,COMP_IPIC2AXI_S_n_152,COMP_IPIC2AXI_S_n_153,COMP_IPIC2AXI_S_n_154,COMP_IPIC2AXI_S_n_155,COMP_IPIC2AXI_S_n_156,COMP_IPIC2AXI_S_n_157,COMP_IPIC2AXI_S_n_158,COMP_IPIC2AXI_S_n_159,COMP_IPIC2AXI_S_n_160}),
        .\gtxd.sig_txd_packet_size_reg[30] (COMP_IPIC2AXI_S_n_84),
        .out(rx_fg_len_empty),
        .p_13_in(p_13_in),
        .s_axi_aclk(s_axi_aclk),
        .s_axi_araddr(s_axi_araddr[5:2]),
        .s_axi_aresetn(s_axi_aresetn),
        .s_axi_arvalid(s_axi_arvalid),
        .s_axi_awaddr(s_axi_awaddr[5:2]),
        .s_axi_awvalid(s_axi_awvalid),
        .s_axi_bready(s_axi_bready),
        .s_axi_bresp(\^s_axi_bresp ),
        .s_axi_bvalid(s_axi_bvalid),
        .s_axi_rdata(s_axi_rdata),
        .s_axi_rready(s_axi_rready),
        .s_axi_rresp(\^s_axi_rresp ),
        .s_axi_rvalid(s_axi_rvalid),
        .s_axi_wdata(s_axi_wdata[31:19]),
        .s_axi_wvalid(s_axi_wvalid),
        .sig_Bus2IP_CS(sig_Bus2IP_CS),
        .sig_IP2Bus_Error(sig_IP2Bus_Error),
        .\sig_ip2bus_data_reg[0] ({sig_IP2Bus_Data[0],sig_IP2Bus_Data[1],sig_IP2Bus_Data[2],sig_IP2Bus_Data[3],sig_IP2Bus_Data[4],sig_IP2Bus_Data[5],sig_IP2Bus_Data[6],sig_IP2Bus_Data[7],sig_IP2Bus_Data[8],sig_IP2Bus_Data[9],sig_IP2Bus_Data[10],sig_IP2Bus_Data[11],sig_IP2Bus_Data[12],sig_IP2Bus_Data[13],sig_IP2Bus_Data[14],sig_IP2Bus_Data[15],sig_IP2Bus_Data[16],sig_IP2Bus_Data[17],sig_IP2Bus_Data[18],sig_IP2Bus_Data[19],sig_IP2Bus_Data[20],sig_IP2Bus_Data[21],sig_IP2Bus_Data[22],sig_IP2Bus_Data[23],sig_IP2Bus_Data[24],sig_IP2Bus_Data[25],sig_IP2Bus_Data[26],sig_IP2Bus_Data[27],sig_IP2Bus_Data[28],sig_IP2Bus_Data[29],sig_IP2Bus_Data[30],sig_IP2Bus_Data[31]}),
        .\sig_ip2bus_data_reg[22] (COMP_IPIF_n_64),
        .\sig_ip2bus_data_reg[30] (COMP_IPIF_n_61),
        .\sig_ip2bus_data_reg[31] (COMP_IPIF_n_19),
        .\sig_ip2bus_data_reg[9] (COMP_IPIF_n_62),
        .\sig_ip2bus_data_reg[9]_0 (COMP_IPIF_n_63),
        .sig_rd_rlen_reg(COMP_IPIF_n_69),
        .\sig_register_array_reg[0][0] (COMP_IPIF_n_5),
        .\sig_register_array_reg[0][0]_0 (COMP_IPIC2AXI_S_n_7),
        .\sig_register_array_reg[0][10] (COMP_IPIF_n_15),
        .\sig_register_array_reg[0][10]_0 (COMP_IPIC2AXI_S_n_17),
        .\sig_register_array_reg[0][11] (COMP_IPIF_n_16),
        .\sig_register_array_reg[0][11]_0 (COMP_IPIC2AXI_S_n_18),
        .\sig_register_array_reg[0][12] (COMP_IPIF_n_17),
        .\sig_register_array_reg[0][12]_0 (COMP_IPIC2AXI_S_n_19),
        .\sig_register_array_reg[0][1] (COMP_IPIF_n_6),
        .\sig_register_array_reg[0][1]_0 (COMP_IPIF_n_25),
        .\sig_register_array_reg[0][1]_1 (COMP_IPIC2AXI_S_n_8),
        .\sig_register_array_reg[0][2] (COMP_IPIF_n_7),
        .\sig_register_array_reg[0][2]_0 (COMP_IPIF_n_24),
        .\sig_register_array_reg[0][2]_1 (COMP_IPIC2AXI_S_n_9),
        .\sig_register_array_reg[0][3] (COMP_IPIF_n_8),
        .\sig_register_array_reg[0][3]_0 (COMP_IPIC2AXI_S_n_10),
        .\sig_register_array_reg[0][4] (COMP_IPIF_n_9),
        .\sig_register_array_reg[0][4]_0 (COMP_IPIC2AXI_S_n_11),
        .\sig_register_array_reg[0][5] (COMP_IPIF_n_10),
        .\sig_register_array_reg[0][5]_0 (COMP_IPIC2AXI_S_n_12),
        .\sig_register_array_reg[0][6] (COMP_IPIF_n_11),
        .\sig_register_array_reg[0][6]_0 (COMP_IPIC2AXI_S_n_13),
        .\sig_register_array_reg[0][7] (COMP_IPIF_n_12),
        .\sig_register_array_reg[0][7]_0 (COMP_IPIC2AXI_S_n_14),
        .\sig_register_array_reg[0][8] (COMP_IPIF_n_13),
        .\sig_register_array_reg[0][8]_0 (COMP_IPIC2AXI_S_n_15),
        .\sig_register_array_reg[0][9] (COMP_IPIF_n_14),
        .\sig_register_array_reg[0][9]_0 (COMP_IPIC2AXI_S_n_16),
        .\sig_register_array_reg[1][0] (COMP_IPIF_n_22),
        .\sig_register_array_reg[1][0]_0 ({\sig_register_array[1]_0 [0],\sig_register_array[1]_0 [1],\sig_register_array[1]_0 [2],\sig_register_array[1]_0 [3],\sig_register_array[1]_0 [4],\sig_register_array[1]_0 [5],\sig_register_array[1]_0 [6],\sig_register_array[1]_0 [7],\sig_register_array[1]_0 [8],\sig_register_array[1]_0 [9],\sig_register_array[1]_0 [10],\sig_register_array[1]_0 [11],\sig_register_array[1]_0 [12]}),
        .\sig_register_array_reg[1][10] ({COMP_IPIC2AXI_S_n_104,COMP_IPIC2AXI_S_n_105,COMP_IPIC2AXI_S_n_106}),
        .sig_rx_channel_reset_reg(COMP_IPIF_n_65),
        .sig_rx_channel_reset_reg_0(\gaxif.COMP_AXI4_n_16 ),
        .sig_rx_channel_reset_reg_1(COMP_IPIC2AXI_S_n_33),
        .sig_rxd_pe_event__1(sig_rxd_pe_event__1),
        .sig_rxd_pf_event__1(sig_rxd_pf_event__1),
        .sig_rxd_reset(sig_rxd_reset),
        .sig_str_rst_reg(COMP_IPIF_n_18),
        .sig_str_rst_reg_0(COMP_IPIF_n_67),
        .sig_str_rst_reg_1(COMP_IPIC2AXI_S_n_5),
        .sig_tx_channel_reset_reg(COMP_IPIF_n_66),
        .sig_tx_channel_reset_reg_0(COMP_IPIC2AXI_S_n_34),
        .sig_txd_pe_event__1(sig_txd_pe_event__1),
        .sig_txd_pf_event__1(sig_txd_pf_event__1),
        .sig_txd_reset(sig_txd_reset),
        .sig_txd_sb_wr_en_reg(COMP_IPIF_n_70),
        .\sig_txd_wr_data_reg[31] (COMP_IPIF_n_20),
        .tx_fifo_or(tx_fifo_or));
  GND GND
       (.G(\<const0> ));
  VCC VCC
       (.P(\<const1> ));
  axi_fifo_mm_s_0_axi_wrapper \gaxif.COMP_AXI4 
       (.AS(ram_full_i),
        .Bus_RNW_reg_reg(COMP_IPIF_n_18),
        .Bus_RNW_reg_reg_0(COMP_IPIF_n_24),
        .Bus_RNW_reg_reg_1(COMP_IPIF_n_25),
        .E(\gaxif.COMP_AXI4_n_7 ),
        .axi4_fifo_rd_en_i(axi4_fifo_rd_en_i),
        .axis_rd_en__0(\grxd.COMP_RX_FIFO/gfifo_gen.COMP_AXIS_FG_FIFO/COMP_FIFO/inst_fifo_gen/axis_rd_en__0 ),
        .\count_reg[8] (COMP_IPIC2AXI_S_n_97),
        .\count_reg[9] (COMP_IPIC2AXI_S_n_95),
        .empty_fwft_i(empty_fwft_i),
        .\gc0.count_reg[0] (\gaxif.COMP_AXI4_n_15 ),
        .\gfifo_gen.gmm2s.gaxi4_strb.input_tstrb_reg[3] ({\gaxif.COMP_AXI4_n_8 ,\gaxif.COMP_AXI4_n_9 ,\gaxif.COMP_AXI4_n_10 ,\gaxif.COMP_AXI4_n_11 }),
        .\grid.ar_id_r_reg[0] (\gaxif.COMP_AXI4_n_21 ),
        .\grid.ar_id_r_reg[0]_0 (\gaxif.COMP_AXI4_n_22 ),
        .p_1_out(p_1_out),
        .ram_full_i_reg(axi4_fifo_readyn),
        .s_axi4_araddr(s_axi4_araddr),
        .\s_axi4_araddr_25__s_port_] (COMP_IPIC2AXI_S_n_77),
        .s_axi4_araddr_2__s_port_(COMP_IPIC2AXI_S_n_78),
        .s_axi4_arid(s_axi4_arid),
        .s_axi4_arlen(s_axi4_arlen),
        .s_axi4_arready(s_axi4_arready),
        .s_axi4_arvalid(s_axi4_arvalid),
        .s_axi4_awaddr(s_axi4_awaddr),
        .s_axi4_awid(s_axi4_awid),
        .s_axi4_awlen(s_axi4_awlen),
        .s_axi4_awready(s_axi4_awready),
        .s_axi4_awvalid(s_axi4_awvalid),
        .s_axi4_bid(s_axi4_bid),
        .s_axi4_bready(s_axi4_bready),
        .s_axi4_bvalid(s_axi4_bvalid),
        .s_axi4_rid(s_axi4_rid),
        .s_axi4_rlast(s_axi4_rlast),
        .s_axi4_rready(s_axi4_rready),
        .s_axi4_rvalid(s_axi4_rvalid),
        .s_axi4_wready(s_axi4_wready),
        .s_axi4_wstrb(s_axi4_wstrb),
        .s_axi4_wvalid(s_axi4_wvalid),
        .s_axi_aclk(s_axi_aclk),
        .sig_axi_rd_en(sig_axi_rd_en),
        .\sig_register_array_reg[0][1] (\gaxif.COMP_AXI4_n_17 ),
        .\sig_register_array_reg[0][1]_0 (\gaxif.COMP_AXI4_n_18 ),
        .\sig_register_array_reg[0][2] (\gaxif.COMP_AXI4_n_13 ),
        .\sig_register_array_reg[0][2]_0 (\gaxif.COMP_AXI4_n_16 ),
        .sig_rx_channel_reset_reg(COMP_IPIC2AXI_S_n_33),
        .sig_rx_channel_reset_reg_0(COMP_IPIC2AXI_S_n_96),
        .sig_txd_sb_wr_en_reg(COMP_IPIC2AXI_S_n_0),
        .txd_wr_en(txd_wr_en));
endmodule

(* ORIG_REF_NAME = "axi_lite_ipif" *) 
module axi_fifo_mm_s_0_axi_lite_ipif
   (s_axi_rresp,
    s_axi_rvalid,
    s_axi_bvalid,
    s_axi_bresp,
    sig_Bus2IP_CS,
    \sig_register_array_reg[0][0] ,
    \sig_register_array_reg[0][1] ,
    \sig_register_array_reg[0][2] ,
    \sig_register_array_reg[0][3] ,
    \sig_register_array_reg[0][4] ,
    \sig_register_array_reg[0][5] ,
    \sig_register_array_reg[0][6] ,
    \sig_register_array_reg[0][7] ,
    \sig_register_array_reg[0][8] ,
    \sig_register_array_reg[0][9] ,
    \sig_register_array_reg[0][10] ,
    \sig_register_array_reg[0][11] ,
    \sig_register_array_reg[0][12] ,
    sig_str_rst_reg,
    \sig_ip2bus_data_reg[31] ,
    \sig_txd_wr_data_reg[31] ,
    IP2Bus_RdAck_reg,
    \sig_register_array_reg[1][0] ,
    IP2Bus_Error,
    \sig_register_array_reg[0][2]_0 ,
    \sig_register_array_reg[0][1]_0 ,
    \sig_register_array_reg[1][0]_0 ,
    D,
    \sig_ip2bus_data_reg[30] ,
    \sig_ip2bus_data_reg[9] ,
    \sig_ip2bus_data_reg[9]_0 ,
    \sig_ip2bus_data_reg[22] ,
    sig_rx_channel_reset_reg,
    sig_tx_channel_reset_reg,
    sig_str_rst_reg_0,
    IP2Bus_WrAck_reg,
    sig_rd_rlen_reg,
    sig_txd_sb_wr_en_reg,
    s_axi_rdata,
    SR,
    s_axi_aclk,
    cs_ce_clr,
    sig_IP2Bus_Error,
    IPIC_STATE,
    \sig_register_array_reg[0][0]_0 ,
    \gaxi_full_sm.r_last_r_reg ,
    \gaxi_full_sm.r_last_r_reg_0 ,
    IP2Bus_Error1_in,
    \sig_register_array_reg[0][1]_1 ,
    \gaxi_full_sm.r_last_r_reg_1 ,
    sig_rx_channel_reset_reg_0,
    \sig_register_array_reg[0][2]_1 ,
    tx_fifo_or,
    s_axi_wdata,
    \sig_register_array_reg[0][3]_0 ,
    axi_str_txd_tready,
    \sig_register_array_reg[0][4]_0 ,
    p_13_in,
    \sig_register_array_reg[0][5]_0 ,
    \sig_register_array_reg[0][6]_0 ,
    sig_txd_reset,
    \sig_register_array_reg[0][7]_0 ,
    sig_rxd_reset,
    \sig_register_array_reg[0][8]_0 ,
    sig_txd_pf_event__1,
    \sig_register_array_reg[0][9]_0 ,
    sig_txd_pe_event__1,
    \sig_register_array_reg[0][10]_0 ,
    sig_rxd_pf_event__1,
    \sig_register_array_reg[0][11]_0 ,
    sig_rxd_pe_event__1,
    \sig_register_array_reg[0][12]_0 ,
    IP2Bus_RdAck_reg_0,
    s_axi_arvalid,
    IP2Bus_WrAck_reg_0,
    s_axi_awvalid,
    s_axi_wvalid,
    s_axi_araddr,
    s_axi_awaddr,
    \goreg_bm.dout_i_reg[0] ,
    eqOp__6,
    s_axi_aresetn,
    sig_tx_channel_reset_reg_0,
    \gtxd.sig_txd_packet_size_reg[30] ,
    \goreg_dm.dout_i_reg[21] ,
    \gfifo_gen.gmm2s.vacancy_i_reg[1] ,
    \gfifo_gen.gmm2s.vacancy_i_reg[2] ,
    Q,
    \gfifo_gen.gmm2s.vacancy_i_reg[3] ,
    \gfifo_gen.gmm2s.vacancy_i_reg[4] ,
    \gfifo_gen.gmm2s.vacancy_i_reg[5] ,
    \gfifo_gen.gmm2s.vacancy_i_reg[6] ,
    \gfifo_gen.gmm2s.vacancy_i_reg[7] ,
    \gfifo_gen.gmm2s.vacancy_i_reg[8] ,
    \gfifo_gen.gmm2s.vacancy_i_reg[9] ,
    out,
    sig_rx_channel_reset_reg_1,
    \sig_register_array_reg[1][10] ,
    s_axi_rready,
    s_axi_bready,
    sig_str_rst_reg_1,
    \sig_ip2bus_data_reg[0] );
  output [0:0]s_axi_rresp;
  output s_axi_rvalid;
  output s_axi_bvalid;
  output [0:0]s_axi_bresp;
  output sig_Bus2IP_CS;
  output \sig_register_array_reg[0][0] ;
  output \sig_register_array_reg[0][1] ;
  output \sig_register_array_reg[0][2] ;
  output \sig_register_array_reg[0][3] ;
  output \sig_register_array_reg[0][4] ;
  output \sig_register_array_reg[0][5] ;
  output \sig_register_array_reg[0][6] ;
  output \sig_register_array_reg[0][7] ;
  output \sig_register_array_reg[0][8] ;
  output \sig_register_array_reg[0][9] ;
  output \sig_register_array_reg[0][10] ;
  output \sig_register_array_reg[0][11] ;
  output \sig_register_array_reg[0][12] ;
  output sig_str_rst_reg;
  output [0:0]\sig_ip2bus_data_reg[31] ;
  output [0:0]\sig_txd_wr_data_reg[31] ;
  output IP2Bus_RdAck_reg;
  output [0:0]\sig_register_array_reg[1][0] ;
  output IP2Bus_Error;
  output \sig_register_array_reg[0][2]_0 ;
  output \sig_register_array_reg[0][1]_0 ;
  output [12:0]\sig_register_array_reg[1][0]_0 ;
  output [21:0]D;
  output \sig_ip2bus_data_reg[30] ;
  output \sig_ip2bus_data_reg[9] ;
  output \sig_ip2bus_data_reg[9]_0 ;
  output \sig_ip2bus_data_reg[22] ;
  output sig_rx_channel_reset_reg;
  output sig_tx_channel_reset_reg;
  output sig_str_rst_reg_0;
  output IP2Bus_WrAck_reg;
  output sig_rd_rlen_reg;
  output sig_txd_sb_wr_en_reg;
  output [31:0]s_axi_rdata;
  input [0:0]SR;
  input s_axi_aclk;
  input cs_ce_clr;
  input sig_IP2Bus_Error;
  input IPIC_STATE;
  input \sig_register_array_reg[0][0]_0 ;
  input \gaxi_full_sm.r_last_r_reg ;
  input \gaxi_full_sm.r_last_r_reg_0 ;
  input IP2Bus_Error1_in;
  input \sig_register_array_reg[0][1]_1 ;
  input \gaxi_full_sm.r_last_r_reg_1 ;
  input sig_rx_channel_reset_reg_0;
  input \sig_register_array_reg[0][2]_1 ;
  input tx_fifo_or;
  input [12:0]s_axi_wdata;
  input \sig_register_array_reg[0][3]_0 ;
  input axi_str_txd_tready;
  input \sig_register_array_reg[0][4]_0 ;
  input p_13_in;
  input \sig_register_array_reg[0][5]_0 ;
  input \sig_register_array_reg[0][6]_0 ;
  input sig_txd_reset;
  input \sig_register_array_reg[0][7]_0 ;
  input sig_rxd_reset;
  input \sig_register_array_reg[0][8]_0 ;
  input sig_txd_pf_event__1;
  input \sig_register_array_reg[0][9]_0 ;
  input sig_txd_pe_event__1;
  input \sig_register_array_reg[0][10]_0 ;
  input sig_rxd_pf_event__1;
  input \sig_register_array_reg[0][11]_0 ;
  input sig_rxd_pe_event__1;
  input \sig_register_array_reg[0][12]_0 ;
  input IP2Bus_RdAck_reg_0;
  input s_axi_arvalid;
  input IP2Bus_WrAck_reg_0;
  input s_axi_awvalid;
  input s_axi_wvalid;
  input [3:0]s_axi_araddr;
  input [3:0]s_axi_awaddr;
  input \goreg_bm.dout_i_reg[0] ;
  input eqOp__6;
  input s_axi_aresetn;
  input sig_tx_channel_reset_reg_0;
  input \gtxd.sig_txd_packet_size_reg[30] ;
  input [21:0]\goreg_dm.dout_i_reg[21] ;
  input [0:0]\gfifo_gen.gmm2s.vacancy_i_reg[1] ;
  input \gfifo_gen.gmm2s.vacancy_i_reg[2] ;
  input [9:0]Q;
  input \gfifo_gen.gmm2s.vacancy_i_reg[3] ;
  input \gfifo_gen.gmm2s.vacancy_i_reg[4] ;
  input \gfifo_gen.gmm2s.vacancy_i_reg[5] ;
  input \gfifo_gen.gmm2s.vacancy_i_reg[6] ;
  input \gfifo_gen.gmm2s.vacancy_i_reg[7] ;
  input \gfifo_gen.gmm2s.vacancy_i_reg[8] ;
  input \gfifo_gen.gmm2s.vacancy_i_reg[9] ;
  input out;
  input sig_rx_channel_reset_reg_1;
  input [2:0]\sig_register_array_reg[1][10] ;
  input s_axi_rready;
  input s_axi_bready;
  input sig_str_rst_reg_1;
  input [31:0]\sig_ip2bus_data_reg[0] ;

  wire [21:0]D;
  wire IP2Bus_Error;
  wire IP2Bus_Error1_in;
  wire IP2Bus_RdAck_reg;
  wire IP2Bus_RdAck_reg_0;
  wire IP2Bus_WrAck_reg;
  wire IP2Bus_WrAck_reg_0;
  wire IPIC_STATE;
  wire [9:0]Q;
  wire [0:0]SR;
  wire axi_str_txd_tready;
  wire cs_ce_clr;
  wire eqOp__6;
  wire \gaxi_full_sm.r_last_r_reg ;
  wire \gaxi_full_sm.r_last_r_reg_0 ;
  wire \gaxi_full_sm.r_last_r_reg_1 ;
  wire [0:0]\gfifo_gen.gmm2s.vacancy_i_reg[1] ;
  wire \gfifo_gen.gmm2s.vacancy_i_reg[2] ;
  wire \gfifo_gen.gmm2s.vacancy_i_reg[3] ;
  wire \gfifo_gen.gmm2s.vacancy_i_reg[4] ;
  wire \gfifo_gen.gmm2s.vacancy_i_reg[5] ;
  wire \gfifo_gen.gmm2s.vacancy_i_reg[6] ;
  wire \gfifo_gen.gmm2s.vacancy_i_reg[7] ;
  wire \gfifo_gen.gmm2s.vacancy_i_reg[8] ;
  wire \gfifo_gen.gmm2s.vacancy_i_reg[9] ;
  wire \goreg_bm.dout_i_reg[0] ;
  wire [21:0]\goreg_dm.dout_i_reg[21] ;
  wire \gtxd.sig_txd_packet_size_reg[30] ;
  wire out;
  wire p_13_in;
  wire s_axi_aclk;
  wire [3:0]s_axi_araddr;
  wire s_axi_aresetn;
  wire s_axi_arvalid;
  wire [3:0]s_axi_awaddr;
  wire s_axi_awvalid;
  wire s_axi_bready;
  wire [0:0]s_axi_bresp;
  wire s_axi_bvalid;
  wire [31:0]s_axi_rdata;
  wire s_axi_rready;
  wire [0:0]s_axi_rresp;
  wire s_axi_rvalid;
  wire [12:0]s_axi_wdata;
  wire s_axi_wvalid;
  wire sig_Bus2IP_CS;
  wire sig_IP2Bus_Error;
  wire [31:0]\sig_ip2bus_data_reg[0] ;
  wire \sig_ip2bus_data_reg[22] ;
  wire \sig_ip2bus_data_reg[30] ;
  wire [0:0]\sig_ip2bus_data_reg[31] ;
  wire \sig_ip2bus_data_reg[9] ;
  wire \sig_ip2bus_data_reg[9]_0 ;
  wire sig_rd_rlen_reg;
  wire \sig_register_array_reg[0][0] ;
  wire \sig_register_array_reg[0][0]_0 ;
  wire \sig_register_array_reg[0][10] ;
  wire \sig_register_array_reg[0][10]_0 ;
  wire \sig_register_array_reg[0][11] ;
  wire \sig_register_array_reg[0][11]_0 ;
  wire \sig_register_array_reg[0][12] ;
  wire \sig_register_array_reg[0][12]_0 ;
  wire \sig_register_array_reg[0][1] ;
  wire \sig_register_array_reg[0][1]_0 ;
  wire \sig_register_array_reg[0][1]_1 ;
  wire \sig_register_array_reg[0][2] ;
  wire \sig_register_array_reg[0][2]_0 ;
  wire \sig_register_array_reg[0][2]_1 ;
  wire \sig_register_array_reg[0][3] ;
  wire \sig_register_array_reg[0][3]_0 ;
  wire \sig_register_array_reg[0][4] ;
  wire \sig_register_array_reg[0][4]_0 ;
  wire \sig_register_array_reg[0][5] ;
  wire \sig_register_array_reg[0][5]_0 ;
  wire \sig_register_array_reg[0][6] ;
  wire \sig_register_array_reg[0][6]_0 ;
  wire \sig_register_array_reg[0][7] ;
  wire \sig_register_array_reg[0][7]_0 ;
  wire \sig_register_array_reg[0][8] ;
  wire \sig_register_array_reg[0][8]_0 ;
  wire \sig_register_array_reg[0][9] ;
  wire \sig_register_array_reg[0][9]_0 ;
  wire [0:0]\sig_register_array_reg[1][0] ;
  wire [12:0]\sig_register_array_reg[1][0]_0 ;
  wire [2:0]\sig_register_array_reg[1][10] ;
  wire sig_rx_channel_reset_reg;
  wire sig_rx_channel_reset_reg_0;
  wire sig_rx_channel_reset_reg_1;
  wire sig_rxd_pe_event__1;
  wire sig_rxd_pf_event__1;
  wire sig_rxd_reset;
  wire sig_str_rst_reg;
  wire sig_str_rst_reg_0;
  wire sig_str_rst_reg_1;
  wire sig_tx_channel_reset_reg;
  wire sig_tx_channel_reset_reg_0;
  wire sig_txd_pe_event__1;
  wire sig_txd_pf_event__1;
  wire sig_txd_reset;
  wire sig_txd_sb_wr_en_reg;
  wire [0:0]\sig_txd_wr_data_reg[31] ;
  wire tx_fifo_or;

  axi_fifo_mm_s_0_slave_attachment I_SLAVE_ATTACHMENT
       (.D(D),
        .IP2Bus_Error(IP2Bus_Error),
        .IP2Bus_Error1_in(IP2Bus_Error1_in),
        .IP2Bus_RdAck_reg(IP2Bus_RdAck_reg),
        .IP2Bus_RdAck_reg_0(IP2Bus_RdAck_reg_0),
        .IP2Bus_WrAck_reg(IP2Bus_WrAck_reg),
        .IP2Bus_WrAck_reg_0(IP2Bus_WrAck_reg_0),
        .IPIC_STATE(IPIC_STATE),
        .IPIC_STATE_reg(sig_Bus2IP_CS),
        .Q(Q),
        .SR(SR),
        .axi_str_txd_tready(axi_str_txd_tready),
        .cs_ce_clr(cs_ce_clr),
        .eqOp__6(eqOp__6),
        .\gaxi_full_sm.r_last_r_reg (\gaxi_full_sm.r_last_r_reg ),
        .\gaxi_full_sm.r_last_r_reg_0 (\gaxi_full_sm.r_last_r_reg_0 ),
        .\gaxi_full_sm.r_last_r_reg_1 (\gaxi_full_sm.r_last_r_reg_1 ),
        .\gfifo_gen.gmm2s.vacancy_i_reg[1] (\gfifo_gen.gmm2s.vacancy_i_reg[1] ),
        .\gfifo_gen.gmm2s.vacancy_i_reg[2] (\gfifo_gen.gmm2s.vacancy_i_reg[2] ),
        .\gfifo_gen.gmm2s.vacancy_i_reg[3] (\gfifo_gen.gmm2s.vacancy_i_reg[3] ),
        .\gfifo_gen.gmm2s.vacancy_i_reg[4] (\gfifo_gen.gmm2s.vacancy_i_reg[4] ),
        .\gfifo_gen.gmm2s.vacancy_i_reg[5] (\gfifo_gen.gmm2s.vacancy_i_reg[5] ),
        .\gfifo_gen.gmm2s.vacancy_i_reg[6] (\gfifo_gen.gmm2s.vacancy_i_reg[6] ),
        .\gfifo_gen.gmm2s.vacancy_i_reg[7] (\gfifo_gen.gmm2s.vacancy_i_reg[7] ),
        .\gfifo_gen.gmm2s.vacancy_i_reg[8] (\gfifo_gen.gmm2s.vacancy_i_reg[8] ),
        .\gfifo_gen.gmm2s.vacancy_i_reg[9] (\gfifo_gen.gmm2s.vacancy_i_reg[9] ),
        .\goreg_bm.dout_i_reg[0] (\goreg_bm.dout_i_reg[0] ),
        .\goreg_dm.dout_i_reg[21] (\goreg_dm.dout_i_reg[21] ),
        .\gtxd.sig_txd_packet_size_reg[30] (\gtxd.sig_txd_packet_size_reg[30] ),
        .out(out),
        .p_13_in(p_13_in),
        .s_axi_aclk(s_axi_aclk),
        .s_axi_araddr(s_axi_araddr),
        .s_axi_aresetn(s_axi_aresetn),
        .s_axi_arvalid(s_axi_arvalid),
        .s_axi_awaddr(s_axi_awaddr),
        .s_axi_awvalid(s_axi_awvalid),
        .s_axi_bready(s_axi_bready),
        .s_axi_bresp(s_axi_bresp),
        .s_axi_bvalid(s_axi_bvalid),
        .s_axi_rdata(s_axi_rdata),
        .s_axi_rready(s_axi_rready),
        .s_axi_rresp(s_axi_rresp),
        .s_axi_rvalid(s_axi_rvalid),
        .s_axi_wdata(s_axi_wdata),
        .s_axi_wvalid(s_axi_wvalid),
        .sig_IP2Bus_Error(sig_IP2Bus_Error),
        .\sig_ip2bus_data_reg[0] (\sig_ip2bus_data_reg[0] ),
        .\sig_ip2bus_data_reg[22] (\sig_ip2bus_data_reg[22] ),
        .\sig_ip2bus_data_reg[30] (\sig_ip2bus_data_reg[30] ),
        .\sig_ip2bus_data_reg[31] (\sig_ip2bus_data_reg[31] ),
        .\sig_ip2bus_data_reg[9] (\sig_ip2bus_data_reg[9] ),
        .\sig_ip2bus_data_reg[9]_0 (\sig_ip2bus_data_reg[9]_0 ),
        .sig_rd_rlen_reg(sig_rd_rlen_reg),
        .\sig_register_array_reg[0][0] (\sig_register_array_reg[0][0] ),
        .\sig_register_array_reg[0][0]_0 (\sig_register_array_reg[0][0]_0 ),
        .\sig_register_array_reg[0][10] (\sig_register_array_reg[0][10] ),
        .\sig_register_array_reg[0][10]_0 (\sig_register_array_reg[0][10]_0 ),
        .\sig_register_array_reg[0][11] (\sig_register_array_reg[0][11] ),
        .\sig_register_array_reg[0][11]_0 (\sig_register_array_reg[0][11]_0 ),
        .\sig_register_array_reg[0][12] (\sig_register_array_reg[0][12] ),
        .\sig_register_array_reg[0][12]_0 (\sig_register_array_reg[0][12]_0 ),
        .\sig_register_array_reg[0][1] (\sig_register_array_reg[0][1] ),
        .\sig_register_array_reg[0][1]_0 (\sig_register_array_reg[0][1]_0 ),
        .\sig_register_array_reg[0][1]_1 (\sig_register_array_reg[0][1]_1 ),
        .\sig_register_array_reg[0][2] (\sig_register_array_reg[0][2] ),
        .\sig_register_array_reg[0][2]_0 (\sig_register_array_reg[0][2]_0 ),
        .\sig_register_array_reg[0][2]_1 (\sig_register_array_reg[0][2]_1 ),
        .\sig_register_array_reg[0][3] (\sig_register_array_reg[0][3] ),
        .\sig_register_array_reg[0][3]_0 (\sig_register_array_reg[0][3]_0 ),
        .\sig_register_array_reg[0][4] (\sig_register_array_reg[0][4] ),
        .\sig_register_array_reg[0][4]_0 (\sig_register_array_reg[0][4]_0 ),
        .\sig_register_array_reg[0][5] (\sig_register_array_reg[0][5] ),
        .\sig_register_array_reg[0][5]_0 (\sig_register_array_reg[0][5]_0 ),
        .\sig_register_array_reg[0][6] (\sig_register_array_reg[0][6] ),
        .\sig_register_array_reg[0][6]_0 (\sig_register_array_reg[0][6]_0 ),
        .\sig_register_array_reg[0][7] (\sig_register_array_reg[0][7] ),
        .\sig_register_array_reg[0][7]_0 (\sig_register_array_reg[0][7]_0 ),
        .\sig_register_array_reg[0][8] (\sig_register_array_reg[0][8] ),
        .\sig_register_array_reg[0][8]_0 (\sig_register_array_reg[0][8]_0 ),
        .\sig_register_array_reg[0][9] (\sig_register_array_reg[0][9] ),
        .\sig_register_array_reg[0][9]_0 (\sig_register_array_reg[0][9]_0 ),
        .\sig_register_array_reg[1][0] (\sig_register_array_reg[1][0] ),
        .\sig_register_array_reg[1][0]_0 (\sig_register_array_reg[1][0]_0 ),
        .\sig_register_array_reg[1][10] (\sig_register_array_reg[1][10] ),
        .sig_rx_channel_reset_reg(sig_rx_channel_reset_reg),
        .sig_rx_channel_reset_reg_0(sig_rx_channel_reset_reg_0),
        .sig_rx_channel_reset_reg_1(sig_rx_channel_reset_reg_1),
        .sig_rxd_pe_event__1(sig_rxd_pe_event__1),
        .sig_rxd_pf_event__1(sig_rxd_pf_event__1),
        .sig_rxd_reset(sig_rxd_reset),
        .sig_str_rst_reg(sig_str_rst_reg),
        .sig_str_rst_reg_0(sig_str_rst_reg_0),
        .sig_str_rst_reg_1(sig_str_rst_reg_1),
        .sig_tx_channel_reset_reg(sig_tx_channel_reset_reg),
        .sig_tx_channel_reset_reg_0(sig_tx_channel_reset_reg_0),
        .sig_txd_pe_event__1(sig_txd_pe_event__1),
        .sig_txd_pf_event__1(sig_txd_pf_event__1),
        .sig_txd_reset(sig_txd_reset),
        .sig_txd_sb_wr_en_reg(sig_txd_sb_wr_en_reg),
        .\sig_txd_wr_data_reg[31] (\sig_txd_wr_data_reg[31] ),
        .tx_fifo_or(tx_fifo_or));
endmodule

(* ORIG_REF_NAME = "axi_read_fsm" *) 
module axi_fifo_mm_s_0_axi_read_fsm
   (s_axi4_arready,
    s_axi4_rvalid,
    s_axi4_rlast,
    D,
    E,
    \grid.S_AXI_RID_reg[3] ,
    \sig_register_array_reg[0][2] ,
    \gc0.count_reg[0] ,
    \gc0.count_reg[0]_0 ,
    \sig_register_array_reg[0][2]_0 ,
    \sig_register_array_reg[0][1] ,
    \sig_register_array_reg[0][1]_0 ,
    axis_rd_en__0,
    axi4_fifo_rd_en_i,
    \grid.ar_id_r_reg[0] ,
    \grid.ar_id_r_reg[0]_0 ,
    \gaxi_full_sm.arlen_cntr_reg[0] ,
    \gaxi_full_sm.arlen_cntr_reg[0]_0 ,
    \grid.ar_id_r_reg[0]_1 ,
    s_axi_aclk,
    AS,
    s_axi4_rready,
    Q,
    \gaxi_full_sm.arlen_cntr_reg[4] ,
    \s_axi4_arlen_5__s_port_] ,
    s_axi4_arlen,
    \gaxi_full_sm.arlen_cntr_reg[3] ,
    s_axi4_arlen_4__s_port_,
    \gaxi_full_sm.arlen_cntr_reg[2] ,
    s_axi4_arlen_0__s_port_,
    \gaxi_full_sm.arlen_cntr_reg[1] ,
    s_axi4_arlen_2__s_port_,
    \gaxi_full_sm.arlen_cntr_reg[0]_1 ,
    \s_axi4_arlen[0]_0 ,
    \gaxi_full_sm.arlen_cntr_reg[6] ,
    \s_axi4_arlen[0]_1 ,
    r_last_int_c__7,
    s_axi4_arid,
    \grid.ar_id_r_reg[3] ,
    \count_reg[9] ,
    Bus_RNW_reg_reg,
    Bus_RNW_reg_reg_0,
    sig_rx_channel_reset_reg,
    \count_reg[8] ,
    sig_rx_channel_reset_reg_0,
    Bus_RNW_reg_reg_1,
    empty_fwft_i,
    \s_axi4_araddr_25__s_port_] ,
    s_axi4_araddr,
    s_axi4_araddr_2__s_port_,
    s_axi4_arvalid);
  output s_axi4_arready;
  output s_axi4_rvalid;
  output s_axi4_rlast;
  output [7:0]D;
  output [0:0]E;
  output [3:0]\grid.S_AXI_RID_reg[3] ;
  output \sig_register_array_reg[0][2] ;
  output \gc0.count_reg[0] ;
  output \gc0.count_reg[0]_0 ;
  output \sig_register_array_reg[0][2]_0 ;
  output \sig_register_array_reg[0][1] ;
  output \sig_register_array_reg[0][1]_0 ;
  output axis_rd_en__0;
  output axi4_fifo_rd_en_i;
  output \grid.ar_id_r_reg[0] ;
  output \grid.ar_id_r_reg[0]_0 ;
  output \gaxi_full_sm.arlen_cntr_reg[0] ;
  output [0:0]\gaxi_full_sm.arlen_cntr_reg[0]_0 ;
  output [0:0]\grid.ar_id_r_reg[0]_1 ;
  input s_axi_aclk;
  input [0:0]AS;
  input s_axi4_rready;
  input [7:0]Q;
  input \gaxi_full_sm.arlen_cntr_reg[4] ;
  input \s_axi4_arlen_5__s_port_] ;
  input [7:0]s_axi4_arlen;
  input \gaxi_full_sm.arlen_cntr_reg[3] ;
  input s_axi4_arlen_4__s_port_;
  input \gaxi_full_sm.arlen_cntr_reg[2] ;
  input s_axi4_arlen_0__s_port_;
  input \gaxi_full_sm.arlen_cntr_reg[1] ;
  input s_axi4_arlen_2__s_port_;
  input \gaxi_full_sm.arlen_cntr_reg[0]_1 ;
  input \s_axi4_arlen[0]_0 ;
  input \gaxi_full_sm.arlen_cntr_reg[6] ;
  input \s_axi4_arlen[0]_1 ;
  input r_last_int_c__7;
  input [3:0]s_axi4_arid;
  input [3:0]\grid.ar_id_r_reg[3] ;
  input \count_reg[9] ;
  input Bus_RNW_reg_reg;
  input Bus_RNW_reg_reg_0;
  input sig_rx_channel_reset_reg;
  input \count_reg[8] ;
  input sig_rx_channel_reset_reg_0;
  input Bus_RNW_reg_reg_1;
  input empty_fwft_i;
  input \s_axi4_araddr_25__s_port_] ;
  input [31:0]s_axi4_araddr;
  input s_axi4_araddr_2__s_port_;
  input s_axi4_arvalid;

  wire [0:0]AS;
  wire Bus_RNW_reg_reg;
  wire Bus_RNW_reg_reg_0;
  wire Bus_RNW_reg_reg_1;
  wire [7:0]D;
  wire [0:0]E;
  wire [7:0]Q;
  wire addr_en_c;
  wire ar_ready_c;
  wire axi4_fifo_rd_en_i;
  wire axi4_rdfd_en;
  wire axis_rd_en__0;
  wire \count_reg[8] ;
  wire \count_reg[9] ;
  wire empty_fwft_i;
  wire \gaxi_full_sm.ar_ready_r_i_2_n_0 ;
  wire \gaxi_full_sm.ar_ready_r_i_3_n_0 ;
  wire \gaxi_full_sm.ar_ready_r_i_5_n_0 ;
  wire \gaxi_full_sm.ar_ready_r_i_7_n_0 ;
  wire \gaxi_full_sm.arlen_cntr_reg[0] ;
  wire [0:0]\gaxi_full_sm.arlen_cntr_reg[0]_0 ;
  wire \gaxi_full_sm.arlen_cntr_reg[0]_1 ;
  wire \gaxi_full_sm.arlen_cntr_reg[1] ;
  wire \gaxi_full_sm.arlen_cntr_reg[2] ;
  wire \gaxi_full_sm.arlen_cntr_reg[3] ;
  wire \gaxi_full_sm.arlen_cntr_reg[4] ;
  wire \gaxi_full_sm.arlen_cntr_reg[6] ;
  wire \gaxi_full_sm.outstanding_read_r_i_1_n_0 ;
  wire \gaxi_full_sm.present_state[0]_i_1_n_0 ;
  wire \gaxi_full_sm.present_state[1]_i_1_n_0 ;
  wire \gaxi_full_sm.present_state[1]_i_2_n_0 ;
  wire \gaxi_full_sm.present_state[1]_i_3_n_0 ;
  wire \gaxi_full_sm.present_state[1]_i_4_n_0 ;
  wire \gaxi_full_sm.r_last_r_i_1_n_0 ;
  wire \gaxi_full_sm.r_last_r_i_2_n_0 ;
  wire \gaxi_full_sm.r_last_r_i_3_n_0 ;
  wire \gaxi_full_sm.r_last_r_i_4_n_0 ;
  wire \gaxi_full_sm.r_valid_r_i_1_n_0 ;
  wire \gaxi_full_sm.r_valid_r_i_2_n_0 ;
  wire \gaxi_full_sm.r_valid_r_i_3_n_0 ;
  wire \gc0.count_reg[0] ;
  wire \gc0.count_reg[0]_0 ;
  wire \greg.ram_rd_en_i_i_5_n_0 ;
  wire \grid.S_AXI_RID[3]_i_10_n_0 ;
  wire \grid.S_AXI_RID[3]_i_5_n_0 ;
  wire \grid.S_AXI_RID[3]_i_6_n_0 ;
  wire \grid.S_AXI_RID[3]_i_7_n_0 ;
  wire \grid.S_AXI_RID[3]_i_8_n_0 ;
  wire \grid.S_AXI_RID[3]_i_9_n_0 ;
  wire [3:0]\grid.S_AXI_RID_reg[3] ;
  wire \grid.ar_id_r_reg[0] ;
  wire \grid.ar_id_r_reg[0]_0 ;
  wire [0:0]\grid.ar_id_r_reg[0]_1 ;
  wire [3:0]\grid.ar_id_r_reg[3] ;
  wire outstanding_read_r;
  wire p_0_in;
  wire p_6_out__0;
  wire p_7_out__0;
  wire pipeline_full_c10_out__0;
  wire [1:0]present_state;
  wire r_last_c11_out__0;
  wire r_last_int_c__7;
  wire [31:0]s_axi4_araddr;
  wire s_axi4_araddr_25__s_net_1;
  wire s_axi4_araddr_2__s_net_1;
  wire [3:0]s_axi4_arid;
  wire [7:0]s_axi4_arlen;
  wire \s_axi4_arlen[0]_0 ;
  wire \s_axi4_arlen[0]_1 ;
  wire s_axi4_arlen_0__s_net_1;
  wire s_axi4_arlen_2__s_net_1;
  wire s_axi4_arlen_4__s_net_1;
  wire s_axi4_arlen_5__s_net_1;
  wire s_axi4_arready;
  wire s_axi4_arvalid;
  wire s_axi4_rlast;
  wire s_axi4_rready;
  wire s_axi4_rvalid;
  wire s_axi_aclk;
  wire \sig_register_array_reg[0][1] ;
  wire \sig_register_array_reg[0][1]_0 ;
  wire \sig_register_array_reg[0][2] ;
  wire \sig_register_array_reg[0][2]_0 ;
  wire sig_rx_channel_reset_reg;
  wire sig_rx_channel_reset_reg_0;

  assign s_axi4_araddr_25__s_net_1 = \s_axi4_araddr_25__s_port_] ;
  assign s_axi4_araddr_2__s_net_1 = s_axi4_araddr_2__s_port_;
  assign s_axi4_arlen_0__s_net_1 = s_axi4_arlen_0__s_port_;
  assign s_axi4_arlen_2__s_net_1 = s_axi4_arlen_2__s_port_;
  assign s_axi4_arlen_4__s_net_1 = s_axi4_arlen_4__s_port_;
  assign s_axi4_arlen_5__s_net_1 = \s_axi4_arlen_5__s_port_] ;
  LUT6 #(
    .INIT(64'hB8BBB8B8B8B8B8B8)) 
    \gaxi_full_sm.ar_ready_r_i_1 
       (.I0(\gaxi_full_sm.ar_ready_r_i_2_n_0 ),
        .I1(present_state[0]),
        .I2(\gaxi_full_sm.ar_ready_r_i_3_n_0 ),
        .I3(present_state[1]),
        .I4(p_0_in),
        .I5(s_axi4_rready),
        .O(ar_ready_c));
  LUT6 #(
    .INIT(64'hFFFFB0BB0000B0BB)) 
    \gaxi_full_sm.ar_ready_r_i_2 
       (.I0(\gaxi_full_sm.ar_ready_r_i_5_n_0 ),
        .I1(axi4_rdfd_en),
        .I2(s_axi4_rready),
        .I3(s_axi4_rvalid),
        .I4(present_state[1]),
        .I5(r_last_c11_out__0),
        .O(\gaxi_full_sm.ar_ready_r_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair108" *) 
  LUT5 #(
    .INIT(32'h450045FF)) 
    \gaxi_full_sm.ar_ready_r_i_3 
       (.I0(outstanding_read_r),
        .I1(s_axi4_rready),
        .I2(s_axi4_rvalid),
        .I3(present_state[1]),
        .I4(axi4_rdfd_en),
        .O(\gaxi_full_sm.ar_ready_r_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0000000200000000)) 
    \gaxi_full_sm.ar_ready_r_i_4 
       (.I0(\gaxi_full_sm.ar_ready_r_i_7_n_0 ),
        .I1(s_axi4_arlen[7]),
        .I2(s_axi4_arlen[6]),
        .I3(s_axi4_arlen[0]),
        .I4(s_axi4_arlen[1]),
        .I5(axi4_rdfd_en),
        .O(p_0_in));
  LUT5 #(
    .INIT(32'h00010000)) 
    \gaxi_full_sm.ar_ready_r_i_5 
       (.I0(s_axi4_arlen[1]),
        .I1(s_axi4_arlen[0]),
        .I2(s_axi4_arlen[6]),
        .I3(s_axi4_arlen[7]),
        .I4(\gaxi_full_sm.ar_ready_r_i_7_n_0 ),
        .O(\gaxi_full_sm.ar_ready_r_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h0100000001000100)) 
    \gaxi_full_sm.ar_ready_r_i_6 
       (.I0(Q[2]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(\gaxi_full_sm.arlen_cntr_reg[0] ),
        .I4(s_axi4_rready),
        .I5(s_axi4_rvalid),
        .O(r_last_c11_out__0));
  LUT4 #(
    .INIT(16'h0001)) 
    \gaxi_full_sm.ar_ready_r_i_7 
       (.I0(s_axi4_arlen[3]),
        .I1(s_axi4_arlen[2]),
        .I2(s_axi4_arlen[5]),
        .I3(s_axi4_arlen[4]),
        .O(\gaxi_full_sm.ar_ready_r_i_7_n_0 ));
  FDCE #(
    .INIT(1'b0)) 
    \gaxi_full_sm.ar_ready_r_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .CLR(AS),
        .D(ar_ready_c),
        .Q(s_axi4_arready));
  LUT4 #(
    .INIT(16'h0FD1)) 
    \gaxi_full_sm.arlen_cntr[0]_i_1 
       (.I0(Q[0]),
        .I1(addr_en_c),
        .I2(s_axi4_arlen[0]),
        .I3(p_7_out__0),
        .O(D[0]));
  LUT6 #(
    .INIT(64'hFFF900F90009FF09)) 
    \gaxi_full_sm.arlen_cntr[1]_i_1 
       (.I0(Q[1]),
        .I1(Q[0]),
        .I2(addr_en_c),
        .I3(p_7_out__0),
        .I4(s_axi4_arlen[0]),
        .I5(s_axi4_arlen[1]),
        .O(D[1]));
  LUT6 #(
    .INIT(64'hFFF900F90009FF09)) 
    \gaxi_full_sm.arlen_cntr[2]_i_1 
       (.I0(Q[2]),
        .I1(\gaxi_full_sm.arlen_cntr_reg[0]_1 ),
        .I2(addr_en_c),
        .I3(p_7_out__0),
        .I4(\s_axi4_arlen[0]_0 ),
        .I5(s_axi4_arlen[2]),
        .O(D[2]));
  LUT6 #(
    .INIT(64'h00F9FF09FFF90009)) 
    \gaxi_full_sm.arlen_cntr[3]_i_1 
       (.I0(Q[3]),
        .I1(\gaxi_full_sm.arlen_cntr_reg[1] ),
        .I2(addr_en_c),
        .I3(p_7_out__0),
        .I4(s_axi4_arlen[3]),
        .I5(s_axi4_arlen_2__s_net_1),
        .O(D[3]));
  LUT6 #(
    .INIT(64'hFFF900F90009FF09)) 
    \gaxi_full_sm.arlen_cntr[4]_i_1 
       (.I0(Q[4]),
        .I1(\gaxi_full_sm.arlen_cntr_reg[2] ),
        .I2(addr_en_c),
        .I3(p_7_out__0),
        .I4(s_axi4_arlen_0__s_net_1),
        .I5(s_axi4_arlen[4]),
        .O(D[4]));
  LUT6 #(
    .INIT(64'hFFF900F90009FF09)) 
    \gaxi_full_sm.arlen_cntr[5]_i_1 
       (.I0(Q[5]),
        .I1(\gaxi_full_sm.arlen_cntr_reg[3] ),
        .I2(addr_en_c),
        .I3(p_7_out__0),
        .I4(s_axi4_arlen_4__s_net_1),
        .I5(s_axi4_arlen[5]),
        .O(D[5]));
  LUT6 #(
    .INIT(64'hFFED00ED0021FF21)) 
    \gaxi_full_sm.arlen_cntr[6]_i_1 
       (.I0(Q[6]),
        .I1(addr_en_c),
        .I2(\gaxi_full_sm.arlen_cntr_reg[4] ),
        .I3(p_7_out__0),
        .I4(s_axi4_arlen_5__s_net_1),
        .I5(s_axi4_arlen[6]),
        .O(D[6]));
  LUT5 #(
    .INIT(32'hFFFFFF20)) 
    \gaxi_full_sm.arlen_cntr[7]_i_1 
       (.I0(present_state[0]),
        .I1(pipeline_full_c10_out__0),
        .I2(present_state[1]),
        .I3(r_last_int_c__7),
        .I4(addr_en_c),
        .O(\gaxi_full_sm.arlen_cntr_reg[0]_0 ));
  LUT6 #(
    .INIT(64'hFFED00ED0021FF21)) 
    \gaxi_full_sm.arlen_cntr[7]_i_2 
       (.I0(Q[7]),
        .I1(addr_en_c),
        .I2(\gaxi_full_sm.arlen_cntr_reg[6] ),
        .I3(p_7_out__0),
        .I4(\s_axi4_arlen[0]_1 ),
        .I5(s_axi4_arlen[7]),
        .O(D[7]));
  LUT6 #(
    .INIT(64'h00000F0000000700)) 
    \gaxi_full_sm.arlen_cntr[7]_i_5 
       (.I0(present_state[0]),
        .I1(pipeline_full_c10_out__0),
        .I2(present_state[1]),
        .I3(axi4_rdfd_en),
        .I4(\gaxi_full_sm.ar_ready_r_i_5_n_0 ),
        .I5(r_last_int_c__7),
        .O(p_7_out__0));
  LUT6 #(
    .INIT(64'h0000000100000000)) 
    \gaxi_full_sm.arlen_cntr[7]_i_7 
       (.I0(Q[3]),
        .I1(Q[4]),
        .I2(Q[5]),
        .I3(Q[6]),
        .I4(Q[7]),
        .I5(s_axi4_rready),
        .O(\gaxi_full_sm.arlen_cntr_reg[0] ));
  LUT6 #(
    .INIT(64'h88F0000088000000)) 
    \gaxi_full_sm.outstanding_read_r_i_1 
       (.I0(addr_en_c),
        .I1(p_0_in),
        .I2(present_state[1]),
        .I3(present_state[0]),
        .I4(pipeline_full_c10_out__0),
        .I5(outstanding_read_r),
        .O(\gaxi_full_sm.outstanding_read_r_i_1_n_0 ));
  FDCE #(
    .INIT(1'b0)) 
    \gaxi_full_sm.outstanding_read_r_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .CLR(AS),
        .D(\gaxi_full_sm.outstanding_read_r_i_1_n_0 ),
        .Q(outstanding_read_r));
  LUT6 #(
    .INIT(64'hCCCCF4FFCCCC0000)) 
    \gaxi_full_sm.present_state[0]_i_1 
       (.I0(s_axi4_rvalid),
        .I1(present_state[0]),
        .I2(s_axi4_rready),
        .I3(p_0_in),
        .I4(present_state[1]),
        .I5(axi4_rdfd_en),
        .O(\gaxi_full_sm.present_state[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFF5555FF0C4444)) 
    \gaxi_full_sm.present_state[1]_i_1 
       (.I0(\gaxi_full_sm.present_state[1]_i_2_n_0 ),
        .I1(addr_en_c),
        .I2(p_0_in),
        .I3(\gaxi_full_sm.present_state[1]_i_3_n_0 ),
        .I4(present_state[0]),
        .I5(\gaxi_full_sm.present_state[1]_i_4_n_0 ),
        .O(\gaxi_full_sm.present_state[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair107" *) 
  LUT4 #(
    .INIT(16'h0080)) 
    \gaxi_full_sm.present_state[1]_i_2 
       (.I0(s_axi4_rready),
        .I1(\gaxi_full_sm.ar_ready_r_i_5_n_0 ),
        .I2(axi4_rdfd_en),
        .I3(present_state[1]),
        .O(\gaxi_full_sm.present_state[1]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair114" *) 
  LUT4 #(
    .INIT(16'h4474)) 
    \gaxi_full_sm.present_state[1]_i_3 
       (.I0(r_last_c11_out__0),
        .I1(present_state[1]),
        .I2(s_axi4_rvalid),
        .I3(s_axi4_rready),
        .O(\gaxi_full_sm.present_state[1]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair109" *) 
  LUT5 #(
    .INIT(32'h0000F200)) 
    \gaxi_full_sm.present_state[1]_i_4 
       (.I0(s_axi4_rvalid),
        .I1(s_axi4_rready),
        .I2(outstanding_read_r),
        .I3(present_state[1]),
        .I4(present_state[0]),
        .O(\gaxi_full_sm.present_state[1]_i_4_n_0 ));
  FDCE #(
    .INIT(1'b0)) 
    \gaxi_full_sm.present_state_reg[0] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .CLR(AS),
        .D(\gaxi_full_sm.present_state[0]_i_1_n_0 ),
        .Q(present_state[0]));
  FDCE #(
    .INIT(1'b0)) 
    \gaxi_full_sm.present_state_reg[1] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .CLR(AS),
        .D(\gaxi_full_sm.present_state[1]_i_1_n_0 ),
        .Q(present_state[1]));
  LUT6 #(
    .INIT(64'hFFFFFFF80000FFF8)) 
    \gaxi_full_sm.r_last_r_i_1 
       (.I0(\gaxi_full_sm.r_last_r_i_2_n_0 ),
        .I1(p_0_in),
        .I2(\gaxi_full_sm.r_last_r_i_3_n_0 ),
        .I3(\gaxi_full_sm.r_last_r_i_4_n_0 ),
        .I4(pipeline_full_c10_out__0),
        .I5(s_axi4_rlast),
        .O(\gaxi_full_sm.r_last_r_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hA2AA)) 
    \gaxi_full_sm.r_last_r_i_2 
       (.I0(addr_en_c),
        .I1(s_axi4_rvalid),
        .I2(s_axi4_rready),
        .I3(present_state[0]),
        .O(\gaxi_full_sm.r_last_r_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair109" *) 
  LUT5 #(
    .INIT(32'h0000D000)) 
    \gaxi_full_sm.r_last_r_i_3 
       (.I0(s_axi4_rvalid),
        .I1(s_axi4_rready),
        .I2(outstanding_read_r),
        .I3(present_state[1]),
        .I4(present_state[0]),
        .O(\gaxi_full_sm.r_last_r_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair114" *) 
  LUT3 #(
    .INIT(8'h80)) 
    \gaxi_full_sm.r_last_r_i_4 
       (.I0(present_state[1]),
        .I1(present_state[0]),
        .I2(r_last_c11_out__0),
        .O(\gaxi_full_sm.r_last_r_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair112" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \gaxi_full_sm.r_last_r_i_5 
       (.I0(s_axi4_rvalid),
        .I1(s_axi4_rready),
        .O(pipeline_full_c10_out__0));
  FDCE #(
    .INIT(1'b0)) 
    \gaxi_full_sm.r_last_r_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .CLR(AS),
        .D(\gaxi_full_sm.r_last_r_i_1_n_0 ),
        .Q(s_axi4_rlast));
  LUT6 #(
    .INIT(64'hBBB8FFFFBBB8BBB8)) 
    \gaxi_full_sm.r_valid_r_i_1 
       (.I0(\gaxi_full_sm.r_valid_r_i_2_n_0 ),
        .I1(present_state[0]),
        .I2(\gaxi_full_sm.r_valid_r_i_3_n_0 ),
        .I3(addr_en_c),
        .I4(s_axi4_rready),
        .I5(s_axi4_rvalid),
        .O(\gaxi_full_sm.r_valid_r_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair107" *) 
  LUT5 #(
    .INIT(32'hF3F3FB00)) 
    \gaxi_full_sm.r_valid_r_i_2 
       (.I0(\gaxi_full_sm.ar_ready_r_i_5_n_0 ),
        .I1(s_axi4_rvalid),
        .I2(s_axi4_rready),
        .I3(axi4_rdfd_en),
        .I4(present_state[1]),
        .O(\gaxi_full_sm.r_valid_r_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair108" *) 
  LUT4 #(
    .INIT(16'hD000)) 
    \gaxi_full_sm.r_valid_r_i_3 
       (.I0(s_axi4_rvalid),
        .I1(s_axi4_rready),
        .I2(outstanding_read_r),
        .I3(present_state[1]),
        .O(\gaxi_full_sm.r_valid_r_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair111" *) 
  LUT5 #(
    .INIT(32'h00001000)) 
    \gaxi_full_sm.r_valid_r_i_4 
       (.I0(\grid.S_AXI_RID[3]_i_8_n_0 ),
        .I1(\grid.S_AXI_RID[3]_i_7_n_0 ),
        .I2(\grid.S_AXI_RID[3]_i_6_n_0 ),
        .I3(\grid.S_AXI_RID[3]_i_5_n_0 ),
        .I4(present_state[1]),
        .O(addr_en_c));
  FDCE #(
    .INIT(1'b0)) 
    \gaxi_full_sm.r_valid_r_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .CLR(AS),
        .D(\gaxi_full_sm.r_valid_r_i_1_n_0 ),
        .Q(s_axi4_rvalid));
  (* SOFT_HLUTNM = "soft_lutpair110" *) 
  LUT5 #(
    .INIT(32'h0000E000)) 
    \goreg_bm.dout_i[40]_i_2 
       (.I0(\gc0.count_reg[0] ),
        .I1(s_axi4_rlast),
        .I2(s_axi4_rvalid),
        .I3(s_axi4_rready),
        .I4(empty_fwft_i),
        .O(axis_rd_en__0));
  (* SOFT_HLUTNM = "soft_lutpair113" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \greg.ram_rd_en_i_i_2 
       (.I0(s_axi4_rvalid),
        .I1(s_axi4_rready),
        .O(\gc0.count_reg[0]_0 ));
  LUT6 #(
    .INIT(64'h0E000E000E5F0E00)) 
    \greg.ram_rd_en_i_i_3 
       (.I0(present_state[0]),
        .I1(outstanding_read_r),
        .I2(pipeline_full_c10_out__0),
        .I3(present_state[1]),
        .I4(\greg.ram_rd_en_i_i_5_n_0 ),
        .I5(s_axi4_araddr_25__s_net_1),
        .O(\gc0.count_reg[0] ));
  LUT4 #(
    .INIT(16'h8000)) 
    \greg.ram_rd_en_i_i_5 
       (.I0(\grid.S_AXI_RID[3]_i_10_n_0 ),
        .I1(s_axi4_araddr_2__s_net_1),
        .I2(\grid.S_AXI_RID[3]_i_9_n_0 ),
        .I3(\grid.S_AXI_RID[3]_i_5_n_0 ),
        .O(\greg.ram_rd_en_i_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair115" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \grid.S_AXI_RID[0]_i_1 
       (.I0(s_axi4_arid[0]),
        .I1(p_6_out__0),
        .I2(\grid.ar_id_r_reg[3] [0]),
        .O(\grid.S_AXI_RID_reg[3] [0]));
  (* SOFT_HLUTNM = "soft_lutpair116" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \grid.S_AXI_RID[1]_i_1 
       (.I0(s_axi4_arid[1]),
        .I1(p_6_out__0),
        .I2(\grid.ar_id_r_reg[3] [1]),
        .O(\grid.S_AXI_RID_reg[3] [1]));
  (* SOFT_HLUTNM = "soft_lutpair116" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \grid.S_AXI_RID[2]_i_1 
       (.I0(s_axi4_arid[2]),
        .I1(p_6_out__0),
        .I2(\grid.ar_id_r_reg[3] [2]),
        .O(\grid.S_AXI_RID_reg[3] [2]));
  LUT6 #(
    .INIT(64'hEE0EEE0EEE2E2222)) 
    \grid.S_AXI_RID[3]_i_1 
       (.I0(axi4_rdfd_en),
        .I1(present_state[1]),
        .I2(s_axi4_rvalid),
        .I3(s_axi4_rready),
        .I4(outstanding_read_r),
        .I5(present_state[0]),
        .O(E));
  LUT4 #(
    .INIT(16'h0008)) 
    \grid.S_AXI_RID[3]_i_10 
       (.I0(s_axi4_arready),
        .I1(s_axi4_arvalid),
        .I2(s_axi4_araddr[1]),
        .I3(s_axi4_araddr[0]),
        .O(\grid.S_AXI_RID[3]_i_10_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \grid.S_AXI_RID[3]_i_11 
       (.I0(s_axi4_araddr[26]),
        .I1(s_axi4_araddr[27]),
        .I2(s_axi4_araddr[28]),
        .I3(s_axi4_araddr[29]),
        .O(\grid.ar_id_r_reg[0] ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \grid.S_AXI_RID[3]_i_12 
       (.I0(s_axi4_araddr[18]),
        .I1(s_axi4_araddr[19]),
        .I2(s_axi4_araddr[20]),
        .I3(s_axi4_araddr[21]),
        .O(\grid.ar_id_r_reg[0]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair115" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \grid.S_AXI_RID[3]_i_2 
       (.I0(s_axi4_arid[3]),
        .I1(p_6_out__0),
        .I2(\grid.ar_id_r_reg[3] [3]),
        .O(\grid.S_AXI_RID_reg[3] [3]));
  (* SOFT_HLUTNM = "soft_lutpair111" *) 
  LUT4 #(
    .INIT(16'h0008)) 
    \grid.S_AXI_RID[3]_i_3 
       (.I0(\grid.S_AXI_RID[3]_i_5_n_0 ),
        .I1(\grid.S_AXI_RID[3]_i_6_n_0 ),
        .I2(\grid.S_AXI_RID[3]_i_7_n_0 ),
        .I3(\grid.S_AXI_RID[3]_i_8_n_0 ),
        .O(axi4_rdfd_en));
  (* SOFT_HLUTNM = "soft_lutpair112" *) 
  LUT5 #(
    .INIT(32'h00DF0000)) 
    \grid.S_AXI_RID[3]_i_4 
       (.I0(s_axi4_rvalid),
        .I1(s_axi4_rready),
        .I2(present_state[0]),
        .I3(present_state[1]),
        .I4(axi4_rdfd_en),
        .O(p_6_out__0));
  LUT6 #(
    .INIT(64'h0000001000000000)) 
    \grid.S_AXI_RID[3]_i_5 
       (.I0(s_axi4_araddr[10]),
        .I1(s_axi4_araddr[11]),
        .I2(s_axi4_araddr[13]),
        .I3(s_axi4_araddr[12]),
        .I4(s_axi4_araddr[30]),
        .I5(s_axi4_araddr[31]),
        .O(\grid.S_AXI_RID[3]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h0000000200000000)) 
    \grid.S_AXI_RID[3]_i_6 
       (.I0(\grid.S_AXI_RID[3]_i_9_n_0 ),
        .I1(s_axi4_araddr[5]),
        .I2(s_axi4_araddr[4]),
        .I3(s_axi4_araddr[3]),
        .I4(s_axi4_araddr[2]),
        .I5(\grid.S_AXI_RID[3]_i_10_n_0 ),
        .O(\grid.S_AXI_RID[3]_i_6_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \grid.S_AXI_RID[3]_i_7 
       (.I0(s_axi4_araddr[25]),
        .I1(s_axi4_araddr[24]),
        .I2(s_axi4_araddr[23]),
        .I3(s_axi4_araddr[22]),
        .I4(\grid.ar_id_r_reg[0] ),
        .O(\grid.S_AXI_RID[3]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \grid.S_AXI_RID[3]_i_8 
       (.I0(s_axi4_araddr[17]),
        .I1(s_axi4_araddr[16]),
        .I2(s_axi4_araddr[15]),
        .I3(s_axi4_araddr[14]),
        .I4(\grid.ar_id_r_reg[0]_0 ),
        .O(\grid.S_AXI_RID[3]_i_8_n_0 ));
  LUT4 #(
    .INIT(16'h0001)) 
    \grid.S_AXI_RID[3]_i_9 
       (.I0(s_axi4_araddr[9]),
        .I1(s_axi4_araddr[8]),
        .I2(s_axi4_araddr[7]),
        .I3(s_axi4_araddr[6]),
        .O(\grid.S_AXI_RID[3]_i_9_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \grid.ar_id_r[3]_i_1 
       (.I0(axi4_rdfd_en),
        .I1(present_state[1]),
        .O(\grid.ar_id_r_reg[0]_1 ));
  (* SOFT_HLUTNM = "soft_lutpair110" *) 
  LUT4 #(
    .INIT(16'h8880)) 
    \grxd.sig_rxd_rd_data[32]_i_2 
       (.I0(s_axi4_rready),
        .I1(s_axi4_rvalid),
        .I2(s_axi4_rlast),
        .I3(\gc0.count_reg[0] ),
        .O(axi4_fifo_rd_en_i));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFA800)) 
    \sig_register_array[0][1]_i_4 
       (.I0(sig_rx_channel_reset_reg_0),
        .I1(\gc0.count_reg[0] ),
        .I2(s_axi4_rlast),
        .I3(\gc0.count_reg[0]_0 ),
        .I4(Bus_RNW_reg_reg),
        .I5(Bus_RNW_reg_reg_1),
        .O(\sig_register_array_reg[0][1] ));
  (* SOFT_HLUTNM = "soft_lutpair113" *) 
  LUT5 #(
    .INIT(32'hA8000000)) 
    \sig_register_array[0][1]_i_5 
       (.I0(sig_rx_channel_reset_reg_0),
        .I1(\gc0.count_reg[0] ),
        .I2(s_axi4_rlast),
        .I3(s_axi4_rvalid),
        .I4(s_axi4_rready),
        .O(\sig_register_array_reg[0][1]_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFA800)) 
    \sig_register_array[0][2]_i_3 
       (.I0(\count_reg[9] ),
        .I1(\gc0.count_reg[0] ),
        .I2(s_axi4_rlast),
        .I3(\gc0.count_reg[0]_0 ),
        .I4(Bus_RNW_reg_reg),
        .I5(Bus_RNW_reg_reg_0),
        .O(\sig_register_array_reg[0][2] ));
  LUT6 #(
    .INIT(64'h1110000000000000)) 
    \sig_register_array[0][2]_i_4 
       (.I0(sig_rx_channel_reset_reg),
        .I1(\count_reg[8] ),
        .I2(\gc0.count_reg[0] ),
        .I3(s_axi4_rlast),
        .I4(s_axi4_rvalid),
        .I5(s_axi4_rready),
        .O(\sig_register_array_reg[0][2]_0 ));
endmodule

(* ORIG_REF_NAME = "axi_read_wrapper" *) 
module axi_fifo_mm_s_0_axi_read_wrapper
   (s_axi4_arready,
    s_axi4_rvalid,
    s_axi4_rlast,
    \sig_register_array_reg[0][2] ,
    \gc0.count_reg[0] ,
    \gc0.count_reg[0]_0 ,
    \sig_register_array_reg[0][2]_0 ,
    \sig_register_array_reg[0][1] ,
    \sig_register_array_reg[0][1]_0 ,
    axis_rd_en__0,
    axi4_fifo_rd_en_i,
    \grid.ar_id_r_reg[0]_0 ,
    \grid.ar_id_r_reg[0]_1 ,
    s_axi4_rid,
    s_axi_aclk,
    AS,
    s_axi4_rready,
    s_axi4_arlen,
    s_axi4_arid,
    \count_reg[9] ,
    Bus_RNW_reg_reg,
    Bus_RNW_reg_reg_0,
    sig_rx_channel_reset_reg,
    \count_reg[8] ,
    sig_rx_channel_reset_reg_0,
    Bus_RNW_reg_reg_1,
    empty_fwft_i,
    \s_axi4_araddr_25__s_port_] ,
    s_axi4_araddr,
    s_axi4_araddr_2__s_port_,
    s_axi4_arvalid);
  output s_axi4_arready;
  output s_axi4_rvalid;
  output s_axi4_rlast;
  output \sig_register_array_reg[0][2] ;
  output \gc0.count_reg[0] ;
  output \gc0.count_reg[0]_0 ;
  output \sig_register_array_reg[0][2]_0 ;
  output \sig_register_array_reg[0][1] ;
  output \sig_register_array_reg[0][1]_0 ;
  output axis_rd_en__0;
  output axi4_fifo_rd_en_i;
  output \grid.ar_id_r_reg[0]_0 ;
  output \grid.ar_id_r_reg[0]_1 ;
  output [3:0]s_axi4_rid;
  input s_axi_aclk;
  input [0:0]AS;
  input s_axi4_rready;
  input [7:0]s_axi4_arlen;
  input [3:0]s_axi4_arid;
  input \count_reg[9] ;
  input Bus_RNW_reg_reg;
  input Bus_RNW_reg_reg_0;
  input sig_rx_channel_reset_reg;
  input \count_reg[8] ;
  input sig_rx_channel_reset_reg_0;
  input Bus_RNW_reg_reg_1;
  input empty_fwft_i;
  input \s_axi4_araddr_25__s_port_] ;
  input [31:0]s_axi4_araddr;
  input s_axi4_araddr_2__s_port_;
  input s_axi4_arvalid;

  wire [0:0]AS;
  wire Bus_RNW_reg_reg;
  wire Bus_RNW_reg_reg_0;
  wire Bus_RNW_reg_reg_1;
  wire [3:0]ar_id_r;
  wire [7:0]arlen_cntr;
  wire axi4_fifo_rd_en_i;
  wire axi_read_fsm_n_10;
  wire axi_read_fsm_n_11;
  wire axi_read_fsm_n_12;
  wire axi_read_fsm_n_13;
  wire axi_read_fsm_n_14;
  wire axi_read_fsm_n_15;
  wire axi_read_fsm_n_26;
  wire axi_read_fsm_n_27;
  wire axi_read_fsm_n_28;
  wire axi_read_fsm_n_3;
  wire axi_read_fsm_n_4;
  wire axi_read_fsm_n_5;
  wire axi_read_fsm_n_6;
  wire axi_read_fsm_n_7;
  wire axi_read_fsm_n_8;
  wire axi_read_fsm_n_9;
  wire axis_rd_en__0;
  wire \count_reg[8] ;
  wire \count_reg[9] ;
  wire empty_fwft_i;
  wire \gaxi_full_sm.arlen_cntr[2]_i_2_n_0 ;
  wire \gaxi_full_sm.arlen_cntr[2]_i_3_n_0 ;
  wire \gaxi_full_sm.arlen_cntr[3]_i_2_n_0 ;
  wire \gaxi_full_sm.arlen_cntr[3]_i_3_n_0 ;
  wire \gaxi_full_sm.arlen_cntr[4]_i_2_n_0 ;
  wire \gaxi_full_sm.arlen_cntr[4]_i_3_n_0 ;
  wire \gaxi_full_sm.arlen_cntr[5]_i_2_n_0 ;
  wire \gaxi_full_sm.arlen_cntr[5]_i_3_n_0 ;
  wire \gaxi_full_sm.arlen_cntr[6]_i_2_n_0 ;
  wire \gaxi_full_sm.arlen_cntr[6]_i_3_n_0 ;
  wire \gaxi_full_sm.arlen_cntr[7]_i_4_n_0 ;
  wire \gaxi_full_sm.arlen_cntr[7]_i_6_n_0 ;
  wire \gc0.count_reg[0] ;
  wire \gc0.count_reg[0]_0 ;
  wire \grid.ar_id_r_reg[0]_0 ;
  wire \grid.ar_id_r_reg[0]_1 ;
  wire r_last_int_c__7;
  wire [31:0]s_axi4_araddr;
  wire s_axi4_araddr_25__s_net_1;
  wire s_axi4_araddr_2__s_net_1;
  wire [3:0]s_axi4_arid;
  wire [7:0]s_axi4_arlen;
  wire s_axi4_arready;
  wire s_axi4_arvalid;
  wire [3:0]s_axi4_rid;
  wire s_axi4_rlast;
  wire s_axi4_rready;
  wire s_axi4_rvalid;
  wire s_axi_aclk;
  wire \sig_register_array_reg[0][1] ;
  wire \sig_register_array_reg[0][1]_0 ;
  wire \sig_register_array_reg[0][2] ;
  wire \sig_register_array_reg[0][2]_0 ;
  wire sig_rx_channel_reset_reg;
  wire sig_rx_channel_reset_reg_0;

  assign s_axi4_araddr_25__s_net_1 = \s_axi4_araddr_25__s_port_] ;
  assign s_axi4_araddr_2__s_net_1 = s_axi4_araddr_2__s_port_;
  axi_fifo_mm_s_0_axi_read_fsm axi_read_fsm
       (.AS(AS),
        .Bus_RNW_reg_reg(Bus_RNW_reg_reg),
        .Bus_RNW_reg_reg_0(Bus_RNW_reg_reg_0),
        .Bus_RNW_reg_reg_1(Bus_RNW_reg_reg_1),
        .D({axi_read_fsm_n_3,axi_read_fsm_n_4,axi_read_fsm_n_5,axi_read_fsm_n_6,axi_read_fsm_n_7,axi_read_fsm_n_8,axi_read_fsm_n_9,axi_read_fsm_n_10}),
        .E(axi_read_fsm_n_11),
        .Q(arlen_cntr),
        .axi4_fifo_rd_en_i(axi4_fifo_rd_en_i),
        .axis_rd_en__0(axis_rd_en__0),
        .\count_reg[8] (\count_reg[8] ),
        .\count_reg[9] (\count_reg[9] ),
        .empty_fwft_i(empty_fwft_i),
        .\gaxi_full_sm.arlen_cntr_reg[0] (axi_read_fsm_n_26),
        .\gaxi_full_sm.arlen_cntr_reg[0]_0 (axi_read_fsm_n_27),
        .\gaxi_full_sm.arlen_cntr_reg[0]_1 (\gaxi_full_sm.arlen_cntr[2]_i_2_n_0 ),
        .\gaxi_full_sm.arlen_cntr_reg[1] (\gaxi_full_sm.arlen_cntr[3]_i_2_n_0 ),
        .\gaxi_full_sm.arlen_cntr_reg[2] (\gaxi_full_sm.arlen_cntr[4]_i_2_n_0 ),
        .\gaxi_full_sm.arlen_cntr_reg[3] (\gaxi_full_sm.arlen_cntr[5]_i_2_n_0 ),
        .\gaxi_full_sm.arlen_cntr_reg[4] (\gaxi_full_sm.arlen_cntr[6]_i_2_n_0 ),
        .\gaxi_full_sm.arlen_cntr_reg[6] (\gaxi_full_sm.arlen_cntr[7]_i_4_n_0 ),
        .\gc0.count_reg[0] (\gc0.count_reg[0] ),
        .\gc0.count_reg[0]_0 (\gc0.count_reg[0]_0 ),
        .\grid.S_AXI_RID_reg[3] ({axi_read_fsm_n_12,axi_read_fsm_n_13,axi_read_fsm_n_14,axi_read_fsm_n_15}),
        .\grid.ar_id_r_reg[0] (\grid.ar_id_r_reg[0]_0 ),
        .\grid.ar_id_r_reg[0]_0 (\grid.ar_id_r_reg[0]_1 ),
        .\grid.ar_id_r_reg[0]_1 (axi_read_fsm_n_28),
        .\grid.ar_id_r_reg[3] (ar_id_r),
        .r_last_int_c__7(r_last_int_c__7),
        .s_axi4_araddr(s_axi4_araddr),
        .\s_axi4_araddr_25__s_port_] (s_axi4_araddr_25__s_net_1),
        .s_axi4_araddr_2__s_port_(s_axi4_araddr_2__s_net_1),
        .s_axi4_arid(s_axi4_arid),
        .s_axi4_arlen(s_axi4_arlen),
        .\s_axi4_arlen[0]_0 (\gaxi_full_sm.arlen_cntr[2]_i_3_n_0 ),
        .\s_axi4_arlen[0]_1 (\gaxi_full_sm.arlen_cntr[7]_i_6_n_0 ),
        .s_axi4_arlen_0__s_port_(\gaxi_full_sm.arlen_cntr[4]_i_3_n_0 ),
        .s_axi4_arlen_2__s_port_(\gaxi_full_sm.arlen_cntr[3]_i_3_n_0 ),
        .s_axi4_arlen_4__s_port_(\gaxi_full_sm.arlen_cntr[5]_i_3_n_0 ),
        .\s_axi4_arlen_5__s_port_] (\gaxi_full_sm.arlen_cntr[6]_i_3_n_0 ),
        .s_axi4_arready(s_axi4_arready),
        .s_axi4_arvalid(s_axi4_arvalid),
        .s_axi4_rlast(s_axi4_rlast),
        .s_axi4_rready(s_axi4_rready),
        .s_axi4_rvalid(s_axi4_rvalid),
        .s_axi_aclk(s_axi_aclk),
        .\sig_register_array_reg[0][1] (\sig_register_array_reg[0][1] ),
        .\sig_register_array_reg[0][1]_0 (\sig_register_array_reg[0][1]_0 ),
        .\sig_register_array_reg[0][2] (\sig_register_array_reg[0][2] ),
        .\sig_register_array_reg[0][2]_0 (\sig_register_array_reg[0][2]_0 ),
        .sig_rx_channel_reset_reg(sig_rx_channel_reset_reg),
        .sig_rx_channel_reset_reg_0(sig_rx_channel_reset_reg_0));
  LUT2 #(
    .INIT(4'hE)) 
    \gaxi_full_sm.arlen_cntr[2]_i_2 
       (.I0(arlen_cntr[0]),
        .I1(arlen_cntr[1]),
        .O(\gaxi_full_sm.arlen_cntr[2]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair120" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \gaxi_full_sm.arlen_cntr[2]_i_3 
       (.I0(s_axi4_arlen[1]),
        .I1(s_axi4_arlen[0]),
        .O(\gaxi_full_sm.arlen_cntr[2]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair119" *) 
  LUT3 #(
    .INIT(8'hFE)) 
    \gaxi_full_sm.arlen_cntr[3]_i_2 
       (.I0(arlen_cntr[1]),
        .I1(arlen_cntr[0]),
        .I2(arlen_cntr[2]),
        .O(\gaxi_full_sm.arlen_cntr[3]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair120" *) 
  LUT3 #(
    .INIT(8'h01)) 
    \gaxi_full_sm.arlen_cntr[3]_i_3 
       (.I0(s_axi4_arlen[0]),
        .I1(s_axi4_arlen[1]),
        .I2(s_axi4_arlen[2]),
        .O(\gaxi_full_sm.arlen_cntr[3]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair117" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    \gaxi_full_sm.arlen_cntr[4]_i_2 
       (.I0(arlen_cntr[2]),
        .I1(arlen_cntr[0]),
        .I2(arlen_cntr[1]),
        .I3(arlen_cntr[3]),
        .O(\gaxi_full_sm.arlen_cntr[4]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair118" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    \gaxi_full_sm.arlen_cntr[4]_i_3 
       (.I0(s_axi4_arlen[3]),
        .I1(s_axi4_arlen[2]),
        .I2(s_axi4_arlen[1]),
        .I3(s_axi4_arlen[0]),
        .O(\gaxi_full_sm.arlen_cntr[4]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair117" *) 
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \gaxi_full_sm.arlen_cntr[5]_i_2 
       (.I0(arlen_cntr[3]),
        .I1(arlen_cntr[1]),
        .I2(arlen_cntr[0]),
        .I3(arlen_cntr[2]),
        .I4(arlen_cntr[4]),
        .O(\gaxi_full_sm.arlen_cntr[5]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair118" *) 
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \gaxi_full_sm.arlen_cntr[5]_i_3 
       (.I0(s_axi4_arlen[0]),
        .I1(s_axi4_arlen[1]),
        .I2(s_axi4_arlen[2]),
        .I3(s_axi4_arlen[3]),
        .I4(s_axi4_arlen[4]),
        .O(\gaxi_full_sm.arlen_cntr[5]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \gaxi_full_sm.arlen_cntr[6]_i_2 
       (.I0(arlen_cntr[4]),
        .I1(arlen_cntr[2]),
        .I2(arlen_cntr[0]),
        .I3(arlen_cntr[1]),
        .I4(arlen_cntr[3]),
        .I5(arlen_cntr[5]),
        .O(\gaxi_full_sm.arlen_cntr[6]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \gaxi_full_sm.arlen_cntr[6]_i_3 
       (.I0(s_axi4_arlen[4]),
        .I1(s_axi4_arlen[3]),
        .I2(s_axi4_arlen[2]),
        .I3(s_axi4_arlen[1]),
        .I4(s_axi4_arlen[0]),
        .I5(s_axi4_arlen[5]),
        .O(\gaxi_full_sm.arlen_cntr[6]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair119" *) 
  LUT4 #(
    .INIT(16'h0002)) 
    \gaxi_full_sm.arlen_cntr[7]_i_3 
       (.I0(axi_read_fsm_n_26),
        .I1(arlen_cntr[0]),
        .I2(arlen_cntr[1]),
        .I3(arlen_cntr[2]),
        .O(r_last_int_c__7));
  LUT2 #(
    .INIT(4'hE)) 
    \gaxi_full_sm.arlen_cntr[7]_i_4 
       (.I0(\gaxi_full_sm.arlen_cntr[6]_i_2_n_0 ),
        .I1(arlen_cntr[6]),
        .O(\gaxi_full_sm.arlen_cntr[7]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \gaxi_full_sm.arlen_cntr[7]_i_6 
       (.I0(s_axi4_arlen[5]),
        .I1(\gaxi_full_sm.arlen_cntr[2]_i_3_n_0 ),
        .I2(s_axi4_arlen[2]),
        .I3(s_axi4_arlen[3]),
        .I4(s_axi4_arlen[4]),
        .I5(s_axi4_arlen[6]),
        .O(\gaxi_full_sm.arlen_cntr[7]_i_6_n_0 ));
  FDPE #(
    .INIT(1'b1)) 
    \gaxi_full_sm.arlen_cntr_reg[0] 
       (.C(s_axi_aclk),
        .CE(axi_read_fsm_n_27),
        .D(axi_read_fsm_n_10),
        .PRE(AS),
        .Q(arlen_cntr[0]));
  FDCE #(
    .INIT(1'b0)) 
    \gaxi_full_sm.arlen_cntr_reg[1] 
       (.C(s_axi_aclk),
        .CE(axi_read_fsm_n_27),
        .CLR(AS),
        .D(axi_read_fsm_n_9),
        .Q(arlen_cntr[1]));
  FDCE #(
    .INIT(1'b0)) 
    \gaxi_full_sm.arlen_cntr_reg[2] 
       (.C(s_axi_aclk),
        .CE(axi_read_fsm_n_27),
        .CLR(AS),
        .D(axi_read_fsm_n_8),
        .Q(arlen_cntr[2]));
  FDCE #(
    .INIT(1'b0)) 
    \gaxi_full_sm.arlen_cntr_reg[3] 
       (.C(s_axi_aclk),
        .CE(axi_read_fsm_n_27),
        .CLR(AS),
        .D(axi_read_fsm_n_7),
        .Q(arlen_cntr[3]));
  FDCE #(
    .INIT(1'b0)) 
    \gaxi_full_sm.arlen_cntr_reg[4] 
       (.C(s_axi_aclk),
        .CE(axi_read_fsm_n_27),
        .CLR(AS),
        .D(axi_read_fsm_n_6),
        .Q(arlen_cntr[4]));
  FDCE #(
    .INIT(1'b0)) 
    \gaxi_full_sm.arlen_cntr_reg[5] 
       (.C(s_axi_aclk),
        .CE(axi_read_fsm_n_27),
        .CLR(AS),
        .D(axi_read_fsm_n_5),
        .Q(arlen_cntr[5]));
  FDCE #(
    .INIT(1'b0)) 
    \gaxi_full_sm.arlen_cntr_reg[6] 
       (.C(s_axi_aclk),
        .CE(axi_read_fsm_n_27),
        .CLR(AS),
        .D(axi_read_fsm_n_4),
        .Q(arlen_cntr[6]));
  FDCE #(
    .INIT(1'b0)) 
    \gaxi_full_sm.arlen_cntr_reg[7] 
       (.C(s_axi_aclk),
        .CE(axi_read_fsm_n_27),
        .CLR(AS),
        .D(axi_read_fsm_n_3),
        .Q(arlen_cntr[7]));
  FDCE #(
    .INIT(1'b0)) 
    \grid.S_AXI_RID_reg[0] 
       (.C(s_axi_aclk),
        .CE(axi_read_fsm_n_11),
        .CLR(AS),
        .D(axi_read_fsm_n_15),
        .Q(s_axi4_rid[0]));
  FDCE #(
    .INIT(1'b0)) 
    \grid.S_AXI_RID_reg[1] 
       (.C(s_axi_aclk),
        .CE(axi_read_fsm_n_11),
        .CLR(AS),
        .D(axi_read_fsm_n_14),
        .Q(s_axi4_rid[1]));
  FDCE #(
    .INIT(1'b0)) 
    \grid.S_AXI_RID_reg[2] 
       (.C(s_axi_aclk),
        .CE(axi_read_fsm_n_11),
        .CLR(AS),
        .D(axi_read_fsm_n_13),
        .Q(s_axi4_rid[2]));
  FDCE #(
    .INIT(1'b0)) 
    \grid.S_AXI_RID_reg[3] 
       (.C(s_axi_aclk),
        .CE(axi_read_fsm_n_11),
        .CLR(AS),
        .D(axi_read_fsm_n_12),
        .Q(s_axi4_rid[3]));
  FDCE #(
    .INIT(1'b0)) 
    \grid.ar_id_r_reg[0] 
       (.C(s_axi_aclk),
        .CE(axi_read_fsm_n_28),
        .CLR(AS),
        .D(s_axi4_arid[0]),
        .Q(ar_id_r[0]));
  FDCE #(
    .INIT(1'b0)) 
    \grid.ar_id_r_reg[1] 
       (.C(s_axi_aclk),
        .CE(axi_read_fsm_n_28),
        .CLR(AS),
        .D(s_axi4_arid[1]),
        .Q(ar_id_r[1]));
  FDCE #(
    .INIT(1'b0)) 
    \grid.ar_id_r_reg[2] 
       (.C(s_axi_aclk),
        .CE(axi_read_fsm_n_28),
        .CLR(AS),
        .D(s_axi4_arid[2]),
        .Q(ar_id_r[2]));
  FDCE #(
    .INIT(1'b0)) 
    \grid.ar_id_r_reg[3] 
       (.C(s_axi_aclk),
        .CE(axi_read_fsm_n_28),
        .CLR(AS),
        .D(s_axi4_arid[3]),
        .Q(ar_id_r[3]));
endmodule

(* ORIG_REF_NAME = "axi_wrapper" *) 
module axi_fifo_mm_s_0_axi_wrapper
   (s_axi4_awready,
    s_axi4_wready,
    s_axi4_arready,
    s_axi4_bvalid,
    s_axi4_rvalid,
    s_axi4_rlast,
    p_1_out,
    E,
    \gfifo_gen.gmm2s.gaxi4_strb.input_tstrb_reg[3] ,
    txd_wr_en,
    \sig_register_array_reg[0][2] ,
    sig_axi_rd_en,
    \gc0.count_reg[0] ,
    \sig_register_array_reg[0][2]_0 ,
    \sig_register_array_reg[0][1] ,
    \sig_register_array_reg[0][1]_0 ,
    axis_rd_en__0,
    axi4_fifo_rd_en_i,
    \grid.ar_id_r_reg[0] ,
    \grid.ar_id_r_reg[0]_0 ,
    s_axi4_rid,
    s_axi4_bid,
    s_axi_aclk,
    AS,
    s_axi4_bready,
    s_axi4_wvalid,
    sig_txd_sb_wr_en_reg,
    s_axi4_wstrb,
    s_axi4_rready,
    s_axi4_arlen,
    s_axi4_arid,
    \count_reg[9] ,
    Bus_RNW_reg_reg,
    Bus_RNW_reg_reg_0,
    sig_rx_channel_reset_reg,
    \count_reg[8] ,
    sig_rx_channel_reset_reg_0,
    Bus_RNW_reg_reg_1,
    empty_fwft_i,
    \s_axi4_araddr_25__s_port_] ,
    s_axi4_araddr,
    s_axi4_araddr_2__s_port_,
    s_axi4_arvalid,
    s_axi4_awlen,
    s_axi4_awaddr,
    s_axi4_awvalid,
    s_axi4_awid,
    ram_full_i_reg);
  output s_axi4_awready;
  output s_axi4_wready;
  output s_axi4_arready;
  output s_axi4_bvalid;
  output s_axi4_rvalid;
  output s_axi4_rlast;
  output p_1_out;
  output [0:0]E;
  output [3:0]\gfifo_gen.gmm2s.gaxi4_strb.input_tstrb_reg[3] ;
  output txd_wr_en;
  output \sig_register_array_reg[0][2] ;
  output sig_axi_rd_en;
  output \gc0.count_reg[0] ;
  output \sig_register_array_reg[0][2]_0 ;
  output \sig_register_array_reg[0][1] ;
  output \sig_register_array_reg[0][1]_0 ;
  output axis_rd_en__0;
  output axi4_fifo_rd_en_i;
  output \grid.ar_id_r_reg[0] ;
  output \grid.ar_id_r_reg[0]_0 ;
  output [3:0]s_axi4_rid;
  output [3:0]s_axi4_bid;
  input s_axi_aclk;
  input [0:0]AS;
  input s_axi4_bready;
  input s_axi4_wvalid;
  input sig_txd_sb_wr_en_reg;
  input [3:0]s_axi4_wstrb;
  input s_axi4_rready;
  input [7:0]s_axi4_arlen;
  input [3:0]s_axi4_arid;
  input \count_reg[9] ;
  input Bus_RNW_reg_reg;
  input Bus_RNW_reg_reg_0;
  input sig_rx_channel_reset_reg;
  input \count_reg[8] ;
  input sig_rx_channel_reset_reg_0;
  input Bus_RNW_reg_reg_1;
  input empty_fwft_i;
  input \s_axi4_araddr_25__s_port_] ;
  input [31:0]s_axi4_araddr;
  input s_axi4_araddr_2__s_port_;
  input s_axi4_arvalid;
  input [7:0]s_axi4_awlen;
  input [31:0]s_axi4_awaddr;
  input s_axi4_awvalid;
  input [3:0]s_axi4_awid;
  input [0:0]ram_full_i_reg;

  wire [0:0]AS;
  wire Bus_RNW_reg_reg;
  wire Bus_RNW_reg_reg_0;
  wire Bus_RNW_reg_reg_1;
  wire [0:0]E;
  wire axi4_fifo_rd_en_i;
  wire axis_rd_en__0;
  wire \count_reg[8] ;
  wire \count_reg[9] ;
  wire empty_fwft_i;
  wire \gc0.count_reg[0] ;
  wire [3:0]\gfifo_gen.gmm2s.gaxi4_strb.input_tstrb_reg[3] ;
  wire \grid.ar_id_r_reg[0] ;
  wire \grid.ar_id_r_reg[0]_0 ;
  wire p_1_out;
  wire [0:0]ram_full_i_reg;
  wire [31:0]s_axi4_araddr;
  wire s_axi4_araddr_25__s_net_1;
  wire s_axi4_araddr_2__s_net_1;
  wire [3:0]s_axi4_arid;
  wire [7:0]s_axi4_arlen;
  wire s_axi4_arready;
  wire s_axi4_arvalid;
  wire [31:0]s_axi4_awaddr;
  wire [3:0]s_axi4_awid;
  wire [7:0]s_axi4_awlen;
  wire s_axi4_awready;
  wire s_axi4_awvalid;
  wire [3:0]s_axi4_bid;
  wire s_axi4_bready;
  wire s_axi4_bvalid;
  wire [3:0]s_axi4_rid;
  wire s_axi4_rlast;
  wire s_axi4_rready;
  wire s_axi4_rvalid;
  wire s_axi4_wready;
  wire [3:0]s_axi4_wstrb;
  wire s_axi4_wvalid;
  wire s_axi_aclk;
  wire sig_axi_rd_en;
  wire \sig_register_array_reg[0][1] ;
  wire \sig_register_array_reg[0][1]_0 ;
  wire \sig_register_array_reg[0][2] ;
  wire \sig_register_array_reg[0][2]_0 ;
  wire sig_rx_channel_reset_reg;
  wire sig_rx_channel_reset_reg_0;
  wire sig_txd_sb_wr_en_reg;
  wire txd_wr_en;

  assign s_axi4_araddr_25__s_net_1 = \s_axi4_araddr_25__s_port_] ;
  assign s_axi4_araddr_2__s_net_1 = s_axi4_araddr_2__s_port_;
  axi_fifo_mm_s_0_axi_read_wrapper axi_rd_sm
       (.AS(AS),
        .Bus_RNW_reg_reg(Bus_RNW_reg_reg),
        .Bus_RNW_reg_reg_0(Bus_RNW_reg_reg_0),
        .Bus_RNW_reg_reg_1(Bus_RNW_reg_reg_1),
        .axi4_fifo_rd_en_i(axi4_fifo_rd_en_i),
        .axis_rd_en__0(axis_rd_en__0),
        .\count_reg[8] (\count_reg[8] ),
        .\count_reg[9] (\count_reg[9] ),
        .empty_fwft_i(empty_fwft_i),
        .\gc0.count_reg[0] (sig_axi_rd_en),
        .\gc0.count_reg[0]_0 (\gc0.count_reg[0] ),
        .\grid.ar_id_r_reg[0]_0 (\grid.ar_id_r_reg[0] ),
        .\grid.ar_id_r_reg[0]_1 (\grid.ar_id_r_reg[0]_0 ),
        .s_axi4_araddr(s_axi4_araddr),
        .\s_axi4_araddr_25__s_port_] (s_axi4_araddr_25__s_net_1),
        .s_axi4_araddr_2__s_port_(s_axi4_araddr_2__s_net_1),
        .s_axi4_arid(s_axi4_arid),
        .s_axi4_arlen(s_axi4_arlen),
        .s_axi4_arready(s_axi4_arready),
        .s_axi4_arvalid(s_axi4_arvalid),
        .s_axi4_rid(s_axi4_rid),
        .s_axi4_rlast(s_axi4_rlast),
        .s_axi4_rready(s_axi4_rready),
        .s_axi4_rvalid(s_axi4_rvalid),
        .s_axi_aclk(s_axi_aclk),
        .\sig_register_array_reg[0][1] (\sig_register_array_reg[0][1] ),
        .\sig_register_array_reg[0][1]_0 (\sig_register_array_reg[0][1]_0 ),
        .\sig_register_array_reg[0][2] (\sig_register_array_reg[0][2] ),
        .\sig_register_array_reg[0][2]_0 (\sig_register_array_reg[0][2]_0 ),
        .sig_rx_channel_reset_reg(sig_rx_channel_reset_reg),
        .sig_rx_channel_reset_reg_0(sig_rx_channel_reset_reg_0));
  axi_fifo_mm_s_0_axi_write_wrapper axi_wr_fsm
       (.AS(AS),
        .E(E),
        .\gfifo_gen.gmm2s.gaxi4_strb.input_tstrb_reg[3] (\gfifo_gen.gmm2s.gaxi4_strb.input_tstrb_reg[3] ),
        .p_1_out(p_1_out),
        .ram_full_i_reg(ram_full_i_reg),
        .s_axi4_awaddr(s_axi4_awaddr),
        .s_axi4_awid(s_axi4_awid),
        .s_axi4_awlen(s_axi4_awlen),
        .s_axi4_awready(s_axi4_awready),
        .s_axi4_awvalid(s_axi4_awvalid),
        .s_axi4_bid(s_axi4_bid),
        .s_axi4_bready(s_axi4_bready),
        .s_axi4_bvalid(s_axi4_bvalid),
        .s_axi4_wready(s_axi4_wready),
        .s_axi4_wstrb(s_axi4_wstrb),
        .s_axi4_wvalid(s_axi4_wvalid),
        .s_axi_aclk(s_axi_aclk),
        .sig_txd_sb_wr_en_reg(sig_txd_sb_wr_en_reg),
        .txd_wr_en(txd_wr_en));
endmodule

(* ORIG_REF_NAME = "axi_write_fsm" *) 
module axi_fifo_mm_s_0_axi_write_fsm
   (s_axi4_awready,
    s_axi4_wready,
    p_1_out,
    E,
    \gfifo_gen.gmm2s.start_wr_reg ,
    \gfifo_gen.gmm2s.gaxi4_strb.input_tstrb_reg[3] ,
    txd_wr_en,
    D,
    I95,
    bvalid_c,
    \gaxi_bid_gen.bvalid_wr_cnt_r_reg[1] ,
    \gaxi_bid_gen.bvalid_wr_cnt_r_reg[0] ,
    \bvalid_count_r_reg[2] ,
    \bvalid_count_r_reg[1] ,
    \bvalid_count_r_reg[0] ,
    s_axi_aclk,
    AS,
    s_axi4_bready,
    s_axi4_wvalid,
    sig_txd_sb_wr_en_reg,
    s_axi4_wstrb,
    \gaxif_wlast_gen.awlen_cntr_r_reg[0] ,
    Q,
    s_axi4_awlen,
    \gaxif_wlast_gen.awlen_cntr_r_reg[5] ,
    \gaxif_wlast_gen.awlen_cntr_r_reg[1] ,
    \bvalid_count_r_reg[2]_0 ,
    \bvalid_count_r_reg[1]_0 ,
    \bvalid_count_r_reg[0]_0 ,
    s_axi4_awaddr,
    s_axi4_awvalid,
    \gaxi_bid_gen.bvalid_wr_cnt_r_reg[1]_0 ,
    \gaxi_bvalid_id_r.bvalid_r_reg );
  output s_axi4_awready;
  output s_axi4_wready;
  output p_1_out;
  output [0:0]E;
  output [0:0]\gfifo_gen.gmm2s.start_wr_reg ;
  output [3:0]\gfifo_gen.gmm2s.gaxi4_strb.input_tstrb_reg[3] ;
  output txd_wr_en;
  output [7:0]D;
  output I95;
  output bvalid_c;
  output \gaxi_bid_gen.bvalid_wr_cnt_r_reg[1] ;
  output \gaxi_bid_gen.bvalid_wr_cnt_r_reg[0] ;
  output \bvalid_count_r_reg[2] ;
  output \bvalid_count_r_reg[1] ;
  output \bvalid_count_r_reg[0] ;
  input s_axi_aclk;
  input [0:0]AS;
  input s_axi4_bready;
  input s_axi4_wvalid;
  input sig_txd_sb_wr_en_reg;
  input [3:0]s_axi4_wstrb;
  input \gaxif_wlast_gen.awlen_cntr_r_reg[0] ;
  input [7:0]Q;
  input [7:0]s_axi4_awlen;
  input \gaxif_wlast_gen.awlen_cntr_r_reg[5] ;
  input \gaxif_wlast_gen.awlen_cntr_r_reg[1] ;
  input \bvalid_count_r_reg[2]_0 ;
  input \bvalid_count_r_reg[1]_0 ;
  input \bvalid_count_r_reg[0]_0 ;
  input [31:0]s_axi4_awaddr;
  input s_axi4_awvalid;
  input [1:0]\gaxi_bid_gen.bvalid_wr_cnt_r_reg[1]_0 ;
  input \gaxi_bvalid_id_r.bvalid_r_reg ;

  wire [0:0]AS;
  wire [7:0]D;
  wire [0:0]E;
  wire I95;
  wire [7:0]Q;
  wire aw_ready_c;
  wire axi4_tdfd_en;
  wire bready_timeout_c__1;
  wire bvalid_c;
  wire \bvalid_count_r_reg[0] ;
  wire \bvalid_count_r_reg[0]_0 ;
  wire \bvalid_count_r_reg[1] ;
  wire \bvalid_count_r_reg[1]_0 ;
  wire \bvalid_count_r_reg[2] ;
  wire \bvalid_count_r_reg[2]_0 ;
  wire \gaxi_bid_gen.bvalid_wr_cnt_r_reg[0] ;
  wire \gaxi_bid_gen.bvalid_wr_cnt_r_reg[1] ;
  wire [1:0]\gaxi_bid_gen.bvalid_wr_cnt_r_reg[1]_0 ;
  wire \gaxi_bvalid_id_r.bvalid_r_reg ;
  wire \gaxi_full_sm.aw_ready_r_i_10_n_0 ;
  wire \gaxi_full_sm.aw_ready_r_i_11_n_0 ;
  wire \gaxi_full_sm.aw_ready_r_i_3_n_0 ;
  wire \gaxi_full_sm.aw_ready_r_i_4_n_0 ;
  wire \gaxi_full_sm.aw_ready_r_i_5_n_0 ;
  wire \gaxi_full_sm.aw_ready_r_i_6_n_0 ;
  wire \gaxi_full_sm.aw_ready_r_i_7_n_0 ;
  wire \gaxi_full_sm.aw_ready_r_i_8_n_0 ;
  wire \gaxi_full_sm.aw_ready_r_i_9_n_0 ;
  wire \gaxi_full_sm.present_state[0]_i_1__0_n_0 ;
  wire \gaxi_full_sm.present_state[0]_i_2_n_0 ;
  wire \gaxi_full_sm.present_state[1]_i_1__0_n_0 ;
  wire \gaxi_full_sm.present_state[1]_i_2__0_n_0 ;
  wire \gaxi_full_sm.w_ready_r_i_3_n_0 ;
  wire \gaxi_full_sm.w_ready_r_i_4_n_0 ;
  wire \gaxif_wlast_gen.awlen_cntr_r_reg[0] ;
  wire \gaxif_wlast_gen.awlen_cntr_r_reg[1] ;
  wire \gaxif_wlast_gen.awlen_cntr_r_reg[5] ;
  wire [3:0]\gfifo_gen.gmm2s.gaxi4_strb.input_tstrb_reg[3] ;
  wire [0:0]\gfifo_gen.gmm2s.start_wr_reg ;
  wire p_1_out;
  wire [1:0]present_state;
  wire [31:0]s_axi4_awaddr;
  wire [7:0]s_axi4_awlen;
  wire s_axi4_awready;
  wire s_axi4_awvalid;
  wire s_axi4_bready;
  wire s_axi4_wready;
  wire [3:0]s_axi4_wstrb;
  wire s_axi4_wvalid;
  wire s_axi_aclk;
  wire sig_txd_sb_wr_en_reg;
  wire txd_wr_en;
  wire w_last_c__7;
  wire w_ready_c;

  LUT6 #(
    .INIT(64'h959595956A6A6A2A)) 
    \bvalid_count_r[0]_i_1 
       (.I0(bvalid_c),
        .I1(s_axi4_bready),
        .I2(\gaxi_bvalid_id_r.bvalid_r_reg ),
        .I3(\bvalid_count_r_reg[1]_0 ),
        .I4(\bvalid_count_r_reg[2]_0 ),
        .I5(\bvalid_count_r_reg[0]_0 ),
        .O(\bvalid_count_r_reg[0] ));
  LUT6 #(
    .INIT(64'hF05A5A5AA4F0F0F0)) 
    \bvalid_count_r[1]_i_1 
       (.I0(\bvalid_count_r_reg[0]_0 ),
        .I1(\bvalid_count_r_reg[2]_0 ),
        .I2(\bvalid_count_r_reg[1]_0 ),
        .I3(\gaxi_bvalid_id_r.bvalid_r_reg ),
        .I4(s_axi4_bready),
        .I5(bvalid_c),
        .O(\bvalid_count_r_reg[1] ));
  LUT6 #(
    .INIT(64'hCC6C6C6CC8CCCCCC)) 
    \bvalid_count_r[2]_i_1 
       (.I0(\bvalid_count_r_reg[0]_0 ),
        .I1(\bvalid_count_r_reg[2]_0 ),
        .I2(\bvalid_count_r_reg[1]_0 ),
        .I3(\gaxi_bvalid_id_r.bvalid_r_reg ),
        .I4(s_axi4_bready),
        .I5(bvalid_c),
        .O(\bvalid_count_r_reg[2] ));
  LUT2 #(
    .INIT(4'h8)) 
    \gaxi_bid_gen.axi_bid_array_reg_0_3_0_3_i_1 
       (.I0(axi4_tdfd_en),
        .I1(s_axi4_awready),
        .O(I95));
  (* SOFT_HLUTNM = "soft_lutpair129" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \gaxi_bid_gen.bvalid_wr_cnt_r[0]_i_1 
       (.I0(bvalid_c),
        .I1(\gaxi_bid_gen.bvalid_wr_cnt_r_reg[1]_0 [0]),
        .O(\gaxi_bid_gen.bvalid_wr_cnt_r_reg[0] ));
  (* SOFT_HLUTNM = "soft_lutpair129" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \gaxi_bid_gen.bvalid_wr_cnt_r[1]_i_1 
       (.I0(\gaxi_bid_gen.bvalid_wr_cnt_r_reg[1]_0 [0]),
        .I1(bvalid_c),
        .I2(\gaxi_bid_gen.bvalid_wr_cnt_r_reg[1]_0 [1]),
        .O(\gaxi_bid_gen.bvalid_wr_cnt_r_reg[1] ));
  LUT4 #(
    .INIT(16'h0880)) 
    \gaxi_bvalid_id_r.bvalid_d1_c_i_1 
       (.I0(s_axi4_wvalid),
        .I1(w_last_c__7),
        .I2(present_state[1]),
        .I3(present_state[0]),
        .O(bvalid_c));
  (* SOFT_HLUTNM = "soft_lutpair124" *) 
  LUT5 #(
    .INIT(32'hF3350335)) 
    \gaxi_full_sm.aw_ready_r_i_1 
       (.I0(axi4_tdfd_en),
        .I1(\gaxi_full_sm.aw_ready_r_i_3_n_0 ),
        .I2(present_state[0]),
        .I3(present_state[1]),
        .I4(s_axi4_bready),
        .O(aw_ready_c));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \gaxi_full_sm.aw_ready_r_i_10 
       (.I0(s_axi4_awaddr[26]),
        .I1(s_axi4_awaddr[27]),
        .I2(s_axi4_awaddr[28]),
        .I3(s_axi4_awaddr[29]),
        .O(\gaxi_full_sm.aw_ready_r_i_10_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \gaxi_full_sm.aw_ready_r_i_11 
       (.I0(s_axi4_awaddr[18]),
        .I1(s_axi4_awaddr[19]),
        .I2(s_axi4_awaddr[20]),
        .I3(s_axi4_awaddr[21]),
        .O(\gaxi_full_sm.aw_ready_r_i_11_n_0 ));
  LUT4 #(
    .INIT(16'h0008)) 
    \gaxi_full_sm.aw_ready_r_i_2 
       (.I0(\gaxi_full_sm.aw_ready_r_i_4_n_0 ),
        .I1(\gaxi_full_sm.aw_ready_r_i_5_n_0 ),
        .I2(\gaxi_full_sm.aw_ready_r_i_6_n_0 ),
        .I3(\gaxi_full_sm.aw_ready_r_i_7_n_0 ),
        .O(axi4_tdfd_en));
  (* SOFT_HLUTNM = "soft_lutpair122" *) 
  LUT5 #(
    .INIT(32'h10FFFFFF)) 
    \gaxi_full_sm.aw_ready_r_i_3 
       (.I0(\bvalid_count_r_reg[2]_0 ),
        .I1(\bvalid_count_r_reg[1]_0 ),
        .I2(\bvalid_count_r_reg[0]_0 ),
        .I3(w_last_c__7),
        .I4(s_axi4_wvalid),
        .O(\gaxi_full_sm.aw_ready_r_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0002000000000000)) 
    \gaxi_full_sm.aw_ready_r_i_4 
       (.I0(s_axi4_awaddr[12]),
        .I1(s_axi4_awaddr[11]),
        .I2(s_axi4_awaddr[13]),
        .I3(s_axi4_awaddr[30]),
        .I4(s_axi4_awready),
        .I5(s_axi4_awaddr[31]),
        .O(\gaxi_full_sm.aw_ready_r_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h0000000200000000)) 
    \gaxi_full_sm.aw_ready_r_i_5 
       (.I0(\gaxi_full_sm.aw_ready_r_i_8_n_0 ),
        .I1(s_axi4_awaddr[6]),
        .I2(s_axi4_awaddr[5]),
        .I3(s_axi4_awaddr[4]),
        .I4(s_axi4_awaddr[3]),
        .I5(\gaxi_full_sm.aw_ready_r_i_9_n_0 ),
        .O(\gaxi_full_sm.aw_ready_r_i_5_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \gaxi_full_sm.aw_ready_r_i_6 
       (.I0(s_axi4_awaddr[25]),
        .I1(s_axi4_awaddr[24]),
        .I2(s_axi4_awaddr[23]),
        .I3(s_axi4_awaddr[22]),
        .I4(\gaxi_full_sm.aw_ready_r_i_10_n_0 ),
        .O(\gaxi_full_sm.aw_ready_r_i_6_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \gaxi_full_sm.aw_ready_r_i_7 
       (.I0(s_axi4_awaddr[17]),
        .I1(s_axi4_awaddr[16]),
        .I2(s_axi4_awaddr[15]),
        .I3(s_axi4_awaddr[14]),
        .I4(\gaxi_full_sm.aw_ready_r_i_11_n_0 ),
        .O(\gaxi_full_sm.aw_ready_r_i_7_n_0 ));
  LUT4 #(
    .INIT(16'h0001)) 
    \gaxi_full_sm.aw_ready_r_i_8 
       (.I0(s_axi4_awaddr[10]),
        .I1(s_axi4_awaddr[9]),
        .I2(s_axi4_awaddr[8]),
        .I3(s_axi4_awaddr[7]),
        .O(\gaxi_full_sm.aw_ready_r_i_8_n_0 ));
  LUT4 #(
    .INIT(16'h0004)) 
    \gaxi_full_sm.aw_ready_r_i_9 
       (.I0(s_axi4_awaddr[0]),
        .I1(s_axi4_awvalid),
        .I2(s_axi4_awaddr[2]),
        .I3(s_axi4_awaddr[1]),
        .O(\gaxi_full_sm.aw_ready_r_i_9_n_0 ));
  FDCE #(
    .INIT(1'b0)) 
    \gaxi_full_sm.aw_ready_r_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .CLR(AS),
        .D(aw_ready_c),
        .Q(s_axi4_awready));
  (* SOFT_HLUTNM = "soft_lutpair127" *) 
  LUT4 #(
    .INIT(16'hAAEA)) 
    \gaxi_full_sm.present_state[0]_i_1__0 
       (.I0(\gaxi_full_sm.present_state[0]_i_2_n_0 ),
        .I1(present_state[0]),
        .I2(present_state[1]),
        .I3(s_axi4_bready),
        .O(\gaxi_full_sm.present_state[0]_i_1__0_n_0 ));
  LUT6 #(
    .INIT(64'h50500050EAEACAEA)) 
    \gaxi_full_sm.present_state[0]_i_2 
       (.I0(present_state[0]),
        .I1(axi4_tdfd_en),
        .I2(s_axi4_wvalid),
        .I3(w_last_c__7),
        .I4(bready_timeout_c__1),
        .I5(present_state[1]),
        .O(\gaxi_full_sm.present_state[0]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair127" *) 
  LUT4 #(
    .INIT(16'hAAEA)) 
    \gaxi_full_sm.present_state[1]_i_1__0 
       (.I0(\gaxi_full_sm.present_state[1]_i_2__0_n_0 ),
        .I1(present_state[0]),
        .I2(present_state[1]),
        .I3(s_axi4_bready),
        .O(\gaxi_full_sm.present_state[1]_i_1__0_n_0 ));
  LUT6 #(
    .INIT(64'h50005555A0004444)) 
    \gaxi_full_sm.present_state[1]_i_2__0 
       (.I0(present_state[0]),
        .I1(axi4_tdfd_en),
        .I2(bready_timeout_c__1),
        .I3(w_last_c__7),
        .I4(s_axi4_wvalid),
        .I5(present_state[1]),
        .O(\gaxi_full_sm.present_state[1]_i_2__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair122" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \gaxi_full_sm.present_state[1]_i_3__0 
       (.I0(\bvalid_count_r_reg[0]_0 ),
        .I1(\bvalid_count_r_reg[1]_0 ),
        .I2(\bvalid_count_r_reg[2]_0 ),
        .O(bready_timeout_c__1));
  FDCE #(
    .INIT(1'b0)) 
    \gaxi_full_sm.present_state_reg[0] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .CLR(AS),
        .D(\gaxi_full_sm.present_state[0]_i_1__0_n_0 ),
        .Q(present_state[0]));
  FDCE #(
    .INIT(1'b0)) 
    \gaxi_full_sm.present_state_reg[1] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .CLR(AS),
        .D(\gaxi_full_sm.present_state[1]_i_1__0_n_0 ),
        .Q(present_state[1]));
  (* SOFT_HLUTNM = "soft_lutpair121" *) 
  LUT5 #(
    .INIT(32'hFFFF0770)) 
    \gaxi_full_sm.w_ready_r_i_1 
       (.I0(s_axi4_wvalid),
        .I1(w_last_c__7),
        .I2(present_state[0]),
        .I3(present_state[1]),
        .I4(\gaxi_full_sm.w_ready_r_i_3_n_0 ),
        .O(w_ready_c));
  LUT4 #(
    .INIT(16'h0002)) 
    \gaxi_full_sm.w_ready_r_i_2 
       (.I0(\gaxi_full_sm.w_ready_r_i_4_n_0 ),
        .I1(Q[2]),
        .I2(Q[3]),
        .I3(Q[4]),
        .O(w_last_c__7));
  (* SOFT_HLUTNM = "soft_lutpair124" *) 
  LUT4 #(
    .INIT(16'h002A)) 
    \gaxi_full_sm.w_ready_r_i_3 
       (.I0(axi4_tdfd_en),
        .I1(present_state[0]),
        .I2(\gaxi_full_sm.aw_ready_r_i_3_n_0 ),
        .I3(present_state[1]),
        .O(\gaxi_full_sm.w_ready_r_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000010)) 
    \gaxi_full_sm.w_ready_r_i_4 
       (.I0(Q[5]),
        .I1(Q[6]),
        .I2(s_axi4_wvalid),
        .I3(Q[7]),
        .I4(Q[0]),
        .I5(Q[1]),
        .O(\gaxi_full_sm.w_ready_r_i_4_n_0 ));
  FDCE #(
    .INIT(1'b0)) 
    \gaxi_full_sm.w_ready_r_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .CLR(AS),
        .D(w_ready_c),
        .Q(s_axi4_wready));
  (* SOFT_HLUTNM = "soft_lutpair128" *) 
  LUT3 #(
    .INIT(8'h8B)) 
    \gaxif_wlast_gen.awlen_cntr_r[0]_i_1 
       (.I0(s_axi4_awlen[0]),
        .I1(\gaxi_full_sm.w_ready_r_i_3_n_0 ),
        .I2(Q[0]),
        .O(D[0]));
  (* SOFT_HLUTNM = "soft_lutpair128" *) 
  LUT4 #(
    .INIT(16'hF909)) 
    \gaxif_wlast_gen.awlen_cntr_r[1]_i_1 
       (.I0(Q[0]),
        .I1(Q[1]),
        .I2(\gaxi_full_sm.w_ready_r_i_3_n_0 ),
        .I3(s_axi4_awlen[1]),
        .O(D[1]));
  LUT5 #(
    .INIT(32'hFFA900A9)) 
    \gaxif_wlast_gen.awlen_cntr_r[2]_i_1 
       (.I0(Q[2]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(\gaxi_full_sm.w_ready_r_i_3_n_0 ),
        .I4(s_axi4_awlen[2]),
        .O(D[2]));
  LUT6 #(
    .INIT(64'hFFFFFE010000FE01)) 
    \gaxif_wlast_gen.awlen_cntr_r[3]_i_1 
       (.I0(Q[0]),
        .I1(Q[1]),
        .I2(Q[2]),
        .I3(Q[3]),
        .I4(\gaxi_full_sm.w_ready_r_i_3_n_0 ),
        .I5(s_axi4_awlen[3]),
        .O(D[3]));
  LUT6 #(
    .INIT(64'hFFFFAAA90000AAA9)) 
    \gaxif_wlast_gen.awlen_cntr_r[4]_i_1 
       (.I0(Q[4]),
        .I1(Q[3]),
        .I2(Q[2]),
        .I3(\gaxif_wlast_gen.awlen_cntr_r_reg[1] ),
        .I4(\gaxi_full_sm.w_ready_r_i_3_n_0 ),
        .I5(s_axi4_awlen[4]),
        .O(D[4]));
  LUT4 #(
    .INIT(16'hF606)) 
    \gaxif_wlast_gen.awlen_cntr_r[5]_i_1 
       (.I0(\gaxif_wlast_gen.awlen_cntr_r_reg[0] ),
        .I1(Q[5]),
        .I2(\gaxi_full_sm.w_ready_r_i_3_n_0 ),
        .I3(s_axi4_awlen[5]),
        .O(D[5]));
  LUT4 #(
    .INIT(16'hF909)) 
    \gaxif_wlast_gen.awlen_cntr_r[6]_i_1 
       (.I0(Q[6]),
        .I1(\gaxif_wlast_gen.awlen_cntr_r_reg[5] ),
        .I2(\gaxi_full_sm.w_ready_r_i_3_n_0 ),
        .I3(s_axi4_awlen[6]),
        .O(D[6]));
  (* SOFT_HLUTNM = "soft_lutpair121" *) 
  LUT5 #(
    .INIT(32'hFFFFAEEA)) 
    \gaxif_wlast_gen.awlen_cntr_r[7]_i_1 
       (.I0(w_last_c__7),
        .I1(s_axi4_wvalid),
        .I2(present_state[1]),
        .I3(present_state[0]),
        .I4(\gaxi_full_sm.w_ready_r_i_3_n_0 ),
        .O(E));
  LUT5 #(
    .INIT(32'hFFA900A9)) 
    \gaxif_wlast_gen.awlen_cntr_r[7]_i_2 
       (.I0(Q[7]),
        .I1(\gaxif_wlast_gen.awlen_cntr_r_reg[5] ),
        .I2(Q[6]),
        .I3(\gaxi_full_sm.w_ready_r_i_3_n_0 ),
        .I4(s_axi4_awlen[7]),
        .O(D[7]));
  LUT5 #(
    .INIT(32'hCCECECCC)) 
    \gfifo_gen.gmm2s.gaxi4_strb.input_tstrb[0]_i_1 
       (.I0(s_axi4_wstrb[0]),
        .I1(sig_txd_sb_wr_en_reg),
        .I2(s_axi4_wvalid),
        .I3(present_state[1]),
        .I4(present_state[0]),
        .O(\gfifo_gen.gmm2s.gaxi4_strb.input_tstrb_reg[3] [0]));
  (* SOFT_HLUTNM = "soft_lutpair125" *) 
  LUT5 #(
    .INIT(32'hCCECECCC)) 
    \gfifo_gen.gmm2s.gaxi4_strb.input_tstrb[1]_i_1 
       (.I0(s_axi4_wstrb[1]),
        .I1(sig_txd_sb_wr_en_reg),
        .I2(s_axi4_wvalid),
        .I3(present_state[1]),
        .I4(present_state[0]),
        .O(\gfifo_gen.gmm2s.gaxi4_strb.input_tstrb_reg[3] [1]));
  (* SOFT_HLUTNM = "soft_lutpair126" *) 
  LUT5 #(
    .INIT(32'hCCECECCC)) 
    \gfifo_gen.gmm2s.gaxi4_strb.input_tstrb[2]_i_1 
       (.I0(s_axi4_wstrb[2]),
        .I1(sig_txd_sb_wr_en_reg),
        .I2(s_axi4_wvalid),
        .I3(present_state[1]),
        .I4(present_state[0]),
        .O(\gfifo_gen.gmm2s.gaxi4_strb.input_tstrb_reg[3] [2]));
  (* SOFT_HLUTNM = "soft_lutpair125" *) 
  LUT4 #(
    .INIT(16'hFF60)) 
    \gfifo_gen.gmm2s.gaxi4_strb.input_tstrb[3]_i_1 
       (.I0(present_state[0]),
        .I1(present_state[1]),
        .I2(s_axi4_wvalid),
        .I3(sig_txd_sb_wr_en_reg),
        .O(\gfifo_gen.gmm2s.start_wr_reg ));
  (* SOFT_HLUTNM = "soft_lutpair123" *) 
  LUT5 #(
    .INIT(32'hCCECECCC)) 
    \gfifo_gen.gmm2s.gaxi4_strb.input_tstrb[3]_i_2 
       (.I0(s_axi4_wstrb[3]),
        .I1(sig_txd_sb_wr_en_reg),
        .I2(s_axi4_wvalid),
        .I3(present_state[1]),
        .I4(present_state[0]),
        .O(\gfifo_gen.gmm2s.gaxi4_strb.input_tstrb_reg[3] [3]));
  (* SOFT_HLUTNM = "soft_lutpair123" *) 
  LUT4 #(
    .INIT(16'h0440)) 
    \gfifo_gen.gmm2s.wr_data_int[31]_i_2 
       (.I0(sig_txd_sb_wr_en_reg),
        .I1(s_axi4_wvalid),
        .I2(present_state[1]),
        .I3(present_state[0]),
        .O(txd_wr_en));
  (* SOFT_HLUTNM = "soft_lutpair126" *) 
  LUT3 #(
    .INIT(8'h28)) 
    \gtxd.sig_txd_packet_size[0]_i_2 
       (.I0(s_axi4_wvalid),
        .I1(present_state[1]),
        .I2(present_state[0]),
        .O(p_1_out));
endmodule

(* ORIG_REF_NAME = "axi_write_wrapper" *) 
module axi_fifo_mm_s_0_axi_write_wrapper
   (s_axi4_awready,
    s_axi4_wready,
    s_axi4_bvalid,
    p_1_out,
    E,
    \gfifo_gen.gmm2s.gaxi4_strb.input_tstrb_reg[3] ,
    txd_wr_en,
    s_axi4_bid,
    s_axi_aclk,
    AS,
    s_axi4_bready,
    s_axi4_wvalid,
    sig_txd_sb_wr_en_reg,
    s_axi4_wstrb,
    s_axi4_awlen,
    s_axi4_awaddr,
    s_axi4_awvalid,
    s_axi4_awid,
    ram_full_i_reg);
  output s_axi4_awready;
  output s_axi4_wready;
  output s_axi4_bvalid;
  output p_1_out;
  output [0:0]E;
  output [3:0]\gfifo_gen.gmm2s.gaxi4_strb.input_tstrb_reg[3] ;
  output txd_wr_en;
  output [3:0]s_axi4_bid;
  input s_axi_aclk;
  input [0:0]AS;
  input s_axi4_bready;
  input s_axi4_wvalid;
  input sig_txd_sb_wr_en_reg;
  input [3:0]s_axi4_wstrb;
  input [7:0]s_axi4_awlen;
  input [31:0]s_axi4_awaddr;
  input s_axi4_awvalid;
  input [3:0]s_axi4_awid;
  input [0:0]ram_full_i_reg;

  wire [0:0]AS;
  wire [1:0]CONV_INTEGER;
  wire [0:0]E;
  wire axi_wr_fsm_n_18;
  wire axi_wr_fsm_n_20;
  wire axi_wr_fsm_n_21;
  wire axi_wr_fsm_n_22;
  wire axi_wr_fsm_n_23;
  wire axi_wr_fsm_n_24;
  wire axi_wr_fsm_n_3;
  wire bvalid_c;
  wire \bvalid_count_r_reg_n_0_[0] ;
  wire \bvalid_count_r_reg_n_0_[1] ;
  wire \bvalid_count_r_reg_n_0_[2] ;
  wire bvalid_d1_c;
  wire [1:0]bvalid_rd_cnt_r;
  wire [1:0]bvalid_wr_cnt_r;
  wire \gaxi_bid_gen.axi_bid_array_reg_0_3_0_3_n_0 ;
  wire \gaxi_bid_gen.axi_bid_array_reg_0_3_0_3_n_1 ;
  wire \gaxi_bid_gen.axi_bid_array_reg_0_3_0_3_n_2 ;
  wire \gaxi_bid_gen.axi_bid_array_reg_0_3_0_3_n_3 ;
  wire \gaxi_bvalid_id_r.bvalid_r_i_1_n_0 ;
  wire \gaxif_wlast_gen.awlen_cntr_r[4]_i_2_n_0 ;
  wire \gaxif_wlast_gen.awlen_cntr_r[5]_i_2_n_0 ;
  wire \gaxif_wlast_gen.awlen_cntr_r[7]_i_3_n_0 ;
  wire [7:0]\gaxif_wlast_gen.awlen_cntr_r_reg__0 ;
  wire [3:0]\gfifo_gen.gmm2s.gaxi4_strb.input_tstrb_reg[3] ;
  wire [7:0]p_0_in;
  wire p_1_out;
  wire [0:0]ram_full_i_reg;
  wire [31:0]s_axi4_awaddr;
  wire [3:0]s_axi4_awid;
  wire [7:0]s_axi4_awlen;
  wire s_axi4_awready;
  wire s_axi4_awvalid;
  wire [3:0]s_axi4_bid;
  wire s_axi4_bready;
  wire s_axi4_bvalid;
  wire s_axi4_wready;
  wire [3:0]s_axi4_wstrb;
  wire s_axi4_wvalid;
  wire s_axi_aclk;
  wire sig_txd_sb_wr_en_reg;
  wire txd_wr_en;
  wire [1:0]\NLW_gaxi_bid_gen.axi_bid_array_reg_0_3_0_3_DOC_UNCONNECTED ;
  wire [1:0]\NLW_gaxi_bid_gen.axi_bid_array_reg_0_3_0_3_DOD_UNCONNECTED ;

  axi_fifo_mm_s_0_axi_write_fsm axi_wr_fsm
       (.AS(AS),
        .D(p_0_in),
        .E(axi_wr_fsm_n_3),
        .I95(axi_wr_fsm_n_18),
        .Q(\gaxif_wlast_gen.awlen_cntr_r_reg__0 ),
        .bvalid_c(bvalid_c),
        .\bvalid_count_r_reg[0] (axi_wr_fsm_n_24),
        .\bvalid_count_r_reg[0]_0 (\bvalid_count_r_reg_n_0_[0] ),
        .\bvalid_count_r_reg[1] (axi_wr_fsm_n_23),
        .\bvalid_count_r_reg[1]_0 (\bvalid_count_r_reg_n_0_[1] ),
        .\bvalid_count_r_reg[2] (axi_wr_fsm_n_22),
        .\bvalid_count_r_reg[2]_0 (\bvalid_count_r_reg_n_0_[2] ),
        .\gaxi_bid_gen.bvalid_wr_cnt_r_reg[0] (axi_wr_fsm_n_21),
        .\gaxi_bid_gen.bvalid_wr_cnt_r_reg[1] (axi_wr_fsm_n_20),
        .\gaxi_bid_gen.bvalid_wr_cnt_r_reg[1]_0 (bvalid_wr_cnt_r),
        .\gaxi_bvalid_id_r.bvalid_r_reg (s_axi4_bvalid),
        .\gaxif_wlast_gen.awlen_cntr_r_reg[0] (\gaxif_wlast_gen.awlen_cntr_r[5]_i_2_n_0 ),
        .\gaxif_wlast_gen.awlen_cntr_r_reg[1] (\gaxif_wlast_gen.awlen_cntr_r[4]_i_2_n_0 ),
        .\gaxif_wlast_gen.awlen_cntr_r_reg[5] (\gaxif_wlast_gen.awlen_cntr_r[7]_i_3_n_0 ),
        .\gfifo_gen.gmm2s.gaxi4_strb.input_tstrb_reg[3] (\gfifo_gen.gmm2s.gaxi4_strb.input_tstrb_reg[3] ),
        .\gfifo_gen.gmm2s.start_wr_reg (E),
        .p_1_out(p_1_out),
        .s_axi4_awaddr(s_axi4_awaddr),
        .s_axi4_awlen(s_axi4_awlen),
        .s_axi4_awready(s_axi4_awready),
        .s_axi4_awvalid(s_axi4_awvalid),
        .s_axi4_bready(s_axi4_bready),
        .s_axi4_wready(s_axi4_wready),
        .s_axi4_wstrb(s_axi4_wstrb),
        .s_axi4_wvalid(s_axi4_wvalid),
        .s_axi_aclk(s_axi_aclk),
        .sig_txd_sb_wr_en_reg(sig_txd_sb_wr_en_reg),
        .txd_wr_en(txd_wr_en));
  FDCE #(
    .INIT(1'b0)) 
    \bvalid_count_r_reg[0] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .CLR(AS),
        .D(axi_wr_fsm_n_24),
        .Q(\bvalid_count_r_reg_n_0_[0] ));
  FDCE #(
    .INIT(1'b0)) 
    \bvalid_count_r_reg[1] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .CLR(AS),
        .D(axi_wr_fsm_n_23),
        .Q(\bvalid_count_r_reg_n_0_[1] ));
  FDCE #(
    .INIT(1'b0)) 
    \bvalid_count_r_reg[2] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .CLR(AS),
        .D(axi_wr_fsm_n_22),
        .Q(\bvalid_count_r_reg_n_0_[2] ));
  FDRE #(
    .INIT(1'b0)) 
    \gaxi_bid_gen.S_AXI_BID_reg[0] 
       (.C(s_axi_aclk),
        .CE(ram_full_i_reg),
        .D(\gaxi_bid_gen.axi_bid_array_reg_0_3_0_3_n_1 ),
        .Q(s_axi4_bid[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \gaxi_bid_gen.S_AXI_BID_reg[1] 
       (.C(s_axi_aclk),
        .CE(ram_full_i_reg),
        .D(\gaxi_bid_gen.axi_bid_array_reg_0_3_0_3_n_0 ),
        .Q(s_axi4_bid[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \gaxi_bid_gen.S_AXI_BID_reg[2] 
       (.C(s_axi_aclk),
        .CE(ram_full_i_reg),
        .D(\gaxi_bid_gen.axi_bid_array_reg_0_3_0_3_n_3 ),
        .Q(s_axi4_bid[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \gaxi_bid_gen.S_AXI_BID_reg[3] 
       (.C(s_axi_aclk),
        .CE(ram_full_i_reg),
        .D(\gaxi_bid_gen.axi_bid_array_reg_0_3_0_3_n_2 ),
        .Q(s_axi4_bid[3]),
        .R(1'b0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM32M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000)) 
    \gaxi_bid_gen.axi_bid_array_reg_0_3_0_3 
       (.ADDRA({1'b0,1'b0,1'b0,CONV_INTEGER}),
        .ADDRB({1'b0,1'b0,1'b0,CONV_INTEGER}),
        .ADDRC({1'b0,1'b0,1'b0,CONV_INTEGER}),
        .ADDRD({1'b0,1'b0,1'b0,bvalid_wr_cnt_r}),
        .DIA(s_axi4_awid[1:0]),
        .DIB(s_axi4_awid[3:2]),
        .DIC({1'b0,1'b0}),
        .DID({1'b0,1'b0}),
        .DOA({\gaxi_bid_gen.axi_bid_array_reg_0_3_0_3_n_0 ,\gaxi_bid_gen.axi_bid_array_reg_0_3_0_3_n_1 }),
        .DOB({\gaxi_bid_gen.axi_bid_array_reg_0_3_0_3_n_2 ,\gaxi_bid_gen.axi_bid_array_reg_0_3_0_3_n_3 }),
        .DOC(\NLW_gaxi_bid_gen.axi_bid_array_reg_0_3_0_3_DOC_UNCONNECTED [1:0]),
        .DOD(\NLW_gaxi_bid_gen.axi_bid_array_reg_0_3_0_3_DOD_UNCONNECTED [1:0]),
        .WCLK(s_axi_aclk),
        .WE(axi_wr_fsm_n_18));
  LUT3 #(
    .INIT(8'h6A)) 
    \gaxi_bid_gen.bvalid_rd_cnt_r[0]_i_1 
       (.I0(bvalid_rd_cnt_r[0]),
        .I1(s_axi4_bready),
        .I2(s_axi4_bvalid),
        .O(CONV_INTEGER[0]));
  LUT4 #(
    .INIT(16'h7F80)) 
    \gaxi_bid_gen.bvalid_rd_cnt_r[1]_i_1 
       (.I0(bvalid_rd_cnt_r[0]),
        .I1(s_axi4_bvalid),
        .I2(s_axi4_bready),
        .I3(bvalid_rd_cnt_r[1]),
        .O(CONV_INTEGER[1]));
  FDCE #(
    .INIT(1'b0)) 
    \gaxi_bid_gen.bvalid_rd_cnt_r_reg[0] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .CLR(AS),
        .D(CONV_INTEGER[0]),
        .Q(bvalid_rd_cnt_r[0]));
  FDCE #(
    .INIT(1'b0)) 
    \gaxi_bid_gen.bvalid_rd_cnt_r_reg[1] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .CLR(AS),
        .D(CONV_INTEGER[1]),
        .Q(bvalid_rd_cnt_r[1]));
  FDCE #(
    .INIT(1'b0)) 
    \gaxi_bid_gen.bvalid_wr_cnt_r_reg[0] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .CLR(AS),
        .D(axi_wr_fsm_n_21),
        .Q(bvalid_wr_cnt_r[0]));
  FDCE #(
    .INIT(1'b0)) 
    \gaxi_bid_gen.bvalid_wr_cnt_r_reg[1] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .CLR(AS),
        .D(axi_wr_fsm_n_20),
        .Q(bvalid_wr_cnt_r[1]));
  FDCE #(
    .INIT(1'b0)) 
    \gaxi_bvalid_id_r.bvalid_d1_c_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .CLR(AS),
        .D(bvalid_c),
        .Q(bvalid_d1_c));
  LUT5 #(
    .INIT(32'hFFEFFF00)) 
    \gaxi_bvalid_id_r.bvalid_r_i_1 
       (.I0(\bvalid_count_r_reg_n_0_[2] ),
        .I1(\bvalid_count_r_reg_n_0_[1] ),
        .I2(s_axi4_bready),
        .I3(bvalid_d1_c),
        .I4(s_axi4_bvalid),
        .O(\gaxi_bvalid_id_r.bvalid_r_i_1_n_0 ));
  FDCE #(
    .INIT(1'b0)) 
    \gaxi_bvalid_id_r.bvalid_r_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .CLR(AS),
        .D(\gaxi_bvalid_id_r.bvalid_r_i_1_n_0 ),
        .Q(s_axi4_bvalid));
  (* SOFT_HLUTNM = "soft_lutpair130" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \gaxif_wlast_gen.awlen_cntr_r[4]_i_2 
       (.I0(\gaxif_wlast_gen.awlen_cntr_r_reg__0 [1]),
        .I1(\gaxif_wlast_gen.awlen_cntr_r_reg__0 [0]),
        .O(\gaxif_wlast_gen.awlen_cntr_r[4]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair130" *) 
  LUT5 #(
    .INIT(32'h00000001)) 
    \gaxif_wlast_gen.awlen_cntr_r[5]_i_2 
       (.I0(\gaxif_wlast_gen.awlen_cntr_r_reg__0 [0]),
        .I1(\gaxif_wlast_gen.awlen_cntr_r_reg__0 [1]),
        .I2(\gaxif_wlast_gen.awlen_cntr_r_reg__0 [2]),
        .I3(\gaxif_wlast_gen.awlen_cntr_r_reg__0 [3]),
        .I4(\gaxif_wlast_gen.awlen_cntr_r_reg__0 [4]),
        .O(\gaxif_wlast_gen.awlen_cntr_r[5]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \gaxif_wlast_gen.awlen_cntr_r[7]_i_3 
       (.I0(\gaxif_wlast_gen.awlen_cntr_r_reg__0 [5]),
        .I1(\gaxif_wlast_gen.awlen_cntr_r_reg__0 [4]),
        .I2(\gaxif_wlast_gen.awlen_cntr_r_reg__0 [3]),
        .I3(\gaxif_wlast_gen.awlen_cntr_r_reg__0 [2]),
        .I4(\gaxif_wlast_gen.awlen_cntr_r_reg__0 [1]),
        .I5(\gaxif_wlast_gen.awlen_cntr_r_reg__0 [0]),
        .O(\gaxif_wlast_gen.awlen_cntr_r[7]_i_3_n_0 ));
  FDPE #(
    .INIT(1'b1)) 
    \gaxif_wlast_gen.awlen_cntr_r_reg[0] 
       (.C(s_axi_aclk),
        .CE(axi_wr_fsm_n_3),
        .D(p_0_in[0]),
        .PRE(AS),
        .Q(\gaxif_wlast_gen.awlen_cntr_r_reg__0 [0]));
  FDPE #(
    .INIT(1'b1)) 
    \gaxif_wlast_gen.awlen_cntr_r_reg[1] 
       (.C(s_axi_aclk),
        .CE(axi_wr_fsm_n_3),
        .D(p_0_in[1]),
        .PRE(AS),
        .Q(\gaxif_wlast_gen.awlen_cntr_r_reg__0 [1]));
  FDPE #(
    .INIT(1'b1)) 
    \gaxif_wlast_gen.awlen_cntr_r_reg[2] 
       (.C(s_axi_aclk),
        .CE(axi_wr_fsm_n_3),
        .D(p_0_in[2]),
        .PRE(AS),
        .Q(\gaxif_wlast_gen.awlen_cntr_r_reg__0 [2]));
  FDPE #(
    .INIT(1'b1)) 
    \gaxif_wlast_gen.awlen_cntr_r_reg[3] 
       (.C(s_axi_aclk),
        .CE(axi_wr_fsm_n_3),
        .D(p_0_in[3]),
        .PRE(AS),
        .Q(\gaxif_wlast_gen.awlen_cntr_r_reg__0 [3]));
  FDPE #(
    .INIT(1'b1)) 
    \gaxif_wlast_gen.awlen_cntr_r_reg[4] 
       (.C(s_axi_aclk),
        .CE(axi_wr_fsm_n_3),
        .D(p_0_in[4]),
        .PRE(AS),
        .Q(\gaxif_wlast_gen.awlen_cntr_r_reg__0 [4]));
  FDPE #(
    .INIT(1'b1)) 
    \gaxif_wlast_gen.awlen_cntr_r_reg[5] 
       (.C(s_axi_aclk),
        .CE(axi_wr_fsm_n_3),
        .D(p_0_in[5]),
        .PRE(AS),
        .Q(\gaxif_wlast_gen.awlen_cntr_r_reg__0 [5]));
  FDPE #(
    .INIT(1'b1)) 
    \gaxif_wlast_gen.awlen_cntr_r_reg[6] 
       (.C(s_axi_aclk),
        .CE(axi_wr_fsm_n_3),
        .D(p_0_in[6]),
        .PRE(AS),
        .Q(\gaxif_wlast_gen.awlen_cntr_r_reg__0 [6]));
  FDPE #(
    .INIT(1'b1)) 
    \gaxif_wlast_gen.awlen_cntr_r_reg[7] 
       (.C(s_axi_aclk),
        .CE(axi_wr_fsm_n_3),
        .D(p_0_in[7]),
        .PRE(AS),
        .Q(\gaxif_wlast_gen.awlen_cntr_r_reg__0 [7]));
endmodule

(* ORIG_REF_NAME = "axis_fg" *) 
module axi_fifo_mm_s_0_axis_fg
   (\gaxi_full_sm.w_ready_r_reg ,
    sig_txd_prog_empty,
    p_7_out,
    \axi_str_txd_tdata[31] ,
    DI,
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[8] ,
    \sig_register_array_reg[0][4] ,
    axi_str_txd_tvalid,
    \gaxi_bid_gen.S_AXI_BID_reg[0] ,
    S,
    \gfifo_gen.gmm2s.vacancy_i_reg[9] ,
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[4] ,
    tx_fifo_or,
    D,
    sig_txd_pf_event__1,
    sig_txd_pe_event__1,
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[9] ,
    \gtxc.txc_str_Valid_reg ,
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[4]_0 ,
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[4] ,
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[8]_0 ,
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[8] ,
    \gtxc.TXC_STATE_reg[0] ,
    \gtxc.TXC_STATE_reg[1] ,
    s_axi_aclk,
    Q,
    DIADI,
    txd_wr_en,
    start_wr,
    axi_str_txd_tready,
    sig_tx_channel_reset_reg,
    sig_str_rst_reg,
    s_axi_aresetn,
    axi_str_txc_tlast,
    \gtxc.TXC_STATE_reg[1]_0 ,
    sig_txd_prog_full_d1,
    sig_txd_prog_empty_d1,
    axi_str_txc_tready,
    \gtxc.TXC_STATE_reg[0]_0 ,
    axi_str_txc_tvalid,
    \gtxc.txc_cntr_reg[1] ,
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[7] ,
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[7] );
  output \gaxi_full_sm.w_ready_r_reg ;
  output sig_txd_prog_empty;
  output p_7_out;
  output [32:0]\axi_str_txd_tdata[31] ;
  output [3:0]DI;
  output [4:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[8] ;
  output \sig_register_array_reg[0][4] ;
  output axi_str_txd_tvalid;
  output [0:0]\gaxi_bid_gen.S_AXI_BID_reg[0] ;
  output [0:0]S;
  output [7:0]\gfifo_gen.gmm2s.vacancy_i_reg[9] ;
  output [0:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[4] ;
  output tx_fifo_or;
  output [7:0]D;
  output sig_txd_pf_event__1;
  output sig_txd_pe_event__1;
  output [0:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[9] ;
  output \gtxc.txc_str_Valid_reg ;
  output [3:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[4]_0 ;
  output [3:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[4] ;
  output [3:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[8]_0 ;
  output [3:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[8] ;
  output \gtxc.TXC_STATE_reg[0] ;
  output \gtxc.TXC_STATE_reg[1] ;
  input s_axi_aclk;
  input [31:0]Q;
  input [4:0]DIADI;
  input txd_wr_en;
  input start_wr;
  input axi_str_txd_tready;
  input sig_tx_channel_reset_reg;
  input sig_str_rst_reg;
  input s_axi_aresetn;
  input axi_str_txc_tlast;
  input \gtxc.TXC_STATE_reg[1]_0 ;
  input sig_txd_prog_full_d1;
  input sig_txd_prog_empty_d1;
  input axi_str_txc_tready;
  input \gtxc.TXC_STATE_reg[0]_0 ;
  input axi_str_txc_tvalid;
  input \gtxc.txc_cntr_reg[1] ;
  input [8:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[7] ;
  input [8:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[7] ;

  wire [7:0]D;
  wire [3:0]DI;
  wire [4:0]DIADI;
  wire [31:0]Q;
  wire [0:0]S;
  wire axi_str_txc_tlast;
  wire axi_str_txc_tready;
  wire axi_str_txc_tvalid;
  wire [32:0]\axi_str_txd_tdata[31] ;
  wire axi_str_txd_tready;
  wire axi_str_txd_tvalid;
  wire [0:0]\gaxi_bid_gen.S_AXI_BID_reg[0] ;
  wire \gaxi_full_sm.w_ready_r_reg ;
  wire [3:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[4] ;
  wire [8:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[7] ;
  wire [4:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[8] ;
  wire [3:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[8]_0 ;
  wire [0:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[4] ;
  wire [3:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[4]_0 ;
  wire [8:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[7] ;
  wire [3:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[8] ;
  wire [0:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[9] ;
  wire [7:0]\gfifo_gen.gmm2s.vacancy_i_reg[9] ;
  wire \gtxc.TXC_STATE_reg[0] ;
  wire \gtxc.TXC_STATE_reg[0]_0 ;
  wire \gtxc.TXC_STATE_reg[1] ;
  wire \gtxc.TXC_STATE_reg[1]_0 ;
  wire \gtxc.txc_cntr_reg[1] ;
  wire \gtxc.txc_str_Valid_reg ;
  wire inverted_reset;
  wire p_7_out;
  wire s_aresetn;
  wire s_axi_aclk;
  wire s_axi_aresetn;
  wire \sig_register_array_reg[0][4] ;
  wire sig_str_rst_reg;
  wire sig_tx_channel_reset_reg;
  wire sig_txd_pe_event__1;
  wire sig_txd_pf_event__1;
  wire sig_txd_prog_empty;
  wire sig_txd_prog_empty_d1;
  wire sig_txd_prog_full_d1;
  wire start_wr;
  wire tx_fifo_or;
  wire txd_wr_en;

  axi_fifo_mm_s_0_fifo_generator_v13_0_5 COMP_FIFO
       (.D(D),
        .DI(DI),
        .DIADI(DIADI),
        .Q(Q),
        .S(S),
        .axi_str_txc_tlast(axi_str_txc_tlast),
        .axi_str_txc_tready(axi_str_txc_tready),
        .axi_str_txc_tvalid(axi_str_txc_tvalid),
        .\axi_str_txd_tdata[31] (\axi_str_txd_tdata[31] ),
        .axi_str_txd_tready(axi_str_txd_tready),
        .axi_str_txd_tvalid(axi_str_txd_tvalid),
        .\gaxi_bid_gen.S_AXI_BID_reg[0] (\gaxi_bid_gen.S_AXI_BID_reg[0] ),
        .\gaxi_full_sm.w_ready_r_reg (\gaxi_full_sm.w_ready_r_reg ),
        .\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[4] (\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[4] ),
        .\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[7] (\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[7] ),
        .\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[8] (\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[8] ),
        .\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[8]_0 (\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[8]_0 ),
        .\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[4] (\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[4] ),
        .\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[4]_0 (\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[4]_0 ),
        .\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[7] (\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[7] ),
        .\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[8] (\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[8] ),
        .\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[9] (\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[9] ),
        .\gfifo_gen.gmm2s.vacancy_i_reg[9] (\gfifo_gen.gmm2s.vacancy_i_reg[9] ),
        .\gtxc.TXC_STATE_reg[0] (\gtxc.TXC_STATE_reg[0] ),
        .\gtxc.TXC_STATE_reg[0]_0 (\gtxc.TXC_STATE_reg[0]_0 ),
        .\gtxc.TXC_STATE_reg[1] (\gtxc.TXC_STATE_reg[1] ),
        .\gtxc.TXC_STATE_reg[1]_0 (\gtxc.TXC_STATE_reg[1]_0 ),
        .\gtxc.txc_cntr_reg[1] (\gtxc.txc_cntr_reg[1] ),
        .\gtxc.txc_str_Valid_reg (\gtxc.txc_str_Valid_reg ),
        .inverted_reset(inverted_reset),
        .p_7_out(p_7_out),
        .s_axi_aclk(s_axi_aclk),
        .\sig_register_array_reg[0][4] (\sig_register_array_reg[0][4] ),
        .sig_txd_pe_event__1(sig_txd_pe_event__1),
        .sig_txd_pf_event__1(sig_txd_pf_event__1),
        .sig_txd_prog_empty(sig_txd_prog_empty),
        .sig_txd_prog_empty_d1(sig_txd_prog_empty_d1),
        .sig_txd_prog_full_d1(sig_txd_prog_full_d1),
        .start_wr(start_wr),
        .tx_fifo_or(tx_fifo_or),
        .txd_wr_en(txd_wr_en));
  LUT3 #(
    .INIT(8'hEF)) 
    sync_areset_n_inv_i_1__0
       (.I0(sig_tx_channel_reset_reg),
        .I1(sig_str_rst_reg),
        .I2(s_axi_aresetn),
        .O(s_aresetn));
  FDRE #(
    .INIT(1'b1)) 
    sync_areset_n_reg_inv
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(s_aresetn),
        .Q(inverted_reset),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "axis_fg" *) 
module axi_fifo_mm_s_0_axis_fg__parameterized0
   (\grxd.fg_rxd_wr_length_reg[1] ,
    empty_fwft_i,
    p_9_out,
    p_8_out,
    \grxd.fg_rxd_wr_length_reg[21] ,
    rx_complete,
    rx_str_wr_en,
    \grxd.sig_rxd_rd_data_reg[32] ,
    \gc0.count_reg[0] ,
    \gc0.count_reg[0]_0 ,
    axi_str_rxd_tready,
    DI,
    Q,
    \sig_register_array_reg[0][2] ,
    \sig_register_array_reg[0][1] ,
    \sig_register_array_reg[0][2]_0 ,
    sig_rxd_pf_event__1,
    sig_rxd_pe_event__1,
    S,
    \count_reg[4] ,
    \count_reg[8] ,
    \grxd.fg_rxd_wr_length_reg[2] ,
    s_axi4_rdata,
    s_axi_aclk,
    axi_str_rxd_tdata,
    axi_str_rxd_tlast,
    axis_rd_en__0,
    axi_str_rxd_tvalid,
    rx_len_wr_en,
    s_axi_aresetn,
    sig_str_rst_reg,
    sig_rxd_rd_data,
    axi4_fifo_rd_en_i,
    sig_rd_rlen_reg,
    \gaxi_full_sm.r_valid_r_reg ,
    \gaxi_full_sm.r_last_r_reg ,
    sig_axi_rd_en,
    \s_axi4_araddr[21] ,
    \s_axi4_araddr[29] ,
    s_axi4_araddr,
    sig_rx_channel_reset_reg,
    sig_rxd_prog_full_d1,
    sig_rxd_prog_empty_d1,
    O,
    \grxd.fg_rxd_wr_length_reg[2]_0 ,
    fg_rxd_wr_length,
    D);
  output \grxd.fg_rxd_wr_length_reg[1] ;
  output empty_fwft_i;
  output p_9_out;
  output p_8_out;
  output \grxd.fg_rxd_wr_length_reg[21] ;
  output rx_complete;
  output rx_str_wr_en;
  output \grxd.sig_rxd_rd_data_reg[32] ;
  output \gc0.count_reg[0] ;
  output \gc0.count_reg[0]_0 ;
  output axi_str_rxd_tready;
  output [3:0]DI;
  output [6:0]Q;
  output \sig_register_array_reg[0][2] ;
  output \sig_register_array_reg[0][1] ;
  output \sig_register_array_reg[0][2]_0 ;
  output sig_rxd_pf_event__1;
  output sig_rxd_pe_event__1;
  output [0:0]S;
  output [3:0]\count_reg[4] ;
  output [3:0]\count_reg[8] ;
  output \grxd.fg_rxd_wr_length_reg[2] ;
  output [31:0]s_axi4_rdata;
  input s_axi_aclk;
  input [31:0]axi_str_rxd_tdata;
  input axi_str_rxd_tlast;
  input axis_rd_en__0;
  input axi_str_rxd_tvalid;
  input rx_len_wr_en;
  input s_axi_aresetn;
  input sig_str_rst_reg;
  input [0:0]sig_rxd_rd_data;
  input axi4_fifo_rd_en_i;
  input sig_rd_rlen_reg;
  input \gaxi_full_sm.r_valid_r_reg ;
  input \gaxi_full_sm.r_last_r_reg ;
  input sig_axi_rd_en;
  input \s_axi4_araddr[21] ;
  input \s_axi4_araddr[29] ;
  input [11:0]s_axi4_araddr;
  input sig_rx_channel_reset_reg;
  input sig_rxd_prog_full_d1;
  input sig_rxd_prog_empty_d1;
  input [0:0]O;
  input [0:0]\grxd.fg_rxd_wr_length_reg[2]_0 ;
  input [0:0]fg_rxd_wr_length;
  input [8:0]D;

  wire [8:0]D;
  wire [3:0]DI;
  wire [0:0]O;
  wire [6:0]Q;
  wire [0:0]S;
  wire axi4_fifo_rd_en_i;
  wire [31:0]axi_str_rxd_tdata;
  wire axi_str_rxd_tlast;
  wire axi_str_rxd_tready;
  wire axi_str_rxd_tvalid;
  wire axis_rd_en__0;
  wire [3:0]\count_reg[4] ;
  wire [3:0]\count_reg[8] ;
  wire empty_fwft_i;
  wire [0:0]fg_rxd_wr_length;
  wire \gaxi_full_sm.r_last_r_reg ;
  wire \gaxi_full_sm.r_valid_r_reg ;
  wire \gc0.count_reg[0] ;
  wire \gc0.count_reg[0]_0 ;
  wire \grxd.fg_rxd_wr_length_reg[1] ;
  wire \grxd.fg_rxd_wr_length_reg[21] ;
  wire \grxd.fg_rxd_wr_length_reg[2] ;
  wire [0:0]\grxd.fg_rxd_wr_length_reg[2]_0 ;
  wire \grxd.sig_rxd_rd_data_reg[32] ;
  wire p_8_out;
  wire p_9_out;
  wire rx_complete;
  wire rx_len_wr_en;
  wire rx_str_wr_en;
  wire [11:0]s_axi4_araddr;
  wire \s_axi4_araddr[21] ;
  wire \s_axi4_araddr[29] ;
  wire [31:0]s_axi4_rdata;
  wire s_axi_aclk;
  wire s_axi_aresetn;
  wire sig_axi_rd_en;
  wire sig_rd_rlen_reg;
  wire \sig_register_array_reg[0][1] ;
  wire \sig_register_array_reg[0][2] ;
  wire \sig_register_array_reg[0][2]_0 ;
  wire sig_rx_channel_reset_reg;
  wire sig_rxd_pe_event__1;
  wire sig_rxd_pf_event__1;
  wire sig_rxd_prog_empty_d1;
  wire sig_rxd_prog_full_d1;
  wire [0:0]sig_rxd_rd_data;
  wire sig_str_rst_reg;
  wire sync_areset_n_inv_i_1_n_0;
  wire sync_areset_n_reg_inv_n_0;

  axi_fifo_mm_s_0_fifo_generator_v13_0_5__parameterized0 COMP_FIFO
       (.D(D),
        .DI(DI),
        .O(O),
        .Q(Q),
        .S(S),
        .axi4_fifo_rd_en_i(axi4_fifo_rd_en_i),
        .axi_str_rxd_tdata(axi_str_rxd_tdata),
        .axi_str_rxd_tlast(axi_str_rxd_tlast),
        .axi_str_rxd_tready(axi_str_rxd_tready),
        .axi_str_rxd_tvalid(axi_str_rxd_tvalid),
        .axis_rd_en__0(axis_rd_en__0),
        .\count_reg[4] (\count_reg[4] ),
        .\count_reg[8] (\count_reg[8] ),
        .empty_fwft_i(empty_fwft_i),
        .fg_rxd_wr_length(fg_rxd_wr_length),
        .\gaxi_full_sm.r_last_r_reg (\gaxi_full_sm.r_last_r_reg ),
        .\gaxi_full_sm.r_valid_r_reg (\gaxi_full_sm.r_valid_r_reg ),
        .\gc0.count_reg[0] (\gc0.count_reg[0] ),
        .\gc0.count_reg[0]_0 (\gc0.count_reg[0]_0 ),
        .\grxd.fg_rxd_wr_length_reg[1] (\grxd.fg_rxd_wr_length_reg[1] ),
        .\grxd.fg_rxd_wr_length_reg[21] (\grxd.fg_rxd_wr_length_reg[21] ),
        .\grxd.fg_rxd_wr_length_reg[2] (\grxd.fg_rxd_wr_length_reg[2] ),
        .\grxd.fg_rxd_wr_length_reg[2]_0 (\grxd.fg_rxd_wr_length_reg[2]_0 ),
        .\grxd.sig_rxd_rd_data_reg[32] (\grxd.sig_rxd_rd_data_reg[32] ),
        .p_8_out(p_8_out),
        .p_9_out(p_9_out),
        .rx_complete(rx_complete),
        .rx_len_wr_en(rx_len_wr_en),
        .rx_str_wr_en(rx_str_wr_en),
        .s_axi4_araddr(s_axi4_araddr),
        .\s_axi4_araddr[21] (\s_axi4_araddr[21] ),
        .\s_axi4_araddr[29] (\s_axi4_araddr[29] ),
        .s_axi4_rdata(s_axi4_rdata),
        .s_axi_aclk(s_axi_aclk),
        .s_axi_aresetn(s_axi_aresetn),
        .sig_axi_rd_en(sig_axi_rd_en),
        .sig_rd_rlen_reg(sig_rd_rlen_reg),
        .\sig_register_array_reg[0][1] (\sig_register_array_reg[0][1] ),
        .\sig_register_array_reg[0][2] (\sig_register_array_reg[0][2] ),
        .\sig_register_array_reg[0][2]_0 (\sig_register_array_reg[0][2]_0 ),
        .sig_rx_channel_reset_reg(sig_rx_channel_reset_reg),
        .sig_rxd_pe_event__1(sig_rxd_pe_event__1),
        .sig_rxd_pf_event__1(sig_rxd_pf_event__1),
        .sig_rxd_prog_empty_d1(sig_rxd_prog_empty_d1),
        .sig_rxd_prog_full_d1(sig_rxd_prog_full_d1),
        .sig_rxd_rd_data(sig_rxd_rd_data),
        .sig_str_rst_reg(sig_str_rst_reg),
        .sync_areset_n_reg_inv(sync_areset_n_reg_inv_n_0));
  LUT3 #(
    .INIT(8'hEF)) 
    sync_areset_n_inv_i_1
       (.I0(sig_rx_channel_reset_reg),
        .I1(sig_str_rst_reg),
        .I2(s_axi_aresetn),
        .O(sync_areset_n_inv_i_1_n_0));
  FDRE #(
    .INIT(1'b1)) 
    sync_areset_n_reg_inv
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(sync_areset_n_inv_i_1_n_0),
        .Q(sync_areset_n_reg_inv_n_0),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "fifo" *) 
module axi_fifo_mm_s_0_fifo
   (\gaxi_full_sm.w_ready_r_reg ,
    SR,
    sig_txd_prog_empty,
    p_7_out,
    Q,
    DI,
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[8] ,
    \sig_register_array_reg[0][4] ,
    axi_str_txd_tvalid,
    \gaxi_bid_gen.S_AXI_BID_reg[0] ,
    S,
    \gfifo_gen.gmm2s.vacancy_i_reg[9]_0 ,
    \sig_ip2bus_data_reg[29] ,
    \sig_ip2bus_data_reg[28] ,
    \sig_ip2bus_data_reg[27] ,
    \sig_ip2bus_data_reg[26] ,
    \sig_ip2bus_data_reg[25] ,
    \sig_ip2bus_data_reg[24] ,
    \sig_ip2bus_data_reg[23] ,
    \sig_ip2bus_data_reg[22] ,
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[4] ,
    tx_fifo_or,
    sig_txd_pf_event__1,
    sig_txd_pe_event__1,
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[9] ,
    \gtxc.txc_str_Valid_reg ,
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[4]_0 ,
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[4] ,
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[8]_0 ,
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[8] ,
    \gtxc.TXC_STATE_reg[0] ,
    \gtxc.TXC_STATE_reg[1] ,
    \sig_ip2bus_data_reg[30] ,
    s_axi_aclk,
    DIADI,
    E,
    txd_wr_en,
    axi_str_txd_tready,
    sig_tx_channel_reset_reg,
    sig_str_rst_reg,
    s_axi_aresetn,
    \GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg[9] ,
    \count_reg[9] ,
    \GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg[7] ,
    axi_str_txc_tlast,
    \gtxc.TXC_STATE_reg[1]_0 ,
    \sig_txd_wr_data_reg[31] ,
    s_axi4_wdata,
    sig_txd_prog_full_d1,
    sig_txd_prog_empty_d1,
    axi_str_txc_tready,
    \gtxc.TXC_STATE_reg[0]_0 ,
    axi_str_txc_tvalid,
    \gtxc.txc_cntr_reg[1] ,
    sig_txd_sb_wr_en_reg,
    D,
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[7] );
  output \gaxi_full_sm.w_ready_r_reg ;
  output [0:0]SR;
  output sig_txd_prog_empty;
  output p_7_out;
  output [32:0]Q;
  output [3:0]DI;
  output [4:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[8] ;
  output \sig_register_array_reg[0][4] ;
  output axi_str_txd_tvalid;
  output [0:0]\gaxi_bid_gen.S_AXI_BID_reg[0] ;
  output [0:0]S;
  output [7:0]\gfifo_gen.gmm2s.vacancy_i_reg[9]_0 ;
  output \sig_ip2bus_data_reg[29] ;
  output \sig_ip2bus_data_reg[28] ;
  output \sig_ip2bus_data_reg[27] ;
  output \sig_ip2bus_data_reg[26] ;
  output \sig_ip2bus_data_reg[25] ;
  output \sig_ip2bus_data_reg[24] ;
  output \sig_ip2bus_data_reg[23] ;
  output \sig_ip2bus_data_reg[22] ;
  output [0:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[4] ;
  output tx_fifo_or;
  output sig_txd_pf_event__1;
  output sig_txd_pe_event__1;
  output [0:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[9] ;
  output \gtxc.txc_str_Valid_reg ;
  output [3:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[4]_0 ;
  output [3:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[4] ;
  output [3:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[8]_0 ;
  output [3:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[8] ;
  output \gtxc.TXC_STATE_reg[0] ;
  output \gtxc.TXC_STATE_reg[1] ;
  output [0:0]\sig_ip2bus_data_reg[30] ;
  input s_axi_aclk;
  input [0:0]DIADI;
  input [0:0]E;
  input txd_wr_en;
  input axi_str_txd_tready;
  input sig_tx_channel_reset_reg;
  input sig_str_rst_reg;
  input s_axi_aresetn;
  input \GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg[9] ;
  input [7:0]\count_reg[9] ;
  input \GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg[7] ;
  input axi_str_txc_tlast;
  input \gtxc.TXC_STATE_reg[1]_0 ;
  input [31:0]\sig_txd_wr_data_reg[31] ;
  input [31:0]s_axi4_wdata;
  input sig_txd_prog_full_d1;
  input sig_txd_prog_empty_d1;
  input axi_str_txc_tready;
  input \gtxc.TXC_STATE_reg[0]_0 ;
  input axi_str_txc_tvalid;
  input \gtxc.txc_cntr_reg[1] ;
  input [3:0]sig_txd_sb_wr_en_reg;
  input [8:0]D;
  input [8:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[7] ;

  wire [8:0]D;
  wire [3:0]DI;
  wire [0:0]DIADI;
  wire [0:0]E;
  wire \GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg[7] ;
  wire \GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg[9] ;
  wire [32:0]Q;
  wire [0:0]S;
  wire [0:0]SR;
  wire axi_str_txc_tlast;
  wire axi_str_txc_tready;
  wire axi_str_txc_tvalid;
  wire axi_str_txd_tready;
  wire axi_str_txd_tvalid;
  wire [7:0]\count_reg[9] ;
  wire [0:0]\gaxi_bid_gen.S_AXI_BID_reg[0] ;
  wire \gaxi_full_sm.w_ready_r_reg ;
  wire [3:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[4] ;
  wire [4:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[8] ;
  wire [3:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[8]_0 ;
  wire [0:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[4] ;
  wire [3:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[4]_0 ;
  wire [8:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[7] ;
  wire [3:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[8] ;
  wire [0:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[9] ;
  wire [7:0]\gfifo_gen.gmm2s.vacancy_i_reg[9]_0 ;
  wire \gtxc.TXC_STATE_reg[0] ;
  wire \gtxc.TXC_STATE_reg[0]_0 ;
  wire \gtxc.TXC_STATE_reg[1] ;
  wire \gtxc.TXC_STATE_reg[1]_0 ;
  wire \gtxc.txc_cntr_reg[1] ;
  wire \gtxc.txc_str_Valid_reg ;
  wire [3:0]input_tstrb;
  wire [9:2]minusOp;
  wire p_7_out;
  wire [31:0]s_axi4_wdata;
  wire s_axi_aclk;
  wire s_axi_aresetn;
  wire \sig_ip2bus_data_reg[22] ;
  wire \sig_ip2bus_data_reg[23] ;
  wire \sig_ip2bus_data_reg[24] ;
  wire \sig_ip2bus_data_reg[25] ;
  wire \sig_ip2bus_data_reg[26] ;
  wire \sig_ip2bus_data_reg[27] ;
  wire \sig_ip2bus_data_reg[28] ;
  wire \sig_ip2bus_data_reg[29] ;
  wire [0:0]\sig_ip2bus_data_reg[30] ;
  wire \sig_register_array_reg[0][4] ;
  wire sig_str_rst_reg;
  wire sig_tx_channel_reset_reg;
  wire sig_txd_pe_event__1;
  wire sig_txd_pf_event__1;
  wire sig_txd_prog_empty;
  wire sig_txd_prog_empty_d1;
  wire sig_txd_prog_full_d1;
  wire [3:0]sig_txd_sb_wr_en_reg;
  wire [31:0]\sig_txd_wr_data_reg[31] ;
  wire start_wr;
  wire tx_fifo_or;
  wire [31:0]txd_wr_data;
  wire txd_wr_en;
  wire [9:2]vacancy_i;
  wire [31:0]wr_data_int;

  axi_fifo_mm_s_0_axis_fg \gfifo_gen.COMP_AXIS_FG_FIFO 
       (.D(minusOp),
        .DI(DI),
        .DIADI({input_tstrb,DIADI}),
        .Q(wr_data_int),
        .S(S),
        .axi_str_txc_tlast(axi_str_txc_tlast),
        .axi_str_txc_tready(axi_str_txc_tready),
        .axi_str_txc_tvalid(axi_str_txc_tvalid),
        .\axi_str_txd_tdata[31] (Q),
        .axi_str_txd_tready(axi_str_txd_tready),
        .axi_str_txd_tvalid(axi_str_txd_tvalid),
        .\gaxi_bid_gen.S_AXI_BID_reg[0] (\gaxi_bid_gen.S_AXI_BID_reg[0] ),
        .\gaxi_full_sm.w_ready_r_reg (\gaxi_full_sm.w_ready_r_reg ),
        .\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[4] (\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[4] ),
        .\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[7] (D),
        .\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[8] (\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[8] ),
        .\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[8]_0 (\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[8]_0 ),
        .\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[4] (\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[4] ),
        .\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[4]_0 (\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[4]_0 ),
        .\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[7] (\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[7] ),
        .\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[8] (\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[8] ),
        .\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[9] (\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[9] ),
        .\gfifo_gen.gmm2s.vacancy_i_reg[9] (\gfifo_gen.gmm2s.vacancy_i_reg[9]_0 ),
        .\gtxc.TXC_STATE_reg[0] (\gtxc.TXC_STATE_reg[0] ),
        .\gtxc.TXC_STATE_reg[0]_0 (\gtxc.TXC_STATE_reg[0]_0 ),
        .\gtxc.TXC_STATE_reg[1] (\gtxc.TXC_STATE_reg[1] ),
        .\gtxc.TXC_STATE_reg[1]_0 (\gtxc.TXC_STATE_reg[1]_0 ),
        .\gtxc.txc_cntr_reg[1] (\gtxc.txc_cntr_reg[1] ),
        .\gtxc.txc_str_Valid_reg (\gtxc.txc_str_Valid_reg ),
        .p_7_out(p_7_out),
        .s_axi_aclk(s_axi_aclk),
        .s_axi_aresetn(s_axi_aresetn),
        .\sig_register_array_reg[0][4] (\sig_register_array_reg[0][4] ),
        .sig_str_rst_reg(sig_str_rst_reg),
        .sig_tx_channel_reset_reg(sig_tx_channel_reset_reg),
        .sig_txd_pe_event__1(sig_txd_pe_event__1),
        .sig_txd_pf_event__1(sig_txd_pf_event__1),
        .sig_txd_prog_empty(sig_txd_prog_empty),
        .sig_txd_prog_empty_d1(sig_txd_prog_empty_d1),
        .sig_txd_prog_full_d1(sig_txd_prog_full_d1),
        .start_wr(start_wr),
        .tx_fifo_or(tx_fifo_or),
        .txd_wr_en(txd_wr_en));
  FDSE #(
    .INIT(1'b0)) 
    \gfifo_gen.gmm2s.gaxi4_strb.input_tstrb_reg[0] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(sig_txd_sb_wr_en_reg[0]),
        .Q(input_tstrb[0]),
        .S(SR));
  FDSE #(
    .INIT(1'b0)) 
    \gfifo_gen.gmm2s.gaxi4_strb.input_tstrb_reg[1] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(sig_txd_sb_wr_en_reg[1]),
        .Q(input_tstrb[1]),
        .S(SR));
  FDSE #(
    .INIT(1'b0)) 
    \gfifo_gen.gmm2s.gaxi4_strb.input_tstrb_reg[2] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(sig_txd_sb_wr_en_reg[2]),
        .Q(input_tstrb[2]),
        .S(SR));
  FDSE #(
    .INIT(1'b0)) 
    \gfifo_gen.gmm2s.gaxi4_strb.input_tstrb_reg[3] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(sig_txd_sb_wr_en_reg[3]),
        .Q(input_tstrb[3]),
        .S(SR));
  FDRE #(
    .INIT(1'b0)) 
    \gfifo_gen.gmm2s.start_wr_reg 
       (.C(s_axi_aclk),
        .CE(E),
        .D(txd_wr_en),
        .Q(start_wr),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \gfifo_gen.gmm2s.vacancy_i_reg[1] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(\gfifo_gen.gmm2s.vacancy_i_reg[9]_0 [1]),
        .Q(\sig_ip2bus_data_reg[30] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \gfifo_gen.gmm2s.vacancy_i_reg[2] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(minusOp[2]),
        .Q(vacancy_i[2]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \gfifo_gen.gmm2s.vacancy_i_reg[3] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(minusOp[3]),
        .Q(vacancy_i[3]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \gfifo_gen.gmm2s.vacancy_i_reg[4] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(minusOp[4]),
        .Q(vacancy_i[4]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \gfifo_gen.gmm2s.vacancy_i_reg[5] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(minusOp[5]),
        .Q(vacancy_i[5]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \gfifo_gen.gmm2s.vacancy_i_reg[6] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(minusOp[6]),
        .Q(vacancy_i[6]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \gfifo_gen.gmm2s.vacancy_i_reg[7] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(minusOp[7]),
        .Q(vacancy_i[7]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \gfifo_gen.gmm2s.vacancy_i_reg[8] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(minusOp[8]),
        .Q(vacancy_i[8]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \gfifo_gen.gmm2s.vacancy_i_reg[9] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(minusOp[9]),
        .Q(vacancy_i[9]),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \gfifo_gen.gmm2s.wr_data_int[0]_i_1 
       (.I0(\sig_txd_wr_data_reg[31] [0]),
        .I1(DIADI),
        .I2(s_axi4_wdata[0]),
        .O(txd_wr_data[0]));
  (* SOFT_HLUTNM = "soft_lutpair40" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \gfifo_gen.gmm2s.wr_data_int[10]_i_1 
       (.I0(\sig_txd_wr_data_reg[31] [10]),
        .I1(DIADI),
        .I2(s_axi4_wdata[10]),
        .O(txd_wr_data[10]));
  (* SOFT_HLUTNM = "soft_lutpair40" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \gfifo_gen.gmm2s.wr_data_int[11]_i_1 
       (.I0(\sig_txd_wr_data_reg[31] [11]),
        .I1(DIADI),
        .I2(s_axi4_wdata[11]),
        .O(txd_wr_data[11]));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \gfifo_gen.gmm2s.wr_data_int[12]_i_1 
       (.I0(\sig_txd_wr_data_reg[31] [12]),
        .I1(DIADI),
        .I2(s_axi4_wdata[12]),
        .O(txd_wr_data[12]));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \gfifo_gen.gmm2s.wr_data_int[13]_i_1 
       (.I0(\sig_txd_wr_data_reg[31] [13]),
        .I1(DIADI),
        .I2(s_axi4_wdata[13]),
        .O(txd_wr_data[13]));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \gfifo_gen.gmm2s.wr_data_int[14]_i_1 
       (.I0(\sig_txd_wr_data_reg[31] [14]),
        .I1(DIADI),
        .I2(s_axi4_wdata[14]),
        .O(txd_wr_data[14]));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \gfifo_gen.gmm2s.wr_data_int[15]_i_1 
       (.I0(\sig_txd_wr_data_reg[31] [15]),
        .I1(DIADI),
        .I2(s_axi4_wdata[15]),
        .O(txd_wr_data[15]));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \gfifo_gen.gmm2s.wr_data_int[16]_i_1 
       (.I0(\sig_txd_wr_data_reg[31] [16]),
        .I1(DIADI),
        .I2(s_axi4_wdata[16]),
        .O(txd_wr_data[16]));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \gfifo_gen.gmm2s.wr_data_int[17]_i_1 
       (.I0(\sig_txd_wr_data_reg[31] [17]),
        .I1(DIADI),
        .I2(s_axi4_wdata[17]),
        .O(txd_wr_data[17]));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \gfifo_gen.gmm2s.wr_data_int[18]_i_1 
       (.I0(\sig_txd_wr_data_reg[31] [18]),
        .I1(DIADI),
        .I2(s_axi4_wdata[18]),
        .O(txd_wr_data[18]));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \gfifo_gen.gmm2s.wr_data_int[19]_i_1 
       (.I0(\sig_txd_wr_data_reg[31] [19]),
        .I1(DIADI),
        .I2(s_axi4_wdata[19]),
        .O(txd_wr_data[19]));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \gfifo_gen.gmm2s.wr_data_int[1]_i_1 
       (.I0(\sig_txd_wr_data_reg[31] [1]),
        .I1(DIADI),
        .I2(s_axi4_wdata[1]),
        .O(txd_wr_data[1]));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \gfifo_gen.gmm2s.wr_data_int[20]_i_1 
       (.I0(\sig_txd_wr_data_reg[31] [20]),
        .I1(DIADI),
        .I2(s_axi4_wdata[20]),
        .O(txd_wr_data[20]));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \gfifo_gen.gmm2s.wr_data_int[21]_i_1 
       (.I0(\sig_txd_wr_data_reg[31] [21]),
        .I1(DIADI),
        .I2(s_axi4_wdata[21]),
        .O(txd_wr_data[21]));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \gfifo_gen.gmm2s.wr_data_int[22]_i_1 
       (.I0(\sig_txd_wr_data_reg[31] [22]),
        .I1(DIADI),
        .I2(s_axi4_wdata[22]),
        .O(txd_wr_data[22]));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \gfifo_gen.gmm2s.wr_data_int[23]_i_1 
       (.I0(\sig_txd_wr_data_reg[31] [23]),
        .I1(DIADI),
        .I2(s_axi4_wdata[23]),
        .O(txd_wr_data[23]));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \gfifo_gen.gmm2s.wr_data_int[24]_i_1 
       (.I0(\sig_txd_wr_data_reg[31] [24]),
        .I1(DIADI),
        .I2(s_axi4_wdata[24]),
        .O(txd_wr_data[24]));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \gfifo_gen.gmm2s.wr_data_int[25]_i_1 
       (.I0(\sig_txd_wr_data_reg[31] [25]),
        .I1(DIADI),
        .I2(s_axi4_wdata[25]),
        .O(txd_wr_data[25]));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \gfifo_gen.gmm2s.wr_data_int[26]_i_1 
       (.I0(\sig_txd_wr_data_reg[31] [26]),
        .I1(DIADI),
        .I2(s_axi4_wdata[26]),
        .O(txd_wr_data[26]));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \gfifo_gen.gmm2s.wr_data_int[27]_i_1 
       (.I0(\sig_txd_wr_data_reg[31] [27]),
        .I1(DIADI),
        .I2(s_axi4_wdata[27]),
        .O(txd_wr_data[27]));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \gfifo_gen.gmm2s.wr_data_int[28]_i_1 
       (.I0(\sig_txd_wr_data_reg[31] [28]),
        .I1(DIADI),
        .I2(s_axi4_wdata[28]),
        .O(txd_wr_data[28]));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \gfifo_gen.gmm2s.wr_data_int[29]_i_1 
       (.I0(\sig_txd_wr_data_reg[31] [29]),
        .I1(DIADI),
        .I2(s_axi4_wdata[29]),
        .O(txd_wr_data[29]));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \gfifo_gen.gmm2s.wr_data_int[2]_i_1 
       (.I0(\sig_txd_wr_data_reg[31] [2]),
        .I1(DIADI),
        .I2(s_axi4_wdata[2]),
        .O(txd_wr_data[2]));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \gfifo_gen.gmm2s.wr_data_int[30]_i_1 
       (.I0(\sig_txd_wr_data_reg[31] [30]),
        .I1(DIADI),
        .I2(s_axi4_wdata[30]),
        .O(txd_wr_data[30]));
  LUT3 #(
    .INIT(8'hFD)) 
    \gfifo_gen.gmm2s.wr_data_int[31]_i_1 
       (.I0(s_axi_aresetn),
        .I1(sig_str_rst_reg),
        .I2(sig_tx_channel_reset_reg),
        .O(SR));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \gfifo_gen.gmm2s.wr_data_int[31]_i_3 
       (.I0(\sig_txd_wr_data_reg[31] [31]),
        .I1(DIADI),
        .I2(s_axi4_wdata[31]),
        .O(txd_wr_data[31]));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \gfifo_gen.gmm2s.wr_data_int[3]_i_1 
       (.I0(\sig_txd_wr_data_reg[31] [3]),
        .I1(DIADI),
        .I2(s_axi4_wdata[3]),
        .O(txd_wr_data[3]));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \gfifo_gen.gmm2s.wr_data_int[4]_i_1 
       (.I0(\sig_txd_wr_data_reg[31] [4]),
        .I1(DIADI),
        .I2(s_axi4_wdata[4]),
        .O(txd_wr_data[4]));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \gfifo_gen.gmm2s.wr_data_int[5]_i_1 
       (.I0(\sig_txd_wr_data_reg[31] [5]),
        .I1(DIADI),
        .I2(s_axi4_wdata[5]),
        .O(txd_wr_data[5]));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \gfifo_gen.gmm2s.wr_data_int[6]_i_1 
       (.I0(\sig_txd_wr_data_reg[31] [6]),
        .I1(DIADI),
        .I2(s_axi4_wdata[6]),
        .O(txd_wr_data[6]));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \gfifo_gen.gmm2s.wr_data_int[7]_i_1 
       (.I0(\sig_txd_wr_data_reg[31] [7]),
        .I1(DIADI),
        .I2(s_axi4_wdata[7]),
        .O(txd_wr_data[7]));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \gfifo_gen.gmm2s.wr_data_int[8]_i_1 
       (.I0(\sig_txd_wr_data_reg[31] [8]),
        .I1(DIADI),
        .I2(s_axi4_wdata[8]),
        .O(txd_wr_data[8]));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \gfifo_gen.gmm2s.wr_data_int[9]_i_1 
       (.I0(\sig_txd_wr_data_reg[31] [9]),
        .I1(DIADI),
        .I2(s_axi4_wdata[9]),
        .O(txd_wr_data[9]));
  FDRE #(
    .INIT(1'b0)) 
    \gfifo_gen.gmm2s.wr_data_int_reg[0] 
       (.C(s_axi_aclk),
        .CE(txd_wr_en),
        .D(txd_wr_data[0]),
        .Q(wr_data_int[0]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \gfifo_gen.gmm2s.wr_data_int_reg[10] 
       (.C(s_axi_aclk),
        .CE(txd_wr_en),
        .D(txd_wr_data[10]),
        .Q(wr_data_int[10]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \gfifo_gen.gmm2s.wr_data_int_reg[11] 
       (.C(s_axi_aclk),
        .CE(txd_wr_en),
        .D(txd_wr_data[11]),
        .Q(wr_data_int[11]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \gfifo_gen.gmm2s.wr_data_int_reg[12] 
       (.C(s_axi_aclk),
        .CE(txd_wr_en),
        .D(txd_wr_data[12]),
        .Q(wr_data_int[12]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \gfifo_gen.gmm2s.wr_data_int_reg[13] 
       (.C(s_axi_aclk),
        .CE(txd_wr_en),
        .D(txd_wr_data[13]),
        .Q(wr_data_int[13]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \gfifo_gen.gmm2s.wr_data_int_reg[14] 
       (.C(s_axi_aclk),
        .CE(txd_wr_en),
        .D(txd_wr_data[14]),
        .Q(wr_data_int[14]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \gfifo_gen.gmm2s.wr_data_int_reg[15] 
       (.C(s_axi_aclk),
        .CE(txd_wr_en),
        .D(txd_wr_data[15]),
        .Q(wr_data_int[15]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \gfifo_gen.gmm2s.wr_data_int_reg[16] 
       (.C(s_axi_aclk),
        .CE(txd_wr_en),
        .D(txd_wr_data[16]),
        .Q(wr_data_int[16]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \gfifo_gen.gmm2s.wr_data_int_reg[17] 
       (.C(s_axi_aclk),
        .CE(txd_wr_en),
        .D(txd_wr_data[17]),
        .Q(wr_data_int[17]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \gfifo_gen.gmm2s.wr_data_int_reg[18] 
       (.C(s_axi_aclk),
        .CE(txd_wr_en),
        .D(txd_wr_data[18]),
        .Q(wr_data_int[18]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \gfifo_gen.gmm2s.wr_data_int_reg[19] 
       (.C(s_axi_aclk),
        .CE(txd_wr_en),
        .D(txd_wr_data[19]),
        .Q(wr_data_int[19]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \gfifo_gen.gmm2s.wr_data_int_reg[1] 
       (.C(s_axi_aclk),
        .CE(txd_wr_en),
        .D(txd_wr_data[1]),
        .Q(wr_data_int[1]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \gfifo_gen.gmm2s.wr_data_int_reg[20] 
       (.C(s_axi_aclk),
        .CE(txd_wr_en),
        .D(txd_wr_data[20]),
        .Q(wr_data_int[20]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \gfifo_gen.gmm2s.wr_data_int_reg[21] 
       (.C(s_axi_aclk),
        .CE(txd_wr_en),
        .D(txd_wr_data[21]),
        .Q(wr_data_int[21]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \gfifo_gen.gmm2s.wr_data_int_reg[22] 
       (.C(s_axi_aclk),
        .CE(txd_wr_en),
        .D(txd_wr_data[22]),
        .Q(wr_data_int[22]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \gfifo_gen.gmm2s.wr_data_int_reg[23] 
       (.C(s_axi_aclk),
        .CE(txd_wr_en),
        .D(txd_wr_data[23]),
        .Q(wr_data_int[23]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \gfifo_gen.gmm2s.wr_data_int_reg[24] 
       (.C(s_axi_aclk),
        .CE(txd_wr_en),
        .D(txd_wr_data[24]),
        .Q(wr_data_int[24]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \gfifo_gen.gmm2s.wr_data_int_reg[25] 
       (.C(s_axi_aclk),
        .CE(txd_wr_en),
        .D(txd_wr_data[25]),
        .Q(wr_data_int[25]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \gfifo_gen.gmm2s.wr_data_int_reg[26] 
       (.C(s_axi_aclk),
        .CE(txd_wr_en),
        .D(txd_wr_data[26]),
        .Q(wr_data_int[26]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \gfifo_gen.gmm2s.wr_data_int_reg[27] 
       (.C(s_axi_aclk),
        .CE(txd_wr_en),
        .D(txd_wr_data[27]),
        .Q(wr_data_int[27]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \gfifo_gen.gmm2s.wr_data_int_reg[28] 
       (.C(s_axi_aclk),
        .CE(txd_wr_en),
        .D(txd_wr_data[28]),
        .Q(wr_data_int[28]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \gfifo_gen.gmm2s.wr_data_int_reg[29] 
       (.C(s_axi_aclk),
        .CE(txd_wr_en),
        .D(txd_wr_data[29]),
        .Q(wr_data_int[29]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \gfifo_gen.gmm2s.wr_data_int_reg[2] 
       (.C(s_axi_aclk),
        .CE(txd_wr_en),
        .D(txd_wr_data[2]),
        .Q(wr_data_int[2]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \gfifo_gen.gmm2s.wr_data_int_reg[30] 
       (.C(s_axi_aclk),
        .CE(txd_wr_en),
        .D(txd_wr_data[30]),
        .Q(wr_data_int[30]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \gfifo_gen.gmm2s.wr_data_int_reg[31] 
       (.C(s_axi_aclk),
        .CE(txd_wr_en),
        .D(txd_wr_data[31]),
        .Q(wr_data_int[31]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \gfifo_gen.gmm2s.wr_data_int_reg[3] 
       (.C(s_axi_aclk),
        .CE(txd_wr_en),
        .D(txd_wr_data[3]),
        .Q(wr_data_int[3]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \gfifo_gen.gmm2s.wr_data_int_reg[4] 
       (.C(s_axi_aclk),
        .CE(txd_wr_en),
        .D(txd_wr_data[4]),
        .Q(wr_data_int[4]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \gfifo_gen.gmm2s.wr_data_int_reg[5] 
       (.C(s_axi_aclk),
        .CE(txd_wr_en),
        .D(txd_wr_data[5]),
        .Q(wr_data_int[5]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \gfifo_gen.gmm2s.wr_data_int_reg[6] 
       (.C(s_axi_aclk),
        .CE(txd_wr_en),
        .D(txd_wr_data[6]),
        .Q(wr_data_int[6]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \gfifo_gen.gmm2s.wr_data_int_reg[7] 
       (.C(s_axi_aclk),
        .CE(txd_wr_en),
        .D(txd_wr_data[7]),
        .Q(wr_data_int[7]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \gfifo_gen.gmm2s.wr_data_int_reg[8] 
       (.C(s_axi_aclk),
        .CE(txd_wr_en),
        .D(txd_wr_data[8]),
        .Q(wr_data_int[8]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \gfifo_gen.gmm2s.wr_data_int_reg[9] 
       (.C(s_axi_aclk),
        .CE(txd_wr_en),
        .D(txd_wr_data[9]),
        .Q(wr_data_int[9]),
        .R(SR));
  LUT4 #(
    .INIT(16'hF888)) 
    \sig_ip2bus_data[22]_i_3 
       (.I0(vacancy_i[9]),
        .I1(\GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg[9] ),
        .I2(\count_reg[9] [7]),
        .I3(\GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg[7] ),
        .O(\sig_ip2bus_data_reg[22] ));
  LUT4 #(
    .INIT(16'hF888)) 
    \sig_ip2bus_data[23]_i_2 
       (.I0(vacancy_i[8]),
        .I1(\GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg[9] ),
        .I2(\count_reg[9] [6]),
        .I3(\GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg[7] ),
        .O(\sig_ip2bus_data_reg[23] ));
  LUT4 #(
    .INIT(16'hF888)) 
    \sig_ip2bus_data[24]_i_2 
       (.I0(vacancy_i[7]),
        .I1(\GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg[9] ),
        .I2(\count_reg[9] [5]),
        .I3(\GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg[7] ),
        .O(\sig_ip2bus_data_reg[24] ));
  LUT4 #(
    .INIT(16'hF888)) 
    \sig_ip2bus_data[25]_i_2 
       (.I0(vacancy_i[6]),
        .I1(\GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg[9] ),
        .I2(\count_reg[9] [4]),
        .I3(\GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg[7] ),
        .O(\sig_ip2bus_data_reg[25] ));
  LUT4 #(
    .INIT(16'hF888)) 
    \sig_ip2bus_data[26]_i_2 
       (.I0(vacancy_i[5]),
        .I1(\GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg[9] ),
        .I2(\count_reg[9] [3]),
        .I3(\GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg[7] ),
        .O(\sig_ip2bus_data_reg[26] ));
  LUT4 #(
    .INIT(16'hF888)) 
    \sig_ip2bus_data[27]_i_2 
       (.I0(vacancy_i[4]),
        .I1(\GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg[9] ),
        .I2(\count_reg[9] [2]),
        .I3(\GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg[7] ),
        .O(\sig_ip2bus_data_reg[27] ));
  LUT4 #(
    .INIT(16'hF888)) 
    \sig_ip2bus_data[28]_i_2 
       (.I0(vacancy_i[3]),
        .I1(\GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg[9] ),
        .I2(\count_reg[9] [1]),
        .I3(\GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg[7] ),
        .O(\sig_ip2bus_data_reg[28] ));
  LUT4 #(
    .INIT(16'hF888)) 
    \sig_ip2bus_data[29]_i_2 
       (.I0(vacancy_i[2]),
        .I1(\GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg[9] ),
        .I2(\count_reg[9] [0]),
        .I3(\GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg[7] ),
        .O(\sig_ip2bus_data_reg[29] ));
endmodule

(* ORIG_REF_NAME = "fifo" *) 
module axi_fifo_mm_s_0_fifo__parameterized0
   (\grxd.fg_rxd_wr_length_reg[1] ,
    empty_fwft_i,
    p_9_out,
    p_8_out,
    \grxd.fg_rxd_wr_length_reg[21] ,
    rx_complete,
    rx_str_wr_en,
    \grxd.sig_rxd_rd_data_reg[32] ,
    \gc0.count_reg[0] ,
    \gc0.count_reg[0]_0 ,
    axi_str_rxd_tready,
    DI,
    Q,
    \sig_register_array_reg[0][2] ,
    \sig_register_array_reg[0][1] ,
    \sig_register_array_reg[0][2]_0 ,
    sig_rxd_pf_event__1,
    sig_rxd_pe_event__1,
    S,
    \count_reg[4] ,
    \count_reg[8] ,
    \grxd.fg_rxd_wr_length_reg[2] ,
    s_axi4_rdata,
    s_axi_aclk,
    axi_str_rxd_tdata,
    axi_str_rxd_tlast,
    axis_rd_en__0,
    axi_str_rxd_tvalid,
    rx_len_wr_en,
    s_axi_aresetn,
    sig_str_rst_reg,
    sig_rxd_rd_data,
    axi4_fifo_rd_en_i,
    sig_rd_rlen_reg,
    \gaxi_full_sm.r_valid_r_reg ,
    \gaxi_full_sm.r_last_r_reg ,
    sig_axi_rd_en,
    \s_axi4_araddr[21] ,
    \s_axi4_araddr[29] ,
    s_axi4_araddr,
    sig_rx_channel_reset_reg,
    sig_rxd_prog_full_d1,
    sig_rxd_prog_empty_d1,
    O,
    \grxd.fg_rxd_wr_length_reg[2]_0 ,
    fg_rxd_wr_length,
    D);
  output \grxd.fg_rxd_wr_length_reg[1] ;
  output empty_fwft_i;
  output p_9_out;
  output p_8_out;
  output \grxd.fg_rxd_wr_length_reg[21] ;
  output rx_complete;
  output rx_str_wr_en;
  output \grxd.sig_rxd_rd_data_reg[32] ;
  output \gc0.count_reg[0] ;
  output \gc0.count_reg[0]_0 ;
  output axi_str_rxd_tready;
  output [3:0]DI;
  output [6:0]Q;
  output \sig_register_array_reg[0][2] ;
  output \sig_register_array_reg[0][1] ;
  output \sig_register_array_reg[0][2]_0 ;
  output sig_rxd_pf_event__1;
  output sig_rxd_pe_event__1;
  output [0:0]S;
  output [3:0]\count_reg[4] ;
  output [3:0]\count_reg[8] ;
  output \grxd.fg_rxd_wr_length_reg[2] ;
  output [31:0]s_axi4_rdata;
  input s_axi_aclk;
  input [31:0]axi_str_rxd_tdata;
  input axi_str_rxd_tlast;
  input axis_rd_en__0;
  input axi_str_rxd_tvalid;
  input rx_len_wr_en;
  input s_axi_aresetn;
  input sig_str_rst_reg;
  input [0:0]sig_rxd_rd_data;
  input axi4_fifo_rd_en_i;
  input sig_rd_rlen_reg;
  input \gaxi_full_sm.r_valid_r_reg ;
  input \gaxi_full_sm.r_last_r_reg ;
  input sig_axi_rd_en;
  input \s_axi4_araddr[21] ;
  input \s_axi4_araddr[29] ;
  input [11:0]s_axi4_araddr;
  input sig_rx_channel_reset_reg;
  input sig_rxd_prog_full_d1;
  input sig_rxd_prog_empty_d1;
  input [0:0]O;
  input [0:0]\grxd.fg_rxd_wr_length_reg[2]_0 ;
  input [0:0]fg_rxd_wr_length;
  input [8:0]D;

  wire [8:0]D;
  wire [3:0]DI;
  wire [0:0]O;
  wire [6:0]Q;
  wire [0:0]S;
  wire axi4_fifo_rd_en_i;
  wire [31:0]axi_str_rxd_tdata;
  wire axi_str_rxd_tlast;
  wire axi_str_rxd_tready;
  wire axi_str_rxd_tvalid;
  wire axis_rd_en__0;
  wire [3:0]\count_reg[4] ;
  wire [3:0]\count_reg[8] ;
  wire empty_fwft_i;
  wire [0:0]fg_rxd_wr_length;
  wire \gaxi_full_sm.r_last_r_reg ;
  wire \gaxi_full_sm.r_valid_r_reg ;
  wire \gc0.count_reg[0] ;
  wire \gc0.count_reg[0]_0 ;
  wire \grxd.fg_rxd_wr_length_reg[1] ;
  wire \grxd.fg_rxd_wr_length_reg[21] ;
  wire \grxd.fg_rxd_wr_length_reg[2] ;
  wire [0:0]\grxd.fg_rxd_wr_length_reg[2]_0 ;
  wire \grxd.sig_rxd_rd_data_reg[32] ;
  wire p_8_out;
  wire p_9_out;
  wire rx_complete;
  wire rx_len_wr_en;
  wire rx_str_wr_en;
  wire [11:0]s_axi4_araddr;
  wire \s_axi4_araddr[21] ;
  wire \s_axi4_araddr[29] ;
  wire [31:0]s_axi4_rdata;
  wire s_axi_aclk;
  wire s_axi_aresetn;
  wire sig_axi_rd_en;
  wire sig_rd_rlen_reg;
  wire \sig_register_array_reg[0][1] ;
  wire \sig_register_array_reg[0][2] ;
  wire \sig_register_array_reg[0][2]_0 ;
  wire sig_rx_channel_reset_reg;
  wire sig_rxd_pe_event__1;
  wire sig_rxd_pf_event__1;
  wire sig_rxd_prog_empty_d1;
  wire sig_rxd_prog_full_d1;
  wire [0:0]sig_rxd_rd_data;
  wire sig_str_rst_reg;

  axi_fifo_mm_s_0_axis_fg__parameterized0 \gfifo_gen.COMP_AXIS_FG_FIFO 
       (.D(D),
        .DI(DI),
        .O(O),
        .Q(Q),
        .S(S),
        .axi4_fifo_rd_en_i(axi4_fifo_rd_en_i),
        .axi_str_rxd_tdata(axi_str_rxd_tdata),
        .axi_str_rxd_tlast(axi_str_rxd_tlast),
        .axi_str_rxd_tready(axi_str_rxd_tready),
        .axi_str_rxd_tvalid(axi_str_rxd_tvalid),
        .axis_rd_en__0(axis_rd_en__0),
        .\count_reg[4] (\count_reg[4] ),
        .\count_reg[8] (\count_reg[8] ),
        .empty_fwft_i(empty_fwft_i),
        .fg_rxd_wr_length(fg_rxd_wr_length),
        .\gaxi_full_sm.r_last_r_reg (\gaxi_full_sm.r_last_r_reg ),
        .\gaxi_full_sm.r_valid_r_reg (\gaxi_full_sm.r_valid_r_reg ),
        .\gc0.count_reg[0] (\gc0.count_reg[0] ),
        .\gc0.count_reg[0]_0 (\gc0.count_reg[0]_0 ),
        .\grxd.fg_rxd_wr_length_reg[1] (\grxd.fg_rxd_wr_length_reg[1] ),
        .\grxd.fg_rxd_wr_length_reg[21] (\grxd.fg_rxd_wr_length_reg[21] ),
        .\grxd.fg_rxd_wr_length_reg[2] (\grxd.fg_rxd_wr_length_reg[2] ),
        .\grxd.fg_rxd_wr_length_reg[2]_0 (\grxd.fg_rxd_wr_length_reg[2]_0 ),
        .\grxd.sig_rxd_rd_data_reg[32] (\grxd.sig_rxd_rd_data_reg[32] ),
        .p_8_out(p_8_out),
        .p_9_out(p_9_out),
        .rx_complete(rx_complete),
        .rx_len_wr_en(rx_len_wr_en),
        .rx_str_wr_en(rx_str_wr_en),
        .s_axi4_araddr(s_axi4_araddr),
        .\s_axi4_araddr[21] (\s_axi4_araddr[21] ),
        .\s_axi4_araddr[29] (\s_axi4_araddr[29] ),
        .s_axi4_rdata(s_axi4_rdata),
        .s_axi_aclk(s_axi_aclk),
        .s_axi_aresetn(s_axi_aresetn),
        .sig_axi_rd_en(sig_axi_rd_en),
        .sig_rd_rlen_reg(sig_rd_rlen_reg),
        .\sig_register_array_reg[0][1] (\sig_register_array_reg[0][1] ),
        .\sig_register_array_reg[0][2] (\sig_register_array_reg[0][2] ),
        .\sig_register_array_reg[0][2]_0 (\sig_register_array_reg[0][2]_0 ),
        .sig_rx_channel_reset_reg(sig_rx_channel_reset_reg),
        .sig_rxd_pe_event__1(sig_rxd_pe_event__1),
        .sig_rxd_pf_event__1(sig_rxd_pf_event__1),
        .sig_rxd_prog_empty_d1(sig_rxd_prog_empty_d1),
        .sig_rxd_prog_full_d1(sig_rxd_prog_full_d1),
        .sig_rxd_rd_data(sig_rxd_rd_data),
        .sig_str_rst_reg(sig_str_rst_reg));
endmodule

(* ORIG_REF_NAME = "ipic2axi_s" *) 
module axi_fifo_mm_s_0_ipic2axi_s
   (\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram ,
    out,
    ram_full_i,
    empty_fwft_i,
    sig_rxd_reset,
    sync_areset_n_reg_inv,
    SR,
    \sig_ip2bus_data_reg[0]_0 ,
    \sig_ip2bus_data_reg[1]_0 ,
    \sig_ip2bus_data_reg[2]_0 ,
    \sig_ip2bus_data_reg[3]_0 ,
    \sig_ip2bus_data_reg[4]_0 ,
    \sig_ip2bus_data_reg[5]_0 ,
    \sig_ip2bus_data_reg[6]_0 ,
    \sig_ip2bus_data_reg[7]_0 ,
    \sig_ip2bus_data_reg[8]_0 ,
    \sig_ip2bus_data_reg[9]_0 ,
    \sig_register_array_reg[0][10]_0 ,
    \sig_register_array_reg[0][11]_0 ,
    \sig_register_array_reg[0][12]_0 ,
    s_axi_arready,
    Q,
    sig_txd_reset,
    IPIC_STATE,
    \gpr1.dout_i_reg[0] ,
    sync_areset_n_reg_inv_0,
    s_axi_awready,
    sig_IP2Bus_Error,
    axi_str_txc_tlast,
    axi_str_txc_tvalid,
    cs_ce_clr,
    \axi_str_txd_tdata[31] ,
    \sig_register_array_reg[0][4]_0 ,
    axi_str_txd_tvalid,
    \gaxi_bid_gen.S_AXI_BID_reg[0] ,
    p_13_in,
    \gc0.count_reg[0] ,
    \gc0.count_reg[0]_0 ,
    axi_str_rxd_tready,
    s2mm_prmry_reset_out_n,
    mm2s_prmry_reset_out_n,
    mm2s_cntrl_reset_out_n,
    IP2Bus_Error1_in,
    \sig_txd_wr_data_reg[31]_0 ,
    \sig_ip2bus_data_reg[29]_0 ,
    \sig_ip2bus_data_reg[30]_0 ,
    \sig_ip2bus_data_reg[28]_0 ,
    \sig_ip2bus_data_reg[27]_0 ,
    \sig_ip2bus_data_reg[26]_0 ,
    \sig_ip2bus_data_reg[25]_0 ,
    \sig_ip2bus_data_reg[24]_0 ,
    \sig_ip2bus_data_reg[23]_0 ,
    \sig_ip2bus_data_reg[22]_0 ,
    eqOp__6,
    \sig_register_array_reg[0][2]_0 ,
    \sig_register_array_reg[0][1]_0 ,
    \sig_register_array_reg[0][2]_1 ,
    tx_fifo_or,
    sig_txd_pf_event__1,
    sig_txd_pe_event__1,
    sig_rxd_pf_event__1,
    sig_rxd_pe_event__1,
    interrupt,
    \sig_ip2bus_data_reg[10]_0 ,
    s_axi4_rdata,
    \sig_ip2bus_data_reg[10]_1 ,
    \s_axi_rdata_i_reg[31] ,
    s_axi_aclk,
    axi_str_rxd_tdata,
    axi_str_rxd_tlast,
    \GEN_BKEND_CE_REGISTERS[11].ce_out_i_reg[11] ,
    IPIC_STATE_reg_0,
    \sig_register_array_reg[0][1]_1 ,
    \sig_register_array_reg[0][2]_2 ,
    \sig_register_array_reg[0][3]_0 ,
    \sig_register_array_reg[0][4]_1 ,
    \sig_register_array_reg[0][5]_0 ,
    \sig_register_array_reg[0][6]_0 ,
    \sig_register_array_reg[0][7]_0 ,
    \sig_register_array_reg[0][8]_0 ,
    \sig_register_array_reg[0][9]_0 ,
    \sig_register_array_reg[0][10]_1 ,
    \sig_register_array_reg[0][11]_1 ,
    \sig_register_array_reg[0][12]_1 ,
    bus2ip_rnw_i_reg,
    E,
    txd_wr_en,
    IPIC_STATE_reg_1,
    sig_Bus2IP_CS,
    sig_rx_channel_reset_reg_0,
    IPIC_STATE_reg_2,
    sig_tx_channel_reset_reg_0,
    bus2ip_rnw_i_reg_0,
    s_axi_aresetn,
    axi_str_txd_tready,
    axis_rd_en__0,
    axi_str_rxd_tvalid,
    axi4_fifo_rd_en_i,
    \gaxi_full_sm.r_valid_r_reg ,
    \gaxi_full_sm.r_last_r_reg ,
    sig_axi_rd_en,
    \s_axi4_araddr[21] ,
    \s_axi4_araddr[29] ,
    s_axi4_araddr,
    s_axi_wdata,
    D,
    \GEN_BKEND_CE_REGISTERS[1].ce_out_i_reg[1] ,
    \GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg[0] ,
    \GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg[9] ,
    \GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg[7] ,
    s_axi4_wdata,
    IP2Bus_Error,
    axi_str_txc_tready,
    IPIC_STATE_reg_3,
    sig_txd_sb_wr_en_reg_0,
    IPIC_STATE_reg_4,
    Bus_RNW_reg_reg,
    \MEM_DECODE_GEN[0].cs_out_i_reg[0] ,
    p_1_out);
  output \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram ;
  output out;
  output ram_full_i;
  output empty_fwft_i;
  output sig_rxd_reset;
  output sync_areset_n_reg_inv;
  output [0:0]SR;
  output \sig_ip2bus_data_reg[0]_0 ;
  output \sig_ip2bus_data_reg[1]_0 ;
  output \sig_ip2bus_data_reg[2]_0 ;
  output \sig_ip2bus_data_reg[3]_0 ;
  output \sig_ip2bus_data_reg[4]_0 ;
  output \sig_ip2bus_data_reg[5]_0 ;
  output \sig_ip2bus_data_reg[6]_0 ;
  output \sig_ip2bus_data_reg[7]_0 ;
  output \sig_ip2bus_data_reg[8]_0 ;
  output \sig_ip2bus_data_reg[9]_0 ;
  output \sig_register_array_reg[0][10]_0 ;
  output \sig_register_array_reg[0][11]_0 ;
  output \sig_register_array_reg[0][12]_0 ;
  output s_axi_arready;
  output [9:0]Q;
  output sig_txd_reset;
  output IPIC_STATE;
  output \gpr1.dout_i_reg[0] ;
  output sync_areset_n_reg_inv_0;
  output s_axi_awready;
  output sig_IP2Bus_Error;
  output axi_str_txc_tlast;
  output axi_str_txc_tvalid;
  output cs_ce_clr;
  output [32:0]\axi_str_txd_tdata[31] ;
  output \sig_register_array_reg[0][4]_0 ;
  output axi_str_txd_tvalid;
  output [0:0]\gaxi_bid_gen.S_AXI_BID_reg[0] ;
  output p_13_in;
  output \gc0.count_reg[0] ;
  output \gc0.count_reg[0]_0 ;
  output axi_str_rxd_tready;
  output s2mm_prmry_reset_out_n;
  output mm2s_prmry_reset_out_n;
  output mm2s_cntrl_reset_out_n;
  output IP2Bus_Error1_in;
  output \sig_txd_wr_data_reg[31]_0 ;
  output \sig_ip2bus_data_reg[29]_0 ;
  output [0:0]\sig_ip2bus_data_reg[30]_0 ;
  output \sig_ip2bus_data_reg[28]_0 ;
  output \sig_ip2bus_data_reg[27]_0 ;
  output \sig_ip2bus_data_reg[26]_0 ;
  output \sig_ip2bus_data_reg[25]_0 ;
  output \sig_ip2bus_data_reg[24]_0 ;
  output \sig_ip2bus_data_reg[23]_0 ;
  output \sig_ip2bus_data_reg[22]_0 ;
  output eqOp__6;
  output \sig_register_array_reg[0][2]_0 ;
  output \sig_register_array_reg[0][1]_0 ;
  output \sig_register_array_reg[0][2]_1 ;
  output tx_fifo_or;
  output sig_txd_pf_event__1;
  output sig_txd_pe_event__1;
  output sig_rxd_pf_event__1;
  output sig_rxd_pe_event__1;
  output interrupt;
  output [2:0]\sig_ip2bus_data_reg[10]_0 ;
  output [31:0]s_axi4_rdata;
  output [21:0]\sig_ip2bus_data_reg[10]_1 ;
  output [31:0]\s_axi_rdata_i_reg[31] ;
  input s_axi_aclk;
  input [31:0]axi_str_rxd_tdata;
  input axi_str_rxd_tlast;
  input \GEN_BKEND_CE_REGISTERS[11].ce_out_i_reg[11] ;
  input IPIC_STATE_reg_0;
  input \sig_register_array_reg[0][1]_1 ;
  input \sig_register_array_reg[0][2]_2 ;
  input \sig_register_array_reg[0][3]_0 ;
  input \sig_register_array_reg[0][4]_1 ;
  input \sig_register_array_reg[0][5]_0 ;
  input \sig_register_array_reg[0][6]_0 ;
  input \sig_register_array_reg[0][7]_0 ;
  input \sig_register_array_reg[0][8]_0 ;
  input \sig_register_array_reg[0][9]_0 ;
  input \sig_register_array_reg[0][10]_1 ;
  input \sig_register_array_reg[0][11]_1 ;
  input \sig_register_array_reg[0][12]_1 ;
  input bus2ip_rnw_i_reg;
  input [0:0]E;
  input txd_wr_en;
  input IPIC_STATE_reg_1;
  input sig_Bus2IP_CS;
  input sig_rx_channel_reset_reg_0;
  input IPIC_STATE_reg_2;
  input sig_tx_channel_reset_reg_0;
  input bus2ip_rnw_i_reg_0;
  input s_axi_aresetn;
  input axi_str_txd_tready;
  input axis_rd_en__0;
  input axi_str_rxd_tvalid;
  input axi4_fifo_rd_en_i;
  input \gaxi_full_sm.r_valid_r_reg ;
  input \gaxi_full_sm.r_last_r_reg ;
  input sig_axi_rd_en;
  input \s_axi4_araddr[21] ;
  input \s_axi4_araddr[29] ;
  input [11:0]s_axi4_araddr;
  input [31:0]s_axi_wdata;
  input [21:0]D;
  input \GEN_BKEND_CE_REGISTERS[1].ce_out_i_reg[1] ;
  input \GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg[0] ;
  input \GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg[9] ;
  input \GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg[7] ;
  input [31:0]s_axi4_wdata;
  input IP2Bus_Error;
  input axi_str_txc_tready;
  input [0:0]IPIC_STATE_reg_3;
  input [3:0]sig_txd_sb_wr_en_reg_0;
  input [0:0]IPIC_STATE_reg_4;
  input [12:0]Bus_RNW_reg_reg;
  input [0:0]\MEM_DECODE_GEN[0].cs_out_i_reg[0] ;
  input p_1_out;

  wire Axi_Str_TxC_AReset;
  wire [12:0]Bus_RNW_reg_reg;
  wire [21:0]D;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram ;
  wire [0:0]E;
  wire [6:6]\FAMILY_SUPPORTED.I_SYNC_FIFO_BRAM/inst_fifo_gen/gconvfifo.rf/grf.rf/p_0_out ;
  wire [6:6]\FAMILY_SUPPORTED.I_SYNC_FIFO_BRAM/inst_fifo_gen/gconvfifo.rf/grf.rf/p_11_out ;
  wire \GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg[0] ;
  wire \GEN_BKEND_CE_REGISTERS[11].ce_out_i_reg[11] ;
  wire \GEN_BKEND_CE_REGISTERS[1].ce_out_i_reg[1] ;
  wire \GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg[7] ;
  wire \GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg[9] ;
  wire IP2Bus_Error;
  wire IP2Bus_Error1_in;
  wire IP2Bus_Error_i_1_n_0;
  wire IPIC_STATE;
  wire IPIC_STATE_reg_0;
  wire IPIC_STATE_reg_1;
  wire IPIC_STATE_reg_2;
  wire [0:0]IPIC_STATE_reg_3;
  wire [0:0]IPIC_STATE_reg_4;
  wire [0:0]\MEM_DECODE_GEN[0].cs_out_i_reg[0] ;
  wire [9:0]Q;
  wire [30:0]R;
  wire [0:0]SR;
  wire \__13/RAM_reg_64_127_0_2_i_1_n_0 ;
  wire \__13/gpr1.dout_i[0]_i_1_n_0 ;
  wire \__13/gpr1.dout_i[10]_i_1_n_0 ;
  wire \__13/gpr1.dout_i[11]_i_1_n_0 ;
  wire \__13/gpr1.dout_i[12]_i_1_n_0 ;
  wire \__13/gpr1.dout_i[13]_i_1_n_0 ;
  wire \__13/gpr1.dout_i[14]_i_1_n_0 ;
  wire \__13/gpr1.dout_i[15]_i_1_n_0 ;
  wire \__13/gpr1.dout_i[16]_i_1_n_0 ;
  wire \__13/gpr1.dout_i[17]_i_1_n_0 ;
  wire \__13/gpr1.dout_i[18]_i_1_n_0 ;
  wire \__13/gpr1.dout_i[19]_i_1_n_0 ;
  wire \__13/gpr1.dout_i[1]_i_1_n_0 ;
  wire \__13/gpr1.dout_i[20]_i_1_n_0 ;
  wire \__13/gpr1.dout_i[21]_i_2_n_0 ;
  wire \__13/gpr1.dout_i[2]_i_1_n_0 ;
  wire \__13/gpr1.dout_i[3]_i_1_n_0 ;
  wire \__13/gpr1.dout_i[4]_i_1_n_0 ;
  wire \__13/gpr1.dout_i[5]_i_1_n_0 ;
  wire \__13/gpr1.dout_i[6]_i_1_n_0 ;
  wire \__13/gpr1.dout_i[7]_i_1_n_0 ;
  wire \__13/gpr1.dout_i[8]_i_1_n_0 ;
  wire \__13/gpr1.dout_i[9]_i_1_n_0 ;
  wire axi4_fifo_rd_en_i;
  wire [31:0]axi_str_rxd_tdata;
  wire axi_str_rxd_tlast;
  wire axi_str_rxd_tready;
  wire axi_str_rxd_tvalid;
  wire axi_str_txc_tlast;
  wire axi_str_txc_tready;
  wire axi_str_txc_tvalid;
  wire [32:0]\axi_str_txd_tdata[31] ;
  wire axi_str_txd_tready;
  wire axi_str_txd_tvalid;
  wire axis_rd_en__0;
  wire bus2ip_rnw_i_reg;
  wire bus2ip_rnw_i_reg_0;
  wire cs_ce_clr;
  wire empty_fwft_i;
  wire eqOp0_out;
  wire eqOp__6;
  wire \eqOp_inferred__1/i__carry__0_n_0 ;
  wire \eqOp_inferred__1/i__carry__0_n_1 ;
  wire \eqOp_inferred__1/i__carry__0_n_2 ;
  wire \eqOp_inferred__1/i__carry__0_n_3 ;
  wire \eqOp_inferred__1/i__carry__1_n_2 ;
  wire \eqOp_inferred__1/i__carry__1_n_3 ;
  wire \eqOp_inferred__1/i__carry__1_n_4 ;
  wire \eqOp_inferred__1/i__carry_n_0 ;
  wire \eqOp_inferred__1/i__carry_n_1 ;
  wire \eqOp_inferred__1/i__carry_n_2 ;
  wire \eqOp_inferred__1/i__carry_n_3 ;
  wire \eqOp_inferred__2/i__carry__0_n_0 ;
  wire \eqOp_inferred__2/i__carry__0_n_1 ;
  wire \eqOp_inferred__2/i__carry__0_n_2 ;
  wire \eqOp_inferred__2/i__carry__0_n_3 ;
  wire \eqOp_inferred__2/i__carry__1_n_1 ;
  wire \eqOp_inferred__2/i__carry__1_n_2 ;
  wire \eqOp_inferred__2/i__carry__1_n_3 ;
  wire \eqOp_inferred__2/i__carry__1_n_4 ;
  wire \eqOp_inferred__2/i__carry_n_0 ;
  wire \eqOp_inferred__2/i__carry_n_1 ;
  wire \eqOp_inferred__2/i__carry_n_2 ;
  wire \eqOp_inferred__2/i__carry_n_3 ;
  wire [21:1]fg_rxd_wr_length;
  wire [0:0]\gaxi_bid_gen.S_AXI_BID_reg[0] ;
  wire \gaxi_full_sm.r_last_r_reg ;
  wire \gaxi_full_sm.r_valid_r_reg ;
  wire \gc0.count_reg[0] ;
  wire \gc0.count_reg[0]_0 ;
  wire [7:0]\gfifo_gen.COMP_AXIS_FG_FIFO/COMP_FIFO/inst_fifo_gen/gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg ;
  wire [7:2]\gfifo_gen.COMP_AXIS_FG_FIFO/COMP_FIFO/inst_fifo_gen/gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg ;
  wire [1:1]\gfifo_gen.COMP_AXIS_FG_FIFO/COMP_FIFO/inst_fifo_gen/gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg__0 ;
  wire \gpr1.dout_i_reg[0] ;
  wire \grxd.COMP_RX_FIFO_n_0 ;
  wire \grxd.COMP_RX_FIFO_n_14 ;
  wire \grxd.COMP_RX_FIFO_n_27 ;
  wire \grxd.COMP_RX_FIFO_n_28 ;
  wire \grxd.COMP_RX_FIFO_n_29 ;
  wire \grxd.COMP_RX_FIFO_n_30 ;
  wire \grxd.COMP_RX_FIFO_n_31 ;
  wire \grxd.COMP_RX_FIFO_n_32 ;
  wire \grxd.COMP_RX_FIFO_n_33 ;
  wire \grxd.COMP_RX_FIFO_n_34 ;
  wire \grxd.COMP_RX_FIFO_n_35 ;
  wire \grxd.COMP_RX_FIFO_n_36 ;
  wire \grxd.COMP_RX_FIFO_n_4 ;
  wire \grxd.COMP_RX_FIFO_n_7 ;
  wire \grxd.COMP_rx_len_fifo_n_1 ;
  wire \grxd.COMP_rx_len_fifo_n_10 ;
  wire \grxd.COMP_rx_len_fifo_n_11 ;
  wire \grxd.COMP_rx_len_fifo_n_12 ;
  wire \grxd.COMP_rx_len_fifo_n_13 ;
  wire \grxd.COMP_rx_len_fifo_n_14 ;
  wire \grxd.COMP_rx_len_fifo_n_15 ;
  wire \grxd.COMP_rx_len_fifo_n_16 ;
  wire \grxd.COMP_rx_len_fifo_n_17 ;
  wire \grxd.COMP_rx_len_fifo_n_18 ;
  wire \grxd.COMP_rx_len_fifo_n_19 ;
  wire \grxd.COMP_rx_len_fifo_n_20 ;
  wire \grxd.COMP_rx_len_fifo_n_21 ;
  wire \grxd.COMP_rx_len_fifo_n_22 ;
  wire \grxd.COMP_rx_len_fifo_n_23 ;
  wire \grxd.COMP_rx_len_fifo_n_24 ;
  wire \grxd.COMP_rx_len_fifo_n_25 ;
  wire \grxd.COMP_rx_len_fifo_n_26 ;
  wire \grxd.COMP_rx_len_fifo_n_27 ;
  wire \grxd.COMP_rx_len_fifo_n_28 ;
  wire \grxd.COMP_rx_len_fifo_n_29 ;
  wire \grxd.COMP_rx_len_fifo_n_3 ;
  wire \grxd.COMP_rx_len_fifo_n_30 ;
  wire \grxd.COMP_rx_len_fifo_n_31 ;
  wire \grxd.COMP_rx_len_fifo_n_32 ;
  wire \grxd.COMP_rx_len_fifo_n_33 ;
  wire \grxd.COMP_rx_len_fifo_n_34 ;
  wire \grxd.COMP_rx_len_fifo_n_35 ;
  wire \grxd.COMP_rx_len_fifo_n_36 ;
  wire \grxd.COMP_rx_len_fifo_n_37 ;
  wire \grxd.COMP_rx_len_fifo_n_38 ;
  wire \grxd.COMP_rx_len_fifo_n_39 ;
  wire \grxd.COMP_rx_len_fifo_n_4 ;
  wire \grxd.COMP_rx_len_fifo_n_40 ;
  wire \grxd.COMP_rx_len_fifo_n_41 ;
  wire \grxd.COMP_rx_len_fifo_n_42 ;
  wire \grxd.COMP_rx_len_fifo_n_43 ;
  wire \grxd.COMP_rx_len_fifo_n_44 ;
  wire \grxd.COMP_rx_len_fifo_n_45 ;
  wire \grxd.COMP_rx_len_fifo_n_46 ;
  wire \grxd.COMP_rx_len_fifo_n_47 ;
  wire \grxd.COMP_rx_len_fifo_n_48 ;
  wire \grxd.COMP_rx_len_fifo_n_5 ;
  wire \grxd.COMP_rx_len_fifo_n_8 ;
  wire \grxd.COMP_rx_len_fifo_n_9 ;
  wire \grxd.fg_rxd_wr_length[10]_i_1_n_0 ;
  wire \grxd.fg_rxd_wr_length[11]_i_1_n_0 ;
  wire \grxd.fg_rxd_wr_length[12]_i_1_n_0 ;
  wire \grxd.fg_rxd_wr_length[13]_i_1_n_0 ;
  wire \grxd.fg_rxd_wr_length[14]_i_1_n_0 ;
  wire \grxd.fg_rxd_wr_length[15]_i_1_n_0 ;
  wire \grxd.fg_rxd_wr_length[16]_i_1_n_0 ;
  wire \grxd.fg_rxd_wr_length[17]_i_1_n_0 ;
  wire \grxd.fg_rxd_wr_length[18]_i_1_n_0 ;
  wire \grxd.fg_rxd_wr_length[19]_i_1_n_0 ;
  wire \grxd.fg_rxd_wr_length[1]_i_1_n_0 ;
  wire \grxd.fg_rxd_wr_length[20]_i_1_n_0 ;
  wire \grxd.fg_rxd_wr_length[21]_i_3_n_0 ;
  wire \grxd.fg_rxd_wr_length[3]_i_1_n_0 ;
  wire \grxd.fg_rxd_wr_length[4]_i_1_n_0 ;
  wire \grxd.fg_rxd_wr_length[5]_i_1_n_0 ;
  wire \grxd.fg_rxd_wr_length[6]_i_1_n_0 ;
  wire \grxd.fg_rxd_wr_length[7]_i_1_n_0 ;
  wire \grxd.fg_rxd_wr_length[8]_i_1_n_0 ;
  wire \grxd.fg_rxd_wr_length[9]_i_1_n_0 ;
  wire \gtxc.TXC_STATE_reg_n_0_[0] ;
  wire \gtxc.TXC_STATE_reg_n_0_[1] ;
  wire \gtxc.txc_cntr[0]_i_1_n_0 ;
  wire \gtxc.txc_cntr[1]_i_1_n_0 ;
  wire \gtxc.txc_cntr[2]_i_1_n_0 ;
  wire \gtxc.txc_str_Lastsig_i_1_n_0 ;
  wire \gtxc.txc_str_Lastsig_i_2_n_0 ;
  wire \gtxd.COMP_TXD_FIFO_n_40 ;
  wire \gtxd.COMP_TXD_FIFO_n_49 ;
  wire \gtxd.COMP_TXD_FIFO_n_57 ;
  wire \gtxd.COMP_TXD_FIFO_n_66 ;
  wire \gtxd.COMP_TXD_FIFO_n_70 ;
  wire \gtxd.COMP_TXD_FIFO_n_71 ;
  wire \gtxd.COMP_TXD_FIFO_n_72 ;
  wire \gtxd.COMP_TXD_FIFO_n_73 ;
  wire \gtxd.COMP_TXD_FIFO_n_74 ;
  wire \gtxd.COMP_TXD_FIFO_n_75 ;
  wire \gtxd.COMP_TXD_FIFO_n_76 ;
  wire \gtxd.COMP_TXD_FIFO_n_77 ;
  wire \gtxd.COMP_TXD_FIFO_n_78 ;
  wire \gtxd.COMP_TXD_FIFO_n_79 ;
  wire \gtxd.COMP_TXD_FIFO_n_80 ;
  wire \gtxd.COMP_TXD_FIFO_n_81 ;
  wire \gtxd.COMP_TXD_FIFO_n_82 ;
  wire \gtxd.COMP_TXD_FIFO_n_83 ;
  wire \gtxd.COMP_TXD_FIFO_n_84 ;
  wire \gtxd.COMP_TXD_FIFO_n_85 ;
  wire \gtxd.COMP_TXD_FIFO_n_86 ;
  wire \gtxd.COMP_TXD_FIFO_n_87 ;
  wire \gtxd.COMP_TXD_FIFO_n_88 ;
  wire \gtxd.COMP_TXD_FIFO_n_89 ;
  wire \gtxd.sig_txd_packet_size[0]_i_1_n_0 ;
  wire \gtxd.sig_txd_packet_size[0]_i_4_n_0 ;
  wire \gtxd.sig_txd_packet_size[0]_i_5_n_0 ;
  wire \gtxd.sig_txd_packet_size[0]_i_6_n_0 ;
  wire \gtxd.sig_txd_packet_size[12]_i_2_n_0 ;
  wire \gtxd.sig_txd_packet_size[12]_i_3_n_0 ;
  wire \gtxd.sig_txd_packet_size[12]_i_4_n_0 ;
  wire \gtxd.sig_txd_packet_size[12]_i_5_n_0 ;
  wire \gtxd.sig_txd_packet_size[16]_i_2_n_0 ;
  wire \gtxd.sig_txd_packet_size[16]_i_3_n_0 ;
  wire \gtxd.sig_txd_packet_size[16]_i_4_n_0 ;
  wire \gtxd.sig_txd_packet_size[16]_i_5_n_0 ;
  wire \gtxd.sig_txd_packet_size[20]_i_2_n_0 ;
  wire \gtxd.sig_txd_packet_size[20]_i_3_n_0 ;
  wire \gtxd.sig_txd_packet_size[20]_i_4_n_0 ;
  wire \gtxd.sig_txd_packet_size[20]_i_5_n_0 ;
  wire \gtxd.sig_txd_packet_size[24]_i_2_n_0 ;
  wire \gtxd.sig_txd_packet_size[24]_i_3_n_0 ;
  wire \gtxd.sig_txd_packet_size[24]_i_4_n_0 ;
  wire \gtxd.sig_txd_packet_size[24]_i_5_n_0 ;
  wire \gtxd.sig_txd_packet_size[28]_i_2_n_0 ;
  wire \gtxd.sig_txd_packet_size[28]_i_3_n_0 ;
  wire \gtxd.sig_txd_packet_size[28]_i_4_n_0 ;
  wire \gtxd.sig_txd_packet_size[4]_i_2_n_0 ;
  wire \gtxd.sig_txd_packet_size[4]_i_3_n_0 ;
  wire \gtxd.sig_txd_packet_size[4]_i_4_n_0 ;
  wire \gtxd.sig_txd_packet_size[4]_i_5_n_0 ;
  wire \gtxd.sig_txd_packet_size[8]_i_2_n_0 ;
  wire \gtxd.sig_txd_packet_size[8]_i_3_n_0 ;
  wire \gtxd.sig_txd_packet_size[8]_i_4_n_0 ;
  wire \gtxd.sig_txd_packet_size[8]_i_5_n_0 ;
  wire [30:0]\gtxd.sig_txd_packet_size_reg ;
  wire \gtxd.sig_txd_packet_size_reg[0]_i_3_n_0 ;
  wire \gtxd.sig_txd_packet_size_reg[0]_i_3_n_1 ;
  wire \gtxd.sig_txd_packet_size_reg[0]_i_3_n_2 ;
  wire \gtxd.sig_txd_packet_size_reg[0]_i_3_n_3 ;
  wire \gtxd.sig_txd_packet_size_reg[0]_i_3_n_4 ;
  wire \gtxd.sig_txd_packet_size_reg[0]_i_3_n_5 ;
  wire \gtxd.sig_txd_packet_size_reg[0]_i_3_n_6 ;
  wire \gtxd.sig_txd_packet_size_reg[0]_i_3_n_7 ;
  wire \gtxd.sig_txd_packet_size_reg[12]_i_1_n_0 ;
  wire \gtxd.sig_txd_packet_size_reg[12]_i_1_n_1 ;
  wire \gtxd.sig_txd_packet_size_reg[12]_i_1_n_2 ;
  wire \gtxd.sig_txd_packet_size_reg[12]_i_1_n_3 ;
  wire \gtxd.sig_txd_packet_size_reg[12]_i_1_n_4 ;
  wire \gtxd.sig_txd_packet_size_reg[12]_i_1_n_5 ;
  wire \gtxd.sig_txd_packet_size_reg[12]_i_1_n_6 ;
  wire \gtxd.sig_txd_packet_size_reg[12]_i_1_n_7 ;
  wire \gtxd.sig_txd_packet_size_reg[16]_i_1_n_0 ;
  wire \gtxd.sig_txd_packet_size_reg[16]_i_1_n_1 ;
  wire \gtxd.sig_txd_packet_size_reg[16]_i_1_n_2 ;
  wire \gtxd.sig_txd_packet_size_reg[16]_i_1_n_3 ;
  wire \gtxd.sig_txd_packet_size_reg[16]_i_1_n_4 ;
  wire \gtxd.sig_txd_packet_size_reg[16]_i_1_n_5 ;
  wire \gtxd.sig_txd_packet_size_reg[16]_i_1_n_6 ;
  wire \gtxd.sig_txd_packet_size_reg[16]_i_1_n_7 ;
  wire \gtxd.sig_txd_packet_size_reg[20]_i_1_n_0 ;
  wire \gtxd.sig_txd_packet_size_reg[20]_i_1_n_1 ;
  wire \gtxd.sig_txd_packet_size_reg[20]_i_1_n_2 ;
  wire \gtxd.sig_txd_packet_size_reg[20]_i_1_n_3 ;
  wire \gtxd.sig_txd_packet_size_reg[20]_i_1_n_4 ;
  wire \gtxd.sig_txd_packet_size_reg[20]_i_1_n_5 ;
  wire \gtxd.sig_txd_packet_size_reg[20]_i_1_n_6 ;
  wire \gtxd.sig_txd_packet_size_reg[20]_i_1_n_7 ;
  wire \gtxd.sig_txd_packet_size_reg[24]_i_1_n_0 ;
  wire \gtxd.sig_txd_packet_size_reg[24]_i_1_n_1 ;
  wire \gtxd.sig_txd_packet_size_reg[24]_i_1_n_2 ;
  wire \gtxd.sig_txd_packet_size_reg[24]_i_1_n_3 ;
  wire \gtxd.sig_txd_packet_size_reg[24]_i_1_n_4 ;
  wire \gtxd.sig_txd_packet_size_reg[24]_i_1_n_5 ;
  wire \gtxd.sig_txd_packet_size_reg[24]_i_1_n_6 ;
  wire \gtxd.sig_txd_packet_size_reg[24]_i_1_n_7 ;
  wire \gtxd.sig_txd_packet_size_reg[28]_i_1_n_2 ;
  wire \gtxd.sig_txd_packet_size_reg[28]_i_1_n_3 ;
  wire \gtxd.sig_txd_packet_size_reg[28]_i_1_n_5 ;
  wire \gtxd.sig_txd_packet_size_reg[28]_i_1_n_6 ;
  wire \gtxd.sig_txd_packet_size_reg[28]_i_1_n_7 ;
  wire \gtxd.sig_txd_packet_size_reg[4]_i_1_n_0 ;
  wire \gtxd.sig_txd_packet_size_reg[4]_i_1_n_1 ;
  wire \gtxd.sig_txd_packet_size_reg[4]_i_1_n_2 ;
  wire \gtxd.sig_txd_packet_size_reg[4]_i_1_n_3 ;
  wire \gtxd.sig_txd_packet_size_reg[4]_i_1_n_4 ;
  wire \gtxd.sig_txd_packet_size_reg[4]_i_1_n_5 ;
  wire \gtxd.sig_txd_packet_size_reg[4]_i_1_n_6 ;
  wire \gtxd.sig_txd_packet_size_reg[4]_i_1_n_7 ;
  wire \gtxd.sig_txd_packet_size_reg[8]_i_1_n_0 ;
  wire \gtxd.sig_txd_packet_size_reg[8]_i_1_n_1 ;
  wire \gtxd.sig_txd_packet_size_reg[8]_i_1_n_2 ;
  wire \gtxd.sig_txd_packet_size_reg[8]_i_1_n_3 ;
  wire \gtxd.sig_txd_packet_size_reg[8]_i_1_n_4 ;
  wire \gtxd.sig_txd_packet_size_reg[8]_i_1_n_5 ;
  wire \gtxd.sig_txd_packet_size_reg[8]_i_1_n_6 ;
  wire \gtxd.sig_txd_packet_size_reg[8]_i_1_n_7 ;
  wire i__carry__0_i_10_n_0;
  wire i__carry__0_i_11_n_0;
  wire i__carry__0_i_12_n_0;
  wire i__carry__0_i_13_n_0;
  wire i__carry__0_i_14_n_0;
  wire i__carry__0_i_15_n_0;
  wire i__carry__0_i_16_n_0;
  wire i__carry__0_i_17_n_0;
  wire i__carry__0_i_18_n_0;
  wire i__carry__0_i_19_n_0;
  wire i__carry__0_i_1__0_n_0;
  wire i__carry__0_i_1__1_n_0;
  wire i__carry__0_i_1_n_0;
  wire i__carry__0_i_2__0_n_0;
  wire i__carry__0_i_2__1_n_0;
  wire i__carry__0_i_2_n_0;
  wire i__carry__0_i_3__0_n_0;
  wire i__carry__0_i_3__1_n_0;
  wire i__carry__0_i_3_n_0;
  wire i__carry__0_i_4__0_n_0;
  wire i__carry__0_i_4__1_n_0;
  wire i__carry__0_i_4_n_0;
  wire i__carry__0_i_5_n_0;
  wire i__carry__0_i_5_n_1;
  wire i__carry__0_i_5_n_2;
  wire i__carry__0_i_5_n_3;
  wire i__carry__0_i_6_n_0;
  wire i__carry__0_i_6_n_1;
  wire i__carry__0_i_6_n_2;
  wire i__carry__0_i_6_n_3;
  wire i__carry__0_i_7_n_0;
  wire i__carry__0_i_7_n_1;
  wire i__carry__0_i_7_n_2;
  wire i__carry__0_i_7_n_3;
  wire i__carry__0_i_8_n_0;
  wire i__carry__0_i_9_n_0;
  wire i__carry__1_i_10_n_0;
  wire i__carry__1_i_11_n_0;
  wire i__carry__1_i_1__0_n_0;
  wire i__carry__1_i_1__1_n_0;
  wire i__carry__1_i_1_n_0;
  wire i__carry__1_i_2__0_n_0;
  wire i__carry__1_i_2__1_n_0;
  wire i__carry__1_i_2_n_0;
  wire i__carry__1_i_3__0_n_0;
  wire i__carry__1_i_3__1_n_0;
  wire i__carry__1_i_3_n_0;
  wire i__carry__1_i_4__0_n_0;
  wire i__carry__1_i_4_n_3;
  wire i__carry__1_i_5_n_0;
  wire i__carry__1_i_5_n_1;
  wire i__carry__1_i_5_n_2;
  wire i__carry__1_i_5_n_3;
  wire i__carry__1_i_6_n_0;
  wire i__carry__1_i_7_n_0;
  wire i__carry__1_i_8_n_0;
  wire i__carry__1_i_9_n_0;
  wire i__carry__2_i_1_n_0;
  wire i__carry__2_i_2_n_0;
  wire i__carry__2_i_3_n_0;
  wire i__carry__2_i_4_n_0;
  wire i__carry__3_i_1_n_0;
  wire i__carry__3_i_2_n_0;
  wire i__carry__3_i_3_n_0;
  wire i__carry__3_i_4_n_0;
  wire i__carry_i_10_n_0;
  wire i__carry_i_11_n_0;
  wire i__carry_i_12_n_0;
  wire i__carry_i_13_n_0;
  wire i__carry_i_14_n_0;
  wire i__carry_i_15_n_0;
  wire i__carry_i_16_n_0;
  wire i__carry_i_17_n_0;
  wire i__carry_i_18_n_0;
  wire i__carry_i_19_n_0;
  wire i__carry_i_1__0_n_0;
  wire i__carry_i_1__1_n_0;
  wire i__carry_i_1_n_0;
  wire i__carry_i_2__0_n_0;
  wire i__carry_i_2__1_n_0;
  wire i__carry_i_2_n_0;
  wire i__carry_i_3__0_n_0;
  wire i__carry_i_3__1_n_0;
  wire i__carry_i_3_n_0;
  wire i__carry_i_4__0_n_0;
  wire i__carry_i_4__1_n_0;
  wire i__carry_i_4_n_0;
  wire i__carry_i_5_n_0;
  wire i__carry_i_5_n_1;
  wire i__carry_i_5_n_2;
  wire i__carry_i_5_n_3;
  wire i__carry_i_6_n_0;
  wire i__carry_i_6_n_1;
  wire i__carry_i_6_n_2;
  wire i__carry_i_6_n_3;
  wire i__carry_i_7_n_0;
  wire i__carry_i_7_n_1;
  wire i__carry_i_7_n_2;
  wire i__carry_i_7_n_3;
  wire i__carry_i_8_n_0;
  wire i__carry_i_9_n_0;
  wire interrupt;
  wire interrupt_INST_0_i_1_n_0;
  wire interrupt_INST_0_i_2_n_0;
  wire interrupt_INST_0_i_3_n_0;
  wire interrupt_INST_0_i_4_n_0;
  wire interrupt_INST_0_i_5_n_0;
  wire mm2s_cntrl_reset_out_n;
  wire mm2s_prmry_reset_out_n;
  wire out;
  wire p_0_out__24_carry__0_n_0;
  wire p_0_out__24_carry__0_n_1;
  wire p_0_out__24_carry__0_n_2;
  wire p_0_out__24_carry__0_n_3;
  wire p_0_out__24_carry__0_n_4;
  wire p_0_out__24_carry__0_n_5;
  wire p_0_out__24_carry__0_n_6;
  wire p_0_out__24_carry__0_n_7;
  wire p_0_out__24_carry__1_n_7;
  wire p_0_out__24_carry_n_0;
  wire p_0_out__24_carry_n_1;
  wire p_0_out__24_carry_n_2;
  wire p_0_out__24_carry_n_3;
  wire p_0_out__24_carry_n_4;
  wire p_0_out__24_carry_n_5;
  wire p_0_out__24_carry_n_6;
  wire p_0_out__24_carry_n_7;
  wire p_0_out__49_carry__0_n_0;
  wire p_0_out__49_carry__0_n_1;
  wire p_0_out__49_carry__0_n_2;
  wire p_0_out__49_carry__0_n_3;
  wire p_0_out__49_carry__0_n_4;
  wire p_0_out__49_carry__0_n_5;
  wire p_0_out__49_carry__0_n_6;
  wire p_0_out__49_carry__0_n_7;
  wire p_0_out__49_carry__1_n_7;
  wire p_0_out__49_carry_n_0;
  wire p_0_out__49_carry_n_1;
  wire p_0_out__49_carry_n_2;
  wire p_0_out__49_carry_n_3;
  wire p_0_out__49_carry_n_4;
  wire p_0_out__49_carry_n_5;
  wire p_0_out__49_carry_n_6;
  wire p_0_out__49_carry_n_7;
  wire p_0_out_carry__0_n_0;
  wire p_0_out_carry__0_n_1;
  wire p_0_out_carry__0_n_2;
  wire p_0_out_carry__0_n_3;
  wire p_0_out_carry__0_n_4;
  wire p_0_out_carry__0_n_5;
  wire p_0_out_carry__0_n_6;
  wire p_0_out_carry__0_n_7;
  wire p_0_out_carry__1_n_7;
  wire p_0_out_carry_n_0;
  wire p_0_out_carry_n_1;
  wire p_0_out_carry_n_2;
  wire p_0_out_carry_n_3;
  wire p_0_out_carry_n_4;
  wire p_0_out_carry_n_5;
  wire p_0_out_carry_n_6;
  wire p_0_out_carry_n_7;
  wire p_13_in;
  wire p_1_out;
  wire \p_2_out_inferred__2/i__carry__0_n_0 ;
  wire \p_2_out_inferred__2/i__carry__0_n_1 ;
  wire \p_2_out_inferred__2/i__carry__0_n_2 ;
  wire \p_2_out_inferred__2/i__carry__0_n_3 ;
  wire \p_2_out_inferred__2/i__carry__0_n_4 ;
  wire \p_2_out_inferred__2/i__carry__0_n_5 ;
  wire \p_2_out_inferred__2/i__carry__0_n_6 ;
  wire \p_2_out_inferred__2/i__carry__0_n_7 ;
  wire \p_2_out_inferred__2/i__carry__1_n_0 ;
  wire \p_2_out_inferred__2/i__carry__1_n_1 ;
  wire \p_2_out_inferred__2/i__carry__1_n_2 ;
  wire \p_2_out_inferred__2/i__carry__1_n_3 ;
  wire \p_2_out_inferred__2/i__carry__1_n_4 ;
  wire \p_2_out_inferred__2/i__carry__1_n_5 ;
  wire \p_2_out_inferred__2/i__carry__1_n_6 ;
  wire \p_2_out_inferred__2/i__carry__1_n_7 ;
  wire \p_2_out_inferred__2/i__carry__2_n_0 ;
  wire \p_2_out_inferred__2/i__carry__2_n_1 ;
  wire \p_2_out_inferred__2/i__carry__2_n_2 ;
  wire \p_2_out_inferred__2/i__carry__2_n_3 ;
  wire \p_2_out_inferred__2/i__carry__2_n_4 ;
  wire \p_2_out_inferred__2/i__carry__2_n_5 ;
  wire \p_2_out_inferred__2/i__carry__2_n_6 ;
  wire \p_2_out_inferred__2/i__carry__2_n_7 ;
  wire \p_2_out_inferred__2/i__carry__3_n_1 ;
  wire \p_2_out_inferred__2/i__carry__3_n_2 ;
  wire \p_2_out_inferred__2/i__carry__3_n_3 ;
  wire \p_2_out_inferred__2/i__carry__3_n_4 ;
  wire \p_2_out_inferred__2/i__carry__3_n_5 ;
  wire \p_2_out_inferred__2/i__carry__3_n_6 ;
  wire \p_2_out_inferred__2/i__carry__3_n_7 ;
  wire \p_2_out_inferred__2/i__carry_n_0 ;
  wire \p_2_out_inferred__2/i__carry_n_1 ;
  wire \p_2_out_inferred__2/i__carry_n_2 ;
  wire \p_2_out_inferred__2/i__carry_n_3 ;
  wire \p_2_out_inferred__2/i__carry_n_4 ;
  wire \p_2_out_inferred__2/i__carry_n_5 ;
  wire \p_2_out_inferred__2/i__carry_n_6 ;
  wire \p_2_out_inferred__2/i__carry_n_7 ;
  wire p_7_out;
  wire p_8_out;
  wire p_9_out;
  wire plusOp_carry__0_i_1__3_n_0;
  wire plusOp_carry__0_i_2__3_n_0;
  wire plusOp_carry__0_i_3__3_n_0;
  wire plusOp_carry__0_i_4__3_n_0;
  wire plusOp_carry__0_n_0;
  wire plusOp_carry__0_n_1;
  wire plusOp_carry__0_n_2;
  wire plusOp_carry__0_n_3;
  wire plusOp_carry__0_n_4;
  wire plusOp_carry__0_n_5;
  wire plusOp_carry__0_n_6;
  wire plusOp_carry__0_n_7;
  wire plusOp_carry__1_i_1__3_n_0;
  wire plusOp_carry__1_i_2_n_0;
  wire plusOp_carry__1_i_3_n_0;
  wire plusOp_carry__1_i_4_n_0;
  wire plusOp_carry__1_n_0;
  wire plusOp_carry__1_n_1;
  wire plusOp_carry__1_n_2;
  wire plusOp_carry__1_n_3;
  wire plusOp_carry__1_n_4;
  wire plusOp_carry__1_n_5;
  wire plusOp_carry__1_n_6;
  wire plusOp_carry__1_n_7;
  wire plusOp_carry__2_i_1_n_0;
  wire plusOp_carry__2_i_2_n_0;
  wire plusOp_carry__2_i_3_n_0;
  wire plusOp_carry__2_i_4_n_0;
  wire plusOp_carry__2_n_0;
  wire plusOp_carry__2_n_1;
  wire plusOp_carry__2_n_2;
  wire plusOp_carry__2_n_3;
  wire plusOp_carry__2_n_4;
  wire plusOp_carry__2_n_5;
  wire plusOp_carry__2_n_6;
  wire plusOp_carry__2_n_7;
  wire plusOp_carry__3_i_1_n_0;
  wire plusOp_carry__3_i_2_n_0;
  wire plusOp_carry__3_i_3_n_0;
  wire plusOp_carry__3_i_4_n_0;
  wire plusOp_carry__3_n_0;
  wire plusOp_carry__3_n_1;
  wire plusOp_carry__3_n_2;
  wire plusOp_carry__3_n_3;
  wire plusOp_carry__3_n_4;
  wire plusOp_carry__3_n_5;
  wire plusOp_carry__3_n_6;
  wire plusOp_carry__3_n_7;
  wire plusOp_carry__4_i_1_n_0;
  wire plusOp_carry__4_n_7;
  wire plusOp_carry_i_1__3_n_0;
  wire plusOp_carry_i_2__3_n_0;
  wire plusOp_carry_i_3_n_0;
  wire plusOp_carry_i_4__3_n_0;
  wire plusOp_carry_n_0;
  wire plusOp_carry_n_1;
  wire plusOp_carry_n_2;
  wire plusOp_carry_n_3;
  wire plusOp_carry_n_4;
  wire plusOp_carry_n_5;
  wire plusOp_carry_n_6;
  wire plusOp_carry_n_7;
  wire ram_full_i;
  wire rx_complete;
  wire rx_fg_len_empty_d1;
  wire rx_len_wr_en;
  wire rx_str_wr_en;
  wire s2mm_prmry_reset_out_n;
  wire [11:0]s_axi4_araddr;
  wire \s_axi4_araddr[21] ;
  wire \s_axi4_araddr[29] ;
  wire [31:0]s_axi4_rdata;
  wire [31:0]s_axi4_wdata;
  wire s_axi_aclk;
  wire s_axi_aresetn;
  wire s_axi_arready;
  wire s_axi_awready;
  wire [31:0]\s_axi_rdata_i_reg[31] ;
  wire [31:0]s_axi_wdata;
  wire sig_Bus2IP_CS;
  wire sig_IP2Bus_Error;
  wire sig_axi_rd_en;
  wire [0:9]sig_ip2bus_data;
  wire \sig_ip2bus_data_reg[0]_0 ;
  wire [2:0]\sig_ip2bus_data_reg[10]_0 ;
  wire [21:0]\sig_ip2bus_data_reg[10]_1 ;
  wire \sig_ip2bus_data_reg[1]_0 ;
  wire \sig_ip2bus_data_reg[22]_0 ;
  wire \sig_ip2bus_data_reg[23]_0 ;
  wire \sig_ip2bus_data_reg[24]_0 ;
  wire \sig_ip2bus_data_reg[25]_0 ;
  wire \sig_ip2bus_data_reg[26]_0 ;
  wire \sig_ip2bus_data_reg[27]_0 ;
  wire \sig_ip2bus_data_reg[28]_0 ;
  wire \sig_ip2bus_data_reg[29]_0 ;
  wire \sig_ip2bus_data_reg[2]_0 ;
  wire [0:0]\sig_ip2bus_data_reg[30]_0 ;
  wire \sig_ip2bus_data_reg[3]_0 ;
  wire \sig_ip2bus_data_reg[4]_0 ;
  wire \sig_ip2bus_data_reg[5]_0 ;
  wire \sig_ip2bus_data_reg[6]_0 ;
  wire \sig_ip2bus_data_reg[7]_0 ;
  wire \sig_ip2bus_data_reg[8]_0 ;
  wire \sig_ip2bus_data_reg[9]_0 ;
  wire sig_rd_rlen_reg_n_0;
  wire \sig_register_array_reg[0][10]_0 ;
  wire \sig_register_array_reg[0][10]_1 ;
  wire \sig_register_array_reg[0][11]_0 ;
  wire \sig_register_array_reg[0][11]_1 ;
  wire \sig_register_array_reg[0][12]_0 ;
  wire \sig_register_array_reg[0][12]_1 ;
  wire \sig_register_array_reg[0][1]_0 ;
  wire \sig_register_array_reg[0][1]_1 ;
  wire \sig_register_array_reg[0][2]_0 ;
  wire \sig_register_array_reg[0][2]_1 ;
  wire \sig_register_array_reg[0][2]_2 ;
  wire \sig_register_array_reg[0][3]_0 ;
  wire \sig_register_array_reg[0][4]_0 ;
  wire \sig_register_array_reg[0][4]_1 ;
  wire \sig_register_array_reg[0][5]_0 ;
  wire \sig_register_array_reg[0][6]_0 ;
  wire \sig_register_array_reg[0][7]_0 ;
  wire \sig_register_array_reg[0][8]_0 ;
  wire \sig_register_array_reg[0][9]_0 ;
  wire \sig_register_array_reg_n_0_[1][0] ;
  wire \sig_register_array_reg_n_0_[1][1] ;
  wire \sig_register_array_reg_n_0_[1][2] ;
  wire \sig_register_array_reg_n_0_[1][3] ;
  wire \sig_register_array_reg_n_0_[1][4] ;
  wire \sig_register_array_reg_n_0_[1][5] ;
  wire \sig_register_array_reg_n_0_[1][6] ;
  wire \sig_register_array_reg_n_0_[1][7] ;
  wire \sig_register_array_reg_n_0_[1][8] ;
  wire \sig_register_array_reg_n_0_[1][9] ;
  wire sig_rx_channel_reset_reg_0;
  wire sig_rxd_pe_event__1;
  wire sig_rxd_pf_event__1;
  wire sig_rxd_prog_empty_d1;
  wire sig_rxd_prog_full_d1;
  wire [32:32]sig_rxd_rd_data;
  wire sig_rxd_reset;
  wire sig_str_rst_i_8_n_0;
  wire sig_tx_channel_reset_reg_0;
  wire sig_txd_pe_event__1;
  wire sig_txd_pf_event__1;
  wire sig_txd_prog_empty;
  wire sig_txd_prog_empty_d1;
  wire sig_txd_prog_full_d1;
  wire sig_txd_reset;
  wire [3:0]sig_txd_sb_wr_en_reg_0;
  wire \sig_txd_wr_data_reg[31]_0 ;
  wire \sig_txd_wr_data_reg_n_0_[0] ;
  wire \sig_txd_wr_data_reg_n_0_[10] ;
  wire \sig_txd_wr_data_reg_n_0_[11] ;
  wire \sig_txd_wr_data_reg_n_0_[12] ;
  wire \sig_txd_wr_data_reg_n_0_[13] ;
  wire \sig_txd_wr_data_reg_n_0_[14] ;
  wire \sig_txd_wr_data_reg_n_0_[15] ;
  wire \sig_txd_wr_data_reg_n_0_[16] ;
  wire \sig_txd_wr_data_reg_n_0_[17] ;
  wire \sig_txd_wr_data_reg_n_0_[18] ;
  wire \sig_txd_wr_data_reg_n_0_[19] ;
  wire \sig_txd_wr_data_reg_n_0_[1] ;
  wire \sig_txd_wr_data_reg_n_0_[20] ;
  wire \sig_txd_wr_data_reg_n_0_[21] ;
  wire \sig_txd_wr_data_reg_n_0_[22] ;
  wire \sig_txd_wr_data_reg_n_0_[23] ;
  wire \sig_txd_wr_data_reg_n_0_[24] ;
  wire \sig_txd_wr_data_reg_n_0_[25] ;
  wire \sig_txd_wr_data_reg_n_0_[26] ;
  wire \sig_txd_wr_data_reg_n_0_[27] ;
  wire \sig_txd_wr_data_reg_n_0_[28] ;
  wire \sig_txd_wr_data_reg_n_0_[29] ;
  wire \sig_txd_wr_data_reg_n_0_[2] ;
  wire \sig_txd_wr_data_reg_n_0_[30] ;
  wire \sig_txd_wr_data_reg_n_0_[31] ;
  wire \sig_txd_wr_data_reg_n_0_[3] ;
  wire \sig_txd_wr_data_reg_n_0_[4] ;
  wire \sig_txd_wr_data_reg_n_0_[5] ;
  wire \sig_txd_wr_data_reg_n_0_[6] ;
  wire \sig_txd_wr_data_reg_n_0_[7] ;
  wire \sig_txd_wr_data_reg_n_0_[8] ;
  wire \sig_txd_wr_data_reg_n_0_[9] ;
  wire sync_areset_n_reg_inv;
  wire sync_areset_n_reg_inv_0;
  wire tx_fifo_or;
  wire [2:0]txc_cntr;
  wire txd_wr_en;
  wire [3:0]\NLW_eqOp_inferred__1/i__carry_O_UNCONNECTED ;
  wire [3:0]\NLW_eqOp_inferred__1/i__carry__0_O_UNCONNECTED ;
  wire [3:3]\NLW_eqOp_inferred__1/i__carry__1_CO_UNCONNECTED ;
  wire [2:0]\NLW_eqOp_inferred__1/i__carry__1_O_UNCONNECTED ;
  wire [3:0]\NLW_eqOp_inferred__2/i__carry_O_UNCONNECTED ;
  wire [3:0]\NLW_eqOp_inferred__2/i__carry__0_O_UNCONNECTED ;
  wire [3:3]\NLW_eqOp_inferred__2/i__carry__1_CO_UNCONNECTED ;
  wire [2:0]\NLW_eqOp_inferred__2/i__carry__1_O_UNCONNECTED ;
  wire [3:2]\NLW_gtxd.sig_txd_packet_size_reg[28]_i_1_CO_UNCONNECTED ;
  wire [3:3]\NLW_gtxd.sig_txd_packet_size_reg[28]_i_1_O_UNCONNECTED ;
  wire [3:1]NLW_i__carry__1_i_4_CO_UNCONNECTED;
  wire [3:2]NLW_i__carry__1_i_4_O_UNCONNECTED;
  wire [3:0]NLW_p_0_out__24_carry__1_CO_UNCONNECTED;
  wire [3:1]NLW_p_0_out__24_carry__1_O_UNCONNECTED;
  wire [3:0]NLW_p_0_out__49_carry__1_CO_UNCONNECTED;
  wire [3:1]NLW_p_0_out__49_carry__1_O_UNCONNECTED;
  wire [3:0]NLW_p_0_out_carry__1_CO_UNCONNECTED;
  wire [3:1]NLW_p_0_out_carry__1_O_UNCONNECTED;
  wire [3:3]\NLW_p_2_out_inferred__2/i__carry__3_CO_UNCONNECTED ;
  wire [3:0]NLW_plusOp_carry__4_CO_UNCONNECTED;
  wire [3:1]NLW_plusOp_carry__4_O_UNCONNECTED;

  (* SOFT_HLUTNM = "soft_lutpair69" *) 
  LUT3 #(
    .INIT(8'hEF)) 
    \GEN_BKEND_CE_REGISTERS[12].ce_out_i[12]_i_1 
       (.I0(s_axi_awready),
        .I1(s_axi_arready),
        .I2(s_axi_aresetn),
        .O(cs_ce_clr));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT5 #(
    .INIT(32'h0C00AAAA)) 
    IP2Bus_Error_i_1
       (.I0(sig_IP2Bus_Error),
        .I1(IP2Bus_Error),
        .I2(IPIC_STATE),
        .I3(sig_Bus2IP_CS),
        .I4(s_axi_aresetn),
        .O(IP2Bus_Error_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    IP2Bus_Error_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(IP2Bus_Error_i_1_n_0),
        .Q(sig_IP2Bus_Error),
        .R(1'b0));
  LUT1 #(
    .INIT(2'h1)) 
    IP2Bus_RdAck_i_1
       (.I0(s_axi_aresetn),
        .O(SR));
  FDRE #(
    .INIT(1'b0)) 
    IP2Bus_RdAck_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(bus2ip_rnw_i_reg),
        .Q(s_axi_arready),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    IP2Bus_WrAck_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(bus2ip_rnw_i_reg_0),
        .Q(s_axi_awready),
        .R(1'b0));
  FDRE IPIC_STATE_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(sig_Bus2IP_CS),
        .Q(IPIC_STATE),
        .R(SR));
  LUT3 #(
    .INIT(8'h40)) 
    \__13/RAM_reg_64_127_0_2_i_1 
       (.I0(\grxd.COMP_rx_len_fifo_n_1 ),
        .I1(rx_len_wr_en),
        .I2(\FAMILY_SUPPORTED.I_SYNC_FIFO_BRAM/inst_fifo_gen/gconvfifo.rf/grf.rf/p_11_out ),
        .O(\__13/RAM_reg_64_127_0_2_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair57" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \__13/gpr1.dout_i[0]_i_1 
       (.I0(\grxd.COMP_rx_len_fifo_n_8 ),
        .I1(\FAMILY_SUPPORTED.I_SYNC_FIFO_BRAM/inst_fifo_gen/gconvfifo.rf/grf.rf/p_0_out ),
        .I2(\grxd.COMP_rx_len_fifo_n_3 ),
        .O(\__13/gpr1.dout_i[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair63" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \__13/gpr1.dout_i[10]_i_1 
       (.I0(\grxd.COMP_rx_len_fifo_n_27 ),
        .I1(\FAMILY_SUPPORTED.I_SYNC_FIFO_BRAM/inst_fifo_gen/gconvfifo.rf/grf.rf/p_0_out ),
        .I2(\grxd.COMP_rx_len_fifo_n_24 ),
        .O(\__13/gpr1.dout_i[10]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair64" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \__13/gpr1.dout_i[11]_i_1 
       (.I0(\grxd.COMP_rx_len_fifo_n_28 ),
        .I1(\FAMILY_SUPPORTED.I_SYNC_FIFO_BRAM/inst_fifo_gen/gconvfifo.rf/grf.rf/p_0_out ),
        .I2(\grxd.COMP_rx_len_fifo_n_25 ),
        .O(\__13/gpr1.dout_i[11]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair64" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \__13/gpr1.dout_i[12]_i_1 
       (.I0(\grxd.COMP_rx_len_fifo_n_32 ),
        .I1(\FAMILY_SUPPORTED.I_SYNC_FIFO_BRAM/inst_fifo_gen/gconvfifo.rf/grf.rf/p_0_out ),
        .I2(\grxd.COMP_rx_len_fifo_n_29 ),
        .O(\__13/gpr1.dout_i[12]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair65" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \__13/gpr1.dout_i[13]_i_1 
       (.I0(\grxd.COMP_rx_len_fifo_n_33 ),
        .I1(\FAMILY_SUPPORTED.I_SYNC_FIFO_BRAM/inst_fifo_gen/gconvfifo.rf/grf.rf/p_0_out ),
        .I2(\grxd.COMP_rx_len_fifo_n_30 ),
        .O(\__13/gpr1.dout_i[13]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair65" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \__13/gpr1.dout_i[14]_i_1 
       (.I0(\grxd.COMP_rx_len_fifo_n_34 ),
        .I1(\FAMILY_SUPPORTED.I_SYNC_FIFO_BRAM/inst_fifo_gen/gconvfifo.rf/grf.rf/p_0_out ),
        .I2(\grxd.COMP_rx_len_fifo_n_31 ),
        .O(\__13/gpr1.dout_i[14]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair66" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \__13/gpr1.dout_i[15]_i_1 
       (.I0(\grxd.COMP_rx_len_fifo_n_38 ),
        .I1(\FAMILY_SUPPORTED.I_SYNC_FIFO_BRAM/inst_fifo_gen/gconvfifo.rf/grf.rf/p_0_out ),
        .I2(\grxd.COMP_rx_len_fifo_n_35 ),
        .O(\__13/gpr1.dout_i[15]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair66" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \__13/gpr1.dout_i[16]_i_1 
       (.I0(\grxd.COMP_rx_len_fifo_n_39 ),
        .I1(\FAMILY_SUPPORTED.I_SYNC_FIFO_BRAM/inst_fifo_gen/gconvfifo.rf/grf.rf/p_0_out ),
        .I2(\grxd.COMP_rx_len_fifo_n_36 ),
        .O(\__13/gpr1.dout_i[16]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair67" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \__13/gpr1.dout_i[17]_i_1 
       (.I0(\grxd.COMP_rx_len_fifo_n_40 ),
        .I1(\FAMILY_SUPPORTED.I_SYNC_FIFO_BRAM/inst_fifo_gen/gconvfifo.rf/grf.rf/p_0_out ),
        .I2(\grxd.COMP_rx_len_fifo_n_37 ),
        .O(\__13/gpr1.dout_i[17]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair67" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \__13/gpr1.dout_i[18]_i_1 
       (.I0(\grxd.COMP_rx_len_fifo_n_44 ),
        .I1(\FAMILY_SUPPORTED.I_SYNC_FIFO_BRAM/inst_fifo_gen/gconvfifo.rf/grf.rf/p_0_out ),
        .I2(\grxd.COMP_rx_len_fifo_n_41 ),
        .O(\__13/gpr1.dout_i[18]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair68" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \__13/gpr1.dout_i[19]_i_1 
       (.I0(\grxd.COMP_rx_len_fifo_n_45 ),
        .I1(\FAMILY_SUPPORTED.I_SYNC_FIFO_BRAM/inst_fifo_gen/gconvfifo.rf/grf.rf/p_0_out ),
        .I2(\grxd.COMP_rx_len_fifo_n_42 ),
        .O(\__13/gpr1.dout_i[19]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair59" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \__13/gpr1.dout_i[1]_i_1 
       (.I0(\grxd.COMP_rx_len_fifo_n_9 ),
        .I1(\FAMILY_SUPPORTED.I_SYNC_FIFO_BRAM/inst_fifo_gen/gconvfifo.rf/grf.rf/p_0_out ),
        .I2(\grxd.COMP_rx_len_fifo_n_4 ),
        .O(\__13/gpr1.dout_i[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair68" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \__13/gpr1.dout_i[20]_i_1 
       (.I0(\grxd.COMP_rx_len_fifo_n_46 ),
        .I1(\FAMILY_SUPPORTED.I_SYNC_FIFO_BRAM/inst_fifo_gen/gconvfifo.rf/grf.rf/p_0_out ),
        .I2(\grxd.COMP_rx_len_fifo_n_43 ),
        .O(\__13/gpr1.dout_i[20]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair57" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \__13/gpr1.dout_i[21]_i_2 
       (.I0(\grxd.COMP_rx_len_fifo_n_48 ),
        .I1(\FAMILY_SUPPORTED.I_SYNC_FIFO_BRAM/inst_fifo_gen/gconvfifo.rf/grf.rf/p_0_out ),
        .I2(\grxd.COMP_rx_len_fifo_n_47 ),
        .O(\__13/gpr1.dout_i[21]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair60" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \__13/gpr1.dout_i[2]_i_1 
       (.I0(\grxd.COMP_rx_len_fifo_n_10 ),
        .I1(\FAMILY_SUPPORTED.I_SYNC_FIFO_BRAM/inst_fifo_gen/gconvfifo.rf/grf.rf/p_0_out ),
        .I2(\grxd.COMP_rx_len_fifo_n_5 ),
        .O(\__13/gpr1.dout_i[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair61" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \__13/gpr1.dout_i[3]_i_1 
       (.I0(\grxd.COMP_rx_len_fifo_n_14 ),
        .I1(\FAMILY_SUPPORTED.I_SYNC_FIFO_BRAM/inst_fifo_gen/gconvfifo.rf/grf.rf/p_0_out ),
        .I2(\grxd.COMP_rx_len_fifo_n_11 ),
        .O(\__13/gpr1.dout_i[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair59" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \__13/gpr1.dout_i[4]_i_1 
       (.I0(\grxd.COMP_rx_len_fifo_n_15 ),
        .I1(\FAMILY_SUPPORTED.I_SYNC_FIFO_BRAM/inst_fifo_gen/gconvfifo.rf/grf.rf/p_0_out ),
        .I2(\grxd.COMP_rx_len_fifo_n_12 ),
        .O(\__13/gpr1.dout_i[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair60" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \__13/gpr1.dout_i[5]_i_1 
       (.I0(\grxd.COMP_rx_len_fifo_n_16 ),
        .I1(\FAMILY_SUPPORTED.I_SYNC_FIFO_BRAM/inst_fifo_gen/gconvfifo.rf/grf.rf/p_0_out ),
        .I2(\grxd.COMP_rx_len_fifo_n_13 ),
        .O(\__13/gpr1.dout_i[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair61" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \__13/gpr1.dout_i[6]_i_1 
       (.I0(\grxd.COMP_rx_len_fifo_n_20 ),
        .I1(\FAMILY_SUPPORTED.I_SYNC_FIFO_BRAM/inst_fifo_gen/gconvfifo.rf/grf.rf/p_0_out ),
        .I2(\grxd.COMP_rx_len_fifo_n_17 ),
        .O(\__13/gpr1.dout_i[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair62" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \__13/gpr1.dout_i[7]_i_1 
       (.I0(\grxd.COMP_rx_len_fifo_n_21 ),
        .I1(\FAMILY_SUPPORTED.I_SYNC_FIFO_BRAM/inst_fifo_gen/gconvfifo.rf/grf.rf/p_0_out ),
        .I2(\grxd.COMP_rx_len_fifo_n_18 ),
        .O(\__13/gpr1.dout_i[7]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair62" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \__13/gpr1.dout_i[8]_i_1 
       (.I0(\grxd.COMP_rx_len_fifo_n_22 ),
        .I1(\FAMILY_SUPPORTED.I_SYNC_FIFO_BRAM/inst_fifo_gen/gconvfifo.rf/grf.rf/p_0_out ),
        .I2(\grxd.COMP_rx_len_fifo_n_19 ),
        .O(\__13/gpr1.dout_i[8]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair63" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \__13/gpr1.dout_i[9]_i_1 
       (.I0(\grxd.COMP_rx_len_fifo_n_26 ),
        .I1(\FAMILY_SUPPORTED.I_SYNC_FIFO_BRAM/inst_fifo_gen/gconvfifo.rf/grf.rf/p_0_out ),
        .I2(\grxd.COMP_rx_len_fifo_n_23 ),
        .O(\__13/gpr1.dout_i[9]_i_1_n_0 ));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \eqOp_inferred__1/i__carry 
       (.CI(1'b0),
        .CO({\eqOp_inferred__1/i__carry_n_0 ,\eqOp_inferred__1/i__carry_n_1 ,\eqOp_inferred__1/i__carry_n_2 ,\eqOp_inferred__1/i__carry_n_3 }),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_eqOp_inferred__1/i__carry_O_UNCONNECTED [3:0]),
        .S({i__carry_i_1_n_0,i__carry_i_2_n_0,i__carry_i_3_n_0,i__carry_i_4_n_0}));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \eqOp_inferred__1/i__carry__0 
       (.CI(\eqOp_inferred__1/i__carry_n_0 ),
        .CO({\eqOp_inferred__1/i__carry__0_n_0 ,\eqOp_inferred__1/i__carry__0_n_1 ,\eqOp_inferred__1/i__carry__0_n_2 ,\eqOp_inferred__1/i__carry__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_eqOp_inferred__1/i__carry__0_O_UNCONNECTED [3:0]),
        .S({i__carry__0_i_1_n_0,i__carry__0_i_2_n_0,i__carry__0_i_3_n_0,i__carry__0_i_4_n_0}));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \eqOp_inferred__1/i__carry__1 
       (.CI(\eqOp_inferred__1/i__carry__0_n_0 ),
        .CO({\NLW_eqOp_inferred__1/i__carry__1_CO_UNCONNECTED [3],eqOp0_out,\eqOp_inferred__1/i__carry__1_n_2 ,\eqOp_inferred__1/i__carry__1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\eqOp_inferred__1/i__carry__1_n_4 ,\NLW_eqOp_inferred__1/i__carry__1_O_UNCONNECTED [2:0]}),
        .S({1'b1,i__carry__1_i_1__0_n_0,i__carry__1_i_2_n_0,i__carry__1_i_3_n_0}));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \eqOp_inferred__2/i__carry 
       (.CI(1'b0),
        .CO({\eqOp_inferred__2/i__carry_n_0 ,\eqOp_inferred__2/i__carry_n_1 ,\eqOp_inferred__2/i__carry_n_2 ,\eqOp_inferred__2/i__carry_n_3 }),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_eqOp_inferred__2/i__carry_O_UNCONNECTED [3:0]),
        .S({i__carry_i_1__0_n_0,i__carry_i_2__0_n_0,i__carry_i_3__0_n_0,i__carry_i_4__0_n_0}));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \eqOp_inferred__2/i__carry__0 
       (.CI(\eqOp_inferred__2/i__carry_n_0 ),
        .CO({\eqOp_inferred__2/i__carry__0_n_0 ,\eqOp_inferred__2/i__carry__0_n_1 ,\eqOp_inferred__2/i__carry__0_n_2 ,\eqOp_inferred__2/i__carry__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_eqOp_inferred__2/i__carry__0_O_UNCONNECTED [3:0]),
        .S({i__carry__0_i_1__0_n_0,i__carry__0_i_2__0_n_0,i__carry__0_i_3__0_n_0,i__carry__0_i_4__0_n_0}));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \eqOp_inferred__2/i__carry__1 
       (.CI(\eqOp_inferred__2/i__carry__0_n_0 ),
        .CO({\NLW_eqOp_inferred__2/i__carry__1_CO_UNCONNECTED [3],\eqOp_inferred__2/i__carry__1_n_1 ,\eqOp_inferred__2/i__carry__1_n_2 ,\eqOp_inferred__2/i__carry__1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\eqOp_inferred__2/i__carry__1_n_4 ,\NLW_eqOp_inferred__2/i__carry__1_O_UNCONNECTED [2:0]}),
        .S({1'b1,i__carry__1_i_1_n_0,i__carry__1_i_2__0_n_0,i__carry__1_i_3__0_n_0}));
  axi_fifo_mm_s_0_fifo__parameterized0 \grxd.COMP_RX_FIFO 
       (.D({p_0_out__49_carry__1_n_7,p_0_out__49_carry__0_n_4,p_0_out__49_carry__0_n_5,p_0_out__49_carry__0_n_6,p_0_out__49_carry__0_n_7,p_0_out__49_carry_n_4,p_0_out__49_carry_n_5,p_0_out__49_carry_n_6,p_0_out__49_carry_n_7}),
        .DI({Q[3:1],\grxd.COMP_RX_FIFO_n_14 }),
        .O(\p_2_out_inferred__2/i__carry_n_7 ),
        .Q({Q[9:4],Q[0]}),
        .S(\grxd.COMP_RX_FIFO_n_27 ),
        .axi4_fifo_rd_en_i(axi4_fifo_rd_en_i),
        .axi_str_rxd_tdata(axi_str_rxd_tdata),
        .axi_str_rxd_tlast(axi_str_rxd_tlast),
        .axi_str_rxd_tready(axi_str_rxd_tready),
        .axi_str_rxd_tvalid(axi_str_rxd_tvalid),
        .axis_rd_en__0(axis_rd_en__0),
        .\count_reg[4] ({\grxd.COMP_RX_FIFO_n_28 ,\grxd.COMP_RX_FIFO_n_29 ,\grxd.COMP_RX_FIFO_n_30 ,\grxd.COMP_RX_FIFO_n_31 }),
        .\count_reg[8] ({\grxd.COMP_RX_FIFO_n_32 ,\grxd.COMP_RX_FIFO_n_33 ,\grxd.COMP_RX_FIFO_n_34 ,\grxd.COMP_RX_FIFO_n_35 }),
        .empty_fwft_i(empty_fwft_i),
        .fg_rxd_wr_length(fg_rxd_wr_length[2]),
        .\gaxi_full_sm.r_last_r_reg (\gaxi_full_sm.r_last_r_reg ),
        .\gaxi_full_sm.r_valid_r_reg (\gaxi_full_sm.r_valid_r_reg ),
        .\gc0.count_reg[0] (\gc0.count_reg[0] ),
        .\gc0.count_reg[0]_0 (\gc0.count_reg[0]_0 ),
        .\grxd.fg_rxd_wr_length_reg[1] (\grxd.COMP_RX_FIFO_n_0 ),
        .\grxd.fg_rxd_wr_length_reg[21] (\grxd.COMP_RX_FIFO_n_4 ),
        .\grxd.fg_rxd_wr_length_reg[2] (\grxd.COMP_RX_FIFO_n_36 ),
        .\grxd.fg_rxd_wr_length_reg[2]_0 (plusOp_carry_n_6),
        .\grxd.sig_rxd_rd_data_reg[32] (\grxd.COMP_RX_FIFO_n_7 ),
        .p_8_out(p_8_out),
        .p_9_out(p_9_out),
        .rx_complete(rx_complete),
        .rx_len_wr_en(rx_len_wr_en),
        .rx_str_wr_en(rx_str_wr_en),
        .s_axi4_araddr(s_axi4_araddr),
        .\s_axi4_araddr[21] (\s_axi4_araddr[21] ),
        .\s_axi4_araddr[29] (\s_axi4_araddr[29] ),
        .s_axi4_rdata(s_axi4_rdata),
        .s_axi_aclk(s_axi_aclk),
        .s_axi_aresetn(s_axi_aresetn),
        .sig_axi_rd_en(sig_axi_rd_en),
        .sig_rd_rlen_reg(sig_rd_rlen_reg_n_0),
        .\sig_register_array_reg[0][1] (\sig_register_array_reg[0][1]_0 ),
        .\sig_register_array_reg[0][2] (\sig_register_array_reg[0][2]_0 ),
        .\sig_register_array_reg[0][2]_0 (\sig_register_array_reg[0][2]_1 ),
        .sig_rx_channel_reset_reg(\gpr1.dout_i_reg[0] ),
        .sig_rxd_pe_event__1(sig_rxd_pe_event__1),
        .sig_rxd_pf_event__1(sig_rxd_pf_event__1),
        .sig_rxd_prog_empty_d1(sig_rxd_prog_empty_d1),
        .sig_rxd_prog_full_d1(sig_rxd_prog_full_d1),
        .sig_rxd_rd_data(sig_rxd_rd_data),
        .sig_str_rst_reg(sync_areset_n_reg_inv));
  axi_fifo_mm_s_0_sync_fifo_fg \grxd.COMP_rx_len_fifo 
       (.D({\__13/gpr1.dout_i[21]_i_2_n_0 ,\__13/gpr1.dout_i[20]_i_1_n_0 ,\__13/gpr1.dout_i[19]_i_1_n_0 ,\__13/gpr1.dout_i[18]_i_1_n_0 ,\__13/gpr1.dout_i[17]_i_1_n_0 ,\__13/gpr1.dout_i[16]_i_1_n_0 ,\__13/gpr1.dout_i[15]_i_1_n_0 ,\__13/gpr1.dout_i[14]_i_1_n_0 ,\__13/gpr1.dout_i[13]_i_1_n_0 ,\__13/gpr1.dout_i[12]_i_1_n_0 ,\__13/gpr1.dout_i[11]_i_1_n_0 ,\__13/gpr1.dout_i[10]_i_1_n_0 ,\__13/gpr1.dout_i[9]_i_1_n_0 ,\__13/gpr1.dout_i[8]_i_1_n_0 ,\__13/gpr1.dout_i[7]_i_1_n_0 ,\__13/gpr1.dout_i[6]_i_1_n_0 ,\__13/gpr1.dout_i[5]_i_1_n_0 ,\__13/gpr1.dout_i[4]_i_1_n_0 ,\__13/gpr1.dout_i[3]_i_1_n_0 ,\__13/gpr1.dout_i[2]_i_1_n_0 ,\__13/gpr1.dout_i[1]_i_1_n_0 ,\__13/gpr1.dout_i[0]_i_1_n_0 }),
        .Q(\FAMILY_SUPPORTED.I_SYNC_FIFO_BRAM/inst_fifo_gen/gconvfifo.rf/grf.rf/p_0_out ),
        .SR(sig_rxd_reset),
        .axi_str_rxd_tlast(axi_str_rxd_tlast),
        .axi_str_rxd_tvalid(axi_str_rxd_tvalid),
        .fg_rxd_wr_length(fg_rxd_wr_length),
        .\gcc0.gc0.count_d1_reg[6] (\grxd.COMP_rx_len_fifo_n_1 ),
        .\gpr1.dout_i_reg[0] (\grxd.COMP_rx_len_fifo_n_3 ),
        .\gpr1.dout_i_reg[0]_0 (\FAMILY_SUPPORTED.I_SYNC_FIFO_BRAM/inst_fifo_gen/gconvfifo.rf/grf.rf/p_11_out ),
        .\gpr1.dout_i_reg[0]_1 (\grxd.COMP_rx_len_fifo_n_8 ),
        .\gpr1.dout_i_reg[10] (\grxd.COMP_rx_len_fifo_n_24 ),
        .\gpr1.dout_i_reg[10]_0 (\grxd.COMP_rx_len_fifo_n_27 ),
        .\gpr1.dout_i_reg[11] (\grxd.COMP_rx_len_fifo_n_25 ),
        .\gpr1.dout_i_reg[11]_0 (\grxd.COMP_rx_len_fifo_n_28 ),
        .\gpr1.dout_i_reg[12] (\grxd.COMP_rx_len_fifo_n_29 ),
        .\gpr1.dout_i_reg[12]_0 (\grxd.COMP_rx_len_fifo_n_32 ),
        .\gpr1.dout_i_reg[13] (\grxd.COMP_rx_len_fifo_n_30 ),
        .\gpr1.dout_i_reg[13]_0 (\grxd.COMP_rx_len_fifo_n_33 ),
        .\gpr1.dout_i_reg[14] (\grxd.COMP_rx_len_fifo_n_31 ),
        .\gpr1.dout_i_reg[14]_0 (\grxd.COMP_rx_len_fifo_n_34 ),
        .\gpr1.dout_i_reg[15] (\grxd.COMP_rx_len_fifo_n_35 ),
        .\gpr1.dout_i_reg[15]_0 (\grxd.COMP_rx_len_fifo_n_38 ),
        .\gpr1.dout_i_reg[16] (\grxd.COMP_rx_len_fifo_n_36 ),
        .\gpr1.dout_i_reg[16]_0 (\grxd.COMP_rx_len_fifo_n_39 ),
        .\gpr1.dout_i_reg[17] (\grxd.COMP_rx_len_fifo_n_37 ),
        .\gpr1.dout_i_reg[17]_0 (\grxd.COMP_rx_len_fifo_n_40 ),
        .\gpr1.dout_i_reg[18] (\grxd.COMP_rx_len_fifo_n_41 ),
        .\gpr1.dout_i_reg[18]_0 (\grxd.COMP_rx_len_fifo_n_44 ),
        .\gpr1.dout_i_reg[19] (\grxd.COMP_rx_len_fifo_n_42 ),
        .\gpr1.dout_i_reg[19]_0 (\grxd.COMP_rx_len_fifo_n_45 ),
        .\gpr1.dout_i_reg[1] (\grxd.COMP_rx_len_fifo_n_4 ),
        .\gpr1.dout_i_reg[1]_0 (\grxd.COMP_rx_len_fifo_n_9 ),
        .\gpr1.dout_i_reg[20] (\grxd.COMP_rx_len_fifo_n_43 ),
        .\gpr1.dout_i_reg[20]_0 (\grxd.COMP_rx_len_fifo_n_46 ),
        .\gpr1.dout_i_reg[21] (\grxd.COMP_rx_len_fifo_n_47 ),
        .\gpr1.dout_i_reg[21]_0 (\grxd.COMP_rx_len_fifo_n_48 ),
        .\gpr1.dout_i_reg[2] (\grxd.COMP_rx_len_fifo_n_5 ),
        .\gpr1.dout_i_reg[2]_0 (\grxd.COMP_rx_len_fifo_n_10 ),
        .\gpr1.dout_i_reg[3] (\grxd.COMP_rx_len_fifo_n_11 ),
        .\gpr1.dout_i_reg[3]_0 (\grxd.COMP_rx_len_fifo_n_14 ),
        .\gpr1.dout_i_reg[4] (\grxd.COMP_rx_len_fifo_n_12 ),
        .\gpr1.dout_i_reg[4]_0 (\grxd.COMP_rx_len_fifo_n_15 ),
        .\gpr1.dout_i_reg[5] (\grxd.COMP_rx_len_fifo_n_13 ),
        .\gpr1.dout_i_reg[5]_0 (\grxd.COMP_rx_len_fifo_n_16 ),
        .\gpr1.dout_i_reg[6] (\grxd.COMP_rx_len_fifo_n_17 ),
        .\gpr1.dout_i_reg[6]_0 (\grxd.COMP_rx_len_fifo_n_20 ),
        .\gpr1.dout_i_reg[7] (\grxd.COMP_rx_len_fifo_n_18 ),
        .\gpr1.dout_i_reg[7]_0 (\grxd.COMP_rx_len_fifo_n_21 ),
        .\gpr1.dout_i_reg[8] (\grxd.COMP_rx_len_fifo_n_19 ),
        .\gpr1.dout_i_reg[8]_0 (\grxd.COMP_rx_len_fifo_n_22 ),
        .\gpr1.dout_i_reg[9] (\grxd.COMP_rx_len_fifo_n_23 ),
        .\gpr1.dout_i_reg[9]_0 (\grxd.COMP_rx_len_fifo_n_26 ),
        .out(out),
        .p_13_in(p_13_in),
        .ram_full_fb_i_reg(\__13/RAM_reg_64_127_0_2_i_1_n_0 ),
        .ram_full_i_reg(\grxd.COMP_RX_FIFO_n_0 ),
        .rx_fg_len_empty_d1(rx_fg_len_empty_d1),
        .rx_len_wr_en(rx_len_wr_en),
        .s_axi_aclk(s_axi_aclk),
        .s_axi_aresetn(s_axi_aresetn),
        .\sig_ip2bus_data_reg[10] (\sig_ip2bus_data_reg[10]_1 ),
        .sig_rd_rlen_reg(sig_rd_rlen_reg_n_0),
        .sig_rx_channel_reset_reg(\gpr1.dout_i_reg[0] ),
        .sig_str_rst_reg(sync_areset_n_reg_inv));
  (* SOFT_HLUTNM = "soft_lutpair58" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \grxd.fg_rxd_wr_length[10]_i_1 
       (.I0(\p_2_out_inferred__2/i__carry__1_n_7 ),
        .I1(axi_str_rxd_tlast),
        .I2(plusOp_carry__1_n_6),
        .O(\grxd.fg_rxd_wr_length[10]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair56" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \grxd.fg_rxd_wr_length[11]_i_1 
       (.I0(\p_2_out_inferred__2/i__carry__1_n_6 ),
        .I1(axi_str_rxd_tlast),
        .I2(plusOp_carry__1_n_5),
        .O(\grxd.fg_rxd_wr_length[11]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair54" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \grxd.fg_rxd_wr_length[12]_i_1 
       (.I0(\p_2_out_inferred__2/i__carry__1_n_5 ),
        .I1(axi_str_rxd_tlast),
        .I2(plusOp_carry__1_n_4),
        .O(\grxd.fg_rxd_wr_length[12]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair56" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \grxd.fg_rxd_wr_length[13]_i_1 
       (.I0(\p_2_out_inferred__2/i__carry__1_n_4 ),
        .I1(axi_str_rxd_tlast),
        .I2(plusOp_carry__2_n_7),
        .O(\grxd.fg_rxd_wr_length[13]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair54" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \grxd.fg_rxd_wr_length[14]_i_1 
       (.I0(\p_2_out_inferred__2/i__carry__2_n_7 ),
        .I1(axi_str_rxd_tlast),
        .I2(plusOp_carry__2_n_6),
        .O(\grxd.fg_rxd_wr_length[14]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair53" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \grxd.fg_rxd_wr_length[15]_i_1 
       (.I0(\p_2_out_inferred__2/i__carry__2_n_6 ),
        .I1(axi_str_rxd_tlast),
        .I2(plusOp_carry__2_n_5),
        .O(\grxd.fg_rxd_wr_length[15]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair55" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \grxd.fg_rxd_wr_length[16]_i_1 
       (.I0(\p_2_out_inferred__2/i__carry__2_n_5 ),
        .I1(axi_str_rxd_tlast),
        .I2(plusOp_carry__2_n_4),
        .O(\grxd.fg_rxd_wr_length[16]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \grxd.fg_rxd_wr_length[17]_i_1 
       (.I0(\p_2_out_inferred__2/i__carry__2_n_4 ),
        .I1(axi_str_rxd_tlast),
        .I2(plusOp_carry__3_n_7),
        .O(\grxd.fg_rxd_wr_length[17]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \grxd.fg_rxd_wr_length[18]_i_1 
       (.I0(\p_2_out_inferred__2/i__carry__3_n_7 ),
        .I1(axi_str_rxd_tlast),
        .I2(plusOp_carry__3_n_6),
        .O(\grxd.fg_rxd_wr_length[18]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \grxd.fg_rxd_wr_length[19]_i_1 
       (.I0(\p_2_out_inferred__2/i__carry__3_n_6 ),
        .I1(axi_str_rxd_tlast),
        .I2(plusOp_carry__3_n_5),
        .O(\grxd.fg_rxd_wr_length[19]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair55" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \grxd.fg_rxd_wr_length[1]_i_1 
       (.I0(fg_rxd_wr_length[1]),
        .I1(axi_str_rxd_tlast),
        .I2(plusOp_carry_n_7),
        .O(\grxd.fg_rxd_wr_length[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \grxd.fg_rxd_wr_length[20]_i_1 
       (.I0(\p_2_out_inferred__2/i__carry__3_n_5 ),
        .I1(axi_str_rxd_tlast),
        .I2(plusOp_carry__3_n_4),
        .O(\grxd.fg_rxd_wr_length[20]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \grxd.fg_rxd_wr_length[21]_i_3 
       (.I0(\p_2_out_inferred__2/i__carry__3_n_4 ),
        .I1(axi_str_rxd_tlast),
        .I2(plusOp_carry__4_n_7),
        .O(\grxd.fg_rxd_wr_length[21]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \grxd.fg_rxd_wr_length[3]_i_1 
       (.I0(\p_2_out_inferred__2/i__carry_n_6 ),
        .I1(axi_str_rxd_tlast),
        .I2(plusOp_carry_n_5),
        .O(\grxd.fg_rxd_wr_length[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair53" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \grxd.fg_rxd_wr_length[4]_i_1 
       (.I0(\p_2_out_inferred__2/i__carry_n_5 ),
        .I1(axi_str_rxd_tlast),
        .I2(plusOp_carry_n_4),
        .O(\grxd.fg_rxd_wr_length[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \grxd.fg_rxd_wr_length[5]_i_1 
       (.I0(\p_2_out_inferred__2/i__carry_n_4 ),
        .I1(axi_str_rxd_tlast),
        .I2(plusOp_carry__0_n_7),
        .O(\grxd.fg_rxd_wr_length[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \grxd.fg_rxd_wr_length[6]_i_1 
       (.I0(\p_2_out_inferred__2/i__carry__0_n_7 ),
        .I1(axi_str_rxd_tlast),
        .I2(plusOp_carry__0_n_6),
        .O(\grxd.fg_rxd_wr_length[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \grxd.fg_rxd_wr_length[7]_i_1 
       (.I0(\p_2_out_inferred__2/i__carry__0_n_6 ),
        .I1(axi_str_rxd_tlast),
        .I2(plusOp_carry__0_n_5),
        .O(\grxd.fg_rxd_wr_length[7]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \grxd.fg_rxd_wr_length[8]_i_1 
       (.I0(\p_2_out_inferred__2/i__carry__0_n_5 ),
        .I1(axi_str_rxd_tlast),
        .I2(plusOp_carry__0_n_4),
        .O(\grxd.fg_rxd_wr_length[8]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair58" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \grxd.fg_rxd_wr_length[9]_i_1 
       (.I0(\p_2_out_inferred__2/i__carry__0_n_4 ),
        .I1(axi_str_rxd_tlast),
        .I2(plusOp_carry__1_n_7),
        .O(\grxd.fg_rxd_wr_length[9]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \grxd.fg_rxd_wr_length_reg[10] 
       (.C(s_axi_aclk),
        .CE(rx_str_wr_en),
        .D(\grxd.fg_rxd_wr_length[10]_i_1_n_0 ),
        .Q(fg_rxd_wr_length[10]),
        .R(\grxd.COMP_RX_FIFO_n_4 ));
  FDRE #(
    .INIT(1'b0)) 
    \grxd.fg_rxd_wr_length_reg[11] 
       (.C(s_axi_aclk),
        .CE(rx_str_wr_en),
        .D(\grxd.fg_rxd_wr_length[11]_i_1_n_0 ),
        .Q(fg_rxd_wr_length[11]),
        .R(\grxd.COMP_RX_FIFO_n_4 ));
  FDRE #(
    .INIT(1'b0)) 
    \grxd.fg_rxd_wr_length_reg[12] 
       (.C(s_axi_aclk),
        .CE(rx_str_wr_en),
        .D(\grxd.fg_rxd_wr_length[12]_i_1_n_0 ),
        .Q(fg_rxd_wr_length[12]),
        .R(\grxd.COMP_RX_FIFO_n_4 ));
  FDRE #(
    .INIT(1'b0)) 
    \grxd.fg_rxd_wr_length_reg[13] 
       (.C(s_axi_aclk),
        .CE(rx_str_wr_en),
        .D(\grxd.fg_rxd_wr_length[13]_i_1_n_0 ),
        .Q(fg_rxd_wr_length[13]),
        .R(\grxd.COMP_RX_FIFO_n_4 ));
  FDRE #(
    .INIT(1'b0)) 
    \grxd.fg_rxd_wr_length_reg[14] 
       (.C(s_axi_aclk),
        .CE(rx_str_wr_en),
        .D(\grxd.fg_rxd_wr_length[14]_i_1_n_0 ),
        .Q(fg_rxd_wr_length[14]),
        .R(\grxd.COMP_RX_FIFO_n_4 ));
  FDRE #(
    .INIT(1'b0)) 
    \grxd.fg_rxd_wr_length_reg[15] 
       (.C(s_axi_aclk),
        .CE(rx_str_wr_en),
        .D(\grxd.fg_rxd_wr_length[15]_i_1_n_0 ),
        .Q(fg_rxd_wr_length[15]),
        .R(\grxd.COMP_RX_FIFO_n_4 ));
  FDRE #(
    .INIT(1'b0)) 
    \grxd.fg_rxd_wr_length_reg[16] 
       (.C(s_axi_aclk),
        .CE(rx_str_wr_en),
        .D(\grxd.fg_rxd_wr_length[16]_i_1_n_0 ),
        .Q(fg_rxd_wr_length[16]),
        .R(\grxd.COMP_RX_FIFO_n_4 ));
  FDRE #(
    .INIT(1'b0)) 
    \grxd.fg_rxd_wr_length_reg[17] 
       (.C(s_axi_aclk),
        .CE(rx_str_wr_en),
        .D(\grxd.fg_rxd_wr_length[17]_i_1_n_0 ),
        .Q(fg_rxd_wr_length[17]),
        .R(\grxd.COMP_RX_FIFO_n_4 ));
  FDRE #(
    .INIT(1'b0)) 
    \grxd.fg_rxd_wr_length_reg[18] 
       (.C(s_axi_aclk),
        .CE(rx_str_wr_en),
        .D(\grxd.fg_rxd_wr_length[18]_i_1_n_0 ),
        .Q(fg_rxd_wr_length[18]),
        .R(\grxd.COMP_RX_FIFO_n_4 ));
  FDRE #(
    .INIT(1'b0)) 
    \grxd.fg_rxd_wr_length_reg[19] 
       (.C(s_axi_aclk),
        .CE(rx_str_wr_en),
        .D(\grxd.fg_rxd_wr_length[19]_i_1_n_0 ),
        .Q(fg_rxd_wr_length[19]),
        .R(\grxd.COMP_RX_FIFO_n_4 ));
  FDRE #(
    .INIT(1'b0)) 
    \grxd.fg_rxd_wr_length_reg[1] 
       (.C(s_axi_aclk),
        .CE(rx_str_wr_en),
        .D(\grxd.fg_rxd_wr_length[1]_i_1_n_0 ),
        .Q(fg_rxd_wr_length[1]),
        .R(\grxd.COMP_RX_FIFO_n_4 ));
  FDRE #(
    .INIT(1'b0)) 
    \grxd.fg_rxd_wr_length_reg[20] 
       (.C(s_axi_aclk),
        .CE(rx_str_wr_en),
        .D(\grxd.fg_rxd_wr_length[20]_i_1_n_0 ),
        .Q(fg_rxd_wr_length[20]),
        .R(\grxd.COMP_RX_FIFO_n_4 ));
  FDRE #(
    .INIT(1'b0)) 
    \grxd.fg_rxd_wr_length_reg[21] 
       (.C(s_axi_aclk),
        .CE(rx_str_wr_en),
        .D(\grxd.fg_rxd_wr_length[21]_i_3_n_0 ),
        .Q(fg_rxd_wr_length[21]),
        .R(\grxd.COMP_RX_FIFO_n_4 ));
  FDRE #(
    .INIT(1'b0)) 
    \grxd.fg_rxd_wr_length_reg[2] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(\grxd.COMP_RX_FIFO_n_36 ),
        .Q(fg_rxd_wr_length[2]),
        .R(Axi_Str_TxC_AReset));
  FDRE #(
    .INIT(1'b0)) 
    \grxd.fg_rxd_wr_length_reg[3] 
       (.C(s_axi_aclk),
        .CE(rx_str_wr_en),
        .D(\grxd.fg_rxd_wr_length[3]_i_1_n_0 ),
        .Q(fg_rxd_wr_length[3]),
        .R(\grxd.COMP_RX_FIFO_n_4 ));
  FDRE #(
    .INIT(1'b0)) 
    \grxd.fg_rxd_wr_length_reg[4] 
       (.C(s_axi_aclk),
        .CE(rx_str_wr_en),
        .D(\grxd.fg_rxd_wr_length[4]_i_1_n_0 ),
        .Q(fg_rxd_wr_length[4]),
        .R(\grxd.COMP_RX_FIFO_n_4 ));
  FDRE #(
    .INIT(1'b0)) 
    \grxd.fg_rxd_wr_length_reg[5] 
       (.C(s_axi_aclk),
        .CE(rx_str_wr_en),
        .D(\grxd.fg_rxd_wr_length[5]_i_1_n_0 ),
        .Q(fg_rxd_wr_length[5]),
        .R(\grxd.COMP_RX_FIFO_n_4 ));
  FDRE #(
    .INIT(1'b0)) 
    \grxd.fg_rxd_wr_length_reg[6] 
       (.C(s_axi_aclk),
        .CE(rx_str_wr_en),
        .D(\grxd.fg_rxd_wr_length[6]_i_1_n_0 ),
        .Q(fg_rxd_wr_length[6]),
        .R(\grxd.COMP_RX_FIFO_n_4 ));
  FDRE #(
    .INIT(1'b0)) 
    \grxd.fg_rxd_wr_length_reg[7] 
       (.C(s_axi_aclk),
        .CE(rx_str_wr_en),
        .D(\grxd.fg_rxd_wr_length[7]_i_1_n_0 ),
        .Q(fg_rxd_wr_length[7]),
        .R(\grxd.COMP_RX_FIFO_n_4 ));
  FDRE #(
    .INIT(1'b0)) 
    \grxd.fg_rxd_wr_length_reg[8] 
       (.C(s_axi_aclk),
        .CE(rx_str_wr_en),
        .D(\grxd.fg_rxd_wr_length[8]_i_1_n_0 ),
        .Q(fg_rxd_wr_length[8]),
        .R(\grxd.COMP_RX_FIFO_n_4 ));
  FDRE #(
    .INIT(1'b0)) 
    \grxd.fg_rxd_wr_length_reg[9] 
       (.C(s_axi_aclk),
        .CE(rx_str_wr_en),
        .D(\grxd.fg_rxd_wr_length[9]_i_1_n_0 ),
        .Q(fg_rxd_wr_length[9]),
        .R(\grxd.COMP_RX_FIFO_n_4 ));
  FDSE #(
    .INIT(1'b0)) 
    \grxd.rx_fg_len_empty_d1_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(out),
        .Q(rx_fg_len_empty_d1),
        .S(SR));
  FDRE #(
    .INIT(1'b0)) 
    \grxd.rx_len_wr_en_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(rx_complete),
        .Q(rx_len_wr_en),
        .R(Axi_Str_TxC_AReset));
  FDSE #(
    .INIT(1'b0)) 
    \grxd.sig_rxd_prog_empty_d1_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(p_9_out),
        .Q(sig_rxd_prog_empty_d1),
        .S(SR));
  FDRE #(
    .INIT(1'b0)) 
    \grxd.sig_rxd_prog_full_d1_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(p_8_out),
        .Q(sig_rxd_prog_full_d1),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \grxd.sig_rxd_rd_data_reg[32] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(\grxd.COMP_RX_FIFO_n_7 ),
        .Q(sig_rxd_rd_data),
        .R(1'b0));
  FDRE \gtxc.TXC_STATE_reg[0] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(\gtxd.COMP_TXD_FIFO_n_88 ),
        .Q(\gtxc.TXC_STATE_reg_n_0_[0] ),
        .R(Axi_Str_TxC_AReset));
  FDRE \gtxc.TXC_STATE_reg[1] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(\gtxd.COMP_TXD_FIFO_n_89 ),
        .Q(\gtxc.TXC_STATE_reg_n_0_[1] ),
        .R(Axi_Str_TxC_AReset));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT4 #(
    .INIT(16'hB708)) 
    \gtxc.txc_cntr[0]_i_1 
       (.I0(\gtxc.TXC_STATE_reg_n_0_[0] ),
        .I1(axi_str_txc_tready),
        .I2(\gtxc.TXC_STATE_reg_n_0_[1] ),
        .I3(txc_cntr[0]),
        .O(\gtxc.txc_cntr[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT5 #(
    .INIT(32'hCF7F0080)) 
    \gtxc.txc_cntr[1]_i_1 
       (.I0(txc_cntr[0]),
        .I1(\gtxc.TXC_STATE_reg_n_0_[0] ),
        .I2(axi_str_txc_tready),
        .I3(\gtxc.TXC_STATE_reg_n_0_[1] ),
        .I4(txc_cntr[1]),
        .O(\gtxc.txc_cntr[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hF0FF7FFF00008000)) 
    \gtxc.txc_cntr[2]_i_1 
       (.I0(txc_cntr[1]),
        .I1(txc_cntr[0]),
        .I2(\gtxc.TXC_STATE_reg_n_0_[0] ),
        .I3(axi_str_txc_tready),
        .I4(\gtxc.TXC_STATE_reg_n_0_[1] ),
        .I5(txc_cntr[2]),
        .O(\gtxc.txc_cntr[2]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \gtxc.txc_cntr_reg[0] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(\gtxc.txc_cntr[0]_i_1_n_0 ),
        .Q(txc_cntr[0]),
        .R(Axi_Str_TxC_AReset));
  FDRE #(
    .INIT(1'b0)) 
    \gtxc.txc_cntr_reg[1] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(\gtxc.txc_cntr[1]_i_1_n_0 ),
        .Q(txc_cntr[1]),
        .R(Axi_Str_TxC_AReset));
  FDRE #(
    .INIT(1'b0)) 
    \gtxc.txc_cntr_reg[2] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(\gtxc.txc_cntr[2]_i_1_n_0 ),
        .Q(txc_cntr[2]),
        .R(Axi_Str_TxC_AReset));
  LUT5 #(
    .INIT(32'hFF3F2000)) 
    \gtxc.txc_str_Lastsig_i_1 
       (.I0(\gtxc.txc_str_Lastsig_i_2_n_0 ),
        .I1(\gtxc.TXC_STATE_reg_n_0_[1] ),
        .I2(axi_str_txc_tready),
        .I3(\gtxc.TXC_STATE_reg_n_0_[0] ),
        .I4(axi_str_txc_tlast),
        .O(\gtxc.txc_str_Lastsig_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h04)) 
    \gtxc.txc_str_Lastsig_i_2 
       (.I0(txc_cntr[1]),
        .I1(txc_cntr[2]),
        .I2(txc_cntr[0]),
        .O(\gtxc.txc_str_Lastsig_i_2_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \gtxc.txc_str_Lastsig_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(\gtxc.txc_str_Lastsig_i_1_n_0 ),
        .Q(axi_str_txc_tlast),
        .R(Axi_Str_TxC_AReset));
  LUT2 #(
    .INIT(4'hB)) 
    \gtxc.txc_str_Valid_i_1 
       (.I0(sync_areset_n_reg_inv),
        .I1(s_axi_aresetn),
        .O(Axi_Str_TxC_AReset));
  FDRE #(
    .INIT(1'b0)) 
    \gtxc.txc_str_Valid_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(\gtxd.COMP_TXD_FIFO_n_71 ),
        .Q(axi_str_txc_tvalid),
        .R(Axi_Str_TxC_AReset));
  axi_fifo_mm_s_0_fifo \gtxd.COMP_TXD_FIFO 
       (.D({p_0_out_carry__1_n_7,p_0_out_carry__0_n_4,p_0_out_carry__0_n_5,p_0_out_carry__0_n_6,p_0_out_carry__0_n_7,p_0_out_carry_n_4,p_0_out_carry_n_5,p_0_out_carry_n_6,p_0_out_carry_n_7}),
        .DI({\gfifo_gen.COMP_AXIS_FG_FIFO/COMP_FIFO/inst_fifo_gen/gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg [3:1],\gtxd.COMP_TXD_FIFO_n_40 }),
        .DIADI(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram ),
        .E(E),
        .\GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg[7] (\GEN_BKEND_CE_REGISTERS[7].ce_out_i_reg[7] ),
        .\GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg[9] (\GEN_BKEND_CE_REGISTERS[9].ce_out_i_reg[9] ),
        .Q(\axi_str_txd_tdata[31] ),
        .S(\gtxd.COMP_TXD_FIFO_n_49 ),
        .SR(sig_txd_reset),
        .axi_str_txc_tlast(axi_str_txc_tlast),
        .axi_str_txc_tready(axi_str_txc_tready),
        .axi_str_txc_tvalid(axi_str_txc_tvalid),
        .axi_str_txd_tready(axi_str_txd_tready),
        .axi_str_txd_tvalid(axi_str_txd_tvalid),
        .\count_reg[9] (Q[9:2]),
        .\gaxi_bid_gen.S_AXI_BID_reg[0] (\gaxi_bid_gen.S_AXI_BID_reg[0] ),
        .\gaxi_full_sm.w_ready_r_reg (ram_full_i),
        .\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[4] ({\gtxd.COMP_TXD_FIFO_n_76 ,\gtxd.COMP_TXD_FIFO_n_77 ,\gtxd.COMP_TXD_FIFO_n_78 ,\gtxd.COMP_TXD_FIFO_n_79 }),
        .\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[8] ({\gfifo_gen.COMP_AXIS_FG_FIFO/COMP_FIFO/inst_fifo_gen/gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg [7:4],\gfifo_gen.COMP_AXIS_FG_FIFO/COMP_FIFO/inst_fifo_gen/gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg [0]}),
        .\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[8]_0 ({\gtxd.COMP_TXD_FIFO_n_80 ,\gtxd.COMP_TXD_FIFO_n_81 ,\gtxd.COMP_TXD_FIFO_n_82 ,\gtxd.COMP_TXD_FIFO_n_83 }),
        .\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[4] (\gtxd.COMP_TXD_FIFO_n_66 ),
        .\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[4]_0 ({\gtxd.COMP_TXD_FIFO_n_72 ,\gtxd.COMP_TXD_FIFO_n_73 ,\gtxd.COMP_TXD_FIFO_n_74 ,\gtxd.COMP_TXD_FIFO_n_75 }),
        .\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[7] ({p_0_out__24_carry__1_n_7,p_0_out__24_carry__0_n_4,p_0_out__24_carry__0_n_5,p_0_out__24_carry__0_n_6,p_0_out__24_carry__0_n_7,p_0_out__24_carry_n_4,p_0_out__24_carry_n_5,p_0_out__24_carry_n_6,p_0_out__24_carry_n_7}),
        .\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[8] ({\gtxd.COMP_TXD_FIFO_n_84 ,\gtxd.COMP_TXD_FIFO_n_85 ,\gtxd.COMP_TXD_FIFO_n_86 ,\gtxd.COMP_TXD_FIFO_n_87 }),
        .\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[9] (\gtxd.COMP_TXD_FIFO_n_70 ),
        .\gfifo_gen.gmm2s.vacancy_i_reg[9]_0 ({\gfifo_gen.COMP_AXIS_FG_FIFO/COMP_FIFO/inst_fifo_gen/gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg ,\gfifo_gen.COMP_AXIS_FG_FIFO/COMP_FIFO/inst_fifo_gen/gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg__0 ,\gtxd.COMP_TXD_FIFO_n_57 }),
        .\gtxc.TXC_STATE_reg[0] (\gtxd.COMP_TXD_FIFO_n_88 ),
        .\gtxc.TXC_STATE_reg[0]_0 (\gtxc.TXC_STATE_reg_n_0_[0] ),
        .\gtxc.TXC_STATE_reg[1] (\gtxd.COMP_TXD_FIFO_n_89 ),
        .\gtxc.TXC_STATE_reg[1]_0 (\gtxc.TXC_STATE_reg_n_0_[1] ),
        .\gtxc.txc_cntr_reg[1] (\gtxc.txc_str_Lastsig_i_2_n_0 ),
        .\gtxc.txc_str_Valid_reg (\gtxd.COMP_TXD_FIFO_n_71 ),
        .p_7_out(p_7_out),
        .s_axi4_wdata(s_axi4_wdata),
        .s_axi_aclk(s_axi_aclk),
        .s_axi_aresetn(s_axi_aresetn),
        .\sig_ip2bus_data_reg[22] (\sig_ip2bus_data_reg[22]_0 ),
        .\sig_ip2bus_data_reg[23] (\sig_ip2bus_data_reg[23]_0 ),
        .\sig_ip2bus_data_reg[24] (\sig_ip2bus_data_reg[24]_0 ),
        .\sig_ip2bus_data_reg[25] (\sig_ip2bus_data_reg[25]_0 ),
        .\sig_ip2bus_data_reg[26] (\sig_ip2bus_data_reg[26]_0 ),
        .\sig_ip2bus_data_reg[27] (\sig_ip2bus_data_reg[27]_0 ),
        .\sig_ip2bus_data_reg[28] (\sig_ip2bus_data_reg[28]_0 ),
        .\sig_ip2bus_data_reg[29] (\sig_ip2bus_data_reg[29]_0 ),
        .\sig_ip2bus_data_reg[30] (\sig_ip2bus_data_reg[30]_0 ),
        .\sig_register_array_reg[0][4] (\sig_register_array_reg[0][4]_0 ),
        .sig_str_rst_reg(sync_areset_n_reg_inv),
        .sig_tx_channel_reset_reg(sync_areset_n_reg_inv_0),
        .sig_txd_pe_event__1(sig_txd_pe_event__1),
        .sig_txd_pf_event__1(sig_txd_pf_event__1),
        .sig_txd_prog_empty(sig_txd_prog_empty),
        .sig_txd_prog_empty_d1(sig_txd_prog_empty_d1),
        .sig_txd_prog_full_d1(sig_txd_prog_full_d1),
        .sig_txd_sb_wr_en_reg(sig_txd_sb_wr_en_reg_0),
        .\sig_txd_wr_data_reg[31] ({\sig_txd_wr_data_reg_n_0_[31] ,\sig_txd_wr_data_reg_n_0_[30] ,\sig_txd_wr_data_reg_n_0_[29] ,\sig_txd_wr_data_reg_n_0_[28] ,\sig_txd_wr_data_reg_n_0_[27] ,\sig_txd_wr_data_reg_n_0_[26] ,\sig_txd_wr_data_reg_n_0_[25] ,\sig_txd_wr_data_reg_n_0_[24] ,\sig_txd_wr_data_reg_n_0_[23] ,\sig_txd_wr_data_reg_n_0_[22] ,\sig_txd_wr_data_reg_n_0_[21] ,\sig_txd_wr_data_reg_n_0_[20] ,\sig_txd_wr_data_reg_n_0_[19] ,\sig_txd_wr_data_reg_n_0_[18] ,\sig_txd_wr_data_reg_n_0_[17] ,\sig_txd_wr_data_reg_n_0_[16] ,\sig_txd_wr_data_reg_n_0_[15] ,\sig_txd_wr_data_reg_n_0_[14] ,\sig_txd_wr_data_reg_n_0_[13] ,\sig_txd_wr_data_reg_n_0_[12] ,\sig_txd_wr_data_reg_n_0_[11] ,\sig_txd_wr_data_reg_n_0_[10] ,\sig_txd_wr_data_reg_n_0_[9] ,\sig_txd_wr_data_reg_n_0_[8] ,\sig_txd_wr_data_reg_n_0_[7] ,\sig_txd_wr_data_reg_n_0_[6] ,\sig_txd_wr_data_reg_n_0_[5] ,\sig_txd_wr_data_reg_n_0_[4] ,\sig_txd_wr_data_reg_n_0_[3] ,\sig_txd_wr_data_reg_n_0_[2] ,\sig_txd_wr_data_reg_n_0_[1] ,\sig_txd_wr_data_reg_n_0_[0] }),
        .tx_fifo_or(tx_fifo_or),
        .txd_wr_en(txd_wr_en));
  LUT4 #(
    .INIT(16'hFEFF)) 
    \gtxd.sig_txd_packet_size[0]_i_1 
       (.I0(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram ),
        .I1(sync_areset_n_reg_inv_0),
        .I2(sync_areset_n_reg_inv),
        .I3(s_axi_aresetn),
        .O(\gtxd.sig_txd_packet_size[0]_i_1_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \gtxd.sig_txd_packet_size[0]_i_4 
       (.I0(\gtxd.sig_txd_packet_size_reg [3]),
        .O(\gtxd.sig_txd_packet_size[0]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \gtxd.sig_txd_packet_size[0]_i_5 
       (.I0(\gtxd.sig_txd_packet_size_reg [2]),
        .O(\gtxd.sig_txd_packet_size[0]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \gtxd.sig_txd_packet_size[0]_i_6 
       (.I0(\gtxd.sig_txd_packet_size_reg [1]),
        .O(\gtxd.sig_txd_packet_size[0]_i_6_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \gtxd.sig_txd_packet_size[0]_i_7 
       (.I0(\gtxd.sig_txd_packet_size_reg [0]),
        .O(R[0]));
  LUT1 #(
    .INIT(2'h2)) 
    \gtxd.sig_txd_packet_size[12]_i_2 
       (.I0(\gtxd.sig_txd_packet_size_reg [15]),
        .O(\gtxd.sig_txd_packet_size[12]_i_2_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \gtxd.sig_txd_packet_size[12]_i_3 
       (.I0(\gtxd.sig_txd_packet_size_reg [14]),
        .O(\gtxd.sig_txd_packet_size[12]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \gtxd.sig_txd_packet_size[12]_i_4 
       (.I0(\gtxd.sig_txd_packet_size_reg [13]),
        .O(\gtxd.sig_txd_packet_size[12]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \gtxd.sig_txd_packet_size[12]_i_5 
       (.I0(\gtxd.sig_txd_packet_size_reg [12]),
        .O(\gtxd.sig_txd_packet_size[12]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \gtxd.sig_txd_packet_size[16]_i_2 
       (.I0(\gtxd.sig_txd_packet_size_reg [19]),
        .O(\gtxd.sig_txd_packet_size[16]_i_2_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \gtxd.sig_txd_packet_size[16]_i_3 
       (.I0(\gtxd.sig_txd_packet_size_reg [18]),
        .O(\gtxd.sig_txd_packet_size[16]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \gtxd.sig_txd_packet_size[16]_i_4 
       (.I0(\gtxd.sig_txd_packet_size_reg [17]),
        .O(\gtxd.sig_txd_packet_size[16]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \gtxd.sig_txd_packet_size[16]_i_5 
       (.I0(\gtxd.sig_txd_packet_size_reg [16]),
        .O(\gtxd.sig_txd_packet_size[16]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \gtxd.sig_txd_packet_size[20]_i_2 
       (.I0(\gtxd.sig_txd_packet_size_reg [23]),
        .O(\gtxd.sig_txd_packet_size[20]_i_2_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \gtxd.sig_txd_packet_size[20]_i_3 
       (.I0(\gtxd.sig_txd_packet_size_reg [22]),
        .O(\gtxd.sig_txd_packet_size[20]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \gtxd.sig_txd_packet_size[20]_i_4 
       (.I0(\gtxd.sig_txd_packet_size_reg [21]),
        .O(\gtxd.sig_txd_packet_size[20]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \gtxd.sig_txd_packet_size[20]_i_5 
       (.I0(\gtxd.sig_txd_packet_size_reg [20]),
        .O(\gtxd.sig_txd_packet_size[20]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \gtxd.sig_txd_packet_size[24]_i_2 
       (.I0(\gtxd.sig_txd_packet_size_reg [27]),
        .O(\gtxd.sig_txd_packet_size[24]_i_2_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \gtxd.sig_txd_packet_size[24]_i_3 
       (.I0(\gtxd.sig_txd_packet_size_reg [26]),
        .O(\gtxd.sig_txd_packet_size[24]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \gtxd.sig_txd_packet_size[24]_i_4 
       (.I0(\gtxd.sig_txd_packet_size_reg [25]),
        .O(\gtxd.sig_txd_packet_size[24]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \gtxd.sig_txd_packet_size[24]_i_5 
       (.I0(\gtxd.sig_txd_packet_size_reg [24]),
        .O(\gtxd.sig_txd_packet_size[24]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \gtxd.sig_txd_packet_size[28]_i_2 
       (.I0(\gtxd.sig_txd_packet_size_reg [30]),
        .O(\gtxd.sig_txd_packet_size[28]_i_2_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \gtxd.sig_txd_packet_size[28]_i_3 
       (.I0(\gtxd.sig_txd_packet_size_reg [29]),
        .O(\gtxd.sig_txd_packet_size[28]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \gtxd.sig_txd_packet_size[28]_i_4 
       (.I0(\gtxd.sig_txd_packet_size_reg [28]),
        .O(\gtxd.sig_txd_packet_size[28]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \gtxd.sig_txd_packet_size[4]_i_2 
       (.I0(\gtxd.sig_txd_packet_size_reg [7]),
        .O(\gtxd.sig_txd_packet_size[4]_i_2_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \gtxd.sig_txd_packet_size[4]_i_3 
       (.I0(\gtxd.sig_txd_packet_size_reg [6]),
        .O(\gtxd.sig_txd_packet_size[4]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \gtxd.sig_txd_packet_size[4]_i_4 
       (.I0(\gtxd.sig_txd_packet_size_reg [5]),
        .O(\gtxd.sig_txd_packet_size[4]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \gtxd.sig_txd_packet_size[4]_i_5 
       (.I0(\gtxd.sig_txd_packet_size_reg [4]),
        .O(\gtxd.sig_txd_packet_size[4]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \gtxd.sig_txd_packet_size[8]_i_2 
       (.I0(\gtxd.sig_txd_packet_size_reg [11]),
        .O(\gtxd.sig_txd_packet_size[8]_i_2_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \gtxd.sig_txd_packet_size[8]_i_3 
       (.I0(\gtxd.sig_txd_packet_size_reg [10]),
        .O(\gtxd.sig_txd_packet_size[8]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \gtxd.sig_txd_packet_size[8]_i_4 
       (.I0(\gtxd.sig_txd_packet_size_reg [9]),
        .O(\gtxd.sig_txd_packet_size[8]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \gtxd.sig_txd_packet_size[8]_i_5 
       (.I0(\gtxd.sig_txd_packet_size_reg [8]),
        .O(\gtxd.sig_txd_packet_size[8]_i_5_n_0 ));
  FDRE \gtxd.sig_txd_packet_size_reg[0] 
       (.C(s_axi_aclk),
        .CE(p_1_out),
        .D(\gtxd.sig_txd_packet_size_reg[0]_i_3_n_7 ),
        .Q(\gtxd.sig_txd_packet_size_reg [0]),
        .R(\gtxd.sig_txd_packet_size[0]_i_1_n_0 ));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \gtxd.sig_txd_packet_size_reg[0]_i_3 
       (.CI(1'b0),
        .CO({\gtxd.sig_txd_packet_size_reg[0]_i_3_n_0 ,\gtxd.sig_txd_packet_size_reg[0]_i_3_n_1 ,\gtxd.sig_txd_packet_size_reg[0]_i_3_n_2 ,\gtxd.sig_txd_packet_size_reg[0]_i_3_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\gtxd.sig_txd_packet_size_reg[0]_i_3_n_4 ,\gtxd.sig_txd_packet_size_reg[0]_i_3_n_5 ,\gtxd.sig_txd_packet_size_reg[0]_i_3_n_6 ,\gtxd.sig_txd_packet_size_reg[0]_i_3_n_7 }),
        .S({\gtxd.sig_txd_packet_size[0]_i_4_n_0 ,\gtxd.sig_txd_packet_size[0]_i_5_n_0 ,\gtxd.sig_txd_packet_size[0]_i_6_n_0 ,R[0]}));
  FDRE \gtxd.sig_txd_packet_size_reg[10] 
       (.C(s_axi_aclk),
        .CE(p_1_out),
        .D(\gtxd.sig_txd_packet_size_reg[8]_i_1_n_5 ),
        .Q(\gtxd.sig_txd_packet_size_reg [10]),
        .R(\gtxd.sig_txd_packet_size[0]_i_1_n_0 ));
  FDRE \gtxd.sig_txd_packet_size_reg[11] 
       (.C(s_axi_aclk),
        .CE(p_1_out),
        .D(\gtxd.sig_txd_packet_size_reg[8]_i_1_n_4 ),
        .Q(\gtxd.sig_txd_packet_size_reg [11]),
        .R(\gtxd.sig_txd_packet_size[0]_i_1_n_0 ));
  FDRE \gtxd.sig_txd_packet_size_reg[12] 
       (.C(s_axi_aclk),
        .CE(p_1_out),
        .D(\gtxd.sig_txd_packet_size_reg[12]_i_1_n_7 ),
        .Q(\gtxd.sig_txd_packet_size_reg [12]),
        .R(\gtxd.sig_txd_packet_size[0]_i_1_n_0 ));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \gtxd.sig_txd_packet_size_reg[12]_i_1 
       (.CI(\gtxd.sig_txd_packet_size_reg[8]_i_1_n_0 ),
        .CO({\gtxd.sig_txd_packet_size_reg[12]_i_1_n_0 ,\gtxd.sig_txd_packet_size_reg[12]_i_1_n_1 ,\gtxd.sig_txd_packet_size_reg[12]_i_1_n_2 ,\gtxd.sig_txd_packet_size_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\gtxd.sig_txd_packet_size_reg[12]_i_1_n_4 ,\gtxd.sig_txd_packet_size_reg[12]_i_1_n_5 ,\gtxd.sig_txd_packet_size_reg[12]_i_1_n_6 ,\gtxd.sig_txd_packet_size_reg[12]_i_1_n_7 }),
        .S({\gtxd.sig_txd_packet_size[12]_i_2_n_0 ,\gtxd.sig_txd_packet_size[12]_i_3_n_0 ,\gtxd.sig_txd_packet_size[12]_i_4_n_0 ,\gtxd.sig_txd_packet_size[12]_i_5_n_0 }));
  FDRE \gtxd.sig_txd_packet_size_reg[13] 
       (.C(s_axi_aclk),
        .CE(p_1_out),
        .D(\gtxd.sig_txd_packet_size_reg[12]_i_1_n_6 ),
        .Q(\gtxd.sig_txd_packet_size_reg [13]),
        .R(\gtxd.sig_txd_packet_size[0]_i_1_n_0 ));
  FDRE \gtxd.sig_txd_packet_size_reg[14] 
       (.C(s_axi_aclk),
        .CE(p_1_out),
        .D(\gtxd.sig_txd_packet_size_reg[12]_i_1_n_5 ),
        .Q(\gtxd.sig_txd_packet_size_reg [14]),
        .R(\gtxd.sig_txd_packet_size[0]_i_1_n_0 ));
  FDRE \gtxd.sig_txd_packet_size_reg[15] 
       (.C(s_axi_aclk),
        .CE(p_1_out),
        .D(\gtxd.sig_txd_packet_size_reg[12]_i_1_n_4 ),
        .Q(\gtxd.sig_txd_packet_size_reg [15]),
        .R(\gtxd.sig_txd_packet_size[0]_i_1_n_0 ));
  FDRE \gtxd.sig_txd_packet_size_reg[16] 
       (.C(s_axi_aclk),
        .CE(p_1_out),
        .D(\gtxd.sig_txd_packet_size_reg[16]_i_1_n_7 ),
        .Q(\gtxd.sig_txd_packet_size_reg [16]),
        .R(\gtxd.sig_txd_packet_size[0]_i_1_n_0 ));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \gtxd.sig_txd_packet_size_reg[16]_i_1 
       (.CI(\gtxd.sig_txd_packet_size_reg[12]_i_1_n_0 ),
        .CO({\gtxd.sig_txd_packet_size_reg[16]_i_1_n_0 ,\gtxd.sig_txd_packet_size_reg[16]_i_1_n_1 ,\gtxd.sig_txd_packet_size_reg[16]_i_1_n_2 ,\gtxd.sig_txd_packet_size_reg[16]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\gtxd.sig_txd_packet_size_reg[16]_i_1_n_4 ,\gtxd.sig_txd_packet_size_reg[16]_i_1_n_5 ,\gtxd.sig_txd_packet_size_reg[16]_i_1_n_6 ,\gtxd.sig_txd_packet_size_reg[16]_i_1_n_7 }),
        .S({\gtxd.sig_txd_packet_size[16]_i_2_n_0 ,\gtxd.sig_txd_packet_size[16]_i_3_n_0 ,\gtxd.sig_txd_packet_size[16]_i_4_n_0 ,\gtxd.sig_txd_packet_size[16]_i_5_n_0 }));
  FDRE \gtxd.sig_txd_packet_size_reg[17] 
       (.C(s_axi_aclk),
        .CE(p_1_out),
        .D(\gtxd.sig_txd_packet_size_reg[16]_i_1_n_6 ),
        .Q(\gtxd.sig_txd_packet_size_reg [17]),
        .R(\gtxd.sig_txd_packet_size[0]_i_1_n_0 ));
  FDRE \gtxd.sig_txd_packet_size_reg[18] 
       (.C(s_axi_aclk),
        .CE(p_1_out),
        .D(\gtxd.sig_txd_packet_size_reg[16]_i_1_n_5 ),
        .Q(\gtxd.sig_txd_packet_size_reg [18]),
        .R(\gtxd.sig_txd_packet_size[0]_i_1_n_0 ));
  FDRE \gtxd.sig_txd_packet_size_reg[19] 
       (.C(s_axi_aclk),
        .CE(p_1_out),
        .D(\gtxd.sig_txd_packet_size_reg[16]_i_1_n_4 ),
        .Q(\gtxd.sig_txd_packet_size_reg [19]),
        .R(\gtxd.sig_txd_packet_size[0]_i_1_n_0 ));
  FDRE \gtxd.sig_txd_packet_size_reg[1] 
       (.C(s_axi_aclk),
        .CE(p_1_out),
        .D(\gtxd.sig_txd_packet_size_reg[0]_i_3_n_6 ),
        .Q(\gtxd.sig_txd_packet_size_reg [1]),
        .R(\gtxd.sig_txd_packet_size[0]_i_1_n_0 ));
  FDRE \gtxd.sig_txd_packet_size_reg[20] 
       (.C(s_axi_aclk),
        .CE(p_1_out),
        .D(\gtxd.sig_txd_packet_size_reg[20]_i_1_n_7 ),
        .Q(\gtxd.sig_txd_packet_size_reg [20]),
        .R(\gtxd.sig_txd_packet_size[0]_i_1_n_0 ));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \gtxd.sig_txd_packet_size_reg[20]_i_1 
       (.CI(\gtxd.sig_txd_packet_size_reg[16]_i_1_n_0 ),
        .CO({\gtxd.sig_txd_packet_size_reg[20]_i_1_n_0 ,\gtxd.sig_txd_packet_size_reg[20]_i_1_n_1 ,\gtxd.sig_txd_packet_size_reg[20]_i_1_n_2 ,\gtxd.sig_txd_packet_size_reg[20]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\gtxd.sig_txd_packet_size_reg[20]_i_1_n_4 ,\gtxd.sig_txd_packet_size_reg[20]_i_1_n_5 ,\gtxd.sig_txd_packet_size_reg[20]_i_1_n_6 ,\gtxd.sig_txd_packet_size_reg[20]_i_1_n_7 }),
        .S({\gtxd.sig_txd_packet_size[20]_i_2_n_0 ,\gtxd.sig_txd_packet_size[20]_i_3_n_0 ,\gtxd.sig_txd_packet_size[20]_i_4_n_0 ,\gtxd.sig_txd_packet_size[20]_i_5_n_0 }));
  FDRE \gtxd.sig_txd_packet_size_reg[21] 
       (.C(s_axi_aclk),
        .CE(p_1_out),
        .D(\gtxd.sig_txd_packet_size_reg[20]_i_1_n_6 ),
        .Q(\gtxd.sig_txd_packet_size_reg [21]),
        .R(\gtxd.sig_txd_packet_size[0]_i_1_n_0 ));
  FDRE \gtxd.sig_txd_packet_size_reg[22] 
       (.C(s_axi_aclk),
        .CE(p_1_out),
        .D(\gtxd.sig_txd_packet_size_reg[20]_i_1_n_5 ),
        .Q(\gtxd.sig_txd_packet_size_reg [22]),
        .R(\gtxd.sig_txd_packet_size[0]_i_1_n_0 ));
  FDRE \gtxd.sig_txd_packet_size_reg[23] 
       (.C(s_axi_aclk),
        .CE(p_1_out),
        .D(\gtxd.sig_txd_packet_size_reg[20]_i_1_n_4 ),
        .Q(\gtxd.sig_txd_packet_size_reg [23]),
        .R(\gtxd.sig_txd_packet_size[0]_i_1_n_0 ));
  FDRE \gtxd.sig_txd_packet_size_reg[24] 
       (.C(s_axi_aclk),
        .CE(p_1_out),
        .D(\gtxd.sig_txd_packet_size_reg[24]_i_1_n_7 ),
        .Q(\gtxd.sig_txd_packet_size_reg [24]),
        .R(\gtxd.sig_txd_packet_size[0]_i_1_n_0 ));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \gtxd.sig_txd_packet_size_reg[24]_i_1 
       (.CI(\gtxd.sig_txd_packet_size_reg[20]_i_1_n_0 ),
        .CO({\gtxd.sig_txd_packet_size_reg[24]_i_1_n_0 ,\gtxd.sig_txd_packet_size_reg[24]_i_1_n_1 ,\gtxd.sig_txd_packet_size_reg[24]_i_1_n_2 ,\gtxd.sig_txd_packet_size_reg[24]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\gtxd.sig_txd_packet_size_reg[24]_i_1_n_4 ,\gtxd.sig_txd_packet_size_reg[24]_i_1_n_5 ,\gtxd.sig_txd_packet_size_reg[24]_i_1_n_6 ,\gtxd.sig_txd_packet_size_reg[24]_i_1_n_7 }),
        .S({\gtxd.sig_txd_packet_size[24]_i_2_n_0 ,\gtxd.sig_txd_packet_size[24]_i_3_n_0 ,\gtxd.sig_txd_packet_size[24]_i_4_n_0 ,\gtxd.sig_txd_packet_size[24]_i_5_n_0 }));
  FDRE \gtxd.sig_txd_packet_size_reg[25] 
       (.C(s_axi_aclk),
        .CE(p_1_out),
        .D(\gtxd.sig_txd_packet_size_reg[24]_i_1_n_6 ),
        .Q(\gtxd.sig_txd_packet_size_reg [25]),
        .R(\gtxd.sig_txd_packet_size[0]_i_1_n_0 ));
  FDRE \gtxd.sig_txd_packet_size_reg[26] 
       (.C(s_axi_aclk),
        .CE(p_1_out),
        .D(\gtxd.sig_txd_packet_size_reg[24]_i_1_n_5 ),
        .Q(\gtxd.sig_txd_packet_size_reg [26]),
        .R(\gtxd.sig_txd_packet_size[0]_i_1_n_0 ));
  FDRE \gtxd.sig_txd_packet_size_reg[27] 
       (.C(s_axi_aclk),
        .CE(p_1_out),
        .D(\gtxd.sig_txd_packet_size_reg[24]_i_1_n_4 ),
        .Q(\gtxd.sig_txd_packet_size_reg [27]),
        .R(\gtxd.sig_txd_packet_size[0]_i_1_n_0 ));
  FDRE \gtxd.sig_txd_packet_size_reg[28] 
       (.C(s_axi_aclk),
        .CE(p_1_out),
        .D(\gtxd.sig_txd_packet_size_reg[28]_i_1_n_7 ),
        .Q(\gtxd.sig_txd_packet_size_reg [28]),
        .R(\gtxd.sig_txd_packet_size[0]_i_1_n_0 ));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \gtxd.sig_txd_packet_size_reg[28]_i_1 
       (.CI(\gtxd.sig_txd_packet_size_reg[24]_i_1_n_0 ),
        .CO({\NLW_gtxd.sig_txd_packet_size_reg[28]_i_1_CO_UNCONNECTED [3:2],\gtxd.sig_txd_packet_size_reg[28]_i_1_n_2 ,\gtxd.sig_txd_packet_size_reg[28]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_gtxd.sig_txd_packet_size_reg[28]_i_1_O_UNCONNECTED [3],\gtxd.sig_txd_packet_size_reg[28]_i_1_n_5 ,\gtxd.sig_txd_packet_size_reg[28]_i_1_n_6 ,\gtxd.sig_txd_packet_size_reg[28]_i_1_n_7 }),
        .S({1'b0,\gtxd.sig_txd_packet_size[28]_i_2_n_0 ,\gtxd.sig_txd_packet_size[28]_i_3_n_0 ,\gtxd.sig_txd_packet_size[28]_i_4_n_0 }));
  FDRE \gtxd.sig_txd_packet_size_reg[29] 
       (.C(s_axi_aclk),
        .CE(p_1_out),
        .D(\gtxd.sig_txd_packet_size_reg[28]_i_1_n_6 ),
        .Q(\gtxd.sig_txd_packet_size_reg [29]),
        .R(\gtxd.sig_txd_packet_size[0]_i_1_n_0 ));
  FDRE \gtxd.sig_txd_packet_size_reg[2] 
       (.C(s_axi_aclk),
        .CE(p_1_out),
        .D(\gtxd.sig_txd_packet_size_reg[0]_i_3_n_5 ),
        .Q(\gtxd.sig_txd_packet_size_reg [2]),
        .R(\gtxd.sig_txd_packet_size[0]_i_1_n_0 ));
  FDRE \gtxd.sig_txd_packet_size_reg[30] 
       (.C(s_axi_aclk),
        .CE(p_1_out),
        .D(\gtxd.sig_txd_packet_size_reg[28]_i_1_n_5 ),
        .Q(\gtxd.sig_txd_packet_size_reg [30]),
        .R(\gtxd.sig_txd_packet_size[0]_i_1_n_0 ));
  FDRE \gtxd.sig_txd_packet_size_reg[3] 
       (.C(s_axi_aclk),
        .CE(p_1_out),
        .D(\gtxd.sig_txd_packet_size_reg[0]_i_3_n_4 ),
        .Q(\gtxd.sig_txd_packet_size_reg [3]),
        .R(\gtxd.sig_txd_packet_size[0]_i_1_n_0 ));
  FDRE \gtxd.sig_txd_packet_size_reg[4] 
       (.C(s_axi_aclk),
        .CE(p_1_out),
        .D(\gtxd.sig_txd_packet_size_reg[4]_i_1_n_7 ),
        .Q(\gtxd.sig_txd_packet_size_reg [4]),
        .R(\gtxd.sig_txd_packet_size[0]_i_1_n_0 ));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \gtxd.sig_txd_packet_size_reg[4]_i_1 
       (.CI(\gtxd.sig_txd_packet_size_reg[0]_i_3_n_0 ),
        .CO({\gtxd.sig_txd_packet_size_reg[4]_i_1_n_0 ,\gtxd.sig_txd_packet_size_reg[4]_i_1_n_1 ,\gtxd.sig_txd_packet_size_reg[4]_i_1_n_2 ,\gtxd.sig_txd_packet_size_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\gtxd.sig_txd_packet_size_reg[4]_i_1_n_4 ,\gtxd.sig_txd_packet_size_reg[4]_i_1_n_5 ,\gtxd.sig_txd_packet_size_reg[4]_i_1_n_6 ,\gtxd.sig_txd_packet_size_reg[4]_i_1_n_7 }),
        .S({\gtxd.sig_txd_packet_size[4]_i_2_n_0 ,\gtxd.sig_txd_packet_size[4]_i_3_n_0 ,\gtxd.sig_txd_packet_size[4]_i_4_n_0 ,\gtxd.sig_txd_packet_size[4]_i_5_n_0 }));
  FDRE \gtxd.sig_txd_packet_size_reg[5] 
       (.C(s_axi_aclk),
        .CE(p_1_out),
        .D(\gtxd.sig_txd_packet_size_reg[4]_i_1_n_6 ),
        .Q(\gtxd.sig_txd_packet_size_reg [5]),
        .R(\gtxd.sig_txd_packet_size[0]_i_1_n_0 ));
  FDRE \gtxd.sig_txd_packet_size_reg[6] 
       (.C(s_axi_aclk),
        .CE(p_1_out),
        .D(\gtxd.sig_txd_packet_size_reg[4]_i_1_n_5 ),
        .Q(\gtxd.sig_txd_packet_size_reg [6]),
        .R(\gtxd.sig_txd_packet_size[0]_i_1_n_0 ));
  FDRE \gtxd.sig_txd_packet_size_reg[7] 
       (.C(s_axi_aclk),
        .CE(p_1_out),
        .D(\gtxd.sig_txd_packet_size_reg[4]_i_1_n_4 ),
        .Q(\gtxd.sig_txd_packet_size_reg [7]),
        .R(\gtxd.sig_txd_packet_size[0]_i_1_n_0 ));
  FDRE \gtxd.sig_txd_packet_size_reg[8] 
       (.C(s_axi_aclk),
        .CE(p_1_out),
        .D(\gtxd.sig_txd_packet_size_reg[8]_i_1_n_7 ),
        .Q(\gtxd.sig_txd_packet_size_reg [8]),
        .R(\gtxd.sig_txd_packet_size[0]_i_1_n_0 ));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \gtxd.sig_txd_packet_size_reg[8]_i_1 
       (.CI(\gtxd.sig_txd_packet_size_reg[4]_i_1_n_0 ),
        .CO({\gtxd.sig_txd_packet_size_reg[8]_i_1_n_0 ,\gtxd.sig_txd_packet_size_reg[8]_i_1_n_1 ,\gtxd.sig_txd_packet_size_reg[8]_i_1_n_2 ,\gtxd.sig_txd_packet_size_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\gtxd.sig_txd_packet_size_reg[8]_i_1_n_4 ,\gtxd.sig_txd_packet_size_reg[8]_i_1_n_5 ,\gtxd.sig_txd_packet_size_reg[8]_i_1_n_6 ,\gtxd.sig_txd_packet_size_reg[8]_i_1_n_7 }),
        .S({\gtxd.sig_txd_packet_size[8]_i_2_n_0 ,\gtxd.sig_txd_packet_size[8]_i_3_n_0 ,\gtxd.sig_txd_packet_size[8]_i_4_n_0 ,\gtxd.sig_txd_packet_size[8]_i_5_n_0 }));
  FDRE \gtxd.sig_txd_packet_size_reg[9] 
       (.C(s_axi_aclk),
        .CE(p_1_out),
        .D(\gtxd.sig_txd_packet_size_reg[8]_i_1_n_6 ),
        .Q(\gtxd.sig_txd_packet_size_reg [9]),
        .R(\gtxd.sig_txd_packet_size[0]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b0)) 
    \gtxd.sig_txd_prog_empty_d1_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(sig_txd_prog_empty),
        .Q(sig_txd_prog_empty_d1),
        .S(SR));
  FDRE #(
    .INIT(1'b0)) 
    \gtxd.sig_txd_prog_full_d1_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(p_7_out),
        .Q(sig_txd_prog_full_d1),
        .R(SR));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    i__carry__0_i_1
       (.I0(s_axi_wdata[24]),
        .I1(\gtxd.sig_txd_packet_size_reg [22]),
        .I2(s_axi_wdata[23]),
        .I3(\gtxd.sig_txd_packet_size_reg [21]),
        .I4(\gtxd.sig_txd_packet_size_reg [23]),
        .I5(s_axi_wdata[25]),
        .O(i__carry__0_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry__0_i_10
       (.I0(\gtxd.sig_txd_packet_size_reg [22]),
        .O(i__carry__0_i_10_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry__0_i_11
       (.I0(\gtxd.sig_txd_packet_size_reg [21]),
        .O(i__carry__0_i_11_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry__0_i_12
       (.I0(\gtxd.sig_txd_packet_size_reg [20]),
        .O(i__carry__0_i_12_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry__0_i_13
       (.I0(\gtxd.sig_txd_packet_size_reg [19]),
        .O(i__carry__0_i_13_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry__0_i_14
       (.I0(\gtxd.sig_txd_packet_size_reg [18]),
        .O(i__carry__0_i_14_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry__0_i_15
       (.I0(\gtxd.sig_txd_packet_size_reg [17]),
        .O(i__carry__0_i_15_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry__0_i_16
       (.I0(\gtxd.sig_txd_packet_size_reg [16]),
        .O(i__carry__0_i_16_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry__0_i_17
       (.I0(\gtxd.sig_txd_packet_size_reg [15]),
        .O(i__carry__0_i_17_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry__0_i_18
       (.I0(\gtxd.sig_txd_packet_size_reg [14]),
        .O(i__carry__0_i_18_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry__0_i_19
       (.I0(\gtxd.sig_txd_packet_size_reg [13]),
        .O(i__carry__0_i_19_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    i__carry__0_i_1__0
       (.I0(s_axi_wdata[24]),
        .I1(R[22]),
        .I2(s_axi_wdata[23]),
        .I3(R[21]),
        .I4(s_axi_wdata[25]),
        .I5(R[23]),
        .O(i__carry__0_i_1__0_n_0));
  LUT1 #(
    .INIT(2'h2)) 
    i__carry__0_i_1__1
       (.I0(fg_rxd_wr_length[9]),
        .O(i__carry__0_i_1__1_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    i__carry__0_i_2
       (.I0(s_axi_wdata[21]),
        .I1(\gtxd.sig_txd_packet_size_reg [19]),
        .I2(s_axi_wdata[20]),
        .I3(\gtxd.sig_txd_packet_size_reg [18]),
        .I4(\gtxd.sig_txd_packet_size_reg [20]),
        .I5(s_axi_wdata[22]),
        .O(i__carry__0_i_2_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    i__carry__0_i_2__0
       (.I0(s_axi_wdata[21]),
        .I1(R[19]),
        .I2(s_axi_wdata[20]),
        .I3(R[18]),
        .I4(s_axi_wdata[22]),
        .I5(R[20]),
        .O(i__carry__0_i_2__0_n_0));
  LUT1 #(
    .INIT(2'h2)) 
    i__carry__0_i_2__1
       (.I0(fg_rxd_wr_length[8]),
        .O(i__carry__0_i_2__1_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    i__carry__0_i_3
       (.I0(s_axi_wdata[18]),
        .I1(\gtxd.sig_txd_packet_size_reg [16]),
        .I2(s_axi_wdata[17]),
        .I3(\gtxd.sig_txd_packet_size_reg [15]),
        .I4(\gtxd.sig_txd_packet_size_reg [17]),
        .I5(s_axi_wdata[19]),
        .O(i__carry__0_i_3_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    i__carry__0_i_3__0
       (.I0(s_axi_wdata[18]),
        .I1(R[16]),
        .I2(s_axi_wdata[17]),
        .I3(R[15]),
        .I4(s_axi_wdata[19]),
        .I5(R[17]),
        .O(i__carry__0_i_3__0_n_0));
  LUT1 #(
    .INIT(2'h2)) 
    i__carry__0_i_3__1
       (.I0(fg_rxd_wr_length[7]),
        .O(i__carry__0_i_3__1_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    i__carry__0_i_4
       (.I0(s_axi_wdata[15]),
        .I1(\gtxd.sig_txd_packet_size_reg [13]),
        .I2(s_axi_wdata[14]),
        .I3(\gtxd.sig_txd_packet_size_reg [12]),
        .I4(\gtxd.sig_txd_packet_size_reg [14]),
        .I5(s_axi_wdata[16]),
        .O(i__carry__0_i_4_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    i__carry__0_i_4__0
       (.I0(s_axi_wdata[15]),
        .I1(R[13]),
        .I2(s_axi_wdata[14]),
        .I3(R[12]),
        .I4(s_axi_wdata[16]),
        .I5(R[14]),
        .O(i__carry__0_i_4__0_n_0));
  LUT1 #(
    .INIT(2'h2)) 
    i__carry__0_i_4__1
       (.I0(fg_rxd_wr_length[6]),
        .O(i__carry__0_i_4__1_n_0));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 i__carry__0_i_5
       (.CI(i__carry__0_i_6_n_0),
        .CO({i__carry__0_i_5_n_0,i__carry__0_i_5_n_1,i__carry__0_i_5_n_2,i__carry__0_i_5_n_3}),
        .CYINIT(1'b0),
        .DI(\gtxd.sig_txd_packet_size_reg [24:21]),
        .O(R[24:21]),
        .S({i__carry__0_i_8_n_0,i__carry__0_i_9_n_0,i__carry__0_i_10_n_0,i__carry__0_i_11_n_0}));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 i__carry__0_i_6
       (.CI(i__carry__0_i_7_n_0),
        .CO({i__carry__0_i_6_n_0,i__carry__0_i_6_n_1,i__carry__0_i_6_n_2,i__carry__0_i_6_n_3}),
        .CYINIT(1'b0),
        .DI(\gtxd.sig_txd_packet_size_reg [20:17]),
        .O(R[20:17]),
        .S({i__carry__0_i_12_n_0,i__carry__0_i_13_n_0,i__carry__0_i_14_n_0,i__carry__0_i_15_n_0}));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 i__carry__0_i_7
       (.CI(i__carry_i_5_n_0),
        .CO({i__carry__0_i_7_n_0,i__carry__0_i_7_n_1,i__carry__0_i_7_n_2,i__carry__0_i_7_n_3}),
        .CYINIT(1'b0),
        .DI(\gtxd.sig_txd_packet_size_reg [16:13]),
        .O(R[16:13]),
        .S({i__carry__0_i_16_n_0,i__carry__0_i_17_n_0,i__carry__0_i_18_n_0,i__carry__0_i_19_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry__0_i_8
       (.I0(\gtxd.sig_txd_packet_size_reg [24]),
        .O(i__carry__0_i_8_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry__0_i_9
       (.I0(\gtxd.sig_txd_packet_size_reg [23]),
        .O(i__carry__0_i_9_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry__1_i_1
       (.I0(R[30]),
        .O(i__carry__1_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry__1_i_10
       (.I0(\gtxd.sig_txd_packet_size_reg [26]),
        .O(i__carry__1_i_10_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry__1_i_11
       (.I0(\gtxd.sig_txd_packet_size_reg [25]),
        .O(i__carry__1_i_11_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry__1_i_1__0
       (.I0(\gtxd.sig_txd_packet_size_reg [30]),
        .O(i__carry__1_i_1__0_n_0));
  LUT1 #(
    .INIT(2'h2)) 
    i__carry__1_i_1__1
       (.I0(fg_rxd_wr_length[13]),
        .O(i__carry__1_i_1__1_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    i__carry__1_i_2
       (.I0(s_axi_wdata[30]),
        .I1(\gtxd.sig_txd_packet_size_reg [28]),
        .I2(s_axi_wdata[29]),
        .I3(\gtxd.sig_txd_packet_size_reg [27]),
        .I4(\gtxd.sig_txd_packet_size_reg [29]),
        .I5(s_axi_wdata[31]),
        .O(i__carry__1_i_2_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    i__carry__1_i_2__0
       (.I0(s_axi_wdata[30]),
        .I1(R[28]),
        .I2(s_axi_wdata[29]),
        .I3(R[27]),
        .I4(s_axi_wdata[31]),
        .I5(R[29]),
        .O(i__carry__1_i_2__0_n_0));
  LUT1 #(
    .INIT(2'h2)) 
    i__carry__1_i_2__1
       (.I0(fg_rxd_wr_length[12]),
        .O(i__carry__1_i_2__1_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    i__carry__1_i_3
       (.I0(s_axi_wdata[27]),
        .I1(\gtxd.sig_txd_packet_size_reg [25]),
        .I2(s_axi_wdata[26]),
        .I3(\gtxd.sig_txd_packet_size_reg [24]),
        .I4(\gtxd.sig_txd_packet_size_reg [26]),
        .I5(s_axi_wdata[28]),
        .O(i__carry__1_i_3_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    i__carry__1_i_3__0
       (.I0(s_axi_wdata[27]),
        .I1(R[25]),
        .I2(s_axi_wdata[26]),
        .I3(R[24]),
        .I4(s_axi_wdata[28]),
        .I5(R[26]),
        .O(i__carry__1_i_3__0_n_0));
  LUT1 #(
    .INIT(2'h2)) 
    i__carry__1_i_3__1
       (.I0(fg_rxd_wr_length[11]),
        .O(i__carry__1_i_3__1_n_0));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 i__carry__1_i_4
       (.CI(i__carry__1_i_5_n_0),
        .CO({NLW_i__carry__1_i_4_CO_UNCONNECTED[3:1],i__carry__1_i_4_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,\gtxd.sig_txd_packet_size_reg [29]}),
        .O({NLW_i__carry__1_i_4_O_UNCONNECTED[3:2],R[30:29]}),
        .S({1'b0,1'b0,i__carry__1_i_6_n_0,i__carry__1_i_7_n_0}));
  LUT1 #(
    .INIT(2'h2)) 
    i__carry__1_i_4__0
       (.I0(fg_rxd_wr_length[10]),
        .O(i__carry__1_i_4__0_n_0));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 i__carry__1_i_5
       (.CI(i__carry__0_i_5_n_0),
        .CO({i__carry__1_i_5_n_0,i__carry__1_i_5_n_1,i__carry__1_i_5_n_2,i__carry__1_i_5_n_3}),
        .CYINIT(1'b0),
        .DI(\gtxd.sig_txd_packet_size_reg [28:25]),
        .O(R[28:25]),
        .S({i__carry__1_i_8_n_0,i__carry__1_i_9_n_0,i__carry__1_i_10_n_0,i__carry__1_i_11_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry__1_i_6
       (.I0(\gtxd.sig_txd_packet_size_reg [30]),
        .O(i__carry__1_i_6_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry__1_i_7
       (.I0(\gtxd.sig_txd_packet_size_reg [29]),
        .O(i__carry__1_i_7_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry__1_i_8
       (.I0(\gtxd.sig_txd_packet_size_reg [28]),
        .O(i__carry__1_i_8_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry__1_i_9
       (.I0(\gtxd.sig_txd_packet_size_reg [27]),
        .O(i__carry__1_i_9_n_0));
  LUT1 #(
    .INIT(2'h2)) 
    i__carry__2_i_1
       (.I0(fg_rxd_wr_length[17]),
        .O(i__carry__2_i_1_n_0));
  LUT1 #(
    .INIT(2'h2)) 
    i__carry__2_i_2
       (.I0(fg_rxd_wr_length[16]),
        .O(i__carry__2_i_2_n_0));
  LUT1 #(
    .INIT(2'h2)) 
    i__carry__2_i_3
       (.I0(fg_rxd_wr_length[15]),
        .O(i__carry__2_i_3_n_0));
  LUT1 #(
    .INIT(2'h2)) 
    i__carry__2_i_4
       (.I0(fg_rxd_wr_length[14]),
        .O(i__carry__2_i_4_n_0));
  LUT1 #(
    .INIT(2'h2)) 
    i__carry__3_i_1
       (.I0(fg_rxd_wr_length[21]),
        .O(i__carry__3_i_1_n_0));
  LUT1 #(
    .INIT(2'h2)) 
    i__carry__3_i_2
       (.I0(fg_rxd_wr_length[20]),
        .O(i__carry__3_i_2_n_0));
  LUT1 #(
    .INIT(2'h2)) 
    i__carry__3_i_3
       (.I0(fg_rxd_wr_length[19]),
        .O(i__carry__3_i_3_n_0));
  LUT1 #(
    .INIT(2'h2)) 
    i__carry__3_i_4
       (.I0(fg_rxd_wr_length[18]),
        .O(i__carry__3_i_4_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    i__carry_i_1
       (.I0(s_axi_wdata[12]),
        .I1(\gtxd.sig_txd_packet_size_reg [10]),
        .I2(s_axi_wdata[11]),
        .I3(\gtxd.sig_txd_packet_size_reg [9]),
        .I4(\gtxd.sig_txd_packet_size_reg [11]),
        .I5(s_axi_wdata[13]),
        .O(i__carry_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry_i_10
       (.I0(\gtxd.sig_txd_packet_size_reg [10]),
        .O(i__carry_i_10_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry_i_11
       (.I0(\gtxd.sig_txd_packet_size_reg [9]),
        .O(i__carry_i_11_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry_i_12
       (.I0(\gtxd.sig_txd_packet_size_reg [8]),
        .O(i__carry_i_12_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry_i_13
       (.I0(\gtxd.sig_txd_packet_size_reg [7]),
        .O(i__carry_i_13_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry_i_14
       (.I0(\gtxd.sig_txd_packet_size_reg [6]),
        .O(i__carry_i_14_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry_i_15
       (.I0(\gtxd.sig_txd_packet_size_reg [5]),
        .O(i__carry_i_15_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry_i_16
       (.I0(\gtxd.sig_txd_packet_size_reg [4]),
        .O(i__carry_i_16_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry_i_17
       (.I0(\gtxd.sig_txd_packet_size_reg [3]),
        .O(i__carry_i_17_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry_i_18
       (.I0(\gtxd.sig_txd_packet_size_reg [2]),
        .O(i__carry_i_18_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry_i_19
       (.I0(\gtxd.sig_txd_packet_size_reg [1]),
        .O(i__carry_i_19_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    i__carry_i_1__0
       (.I0(s_axi_wdata[12]),
        .I1(R[10]),
        .I2(s_axi_wdata[11]),
        .I3(R[9]),
        .I4(s_axi_wdata[13]),
        .I5(R[11]),
        .O(i__carry_i_1__0_n_0));
  LUT1 #(
    .INIT(2'h2)) 
    i__carry_i_1__1
       (.I0(fg_rxd_wr_length[5]),
        .O(i__carry_i_1__1_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    i__carry_i_2
       (.I0(s_axi_wdata[9]),
        .I1(\gtxd.sig_txd_packet_size_reg [7]),
        .I2(s_axi_wdata[8]),
        .I3(\gtxd.sig_txd_packet_size_reg [6]),
        .I4(\gtxd.sig_txd_packet_size_reg [8]),
        .I5(s_axi_wdata[10]),
        .O(i__carry_i_2_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    i__carry_i_2__0
       (.I0(s_axi_wdata[9]),
        .I1(R[7]),
        .I2(s_axi_wdata[8]),
        .I3(R[6]),
        .I4(s_axi_wdata[10]),
        .I5(R[8]),
        .O(i__carry_i_2__0_n_0));
  LUT1 #(
    .INIT(2'h2)) 
    i__carry_i_2__1
       (.I0(fg_rxd_wr_length[4]),
        .O(i__carry_i_2__1_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    i__carry_i_3
       (.I0(s_axi_wdata[6]),
        .I1(\gtxd.sig_txd_packet_size_reg [4]),
        .I2(s_axi_wdata[5]),
        .I3(\gtxd.sig_txd_packet_size_reg [3]),
        .I4(\gtxd.sig_txd_packet_size_reg [5]),
        .I5(s_axi_wdata[7]),
        .O(i__carry_i_3_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    i__carry_i_3__0
       (.I0(s_axi_wdata[6]),
        .I1(R[4]),
        .I2(s_axi_wdata[5]),
        .I3(R[3]),
        .I4(s_axi_wdata[7]),
        .I5(R[5]),
        .O(i__carry_i_3__0_n_0));
  LUT1 #(
    .INIT(2'h2)) 
    i__carry_i_3__1
       (.I0(fg_rxd_wr_length[3]),
        .O(i__carry_i_3__1_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    i__carry_i_4
       (.I0(\gtxd.sig_txd_packet_size_reg [0]),
        .I1(s_axi_wdata[2]),
        .I2(s_axi_wdata[3]),
        .I3(\gtxd.sig_txd_packet_size_reg [1]),
        .I4(\gtxd.sig_txd_packet_size_reg [2]),
        .I5(s_axi_wdata[4]),
        .O(i__carry_i_4_n_0));
  LUT6 #(
    .INIT(64'h0990000000000990)) 
    i__carry_i_4__0
       (.I0(s_axi_wdata[3]),
        .I1(R[1]),
        .I2(\gtxd.sig_txd_packet_size_reg [0]),
        .I3(s_axi_wdata[2]),
        .I4(s_axi_wdata[4]),
        .I5(R[2]),
        .O(i__carry_i_4__0_n_0));
  LUT1 #(
    .INIT(2'h2)) 
    i__carry_i_4__1
       (.I0(fg_rxd_wr_length[2]),
        .O(i__carry_i_4__1_n_0));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 i__carry_i_5
       (.CI(i__carry_i_6_n_0),
        .CO({i__carry_i_5_n_0,i__carry_i_5_n_1,i__carry_i_5_n_2,i__carry_i_5_n_3}),
        .CYINIT(1'b0),
        .DI(\gtxd.sig_txd_packet_size_reg [12:9]),
        .O(R[12:9]),
        .S({i__carry_i_8_n_0,i__carry_i_9_n_0,i__carry_i_10_n_0,i__carry_i_11_n_0}));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 i__carry_i_6
       (.CI(i__carry_i_7_n_0),
        .CO({i__carry_i_6_n_0,i__carry_i_6_n_1,i__carry_i_6_n_2,i__carry_i_6_n_3}),
        .CYINIT(1'b0),
        .DI(\gtxd.sig_txd_packet_size_reg [8:5]),
        .O(R[8:5]),
        .S({i__carry_i_12_n_0,i__carry_i_13_n_0,i__carry_i_14_n_0,i__carry_i_15_n_0}));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 i__carry_i_7
       (.CI(1'b0),
        .CO({i__carry_i_7_n_0,i__carry_i_7_n_1,i__carry_i_7_n_2,i__carry_i_7_n_3}),
        .CYINIT(\gtxd.sig_txd_packet_size_reg [0]),
        .DI(\gtxd.sig_txd_packet_size_reg [4:1]),
        .O(R[4:1]),
        .S({i__carry_i_16_n_0,i__carry_i_17_n_0,i__carry_i_18_n_0,i__carry_i_19_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry_i_8
       (.I0(\gtxd.sig_txd_packet_size_reg [12]),
        .O(i__carry_i_8_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry_i_9
       (.I0(\gtxd.sig_txd_packet_size_reg [11]),
        .O(i__carry_i_9_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFEA)) 
    interrupt_INST_0
       (.I0(interrupt_INST_0_i_1_n_0),
        .I1(\sig_register_array_reg_n_0_[1][8] ),
        .I2(\sig_ip2bus_data_reg[8]_0 ),
        .I3(interrupt_INST_0_i_2_n_0),
        .I4(interrupt_INST_0_i_3_n_0),
        .O(interrupt));
  LUT5 #(
    .INIT(32'hFFEAEAEA)) 
    interrupt_INST_0_i_1
       (.I0(interrupt_INST_0_i_4_n_0),
        .I1(\sig_ip2bus_data_reg[3]_0 ),
        .I2(\sig_register_array_reg_n_0_[1][3] ),
        .I3(\sig_ip2bus_data_reg[2]_0 ),
        .I4(\sig_register_array_reg_n_0_[1][2] ),
        .O(interrupt_INST_0_i_1_n_0));
  LUT5 #(
    .INIT(32'hFFEAEAEA)) 
    interrupt_INST_0_i_2
       (.I0(interrupt_INST_0_i_5_n_0),
        .I1(\sig_register_array_reg[0][11]_0 ),
        .I2(\sig_ip2bus_data_reg[10]_0 [1]),
        .I3(\sig_register_array_reg[0][10]_0 ),
        .I4(\sig_ip2bus_data_reg[10]_0 [2]),
        .O(interrupt_INST_0_i_2_n_0));
  LUT4 #(
    .INIT(16'hF888)) 
    interrupt_INST_0_i_3
       (.I0(\sig_register_array_reg_n_0_[1][4] ),
        .I1(\sig_ip2bus_data_reg[4]_0 ),
        .I2(\sig_register_array_reg_n_0_[1][5] ),
        .I3(\sig_ip2bus_data_reg[5]_0 ),
        .O(interrupt_INST_0_i_3_n_0));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    interrupt_INST_0_i_4
       (.I0(\sig_ip2bus_data_reg[7]_0 ),
        .I1(\sig_register_array_reg_n_0_[1][7] ),
        .I2(\sig_ip2bus_data_reg[6]_0 ),
        .I3(\sig_register_array_reg_n_0_[1][6] ),
        .I4(\sig_register_array_reg_n_0_[1][1] ),
        .I5(\sig_ip2bus_data_reg[1]_0 ),
        .O(interrupt_INST_0_i_4_n_0));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    interrupt_INST_0_i_5
       (.I0(\sig_register_array_reg[0][12]_0 ),
        .I1(\sig_ip2bus_data_reg[10]_0 [0]),
        .I2(\sig_ip2bus_data_reg[0]_0 ),
        .I3(\sig_register_array_reg_n_0_[1][0] ),
        .I4(\sig_register_array_reg_n_0_[1][9] ),
        .I5(\sig_ip2bus_data_reg[9]_0 ),
        .O(interrupt_INST_0_i_5_n_0));
  (* SOFT_HLUTNM = "soft_lutpair70" *) 
  LUT2 #(
    .INIT(4'h2)) 
    mm2s_cntrl_reset_out_n_INST_0
       (.I0(s_axi_aresetn),
        .I1(sync_areset_n_reg_inv),
        .O(mm2s_cntrl_reset_out_n));
  (* SOFT_HLUTNM = "soft_lutpair70" *) 
  LUT3 #(
    .INIT(8'h04)) 
    mm2s_prmry_reset_out_n_INST_0
       (.I0(sync_areset_n_reg_inv_0),
        .I1(s_axi_aresetn),
        .I2(sync_areset_n_reg_inv),
        .O(mm2s_prmry_reset_out_n));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 p_0_out__24_carry
       (.CI(1'b0),
        .CO({p_0_out__24_carry_n_0,p_0_out__24_carry_n_1,p_0_out__24_carry_n_2,p_0_out__24_carry_n_3}),
        .CYINIT(\gtxd.COMP_TXD_FIFO_n_57 ),
        .DI({\gfifo_gen.COMP_AXIS_FG_FIFO/COMP_FIFO/inst_fifo_gen/gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg [3:2],\gfifo_gen.COMP_AXIS_FG_FIFO/COMP_FIFO/inst_fifo_gen/gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg__0 ,\gtxd.COMP_TXD_FIFO_n_66 }),
        .O({p_0_out__24_carry_n_4,p_0_out__24_carry_n_5,p_0_out__24_carry_n_6,p_0_out__24_carry_n_7}),
        .S({\gtxd.COMP_TXD_FIFO_n_72 ,\gtxd.COMP_TXD_FIFO_n_73 ,\gtxd.COMP_TXD_FIFO_n_74 ,\gtxd.COMP_TXD_FIFO_n_75 }));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 p_0_out__24_carry__0
       (.CI(p_0_out__24_carry_n_0),
        .CO({p_0_out__24_carry__0_n_0,p_0_out__24_carry__0_n_1,p_0_out__24_carry__0_n_2,p_0_out__24_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI(\gfifo_gen.COMP_AXIS_FG_FIFO/COMP_FIFO/inst_fifo_gen/gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg [7:4]),
        .O({p_0_out__24_carry__0_n_4,p_0_out__24_carry__0_n_5,p_0_out__24_carry__0_n_6,p_0_out__24_carry__0_n_7}),
        .S({\gtxd.COMP_TXD_FIFO_n_84 ,\gtxd.COMP_TXD_FIFO_n_85 ,\gtxd.COMP_TXD_FIFO_n_86 ,\gtxd.COMP_TXD_FIFO_n_87 }));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 p_0_out__24_carry__1
       (.CI(p_0_out__24_carry__0_n_0),
        .CO(NLW_p_0_out__24_carry__1_CO_UNCONNECTED[3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({NLW_p_0_out__24_carry__1_O_UNCONNECTED[3:1],p_0_out__24_carry__1_n_7}),
        .S({1'b0,1'b0,1'b0,\gtxd.COMP_TXD_FIFO_n_70 }));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 p_0_out__49_carry
       (.CI(1'b0),
        .CO({p_0_out__49_carry_n_0,p_0_out__49_carry_n_1,p_0_out__49_carry_n_2,p_0_out__49_carry_n_3}),
        .CYINIT(Q[0]),
        .DI({Q[3:1],\grxd.COMP_RX_FIFO_n_14 }),
        .O({p_0_out__49_carry_n_4,p_0_out__49_carry_n_5,p_0_out__49_carry_n_6,p_0_out__49_carry_n_7}),
        .S({\grxd.COMP_RX_FIFO_n_28 ,\grxd.COMP_RX_FIFO_n_29 ,\grxd.COMP_RX_FIFO_n_30 ,\grxd.COMP_RX_FIFO_n_31 }));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 p_0_out__49_carry__0
       (.CI(p_0_out__49_carry_n_0),
        .CO({p_0_out__49_carry__0_n_0,p_0_out__49_carry__0_n_1,p_0_out__49_carry__0_n_2,p_0_out__49_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI(Q[7:4]),
        .O({p_0_out__49_carry__0_n_4,p_0_out__49_carry__0_n_5,p_0_out__49_carry__0_n_6,p_0_out__49_carry__0_n_7}),
        .S({\grxd.COMP_RX_FIFO_n_32 ,\grxd.COMP_RX_FIFO_n_33 ,\grxd.COMP_RX_FIFO_n_34 ,\grxd.COMP_RX_FIFO_n_35 }));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 p_0_out__49_carry__1
       (.CI(p_0_out__49_carry__0_n_0),
        .CO(NLW_p_0_out__49_carry__1_CO_UNCONNECTED[3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({NLW_p_0_out__49_carry__1_O_UNCONNECTED[3:1],p_0_out__49_carry__1_n_7}),
        .S({1'b0,1'b0,1'b0,\grxd.COMP_RX_FIFO_n_27 }));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 p_0_out_carry
       (.CI(1'b0),
        .CO({p_0_out_carry_n_0,p_0_out_carry_n_1,p_0_out_carry_n_2,p_0_out_carry_n_3}),
        .CYINIT(\gfifo_gen.COMP_AXIS_FG_FIFO/COMP_FIFO/inst_fifo_gen/gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg [0]),
        .DI({\gfifo_gen.COMP_AXIS_FG_FIFO/COMP_FIFO/inst_fifo_gen/gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg [3:1],\gtxd.COMP_TXD_FIFO_n_40 }),
        .O({p_0_out_carry_n_4,p_0_out_carry_n_5,p_0_out_carry_n_6,p_0_out_carry_n_7}),
        .S({\gtxd.COMP_TXD_FIFO_n_76 ,\gtxd.COMP_TXD_FIFO_n_77 ,\gtxd.COMP_TXD_FIFO_n_78 ,\gtxd.COMP_TXD_FIFO_n_79 }));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 p_0_out_carry__0
       (.CI(p_0_out_carry_n_0),
        .CO({p_0_out_carry__0_n_0,p_0_out_carry__0_n_1,p_0_out_carry__0_n_2,p_0_out_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI(\gfifo_gen.COMP_AXIS_FG_FIFO/COMP_FIFO/inst_fifo_gen/gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg [7:4]),
        .O({p_0_out_carry__0_n_4,p_0_out_carry__0_n_5,p_0_out_carry__0_n_6,p_0_out_carry__0_n_7}),
        .S({\gtxd.COMP_TXD_FIFO_n_80 ,\gtxd.COMP_TXD_FIFO_n_81 ,\gtxd.COMP_TXD_FIFO_n_82 ,\gtxd.COMP_TXD_FIFO_n_83 }));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 p_0_out_carry__1
       (.CI(p_0_out_carry__0_n_0),
        .CO(NLW_p_0_out_carry__1_CO_UNCONNECTED[3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({NLW_p_0_out_carry__1_O_UNCONNECTED[3:1],p_0_out_carry__1_n_7}),
        .S({1'b0,1'b0,1'b0,\gtxd.COMP_TXD_FIFO_n_49 }));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \p_2_out_inferred__2/i__carry 
       (.CI(1'b0),
        .CO({\p_2_out_inferred__2/i__carry_n_0 ,\p_2_out_inferred__2/i__carry_n_1 ,\p_2_out_inferred__2/i__carry_n_2 ,\p_2_out_inferred__2/i__carry_n_3 }),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\p_2_out_inferred__2/i__carry_n_4 ,\p_2_out_inferred__2/i__carry_n_5 ,\p_2_out_inferred__2/i__carry_n_6 ,\p_2_out_inferred__2/i__carry_n_7 }),
        .S({i__carry_i_1__1_n_0,i__carry_i_2__1_n_0,i__carry_i_3__1_n_0,i__carry_i_4__1_n_0}));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \p_2_out_inferred__2/i__carry__0 
       (.CI(\p_2_out_inferred__2/i__carry_n_0 ),
        .CO({\p_2_out_inferred__2/i__carry__0_n_0 ,\p_2_out_inferred__2/i__carry__0_n_1 ,\p_2_out_inferred__2/i__carry__0_n_2 ,\p_2_out_inferred__2/i__carry__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\p_2_out_inferred__2/i__carry__0_n_4 ,\p_2_out_inferred__2/i__carry__0_n_5 ,\p_2_out_inferred__2/i__carry__0_n_6 ,\p_2_out_inferred__2/i__carry__0_n_7 }),
        .S({i__carry__0_i_1__1_n_0,i__carry__0_i_2__1_n_0,i__carry__0_i_3__1_n_0,i__carry__0_i_4__1_n_0}));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \p_2_out_inferred__2/i__carry__1 
       (.CI(\p_2_out_inferred__2/i__carry__0_n_0 ),
        .CO({\p_2_out_inferred__2/i__carry__1_n_0 ,\p_2_out_inferred__2/i__carry__1_n_1 ,\p_2_out_inferred__2/i__carry__1_n_2 ,\p_2_out_inferred__2/i__carry__1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\p_2_out_inferred__2/i__carry__1_n_4 ,\p_2_out_inferred__2/i__carry__1_n_5 ,\p_2_out_inferred__2/i__carry__1_n_6 ,\p_2_out_inferred__2/i__carry__1_n_7 }),
        .S({i__carry__1_i_1__1_n_0,i__carry__1_i_2__1_n_0,i__carry__1_i_3__1_n_0,i__carry__1_i_4__0_n_0}));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \p_2_out_inferred__2/i__carry__2 
       (.CI(\p_2_out_inferred__2/i__carry__1_n_0 ),
        .CO({\p_2_out_inferred__2/i__carry__2_n_0 ,\p_2_out_inferred__2/i__carry__2_n_1 ,\p_2_out_inferred__2/i__carry__2_n_2 ,\p_2_out_inferred__2/i__carry__2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\p_2_out_inferred__2/i__carry__2_n_4 ,\p_2_out_inferred__2/i__carry__2_n_5 ,\p_2_out_inferred__2/i__carry__2_n_6 ,\p_2_out_inferred__2/i__carry__2_n_7 }),
        .S({i__carry__2_i_1_n_0,i__carry__2_i_2_n_0,i__carry__2_i_3_n_0,i__carry__2_i_4_n_0}));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \p_2_out_inferred__2/i__carry__3 
       (.CI(\p_2_out_inferred__2/i__carry__2_n_0 ),
        .CO({\NLW_p_2_out_inferred__2/i__carry__3_CO_UNCONNECTED [3],\p_2_out_inferred__2/i__carry__3_n_1 ,\p_2_out_inferred__2/i__carry__3_n_2 ,\p_2_out_inferred__2/i__carry__3_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\p_2_out_inferred__2/i__carry__3_n_4 ,\p_2_out_inferred__2/i__carry__3_n_5 ,\p_2_out_inferred__2/i__carry__3_n_6 ,\p_2_out_inferred__2/i__carry__3_n_7 }),
        .S({i__carry__3_i_1_n_0,i__carry__3_i_2_n_0,i__carry__3_i_3_n_0,i__carry__3_i_4_n_0}));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 plusOp_carry
       (.CI(1'b0),
        .CO({plusOp_carry_n_0,plusOp_carry_n_1,plusOp_carry_n_2,plusOp_carry_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,fg_rxd_wr_length[2],1'b0}),
        .O({plusOp_carry_n_4,plusOp_carry_n_5,plusOp_carry_n_6,plusOp_carry_n_7}),
        .S({plusOp_carry_i_1__3_n_0,plusOp_carry_i_2__3_n_0,plusOp_carry_i_3_n_0,plusOp_carry_i_4__3_n_0}));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 plusOp_carry__0
       (.CI(plusOp_carry_n_0),
        .CO({plusOp_carry__0_n_0,plusOp_carry__0_n_1,plusOp_carry__0_n_2,plusOp_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({plusOp_carry__0_n_4,plusOp_carry__0_n_5,plusOp_carry__0_n_6,plusOp_carry__0_n_7}),
        .S({plusOp_carry__0_i_1__3_n_0,plusOp_carry__0_i_2__3_n_0,plusOp_carry__0_i_3__3_n_0,plusOp_carry__0_i_4__3_n_0}));
  LUT1 #(
    .INIT(2'h2)) 
    plusOp_carry__0_i_1__3
       (.I0(fg_rxd_wr_length[8]),
        .O(plusOp_carry__0_i_1__3_n_0));
  LUT1 #(
    .INIT(2'h2)) 
    plusOp_carry__0_i_2__3
       (.I0(fg_rxd_wr_length[7]),
        .O(plusOp_carry__0_i_2__3_n_0));
  LUT1 #(
    .INIT(2'h2)) 
    plusOp_carry__0_i_3__3
       (.I0(fg_rxd_wr_length[6]),
        .O(plusOp_carry__0_i_3__3_n_0));
  LUT1 #(
    .INIT(2'h2)) 
    plusOp_carry__0_i_4__3
       (.I0(fg_rxd_wr_length[5]),
        .O(plusOp_carry__0_i_4__3_n_0));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 plusOp_carry__1
       (.CI(plusOp_carry__0_n_0),
        .CO({plusOp_carry__1_n_0,plusOp_carry__1_n_1,plusOp_carry__1_n_2,plusOp_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({plusOp_carry__1_n_4,plusOp_carry__1_n_5,plusOp_carry__1_n_6,plusOp_carry__1_n_7}),
        .S({plusOp_carry__1_i_1__3_n_0,plusOp_carry__1_i_2_n_0,plusOp_carry__1_i_3_n_0,plusOp_carry__1_i_4_n_0}));
  LUT1 #(
    .INIT(2'h2)) 
    plusOp_carry__1_i_1__3
       (.I0(fg_rxd_wr_length[12]),
        .O(plusOp_carry__1_i_1__3_n_0));
  LUT1 #(
    .INIT(2'h2)) 
    plusOp_carry__1_i_2
       (.I0(fg_rxd_wr_length[11]),
        .O(plusOp_carry__1_i_2_n_0));
  LUT1 #(
    .INIT(2'h2)) 
    plusOp_carry__1_i_3
       (.I0(fg_rxd_wr_length[10]),
        .O(plusOp_carry__1_i_3_n_0));
  LUT1 #(
    .INIT(2'h2)) 
    plusOp_carry__1_i_4
       (.I0(fg_rxd_wr_length[9]),
        .O(plusOp_carry__1_i_4_n_0));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 plusOp_carry__2
       (.CI(plusOp_carry__1_n_0),
        .CO({plusOp_carry__2_n_0,plusOp_carry__2_n_1,plusOp_carry__2_n_2,plusOp_carry__2_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({plusOp_carry__2_n_4,plusOp_carry__2_n_5,plusOp_carry__2_n_6,plusOp_carry__2_n_7}),
        .S({plusOp_carry__2_i_1_n_0,plusOp_carry__2_i_2_n_0,plusOp_carry__2_i_3_n_0,plusOp_carry__2_i_4_n_0}));
  LUT1 #(
    .INIT(2'h2)) 
    plusOp_carry__2_i_1
       (.I0(fg_rxd_wr_length[16]),
        .O(plusOp_carry__2_i_1_n_0));
  LUT1 #(
    .INIT(2'h2)) 
    plusOp_carry__2_i_2
       (.I0(fg_rxd_wr_length[15]),
        .O(plusOp_carry__2_i_2_n_0));
  LUT1 #(
    .INIT(2'h2)) 
    plusOp_carry__2_i_3
       (.I0(fg_rxd_wr_length[14]),
        .O(plusOp_carry__2_i_3_n_0));
  LUT1 #(
    .INIT(2'h2)) 
    plusOp_carry__2_i_4
       (.I0(fg_rxd_wr_length[13]),
        .O(plusOp_carry__2_i_4_n_0));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 plusOp_carry__3
       (.CI(plusOp_carry__2_n_0),
        .CO({plusOp_carry__3_n_0,plusOp_carry__3_n_1,plusOp_carry__3_n_2,plusOp_carry__3_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({plusOp_carry__3_n_4,plusOp_carry__3_n_5,plusOp_carry__3_n_6,plusOp_carry__3_n_7}),
        .S({plusOp_carry__3_i_1_n_0,plusOp_carry__3_i_2_n_0,plusOp_carry__3_i_3_n_0,plusOp_carry__3_i_4_n_0}));
  LUT1 #(
    .INIT(2'h2)) 
    plusOp_carry__3_i_1
       (.I0(fg_rxd_wr_length[20]),
        .O(plusOp_carry__3_i_1_n_0));
  LUT1 #(
    .INIT(2'h2)) 
    plusOp_carry__3_i_2
       (.I0(fg_rxd_wr_length[19]),
        .O(plusOp_carry__3_i_2_n_0));
  LUT1 #(
    .INIT(2'h2)) 
    plusOp_carry__3_i_3
       (.I0(fg_rxd_wr_length[18]),
        .O(plusOp_carry__3_i_3_n_0));
  LUT1 #(
    .INIT(2'h2)) 
    plusOp_carry__3_i_4
       (.I0(fg_rxd_wr_length[17]),
        .O(plusOp_carry__3_i_4_n_0));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 plusOp_carry__4
       (.CI(plusOp_carry__3_n_0),
        .CO(NLW_plusOp_carry__4_CO_UNCONNECTED[3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({NLW_plusOp_carry__4_O_UNCONNECTED[3:1],plusOp_carry__4_n_7}),
        .S({1'b0,1'b0,1'b0,plusOp_carry__4_i_1_n_0}));
  LUT1 #(
    .INIT(2'h2)) 
    plusOp_carry__4_i_1
       (.I0(fg_rxd_wr_length[21]),
        .O(plusOp_carry__4_i_1_n_0));
  LUT1 #(
    .INIT(2'h2)) 
    plusOp_carry_i_1__3
       (.I0(fg_rxd_wr_length[4]),
        .O(plusOp_carry_i_1__3_n_0));
  LUT1 #(
    .INIT(2'h2)) 
    plusOp_carry_i_2__3
       (.I0(fg_rxd_wr_length[3]),
        .O(plusOp_carry_i_2__3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    plusOp_carry_i_3
       (.I0(fg_rxd_wr_length[2]),
        .O(plusOp_carry_i_3_n_0));
  LUT1 #(
    .INIT(2'h2)) 
    plusOp_carry_i_4__3
       (.I0(fg_rxd_wr_length[1]),
        .O(plusOp_carry_i_4__3_n_0));
  (* SOFT_HLUTNM = "soft_lutpair69" *) 
  LUT3 #(
    .INIT(8'h04)) 
    s2mm_prmry_reset_out_n_INST_0
       (.I0(\gpr1.dout_i_reg[0] ),
        .I1(s_axi_aresetn),
        .I2(sync_areset_n_reg_inv),
        .O(s2mm_prmry_reset_out_n));
  LUT4 #(
    .INIT(16'hF888)) 
    \sig_ip2bus_data[0]_i_2 
       (.I0(\sig_ip2bus_data_reg[0]_0 ),
        .I1(\GEN_BKEND_CE_REGISTERS[1].ce_out_i_reg[1] ),
        .I2(\sig_register_array_reg_n_0_[1][0] ),
        .I3(\GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg[0] ),
        .O(sig_ip2bus_data[0]));
  LUT4 #(
    .INIT(16'hF888)) 
    \sig_ip2bus_data[1]_i_1 
       (.I0(\sig_ip2bus_data_reg[1]_0 ),
        .I1(\GEN_BKEND_CE_REGISTERS[1].ce_out_i_reg[1] ),
        .I2(\sig_register_array_reg_n_0_[1][1] ),
        .I3(\GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg[0] ),
        .O(sig_ip2bus_data[1]));
  LUT4 #(
    .INIT(16'hF888)) 
    \sig_ip2bus_data[2]_i_1 
       (.I0(\sig_ip2bus_data_reg[2]_0 ),
        .I1(\GEN_BKEND_CE_REGISTERS[1].ce_out_i_reg[1] ),
        .I2(\sig_register_array_reg_n_0_[1][2] ),
        .I3(\GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg[0] ),
        .O(sig_ip2bus_data[2]));
  LUT4 #(
    .INIT(16'hF888)) 
    \sig_ip2bus_data[3]_i_1 
       (.I0(\sig_ip2bus_data_reg[3]_0 ),
        .I1(\GEN_BKEND_CE_REGISTERS[1].ce_out_i_reg[1] ),
        .I2(\sig_register_array_reg_n_0_[1][3] ),
        .I3(\GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg[0] ),
        .O(sig_ip2bus_data[3]));
  LUT4 #(
    .INIT(16'hF888)) 
    \sig_ip2bus_data[4]_i_1 
       (.I0(\sig_ip2bus_data_reg[4]_0 ),
        .I1(\GEN_BKEND_CE_REGISTERS[1].ce_out_i_reg[1] ),
        .I2(\sig_register_array_reg_n_0_[1][4] ),
        .I3(\GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg[0] ),
        .O(sig_ip2bus_data[4]));
  LUT4 #(
    .INIT(16'hF888)) 
    \sig_ip2bus_data[5]_i_1 
       (.I0(\sig_ip2bus_data_reg[5]_0 ),
        .I1(\GEN_BKEND_CE_REGISTERS[1].ce_out_i_reg[1] ),
        .I2(\sig_register_array_reg_n_0_[1][5] ),
        .I3(\GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg[0] ),
        .O(sig_ip2bus_data[5]));
  LUT4 #(
    .INIT(16'hF888)) 
    \sig_ip2bus_data[6]_i_1 
       (.I0(\sig_ip2bus_data_reg[6]_0 ),
        .I1(\GEN_BKEND_CE_REGISTERS[1].ce_out_i_reg[1] ),
        .I2(\sig_register_array_reg_n_0_[1][6] ),
        .I3(\GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg[0] ),
        .O(sig_ip2bus_data[6]));
  LUT4 #(
    .INIT(16'hF888)) 
    \sig_ip2bus_data[7]_i_1 
       (.I0(\sig_ip2bus_data_reg[7]_0 ),
        .I1(\GEN_BKEND_CE_REGISTERS[1].ce_out_i_reg[1] ),
        .I2(\sig_register_array_reg_n_0_[1][7] ),
        .I3(\GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg[0] ),
        .O(sig_ip2bus_data[7]));
  LUT4 #(
    .INIT(16'hF888)) 
    \sig_ip2bus_data[8]_i_1 
       (.I0(\sig_ip2bus_data_reg[8]_0 ),
        .I1(\GEN_BKEND_CE_REGISTERS[1].ce_out_i_reg[1] ),
        .I2(\sig_register_array_reg_n_0_[1][8] ),
        .I3(\GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg[0] ),
        .O(sig_ip2bus_data[8]));
  LUT4 #(
    .INIT(16'hF888)) 
    \sig_ip2bus_data[9]_i_1 
       (.I0(\sig_ip2bus_data_reg[9]_0 ),
        .I1(\GEN_BKEND_CE_REGISTERS[1].ce_out_i_reg[1] ),
        .I2(\sig_register_array_reg_n_0_[1][9] ),
        .I3(\GEN_BKEND_CE_REGISTERS[0].ce_out_i_reg[0] ),
        .O(sig_ip2bus_data[9]));
  FDRE #(
    .INIT(1'b0)) 
    \sig_ip2bus_data_reg[0] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(sig_ip2bus_data[0]),
        .Q(\s_axi_rdata_i_reg[31] [31]),
        .R(\MEM_DECODE_GEN[0].cs_out_i_reg[0] ));
  FDRE #(
    .INIT(1'b0)) 
    \sig_ip2bus_data_reg[10] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(D[21]),
        .Q(\s_axi_rdata_i_reg[31] [21]),
        .R(\MEM_DECODE_GEN[0].cs_out_i_reg[0] ));
  FDRE #(
    .INIT(1'b0)) 
    \sig_ip2bus_data_reg[11] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(D[20]),
        .Q(\s_axi_rdata_i_reg[31] [20]),
        .R(\MEM_DECODE_GEN[0].cs_out_i_reg[0] ));
  FDRE #(
    .INIT(1'b0)) 
    \sig_ip2bus_data_reg[12] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(D[19]),
        .Q(\s_axi_rdata_i_reg[31] [19]),
        .R(\MEM_DECODE_GEN[0].cs_out_i_reg[0] ));
  FDRE #(
    .INIT(1'b0)) 
    \sig_ip2bus_data_reg[13] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(D[18]),
        .Q(\s_axi_rdata_i_reg[31] [18]),
        .R(\MEM_DECODE_GEN[0].cs_out_i_reg[0] ));
  FDRE #(
    .INIT(1'b0)) 
    \sig_ip2bus_data_reg[14] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(D[17]),
        .Q(\s_axi_rdata_i_reg[31] [17]),
        .R(\MEM_DECODE_GEN[0].cs_out_i_reg[0] ));
  FDRE #(
    .INIT(1'b0)) 
    \sig_ip2bus_data_reg[15] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(D[16]),
        .Q(\s_axi_rdata_i_reg[31] [16]),
        .R(\MEM_DECODE_GEN[0].cs_out_i_reg[0] ));
  FDRE #(
    .INIT(1'b0)) 
    \sig_ip2bus_data_reg[16] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(D[15]),
        .Q(\s_axi_rdata_i_reg[31] [15]),
        .R(\MEM_DECODE_GEN[0].cs_out_i_reg[0] ));
  FDRE #(
    .INIT(1'b0)) 
    \sig_ip2bus_data_reg[17] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(D[14]),
        .Q(\s_axi_rdata_i_reg[31] [14]),
        .R(\MEM_DECODE_GEN[0].cs_out_i_reg[0] ));
  FDRE #(
    .INIT(1'b0)) 
    \sig_ip2bus_data_reg[18] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(D[13]),
        .Q(\s_axi_rdata_i_reg[31] [13]),
        .R(\MEM_DECODE_GEN[0].cs_out_i_reg[0] ));
  FDRE #(
    .INIT(1'b0)) 
    \sig_ip2bus_data_reg[19] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(D[12]),
        .Q(\s_axi_rdata_i_reg[31] [12]),
        .R(\MEM_DECODE_GEN[0].cs_out_i_reg[0] ));
  FDRE #(
    .INIT(1'b0)) 
    \sig_ip2bus_data_reg[1] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(sig_ip2bus_data[1]),
        .Q(\s_axi_rdata_i_reg[31] [30]),
        .R(\MEM_DECODE_GEN[0].cs_out_i_reg[0] ));
  FDRE #(
    .INIT(1'b0)) 
    \sig_ip2bus_data_reg[20] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(D[11]),
        .Q(\s_axi_rdata_i_reg[31] [11]),
        .R(\MEM_DECODE_GEN[0].cs_out_i_reg[0] ));
  FDRE #(
    .INIT(1'b0)) 
    \sig_ip2bus_data_reg[21] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(D[10]),
        .Q(\s_axi_rdata_i_reg[31] [10]),
        .R(\MEM_DECODE_GEN[0].cs_out_i_reg[0] ));
  FDRE #(
    .INIT(1'b0)) 
    \sig_ip2bus_data_reg[22] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(D[9]),
        .Q(\s_axi_rdata_i_reg[31] [9]),
        .R(\MEM_DECODE_GEN[0].cs_out_i_reg[0] ));
  FDRE #(
    .INIT(1'b0)) 
    \sig_ip2bus_data_reg[23] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(D[8]),
        .Q(\s_axi_rdata_i_reg[31] [8]),
        .R(\MEM_DECODE_GEN[0].cs_out_i_reg[0] ));
  FDRE #(
    .INIT(1'b0)) 
    \sig_ip2bus_data_reg[24] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(D[7]),
        .Q(\s_axi_rdata_i_reg[31] [7]),
        .R(\MEM_DECODE_GEN[0].cs_out_i_reg[0] ));
  FDRE #(
    .INIT(1'b0)) 
    \sig_ip2bus_data_reg[25] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(D[6]),
        .Q(\s_axi_rdata_i_reg[31] [6]),
        .R(\MEM_DECODE_GEN[0].cs_out_i_reg[0] ));
  FDRE #(
    .INIT(1'b0)) 
    \sig_ip2bus_data_reg[26] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(D[5]),
        .Q(\s_axi_rdata_i_reg[31] [5]),
        .R(\MEM_DECODE_GEN[0].cs_out_i_reg[0] ));
  FDRE #(
    .INIT(1'b0)) 
    \sig_ip2bus_data_reg[27] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(D[4]),
        .Q(\s_axi_rdata_i_reg[31] [4]),
        .R(\MEM_DECODE_GEN[0].cs_out_i_reg[0] ));
  FDRE #(
    .INIT(1'b0)) 
    \sig_ip2bus_data_reg[28] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(D[3]),
        .Q(\s_axi_rdata_i_reg[31] [3]),
        .R(\MEM_DECODE_GEN[0].cs_out_i_reg[0] ));
  FDRE #(
    .INIT(1'b0)) 
    \sig_ip2bus_data_reg[29] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(D[2]),
        .Q(\s_axi_rdata_i_reg[31] [2]),
        .R(\MEM_DECODE_GEN[0].cs_out_i_reg[0] ));
  FDRE #(
    .INIT(1'b0)) 
    \sig_ip2bus_data_reg[2] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(sig_ip2bus_data[2]),
        .Q(\s_axi_rdata_i_reg[31] [29]),
        .R(\MEM_DECODE_GEN[0].cs_out_i_reg[0] ));
  FDRE #(
    .INIT(1'b0)) 
    \sig_ip2bus_data_reg[30] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(D[1]),
        .Q(\s_axi_rdata_i_reg[31] [1]),
        .R(\MEM_DECODE_GEN[0].cs_out_i_reg[0] ));
  FDRE #(
    .INIT(1'b0)) 
    \sig_ip2bus_data_reg[31] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(D[0]),
        .Q(\s_axi_rdata_i_reg[31] [0]),
        .R(\MEM_DECODE_GEN[0].cs_out_i_reg[0] ));
  FDRE #(
    .INIT(1'b0)) 
    \sig_ip2bus_data_reg[3] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(sig_ip2bus_data[3]),
        .Q(\s_axi_rdata_i_reg[31] [28]),
        .R(\MEM_DECODE_GEN[0].cs_out_i_reg[0] ));
  FDRE #(
    .INIT(1'b0)) 
    \sig_ip2bus_data_reg[4] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(sig_ip2bus_data[4]),
        .Q(\s_axi_rdata_i_reg[31] [27]),
        .R(\MEM_DECODE_GEN[0].cs_out_i_reg[0] ));
  FDRE #(
    .INIT(1'b0)) 
    \sig_ip2bus_data_reg[5] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(sig_ip2bus_data[5]),
        .Q(\s_axi_rdata_i_reg[31] [26]),
        .R(\MEM_DECODE_GEN[0].cs_out_i_reg[0] ));
  FDRE #(
    .INIT(1'b0)) 
    \sig_ip2bus_data_reg[6] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(sig_ip2bus_data[6]),
        .Q(\s_axi_rdata_i_reg[31] [25]),
        .R(\MEM_DECODE_GEN[0].cs_out_i_reg[0] ));
  FDRE #(
    .INIT(1'b0)) 
    \sig_ip2bus_data_reg[7] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(sig_ip2bus_data[7]),
        .Q(\s_axi_rdata_i_reg[31] [24]),
        .R(\MEM_DECODE_GEN[0].cs_out_i_reg[0] ));
  FDRE #(
    .INIT(1'b0)) 
    \sig_ip2bus_data_reg[8] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(sig_ip2bus_data[8]),
        .Q(\s_axi_rdata_i_reg[31] [23]),
        .R(\MEM_DECODE_GEN[0].cs_out_i_reg[0] ));
  FDRE #(
    .INIT(1'b0)) 
    \sig_ip2bus_data_reg[9] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(sig_ip2bus_data[9]),
        .Q(\s_axi_rdata_i_reg[31] [22]),
        .R(\MEM_DECODE_GEN[0].cs_out_i_reg[0] ));
  FDRE #(
    .INIT(1'b0)) 
    sig_rd_rlen_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(IPIC_STATE_reg_2),
        .Q(sig_rd_rlen_reg_n_0),
        .R(1'b0));
  FDRE \sig_register_array_reg[0][0] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(IPIC_STATE_reg_0),
        .Q(\sig_ip2bus_data_reg[0]_0 ),
        .R(SR));
  FDRE \sig_register_array_reg[0][10] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(\sig_register_array_reg[0][10]_1 ),
        .Q(\sig_register_array_reg[0][10]_0 ),
        .R(SR));
  FDRE \sig_register_array_reg[0][11] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(\sig_register_array_reg[0][11]_1 ),
        .Q(\sig_register_array_reg[0][11]_0 ),
        .R(SR));
  FDRE \sig_register_array_reg[0][12] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(\sig_register_array_reg[0][12]_1 ),
        .Q(\sig_register_array_reg[0][12]_0 ),
        .R(SR));
  FDRE \sig_register_array_reg[0][1] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(\sig_register_array_reg[0][1]_1 ),
        .Q(\sig_ip2bus_data_reg[1]_0 ),
        .R(SR));
  FDRE \sig_register_array_reg[0][2] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(\sig_register_array_reg[0][2]_2 ),
        .Q(\sig_ip2bus_data_reg[2]_0 ),
        .R(SR));
  FDRE \sig_register_array_reg[0][3] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(\sig_register_array_reg[0][3]_0 ),
        .Q(\sig_ip2bus_data_reg[3]_0 ),
        .R(SR));
  FDRE \sig_register_array_reg[0][4] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(\sig_register_array_reg[0][4]_1 ),
        .Q(\sig_ip2bus_data_reg[4]_0 ),
        .R(SR));
  FDRE \sig_register_array_reg[0][5] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(\sig_register_array_reg[0][5]_0 ),
        .Q(\sig_ip2bus_data_reg[5]_0 ),
        .R(SR));
  FDRE \sig_register_array_reg[0][6] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(\sig_register_array_reg[0][6]_0 ),
        .Q(\sig_ip2bus_data_reg[6]_0 ),
        .R(SR));
  FDRE \sig_register_array_reg[0][7] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(\sig_register_array_reg[0][7]_0 ),
        .Q(\sig_ip2bus_data_reg[7]_0 ),
        .R(SR));
  FDRE \sig_register_array_reg[0][8] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(\sig_register_array_reg[0][8]_0 ),
        .Q(\sig_ip2bus_data_reg[8]_0 ),
        .R(SR));
  FDRE \sig_register_array_reg[0][9] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(\sig_register_array_reg[0][9]_0 ),
        .Q(\sig_ip2bus_data_reg[9]_0 ),
        .R(SR));
  FDRE \sig_register_array_reg[1][0] 
       (.C(s_axi_aclk),
        .CE(IPIC_STATE_reg_4),
        .D(Bus_RNW_reg_reg[12]),
        .Q(\sig_register_array_reg_n_0_[1][0] ),
        .R(SR));
  FDRE \sig_register_array_reg[1][10] 
       (.C(s_axi_aclk),
        .CE(IPIC_STATE_reg_4),
        .D(Bus_RNW_reg_reg[2]),
        .Q(\sig_ip2bus_data_reg[10]_0 [2]),
        .R(SR));
  FDRE \sig_register_array_reg[1][11] 
       (.C(s_axi_aclk),
        .CE(IPIC_STATE_reg_4),
        .D(Bus_RNW_reg_reg[1]),
        .Q(\sig_ip2bus_data_reg[10]_0 [1]),
        .R(SR));
  FDRE \sig_register_array_reg[1][12] 
       (.C(s_axi_aclk),
        .CE(IPIC_STATE_reg_4),
        .D(Bus_RNW_reg_reg[0]),
        .Q(\sig_ip2bus_data_reg[10]_0 [0]),
        .R(SR));
  FDRE \sig_register_array_reg[1][1] 
       (.C(s_axi_aclk),
        .CE(IPIC_STATE_reg_4),
        .D(Bus_RNW_reg_reg[11]),
        .Q(\sig_register_array_reg_n_0_[1][1] ),
        .R(SR));
  FDRE \sig_register_array_reg[1][2] 
       (.C(s_axi_aclk),
        .CE(IPIC_STATE_reg_4),
        .D(Bus_RNW_reg_reg[10]),
        .Q(\sig_register_array_reg_n_0_[1][2] ),
        .R(SR));
  FDRE \sig_register_array_reg[1][3] 
       (.C(s_axi_aclk),
        .CE(IPIC_STATE_reg_4),
        .D(Bus_RNW_reg_reg[9]),
        .Q(\sig_register_array_reg_n_0_[1][3] ),
        .R(SR));
  FDRE \sig_register_array_reg[1][4] 
       (.C(s_axi_aclk),
        .CE(IPIC_STATE_reg_4),
        .D(Bus_RNW_reg_reg[8]),
        .Q(\sig_register_array_reg_n_0_[1][4] ),
        .R(SR));
  FDRE \sig_register_array_reg[1][5] 
       (.C(s_axi_aclk),
        .CE(IPIC_STATE_reg_4),
        .D(Bus_RNW_reg_reg[7]),
        .Q(\sig_register_array_reg_n_0_[1][5] ),
        .R(SR));
  FDRE \sig_register_array_reg[1][6] 
       (.C(s_axi_aclk),
        .CE(IPIC_STATE_reg_4),
        .D(Bus_RNW_reg_reg[6]),
        .Q(\sig_register_array_reg_n_0_[1][6] ),
        .R(SR));
  FDRE \sig_register_array_reg[1][7] 
       (.C(s_axi_aclk),
        .CE(IPIC_STATE_reg_4),
        .D(Bus_RNW_reg_reg[5]),
        .Q(\sig_register_array_reg_n_0_[1][7] ),
        .R(SR));
  FDRE \sig_register_array_reg[1][8] 
       (.C(s_axi_aclk),
        .CE(IPIC_STATE_reg_4),
        .D(Bus_RNW_reg_reg[4]),
        .Q(\sig_register_array_reg_n_0_[1][8] ),
        .R(SR));
  FDRE \sig_register_array_reg[1][9] 
       (.C(s_axi_aclk),
        .CE(IPIC_STATE_reg_4),
        .D(Bus_RNW_reg_reg[3]),
        .Q(\sig_register_array_reg_n_0_[1][9] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    sig_rx_channel_reset_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(sig_rx_channel_reset_reg_0),
        .Q(\gpr1.dout_i_reg[0] ),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT2 #(
    .INIT(4'hB)) 
    sig_str_rst_i_6
       (.I0(IPIC_STATE),
        .I1(sig_Bus2IP_CS),
        .O(IP2Bus_Error1_in));
  LUT5 #(
    .INIT(32'h00200000)) 
    sig_str_rst_i_7
       (.I0(s_axi_wdata[5]),
        .I1(s_axi_wdata[4]),
        .I2(s_axi_wdata[7]),
        .I3(s_axi_wdata[6]),
        .I4(sig_str_rst_i_8_n_0),
        .O(eqOp__6));
  LUT4 #(
    .INIT(16'h0400)) 
    sig_str_rst_i_8
       (.I0(s_axi_wdata[1]),
        .I1(s_axi_wdata[0]),
        .I2(s_axi_wdata[3]),
        .I3(s_axi_wdata[2]),
        .O(sig_str_rst_i_8_n_0));
  FDSE #(
    .INIT(1'b0)) 
    sig_str_rst_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(\GEN_BKEND_CE_REGISTERS[11].ce_out_i_reg[11] ),
        .Q(sync_areset_n_reg_inv),
        .S(SR));
  FDRE #(
    .INIT(1'b0)) 
    sig_tx_channel_reset_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(sig_tx_channel_reset_reg_0),
        .Q(sync_areset_n_reg_inv_0),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    sig_txd_sb_wr_en_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(IPIC_STATE_reg_1),
        .Q(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram ),
        .R(1'b0));
  LUT4 #(
    .INIT(16'hFE02)) 
    \sig_txd_wr_data[31]_i_5 
       (.I0(\eqOp_inferred__1/i__carry__1_n_4 ),
        .I1(s_axi_wdata[1]),
        .I2(s_axi_wdata[0]),
        .I3(\eqOp_inferred__2/i__carry__1_n_4 ),
        .O(\sig_txd_wr_data_reg[31]_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \sig_txd_wr_data_reg[0] 
       (.C(s_axi_aclk),
        .CE(IPIC_STATE_reg_3),
        .D(s_axi_wdata[0]),
        .Q(\sig_txd_wr_data_reg_n_0_[0] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \sig_txd_wr_data_reg[10] 
       (.C(s_axi_aclk),
        .CE(IPIC_STATE_reg_3),
        .D(s_axi_wdata[10]),
        .Q(\sig_txd_wr_data_reg_n_0_[10] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \sig_txd_wr_data_reg[11] 
       (.C(s_axi_aclk),
        .CE(IPIC_STATE_reg_3),
        .D(s_axi_wdata[11]),
        .Q(\sig_txd_wr_data_reg_n_0_[11] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \sig_txd_wr_data_reg[12] 
       (.C(s_axi_aclk),
        .CE(IPIC_STATE_reg_3),
        .D(s_axi_wdata[12]),
        .Q(\sig_txd_wr_data_reg_n_0_[12] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \sig_txd_wr_data_reg[13] 
       (.C(s_axi_aclk),
        .CE(IPIC_STATE_reg_3),
        .D(s_axi_wdata[13]),
        .Q(\sig_txd_wr_data_reg_n_0_[13] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \sig_txd_wr_data_reg[14] 
       (.C(s_axi_aclk),
        .CE(IPIC_STATE_reg_3),
        .D(s_axi_wdata[14]),
        .Q(\sig_txd_wr_data_reg_n_0_[14] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \sig_txd_wr_data_reg[15] 
       (.C(s_axi_aclk),
        .CE(IPIC_STATE_reg_3),
        .D(s_axi_wdata[15]),
        .Q(\sig_txd_wr_data_reg_n_0_[15] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \sig_txd_wr_data_reg[16] 
       (.C(s_axi_aclk),
        .CE(IPIC_STATE_reg_3),
        .D(s_axi_wdata[16]),
        .Q(\sig_txd_wr_data_reg_n_0_[16] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \sig_txd_wr_data_reg[17] 
       (.C(s_axi_aclk),
        .CE(IPIC_STATE_reg_3),
        .D(s_axi_wdata[17]),
        .Q(\sig_txd_wr_data_reg_n_0_[17] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \sig_txd_wr_data_reg[18] 
       (.C(s_axi_aclk),
        .CE(IPIC_STATE_reg_3),
        .D(s_axi_wdata[18]),
        .Q(\sig_txd_wr_data_reg_n_0_[18] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \sig_txd_wr_data_reg[19] 
       (.C(s_axi_aclk),
        .CE(IPIC_STATE_reg_3),
        .D(s_axi_wdata[19]),
        .Q(\sig_txd_wr_data_reg_n_0_[19] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \sig_txd_wr_data_reg[1] 
       (.C(s_axi_aclk),
        .CE(IPIC_STATE_reg_3),
        .D(s_axi_wdata[1]),
        .Q(\sig_txd_wr_data_reg_n_0_[1] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \sig_txd_wr_data_reg[20] 
       (.C(s_axi_aclk),
        .CE(IPIC_STATE_reg_3),
        .D(s_axi_wdata[20]),
        .Q(\sig_txd_wr_data_reg_n_0_[20] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \sig_txd_wr_data_reg[21] 
       (.C(s_axi_aclk),
        .CE(IPIC_STATE_reg_3),
        .D(s_axi_wdata[21]),
        .Q(\sig_txd_wr_data_reg_n_0_[21] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \sig_txd_wr_data_reg[22] 
       (.C(s_axi_aclk),
        .CE(IPIC_STATE_reg_3),
        .D(s_axi_wdata[22]),
        .Q(\sig_txd_wr_data_reg_n_0_[22] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \sig_txd_wr_data_reg[23] 
       (.C(s_axi_aclk),
        .CE(IPIC_STATE_reg_3),
        .D(s_axi_wdata[23]),
        .Q(\sig_txd_wr_data_reg_n_0_[23] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \sig_txd_wr_data_reg[24] 
       (.C(s_axi_aclk),
        .CE(IPIC_STATE_reg_3),
        .D(s_axi_wdata[24]),
        .Q(\sig_txd_wr_data_reg_n_0_[24] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \sig_txd_wr_data_reg[25] 
       (.C(s_axi_aclk),
        .CE(IPIC_STATE_reg_3),
        .D(s_axi_wdata[25]),
        .Q(\sig_txd_wr_data_reg_n_0_[25] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \sig_txd_wr_data_reg[26] 
       (.C(s_axi_aclk),
        .CE(IPIC_STATE_reg_3),
        .D(s_axi_wdata[26]),
        .Q(\sig_txd_wr_data_reg_n_0_[26] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \sig_txd_wr_data_reg[27] 
       (.C(s_axi_aclk),
        .CE(IPIC_STATE_reg_3),
        .D(s_axi_wdata[27]),
        .Q(\sig_txd_wr_data_reg_n_0_[27] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \sig_txd_wr_data_reg[28] 
       (.C(s_axi_aclk),
        .CE(IPIC_STATE_reg_3),
        .D(s_axi_wdata[28]),
        .Q(\sig_txd_wr_data_reg_n_0_[28] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \sig_txd_wr_data_reg[29] 
       (.C(s_axi_aclk),
        .CE(IPIC_STATE_reg_3),
        .D(s_axi_wdata[29]),
        .Q(\sig_txd_wr_data_reg_n_0_[29] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \sig_txd_wr_data_reg[2] 
       (.C(s_axi_aclk),
        .CE(IPIC_STATE_reg_3),
        .D(s_axi_wdata[2]),
        .Q(\sig_txd_wr_data_reg_n_0_[2] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \sig_txd_wr_data_reg[30] 
       (.C(s_axi_aclk),
        .CE(IPIC_STATE_reg_3),
        .D(s_axi_wdata[30]),
        .Q(\sig_txd_wr_data_reg_n_0_[30] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \sig_txd_wr_data_reg[31] 
       (.C(s_axi_aclk),
        .CE(IPIC_STATE_reg_3),
        .D(s_axi_wdata[31]),
        .Q(\sig_txd_wr_data_reg_n_0_[31] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \sig_txd_wr_data_reg[3] 
       (.C(s_axi_aclk),
        .CE(IPIC_STATE_reg_3),
        .D(s_axi_wdata[3]),
        .Q(\sig_txd_wr_data_reg_n_0_[3] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \sig_txd_wr_data_reg[4] 
       (.C(s_axi_aclk),
        .CE(IPIC_STATE_reg_3),
        .D(s_axi_wdata[4]),
        .Q(\sig_txd_wr_data_reg_n_0_[4] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \sig_txd_wr_data_reg[5] 
       (.C(s_axi_aclk),
        .CE(IPIC_STATE_reg_3),
        .D(s_axi_wdata[5]),
        .Q(\sig_txd_wr_data_reg_n_0_[5] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \sig_txd_wr_data_reg[6] 
       (.C(s_axi_aclk),
        .CE(IPIC_STATE_reg_3),
        .D(s_axi_wdata[6]),
        .Q(\sig_txd_wr_data_reg_n_0_[6] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \sig_txd_wr_data_reg[7] 
       (.C(s_axi_aclk),
        .CE(IPIC_STATE_reg_3),
        .D(s_axi_wdata[7]),
        .Q(\sig_txd_wr_data_reg_n_0_[7] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \sig_txd_wr_data_reg[8] 
       (.C(s_axi_aclk),
        .CE(IPIC_STATE_reg_3),
        .D(s_axi_wdata[8]),
        .Q(\sig_txd_wr_data_reg_n_0_[8] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \sig_txd_wr_data_reg[9] 
       (.C(s_axi_aclk),
        .CE(IPIC_STATE_reg_3),
        .D(s_axi_wdata[9]),
        .Q(\sig_txd_wr_data_reg_n_0_[9] ),
        .R(SR));
endmodule

(* ORIG_REF_NAME = "pselect_f" *) 
module axi_fifo_mm_s_0_pselect_f
   (ce_expnd_i_12,
    \bus2ip_addr_i_reg[5] ,
    Q);
  output ce_expnd_i_12;
  input [3:0]\bus2ip_addr_i_reg[5] ;
  input Q;

  wire Q;
  wire [3:0]\bus2ip_addr_i_reg[5] ;
  wire ce_expnd_i_12;

  LUT5 #(
    .INIT(32'h00000010)) 
    CS
       (.I0(\bus2ip_addr_i_reg[5] [3]),
        .I1(\bus2ip_addr_i_reg[5] [2]),
        .I2(Q),
        .I3(\bus2ip_addr_i_reg[5] [0]),
        .I4(\bus2ip_addr_i_reg[5] [1]),
        .O(ce_expnd_i_12));
endmodule

(* ORIG_REF_NAME = "pselect_f" *) 
module axi_fifo_mm_s_0_pselect_f__parameterized0
   (ce_expnd_i_11,
    \bus2ip_addr_i_reg[5] ,
    Q);
  output ce_expnd_i_11;
  input [3:0]\bus2ip_addr_i_reg[5] ;
  input Q;

  wire Q;
  wire [3:0]\bus2ip_addr_i_reg[5] ;
  wire ce_expnd_i_11;

  LUT5 #(
    .INIT(32'h01000000)) 
    CS
       (.I0(\bus2ip_addr_i_reg[5] [3]),
        .I1(\bus2ip_addr_i_reg[5] [2]),
        .I2(\bus2ip_addr_i_reg[5] [1]),
        .I3(\bus2ip_addr_i_reg[5] [0]),
        .I4(Q),
        .O(ce_expnd_i_11));
endmodule

(* ORIG_REF_NAME = "pselect_f" *) 
module axi_fifo_mm_s_0_pselect_f__parameterized1
   (ce_expnd_i_10,
    \bus2ip_addr_i_reg[5] ,
    Q);
  output ce_expnd_i_10;
  input [3:0]\bus2ip_addr_i_reg[5] ;
  input Q;

  wire Q;
  wire [3:0]\bus2ip_addr_i_reg[5] ;
  wire ce_expnd_i_10;

  LUT5 #(
    .INIT(32'h01000000)) 
    CS
       (.I0(\bus2ip_addr_i_reg[5] [3]),
        .I1(\bus2ip_addr_i_reg[5] [2]),
        .I2(\bus2ip_addr_i_reg[5] [0]),
        .I3(\bus2ip_addr_i_reg[5] [1]),
        .I4(Q),
        .O(ce_expnd_i_10));
endmodule

(* ORIG_REF_NAME = "pselect_f" *) 
module axi_fifo_mm_s_0_pselect_f__parameterized10
   (ce_expnd_i_1,
    \bus2ip_addr_i_reg[5] ,
    Q);
  output ce_expnd_i_1;
  input [3:0]\bus2ip_addr_i_reg[5] ;
  input Q;

  wire Q;
  wire [3:0]\bus2ip_addr_i_reg[5] ;
  wire ce_expnd_i_1;

  LUT5 #(
    .INIT(32'h40000000)) 
    CS
       (.I0(\bus2ip_addr_i_reg[5] [2]),
        .I1(\bus2ip_addr_i_reg[5] [3]),
        .I2(Q),
        .I3(\bus2ip_addr_i_reg[5] [0]),
        .I4(\bus2ip_addr_i_reg[5] [1]),
        .O(ce_expnd_i_1));
endmodule

(* ORIG_REF_NAME = "pselect_f" *) 
module axi_fifo_mm_s_0_pselect_f__parameterized11
   (ce_expnd_i_0,
    \bus2ip_addr_i_reg[5] ,
    Q);
  output ce_expnd_i_0;
  input [3:0]\bus2ip_addr_i_reg[5] ;
  input Q;

  wire Q;
  wire [3:0]\bus2ip_addr_i_reg[5] ;
  wire ce_expnd_i_0;

  LUT5 #(
    .INIT(32'h10000000)) 
    CS
       (.I0(\bus2ip_addr_i_reg[5] [1]),
        .I1(\bus2ip_addr_i_reg[5] [0]),
        .I2(\bus2ip_addr_i_reg[5] [3]),
        .I3(\bus2ip_addr_i_reg[5] [2]),
        .I4(Q),
        .O(ce_expnd_i_0));
endmodule

(* ORIG_REF_NAME = "pselect_f" *) 
module axi_fifo_mm_s_0_pselect_f__parameterized3
   (ce_expnd_i_8,
    \bus2ip_addr_i_reg[5] ,
    Q);
  output ce_expnd_i_8;
  input [3:0]\bus2ip_addr_i_reg[5] ;
  input Q;

  wire Q;
  wire [3:0]\bus2ip_addr_i_reg[5] ;
  wire ce_expnd_i_8;

  LUT5 #(
    .INIT(32'h01000000)) 
    CS
       (.I0(\bus2ip_addr_i_reg[5] [3]),
        .I1(\bus2ip_addr_i_reg[5] [1]),
        .I2(\bus2ip_addr_i_reg[5] [0]),
        .I3(\bus2ip_addr_i_reg[5] [2]),
        .I4(Q),
        .O(ce_expnd_i_8));
endmodule

(* ORIG_REF_NAME = "pselect_f" *) 
module axi_fifo_mm_s_0_pselect_f__parameterized4
   (ce_expnd_i_7,
    \bus2ip_addr_i_reg[5] ,
    Q);
  output ce_expnd_i_7;
  input [3:0]\bus2ip_addr_i_reg[5] ;
  input Q;

  wire Q;
  wire [3:0]\bus2ip_addr_i_reg[5] ;
  wire ce_expnd_i_7;

  LUT5 #(
    .INIT(32'h10000000)) 
    CS
       (.I0(\bus2ip_addr_i_reg[5] [3]),
        .I1(\bus2ip_addr_i_reg[5] [1]),
        .I2(Q),
        .I3(\bus2ip_addr_i_reg[5] [0]),
        .I4(\bus2ip_addr_i_reg[5] [2]),
        .O(ce_expnd_i_7));
endmodule

(* ORIG_REF_NAME = "pselect_f" *) 
module axi_fifo_mm_s_0_pselect_f__parameterized5
   (ce_expnd_i_6,
    \bus2ip_addr_i_reg[5] ,
    Q);
  output ce_expnd_i_6;
  input [3:0]\bus2ip_addr_i_reg[5] ;
  input Q;

  wire Q;
  wire [3:0]\bus2ip_addr_i_reg[5] ;
  wire ce_expnd_i_6;

  LUT5 #(
    .INIT(32'h10000000)) 
    CS
       (.I0(\bus2ip_addr_i_reg[5] [3]),
        .I1(\bus2ip_addr_i_reg[5] [0]),
        .I2(Q),
        .I3(\bus2ip_addr_i_reg[5] [1]),
        .I4(\bus2ip_addr_i_reg[5] [2]),
        .O(ce_expnd_i_6));
endmodule

(* ORIG_REF_NAME = "pselect_f" *) 
module axi_fifo_mm_s_0_pselect_f__parameterized6
   (ce_expnd_i_5,
    \bus2ip_addr_i_reg[5] ,
    Q);
  output ce_expnd_i_5;
  input [3:0]\bus2ip_addr_i_reg[5] ;
  input Q;

  wire Q;
  wire [3:0]\bus2ip_addr_i_reg[5] ;
  wire ce_expnd_i_5;

  LUT5 #(
    .INIT(32'h40000000)) 
    CS
       (.I0(\bus2ip_addr_i_reg[5] [3]),
        .I1(Q),
        .I2(\bus2ip_addr_i_reg[5] [2]),
        .I3(\bus2ip_addr_i_reg[5] [0]),
        .I4(\bus2ip_addr_i_reg[5] [1]),
        .O(ce_expnd_i_5));
endmodule

(* ORIG_REF_NAME = "pselect_f" *) 
module axi_fifo_mm_s_0_pselect_f__parameterized7
   (ce_expnd_i_4,
    \bus2ip_addr_i_reg[5] ,
    Q);
  output ce_expnd_i_4;
  input [3:0]\bus2ip_addr_i_reg[5] ;
  input Q;

  wire Q;
  wire [3:0]\bus2ip_addr_i_reg[5] ;
  wire ce_expnd_i_4;

  LUT5 #(
    .INIT(32'h01000000)) 
    CS
       (.I0(\bus2ip_addr_i_reg[5] [2]),
        .I1(\bus2ip_addr_i_reg[5] [1]),
        .I2(\bus2ip_addr_i_reg[5] [0]),
        .I3(Q),
        .I4(\bus2ip_addr_i_reg[5] [3]),
        .O(ce_expnd_i_4));
endmodule

(* ORIG_REF_NAME = "pselect_f" *) 
module axi_fifo_mm_s_0_pselect_f__parameterized8
   (ce_expnd_i_3,
    \bus2ip_addr_i_reg[5] ,
    Q);
  output ce_expnd_i_3;
  input [3:0]\bus2ip_addr_i_reg[5] ;
  input Q;

  wire Q;
  wire [3:0]\bus2ip_addr_i_reg[5] ;
  wire ce_expnd_i_3;

  LUT5 #(
    .INIT(32'h10000000)) 
    CS
       (.I0(\bus2ip_addr_i_reg[5] [2]),
        .I1(\bus2ip_addr_i_reg[5] [1]),
        .I2(\bus2ip_addr_i_reg[5] [3]),
        .I3(\bus2ip_addr_i_reg[5] [0]),
        .I4(Q),
        .O(ce_expnd_i_3));
endmodule

(* ORIG_REF_NAME = "pselect_f" *) 
module axi_fifo_mm_s_0_pselect_f__parameterized9
   (ce_expnd_i_2,
    \bus2ip_addr_i_reg[5] ,
    Q);
  output ce_expnd_i_2;
  input [3:0]\bus2ip_addr_i_reg[5] ;
  input Q;

  wire Q;
  wire [3:0]\bus2ip_addr_i_reg[5] ;
  wire ce_expnd_i_2;

  LUT5 #(
    .INIT(32'h10000000)) 
    CS
       (.I0(\bus2ip_addr_i_reg[5] [2]),
        .I1(\bus2ip_addr_i_reg[5] [0]),
        .I2(\bus2ip_addr_i_reg[5] [3]),
        .I3(\bus2ip_addr_i_reg[5] [1]),
        .I4(Q),
        .O(ce_expnd_i_2));
endmodule

(* ORIG_REF_NAME = "slave_attachment" *) 
module axi_fifo_mm_s_0_slave_attachment
   (s_axi_rresp,
    s_axi_rvalid,
    s_axi_bvalid,
    s_axi_bresp,
    IPIC_STATE_reg,
    \sig_register_array_reg[0][0] ,
    \sig_register_array_reg[0][1] ,
    \sig_register_array_reg[0][2] ,
    \sig_register_array_reg[0][3] ,
    \sig_register_array_reg[0][4] ,
    \sig_register_array_reg[0][5] ,
    \sig_register_array_reg[0][6] ,
    \sig_register_array_reg[0][7] ,
    \sig_register_array_reg[0][8] ,
    \sig_register_array_reg[0][9] ,
    \sig_register_array_reg[0][10] ,
    \sig_register_array_reg[0][11] ,
    \sig_register_array_reg[0][12] ,
    sig_str_rst_reg,
    \sig_ip2bus_data_reg[31] ,
    \sig_txd_wr_data_reg[31] ,
    IP2Bus_RdAck_reg,
    \sig_register_array_reg[1][0] ,
    IP2Bus_Error,
    \sig_register_array_reg[0][2]_0 ,
    \sig_register_array_reg[0][1]_0 ,
    \sig_register_array_reg[1][0]_0 ,
    D,
    \sig_ip2bus_data_reg[30] ,
    \sig_ip2bus_data_reg[9] ,
    \sig_ip2bus_data_reg[9]_0 ,
    \sig_ip2bus_data_reg[22] ,
    sig_rx_channel_reset_reg,
    sig_tx_channel_reset_reg,
    sig_str_rst_reg_0,
    IP2Bus_WrAck_reg,
    sig_rd_rlen_reg,
    sig_txd_sb_wr_en_reg,
    s_axi_rdata,
    SR,
    s_axi_aclk,
    cs_ce_clr,
    sig_IP2Bus_Error,
    IPIC_STATE,
    \sig_register_array_reg[0][0]_0 ,
    \gaxi_full_sm.r_last_r_reg ,
    \gaxi_full_sm.r_last_r_reg_0 ,
    IP2Bus_Error1_in,
    \sig_register_array_reg[0][1]_1 ,
    \gaxi_full_sm.r_last_r_reg_1 ,
    sig_rx_channel_reset_reg_0,
    \sig_register_array_reg[0][2]_1 ,
    tx_fifo_or,
    s_axi_wdata,
    \sig_register_array_reg[0][3]_0 ,
    axi_str_txd_tready,
    \sig_register_array_reg[0][4]_0 ,
    p_13_in,
    \sig_register_array_reg[0][5]_0 ,
    \sig_register_array_reg[0][6]_0 ,
    sig_txd_reset,
    \sig_register_array_reg[0][7]_0 ,
    sig_rxd_reset,
    \sig_register_array_reg[0][8]_0 ,
    sig_txd_pf_event__1,
    \sig_register_array_reg[0][9]_0 ,
    sig_txd_pe_event__1,
    \sig_register_array_reg[0][10]_0 ,
    sig_rxd_pf_event__1,
    \sig_register_array_reg[0][11]_0 ,
    sig_rxd_pe_event__1,
    \sig_register_array_reg[0][12]_0 ,
    IP2Bus_RdAck_reg_0,
    s_axi_arvalid,
    IP2Bus_WrAck_reg_0,
    s_axi_awvalid,
    s_axi_wvalid,
    s_axi_araddr,
    s_axi_awaddr,
    \goreg_bm.dout_i_reg[0] ,
    eqOp__6,
    s_axi_aresetn,
    sig_tx_channel_reset_reg_0,
    \gtxd.sig_txd_packet_size_reg[30] ,
    \goreg_dm.dout_i_reg[21] ,
    \gfifo_gen.gmm2s.vacancy_i_reg[1] ,
    \gfifo_gen.gmm2s.vacancy_i_reg[2] ,
    Q,
    \gfifo_gen.gmm2s.vacancy_i_reg[3] ,
    \gfifo_gen.gmm2s.vacancy_i_reg[4] ,
    \gfifo_gen.gmm2s.vacancy_i_reg[5] ,
    \gfifo_gen.gmm2s.vacancy_i_reg[6] ,
    \gfifo_gen.gmm2s.vacancy_i_reg[7] ,
    \gfifo_gen.gmm2s.vacancy_i_reg[8] ,
    \gfifo_gen.gmm2s.vacancy_i_reg[9] ,
    out,
    sig_rx_channel_reset_reg_1,
    \sig_register_array_reg[1][10] ,
    s_axi_rready,
    s_axi_bready,
    sig_str_rst_reg_1,
    \sig_ip2bus_data_reg[0] );
  output [0:0]s_axi_rresp;
  output s_axi_rvalid;
  output s_axi_bvalid;
  output [0:0]s_axi_bresp;
  output IPIC_STATE_reg;
  output \sig_register_array_reg[0][0] ;
  output \sig_register_array_reg[0][1] ;
  output \sig_register_array_reg[0][2] ;
  output \sig_register_array_reg[0][3] ;
  output \sig_register_array_reg[0][4] ;
  output \sig_register_array_reg[0][5] ;
  output \sig_register_array_reg[0][6] ;
  output \sig_register_array_reg[0][7] ;
  output \sig_register_array_reg[0][8] ;
  output \sig_register_array_reg[0][9] ;
  output \sig_register_array_reg[0][10] ;
  output \sig_register_array_reg[0][11] ;
  output \sig_register_array_reg[0][12] ;
  output sig_str_rst_reg;
  output [0:0]\sig_ip2bus_data_reg[31] ;
  output [0:0]\sig_txd_wr_data_reg[31] ;
  output IP2Bus_RdAck_reg;
  output [0:0]\sig_register_array_reg[1][0] ;
  output IP2Bus_Error;
  output \sig_register_array_reg[0][2]_0 ;
  output \sig_register_array_reg[0][1]_0 ;
  output [12:0]\sig_register_array_reg[1][0]_0 ;
  output [21:0]D;
  output \sig_ip2bus_data_reg[30] ;
  output \sig_ip2bus_data_reg[9] ;
  output \sig_ip2bus_data_reg[9]_0 ;
  output \sig_ip2bus_data_reg[22] ;
  output sig_rx_channel_reset_reg;
  output sig_tx_channel_reset_reg;
  output sig_str_rst_reg_0;
  output IP2Bus_WrAck_reg;
  output sig_rd_rlen_reg;
  output sig_txd_sb_wr_en_reg;
  output [31:0]s_axi_rdata;
  input [0:0]SR;
  input s_axi_aclk;
  input cs_ce_clr;
  input sig_IP2Bus_Error;
  input IPIC_STATE;
  input \sig_register_array_reg[0][0]_0 ;
  input \gaxi_full_sm.r_last_r_reg ;
  input \gaxi_full_sm.r_last_r_reg_0 ;
  input IP2Bus_Error1_in;
  input \sig_register_array_reg[0][1]_1 ;
  input \gaxi_full_sm.r_last_r_reg_1 ;
  input sig_rx_channel_reset_reg_0;
  input \sig_register_array_reg[0][2]_1 ;
  input tx_fifo_or;
  input [12:0]s_axi_wdata;
  input \sig_register_array_reg[0][3]_0 ;
  input axi_str_txd_tready;
  input \sig_register_array_reg[0][4]_0 ;
  input p_13_in;
  input \sig_register_array_reg[0][5]_0 ;
  input \sig_register_array_reg[0][6]_0 ;
  input sig_txd_reset;
  input \sig_register_array_reg[0][7]_0 ;
  input sig_rxd_reset;
  input \sig_register_array_reg[0][8]_0 ;
  input sig_txd_pf_event__1;
  input \sig_register_array_reg[0][9]_0 ;
  input sig_txd_pe_event__1;
  input \sig_register_array_reg[0][10]_0 ;
  input sig_rxd_pf_event__1;
  input \sig_register_array_reg[0][11]_0 ;
  input sig_rxd_pe_event__1;
  input \sig_register_array_reg[0][12]_0 ;
  input IP2Bus_RdAck_reg_0;
  input s_axi_arvalid;
  input IP2Bus_WrAck_reg_0;
  input s_axi_awvalid;
  input s_axi_wvalid;
  input [3:0]s_axi_araddr;
  input [3:0]s_axi_awaddr;
  input \goreg_bm.dout_i_reg[0] ;
  input eqOp__6;
  input s_axi_aresetn;
  input sig_tx_channel_reset_reg_0;
  input \gtxd.sig_txd_packet_size_reg[30] ;
  input [21:0]\goreg_dm.dout_i_reg[21] ;
  input [0:0]\gfifo_gen.gmm2s.vacancy_i_reg[1] ;
  input \gfifo_gen.gmm2s.vacancy_i_reg[2] ;
  input [9:0]Q;
  input \gfifo_gen.gmm2s.vacancy_i_reg[3] ;
  input \gfifo_gen.gmm2s.vacancy_i_reg[4] ;
  input \gfifo_gen.gmm2s.vacancy_i_reg[5] ;
  input \gfifo_gen.gmm2s.vacancy_i_reg[6] ;
  input \gfifo_gen.gmm2s.vacancy_i_reg[7] ;
  input \gfifo_gen.gmm2s.vacancy_i_reg[8] ;
  input \gfifo_gen.gmm2s.vacancy_i_reg[9] ;
  input out;
  input sig_rx_channel_reset_reg_1;
  input [2:0]\sig_register_array_reg[1][10] ;
  input s_axi_rready;
  input s_axi_bready;
  input sig_str_rst_reg_1;
  input [31:0]\sig_ip2bus_data_reg[0] ;

  wire [21:0]D;
  wire IP2Bus_Error;
  wire IP2Bus_Error1_in;
  wire IP2Bus_RdAck_reg;
  wire IP2Bus_RdAck_reg_0;
  wire IP2Bus_WrAck_reg;
  wire IP2Bus_WrAck_reg_0;
  wire IPIC_STATE;
  wire IPIC_STATE_reg;
  wire [9:0]Q;
  wire [0:0]SR;
  wire axi_str_txd_tready;
  wire \bus2ip_addr_i[2]_i_1_n_0 ;
  wire \bus2ip_addr_i[3]_i_1_n_0 ;
  wire \bus2ip_addr_i[4]_i_1_n_0 ;
  wire \bus2ip_addr_i[5]_i_1_n_0 ;
  wire \bus2ip_addr_i[5]_i_2_n_0 ;
  wire \bus2ip_addr_i_reg_n_0_[2] ;
  wire \bus2ip_addr_i_reg_n_0_[3] ;
  wire \bus2ip_addr_i_reg_n_0_[4] ;
  wire \bus2ip_addr_i_reg_n_0_[5] ;
  wire bus2ip_rnw_i03_out;
  wire cs_ce_clr;
  wire eqOp__6;
  wire \gaxi_full_sm.r_last_r_reg ;
  wire \gaxi_full_sm.r_last_r_reg_0 ;
  wire \gaxi_full_sm.r_last_r_reg_1 ;
  wire [0:0]\gfifo_gen.gmm2s.vacancy_i_reg[1] ;
  wire \gfifo_gen.gmm2s.vacancy_i_reg[2] ;
  wire \gfifo_gen.gmm2s.vacancy_i_reg[3] ;
  wire \gfifo_gen.gmm2s.vacancy_i_reg[4] ;
  wire \gfifo_gen.gmm2s.vacancy_i_reg[5] ;
  wire \gfifo_gen.gmm2s.vacancy_i_reg[6] ;
  wire \gfifo_gen.gmm2s.vacancy_i_reg[7] ;
  wire \gfifo_gen.gmm2s.vacancy_i_reg[8] ;
  wire \gfifo_gen.gmm2s.vacancy_i_reg[9] ;
  wire \goreg_bm.dout_i_reg[0] ;
  wire [21:0]\goreg_dm.dout_i_reg[21] ;
  wire \gtxd.sig_txd_packet_size_reg[30] ;
  wire out;
  wire p_13_in;
  wire rst;
  wire s_axi_aclk;
  wire [3:0]s_axi_araddr;
  wire s_axi_aresetn;
  wire s_axi_arvalid;
  wire [3:0]s_axi_awaddr;
  wire s_axi_awvalid;
  wire s_axi_bready;
  wire [0:0]s_axi_bresp;
  wire \s_axi_bresp_i[1]_i_1_n_0 ;
  wire s_axi_bvalid;
  wire s_axi_bvalid_i_i_1_n_0;
  wire [31:0]s_axi_rdata;
  wire s_axi_rdata_i;
  wire s_axi_rready;
  wire [0:0]s_axi_rresp;
  wire s_axi_rvalid;
  wire s_axi_rvalid_i_i_1_n_0;
  wire [12:0]s_axi_wdata;
  wire s_axi_wvalid;
  wire sig_Bus2IP_RNW;
  wire sig_IP2Bus_Error;
  wire [31:0]\sig_ip2bus_data_reg[0] ;
  wire \sig_ip2bus_data_reg[22] ;
  wire \sig_ip2bus_data_reg[30] ;
  wire [0:0]\sig_ip2bus_data_reg[31] ;
  wire \sig_ip2bus_data_reg[9] ;
  wire \sig_ip2bus_data_reg[9]_0 ;
  wire sig_rd_rlen_reg;
  wire \sig_register_array_reg[0][0] ;
  wire \sig_register_array_reg[0][0]_0 ;
  wire \sig_register_array_reg[0][10] ;
  wire \sig_register_array_reg[0][10]_0 ;
  wire \sig_register_array_reg[0][11] ;
  wire \sig_register_array_reg[0][11]_0 ;
  wire \sig_register_array_reg[0][12] ;
  wire \sig_register_array_reg[0][12]_0 ;
  wire \sig_register_array_reg[0][1] ;
  wire \sig_register_array_reg[0][1]_0 ;
  wire \sig_register_array_reg[0][1]_1 ;
  wire \sig_register_array_reg[0][2] ;
  wire \sig_register_array_reg[0][2]_0 ;
  wire \sig_register_array_reg[0][2]_1 ;
  wire \sig_register_array_reg[0][3] ;
  wire \sig_register_array_reg[0][3]_0 ;
  wire \sig_register_array_reg[0][4] ;
  wire \sig_register_array_reg[0][4]_0 ;
  wire \sig_register_array_reg[0][5] ;
  wire \sig_register_array_reg[0][5]_0 ;
  wire \sig_register_array_reg[0][6] ;
  wire \sig_register_array_reg[0][6]_0 ;
  wire \sig_register_array_reg[0][7] ;
  wire \sig_register_array_reg[0][7]_0 ;
  wire \sig_register_array_reg[0][8] ;
  wire \sig_register_array_reg[0][8]_0 ;
  wire \sig_register_array_reg[0][9] ;
  wire \sig_register_array_reg[0][9]_0 ;
  wire [0:0]\sig_register_array_reg[1][0] ;
  wire [12:0]\sig_register_array_reg[1][0]_0 ;
  wire [2:0]\sig_register_array_reg[1][10] ;
  wire sig_rx_channel_reset_reg;
  wire sig_rx_channel_reset_reg_0;
  wire sig_rx_channel_reset_reg_1;
  wire sig_rxd_pe_event__1;
  wire sig_rxd_pf_event__1;
  wire sig_rxd_reset;
  wire sig_str_rst_reg;
  wire sig_str_rst_reg_0;
  wire sig_str_rst_reg_1;
  wire sig_tx_channel_reset_reg;
  wire sig_tx_channel_reset_reg_0;
  wire sig_txd_pe_event__1;
  wire sig_txd_pf_event__1;
  wire sig_txd_reset;
  wire sig_txd_sb_wr_en_reg;
  wire [0:0]\sig_txd_wr_data_reg[31] ;
  wire start2;
  wire start2_i_1_n_0;
  wire [1:0]state;
  wire \state[0]_i_1_n_0 ;
  wire \state[1]_i_1_n_0 ;
  wire \state[1]_i_2_n_0 ;
  wire \state[1]_i_3_n_0 ;
  wire tx_fifo_or;

  axi_fifo_mm_s_0_address_decoder I_DECODER
       (.D(D),
        .IP2Bus_Error(IP2Bus_Error),
        .IP2Bus_Error1_in(IP2Bus_Error1_in),
        .IP2Bus_RdAck_reg(IP2Bus_RdAck_reg),
        .IP2Bus_RdAck_reg_0(IP2Bus_RdAck_reg_0),
        .IP2Bus_WrAck_reg(IP2Bus_WrAck_reg),
        .IP2Bus_WrAck_reg_0(IP2Bus_WrAck_reg_0),
        .IPIC_STATE(IPIC_STATE),
        .IPIC_STATE_reg(IPIC_STATE_reg),
        .Q(start2),
        .axi_str_txd_tready(axi_str_txd_tready),
        .\bus2ip_addr_i_reg[5] ({\bus2ip_addr_i_reg_n_0_[5] ,\bus2ip_addr_i_reg_n_0_[4] ,\bus2ip_addr_i_reg_n_0_[3] ,\bus2ip_addr_i_reg_n_0_[2] }),
        .\count_reg[9] (Q),
        .cs_ce_clr(cs_ce_clr),
        .eqOp__6(eqOp__6),
        .\gaxi_full_sm.r_last_r_reg (\gaxi_full_sm.r_last_r_reg ),
        .\gaxi_full_sm.r_last_r_reg_0 (\gaxi_full_sm.r_last_r_reg_0 ),
        .\gaxi_full_sm.r_last_r_reg_1 (\gaxi_full_sm.r_last_r_reg_1 ),
        .\gfifo_gen.gmm2s.vacancy_i_reg[1] (\gfifo_gen.gmm2s.vacancy_i_reg[1] ),
        .\gfifo_gen.gmm2s.vacancy_i_reg[2] (\gfifo_gen.gmm2s.vacancy_i_reg[2] ),
        .\gfifo_gen.gmm2s.vacancy_i_reg[3] (\gfifo_gen.gmm2s.vacancy_i_reg[3] ),
        .\gfifo_gen.gmm2s.vacancy_i_reg[4] (\gfifo_gen.gmm2s.vacancy_i_reg[4] ),
        .\gfifo_gen.gmm2s.vacancy_i_reg[5] (\gfifo_gen.gmm2s.vacancy_i_reg[5] ),
        .\gfifo_gen.gmm2s.vacancy_i_reg[6] (\gfifo_gen.gmm2s.vacancy_i_reg[6] ),
        .\gfifo_gen.gmm2s.vacancy_i_reg[7] (\gfifo_gen.gmm2s.vacancy_i_reg[7] ),
        .\gfifo_gen.gmm2s.vacancy_i_reg[8] (\gfifo_gen.gmm2s.vacancy_i_reg[8] ),
        .\gfifo_gen.gmm2s.vacancy_i_reg[9] (\gfifo_gen.gmm2s.vacancy_i_reg[9] ),
        .\goreg_bm.dout_i_reg[0] (\goreg_bm.dout_i_reg[0] ),
        .\goreg_dm.dout_i_reg[21] (\goreg_dm.dout_i_reg[21] ),
        .\gtxd.sig_txd_packet_size_reg[30] (\gtxd.sig_txd_packet_size_reg[30] ),
        .out(out),
        .p_13_in(p_13_in),
        .s_axi_aclk(s_axi_aclk),
        .s_axi_aresetn(s_axi_aresetn),
        .s_axi_wdata(s_axi_wdata),
        .sig_Bus2IP_RNW(sig_Bus2IP_RNW),
        .\sig_ip2bus_data_reg[22] (\sig_ip2bus_data_reg[22] ),
        .\sig_ip2bus_data_reg[30] (\sig_ip2bus_data_reg[30] ),
        .\sig_ip2bus_data_reg[31] (\sig_ip2bus_data_reg[31] ),
        .\sig_ip2bus_data_reg[9] (\sig_ip2bus_data_reg[9] ),
        .\sig_ip2bus_data_reg[9]_0 (\sig_ip2bus_data_reg[9]_0 ),
        .sig_rd_rlen_reg(sig_rd_rlen_reg),
        .\sig_register_array_reg[0][0] (\sig_register_array_reg[0][0] ),
        .\sig_register_array_reg[0][0]_0 (\sig_register_array_reg[0][0]_0 ),
        .\sig_register_array_reg[0][10] (\sig_register_array_reg[0][10] ),
        .\sig_register_array_reg[0][10]_0 (\sig_register_array_reg[0][10]_0 ),
        .\sig_register_array_reg[0][11] (\sig_register_array_reg[0][11] ),
        .\sig_register_array_reg[0][11]_0 (\sig_register_array_reg[0][11]_0 ),
        .\sig_register_array_reg[0][12] (\sig_register_array_reg[0][12] ),
        .\sig_register_array_reg[0][12]_0 (\sig_register_array_reg[0][12]_0 ),
        .\sig_register_array_reg[0][1] (\sig_register_array_reg[0][1] ),
        .\sig_register_array_reg[0][1]_0 (\sig_register_array_reg[0][1]_0 ),
        .\sig_register_array_reg[0][1]_1 (\sig_register_array_reg[0][1]_1 ),
        .\sig_register_array_reg[0][2] (\sig_register_array_reg[0][2] ),
        .\sig_register_array_reg[0][2]_0 (\sig_register_array_reg[0][2]_0 ),
        .\sig_register_array_reg[0][2]_1 (\sig_register_array_reg[0][2]_1 ),
        .\sig_register_array_reg[0][3] (\sig_register_array_reg[0][3] ),
        .\sig_register_array_reg[0][3]_0 (\sig_register_array_reg[0][3]_0 ),
        .\sig_register_array_reg[0][4] (\sig_register_array_reg[0][4] ),
        .\sig_register_array_reg[0][4]_0 (\sig_register_array_reg[0][4]_0 ),
        .\sig_register_array_reg[0][5] (\sig_register_array_reg[0][5] ),
        .\sig_register_array_reg[0][5]_0 (\sig_register_array_reg[0][5]_0 ),
        .\sig_register_array_reg[0][6] (\sig_register_array_reg[0][6] ),
        .\sig_register_array_reg[0][6]_0 (\sig_register_array_reg[0][6]_0 ),
        .\sig_register_array_reg[0][7] (\sig_register_array_reg[0][7] ),
        .\sig_register_array_reg[0][7]_0 (\sig_register_array_reg[0][7]_0 ),
        .\sig_register_array_reg[0][8] (\sig_register_array_reg[0][8] ),
        .\sig_register_array_reg[0][8]_0 (\sig_register_array_reg[0][8]_0 ),
        .\sig_register_array_reg[0][9] (\sig_register_array_reg[0][9] ),
        .\sig_register_array_reg[0][9]_0 (\sig_register_array_reg[0][9]_0 ),
        .\sig_register_array_reg[1][0] (\sig_register_array_reg[1][0] ),
        .\sig_register_array_reg[1][0]_0 (\sig_register_array_reg[1][0]_0 ),
        .\sig_register_array_reg[1][10] (\sig_register_array_reg[1][10] ),
        .sig_rx_channel_reset_reg(sig_rx_channel_reset_reg),
        .sig_rx_channel_reset_reg_0(sig_rx_channel_reset_reg_0),
        .sig_rx_channel_reset_reg_1(sig_rx_channel_reset_reg_1),
        .sig_rxd_pe_event__1(sig_rxd_pe_event__1),
        .sig_rxd_pf_event__1(sig_rxd_pf_event__1),
        .sig_rxd_reset(sig_rxd_reset),
        .sig_str_rst_reg(sig_str_rst_reg),
        .sig_str_rst_reg_0(sig_str_rst_reg_0),
        .sig_str_rst_reg_1(sig_str_rst_reg_1),
        .sig_tx_channel_reset_reg(sig_tx_channel_reset_reg),
        .sig_tx_channel_reset_reg_0(sig_tx_channel_reset_reg_0),
        .sig_txd_pe_event__1(sig_txd_pe_event__1),
        .sig_txd_pf_event__1(sig_txd_pf_event__1),
        .sig_txd_reset(sig_txd_reset),
        .sig_txd_sb_wr_en_reg(sig_txd_sb_wr_en_reg),
        .\sig_txd_wr_data_reg[31] (\sig_txd_wr_data_reg[31] ),
        .tx_fifo_or(tx_fifo_or));
  (* SOFT_HLUTNM = "soft_lutpair105" *) 
  LUT5 #(
    .INIT(32'hFFFB0008)) 
    \bus2ip_addr_i[2]_i_1 
       (.I0(s_axi_araddr[0]),
        .I1(s_axi_arvalid),
        .I2(state[0]),
        .I3(state[1]),
        .I4(s_axi_awaddr[0]),
        .O(\bus2ip_addr_i[2]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFFB0008)) 
    \bus2ip_addr_i[3]_i_1 
       (.I0(s_axi_araddr[1]),
        .I1(s_axi_arvalid),
        .I2(state[0]),
        .I3(state[1]),
        .I4(s_axi_awaddr[1]),
        .O(\bus2ip_addr_i[3]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFFB0008)) 
    \bus2ip_addr_i[4]_i_1 
       (.I0(s_axi_araddr[2]),
        .I1(s_axi_arvalid),
        .I2(state[0]),
        .I3(state[1]),
        .I4(s_axi_awaddr[2]),
        .O(\bus2ip_addr_i[4]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h03020202)) 
    \bus2ip_addr_i[5]_i_1 
       (.I0(s_axi_arvalid),
        .I1(state[1]),
        .I2(state[0]),
        .I3(s_axi_awvalid),
        .I4(s_axi_wvalid),
        .O(\bus2ip_addr_i[5]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFFB0008)) 
    \bus2ip_addr_i[5]_i_2 
       (.I0(s_axi_araddr[3]),
        .I1(s_axi_arvalid),
        .I2(state[0]),
        .I3(state[1]),
        .I4(s_axi_awaddr[3]),
        .O(\bus2ip_addr_i[5]_i_2_n_0 ));
  FDRE \bus2ip_addr_i_reg[2] 
       (.C(s_axi_aclk),
        .CE(\bus2ip_addr_i[5]_i_1_n_0 ),
        .D(\bus2ip_addr_i[2]_i_1_n_0 ),
        .Q(\bus2ip_addr_i_reg_n_0_[2] ),
        .R(rst));
  FDRE \bus2ip_addr_i_reg[3] 
       (.C(s_axi_aclk),
        .CE(\bus2ip_addr_i[5]_i_1_n_0 ),
        .D(\bus2ip_addr_i[3]_i_1_n_0 ),
        .Q(\bus2ip_addr_i_reg_n_0_[3] ),
        .R(rst));
  FDRE \bus2ip_addr_i_reg[4] 
       (.C(s_axi_aclk),
        .CE(\bus2ip_addr_i[5]_i_1_n_0 ),
        .D(\bus2ip_addr_i[4]_i_1_n_0 ),
        .Q(\bus2ip_addr_i_reg_n_0_[4] ),
        .R(rst));
  FDRE \bus2ip_addr_i_reg[5] 
       (.C(s_axi_aclk),
        .CE(\bus2ip_addr_i[5]_i_1_n_0 ),
        .D(\bus2ip_addr_i[5]_i_2_n_0 ),
        .Q(\bus2ip_addr_i_reg_n_0_[5] ),
        .R(rst));
  (* SOFT_HLUTNM = "soft_lutpair105" *) 
  LUT3 #(
    .INIT(8'h02)) 
    bus2ip_rnw_i_i_1
       (.I0(s_axi_arvalid),
        .I1(state[0]),
        .I2(state[1]),
        .O(bus2ip_rnw_i03_out));
  FDRE bus2ip_rnw_i_reg
       (.C(s_axi_aclk),
        .CE(\bus2ip_addr_i[5]_i_1_n_0 ),
        .D(bus2ip_rnw_i03_out),
        .Q(sig_Bus2IP_RNW),
        .R(rst));
  FDRE rst_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(SR),
        .Q(rst),
        .R(1'b0));
  LUT4 #(
    .INIT(16'hFB08)) 
    \s_axi_bresp_i[1]_i_1 
       (.I0(sig_IP2Bus_Error),
        .I1(state[1]),
        .I2(state[0]),
        .I3(s_axi_bresp),
        .O(\s_axi_bresp_i[1]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_bresp_i_reg[1] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(\s_axi_bresp_i[1]_i_1_n_0 ),
        .Q(s_axi_bresp),
        .R(rst));
  LUT5 #(
    .INIT(32'h08FF0808)) 
    s_axi_bvalid_i_i_1
       (.I0(IP2Bus_WrAck_reg_0),
        .I1(state[1]),
        .I2(state[0]),
        .I3(s_axi_bready),
        .I4(s_axi_bvalid),
        .O(s_axi_bvalid_i_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    s_axi_bvalid_i_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(s_axi_bvalid_i_i_1_n_0),
        .Q(s_axi_bvalid),
        .R(rst));
  LUT2 #(
    .INIT(4'h2)) 
    \s_axi_rdata_i[31]_i_1 
       (.I0(state[0]),
        .I1(state[1]),
        .O(s_axi_rdata_i));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[0] 
       (.C(s_axi_aclk),
        .CE(s_axi_rdata_i),
        .D(\sig_ip2bus_data_reg[0] [0]),
        .Q(s_axi_rdata[0]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[10] 
       (.C(s_axi_aclk),
        .CE(s_axi_rdata_i),
        .D(\sig_ip2bus_data_reg[0] [10]),
        .Q(s_axi_rdata[10]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[11] 
       (.C(s_axi_aclk),
        .CE(s_axi_rdata_i),
        .D(\sig_ip2bus_data_reg[0] [11]),
        .Q(s_axi_rdata[11]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[12] 
       (.C(s_axi_aclk),
        .CE(s_axi_rdata_i),
        .D(\sig_ip2bus_data_reg[0] [12]),
        .Q(s_axi_rdata[12]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[13] 
       (.C(s_axi_aclk),
        .CE(s_axi_rdata_i),
        .D(\sig_ip2bus_data_reg[0] [13]),
        .Q(s_axi_rdata[13]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[14] 
       (.C(s_axi_aclk),
        .CE(s_axi_rdata_i),
        .D(\sig_ip2bus_data_reg[0] [14]),
        .Q(s_axi_rdata[14]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[15] 
       (.C(s_axi_aclk),
        .CE(s_axi_rdata_i),
        .D(\sig_ip2bus_data_reg[0] [15]),
        .Q(s_axi_rdata[15]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[16] 
       (.C(s_axi_aclk),
        .CE(s_axi_rdata_i),
        .D(\sig_ip2bus_data_reg[0] [16]),
        .Q(s_axi_rdata[16]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[17] 
       (.C(s_axi_aclk),
        .CE(s_axi_rdata_i),
        .D(\sig_ip2bus_data_reg[0] [17]),
        .Q(s_axi_rdata[17]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[18] 
       (.C(s_axi_aclk),
        .CE(s_axi_rdata_i),
        .D(\sig_ip2bus_data_reg[0] [18]),
        .Q(s_axi_rdata[18]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[19] 
       (.C(s_axi_aclk),
        .CE(s_axi_rdata_i),
        .D(\sig_ip2bus_data_reg[0] [19]),
        .Q(s_axi_rdata[19]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[1] 
       (.C(s_axi_aclk),
        .CE(s_axi_rdata_i),
        .D(\sig_ip2bus_data_reg[0] [1]),
        .Q(s_axi_rdata[1]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[20] 
       (.C(s_axi_aclk),
        .CE(s_axi_rdata_i),
        .D(\sig_ip2bus_data_reg[0] [20]),
        .Q(s_axi_rdata[20]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[21] 
       (.C(s_axi_aclk),
        .CE(s_axi_rdata_i),
        .D(\sig_ip2bus_data_reg[0] [21]),
        .Q(s_axi_rdata[21]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[22] 
       (.C(s_axi_aclk),
        .CE(s_axi_rdata_i),
        .D(\sig_ip2bus_data_reg[0] [22]),
        .Q(s_axi_rdata[22]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[23] 
       (.C(s_axi_aclk),
        .CE(s_axi_rdata_i),
        .D(\sig_ip2bus_data_reg[0] [23]),
        .Q(s_axi_rdata[23]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[24] 
       (.C(s_axi_aclk),
        .CE(s_axi_rdata_i),
        .D(\sig_ip2bus_data_reg[0] [24]),
        .Q(s_axi_rdata[24]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[25] 
       (.C(s_axi_aclk),
        .CE(s_axi_rdata_i),
        .D(\sig_ip2bus_data_reg[0] [25]),
        .Q(s_axi_rdata[25]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[26] 
       (.C(s_axi_aclk),
        .CE(s_axi_rdata_i),
        .D(\sig_ip2bus_data_reg[0] [26]),
        .Q(s_axi_rdata[26]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[27] 
       (.C(s_axi_aclk),
        .CE(s_axi_rdata_i),
        .D(\sig_ip2bus_data_reg[0] [27]),
        .Q(s_axi_rdata[27]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[28] 
       (.C(s_axi_aclk),
        .CE(s_axi_rdata_i),
        .D(\sig_ip2bus_data_reg[0] [28]),
        .Q(s_axi_rdata[28]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[29] 
       (.C(s_axi_aclk),
        .CE(s_axi_rdata_i),
        .D(\sig_ip2bus_data_reg[0] [29]),
        .Q(s_axi_rdata[29]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[2] 
       (.C(s_axi_aclk),
        .CE(s_axi_rdata_i),
        .D(\sig_ip2bus_data_reg[0] [2]),
        .Q(s_axi_rdata[2]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[30] 
       (.C(s_axi_aclk),
        .CE(s_axi_rdata_i),
        .D(\sig_ip2bus_data_reg[0] [30]),
        .Q(s_axi_rdata[30]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[31] 
       (.C(s_axi_aclk),
        .CE(s_axi_rdata_i),
        .D(\sig_ip2bus_data_reg[0] [31]),
        .Q(s_axi_rdata[31]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[3] 
       (.C(s_axi_aclk),
        .CE(s_axi_rdata_i),
        .D(\sig_ip2bus_data_reg[0] [3]),
        .Q(s_axi_rdata[3]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[4] 
       (.C(s_axi_aclk),
        .CE(s_axi_rdata_i),
        .D(\sig_ip2bus_data_reg[0] [4]),
        .Q(s_axi_rdata[4]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[5] 
       (.C(s_axi_aclk),
        .CE(s_axi_rdata_i),
        .D(\sig_ip2bus_data_reg[0] [5]),
        .Q(s_axi_rdata[5]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[6] 
       (.C(s_axi_aclk),
        .CE(s_axi_rdata_i),
        .D(\sig_ip2bus_data_reg[0] [6]),
        .Q(s_axi_rdata[6]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[7] 
       (.C(s_axi_aclk),
        .CE(s_axi_rdata_i),
        .D(\sig_ip2bus_data_reg[0] [7]),
        .Q(s_axi_rdata[7]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[8] 
       (.C(s_axi_aclk),
        .CE(s_axi_rdata_i),
        .D(\sig_ip2bus_data_reg[0] [8]),
        .Q(s_axi_rdata[8]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rdata_i_reg[9] 
       (.C(s_axi_aclk),
        .CE(s_axi_rdata_i),
        .D(\sig_ip2bus_data_reg[0] [9]),
        .Q(s_axi_rdata[9]),
        .R(rst));
  FDRE #(
    .INIT(1'b0)) 
    \s_axi_rresp_i_reg[1] 
       (.C(s_axi_aclk),
        .CE(s_axi_rdata_i),
        .D(sig_IP2Bus_Error),
        .Q(s_axi_rresp),
        .R(rst));
  LUT5 #(
    .INIT(32'h08FF0808)) 
    s_axi_rvalid_i_i_1
       (.I0(IP2Bus_RdAck_reg_0),
        .I1(state[0]),
        .I2(state[1]),
        .I3(s_axi_rready),
        .I4(s_axi_rvalid),
        .O(s_axi_rvalid_i_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    s_axi_rvalid_i_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(s_axi_rvalid_i_i_1_n_0),
        .Q(s_axi_rvalid),
        .R(rst));
  (* SOFT_HLUTNM = "soft_lutpair106" *) 
  LUT5 #(
    .INIT(32'h000F0008)) 
    start2_i_1
       (.I0(s_axi_awvalid),
        .I1(s_axi_wvalid),
        .I2(state[1]),
        .I3(state[0]),
        .I4(s_axi_arvalid),
        .O(start2_i_1_n_0));
  FDRE start2_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(start2_i_1_n_0),
        .Q(start2),
        .R(rst));
  LUT5 #(
    .INIT(32'h0FFFACAC)) 
    \state[0]_i_1 
       (.I0(IP2Bus_WrAck_reg_0),
        .I1(s_axi_arvalid),
        .I2(state[1]),
        .I3(\state[1]_i_2_n_0 ),
        .I4(state[0]),
        .O(\state[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h3AF03AFF3AF03AF0)) 
    \state[1]_i_1 
       (.I0(IP2Bus_RdAck_reg_0),
        .I1(\state[1]_i_2_n_0 ),
        .I2(state[1]),
        .I3(state[0]),
        .I4(s_axi_arvalid),
        .I5(\state[1]_i_3_n_0 ),
        .O(\state[1]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hF888)) 
    \state[1]_i_2 
       (.I0(s_axi_rready),
        .I1(s_axi_rvalid),
        .I2(s_axi_bready),
        .I3(s_axi_bvalid),
        .O(\state[1]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair106" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \state[1]_i_3 
       (.I0(s_axi_awvalid),
        .I1(s_axi_wvalid),
        .O(\state[1]_i_3_n_0 ));
  FDRE \state_reg[0] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(\state[0]_i_1_n_0 ),
        .Q(state[0]),
        .R(rst));
  FDRE \state_reg[1] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(\state[1]_i_1_n_0 ),
        .Q(state[1]),
        .R(rst));
endmodule

(* ORIG_REF_NAME = "sync_fifo_fg" *) 
module axi_fifo_mm_s_0_sync_fifo_fg
   (out,
    \gcc0.gc0.count_d1_reg[6] ,
    SR,
    \gpr1.dout_i_reg[0] ,
    \gpr1.dout_i_reg[1] ,
    \gpr1.dout_i_reg[2] ,
    Q,
    \gpr1.dout_i_reg[0]_0 ,
    \gpr1.dout_i_reg[0]_1 ,
    \gpr1.dout_i_reg[1]_0 ,
    \gpr1.dout_i_reg[2]_0 ,
    \gpr1.dout_i_reg[3] ,
    \gpr1.dout_i_reg[4] ,
    \gpr1.dout_i_reg[5] ,
    \gpr1.dout_i_reg[3]_0 ,
    \gpr1.dout_i_reg[4]_0 ,
    \gpr1.dout_i_reg[5]_0 ,
    \gpr1.dout_i_reg[6] ,
    \gpr1.dout_i_reg[7] ,
    \gpr1.dout_i_reg[8] ,
    \gpr1.dout_i_reg[6]_0 ,
    \gpr1.dout_i_reg[7]_0 ,
    \gpr1.dout_i_reg[8]_0 ,
    \gpr1.dout_i_reg[9] ,
    \gpr1.dout_i_reg[10] ,
    \gpr1.dout_i_reg[11] ,
    \gpr1.dout_i_reg[9]_0 ,
    \gpr1.dout_i_reg[10]_0 ,
    \gpr1.dout_i_reg[11]_0 ,
    \gpr1.dout_i_reg[12] ,
    \gpr1.dout_i_reg[13] ,
    \gpr1.dout_i_reg[14] ,
    \gpr1.dout_i_reg[12]_0 ,
    \gpr1.dout_i_reg[13]_0 ,
    \gpr1.dout_i_reg[14]_0 ,
    \gpr1.dout_i_reg[15] ,
    \gpr1.dout_i_reg[16] ,
    \gpr1.dout_i_reg[17] ,
    \gpr1.dout_i_reg[15]_0 ,
    \gpr1.dout_i_reg[16]_0 ,
    \gpr1.dout_i_reg[17]_0 ,
    \gpr1.dout_i_reg[18] ,
    \gpr1.dout_i_reg[19] ,
    \gpr1.dout_i_reg[20] ,
    \gpr1.dout_i_reg[18]_0 ,
    \gpr1.dout_i_reg[19]_0 ,
    \gpr1.dout_i_reg[20]_0 ,
    \gpr1.dout_i_reg[21] ,
    \gpr1.dout_i_reg[21]_0 ,
    p_13_in,
    \sig_ip2bus_data_reg[10] ,
    s_axi_aclk,
    fg_rxd_wr_length,
    ram_full_fb_i_reg,
    rx_fg_len_empty_d1,
    ram_full_i_reg,
    axi_str_rxd_tvalid,
    axi_str_rxd_tlast,
    rx_len_wr_en,
    sig_rd_rlen_reg,
    s_axi_aresetn,
    sig_str_rst_reg,
    sig_rx_channel_reset_reg,
    D);
  output out;
  output \gcc0.gc0.count_d1_reg[6] ;
  output [0:0]SR;
  output \gpr1.dout_i_reg[0] ;
  output \gpr1.dout_i_reg[1] ;
  output \gpr1.dout_i_reg[2] ;
  output [0:0]Q;
  output [0:0]\gpr1.dout_i_reg[0]_0 ;
  output \gpr1.dout_i_reg[0]_1 ;
  output \gpr1.dout_i_reg[1]_0 ;
  output \gpr1.dout_i_reg[2]_0 ;
  output \gpr1.dout_i_reg[3] ;
  output \gpr1.dout_i_reg[4] ;
  output \gpr1.dout_i_reg[5] ;
  output \gpr1.dout_i_reg[3]_0 ;
  output \gpr1.dout_i_reg[4]_0 ;
  output \gpr1.dout_i_reg[5]_0 ;
  output \gpr1.dout_i_reg[6] ;
  output \gpr1.dout_i_reg[7] ;
  output \gpr1.dout_i_reg[8] ;
  output \gpr1.dout_i_reg[6]_0 ;
  output \gpr1.dout_i_reg[7]_0 ;
  output \gpr1.dout_i_reg[8]_0 ;
  output \gpr1.dout_i_reg[9] ;
  output \gpr1.dout_i_reg[10] ;
  output \gpr1.dout_i_reg[11] ;
  output \gpr1.dout_i_reg[9]_0 ;
  output \gpr1.dout_i_reg[10]_0 ;
  output \gpr1.dout_i_reg[11]_0 ;
  output \gpr1.dout_i_reg[12] ;
  output \gpr1.dout_i_reg[13] ;
  output \gpr1.dout_i_reg[14] ;
  output \gpr1.dout_i_reg[12]_0 ;
  output \gpr1.dout_i_reg[13]_0 ;
  output \gpr1.dout_i_reg[14]_0 ;
  output \gpr1.dout_i_reg[15] ;
  output \gpr1.dout_i_reg[16] ;
  output \gpr1.dout_i_reg[17] ;
  output \gpr1.dout_i_reg[15]_0 ;
  output \gpr1.dout_i_reg[16]_0 ;
  output \gpr1.dout_i_reg[17]_0 ;
  output \gpr1.dout_i_reg[18] ;
  output \gpr1.dout_i_reg[19] ;
  output \gpr1.dout_i_reg[20] ;
  output \gpr1.dout_i_reg[18]_0 ;
  output \gpr1.dout_i_reg[19]_0 ;
  output \gpr1.dout_i_reg[20]_0 ;
  output \gpr1.dout_i_reg[21] ;
  output \gpr1.dout_i_reg[21]_0 ;
  output p_13_in;
  output [21:0]\sig_ip2bus_data_reg[10] ;
  input s_axi_aclk;
  input [20:0]fg_rxd_wr_length;
  input ram_full_fb_i_reg;
  input rx_fg_len_empty_d1;
  input ram_full_i_reg;
  input axi_str_rxd_tvalid;
  input axi_str_rxd_tlast;
  input rx_len_wr_en;
  input sig_rd_rlen_reg;
  input s_axi_aresetn;
  input sig_str_rst_reg;
  input sig_rx_channel_reset_reg;
  input [21:0]D;

  wire [21:0]D;
  wire [0:0]Q;
  wire [0:0]SR;
  wire axi_str_rxd_tlast;
  wire axi_str_rxd_tvalid;
  wire [20:0]fg_rxd_wr_length;
  wire \gcc0.gc0.count_d1_reg[6] ;
  wire \gpr1.dout_i_reg[0] ;
  wire [0:0]\gpr1.dout_i_reg[0]_0 ;
  wire \gpr1.dout_i_reg[0]_1 ;
  wire \gpr1.dout_i_reg[10] ;
  wire \gpr1.dout_i_reg[10]_0 ;
  wire \gpr1.dout_i_reg[11] ;
  wire \gpr1.dout_i_reg[11]_0 ;
  wire \gpr1.dout_i_reg[12] ;
  wire \gpr1.dout_i_reg[12]_0 ;
  wire \gpr1.dout_i_reg[13] ;
  wire \gpr1.dout_i_reg[13]_0 ;
  wire \gpr1.dout_i_reg[14] ;
  wire \gpr1.dout_i_reg[14]_0 ;
  wire \gpr1.dout_i_reg[15] ;
  wire \gpr1.dout_i_reg[15]_0 ;
  wire \gpr1.dout_i_reg[16] ;
  wire \gpr1.dout_i_reg[16]_0 ;
  wire \gpr1.dout_i_reg[17] ;
  wire \gpr1.dout_i_reg[17]_0 ;
  wire \gpr1.dout_i_reg[18] ;
  wire \gpr1.dout_i_reg[18]_0 ;
  wire \gpr1.dout_i_reg[19] ;
  wire \gpr1.dout_i_reg[19]_0 ;
  wire \gpr1.dout_i_reg[1] ;
  wire \gpr1.dout_i_reg[1]_0 ;
  wire \gpr1.dout_i_reg[20] ;
  wire \gpr1.dout_i_reg[20]_0 ;
  wire \gpr1.dout_i_reg[21] ;
  wire \gpr1.dout_i_reg[21]_0 ;
  wire \gpr1.dout_i_reg[2] ;
  wire \gpr1.dout_i_reg[2]_0 ;
  wire \gpr1.dout_i_reg[3] ;
  wire \gpr1.dout_i_reg[3]_0 ;
  wire \gpr1.dout_i_reg[4] ;
  wire \gpr1.dout_i_reg[4]_0 ;
  wire \gpr1.dout_i_reg[5] ;
  wire \gpr1.dout_i_reg[5]_0 ;
  wire \gpr1.dout_i_reg[6] ;
  wire \gpr1.dout_i_reg[6]_0 ;
  wire \gpr1.dout_i_reg[7] ;
  wire \gpr1.dout_i_reg[7]_0 ;
  wire \gpr1.dout_i_reg[8] ;
  wire \gpr1.dout_i_reg[8]_0 ;
  wire \gpr1.dout_i_reg[9] ;
  wire \gpr1.dout_i_reg[9]_0 ;
  wire out;
  wire p_13_in;
  wire ram_full_fb_i_reg;
  wire ram_full_i_reg;
  wire rx_fg_len_empty_d1;
  wire rx_len_wr_en;
  wire s_axi_aclk;
  wire s_axi_aresetn;
  wire [21:0]\sig_ip2bus_data_reg[10] ;
  wire sig_rd_rlen_reg;
  wire sig_rx_channel_reset_reg;
  wire sig_str_rst_reg;

  axi_fifo_mm_s_0_fifo_generator_v13_1_3 \FAMILY_SUPPORTED.I_SYNC_FIFO_BRAM 
       (.D(D),
        .Q(\gpr1.dout_i_reg[0]_0 ),
        .axi_str_rxd_tlast(axi_str_rxd_tlast),
        .axi_str_rxd_tvalid(axi_str_rxd_tvalid),
        .fg_rxd_wr_length(fg_rxd_wr_length),
        .\gcc0.gc0.count_d1_reg[6] (\gcc0.gc0.count_d1_reg[6] ),
        .\gpr1.dout_i_reg[0] (SR),
        .\gpr1.dout_i_reg[0]_0 (\gpr1.dout_i_reg[0] ),
        .\gpr1.dout_i_reg[0]_1 (\gpr1.dout_i_reg[0]_1 ),
        .\gpr1.dout_i_reg[10] (\gpr1.dout_i_reg[10] ),
        .\gpr1.dout_i_reg[10]_0 (\gpr1.dout_i_reg[10]_0 ),
        .\gpr1.dout_i_reg[11] (\gpr1.dout_i_reg[11] ),
        .\gpr1.dout_i_reg[11]_0 (\gpr1.dout_i_reg[11]_0 ),
        .\gpr1.dout_i_reg[12] (\gpr1.dout_i_reg[12] ),
        .\gpr1.dout_i_reg[12]_0 (\gpr1.dout_i_reg[12]_0 ),
        .\gpr1.dout_i_reg[13] (\gpr1.dout_i_reg[13] ),
        .\gpr1.dout_i_reg[13]_0 (\gpr1.dout_i_reg[13]_0 ),
        .\gpr1.dout_i_reg[14] (\gpr1.dout_i_reg[14] ),
        .\gpr1.dout_i_reg[14]_0 (\gpr1.dout_i_reg[14]_0 ),
        .\gpr1.dout_i_reg[15] (\gpr1.dout_i_reg[15] ),
        .\gpr1.dout_i_reg[15]_0 (\gpr1.dout_i_reg[15]_0 ),
        .\gpr1.dout_i_reg[16] (\gpr1.dout_i_reg[16] ),
        .\gpr1.dout_i_reg[16]_0 (\gpr1.dout_i_reg[16]_0 ),
        .\gpr1.dout_i_reg[17] (\gpr1.dout_i_reg[17] ),
        .\gpr1.dout_i_reg[17]_0 (\gpr1.dout_i_reg[17]_0 ),
        .\gpr1.dout_i_reg[18] (\gpr1.dout_i_reg[18] ),
        .\gpr1.dout_i_reg[18]_0 (\gpr1.dout_i_reg[18]_0 ),
        .\gpr1.dout_i_reg[19] (\gpr1.dout_i_reg[19] ),
        .\gpr1.dout_i_reg[19]_0 (\gpr1.dout_i_reg[19]_0 ),
        .\gpr1.dout_i_reg[1] (\gpr1.dout_i_reg[1] ),
        .\gpr1.dout_i_reg[1]_0 (\gpr1.dout_i_reg[1]_0 ),
        .\gpr1.dout_i_reg[20] (\gpr1.dout_i_reg[20] ),
        .\gpr1.dout_i_reg[20]_0 (\gpr1.dout_i_reg[20]_0 ),
        .\gpr1.dout_i_reg[21] (\gpr1.dout_i_reg[21] ),
        .\gpr1.dout_i_reg[21]_0 (\gpr1.dout_i_reg[21]_0 ),
        .\gpr1.dout_i_reg[21]_1 (Q),
        .\gpr1.dout_i_reg[2] (\gpr1.dout_i_reg[2] ),
        .\gpr1.dout_i_reg[2]_0 (\gpr1.dout_i_reg[2]_0 ),
        .\gpr1.dout_i_reg[3] (\gpr1.dout_i_reg[3] ),
        .\gpr1.dout_i_reg[3]_0 (\gpr1.dout_i_reg[3]_0 ),
        .\gpr1.dout_i_reg[4] (\gpr1.dout_i_reg[4] ),
        .\gpr1.dout_i_reg[4]_0 (\gpr1.dout_i_reg[4]_0 ),
        .\gpr1.dout_i_reg[5] (\gpr1.dout_i_reg[5] ),
        .\gpr1.dout_i_reg[5]_0 (\gpr1.dout_i_reg[5]_0 ),
        .\gpr1.dout_i_reg[6] (\gpr1.dout_i_reg[6] ),
        .\gpr1.dout_i_reg[6]_0 (\gpr1.dout_i_reg[6]_0 ),
        .\gpr1.dout_i_reg[7] (\gpr1.dout_i_reg[7] ),
        .\gpr1.dout_i_reg[7]_0 (\gpr1.dout_i_reg[7]_0 ),
        .\gpr1.dout_i_reg[8] (\gpr1.dout_i_reg[8] ),
        .\gpr1.dout_i_reg[8]_0 (\gpr1.dout_i_reg[8]_0 ),
        .\gpr1.dout_i_reg[9] (\gpr1.dout_i_reg[9] ),
        .\gpr1.dout_i_reg[9]_0 (\gpr1.dout_i_reg[9]_0 ),
        .out(out),
        .p_13_in(p_13_in),
        .ram_full_fb_i_reg(ram_full_fb_i_reg),
        .ram_full_i_reg(ram_full_i_reg),
        .rx_fg_len_empty_d1(rx_fg_len_empty_d1),
        .rx_len_wr_en(rx_len_wr_en),
        .s_axi_aclk(s_axi_aclk),
        .s_axi_aresetn(s_axi_aresetn),
        .\sig_ip2bus_data_reg[10] (\sig_ip2bus_data_reg[10] ),
        .sig_rd_rlen_reg(sig_rd_rlen_reg),
        .sig_rx_channel_reset_reg(sig_rx_channel_reset_reg),
        .sig_str_rst_reg(sig_str_rst_reg));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_generic_cstr" *) 
module axi_fifo_mm_s_0_blk_mem_gen_generic_cstr
   (D,
    s_axi_aclk,
    ram_rd_en_i,
    WEBWE,
    \gc0.count_d1_reg[8] ,
    \gcc0.gc1.gsym.count_d2_reg[8] ,
    Q,
    DIADI);
  output [32:0]D;
  input s_axi_aclk;
  input ram_rd_en_i;
  input [0:0]WEBWE;
  input [8:0]\gc0.count_d1_reg[8] ;
  input [8:0]\gcc0.gc1.gsym.count_d2_reg[8] ;
  input [31:0]Q;
  input [4:0]DIADI;

  wire [32:0]D;
  wire [4:0]DIADI;
  wire [31:0]Q;
  wire [0:0]WEBWE;
  wire [8:0]\gc0.count_d1_reg[8] ;
  wire [8:0]\gcc0.gc1.gsym.count_d2_reg[8] ;
  wire ram_rd_en_i;
  wire s_axi_aclk;

  axi_fifo_mm_s_0_blk_mem_gen_prim_width \ramloop[0].ram.r 
       (.D(D),
        .DIADI(DIADI),
        .Q(Q),
        .WEBWE(WEBWE),
        .\gc0.count_d1_reg[8] (\gc0.count_d1_reg[8] ),
        .\gcc0.gc1.gsym.count_d2_reg[8] (\gcc0.gc1.gsym.count_d2_reg[8] ),
        .ram_rd_en_i(ram_rd_en_i),
        .s_axi_aclk(s_axi_aclk));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_generic_cstr" *) 
module axi_fifo_mm_s_0_blk_mem_gen_generic_cstr_10
   (D,
    s_axi_aclk,
    ram_rd_en_i,
    WEBWE,
    \gc0.count_d1_reg[8] ,
    \gcc0.gc0.count_d1_reg[8] ,
    axi_str_rxd_tdata,
    axi_str_rxd_tlast);
  output [32:0]D;
  input s_axi_aclk;
  input ram_rd_en_i;
  input [0:0]WEBWE;
  input [8:0]\gc0.count_d1_reg[8] ;
  input [8:0]\gcc0.gc0.count_d1_reg[8] ;
  input [31:0]axi_str_rxd_tdata;
  input axi_str_rxd_tlast;

  wire [32:0]D;
  wire [0:0]WEBWE;
  wire [31:0]axi_str_rxd_tdata;
  wire axi_str_rxd_tlast;
  wire [8:0]\gc0.count_d1_reg[8] ;
  wire [8:0]\gcc0.gc0.count_d1_reg[8] ;
  wire ram_rd_en_i;
  wire s_axi_aclk;

  axi_fifo_mm_s_0_blk_mem_gen_prim_width_11 \ramloop[0].ram.r 
       (.D(D),
        .WEBWE(WEBWE),
        .axi_str_rxd_tdata(axi_str_rxd_tdata),
        .axi_str_rxd_tlast(axi_str_rxd_tlast),
        .\gc0.count_d1_reg[8] (\gc0.count_d1_reg[8] ),
        .\gcc0.gc0.count_d1_reg[8] (\gcc0.gc0.count_d1_reg[8] ),
        .ram_rd_en_i(ram_rd_en_i),
        .s_axi_aclk(s_axi_aclk));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_width" *) 
module axi_fifo_mm_s_0_blk_mem_gen_prim_width
   (D,
    s_axi_aclk,
    ram_rd_en_i,
    WEBWE,
    \gc0.count_d1_reg[8] ,
    \gcc0.gc1.gsym.count_d2_reg[8] ,
    Q,
    DIADI);
  output [32:0]D;
  input s_axi_aclk;
  input ram_rd_en_i;
  input [0:0]WEBWE;
  input [8:0]\gc0.count_d1_reg[8] ;
  input [8:0]\gcc0.gc1.gsym.count_d2_reg[8] ;
  input [31:0]Q;
  input [4:0]DIADI;

  wire [32:0]D;
  wire [4:0]DIADI;
  wire [31:0]Q;
  wire [0:0]WEBWE;
  wire [8:0]\gc0.count_d1_reg[8] ;
  wire [8:0]\gcc0.gc1.gsym.count_d2_reg[8] ;
  wire ram_rd_en_i;
  wire s_axi_aclk;

  axi_fifo_mm_s_0_blk_mem_gen_prim_wrapper \prim_noinit.ram 
       (.D(D),
        .DIADI(DIADI),
        .Q(Q),
        .WEBWE(WEBWE),
        .\gc0.count_d1_reg[8] (\gc0.count_d1_reg[8] ),
        .\gcc0.gc1.gsym.count_d2_reg[8] (\gcc0.gc1.gsym.count_d2_reg[8] ),
        .ram_rd_en_i(ram_rd_en_i),
        .s_axi_aclk(s_axi_aclk));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_width" *) 
module axi_fifo_mm_s_0_blk_mem_gen_prim_width_11
   (D,
    s_axi_aclk,
    ram_rd_en_i,
    WEBWE,
    \gc0.count_d1_reg[8] ,
    \gcc0.gc0.count_d1_reg[8] ,
    axi_str_rxd_tdata,
    axi_str_rxd_tlast);
  output [32:0]D;
  input s_axi_aclk;
  input ram_rd_en_i;
  input [0:0]WEBWE;
  input [8:0]\gc0.count_d1_reg[8] ;
  input [8:0]\gcc0.gc0.count_d1_reg[8] ;
  input [31:0]axi_str_rxd_tdata;
  input axi_str_rxd_tlast;

  wire [32:0]D;
  wire [0:0]WEBWE;
  wire [31:0]axi_str_rxd_tdata;
  wire axi_str_rxd_tlast;
  wire [8:0]\gc0.count_d1_reg[8] ;
  wire [8:0]\gcc0.gc0.count_d1_reg[8] ;
  wire ram_rd_en_i;
  wire s_axi_aclk;

  axi_fifo_mm_s_0_blk_mem_gen_prim_wrapper_12 \prim_noinit.ram 
       (.D(D),
        .WEBWE(WEBWE),
        .axi_str_rxd_tdata(axi_str_rxd_tdata),
        .axi_str_rxd_tlast(axi_str_rxd_tlast),
        .\gc0.count_d1_reg[8] (\gc0.count_d1_reg[8] ),
        .\gcc0.gc0.count_d1_reg[8] (\gcc0.gc0.count_d1_reg[8] ),
        .ram_rd_en_i(ram_rd_en_i),
        .s_axi_aclk(s_axi_aclk));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_wrapper" *) 
module axi_fifo_mm_s_0_blk_mem_gen_prim_wrapper
   (D,
    s_axi_aclk,
    ram_rd_en_i,
    WEBWE,
    \gc0.count_d1_reg[8] ,
    \gcc0.gc1.gsym.count_d2_reg[8] ,
    Q,
    DIADI);
  output [32:0]D;
  input s_axi_aclk;
  input ram_rd_en_i;
  input [0:0]WEBWE;
  input [8:0]\gc0.count_d1_reg[8] ;
  input [8:0]\gcc0.gc1.gsym.count_d2_reg[8] ;
  input [31:0]Q;
  input [4:0]DIADI;

  wire [32:0]D;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_21 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_22 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_23 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_29 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_30 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_31 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_37 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_38 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_39 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_45 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_46 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_53 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_54 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_55 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_61 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_62 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_63 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_69 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_70 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_71 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_77 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_78 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_79 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_85 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_86 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_87 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_88 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_89 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_90 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_91 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_92 ;
  wire [4:0]DIADI;
  wire [31:0]Q;
  wire [0:0]WEBWE;
  wire [8:1]doutb;
  wire [8:0]\gc0.count_d1_reg[8] ;
  wire [8:0]\gcc0.gc1.gsym.count_d2_reg[8] ;
  wire ram_rd_en_i;
  wire s_axi_aclk;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_CASCADEOUTA_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_CASCADEOUTB_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_DBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_SBITERR_UNCONNECTED ;
  wire [7:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_RDADDRECC_UNCONNECTED ;

  (* CLOCK_DOMAINS = "COMMON" *) 
  (* box_type = "PRIMITIVE" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_10(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_11(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_12(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_13(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_14(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_15(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_16(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_17(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_18(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_19(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_20(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_21(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_22(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_23(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_24(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_25(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_26(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_27(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_28(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_29(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_30(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_31(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_32(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_33(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_34(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_35(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_36(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_37(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_38(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_39(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_40(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_41(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_42(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_43(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_44(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_45(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_46(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_47(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_48(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_49(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_50(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_51(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_52(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_53(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_54(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_55(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_56(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_57(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_58(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_59(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_60(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_61(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_62(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_63(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_64(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_65(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_66(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_67(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_68(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_69(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_70(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_71(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_72(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_73(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_74(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_75(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_76(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_77(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_78(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_79(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("SDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(72),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("REGCE"),
    .RSTREG_PRIORITY_B("REGCE"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("READ_FIRST"),
    .WRITE_WIDTH_A(0),
    .WRITE_WIDTH_B(72)) 
    \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram 
       (.ADDRARDADDR({1'b1,\gc0.count_d1_reg[8] ,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,\gcc0.gc1.gsym.count_d2_reg[8] ,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b0),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_CASCADEOUTA_UNCONNECTED ),
        .CASCADEOUTB(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_CASCADEOUTB_UNCONNECTED ),
        .CLKARDCLK(s_axi_aclk),
        .CLKBWRCLK(s_axi_aclk),
        .DBITERR(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_DBITERR_UNCONNECTED ),
        .DIADI({1'b0,1'b0,1'b0,Q[11:7],1'b0,1'b0,1'b0,Q[6:2],1'b0,1'b0,1'b0,Q[1:0],DIADI[4:2],1'b0,1'b0,DIADI[1],1'b0,1'b0,1'b0,1'b0,DIADI[0]}),
        .DIBDI({1'b0,1'b0,1'b0,Q[31:27],1'b0,1'b0,1'b0,Q[26:22],1'b0,1'b0,1'b0,Q[21:17],1'b0,1'b0,1'b0,Q[16:12]}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b0,1'b0,1'b0,1'b0}),
        .DOADO({\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_21 ,\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_22 ,\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_23 ,D[12:8],\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_29 ,\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_30 ,\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_31 ,D[7:3],\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_37 ,\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_38 ,\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_39 ,D[2:1],doutb[8:6],\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_45 ,\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_46 ,doutb[5:1],D[0]}),
        .DOBDO({\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_53 ,\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_54 ,\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_55 ,D[32:28],\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_61 ,\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_62 ,\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_63 ,D[27:23],\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_69 ,\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_70 ,\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_71 ,D[22:18],\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_77 ,\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_78 ,\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_79 ,D[17:13]}),
        .DOPADOP({\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_85 ,\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_86 ,\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_87 ,\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_88 }),
        .DOPBDOP({\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_89 ,\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_90 ,\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_91 ,\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_92 }),
        .ECCPARITY(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_ECCPARITY_UNCONNECTED [7:0]),
        .ENARDEN(ram_rd_en_i),
        .ENBWREN(WEBWE),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_SBITERR_UNCONNECTED ),
        .WEA({1'b0,1'b0,1'b0,1'b0}),
        .WEBWE({WEBWE,WEBWE,WEBWE,WEBWE,WEBWE,WEBWE,WEBWE,WEBWE}));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_wrapper" *) 
module axi_fifo_mm_s_0_blk_mem_gen_prim_wrapper_12
   (D,
    s_axi_aclk,
    ram_rd_en_i,
    WEBWE,
    \gc0.count_d1_reg[8] ,
    \gcc0.gc0.count_d1_reg[8] ,
    axi_str_rxd_tdata,
    axi_str_rxd_tlast);
  output [32:0]D;
  input s_axi_aclk;
  input ram_rd_en_i;
  input [0:0]WEBWE;
  input [8:0]\gc0.count_d1_reg[8] ;
  input [8:0]\gcc0.gc0.count_d1_reg[8] ;
  input [31:0]axi_str_rxd_tdata;
  input axi_str_rxd_tlast;

  wire [32:0]D;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_21 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_22 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_23 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_29 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_30 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_31 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_37 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_38 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_39 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_45 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_46 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_53 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_54 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_55 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_61 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_62 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_63 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_69 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_70 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_71 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_77 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_78 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_79 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_85 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_86 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_87 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_88 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_89 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_90 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_91 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_92 ;
  wire [0:0]WEBWE;
  wire [31:0]axi_str_rxd_tdata;
  wire axi_str_rxd_tlast;
  wire [8:1]doutb;
  wire [8:0]\gc0.count_d1_reg[8] ;
  wire [8:0]\gcc0.gc0.count_d1_reg[8] ;
  wire ram_rd_en_i;
  wire s_axi_aclk;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_CASCADEOUTA_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_CASCADEOUTB_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_DBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_SBITERR_UNCONNECTED ;
  wire [7:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_RDADDRECC_UNCONNECTED ;

  (* CLOCK_DOMAINS = "COMMON" *) 
  (* box_type = "PRIMITIVE" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_10(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_11(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_12(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_13(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_14(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_15(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_16(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_17(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_18(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_19(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_20(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_21(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_22(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_23(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_24(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_25(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_26(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_27(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_28(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_29(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_30(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_31(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_32(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_33(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_34(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_35(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_36(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_37(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_38(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_39(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_40(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_41(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_42(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_43(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_44(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_45(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_46(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_47(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_48(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_49(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_50(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_51(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_52(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_53(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_54(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_55(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_56(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_57(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_58(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_59(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_60(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_61(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_62(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_63(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_64(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_65(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_66(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_67(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_68(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_69(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_70(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_71(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_72(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_73(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_74(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_75(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_76(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_77(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_78(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_79(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("SDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(72),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("REGCE"),
    .RSTREG_PRIORITY_B("REGCE"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("READ_FIRST"),
    .WRITE_WIDTH_A(0),
    .WRITE_WIDTH_B(72)) 
    \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram 
       (.ADDRARDADDR({1'b1,\gc0.count_d1_reg[8] ,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,\gcc0.gc0.count_d1_reg[8] ,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b0),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_CASCADEOUTA_UNCONNECTED ),
        .CASCADEOUTB(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_CASCADEOUTB_UNCONNECTED ),
        .CLKARDCLK(s_axi_aclk),
        .CLKBWRCLK(s_axi_aclk),
        .DBITERR(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_DBITERR_UNCONNECTED ),
        .DIADI({1'b0,1'b0,1'b0,axi_str_rxd_tdata[11:7],1'b0,1'b0,1'b0,axi_str_rxd_tdata[6:2],1'b0,1'b0,1'b0,axi_str_rxd_tdata[1:0],1'b1,1'b1,1'b1,1'b0,1'b0,1'b1,1'b0,1'b0,1'b0,1'b0,axi_str_rxd_tlast}),
        .DIBDI({1'b0,1'b0,1'b0,axi_str_rxd_tdata[31:27],1'b0,1'b0,1'b0,axi_str_rxd_tdata[26:22],1'b0,1'b0,1'b0,axi_str_rxd_tdata[21:17],1'b0,1'b0,1'b0,axi_str_rxd_tdata[16:12]}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b0,1'b0,1'b0,1'b0}),
        .DOADO({\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_21 ,\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_22 ,\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_23 ,D[12:8],\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_29 ,\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_30 ,\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_31 ,D[7:3],\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_37 ,\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_38 ,\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_39 ,D[2:1],doutb[8:6],\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_45 ,\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_46 ,doutb[5:1],D[0]}),
        .DOBDO({\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_53 ,\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_54 ,\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_55 ,D[32:28],\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_61 ,\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_62 ,\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_63 ,D[27:23],\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_69 ,\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_70 ,\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_71 ,D[22:18],\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_77 ,\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_78 ,\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_79 ,D[17:13]}),
        .DOPADOP({\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_85 ,\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_86 ,\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_87 ,\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_88 }),
        .DOPBDOP({\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_89 ,\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_90 ,\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_91 ,\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_n_92 }),
        .ECCPARITY(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_ECCPARITY_UNCONNECTED [7:0]),
        .ENARDEN(ram_rd_en_i),
        .ENBWREN(WEBWE),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_SBITERR_UNCONNECTED ),
        .WEA({1'b0,1'b0,1'b0,1'b0}),
        .WEBWE({WEBWE,WEBWE,WEBWE,WEBWE,WEBWE,WEBWE,WEBWE,WEBWE}));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_top" *) 
module axi_fifo_mm_s_0_blk_mem_gen_top
   (D,
    s_axi_aclk,
    ram_rd_en_i,
    WEBWE,
    \gc0.count_d1_reg[8] ,
    \gcc0.gc1.gsym.count_d2_reg[8] ,
    Q,
    DIADI);
  output [32:0]D;
  input s_axi_aclk;
  input ram_rd_en_i;
  input [0:0]WEBWE;
  input [8:0]\gc0.count_d1_reg[8] ;
  input [8:0]\gcc0.gc1.gsym.count_d2_reg[8] ;
  input [31:0]Q;
  input [4:0]DIADI;

  wire [32:0]D;
  wire [4:0]DIADI;
  wire [31:0]Q;
  wire [0:0]WEBWE;
  wire [8:0]\gc0.count_d1_reg[8] ;
  wire [8:0]\gcc0.gc1.gsym.count_d2_reg[8] ;
  wire ram_rd_en_i;
  wire s_axi_aclk;

  axi_fifo_mm_s_0_blk_mem_gen_generic_cstr \valid.cstr 
       (.D(D),
        .DIADI(DIADI),
        .Q(Q),
        .WEBWE(WEBWE),
        .\gc0.count_d1_reg[8] (\gc0.count_d1_reg[8] ),
        .\gcc0.gc1.gsym.count_d2_reg[8] (\gcc0.gc1.gsym.count_d2_reg[8] ),
        .ram_rd_en_i(ram_rd_en_i),
        .s_axi_aclk(s_axi_aclk));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_top" *) 
module axi_fifo_mm_s_0_blk_mem_gen_top_9
   (D,
    s_axi_aclk,
    ram_rd_en_i,
    WEBWE,
    \gc0.count_d1_reg[8] ,
    \gcc0.gc0.count_d1_reg[8] ,
    axi_str_rxd_tdata,
    axi_str_rxd_tlast);
  output [32:0]D;
  input s_axi_aclk;
  input ram_rd_en_i;
  input [0:0]WEBWE;
  input [8:0]\gc0.count_d1_reg[8] ;
  input [8:0]\gcc0.gc0.count_d1_reg[8] ;
  input [31:0]axi_str_rxd_tdata;
  input axi_str_rxd_tlast;

  wire [32:0]D;
  wire [0:0]WEBWE;
  wire [31:0]axi_str_rxd_tdata;
  wire axi_str_rxd_tlast;
  wire [8:0]\gc0.count_d1_reg[8] ;
  wire [8:0]\gcc0.gc0.count_d1_reg[8] ;
  wire ram_rd_en_i;
  wire s_axi_aclk;

  axi_fifo_mm_s_0_blk_mem_gen_generic_cstr_10 \valid.cstr 
       (.D(D),
        .WEBWE(WEBWE),
        .axi_str_rxd_tdata(axi_str_rxd_tdata),
        .axi_str_rxd_tlast(axi_str_rxd_tlast),
        .\gc0.count_d1_reg[8] (\gc0.count_d1_reg[8] ),
        .\gcc0.gc0.count_d1_reg[8] (\gcc0.gc0.count_d1_reg[8] ),
        .ram_rd_en_i(ram_rd_en_i),
        .s_axi_aclk(s_axi_aclk));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_v8_3_5" *) 
module axi_fifo_mm_s_0_blk_mem_gen_v8_3_5
   (D,
    s_axi_aclk,
    ram_rd_en_i,
    WEBWE,
    \gc0.count_d1_reg[8] ,
    \gcc0.gc1.gsym.count_d2_reg[8] ,
    Q,
    DIADI);
  output [32:0]D;
  input s_axi_aclk;
  input ram_rd_en_i;
  input [0:0]WEBWE;
  input [8:0]\gc0.count_d1_reg[8] ;
  input [8:0]\gcc0.gc1.gsym.count_d2_reg[8] ;
  input [31:0]Q;
  input [4:0]DIADI;

  wire [32:0]D;
  wire [4:0]DIADI;
  wire [31:0]Q;
  wire [0:0]WEBWE;
  wire [8:0]\gc0.count_d1_reg[8] ;
  wire [8:0]\gcc0.gc1.gsym.count_d2_reg[8] ;
  wire ram_rd_en_i;
  wire s_axi_aclk;

  axi_fifo_mm_s_0_blk_mem_gen_v8_3_5_synth inst_blk_mem_gen
       (.D(D),
        .DIADI(DIADI),
        .Q(Q),
        .WEBWE(WEBWE),
        .\gc0.count_d1_reg[8] (\gc0.count_d1_reg[8] ),
        .\gcc0.gc1.gsym.count_d2_reg[8] (\gcc0.gc1.gsym.count_d2_reg[8] ),
        .ram_rd_en_i(ram_rd_en_i),
        .s_axi_aclk(s_axi_aclk));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_v8_3_5" *) 
module axi_fifo_mm_s_0_blk_mem_gen_v8_3_5_7
   (D,
    s_axi_aclk,
    ram_rd_en_i,
    WEBWE,
    \gc0.count_d1_reg[8] ,
    \gcc0.gc0.count_d1_reg[8] ,
    axi_str_rxd_tdata,
    axi_str_rxd_tlast);
  output [32:0]D;
  input s_axi_aclk;
  input ram_rd_en_i;
  input [0:0]WEBWE;
  input [8:0]\gc0.count_d1_reg[8] ;
  input [8:0]\gcc0.gc0.count_d1_reg[8] ;
  input [31:0]axi_str_rxd_tdata;
  input axi_str_rxd_tlast;

  wire [32:0]D;
  wire [0:0]WEBWE;
  wire [31:0]axi_str_rxd_tdata;
  wire axi_str_rxd_tlast;
  wire [8:0]\gc0.count_d1_reg[8] ;
  wire [8:0]\gcc0.gc0.count_d1_reg[8] ;
  wire ram_rd_en_i;
  wire s_axi_aclk;

  axi_fifo_mm_s_0_blk_mem_gen_v8_3_5_synth_8 inst_blk_mem_gen
       (.D(D),
        .WEBWE(WEBWE),
        .axi_str_rxd_tdata(axi_str_rxd_tdata),
        .axi_str_rxd_tlast(axi_str_rxd_tlast),
        .\gc0.count_d1_reg[8] (\gc0.count_d1_reg[8] ),
        .\gcc0.gc0.count_d1_reg[8] (\gcc0.gc0.count_d1_reg[8] ),
        .ram_rd_en_i(ram_rd_en_i),
        .s_axi_aclk(s_axi_aclk));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_v8_3_5_synth" *) 
module axi_fifo_mm_s_0_blk_mem_gen_v8_3_5_synth
   (D,
    s_axi_aclk,
    ram_rd_en_i,
    WEBWE,
    \gc0.count_d1_reg[8] ,
    \gcc0.gc1.gsym.count_d2_reg[8] ,
    Q,
    DIADI);
  output [32:0]D;
  input s_axi_aclk;
  input ram_rd_en_i;
  input [0:0]WEBWE;
  input [8:0]\gc0.count_d1_reg[8] ;
  input [8:0]\gcc0.gc1.gsym.count_d2_reg[8] ;
  input [31:0]Q;
  input [4:0]DIADI;

  wire [32:0]D;
  wire [4:0]DIADI;
  wire [31:0]Q;
  wire [0:0]WEBWE;
  wire [8:0]\gc0.count_d1_reg[8] ;
  wire [8:0]\gcc0.gc1.gsym.count_d2_reg[8] ;
  wire ram_rd_en_i;
  wire s_axi_aclk;

  axi_fifo_mm_s_0_blk_mem_gen_top \gnbram.gnativebmg.native_blk_mem_gen 
       (.D(D),
        .DIADI(DIADI),
        .Q(Q),
        .WEBWE(WEBWE),
        .\gc0.count_d1_reg[8] (\gc0.count_d1_reg[8] ),
        .\gcc0.gc1.gsym.count_d2_reg[8] (\gcc0.gc1.gsym.count_d2_reg[8] ),
        .ram_rd_en_i(ram_rd_en_i),
        .s_axi_aclk(s_axi_aclk));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_v8_3_5_synth" *) 
module axi_fifo_mm_s_0_blk_mem_gen_v8_3_5_synth_8
   (D,
    s_axi_aclk,
    ram_rd_en_i,
    WEBWE,
    \gc0.count_d1_reg[8] ,
    \gcc0.gc0.count_d1_reg[8] ,
    axi_str_rxd_tdata,
    axi_str_rxd_tlast);
  output [32:0]D;
  input s_axi_aclk;
  input ram_rd_en_i;
  input [0:0]WEBWE;
  input [8:0]\gc0.count_d1_reg[8] ;
  input [8:0]\gcc0.gc0.count_d1_reg[8] ;
  input [31:0]axi_str_rxd_tdata;
  input axi_str_rxd_tlast;

  wire [32:0]D;
  wire [0:0]WEBWE;
  wire [31:0]axi_str_rxd_tdata;
  wire axi_str_rxd_tlast;
  wire [8:0]\gc0.count_d1_reg[8] ;
  wire [8:0]\gcc0.gc0.count_d1_reg[8] ;
  wire ram_rd_en_i;
  wire s_axi_aclk;

  axi_fifo_mm_s_0_blk_mem_gen_top_9 \gnbram.gnativebmg.native_blk_mem_gen 
       (.D(D),
        .WEBWE(WEBWE),
        .axi_str_rxd_tdata(axi_str_rxd_tdata),
        .axi_str_rxd_tlast(axi_str_rxd_tlast),
        .\gc0.count_d1_reg[8] (\gc0.count_d1_reg[8] ),
        .\gcc0.gc0.count_d1_reg[8] (\gcc0.gc0.count_d1_reg[8] ),
        .ram_rd_en_i(ram_rd_en_i),
        .s_axi_aclk(s_axi_aclk));
endmodule

(* ORIG_REF_NAME = "fifo_generator_v13_0_5" *) 
module axi_fifo_mm_s_0_fifo_generator_v13_0_5
   (\gaxi_full_sm.w_ready_r_reg ,
    sig_txd_prog_empty,
    p_7_out,
    \axi_str_txd_tdata[31] ,
    DI,
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[8] ,
    \sig_register_array_reg[0][4] ,
    axi_str_txd_tvalid,
    \gaxi_bid_gen.S_AXI_BID_reg[0] ,
    S,
    \gfifo_gen.gmm2s.vacancy_i_reg[9] ,
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[4] ,
    tx_fifo_or,
    D,
    sig_txd_pf_event__1,
    sig_txd_pe_event__1,
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[9] ,
    \gtxc.txc_str_Valid_reg ,
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[4]_0 ,
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[4] ,
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[8]_0 ,
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[8] ,
    \gtxc.TXC_STATE_reg[0] ,
    \gtxc.TXC_STATE_reg[1] ,
    s_axi_aclk,
    Q,
    DIADI,
    inverted_reset,
    txd_wr_en,
    start_wr,
    axi_str_txd_tready,
    axi_str_txc_tlast,
    \gtxc.TXC_STATE_reg[1]_0 ,
    sig_txd_prog_full_d1,
    sig_txd_prog_empty_d1,
    axi_str_txc_tready,
    \gtxc.TXC_STATE_reg[0]_0 ,
    axi_str_txc_tvalid,
    \gtxc.txc_cntr_reg[1] ,
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[7] ,
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[7] );
  output \gaxi_full_sm.w_ready_r_reg ;
  output sig_txd_prog_empty;
  output p_7_out;
  output [32:0]\axi_str_txd_tdata[31] ;
  output [3:0]DI;
  output [4:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[8] ;
  output \sig_register_array_reg[0][4] ;
  output axi_str_txd_tvalid;
  output [0:0]\gaxi_bid_gen.S_AXI_BID_reg[0] ;
  output [0:0]S;
  output [7:0]\gfifo_gen.gmm2s.vacancy_i_reg[9] ;
  output [0:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[4] ;
  output tx_fifo_or;
  output [7:0]D;
  output sig_txd_pf_event__1;
  output sig_txd_pe_event__1;
  output [0:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[9] ;
  output \gtxc.txc_str_Valid_reg ;
  output [3:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[4]_0 ;
  output [3:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[4] ;
  output [3:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[8]_0 ;
  output [3:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[8] ;
  output \gtxc.TXC_STATE_reg[0] ;
  output \gtxc.TXC_STATE_reg[1] ;
  input s_axi_aclk;
  input [31:0]Q;
  input [4:0]DIADI;
  input inverted_reset;
  input txd_wr_en;
  input start_wr;
  input axi_str_txd_tready;
  input axi_str_txc_tlast;
  input \gtxc.TXC_STATE_reg[1]_0 ;
  input sig_txd_prog_full_d1;
  input sig_txd_prog_empty_d1;
  input axi_str_txc_tready;
  input \gtxc.TXC_STATE_reg[0]_0 ;
  input axi_str_txc_tvalid;
  input \gtxc.txc_cntr_reg[1] ;
  input [8:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[7] ;
  input [8:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[7] ;

  wire [7:0]D;
  wire [3:0]DI;
  wire [4:0]DIADI;
  wire [31:0]Q;
  wire [0:0]S;
  wire axi_str_txc_tlast;
  wire axi_str_txc_tready;
  wire axi_str_txc_tvalid;
  wire [32:0]\axi_str_txd_tdata[31] ;
  wire axi_str_txd_tready;
  wire axi_str_txd_tvalid;
  wire [0:0]\gaxi_bid_gen.S_AXI_BID_reg[0] ;
  wire \gaxi_full_sm.w_ready_r_reg ;
  wire [3:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[4] ;
  wire [8:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[7] ;
  wire [4:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[8] ;
  wire [3:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[8]_0 ;
  wire [0:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[4] ;
  wire [3:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[4]_0 ;
  wire [8:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[7] ;
  wire [3:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[8] ;
  wire [0:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[9] ;
  wire [7:0]\gfifo_gen.gmm2s.vacancy_i_reg[9] ;
  wire \gtxc.TXC_STATE_reg[0] ;
  wire \gtxc.TXC_STATE_reg[0]_0 ;
  wire \gtxc.TXC_STATE_reg[1] ;
  wire \gtxc.TXC_STATE_reg[1]_0 ;
  wire \gtxc.txc_cntr_reg[1] ;
  wire \gtxc.txc_str_Valid_reg ;
  wire inverted_reset;
  wire p_7_out;
  wire s_axi_aclk;
  wire \sig_register_array_reg[0][4] ;
  wire sig_txd_pe_event__1;
  wire sig_txd_pf_event__1;
  wire sig_txd_prog_empty;
  wire sig_txd_prog_empty_d1;
  wire sig_txd_prog_full_d1;
  wire start_wr;
  wire tx_fifo_or;
  wire txd_wr_en;

  axi_fifo_mm_s_0_fifo_generator_v13_0_5_synth inst_fifo_gen
       (.D(D),
        .DI(DI),
        .DIADI(DIADI),
        .Q(Q),
        .S(S),
        .axi_str_txc_tlast(axi_str_txc_tlast),
        .axi_str_txc_tready(axi_str_txc_tready),
        .axi_str_txc_tvalid(axi_str_txc_tvalid),
        .\axi_str_txd_tdata[31] (\axi_str_txd_tdata[31] ),
        .axi_str_txd_tready(axi_str_txd_tready),
        .axi_str_txd_tvalid(axi_str_txd_tvalid),
        .\gaxi_bid_gen.S_AXI_BID_reg[0] (\gaxi_bid_gen.S_AXI_BID_reg[0] ),
        .\gaxi_full_sm.w_ready_r_reg (\gaxi_full_sm.w_ready_r_reg ),
        .\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[4]_0 (\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[4] ),
        .\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[7]_0 (\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[7] ),
        .\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[8]_0 (\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[8] ),
        .\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[8]_1 (\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[8]_0 ),
        .\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[4]_0 (\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[4] ),
        .\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[4]_1 (\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[4]_0 ),
        .\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[7]_0 (\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[7] ),
        .\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[8]_0 (\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[8] ),
        .\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[9]_0 (\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[9] ),
        .\gfifo_gen.gmm2s.vacancy_i_reg[9] (\gfifo_gen.gmm2s.vacancy_i_reg[9] ),
        .\gtxc.TXC_STATE_reg[0] (\gtxc.TXC_STATE_reg[0] ),
        .\gtxc.TXC_STATE_reg[0]_0 (\gtxc.TXC_STATE_reg[0]_0 ),
        .\gtxc.TXC_STATE_reg[1] (\gtxc.TXC_STATE_reg[1] ),
        .\gtxc.TXC_STATE_reg[1]_0 (\gtxc.TXC_STATE_reg[1]_0 ),
        .\gtxc.txc_cntr_reg[1] (\gtxc.txc_cntr_reg[1] ),
        .\gtxc.txc_str_Valid_reg (\gtxc.txc_str_Valid_reg ),
        .inverted_reset(inverted_reset),
        .p_7_out(p_7_out),
        .s_axi_aclk(s_axi_aclk),
        .\sig_register_array_reg[0][4] (\sig_register_array_reg[0][4] ),
        .sig_txd_pe_event__1(sig_txd_pe_event__1),
        .sig_txd_pf_event__1(sig_txd_pf_event__1),
        .sig_txd_prog_empty(sig_txd_prog_empty),
        .sig_txd_prog_empty_d1(sig_txd_prog_empty_d1),
        .sig_txd_prog_full_d1(sig_txd_prog_full_d1),
        .start_wr(start_wr),
        .tx_fifo_or(tx_fifo_or),
        .txd_wr_en(txd_wr_en));
endmodule

(* ORIG_REF_NAME = "fifo_generator_v13_0_5" *) 
module axi_fifo_mm_s_0_fifo_generator_v13_0_5__parameterized0
   (\grxd.fg_rxd_wr_length_reg[1] ,
    empty_fwft_i,
    p_9_out,
    p_8_out,
    \grxd.fg_rxd_wr_length_reg[21] ,
    rx_complete,
    rx_str_wr_en,
    \grxd.sig_rxd_rd_data_reg[32] ,
    \gc0.count_reg[0] ,
    \gc0.count_reg[0]_0 ,
    axi_str_rxd_tready,
    DI,
    Q,
    \sig_register_array_reg[0][2] ,
    \sig_register_array_reg[0][1] ,
    \sig_register_array_reg[0][2]_0 ,
    sig_rxd_pf_event__1,
    sig_rxd_pe_event__1,
    S,
    \count_reg[4] ,
    \count_reg[8] ,
    \grxd.fg_rxd_wr_length_reg[2] ,
    s_axi4_rdata,
    s_axi_aclk,
    axi_str_rxd_tdata,
    axi_str_rxd_tlast,
    sync_areset_n_reg_inv,
    axis_rd_en__0,
    axi_str_rxd_tvalid,
    rx_len_wr_en,
    s_axi_aresetn,
    sig_str_rst_reg,
    sig_rxd_rd_data,
    axi4_fifo_rd_en_i,
    sig_rd_rlen_reg,
    \gaxi_full_sm.r_valid_r_reg ,
    \gaxi_full_sm.r_last_r_reg ,
    sig_axi_rd_en,
    \s_axi4_araddr[21] ,
    \s_axi4_araddr[29] ,
    s_axi4_araddr,
    sig_rx_channel_reset_reg,
    sig_rxd_prog_full_d1,
    sig_rxd_prog_empty_d1,
    O,
    \grxd.fg_rxd_wr_length_reg[2]_0 ,
    fg_rxd_wr_length,
    D);
  output \grxd.fg_rxd_wr_length_reg[1] ;
  output empty_fwft_i;
  output p_9_out;
  output p_8_out;
  output \grxd.fg_rxd_wr_length_reg[21] ;
  output rx_complete;
  output rx_str_wr_en;
  output \grxd.sig_rxd_rd_data_reg[32] ;
  output \gc0.count_reg[0] ;
  output \gc0.count_reg[0]_0 ;
  output axi_str_rxd_tready;
  output [3:0]DI;
  output [6:0]Q;
  output \sig_register_array_reg[0][2] ;
  output \sig_register_array_reg[0][1] ;
  output \sig_register_array_reg[0][2]_0 ;
  output sig_rxd_pf_event__1;
  output sig_rxd_pe_event__1;
  output [0:0]S;
  output [3:0]\count_reg[4] ;
  output [3:0]\count_reg[8] ;
  output \grxd.fg_rxd_wr_length_reg[2] ;
  output [31:0]s_axi4_rdata;
  input s_axi_aclk;
  input [31:0]axi_str_rxd_tdata;
  input axi_str_rxd_tlast;
  input sync_areset_n_reg_inv;
  input axis_rd_en__0;
  input axi_str_rxd_tvalid;
  input rx_len_wr_en;
  input s_axi_aresetn;
  input sig_str_rst_reg;
  input [0:0]sig_rxd_rd_data;
  input axi4_fifo_rd_en_i;
  input sig_rd_rlen_reg;
  input \gaxi_full_sm.r_valid_r_reg ;
  input \gaxi_full_sm.r_last_r_reg ;
  input sig_axi_rd_en;
  input \s_axi4_araddr[21] ;
  input \s_axi4_araddr[29] ;
  input [11:0]s_axi4_araddr;
  input sig_rx_channel_reset_reg;
  input sig_rxd_prog_full_d1;
  input sig_rxd_prog_empty_d1;
  input [0:0]O;
  input [0:0]\grxd.fg_rxd_wr_length_reg[2]_0 ;
  input [0:0]fg_rxd_wr_length;
  input [8:0]D;

  wire [8:0]D;
  wire [3:0]DI;
  wire [0:0]O;
  wire [6:0]Q;
  wire [0:0]S;
  wire axi4_fifo_rd_en_i;
  wire [31:0]axi_str_rxd_tdata;
  wire axi_str_rxd_tlast;
  wire axi_str_rxd_tready;
  wire axi_str_rxd_tvalid;
  wire axis_rd_en__0;
  wire [3:0]\count_reg[4] ;
  wire [3:0]\count_reg[8] ;
  wire empty_fwft_i;
  wire [0:0]fg_rxd_wr_length;
  wire \gaxi_full_sm.r_last_r_reg ;
  wire \gaxi_full_sm.r_valid_r_reg ;
  wire \gc0.count_reg[0] ;
  wire \gc0.count_reg[0]_0 ;
  wire \grxd.fg_rxd_wr_length_reg[1] ;
  wire \grxd.fg_rxd_wr_length_reg[21] ;
  wire \grxd.fg_rxd_wr_length_reg[2] ;
  wire [0:0]\grxd.fg_rxd_wr_length_reg[2]_0 ;
  wire \grxd.sig_rxd_rd_data_reg[32] ;
  wire p_8_out;
  wire p_9_out;
  wire rx_complete;
  wire rx_len_wr_en;
  wire rx_str_wr_en;
  wire [11:0]s_axi4_araddr;
  wire \s_axi4_araddr[21] ;
  wire \s_axi4_araddr[29] ;
  wire [31:0]s_axi4_rdata;
  wire s_axi_aclk;
  wire s_axi_aresetn;
  wire sig_axi_rd_en;
  wire sig_rd_rlen_reg;
  wire \sig_register_array_reg[0][1] ;
  wire \sig_register_array_reg[0][2] ;
  wire \sig_register_array_reg[0][2]_0 ;
  wire sig_rx_channel_reset_reg;
  wire sig_rxd_pe_event__1;
  wire sig_rxd_pf_event__1;
  wire sig_rxd_prog_empty_d1;
  wire sig_rxd_prog_full_d1;
  wire [0:0]sig_rxd_rd_data;
  wire sig_str_rst_reg;
  wire sync_areset_n_reg_inv;

  axi_fifo_mm_s_0_fifo_generator_v13_0_5_synth__parameterized0 inst_fifo_gen
       (.D(D),
        .DI(DI),
        .O(O),
        .Q(Q),
        .S(S),
        .axi4_fifo_rd_en_i(axi4_fifo_rd_en_i),
        .axi_str_rxd_tdata(axi_str_rxd_tdata),
        .axi_str_rxd_tlast(axi_str_rxd_tlast),
        .axi_str_rxd_tready(axi_str_rxd_tready),
        .axi_str_rxd_tvalid(axi_str_rxd_tvalid),
        .axis_rd_en__0(axis_rd_en__0),
        .\count_reg[4] (\count_reg[4] ),
        .\count_reg[8] (\count_reg[8] ),
        .empty_fwft_i(empty_fwft_i),
        .fg_rxd_wr_length(fg_rxd_wr_length),
        .\gaxi_full_sm.r_last_r_reg (\gaxi_full_sm.r_last_r_reg ),
        .\gaxi_full_sm.r_valid_r_reg (\gaxi_full_sm.r_valid_r_reg ),
        .\gc0.count_reg[0] (\gc0.count_reg[0] ),
        .\gc0.count_reg[0]_0 (\gc0.count_reg[0]_0 ),
        .\grxd.fg_rxd_wr_length_reg[1] (\grxd.fg_rxd_wr_length_reg[1] ),
        .\grxd.fg_rxd_wr_length_reg[21] (\grxd.fg_rxd_wr_length_reg[21] ),
        .\grxd.fg_rxd_wr_length_reg[2] (\grxd.fg_rxd_wr_length_reg[2] ),
        .\grxd.fg_rxd_wr_length_reg[2]_0 (\grxd.fg_rxd_wr_length_reg[2]_0 ),
        .\grxd.sig_rxd_rd_data_reg[32] (\grxd.sig_rxd_rd_data_reg[32] ),
        .p_8_out(p_8_out),
        .p_9_out(p_9_out),
        .rx_complete(rx_complete),
        .rx_len_wr_en(rx_len_wr_en),
        .rx_str_wr_en(rx_str_wr_en),
        .s_axi4_araddr(s_axi4_araddr),
        .\s_axi4_araddr[21] (\s_axi4_araddr[21] ),
        .\s_axi4_araddr[29] (\s_axi4_araddr[29] ),
        .s_axi4_rdata(s_axi4_rdata),
        .s_axi_aclk(s_axi_aclk),
        .s_axi_aresetn(s_axi_aresetn),
        .sig_axi_rd_en(sig_axi_rd_en),
        .sig_rd_rlen_reg(sig_rd_rlen_reg),
        .\sig_register_array_reg[0][1] (\sig_register_array_reg[0][1] ),
        .\sig_register_array_reg[0][2] (\sig_register_array_reg[0][2] ),
        .\sig_register_array_reg[0][2]_0 (\sig_register_array_reg[0][2]_0 ),
        .sig_rx_channel_reset_reg(sig_rx_channel_reset_reg),
        .sig_rxd_pe_event__1(sig_rxd_pe_event__1),
        .sig_rxd_pf_event__1(sig_rxd_pf_event__1),
        .sig_rxd_prog_empty_d1(sig_rxd_prog_empty_d1),
        .sig_rxd_prog_full_d1(sig_rxd_prog_full_d1),
        .sig_rxd_rd_data(sig_rxd_rd_data),
        .sig_str_rst_reg(sig_str_rst_reg),
        .sync_areset_n_reg_inv(sync_areset_n_reg_inv));
endmodule

(* ORIG_REF_NAME = "fifo_generator_v13_0_5_compare" *) 
module axi_fifo_mm_s_0_fifo_generator_v13_0_5_compare
   (ram_full_comb,
    \gc0.count_d1_reg[6] ,
    v1_reg_0,
    p_3_out,
    \grstd1.grst_full.grst_f.rst_d5_reg ,
    comp1,
    sig_txd_sb_wr_en_reg,
    p_6_out);
  output ram_full_comb;
  input [3:0]\gc0.count_d1_reg[6] ;
  input [0:0]v1_reg_0;
  input p_3_out;
  input \grstd1.grst_full.grst_f.rst_d5_reg ;
  input comp1;
  input [0:0]sig_txd_sb_wr_en_reg;
  input p_6_out;

  wire carrynet_0;
  wire carrynet_1;
  wire carrynet_2;
  wire carrynet_3;
  wire comp0;
  wire comp1;
  wire [3:0]\gc0.count_d1_reg[6] ;
  wire \grstd1.grst_full.grst_f.rst_d5_reg ;
  wire p_3_out;
  wire p_6_out;
  wire ram_full_comb;
  wire [0:0]sig_txd_sb_wr_en_reg;
  wire [0:0]v1_reg_0;
  wire [3:0]\NLW_gmux.gm[0].gm1.m1_CARRY4_O_UNCONNECTED ;
  wire [3:1]\NLW_gmux.gm[4].gms.ms_CARRY4_CO_UNCONNECTED ;
  wire [3:1]\NLW_gmux.gm[4].gms.ms_CARRY4_DI_UNCONNECTED ;
  wire [3:0]\NLW_gmux.gm[4].gms.ms_CARRY4_O_UNCONNECTED ;
  wire [3:1]\NLW_gmux.gm[4].gms.ms_CARRY4_S_UNCONNECTED ;

  (* XILINX_LEGACY_PRIM = "(MUXCY,XORCY)" *) 
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \gmux.gm[0].gm1.m1_CARRY4 
       (.CI(1'b0),
        .CO({carrynet_3,carrynet_2,carrynet_1,carrynet_0}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_gmux.gm[0].gm1.m1_CARRY4_O_UNCONNECTED [3:0]),
        .S(\gc0.count_d1_reg[6] ));
  (* XILINX_LEGACY_PRIM = "(MUXCY,XORCY)" *) 
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \gmux.gm[4].gms.ms_CARRY4 
       (.CI(carrynet_3),
        .CO({\NLW_gmux.gm[4].gms.ms_CARRY4_CO_UNCONNECTED [3:1],comp0}),
        .CYINIT(1'b0),
        .DI({\NLW_gmux.gm[4].gms.ms_CARRY4_DI_UNCONNECTED [3:1],1'b0}),
        .O(\NLW_gmux.gm[4].gms.ms_CARRY4_O_UNCONNECTED [3:0]),
        .S({\NLW_gmux.gm[4].gms.ms_CARRY4_S_UNCONNECTED [3:1],v1_reg_0}));
  LUT6 #(
    .INIT(64'h04040404FF0C0C0C)) 
    ram_full_fb_i_i_1
       (.I0(comp0),
        .I1(p_3_out),
        .I2(\grstd1.grst_full.grst_f.rst_d5_reg ),
        .I3(comp1),
        .I4(sig_txd_sb_wr_en_reg),
        .I5(p_6_out),
        .O(ram_full_comb));
endmodule

(* ORIG_REF_NAME = "fifo_generator_v13_0_5_compare" *) 
module axi_fifo_mm_s_0_fifo_generator_v13_0_5_compare_0
   (comp1,
    p_2_out,
    \gc0.count_d1_reg[6] ,
    v1_reg_1,
    axis_almost_full,
    \grstd1.grst_full.grst_f.rst_d5_reg ,
    p_0_in,
    sig_txd_sb_wr_en_reg,
    p_6_out);
  output comp1;
  output p_2_out;
  input [3:0]\gc0.count_d1_reg[6] ;
  input [0:0]v1_reg_1;
  input axis_almost_full;
  input \grstd1.grst_full.grst_f.rst_d5_reg ;
  input p_0_in;
  input [0:0]sig_txd_sb_wr_en_reg;
  input p_6_out;

  wire axis_almost_full;
  wire carrynet_0;
  wire carrynet_1;
  wire carrynet_2;
  wire carrynet_3;
  wire comp1;
  wire [3:0]\gc0.count_d1_reg[6] ;
  wire \grstd1.grst_full.grst_f.rst_d5_reg ;
  wire p_0_in;
  wire p_2_out;
  wire p_6_out;
  wire [0:0]sig_txd_sb_wr_en_reg;
  wire [0:0]v1_reg_1;
  wire [3:0]\NLW_gmux.gm[0].gm1.m1_CARRY4_O_UNCONNECTED ;
  wire [3:1]\NLW_gmux.gm[4].gms.ms_CARRY4_CO_UNCONNECTED ;
  wire [3:1]\NLW_gmux.gm[4].gms.ms_CARRY4_DI_UNCONNECTED ;
  wire [3:0]\NLW_gmux.gm[4].gms.ms_CARRY4_O_UNCONNECTED ;
  wire [3:1]\NLW_gmux.gm[4].gms.ms_CARRY4_S_UNCONNECTED ;

  LUT6 #(
    .INIT(64'h0C0C0404FF0C0C0C)) 
    \gaf.gaf0.ram_afull_i_i_1 
       (.I0(comp1),
        .I1(axis_almost_full),
        .I2(\grstd1.grst_full.grst_f.rst_d5_reg ),
        .I3(p_0_in),
        .I4(sig_txd_sb_wr_en_reg),
        .I5(p_6_out),
        .O(p_2_out));
  (* XILINX_LEGACY_PRIM = "(MUXCY,XORCY)" *) 
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \gmux.gm[0].gm1.m1_CARRY4 
       (.CI(1'b0),
        .CO({carrynet_3,carrynet_2,carrynet_1,carrynet_0}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_gmux.gm[0].gm1.m1_CARRY4_O_UNCONNECTED [3:0]),
        .S(\gc0.count_d1_reg[6] ));
  (* XILINX_LEGACY_PRIM = "(MUXCY,XORCY)" *) 
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \gmux.gm[4].gms.ms_CARRY4 
       (.CI(carrynet_3),
        .CO({\NLW_gmux.gm[4].gms.ms_CARRY4_CO_UNCONNECTED [3:1],comp1}),
        .CYINIT(1'b0),
        .DI({\NLW_gmux.gm[4].gms.ms_CARRY4_DI_UNCONNECTED [3:1],1'b0}),
        .O(\NLW_gmux.gm[4].gms.ms_CARRY4_O_UNCONNECTED [3:0]),
        .S({\NLW_gmux.gm[4].gms.ms_CARRY4_S_UNCONNECTED [3:1],v1_reg_1}));
endmodule

(* ORIG_REF_NAME = "fifo_generator_v13_0_5_compare" *) 
module axi_fifo_mm_s_0_fifo_generator_v13_0_5_compare_1
   (p_0_in,
    v1_reg);
  output p_0_in;
  input [4:0]v1_reg;

  wire carrynet_0;
  wire carrynet_1;
  wire carrynet_2;
  wire carrynet_3;
  wire p_0_in;
  wire [4:0]v1_reg;
  wire [3:0]\NLW_gmux.gm[0].gm1.m1_CARRY4_O_UNCONNECTED ;
  wire [3:1]\NLW_gmux.gm[4].gms.ms_CARRY4_CO_UNCONNECTED ;
  wire [3:1]\NLW_gmux.gm[4].gms.ms_CARRY4_DI_UNCONNECTED ;
  wire [3:0]\NLW_gmux.gm[4].gms.ms_CARRY4_O_UNCONNECTED ;
  wire [3:1]\NLW_gmux.gm[4].gms.ms_CARRY4_S_UNCONNECTED ;

  (* XILINX_LEGACY_PRIM = "(MUXCY,XORCY)" *) 
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \gmux.gm[0].gm1.m1_CARRY4 
       (.CI(1'b0),
        .CO({carrynet_3,carrynet_2,carrynet_1,carrynet_0}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_gmux.gm[0].gm1.m1_CARRY4_O_UNCONNECTED [3:0]),
        .S(v1_reg[3:0]));
  (* XILINX_LEGACY_PRIM = "(MUXCY,XORCY)" *) 
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \gmux.gm[4].gms.ms_CARRY4 
       (.CI(carrynet_3),
        .CO({\NLW_gmux.gm[4].gms.ms_CARRY4_CO_UNCONNECTED [3:1],p_0_in}),
        .CYINIT(1'b0),
        .DI({\NLW_gmux.gm[4].gms.ms_CARRY4_DI_UNCONNECTED [3:1],1'b0}),
        .O(\NLW_gmux.gm[4].gms.ms_CARRY4_O_UNCONNECTED [3:0]),
        .S({\NLW_gmux.gm[4].gms.ms_CARRY4_S_UNCONNECTED [3:1],v1_reg[4]}));
endmodule

(* ORIG_REF_NAME = "fifo_generator_v13_0_5_compare" *) 
module axi_fifo_mm_s_0_fifo_generator_v13_0_5_compare_14
   (comp0,
    \gc0.count_d1_reg[6] ,
    v1_reg_0);
  output comp0;
  input [3:0]\gc0.count_d1_reg[6] ;
  input [0:0]v1_reg_0;

  wire carrynet_0;
  wire carrynet_1;
  wire carrynet_2;
  wire carrynet_3;
  wire comp0;
  wire [3:0]\gc0.count_d1_reg[6] ;
  wire [0:0]v1_reg_0;
  wire [3:0]\NLW_gmux.gm[0].gm1.m1_CARRY4_O_UNCONNECTED ;
  wire [3:1]\NLW_gmux.gm[4].gms.ms_CARRY4_CO_UNCONNECTED ;
  wire [3:1]\NLW_gmux.gm[4].gms.ms_CARRY4_DI_UNCONNECTED ;
  wire [3:0]\NLW_gmux.gm[4].gms.ms_CARRY4_O_UNCONNECTED ;
  wire [3:1]\NLW_gmux.gm[4].gms.ms_CARRY4_S_UNCONNECTED ;

  (* XILINX_LEGACY_PRIM = "(MUXCY,XORCY)" *) 
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \gmux.gm[0].gm1.m1_CARRY4 
       (.CI(1'b0),
        .CO({carrynet_3,carrynet_2,carrynet_1,carrynet_0}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_gmux.gm[0].gm1.m1_CARRY4_O_UNCONNECTED [3:0]),
        .S(\gc0.count_d1_reg[6] ));
  (* XILINX_LEGACY_PRIM = "(MUXCY,XORCY)" *) 
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \gmux.gm[4].gms.ms_CARRY4 
       (.CI(carrynet_3),
        .CO({\NLW_gmux.gm[4].gms.ms_CARRY4_CO_UNCONNECTED [3:1],comp0}),
        .CYINIT(1'b0),
        .DI({\NLW_gmux.gm[4].gms.ms_CARRY4_DI_UNCONNECTED [3:1],1'b0}),
        .O(\NLW_gmux.gm[4].gms.ms_CARRY4_O_UNCONNECTED [3:0]),
        .S({\NLW_gmux.gm[4].gms.ms_CARRY4_S_UNCONNECTED [3:1],v1_reg_0}));
endmodule

(* ORIG_REF_NAME = "fifo_generator_v13_0_5_compare" *) 
module axi_fifo_mm_s_0_fifo_generator_v13_0_5_compare_15
   (ram_full_comb,
    v1_reg,
    E,
    comp0,
    p_6_out,
    p_2_out,
    \grstd1.grst_full.grst_f.rst_d5_reg );
  output ram_full_comb;
  input [4:0]v1_reg;
  input [0:0]E;
  input comp0;
  input p_6_out;
  input p_2_out;
  input \grstd1.grst_full.grst_f.rst_d5_reg ;

  wire [0:0]E;
  wire carrynet_0;
  wire carrynet_1;
  wire carrynet_2;
  wire carrynet_3;
  wire comp0;
  wire comp1;
  wire \grstd1.grst_full.grst_f.rst_d5_reg ;
  wire p_2_out;
  wire p_6_out;
  wire ram_full_comb;
  wire [4:0]v1_reg;
  wire [3:0]\NLW_gmux.gm[0].gm1.m1_CARRY4_O_UNCONNECTED ;
  wire [3:1]\NLW_gmux.gm[4].gms.ms_CARRY4_CO_UNCONNECTED ;
  wire [3:1]\NLW_gmux.gm[4].gms.ms_CARRY4_DI_UNCONNECTED ;
  wire [3:0]\NLW_gmux.gm[4].gms.ms_CARRY4_O_UNCONNECTED ;
  wire [3:1]\NLW_gmux.gm[4].gms.ms_CARRY4_S_UNCONNECTED ;

  (* XILINX_LEGACY_PRIM = "(MUXCY,XORCY)" *) 
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \gmux.gm[0].gm1.m1_CARRY4 
       (.CI(1'b0),
        .CO({carrynet_3,carrynet_2,carrynet_1,carrynet_0}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_gmux.gm[0].gm1.m1_CARRY4_O_UNCONNECTED [3:0]),
        .S(v1_reg[3:0]));
  (* XILINX_LEGACY_PRIM = "(MUXCY,XORCY)" *) 
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \gmux.gm[4].gms.ms_CARRY4 
       (.CI(carrynet_3),
        .CO({\NLW_gmux.gm[4].gms.ms_CARRY4_CO_UNCONNECTED [3:1],comp1}),
        .CYINIT(1'b0),
        .DI({\NLW_gmux.gm[4].gms.ms_CARRY4_DI_UNCONNECTED [3:1],1'b0}),
        .O(\NLW_gmux.gm[4].gms.ms_CARRY4_O_UNCONNECTED [3:0]),
        .S({\NLW_gmux.gm[4].gms.ms_CARRY4_S_UNCONNECTED [3:1],v1_reg[4]}));
  LUT6 #(
    .INIT(64'h008800880FFF0088)) 
    ram_full_fb_i_i_1__0
       (.I0(E),
        .I1(comp1),
        .I2(comp0),
        .I3(p_6_out),
        .I4(p_2_out),
        .I5(\grstd1.grst_full.grst_f.rst_d5_reg ),
        .O(ram_full_comb));
endmodule

(* ORIG_REF_NAME = "fifo_generator_v13_0_5_compare" *) 
module axi_fifo_mm_s_0_fifo_generator_v13_0_5_compare_2
   (ram_empty_fb_i_reg,
    \gc0.count_d1_reg[0] ,
    \gc0.count_d1_reg[2] ,
    \gc0.count_d1_reg[4] ,
    \gc0.count_d1_reg[6] ,
    \gcc0.gc1.gsym.count_d2_reg[8] ,
    p_2_out,
    p_16_out,
    E,
    comp1);
  output ram_empty_fb_i_reg;
  input \gc0.count_d1_reg[0] ;
  input \gc0.count_d1_reg[2] ;
  input \gc0.count_d1_reg[4] ;
  input \gc0.count_d1_reg[6] ;
  input \gcc0.gc1.gsym.count_d2_reg[8] ;
  input p_2_out;
  input p_16_out;
  input [0:0]E;
  input comp1;

  wire [0:0]E;
  wire carrynet_0;
  wire carrynet_1;
  wire carrynet_2;
  wire carrynet_3;
  wire comp0;
  wire comp1;
  wire \gc0.count_d1_reg[0] ;
  wire \gc0.count_d1_reg[2] ;
  wire \gc0.count_d1_reg[4] ;
  wire \gc0.count_d1_reg[6] ;
  wire \gcc0.gc1.gsym.count_d2_reg[8] ;
  wire p_16_out;
  wire p_2_out;
  wire ram_empty_fb_i_reg;
  wire [3:0]\NLW_gmux.gm[0].gm1.m1_CARRY4_O_UNCONNECTED ;
  wire [3:1]\NLW_gmux.gm[4].gms.ms_CARRY4_CO_UNCONNECTED ;
  wire [3:1]\NLW_gmux.gm[4].gms.ms_CARRY4_DI_UNCONNECTED ;
  wire [3:0]\NLW_gmux.gm[4].gms.ms_CARRY4_O_UNCONNECTED ;
  wire [3:1]\NLW_gmux.gm[4].gms.ms_CARRY4_S_UNCONNECTED ;

  (* XILINX_LEGACY_PRIM = "(MUXCY,XORCY)" *) 
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \gmux.gm[0].gm1.m1_CARRY4 
       (.CI(1'b0),
        .CO({carrynet_3,carrynet_2,carrynet_1,carrynet_0}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_gmux.gm[0].gm1.m1_CARRY4_O_UNCONNECTED [3:0]),
        .S({\gc0.count_d1_reg[6] ,\gc0.count_d1_reg[4] ,\gc0.count_d1_reg[2] ,\gc0.count_d1_reg[0] }));
  (* XILINX_LEGACY_PRIM = "(MUXCY,XORCY)" *) 
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \gmux.gm[4].gms.ms_CARRY4 
       (.CI(carrynet_3),
        .CO({\NLW_gmux.gm[4].gms.ms_CARRY4_CO_UNCONNECTED [3:1],comp0}),
        .CYINIT(1'b0),
        .DI({\NLW_gmux.gm[4].gms.ms_CARRY4_DI_UNCONNECTED [3:1],1'b0}),
        .O(\NLW_gmux.gm[4].gms.ms_CARRY4_O_UNCONNECTED [3:0]),
        .S({\NLW_gmux.gm[4].gms.ms_CARRY4_S_UNCONNECTED [3:1],\gcc0.gc1.gsym.count_d2_reg[8] }));
  LUT5 #(
    .INIT(32'h2F2A2A2A)) 
    ram_empty_fb_i_i_1
       (.I0(p_2_out),
        .I1(comp0),
        .I2(p_16_out),
        .I3(E),
        .I4(comp1),
        .O(ram_empty_fb_i_reg));
endmodule

(* ORIG_REF_NAME = "fifo_generator_v13_0_5_compare" *) 
module axi_fifo_mm_s_0_fifo_generator_v13_0_5_compare_20
   (ram_empty_fb_i_reg,
    \gc0.count_d1_reg[0] ,
    \gc0.count_d1_reg[2] ,
    \gc0.count_d1_reg[4] ,
    \gc0.count_d1_reg[6] ,
    \gcc0.gc0.count_d1_reg[8] ,
    p_2_out,
    E,
    comp1,
    p_16_out);
  output ram_empty_fb_i_reg;
  input \gc0.count_d1_reg[0] ;
  input \gc0.count_d1_reg[2] ;
  input \gc0.count_d1_reg[4] ;
  input \gc0.count_d1_reg[6] ;
  input \gcc0.gc0.count_d1_reg[8] ;
  input p_2_out;
  input [0:0]E;
  input comp1;
  input p_16_out;

  wire [0:0]E;
  wire carrynet_0;
  wire carrynet_1;
  wire carrynet_2;
  wire carrynet_3;
  wire comp0;
  wire comp1;
  wire \gc0.count_d1_reg[0] ;
  wire \gc0.count_d1_reg[2] ;
  wire \gc0.count_d1_reg[4] ;
  wire \gc0.count_d1_reg[6] ;
  wire \gcc0.gc0.count_d1_reg[8] ;
  wire p_16_out;
  wire p_2_out;
  wire ram_empty_fb_i_reg;
  wire [3:0]\NLW_gmux.gm[0].gm1.m1_CARRY4_O_UNCONNECTED ;
  wire [3:1]\NLW_gmux.gm[4].gms.ms_CARRY4_CO_UNCONNECTED ;
  wire [3:1]\NLW_gmux.gm[4].gms.ms_CARRY4_DI_UNCONNECTED ;
  wire [3:0]\NLW_gmux.gm[4].gms.ms_CARRY4_O_UNCONNECTED ;
  wire [3:1]\NLW_gmux.gm[4].gms.ms_CARRY4_S_UNCONNECTED ;

  (* XILINX_LEGACY_PRIM = "(MUXCY,XORCY)" *) 
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \gmux.gm[0].gm1.m1_CARRY4 
       (.CI(1'b0),
        .CO({carrynet_3,carrynet_2,carrynet_1,carrynet_0}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_gmux.gm[0].gm1.m1_CARRY4_O_UNCONNECTED [3:0]),
        .S({\gc0.count_d1_reg[6] ,\gc0.count_d1_reg[4] ,\gc0.count_d1_reg[2] ,\gc0.count_d1_reg[0] }));
  (* XILINX_LEGACY_PRIM = "(MUXCY,XORCY)" *) 
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \gmux.gm[4].gms.ms_CARRY4 
       (.CI(carrynet_3),
        .CO({\NLW_gmux.gm[4].gms.ms_CARRY4_CO_UNCONNECTED [3:1],comp0}),
        .CYINIT(1'b0),
        .DI({\NLW_gmux.gm[4].gms.ms_CARRY4_DI_UNCONNECTED [3:1],1'b0}),
        .O(\NLW_gmux.gm[4].gms.ms_CARRY4_O_UNCONNECTED [3:0]),
        .S({\NLW_gmux.gm[4].gms.ms_CARRY4_S_UNCONNECTED [3:1],\gcc0.gc0.count_d1_reg[8] }));
  LUT5 #(
    .INIT(32'h2222FAAA)) 
    ram_empty_fb_i_i_1__0
       (.I0(p_2_out),
        .I1(comp0),
        .I2(E),
        .I3(comp1),
        .I4(p_16_out),
        .O(ram_empty_fb_i_reg));
endmodule

(* ORIG_REF_NAME = "fifo_generator_v13_0_5_compare" *) 
module axi_fifo_mm_s_0_fifo_generator_v13_0_5_compare_21
   (comp1,
    v1_reg);
  output comp1;
  input [4:0]v1_reg;

  wire carrynet_0;
  wire carrynet_1;
  wire carrynet_2;
  wire carrynet_3;
  wire comp1;
  wire [4:0]v1_reg;
  wire [3:0]\NLW_gmux.gm[0].gm1.m1_CARRY4_O_UNCONNECTED ;
  wire [3:1]\NLW_gmux.gm[4].gms.ms_CARRY4_CO_UNCONNECTED ;
  wire [3:1]\NLW_gmux.gm[4].gms.ms_CARRY4_DI_UNCONNECTED ;
  wire [3:0]\NLW_gmux.gm[4].gms.ms_CARRY4_O_UNCONNECTED ;
  wire [3:1]\NLW_gmux.gm[4].gms.ms_CARRY4_S_UNCONNECTED ;

  (* XILINX_LEGACY_PRIM = "(MUXCY,XORCY)" *) 
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \gmux.gm[0].gm1.m1_CARRY4 
       (.CI(1'b0),
        .CO({carrynet_3,carrynet_2,carrynet_1,carrynet_0}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_gmux.gm[0].gm1.m1_CARRY4_O_UNCONNECTED [3:0]),
        .S(v1_reg[3:0]));
  (* XILINX_LEGACY_PRIM = "(MUXCY,XORCY)" *) 
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \gmux.gm[4].gms.ms_CARRY4 
       (.CI(carrynet_3),
        .CO({\NLW_gmux.gm[4].gms.ms_CARRY4_CO_UNCONNECTED [3:1],comp1}),
        .CYINIT(1'b0),
        .DI({\NLW_gmux.gm[4].gms.ms_CARRY4_DI_UNCONNECTED [3:1],1'b0}),
        .O(\NLW_gmux.gm[4].gms.ms_CARRY4_O_UNCONNECTED [3:0]),
        .S({\NLW_gmux.gm[4].gms.ms_CARRY4_S_UNCONNECTED [3:1],v1_reg[4]}));
endmodule

(* ORIG_REF_NAME = "fifo_generator_v13_0_5_compare" *) 
module axi_fifo_mm_s_0_fifo_generator_v13_0_5_compare_3
   (comp1,
    v1_reg);
  output comp1;
  input [4:0]v1_reg;

  wire carrynet_0;
  wire carrynet_1;
  wire carrynet_2;
  wire carrynet_3;
  wire comp1;
  wire [4:0]v1_reg;
  wire [3:0]\NLW_gmux.gm[0].gm1.m1_CARRY4_O_UNCONNECTED ;
  wire [3:1]\NLW_gmux.gm[4].gms.ms_CARRY4_CO_UNCONNECTED ;
  wire [3:1]\NLW_gmux.gm[4].gms.ms_CARRY4_DI_UNCONNECTED ;
  wire [3:0]\NLW_gmux.gm[4].gms.ms_CARRY4_O_UNCONNECTED ;
  wire [3:1]\NLW_gmux.gm[4].gms.ms_CARRY4_S_UNCONNECTED ;

  (* XILINX_LEGACY_PRIM = "(MUXCY,XORCY)" *) 
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \gmux.gm[0].gm1.m1_CARRY4 
       (.CI(1'b0),
        .CO({carrynet_3,carrynet_2,carrynet_1,carrynet_0}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_gmux.gm[0].gm1.m1_CARRY4_O_UNCONNECTED [3:0]),
        .S(v1_reg[3:0]));
  (* XILINX_LEGACY_PRIM = "(MUXCY,XORCY)" *) 
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \gmux.gm[4].gms.ms_CARRY4 
       (.CI(carrynet_3),
        .CO({\NLW_gmux.gm[4].gms.ms_CARRY4_CO_UNCONNECTED [3:1],comp1}),
        .CYINIT(1'b0),
        .DI({\NLW_gmux.gm[4].gms.ms_CARRY4_DI_UNCONNECTED [3:1],1'b0}),
        .O(\NLW_gmux.gm[4].gms.ms_CARRY4_O_UNCONNECTED [3:0]),
        .S({\NLW_gmux.gm[4].gms.ms_CARRY4_S_UNCONNECTED [3:1],v1_reg[4]}));
endmodule

(* ORIG_REF_NAME = "fifo_generator_v13_0_5_dc_ss_fwft" *) 
module axi_fifo_mm_s_0_fifo_generator_v13_0_5_dc_ss_fwft
   (DI,
    \sig_ip2bus_data_reg[20] ,
    \sig_register_array_reg[0][2] ,
    \sig_register_array_reg[0][1] ,
    \sig_register_array_reg[0][2]_0 ,
    \count_reg[9] ,
    \count_reg[4] ,
    \count_reg[8] ,
    sig_rx_channel_reset_reg,
    sig_rxd_rd_data,
    valid_fwft,
    axis_rd_en__0,
    \gpregsm1.user_valid_reg ,
    D,
    s_axi_aclk,
    Q);
  output [3:0]DI;
  output [6:0]\sig_ip2bus_data_reg[20] ;
  output \sig_register_array_reg[0][2] ;
  output \sig_register_array_reg[0][1] ;
  output \sig_register_array_reg[0][2]_0 ;
  output [0:0]\count_reg[9] ;
  output [3:0]\count_reg[4] ;
  output [3:0]\count_reg[8] ;
  input sig_rx_channel_reset_reg;
  input [0:0]sig_rxd_rd_data;
  input valid_fwft;
  input axis_rd_en__0;
  input [0:0]\gpregsm1.user_valid_reg ;
  input [8:0]D;
  input s_axi_aclk;
  input [0:0]Q;

  wire [8:0]D;
  wire [3:0]DI;
  wire [0:0]Q;
  wire axis_rd_en__0;
  wire [3:0]\count_reg[4] ;
  wire [3:0]\count_reg[8] ;
  wire [0:0]\count_reg[9] ;
  wire [0:0]\gpregsm1.user_valid_reg ;
  wire s_axi_aclk;
  wire [6:0]\sig_ip2bus_data_reg[20] ;
  wire \sig_register_array_reg[0][1] ;
  wire \sig_register_array_reg[0][2] ;
  wire \sig_register_array_reg[0][2]_0 ;
  wire sig_rx_channel_reset_reg;
  wire [0:0]sig_rxd_rd_data;
  wire valid_fwft;

  axi_fifo_mm_s_0_fifo_generator_v13_0_5_updn_cntr dc
       (.D(D),
        .DI(DI),
        .Q(Q),
        .axis_rd_en__0(axis_rd_en__0),
        .\count_reg[4]_0 (\count_reg[4] ),
        .\count_reg[8]_0 (\count_reg[8] ),
        .\count_reg[9]_0 (\count_reg[9] ),
        .\gpregsm1.user_valid_reg (\gpregsm1.user_valid_reg ),
        .s_axi_aclk(s_axi_aclk),
        .\sig_ip2bus_data_reg[20] (\sig_ip2bus_data_reg[20] ),
        .\sig_register_array_reg[0][1] (\sig_register_array_reg[0][1] ),
        .\sig_register_array_reg[0][2] (\sig_register_array_reg[0][2] ),
        .\sig_register_array_reg[0][2]_0 (\sig_register_array_reg[0][2]_0 ),
        .sig_rx_channel_reset_reg(sig_rx_channel_reset_reg),
        .sig_rxd_rd_data(sig_rxd_rd_data),
        .valid_fwft(valid_fwft));
endmodule

(* ORIG_REF_NAME = "fifo_generator_v13_0_5_fifo_generator_ramfifo" *) 
module axi_fifo_mm_s_0_fifo_generator_v13_0_5_fifo_generator_ramfifo
   (\gaxi_full_sm.w_ready_r_reg ,
    sig_txd_prog_empty,
    p_7_out,
    E,
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[9] ,
    \axi_str_txd_tdata[31] ,
    DI,
    \sig_register_array_reg[0][4] ,
    axi_str_txd_tvalid,
    \gaxi_bid_gen.S_AXI_BID_reg[0] ,
    sig_txd_pf_event__1,
    sig_txd_pe_event__1,
    axis_wr_eop,
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_reg ,
    \gtxc.txc_str_Valid_reg ,
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[4] ,
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[4] ,
    \gtxc.TXC_STATE_reg[0] ,
    \gtxc.TXC_STATE_reg[1] ,
    s_axi_aclk,
    Q,
    DIADI,
    inverted_reset,
    txd_wr_en,
    start_wr,
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_reg_0 ,
    axi_str_txd_tready,
    axis_wr_eop_d1,
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[9]_0 ,
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[5] ,
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[5]_0 ,
    axi_str_txc_tlast,
    \gtxc.TXC_STATE_reg[1]_0 ,
    sig_txd_prog_full_d1,
    sig_txd_prog_empty_d1,
    axi_str_txc_tready,
    \gtxc.TXC_STATE_reg[0]_0 ,
    axi_str_txc_tvalid,
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[1] ,
    \gtxc.txc_cntr_reg[1] );
  output \gaxi_full_sm.w_ready_r_reg ;
  output sig_txd_prog_empty;
  output p_7_out;
  output [0:0]E;
  output [0:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[9] ;
  output [32:0]\axi_str_txd_tdata[31] ;
  output [0:0]DI;
  output \sig_register_array_reg[0][4] ;
  output axi_str_txd_tvalid;
  output [0:0]\gaxi_bid_gen.S_AXI_BID_reg[0] ;
  output sig_txd_pf_event__1;
  output sig_txd_pe_event__1;
  output axis_wr_eop;
  output \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_reg ;
  output \gtxc.txc_str_Valid_reg ;
  output [0:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[4] ;
  output [0:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[4] ;
  output \gtxc.TXC_STATE_reg[0] ;
  output \gtxc.TXC_STATE_reg[1] ;
  input s_axi_aclk;
  input [31:0]Q;
  input [4:0]DIADI;
  input inverted_reset;
  input txd_wr_en;
  input start_wr;
  input \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_reg_0 ;
  input axi_str_txd_tready;
  input axis_wr_eop_d1;
  input [7:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[9]_0 ;
  input \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[5] ;
  input \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[5]_0 ;
  input axi_str_txc_tlast;
  input \gtxc.TXC_STATE_reg[1]_0 ;
  input sig_txd_prog_full_d1;
  input sig_txd_prog_empty_d1;
  input axi_str_txc_tready;
  input \gtxc.TXC_STATE_reg[0]_0 ;
  input axi_str_txc_tvalid;
  input [0:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[1] ;
  input \gtxc.txc_cntr_reg[1] ;

  wire [0:0]DI;
  wire [4:0]DIADI;
  wire [0:0]E;
  wire [31:0]Q;
  wire WR_RST;
  wire axi_str_txc_tlast;
  wire axi_str_txc_tready;
  wire axi_str_txc_tvalid;
  wire [32:0]\axi_str_txd_tdata[31] ;
  wire axi_str_txd_tready;
  wire axi_str_txd_tvalid;
  wire axis_rd_en__0;
  wire axis_wr_eop;
  wire axis_wr_eop_d1;
  wire clear;
  wire dout_i;
  wire [0:0]\gaxi_bid_gen.S_AXI_BID_reg[0] ;
  wire \gaxi_full_sm.w_ready_r_reg ;
  wire [0:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[4] ;
  wire \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[5] ;
  wire \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[5]_0 ;
  wire [0:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[9] ;
  wire [7:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[9]_0 ;
  wire \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_reg ;
  wire \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_reg_0 ;
  wire [0:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[1] ;
  wire [0:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[4] ;
  wire \gntv_or_sync_fifo.gl0.wr_n_35 ;
  wire \gntv_or_sync_fifo.gl0.wr_n_36 ;
  wire \gntv_or_sync_fifo.gl0.wr_n_37 ;
  wire \gntv_or_sync_fifo.gl0.wr_n_38 ;
  wire \gntv_or_sync_fifo.gl0.wr_n_39 ;
  wire \gntv_or_sync_fifo.gl0.wr_n_40 ;
  wire \gntv_or_sync_fifo.gl0.wr_n_41 ;
  wire \gntv_or_sync_fifo.gl0.wr_n_42 ;
  wire \gntv_or_sync_fifo.gl0.wr_n_43 ;
  wire \gntv_or_sync_fifo.gl0.wr_n_44 ;
  wire [0:0]\grss.gpe.rdpe/rd_pntr_inv_pad ;
  wire \gtxc.TXC_STATE_reg[0] ;
  wire \gtxc.TXC_STATE_reg[0]_0 ;
  wire \gtxc.TXC_STATE_reg[1] ;
  wire \gtxc.TXC_STATE_reg[1]_0 ;
  wire \gtxc.txc_cntr_reg[1] ;
  wire \gtxc.txc_str_Valid_reg ;
  wire [3:0]\gwss.wsts/c0/v1_reg ;
  wire [3:0]\gwss.wsts/c1/v1_reg ;
  wire [4:0]\gwss.wsts/gaf.c2/v1_reg ;
  wire inverted_reset;
  wire p_0_in__10;
  wire [8:0]p_0_out;
  wire [8:0]p_10_out;
  wire [7:0]p_11_out;
  wire p_16_out;
  wire p_6_out;
  wire p_7_out;
  wire p_8_out;
  wire ram_rd_en_i;
  wire rd_en_int_sync;
  wire rd_en_int_sync_1;
  wire [0:0]rd_rst_i;
  wire rst_full_ff_i;
  wire rst_full_gen_i;
  wire rst_int_sync;
  wire rst_int_sync_1;
  wire s_axi_aclk;
  wire \sig_register_array_reg[0][4] ;
  wire sig_txd_pe_event__1;
  wire sig_txd_pf_event__1;
  wire sig_txd_prog_empty;
  wire sig_txd_prog_empty_d1;
  wire sig_txd_prog_full_d1;
  wire start_wr;
  wire txd_wr_en;
  wire wr_en_int_sync;
  wire wr_en_int_sync_1;
  wire wr_en_into_bram;
  wire [8:0]wr_pntr_plus2;

  axi_fifo_mm_s_0_fifo_generator_v13_0_5_rd_logic \gntv_or_sync_fifo.gl0.rd 
       (.\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram (p_0_out),
        .DI(DI),
        .DIADI(DIADI[0]),
        .E(dout_i),
        .Q({clear,rd_rst_i}),
        .S({\gntv_or_sync_fifo.gl0.wr_n_41 ,\gntv_or_sync_fifo.gl0.wr_n_42 ,\gntv_or_sync_fifo.gl0.wr_n_43 ,\gntv_or_sync_fifo.gl0.wr_n_44 }),
        .axi_str_txc_tready(axi_str_txc_tready),
        .axi_str_txc_tvalid(axi_str_txc_tvalid),
        .axi_str_txd_tready(axi_str_txd_tready),
        .axi_str_txd_tvalid(axi_str_txd_tvalid),
        .axis_rd_en__0(axis_rd_en__0),
        .axis_wr_eop_d1(axis_wr_eop_d1),
        .\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[3] (\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[9]_0 [3:0]),
        .\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[5] (\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[5] ),
        .\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_reg (\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_reg ),
        .\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_reg_0 (\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_reg_0 ),
        .\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[1] (\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[1] ),
        .\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[4] (\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[4] ),
        .\gcc0.gc1.gsym.count_d1_reg[7] (p_11_out),
        .\gcc0.gc1.gsym.count_d2_reg[7] ({\gntv_or_sync_fifo.gl0.wr_n_37 ,\gntv_or_sync_fifo.gl0.wr_n_38 ,\gntv_or_sync_fifo.gl0.wr_n_39 ,\gntv_or_sync_fifo.gl0.wr_n_40 }),
        .\gcc0.gc1.gsym.count_d2_reg[8] (\gntv_or_sync_fifo.gl0.wr_n_36 ),
        .\gcc0.gc1.gsym.count_d2_reg[8]_0 (p_10_out),
        .\gcc0.gc1.gsym.count_d2_reg[8]_1 (\gntv_or_sync_fifo.gl0.wr_n_35 ),
        .\gcc0.gc1.gsym.count_reg[8] (wr_pntr_plus2),
        .\goreg_bm.dout_i_reg[0] (\axi_str_txd_tdata[31] [0]),
        .\gtxc.TXC_STATE_reg[0] (\gtxc.TXC_STATE_reg[0]_0 ),
        .\gtxc.TXC_STATE_reg[1] (\gtxc.TXC_STATE_reg[1]_0 ),
        .\gtxc.txc_str_Valid_reg (\gtxc.txc_str_Valid_reg ),
        .p_0_in__10(p_0_in__10),
        .p_16_out(p_16_out),
        .p_6_out(p_6_out),
        .p_8_out(p_8_out),
        .ram_full_i_reg(\gaxi_full_sm.w_ready_r_reg ),
        .ram_rd_en_i(ram_rd_en_i),
        .rd_en_int_sync(rd_en_int_sync),
        .rd_pntr_inv_pad(\grss.gpe.rdpe/rd_pntr_inv_pad ),
        .rst_int_sync(rst_int_sync),
        .rst_int_sync_1(rst_int_sync_1),
        .s_axi_aclk(s_axi_aclk),
        .sig_txd_pe_event__1(sig_txd_pe_event__1),
        .sig_txd_prog_empty(sig_txd_prog_empty),
        .sig_txd_prog_empty_d1(sig_txd_prog_empty_d1),
        .start_wr(start_wr),
        .txd_wr_en(txd_wr_en),
        .v1_reg(\gwss.wsts/c0/v1_reg ),
        .v1_reg_0(\gwss.wsts/c1/v1_reg ),
        .v1_reg_1(\gwss.wsts/gaf.c2/v1_reg ));
  axi_fifo_mm_s_0_fifo_generator_v13_0_5_wr_logic \gntv_or_sync_fifo.gl0.wr 
       (.AR(WR_RST),
        .\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram (p_10_out),
        .DIADI(DIADI[0]),
        .E(E),
        .Q(p_11_out),
        .S({\gntv_or_sync_fifo.gl0.wr_n_41 ,\gntv_or_sync_fifo.gl0.wr_n_42 ,\gntv_or_sync_fifo.gl0.wr_n_43 ,\gntv_or_sync_fifo.gl0.wr_n_44 }),
        .axis_rd_en__0(axis_rd_en__0),
        .axis_wr_eop(axis_wr_eop),
        .\gaxi_bid_gen.S_AXI_BID_reg[0] (\gaxi_bid_gen.S_AXI_BID_reg[0] ),
        .\gaxi_full_sm.w_ready_r_reg (\gaxi_full_sm.w_ready_r_reg ),
        .\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[5] (\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[5]_0 ),
        .\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[9] (\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[9]_0 ),
        .\gc0.count_d1_reg[6] (\gwss.wsts/c0/v1_reg ),
        .\gc0.count_d1_reg[6]_0 (\gwss.wsts/c1/v1_reg ),
        .\gc0.count_d1_reg[8] (p_0_out),
        .\gcc0.gc1.gsym.count_d1_reg[8] (wr_pntr_plus2),
        .\greg.gpcry_sym.diff_pntr_pad_reg[8] ({\gntv_or_sync_fifo.gl0.wr_n_37 ,\gntv_or_sync_fifo.gl0.wr_n_38 ,\gntv_or_sync_fifo.gl0.wr_n_39 ,\gntv_or_sync_fifo.gl0.wr_n_40 }),
        .\greg.gpcry_sym.diff_pntr_pad_reg[9] (\gntv_or_sync_fifo.gl0.wr_n_35 ),
        .\grstd1.grst_full.grst_f.rst_d5_reg (rst_full_gen_i),
        .out(rst_full_ff_i),
        .p_0_in__10(p_0_in__10),
        .p_16_out(p_16_out),
        .p_6_out(p_6_out),
        .p_7_out(p_7_out),
        .p_8_out(p_8_out),
        .ram_empty_fb_i_reg(\gntv_or_sync_fifo.gl0.wr_n_36 ),
        .rd_pntr_inv_pad(\grss.gpe.rdpe/rd_pntr_inv_pad ),
        .s_axi_aclk(s_axi_aclk),
        .sig_txd_pf_event__1(sig_txd_pf_event__1),
        .sig_txd_prog_full_d1(sig_txd_prog_full_d1),
        .start_wr(start_wr),
        .txd_wr_en(txd_wr_en),
        .v1_reg(\gwss.wsts/gaf.c2/v1_reg ));
  axi_fifo_mm_s_0_fifo_generator_v13_0_5_memory \gntv_or_sync_fifo.mem 
       (.DIADI(DIADI),
        .E(dout_i),
        .Q(Q),
        .WEBWE(wr_en_into_bram),
        .axi_str_txc_tlast(axi_str_txc_tlast),
        .axi_str_txc_tready(axi_str_txc_tready),
        .\axi_str_txd_tdata[31] (\axi_str_txd_tdata[31] ),
        .axi_str_txd_tready(axi_str_txd_tready),
        .axi_str_txd_tvalid(axi_str_txd_tvalid),
        .axis_wr_eop_d1(axis_wr_eop_d1),
        .\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[1] (\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[9]_0 [1]),
        .\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[4] (\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[4] ),
        .\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[9] (\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[9] ),
        .\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_reg (\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_reg_0 ),
        .\gc0.count_d1_reg[8] (p_0_out),
        .\gcc0.gc1.gsym.count_d2_reg[8] (p_10_out),
        .\gtxc.TXC_STATE_reg[0] (\gtxc.TXC_STATE_reg[0] ),
        .\gtxc.TXC_STATE_reg[0]_0 (\gtxc.TXC_STATE_reg[0]_0 ),
        .\gtxc.TXC_STATE_reg[1] (\gtxc.TXC_STATE_reg[1] ),
        .\gtxc.TXC_STATE_reg[1]_0 (\gtxc.TXC_STATE_reg[1]_0 ),
        .\gtxc.txc_cntr_reg[1] (\gtxc.txc_cntr_reg[1] ),
        .p_8_out(p_8_out),
        .ram_rd_en_i(ram_rd_en_i),
        .s_axi_aclk(s_axi_aclk),
        .\sig_register_array_reg[0][4] (\sig_register_array_reg[0][4] ));
  FDRE #(
    .INIT(1'b0)) 
    \gsafety_cc.rd_en_int_sync_1_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(p_6_out),
        .Q(rd_en_int_sync_1),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \gsafety_cc.rd_en_int_sync_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(rd_en_int_sync_1),
        .Q(rd_en_int_sync),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \gsafety_cc.rst_int_sync_1_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(rd_rst_i),
        .Q(rst_int_sync_1),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \gsafety_cc.rst_int_sync_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(rst_int_sync_1),
        .Q(rst_int_sync),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \gsafety_cc.wr_en_int_sync_1_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(p_16_out),
        .Q(wr_en_int_sync_1),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \gsafety_cc.wr_en_int_sync_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(wr_en_int_sync_1),
        .Q(wr_en_int_sync),
        .R(1'b0));
  axi_fifo_mm_s_0_fifo_generator_v13_0_5_reset_blk_ramfifo__parameterized0 rstblk
       (.AR(WR_RST),
        .Q({clear,rd_rst_i}),
        .WEBWE(wr_en_into_bram),
        .\gaf.gaf0.ram_afull_i_reg (rst_full_gen_i),
        .inverted_reset(inverted_reset),
        .out(rst_full_ff_i),
        .p_16_out(p_16_out),
        .rst_int_sync(rst_int_sync),
        .rst_int_sync_1(rst_int_sync_1),
        .s_axi_aclk(s_axi_aclk),
        .wr_en_int_sync(wr_en_int_sync));
endmodule

(* ORIG_REF_NAME = "fifo_generator_v13_0_5_fifo_generator_ramfifo" *) 
module axi_fifo_mm_s_0_fifo_generator_v13_0_5_fifo_generator_ramfifo__parameterized0
   (\grxd.fg_rxd_wr_length_reg[1] ,
    empty_fwft_i,
    p_9_out,
    p_8_out,
    \grxd.fg_rxd_wr_length_reg[21] ,
    rx_complete,
    rx_str_wr_en,
    \grxd.sig_rxd_rd_data_reg[32] ,
    \gc0.count_reg[0] ,
    \gc0.count_reg[0]_0 ,
    axi_str_rxd_tready,
    DI,
    Q,
    \sig_register_array_reg[0][2] ,
    \sig_register_array_reg[0][1] ,
    \sig_register_array_reg[0][2]_0 ,
    sig_rxd_pf_event__1,
    sig_rxd_pe_event__1,
    S,
    \count_reg[4] ,
    \count_reg[8] ,
    \grxd.fg_rxd_wr_length_reg[2] ,
    s_axi4_rdata,
    s_axi_aclk,
    axi_str_rxd_tdata,
    axi_str_rxd_tlast,
    sync_areset_n_reg_inv,
    axis_rd_en__0,
    axi_str_rxd_tvalid,
    rx_len_wr_en,
    s_axi_aresetn,
    sig_str_rst_reg,
    sig_rxd_rd_data,
    axi4_fifo_rd_en_i,
    sig_rd_rlen_reg,
    \gaxi_full_sm.r_valid_r_reg ,
    \gaxi_full_sm.r_last_r_reg ,
    sig_axi_rd_en,
    \s_axi4_araddr[21] ,
    \s_axi4_araddr[29] ,
    s_axi4_araddr,
    sig_rx_channel_reset_reg,
    sig_rxd_prog_full_d1,
    sig_rxd_prog_empty_d1,
    O,
    \grxd.fg_rxd_wr_length_reg[2]_0 ,
    fg_rxd_wr_length,
    D);
  output \grxd.fg_rxd_wr_length_reg[1] ;
  output empty_fwft_i;
  output p_9_out;
  output p_8_out;
  output \grxd.fg_rxd_wr_length_reg[21] ;
  output rx_complete;
  output rx_str_wr_en;
  output \grxd.sig_rxd_rd_data_reg[32] ;
  output \gc0.count_reg[0] ;
  output \gc0.count_reg[0]_0 ;
  output axi_str_rxd_tready;
  output [3:0]DI;
  output [6:0]Q;
  output \sig_register_array_reg[0][2] ;
  output \sig_register_array_reg[0][1] ;
  output \sig_register_array_reg[0][2]_0 ;
  output sig_rxd_pf_event__1;
  output sig_rxd_pe_event__1;
  output [0:0]S;
  output [3:0]\count_reg[4] ;
  output [3:0]\count_reg[8] ;
  output \grxd.fg_rxd_wr_length_reg[2] ;
  output [31:0]s_axi4_rdata;
  input s_axi_aclk;
  input [31:0]axi_str_rxd_tdata;
  input axi_str_rxd_tlast;
  input sync_areset_n_reg_inv;
  input axis_rd_en__0;
  input axi_str_rxd_tvalid;
  input rx_len_wr_en;
  input s_axi_aresetn;
  input sig_str_rst_reg;
  input [0:0]sig_rxd_rd_data;
  input axi4_fifo_rd_en_i;
  input sig_rd_rlen_reg;
  input \gaxi_full_sm.r_valid_r_reg ;
  input \gaxi_full_sm.r_last_r_reg ;
  input sig_axi_rd_en;
  input \s_axi4_araddr[21] ;
  input \s_axi4_araddr[29] ;
  input [11:0]s_axi4_araddr;
  input sig_rx_channel_reset_reg;
  input sig_rxd_prog_full_d1;
  input sig_rxd_prog_empty_d1;
  input [0:0]O;
  input [0:0]\grxd.fg_rxd_wr_length_reg[2]_0 ;
  input [0:0]fg_rxd_wr_length;
  input [8:0]D;

  wire [8:0]D;
  wire [3:0]DI;
  wire [0:0]O;
  wire [6:0]Q;
  wire [0:0]S;
  wire axi4_fifo_rd_en_i;
  wire [31:0]axi_str_rxd_tdata;
  wire axi_str_rxd_tlast;
  wire axi_str_rxd_tready;
  wire axi_str_rxd_tvalid;
  wire axis_rd_en__0;
  wire [3:0]\count_reg[4] ;
  wire [3:0]\count_reg[8] ;
  wire dout_i;
  wire empty_fwft_i;
  wire [0:0]fg_rxd_wr_length;
  wire \gaxi_full_sm.r_last_r_reg ;
  wire \gaxi_full_sm.r_valid_r_reg ;
  wire \gc0.count_reg[0] ;
  wire \gc0.count_reg[0]_0 ;
  wire \gntv_or_sync_fifo.gl0.rd_n_41 ;
  wire \gntv_or_sync_fifo.gl0.wr_n_28 ;
  wire \gntv_or_sync_fifo.gl0.wr_n_29 ;
  wire \gntv_or_sync_fifo.gl0.wr_n_30 ;
  wire \gntv_or_sync_fifo.gl0.wr_n_31 ;
  wire \gntv_or_sync_fifo.gl0.wr_n_32 ;
  wire \gntv_or_sync_fifo.gl0.wr_n_33 ;
  wire \gntv_or_sync_fifo.gl0.wr_n_34 ;
  wire \gntv_or_sync_fifo.gl0.wr_n_35 ;
  wire \gntv_or_sync_fifo.gl0.wr_n_36 ;
  wire \gntv_or_sync_fifo.gl0.wr_n_37 ;
  wire \gr1.gdcf.dc/cntr_en ;
  wire [0:0]\grss.gpe.rdpe/rd_pntr_inv_pad ;
  wire \grxd.fg_rxd_wr_length_reg[1] ;
  wire \grxd.fg_rxd_wr_length_reg[21] ;
  wire \grxd.fg_rxd_wr_length_reg[2] ;
  wire [0:0]\grxd.fg_rxd_wr_length_reg[2]_0 ;
  wire \grxd.sig_rxd_rd_data_reg[32] ;
  wire \gsafety_cc.rd_en_int_sync_1_reg_n_0 ;
  wire \gsafety_cc.wr_en_int_sync_1_reg_n_0 ;
  wire [3:0]\gwss.wsts/c0/v1_reg ;
  wire [4:0]\gwss.wsts/c1/v1_reg ;
  wire [8:0]p_0_out;
  wire [8:0]p_10_out;
  wire [8:0]p_11_out;
  wire p_16_out;
  wire p_6_out;
  wire p_8_out;
  wire p_9_out;
  wire ram_rd_en_i;
  wire rd_en_int_sync;
  wire [0:0]rd_rst_i;
  wire rst_full_ff_i;
  wire rst_full_gen_i;
  wire rst_int_sync;
  wire rst_int_sync_1;
  wire rstblk_n_2;
  wire rstblk_n_4;
  wire rx_complete;
  wire rx_len_wr_en;
  wire rx_str_wr_en;
  wire [11:0]s_axi4_araddr;
  wire \s_axi4_araddr[21] ;
  wire \s_axi4_araddr[29] ;
  wire [31:0]s_axi4_rdata;
  wire s_axi_aclk;
  wire s_axi_aresetn;
  wire sig_axi_rd_en;
  wire sig_rd_rlen_reg;
  wire \sig_register_array_reg[0][1] ;
  wire \sig_register_array_reg[0][2] ;
  wire \sig_register_array_reg[0][2]_0 ;
  wire sig_rx_channel_reset_reg;
  wire sig_rxd_pe_event__1;
  wire sig_rxd_pf_event__1;
  wire sig_rxd_prog_empty_d1;
  wire sig_rxd_prog_full_d1;
  wire [0:0]sig_rxd_rd_data;
  wire sig_str_rst_reg;
  wire sync_areset_n_reg_inv;
  wire valid_fwft;
  wire wr_en_int_sync;
  wire wr_en_into_bram;

  axi_fifo_mm_s_0_fifo_generator_v13_0_5_rd_logic_5 \gntv_or_sync_fifo.gl0.rd 
       (.D(D),
        .\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram (p_0_out),
        .DI(DI),
        .E(dout_i),
        .Q({rstblk_n_4,rd_rst_i}),
        .S({\gntv_or_sync_fifo.gl0.wr_n_34 ,\gntv_or_sync_fifo.gl0.wr_n_35 ,\gntv_or_sync_fifo.gl0.wr_n_36 ,\gntv_or_sync_fifo.gl0.wr_n_37 }),
        .axis_rd_en__0(axis_rd_en__0),
        .\count_reg[4] (\count_reg[4] ),
        .\count_reg[8] (\count_reg[8] ),
        .\count_reg[9] (S),
        .empty_fwft_i(empty_fwft_i),
        .\gaxi_full_sm.r_last_r_reg (\gaxi_full_sm.r_last_r_reg ),
        .\gaxi_full_sm.r_valid_r_reg (\gaxi_full_sm.r_valid_r_reg ),
        .\gc0.count_reg[0] (\gc0.count_reg[0] ),
        .\gc0.count_reg[0]_0 (\gc0.count_reg[0]_0 ),
        .\gcc0.gc0.count_d1_reg[7] ({\gntv_or_sync_fifo.gl0.wr_n_30 ,\gntv_or_sync_fifo.gl0.wr_n_31 ,\gntv_or_sync_fifo.gl0.wr_n_32 ,\gntv_or_sync_fifo.gl0.wr_n_33 }),
        .\gcc0.gc0.count_d1_reg[8] (\gntv_or_sync_fifo.gl0.wr_n_29 ),
        .\gcc0.gc0.count_d1_reg[8]_0 (p_10_out),
        .\gcc0.gc0.count_d1_reg[8]_1 (\gntv_or_sync_fifo.gl0.wr_n_28 ),
        .\gcc0.gc0.count_reg[8] (p_11_out),
        .\gdiff.gcry_1_sym.diff_pntr_pad_reg[9] (\gntv_or_sync_fifo.gl0.rd_n_41 ),
        .\gpregsm1.user_valid_reg (\gr1.gdcf.dc/cntr_en ),
        .p_16_out(p_16_out),
        .p_6_out(p_6_out),
        .p_9_out(p_9_out),
        .ram_rd_en_i(ram_rd_en_i),
        .rd_en_int_sync(rd_en_int_sync),
        .rd_pntr_inv_pad(\grss.gpe.rdpe/rd_pntr_inv_pad ),
        .rst_int_sync(rst_int_sync),
        .rst_int_sync_1(rst_int_sync_1),
        .s_axi4_araddr(s_axi4_araddr),
        .\s_axi4_araddr[21] (\s_axi4_araddr[21] ),
        .\s_axi4_araddr[29] (\s_axi4_araddr[29] ),
        .s_axi_aclk(s_axi_aclk),
        .sig_axi_rd_en(sig_axi_rd_en),
        .\sig_ip2bus_data_reg[20] (Q),
        .\sig_register_array_reg[0][1] (\sig_register_array_reg[0][1] ),
        .\sig_register_array_reg[0][2] (\sig_register_array_reg[0][2] ),
        .\sig_register_array_reg[0][2]_0 (\sig_register_array_reg[0][2]_0 ),
        .sig_rx_channel_reset_reg(sig_rx_channel_reset_reg),
        .sig_rxd_pe_event__1(sig_rxd_pe_event__1),
        .sig_rxd_prog_empty_d1(sig_rxd_prog_empty_d1),
        .sig_rxd_rd_data(sig_rxd_rd_data),
        .v1_reg(\gwss.wsts/c1/v1_reg ),
        .v1_reg_0(\gwss.wsts/c0/v1_reg ),
        .valid_fwft(valid_fwft));
  axi_fifo_mm_s_0_fifo_generator_v13_0_5_wr_logic__parameterized0 \gntv_or_sync_fifo.gl0.wr 
       (.AR(rstblk_n_2),
        .\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram (p_10_out),
        .O(O),
        .Q(p_11_out),
        .S({\gntv_or_sync_fifo.gl0.wr_n_34 ,\gntv_or_sync_fifo.gl0.wr_n_35 ,\gntv_or_sync_fifo.gl0.wr_n_36 ,\gntv_or_sync_fifo.gl0.wr_n_37 }),
        .axi_str_rxd_tlast(axi_str_rxd_tlast),
        .axi_str_rxd_tready(axi_str_rxd_tready),
        .axi_str_rxd_tvalid(axi_str_rxd_tvalid),
        .axis_rd_en__0(axis_rd_en__0),
        .\count_reg[9] (\gr1.gdcf.dc/cntr_en ),
        .fg_rxd_wr_length(fg_rxd_wr_length),
        .\gc0.count_d1_reg[6] (\gwss.wsts/c0/v1_reg ),
        .\gc0.count_d1_reg[8] (\gntv_or_sync_fifo.gl0.rd_n_41 ),
        .\gc0.count_d1_reg[8]_0 (p_0_out),
        .\greg.gpcry_sym.diff_pntr_pad_reg[8] ({\gntv_or_sync_fifo.gl0.wr_n_30 ,\gntv_or_sync_fifo.gl0.wr_n_31 ,\gntv_or_sync_fifo.gl0.wr_n_32 ,\gntv_or_sync_fifo.gl0.wr_n_33 }),
        .\greg.gpcry_sym.diff_pntr_pad_reg[9] (\gntv_or_sync_fifo.gl0.wr_n_28 ),
        .\grstd1.grst_full.grst_f.rst_d5_reg (rst_full_gen_i),
        .\grxd.fg_rxd_wr_length_reg[1] (\grxd.fg_rxd_wr_length_reg[1] ),
        .\grxd.fg_rxd_wr_length_reg[21] (\grxd.fg_rxd_wr_length_reg[21] ),
        .\grxd.fg_rxd_wr_length_reg[2] (\grxd.fg_rxd_wr_length_reg[2] ),
        .\grxd.fg_rxd_wr_length_reg[2]_0 (\grxd.fg_rxd_wr_length_reg[2]_0 ),
        .out(rst_full_ff_i),
        .p_16_out(p_16_out),
        .p_6_out(p_6_out),
        .p_8_out(p_8_out),
        .ram_empty_fb_i_reg(\gntv_or_sync_fifo.gl0.wr_n_29 ),
        .rd_pntr_inv_pad(\grss.gpe.rdpe/rd_pntr_inv_pad ),
        .rx_complete(rx_complete),
        .rx_len_wr_en(rx_len_wr_en),
        .rx_str_wr_en(rx_str_wr_en),
        .s_axi_aclk(s_axi_aclk),
        .s_axi_aresetn(s_axi_aresetn),
        .sig_rxd_pf_event__1(sig_rxd_pf_event__1),
        .sig_rxd_prog_full_d1(sig_rxd_prog_full_d1),
        .sig_str_rst_reg(sig_str_rst_reg),
        .v1_reg(\gwss.wsts/c1/v1_reg ),
        .valid_fwft(valid_fwft));
  axi_fifo_mm_s_0_fifo_generator_v13_0_5_memory_6 \gntv_or_sync_fifo.mem 
       (.E(dout_i),
        .WEBWE(wr_en_into_bram),
        .axi4_fifo_rd_en_i(axi4_fifo_rd_en_i),
        .axi_str_rxd_tdata(axi_str_rxd_tdata),
        .axi_str_rxd_tlast(axi_str_rxd_tlast),
        .\gc0.count_d1_reg[8] (p_0_out),
        .\gcc0.gc0.count_d1_reg[8] (p_10_out),
        .\grxd.sig_rxd_rd_data_reg[32] (\grxd.sig_rxd_rd_data_reg[32] ),
        .ram_rd_en_i(ram_rd_en_i),
        .s_axi4_rdata(s_axi4_rdata),
        .s_axi_aclk(s_axi_aclk),
        .s_axi_aresetn(s_axi_aresetn),
        .sig_rd_rlen_reg(sig_rd_rlen_reg),
        .sig_rxd_rd_data(sig_rxd_rd_data),
        .sig_str_rst_reg(sig_str_rst_reg));
  FDRE #(
    .INIT(1'b0)) 
    \gsafety_cc.rd_en_int_sync_1_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(p_6_out),
        .Q(\gsafety_cc.rd_en_int_sync_1_reg_n_0 ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \gsafety_cc.rd_en_int_sync_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(\gsafety_cc.rd_en_int_sync_1_reg_n_0 ),
        .Q(rd_en_int_sync),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \gsafety_cc.rst_int_sync_1_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(rd_rst_i),
        .Q(rst_int_sync_1),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \gsafety_cc.rst_int_sync_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(rst_int_sync_1),
        .Q(rst_int_sync),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \gsafety_cc.wr_en_int_sync_1_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(p_16_out),
        .Q(\gsafety_cc.wr_en_int_sync_1_reg_n_0 ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \gsafety_cc.wr_en_int_sync_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(\gsafety_cc.wr_en_int_sync_1_reg_n_0 ),
        .Q(wr_en_int_sync),
        .R(1'b0));
  axi_fifo_mm_s_0_fifo_generator_v13_0_5_reset_blk_ramfifo__parameterized1 rstblk
       (.AR(rstblk_n_2),
        .Q({rstblk_n_4,rd_rst_i}),
        .WEBWE(wr_en_into_bram),
        .out(rst_full_ff_i),
        .p_16_out(p_16_out),
        .ram_full_i_reg(rst_full_gen_i),
        .rst_int_sync(rst_int_sync),
        .rst_int_sync_1(rst_int_sync_1),
        .s_axi_aclk(s_axi_aclk),
        .sync_areset_n_reg_inv(sync_areset_n_reg_inv),
        .wr_en_int_sync(wr_en_int_sync));
endmodule

(* ORIG_REF_NAME = "fifo_generator_v13_0_5_fifo_generator_top" *) 
module axi_fifo_mm_s_0_fifo_generator_v13_0_5_fifo_generator_top
   (\gaxi_full_sm.w_ready_r_reg ,
    sig_txd_prog_empty,
    p_7_out,
    E,
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[9] ,
    \axi_str_txd_tdata[31] ,
    DI,
    \sig_register_array_reg[0][4] ,
    axi_str_txd_tvalid,
    \gaxi_bid_gen.S_AXI_BID_reg[0] ,
    sig_txd_pf_event__1,
    sig_txd_pe_event__1,
    axis_wr_eop,
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_reg ,
    \gtxc.txc_str_Valid_reg ,
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[4] ,
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[4] ,
    \gtxc.TXC_STATE_reg[0] ,
    \gtxc.TXC_STATE_reg[1] ,
    s_axi_aclk,
    Q,
    DIADI,
    inverted_reset,
    txd_wr_en,
    start_wr,
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_reg_0 ,
    axi_str_txd_tready,
    axis_wr_eop_d1,
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[9]_0 ,
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[5] ,
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[5]_0 ,
    axi_str_txc_tlast,
    \gtxc.TXC_STATE_reg[1]_0 ,
    sig_txd_prog_full_d1,
    sig_txd_prog_empty_d1,
    axi_str_txc_tready,
    \gtxc.TXC_STATE_reg[0]_0 ,
    axi_str_txc_tvalid,
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[1] ,
    \gtxc.txc_cntr_reg[1] );
  output \gaxi_full_sm.w_ready_r_reg ;
  output sig_txd_prog_empty;
  output p_7_out;
  output [0:0]E;
  output [0:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[9] ;
  output [32:0]\axi_str_txd_tdata[31] ;
  output [0:0]DI;
  output \sig_register_array_reg[0][4] ;
  output axi_str_txd_tvalid;
  output [0:0]\gaxi_bid_gen.S_AXI_BID_reg[0] ;
  output sig_txd_pf_event__1;
  output sig_txd_pe_event__1;
  output axis_wr_eop;
  output \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_reg ;
  output \gtxc.txc_str_Valid_reg ;
  output [0:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[4] ;
  output [0:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[4] ;
  output \gtxc.TXC_STATE_reg[0] ;
  output \gtxc.TXC_STATE_reg[1] ;
  input s_axi_aclk;
  input [31:0]Q;
  input [4:0]DIADI;
  input inverted_reset;
  input txd_wr_en;
  input start_wr;
  input \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_reg_0 ;
  input axi_str_txd_tready;
  input axis_wr_eop_d1;
  input [7:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[9]_0 ;
  input \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[5] ;
  input \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[5]_0 ;
  input axi_str_txc_tlast;
  input \gtxc.TXC_STATE_reg[1]_0 ;
  input sig_txd_prog_full_d1;
  input sig_txd_prog_empty_d1;
  input axi_str_txc_tready;
  input \gtxc.TXC_STATE_reg[0]_0 ;
  input axi_str_txc_tvalid;
  input [0:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[1] ;
  input \gtxc.txc_cntr_reg[1] ;

  wire [0:0]DI;
  wire [4:0]DIADI;
  wire [0:0]E;
  wire [31:0]Q;
  wire axi_str_txc_tlast;
  wire axi_str_txc_tready;
  wire axi_str_txc_tvalid;
  wire [32:0]\axi_str_txd_tdata[31] ;
  wire axi_str_txd_tready;
  wire axi_str_txd_tvalid;
  wire axis_wr_eop;
  wire axis_wr_eop_d1;
  wire [0:0]\gaxi_bid_gen.S_AXI_BID_reg[0] ;
  wire \gaxi_full_sm.w_ready_r_reg ;
  wire [0:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[4] ;
  wire \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[5] ;
  wire \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[5]_0 ;
  wire [0:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[9] ;
  wire [7:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[9]_0 ;
  wire \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_reg ;
  wire \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_reg_0 ;
  wire [0:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[1] ;
  wire [0:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[4] ;
  wire \gtxc.TXC_STATE_reg[0] ;
  wire \gtxc.TXC_STATE_reg[0]_0 ;
  wire \gtxc.TXC_STATE_reg[1] ;
  wire \gtxc.TXC_STATE_reg[1]_0 ;
  wire \gtxc.txc_cntr_reg[1] ;
  wire \gtxc.txc_str_Valid_reg ;
  wire inverted_reset;
  wire p_7_out;
  wire s_axi_aclk;
  wire \sig_register_array_reg[0][4] ;
  wire sig_txd_pe_event__1;
  wire sig_txd_pf_event__1;
  wire sig_txd_prog_empty;
  wire sig_txd_prog_empty_d1;
  wire sig_txd_prog_full_d1;
  wire start_wr;
  wire txd_wr_en;

  axi_fifo_mm_s_0_fifo_generator_v13_0_5_fifo_generator_ramfifo \grf.rf 
       (.DI(DI),
        .DIADI(DIADI),
        .E(E),
        .Q(Q),
        .axi_str_txc_tlast(axi_str_txc_tlast),
        .axi_str_txc_tready(axi_str_txc_tready),
        .axi_str_txc_tvalid(axi_str_txc_tvalid),
        .\axi_str_txd_tdata[31] (\axi_str_txd_tdata[31] ),
        .axi_str_txd_tready(axi_str_txd_tready),
        .axi_str_txd_tvalid(axi_str_txd_tvalid),
        .axis_wr_eop(axis_wr_eop),
        .axis_wr_eop_d1(axis_wr_eop_d1),
        .\gaxi_bid_gen.S_AXI_BID_reg[0] (\gaxi_bid_gen.S_AXI_BID_reg[0] ),
        .\gaxi_full_sm.w_ready_r_reg (\gaxi_full_sm.w_ready_r_reg ),
        .\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[4] (\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[4] ),
        .\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[5] (\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[5] ),
        .\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[5]_0 (\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[5]_0 ),
        .\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[9] (\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[9] ),
        .\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[9]_0 (\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[9]_0 ),
        .\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_reg (\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_reg ),
        .\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_reg_0 (\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_reg_0 ),
        .\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[1] (\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[1] ),
        .\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[4] (\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[4] ),
        .\gtxc.TXC_STATE_reg[0] (\gtxc.TXC_STATE_reg[0] ),
        .\gtxc.TXC_STATE_reg[0]_0 (\gtxc.TXC_STATE_reg[0]_0 ),
        .\gtxc.TXC_STATE_reg[1] (\gtxc.TXC_STATE_reg[1] ),
        .\gtxc.TXC_STATE_reg[1]_0 (\gtxc.TXC_STATE_reg[1]_0 ),
        .\gtxc.txc_cntr_reg[1] (\gtxc.txc_cntr_reg[1] ),
        .\gtxc.txc_str_Valid_reg (\gtxc.txc_str_Valid_reg ),
        .inverted_reset(inverted_reset),
        .p_7_out(p_7_out),
        .s_axi_aclk(s_axi_aclk),
        .\sig_register_array_reg[0][4] (\sig_register_array_reg[0][4] ),
        .sig_txd_pe_event__1(sig_txd_pe_event__1),
        .sig_txd_pf_event__1(sig_txd_pf_event__1),
        .sig_txd_prog_empty(sig_txd_prog_empty),
        .sig_txd_prog_empty_d1(sig_txd_prog_empty_d1),
        .sig_txd_prog_full_d1(sig_txd_prog_full_d1),
        .start_wr(start_wr),
        .txd_wr_en(txd_wr_en));
endmodule

(* ORIG_REF_NAME = "fifo_generator_v13_0_5_fifo_generator_top" *) 
module axi_fifo_mm_s_0_fifo_generator_v13_0_5_fifo_generator_top__parameterized0
   (\grxd.fg_rxd_wr_length_reg[1] ,
    empty_fwft_i,
    p_9_out,
    p_8_out,
    \grxd.fg_rxd_wr_length_reg[21] ,
    rx_complete,
    rx_str_wr_en,
    \grxd.sig_rxd_rd_data_reg[32] ,
    \gc0.count_reg[0] ,
    \gc0.count_reg[0]_0 ,
    axi_str_rxd_tready,
    DI,
    Q,
    \sig_register_array_reg[0][2] ,
    \sig_register_array_reg[0][1] ,
    \sig_register_array_reg[0][2]_0 ,
    sig_rxd_pf_event__1,
    sig_rxd_pe_event__1,
    S,
    \count_reg[4] ,
    \count_reg[8] ,
    \grxd.fg_rxd_wr_length_reg[2] ,
    s_axi4_rdata,
    s_axi_aclk,
    axi_str_rxd_tdata,
    axi_str_rxd_tlast,
    sync_areset_n_reg_inv,
    axis_rd_en__0,
    axi_str_rxd_tvalid,
    rx_len_wr_en,
    s_axi_aresetn,
    sig_str_rst_reg,
    sig_rxd_rd_data,
    axi4_fifo_rd_en_i,
    sig_rd_rlen_reg,
    \gaxi_full_sm.r_valid_r_reg ,
    \gaxi_full_sm.r_last_r_reg ,
    sig_axi_rd_en,
    \s_axi4_araddr[21] ,
    \s_axi4_araddr[29] ,
    s_axi4_araddr,
    sig_rx_channel_reset_reg,
    sig_rxd_prog_full_d1,
    sig_rxd_prog_empty_d1,
    O,
    \grxd.fg_rxd_wr_length_reg[2]_0 ,
    fg_rxd_wr_length,
    D);
  output \grxd.fg_rxd_wr_length_reg[1] ;
  output empty_fwft_i;
  output p_9_out;
  output p_8_out;
  output \grxd.fg_rxd_wr_length_reg[21] ;
  output rx_complete;
  output rx_str_wr_en;
  output \grxd.sig_rxd_rd_data_reg[32] ;
  output \gc0.count_reg[0] ;
  output \gc0.count_reg[0]_0 ;
  output axi_str_rxd_tready;
  output [3:0]DI;
  output [6:0]Q;
  output \sig_register_array_reg[0][2] ;
  output \sig_register_array_reg[0][1] ;
  output \sig_register_array_reg[0][2]_0 ;
  output sig_rxd_pf_event__1;
  output sig_rxd_pe_event__1;
  output [0:0]S;
  output [3:0]\count_reg[4] ;
  output [3:0]\count_reg[8] ;
  output \grxd.fg_rxd_wr_length_reg[2] ;
  output [31:0]s_axi4_rdata;
  input s_axi_aclk;
  input [31:0]axi_str_rxd_tdata;
  input axi_str_rxd_tlast;
  input sync_areset_n_reg_inv;
  input axis_rd_en__0;
  input axi_str_rxd_tvalid;
  input rx_len_wr_en;
  input s_axi_aresetn;
  input sig_str_rst_reg;
  input [0:0]sig_rxd_rd_data;
  input axi4_fifo_rd_en_i;
  input sig_rd_rlen_reg;
  input \gaxi_full_sm.r_valid_r_reg ;
  input \gaxi_full_sm.r_last_r_reg ;
  input sig_axi_rd_en;
  input \s_axi4_araddr[21] ;
  input \s_axi4_araddr[29] ;
  input [11:0]s_axi4_araddr;
  input sig_rx_channel_reset_reg;
  input sig_rxd_prog_full_d1;
  input sig_rxd_prog_empty_d1;
  input [0:0]O;
  input [0:0]\grxd.fg_rxd_wr_length_reg[2]_0 ;
  input [0:0]fg_rxd_wr_length;
  input [8:0]D;

  wire [8:0]D;
  wire [3:0]DI;
  wire [0:0]O;
  wire [6:0]Q;
  wire [0:0]S;
  wire axi4_fifo_rd_en_i;
  wire [31:0]axi_str_rxd_tdata;
  wire axi_str_rxd_tlast;
  wire axi_str_rxd_tready;
  wire axi_str_rxd_tvalid;
  wire axis_rd_en__0;
  wire [3:0]\count_reg[4] ;
  wire [3:0]\count_reg[8] ;
  wire empty_fwft_i;
  wire [0:0]fg_rxd_wr_length;
  wire \gaxi_full_sm.r_last_r_reg ;
  wire \gaxi_full_sm.r_valid_r_reg ;
  wire \gc0.count_reg[0] ;
  wire \gc0.count_reg[0]_0 ;
  wire \grxd.fg_rxd_wr_length_reg[1] ;
  wire \grxd.fg_rxd_wr_length_reg[21] ;
  wire \grxd.fg_rxd_wr_length_reg[2] ;
  wire [0:0]\grxd.fg_rxd_wr_length_reg[2]_0 ;
  wire \grxd.sig_rxd_rd_data_reg[32] ;
  wire p_8_out;
  wire p_9_out;
  wire rx_complete;
  wire rx_len_wr_en;
  wire rx_str_wr_en;
  wire [11:0]s_axi4_araddr;
  wire \s_axi4_araddr[21] ;
  wire \s_axi4_araddr[29] ;
  wire [31:0]s_axi4_rdata;
  wire s_axi_aclk;
  wire s_axi_aresetn;
  wire sig_axi_rd_en;
  wire sig_rd_rlen_reg;
  wire \sig_register_array_reg[0][1] ;
  wire \sig_register_array_reg[0][2] ;
  wire \sig_register_array_reg[0][2]_0 ;
  wire sig_rx_channel_reset_reg;
  wire sig_rxd_pe_event__1;
  wire sig_rxd_pf_event__1;
  wire sig_rxd_prog_empty_d1;
  wire sig_rxd_prog_full_d1;
  wire [0:0]sig_rxd_rd_data;
  wire sig_str_rst_reg;
  wire sync_areset_n_reg_inv;

  axi_fifo_mm_s_0_fifo_generator_v13_0_5_fifo_generator_ramfifo__parameterized0 \grf.rf 
       (.D(D),
        .DI(DI),
        .O(O),
        .Q(Q),
        .S(S),
        .axi4_fifo_rd_en_i(axi4_fifo_rd_en_i),
        .axi_str_rxd_tdata(axi_str_rxd_tdata),
        .axi_str_rxd_tlast(axi_str_rxd_tlast),
        .axi_str_rxd_tready(axi_str_rxd_tready),
        .axi_str_rxd_tvalid(axi_str_rxd_tvalid),
        .axis_rd_en__0(axis_rd_en__0),
        .\count_reg[4] (\count_reg[4] ),
        .\count_reg[8] (\count_reg[8] ),
        .empty_fwft_i(empty_fwft_i),
        .fg_rxd_wr_length(fg_rxd_wr_length),
        .\gaxi_full_sm.r_last_r_reg (\gaxi_full_sm.r_last_r_reg ),
        .\gaxi_full_sm.r_valid_r_reg (\gaxi_full_sm.r_valid_r_reg ),
        .\gc0.count_reg[0] (\gc0.count_reg[0] ),
        .\gc0.count_reg[0]_0 (\gc0.count_reg[0]_0 ),
        .\grxd.fg_rxd_wr_length_reg[1] (\grxd.fg_rxd_wr_length_reg[1] ),
        .\grxd.fg_rxd_wr_length_reg[21] (\grxd.fg_rxd_wr_length_reg[21] ),
        .\grxd.fg_rxd_wr_length_reg[2] (\grxd.fg_rxd_wr_length_reg[2] ),
        .\grxd.fg_rxd_wr_length_reg[2]_0 (\grxd.fg_rxd_wr_length_reg[2]_0 ),
        .\grxd.sig_rxd_rd_data_reg[32] (\grxd.sig_rxd_rd_data_reg[32] ),
        .p_8_out(p_8_out),
        .p_9_out(p_9_out),
        .rx_complete(rx_complete),
        .rx_len_wr_en(rx_len_wr_en),
        .rx_str_wr_en(rx_str_wr_en),
        .s_axi4_araddr(s_axi4_araddr),
        .\s_axi4_araddr[21] (\s_axi4_araddr[21] ),
        .\s_axi4_araddr[29] (\s_axi4_araddr[29] ),
        .s_axi4_rdata(s_axi4_rdata),
        .s_axi_aclk(s_axi_aclk),
        .s_axi_aresetn(s_axi_aresetn),
        .sig_axi_rd_en(sig_axi_rd_en),
        .sig_rd_rlen_reg(sig_rd_rlen_reg),
        .\sig_register_array_reg[0][1] (\sig_register_array_reg[0][1] ),
        .\sig_register_array_reg[0][2] (\sig_register_array_reg[0][2] ),
        .\sig_register_array_reg[0][2]_0 (\sig_register_array_reg[0][2]_0 ),
        .sig_rx_channel_reset_reg(sig_rx_channel_reset_reg),
        .sig_rxd_pe_event__1(sig_rxd_pe_event__1),
        .sig_rxd_pf_event__1(sig_rxd_pf_event__1),
        .sig_rxd_prog_empty_d1(sig_rxd_prog_empty_d1),
        .sig_rxd_prog_full_d1(sig_rxd_prog_full_d1),
        .sig_rxd_rd_data(sig_rxd_rd_data),
        .sig_str_rst_reg(sig_str_rst_reg),
        .sync_areset_n_reg_inv(sync_areset_n_reg_inv));
endmodule

(* ORIG_REF_NAME = "fifo_generator_v13_0_5_memory" *) 
module axi_fifo_mm_s_0_fifo_generator_v13_0_5_memory
   (\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[9] ,
    \axi_str_txd_tdata[31] ,
    \sig_register_array_reg[0][4] ,
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[4] ,
    \gtxc.TXC_STATE_reg[0] ,
    \gtxc.TXC_STATE_reg[1] ,
    s_axi_aclk,
    ram_rd_en_i,
    WEBWE,
    \gc0.count_d1_reg[8] ,
    \gcc0.gc1.gsym.count_d2_reg[8] ,
    Q,
    DIADI,
    axis_wr_eop_d1,
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_reg ,
    p_8_out,
    axi_str_txd_tready,
    axi_str_txc_tlast,
    \gtxc.TXC_STATE_reg[1]_0 ,
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[1] ,
    axi_str_txc_tready,
    \gtxc.txc_cntr_reg[1] ,
    \gtxc.TXC_STATE_reg[0]_0 ,
    axi_str_txd_tvalid,
    E);
  output [0:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[9] ;
  output [32:0]\axi_str_txd_tdata[31] ;
  output \sig_register_array_reg[0][4] ;
  output [0:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[4] ;
  output \gtxc.TXC_STATE_reg[0] ;
  output \gtxc.TXC_STATE_reg[1] ;
  input s_axi_aclk;
  input ram_rd_en_i;
  input [0:0]WEBWE;
  input [8:0]\gc0.count_d1_reg[8] ;
  input [8:0]\gcc0.gc1.gsym.count_d2_reg[8] ;
  input [31:0]Q;
  input [4:0]DIADI;
  input axis_wr_eop_d1;
  input \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_reg ;
  input p_8_out;
  input axi_str_txd_tready;
  input axi_str_txc_tlast;
  input \gtxc.TXC_STATE_reg[1]_0 ;
  input [0:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[1] ;
  input axi_str_txc_tready;
  input \gtxc.txc_cntr_reg[1] ;
  input \gtxc.TXC_STATE_reg[0]_0 ;
  input axi_str_txd_tvalid;
  input [0:0]E;

  wire [4:0]DIADI;
  wire [0:0]E;
  wire [31:0]Q;
  wire [0:0]WEBWE;
  wire axi_str_txc_tlast;
  wire axi_str_txc_tready;
  wire [32:0]\axi_str_txd_tdata[31] ;
  wire axi_str_txd_tready;
  wire axi_str_txd_tvalid;
  wire axis_wr_eop_d1;
  wire [40:0]doutb;
  wire [0:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[1] ;
  wire [0:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[4] ;
  wire [0:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[9] ;
  wire \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_reg ;
  wire [8:0]\gc0.count_d1_reg[8] ;
  wire [8:0]\gcc0.gc1.gsym.count_d2_reg[8] ;
  wire \gtxc.TXC_STATE[1]_i_2_n_0 ;
  wire \gtxc.TXC_STATE_reg[0] ;
  wire \gtxc.TXC_STATE_reg[0]_0 ;
  wire \gtxc.TXC_STATE_reg[1] ;
  wire \gtxc.TXC_STATE_reg[1]_0 ;
  wire \gtxc.txc_cntr_reg[1] ;
  wire p_8_out;
  wire ram_rd_en_i;
  wire s_axi_aclk;
  wire \sig_register_array_reg[0][4] ;

  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT5 #(
    .INIT(32'hAA6AAAAA)) 
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt[9]_i_1 
       (.I0(axis_wr_eop_d1),
        .I1(\axi_str_txd_tdata[31] [0]),
        .I2(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_reg ),
        .I3(p_8_out),
        .I4(axi_str_txd_tready),
        .O(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[9] ));
  axi_fifo_mm_s_0_blk_mem_gen_v8_3_5 \gbm.gbmg.gbmga.ngecc.bmg 
       (.D({doutb[40:9],doutb[0]}),
        .DIADI(DIADI),
        .Q(Q),
        .WEBWE(WEBWE),
        .\gc0.count_d1_reg[8] (\gc0.count_d1_reg[8] ),
        .\gcc0.gc1.gsym.count_d2_reg[8] (\gcc0.gc1.gsym.count_d2_reg[8] ),
        .ram_rd_en_i(ram_rd_en_i),
        .s_axi_aclk(s_axi_aclk));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_bm.dout_i_reg[0] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(doutb[0]),
        .Q(\axi_str_txd_tdata[31] [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_bm.dout_i_reg[10] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(doutb[10]),
        .Q(\axi_str_txd_tdata[31] [2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_bm.dout_i_reg[11] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(doutb[11]),
        .Q(\axi_str_txd_tdata[31] [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_bm.dout_i_reg[12] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(doutb[12]),
        .Q(\axi_str_txd_tdata[31] [4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_bm.dout_i_reg[13] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(doutb[13]),
        .Q(\axi_str_txd_tdata[31] [5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_bm.dout_i_reg[14] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(doutb[14]),
        .Q(\axi_str_txd_tdata[31] [6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_bm.dout_i_reg[15] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(doutb[15]),
        .Q(\axi_str_txd_tdata[31] [7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_bm.dout_i_reg[16] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(doutb[16]),
        .Q(\axi_str_txd_tdata[31] [8]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_bm.dout_i_reg[17] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(doutb[17]),
        .Q(\axi_str_txd_tdata[31] [9]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_bm.dout_i_reg[18] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(doutb[18]),
        .Q(\axi_str_txd_tdata[31] [10]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_bm.dout_i_reg[19] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(doutb[19]),
        .Q(\axi_str_txd_tdata[31] [11]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_bm.dout_i_reg[20] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(doutb[20]),
        .Q(\axi_str_txd_tdata[31] [12]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_bm.dout_i_reg[21] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(doutb[21]),
        .Q(\axi_str_txd_tdata[31] [13]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_bm.dout_i_reg[22] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(doutb[22]),
        .Q(\axi_str_txd_tdata[31] [14]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_bm.dout_i_reg[23] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(doutb[23]),
        .Q(\axi_str_txd_tdata[31] [15]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_bm.dout_i_reg[24] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(doutb[24]),
        .Q(\axi_str_txd_tdata[31] [16]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_bm.dout_i_reg[25] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(doutb[25]),
        .Q(\axi_str_txd_tdata[31] [17]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_bm.dout_i_reg[26] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(doutb[26]),
        .Q(\axi_str_txd_tdata[31] [18]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_bm.dout_i_reg[27] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(doutb[27]),
        .Q(\axi_str_txd_tdata[31] [19]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_bm.dout_i_reg[28] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(doutb[28]),
        .Q(\axi_str_txd_tdata[31] [20]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_bm.dout_i_reg[29] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(doutb[29]),
        .Q(\axi_str_txd_tdata[31] [21]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_bm.dout_i_reg[30] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(doutb[30]),
        .Q(\axi_str_txd_tdata[31] [22]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_bm.dout_i_reg[31] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(doutb[31]),
        .Q(\axi_str_txd_tdata[31] [23]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_bm.dout_i_reg[32] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(doutb[32]),
        .Q(\axi_str_txd_tdata[31] [24]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_bm.dout_i_reg[33] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(doutb[33]),
        .Q(\axi_str_txd_tdata[31] [25]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_bm.dout_i_reg[34] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(doutb[34]),
        .Q(\axi_str_txd_tdata[31] [26]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_bm.dout_i_reg[35] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(doutb[35]),
        .Q(\axi_str_txd_tdata[31] [27]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_bm.dout_i_reg[36] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(doutb[36]),
        .Q(\axi_str_txd_tdata[31] [28]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_bm.dout_i_reg[37] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(doutb[37]),
        .Q(\axi_str_txd_tdata[31] [29]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_bm.dout_i_reg[38] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(doutb[38]),
        .Q(\axi_str_txd_tdata[31] [30]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_bm.dout_i_reg[39] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(doutb[39]),
        .Q(\axi_str_txd_tdata[31] [31]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_bm.dout_i_reg[40] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(doutb[40]),
        .Q(\axi_str_txd_tdata[31] [32]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_bm.dout_i_reg[9] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(doutb[9]),
        .Q(\axi_str_txd_tdata[31] [1]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h000700FF000700F0)) 
    \gtxc.TXC_STATE[0]_i_1 
       (.I0(axi_str_txc_tready),
        .I1(\gtxc.txc_cntr_reg[1] ),
        .I2(\gtxc.TXC_STATE[1]_i_2_n_0 ),
        .I3(\gtxc.TXC_STATE_reg[1]_0 ),
        .I4(\gtxc.TXC_STATE_reg[0]_0 ),
        .I5(axi_str_txd_tvalid),
        .O(\gtxc.TXC_STATE_reg[0] ));
  LUT5 #(
    .INIT(32'h00F80F00)) 
    \gtxc.TXC_STATE[1]_i_1 
       (.I0(axi_str_txc_tready),
        .I1(\gtxc.txc_cntr_reg[1] ),
        .I2(\gtxc.TXC_STATE[1]_i_2_n_0 ),
        .I3(\gtxc.TXC_STATE_reg[1]_0 ),
        .I4(\gtxc.TXC_STATE_reg[0]_0 ),
        .O(\gtxc.TXC_STATE_reg[1] ));
  LUT4 #(
    .INIT(16'h4000)) 
    \gtxc.TXC_STATE[1]_i_2 
       (.I0(axi_str_txc_tlast),
        .I1(\gtxc.TXC_STATE_reg[1]_0 ),
        .I2(axi_str_txd_tready),
        .I3(\axi_str_txd_tdata[31] [0]),
        .O(\gtxc.TXC_STATE[1]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h9999599999999999)) 
    p_0_out_carry_i_5
       (.I0(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[1] ),
        .I1(axis_wr_eop_d1),
        .I2(\axi_str_txd_tdata[31] [0]),
        .I3(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_reg ),
        .I4(p_8_out),
        .I5(axi_str_txd_tready),
        .O(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[4] ));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT3 #(
    .INIT(8'h20)) 
    \sig_register_array[0][4]_i_3 
       (.I0(\axi_str_txd_tdata[31] [0]),
        .I1(p_8_out),
        .I2(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_reg ),
        .O(\sig_register_array_reg[0][4] ));
endmodule

(* ORIG_REF_NAME = "fifo_generator_v13_0_5_memory" *) 
module axi_fifo_mm_s_0_fifo_generator_v13_0_5_memory_6
   (\grxd.sig_rxd_rd_data_reg[32] ,
    s_axi4_rdata,
    s_axi_aclk,
    ram_rd_en_i,
    WEBWE,
    \gc0.count_d1_reg[8] ,
    \gcc0.gc0.count_d1_reg[8] ,
    axi_str_rxd_tdata,
    axi_str_rxd_tlast,
    sig_rxd_rd_data,
    axi4_fifo_rd_en_i,
    sig_str_rst_reg,
    s_axi_aresetn,
    sig_rd_rlen_reg,
    E);
  output \grxd.sig_rxd_rd_data_reg[32] ;
  output [31:0]s_axi4_rdata;
  input s_axi_aclk;
  input ram_rd_en_i;
  input [0:0]WEBWE;
  input [8:0]\gc0.count_d1_reg[8] ;
  input [8:0]\gcc0.gc0.count_d1_reg[8] ;
  input [31:0]axi_str_rxd_tdata;
  input axi_str_rxd_tlast;
  input [0:0]sig_rxd_rd_data;
  input axi4_fifo_rd_en_i;
  input sig_str_rst_reg;
  input s_axi_aresetn;
  input sig_rd_rlen_reg;
  input [0:0]E;

  wire [0:0]E;
  wire [0:0]WEBWE;
  wire axi4_fifo_rd_en_i;
  wire axi4_rlast;
  wire [31:0]axi_str_rxd_tdata;
  wire axi_str_rxd_tlast;
  wire [40:0]doutb;
  wire [8:0]\gc0.count_d1_reg[8] ;
  wire [8:0]\gcc0.gc0.count_d1_reg[8] ;
  wire \grxd.sig_rxd_rd_data_reg[32] ;
  wire ram_rd_en_i;
  wire [31:0]s_axi4_rdata;
  wire s_axi_aclk;
  wire s_axi_aresetn;
  wire sig_rd_rlen_reg;
  wire [0:0]sig_rxd_rd_data;
  wire sig_str_rst_reg;

  axi_fifo_mm_s_0_blk_mem_gen_v8_3_5_7 \gbm.gbmg.gbmga.ngecc.bmg 
       (.D({doutb[40:9],doutb[0]}),
        .WEBWE(WEBWE),
        .axi_str_rxd_tdata(axi_str_rxd_tdata),
        .axi_str_rxd_tlast(axi_str_rxd_tlast),
        .\gc0.count_d1_reg[8] (\gc0.count_d1_reg[8] ),
        .\gcc0.gc0.count_d1_reg[8] (\gcc0.gc0.count_d1_reg[8] ),
        .ram_rd_en_i(ram_rd_en_i),
        .s_axi_aclk(s_axi_aclk));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_bm.dout_i_reg[0] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(doutb[0]),
        .Q(axi4_rlast),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_bm.dout_i_reg[10] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(doutb[10]),
        .Q(s_axi4_rdata[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_bm.dout_i_reg[11] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(doutb[11]),
        .Q(s_axi4_rdata[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_bm.dout_i_reg[12] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(doutb[12]),
        .Q(s_axi4_rdata[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_bm.dout_i_reg[13] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(doutb[13]),
        .Q(s_axi4_rdata[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_bm.dout_i_reg[14] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(doutb[14]),
        .Q(s_axi4_rdata[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_bm.dout_i_reg[15] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(doutb[15]),
        .Q(s_axi4_rdata[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_bm.dout_i_reg[16] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(doutb[16]),
        .Q(s_axi4_rdata[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_bm.dout_i_reg[17] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(doutb[17]),
        .Q(s_axi4_rdata[8]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_bm.dout_i_reg[18] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(doutb[18]),
        .Q(s_axi4_rdata[9]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_bm.dout_i_reg[19] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(doutb[19]),
        .Q(s_axi4_rdata[10]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_bm.dout_i_reg[20] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(doutb[20]),
        .Q(s_axi4_rdata[11]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_bm.dout_i_reg[21] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(doutb[21]),
        .Q(s_axi4_rdata[12]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_bm.dout_i_reg[22] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(doutb[22]),
        .Q(s_axi4_rdata[13]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_bm.dout_i_reg[23] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(doutb[23]),
        .Q(s_axi4_rdata[14]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_bm.dout_i_reg[24] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(doutb[24]),
        .Q(s_axi4_rdata[15]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_bm.dout_i_reg[25] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(doutb[25]),
        .Q(s_axi4_rdata[16]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_bm.dout_i_reg[26] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(doutb[26]),
        .Q(s_axi4_rdata[17]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_bm.dout_i_reg[27] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(doutb[27]),
        .Q(s_axi4_rdata[18]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_bm.dout_i_reg[28] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(doutb[28]),
        .Q(s_axi4_rdata[19]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_bm.dout_i_reg[29] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(doutb[29]),
        .Q(s_axi4_rdata[20]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_bm.dout_i_reg[30] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(doutb[30]),
        .Q(s_axi4_rdata[21]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_bm.dout_i_reg[31] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(doutb[31]),
        .Q(s_axi4_rdata[22]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_bm.dout_i_reg[32] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(doutb[32]),
        .Q(s_axi4_rdata[23]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_bm.dout_i_reg[33] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(doutb[33]),
        .Q(s_axi4_rdata[24]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_bm.dout_i_reg[34] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(doutb[34]),
        .Q(s_axi4_rdata[25]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_bm.dout_i_reg[35] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(doutb[35]),
        .Q(s_axi4_rdata[26]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_bm.dout_i_reg[36] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(doutb[36]),
        .Q(s_axi4_rdata[27]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_bm.dout_i_reg[37] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(doutb[37]),
        .Q(s_axi4_rdata[28]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_bm.dout_i_reg[38] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(doutb[38]),
        .Q(s_axi4_rdata[29]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_bm.dout_i_reg[39] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(doutb[39]),
        .Q(s_axi4_rdata[30]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_bm.dout_i_reg[40] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(doutb[40]),
        .Q(s_axi4_rdata[31]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_bm.dout_i_reg[9] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(doutb[9]),
        .Q(s_axi4_rdata[0]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h00FF0000002A0000)) 
    \grxd.sig_rxd_rd_data[32]_i_1 
       (.I0(sig_rxd_rd_data),
        .I1(axi4_rlast),
        .I2(axi4_fifo_rd_en_i),
        .I3(sig_str_rst_reg),
        .I4(s_axi_aresetn),
        .I5(sig_rd_rlen_reg),
        .O(\grxd.sig_rxd_rd_data_reg[32] ));
endmodule

(* ORIG_REF_NAME = "fifo_generator_v13_0_5_rd_bin_cntr" *) 
module axi_fifo_mm_s_0_fifo_generator_v13_0_5_rd_bin_cntr
   (v1_reg,
    Q,
    v1_reg_2,
    v1_reg_0,
    v1_reg_1,
    ram_empty_fb_i_reg,
    ram_empty_fb_i_reg_0,
    ram_empty_fb_i_reg_1,
    ram_empty_fb_i_reg_2,
    \gcc0.gc1.gsym.count_d2_reg[8] ,
    \gcc0.gc1.gsym.count_d1_reg[7] ,
    \gcc0.gc1.gsym.count_reg[8] ,
    E,
    s_axi_aclk,
    \ngwrdrst.grst.g7serrst.rd_rst_reg_reg[2] );
  output [3:0]v1_reg;
  output [8:0]Q;
  output [4:0]v1_reg_2;
  output [3:0]v1_reg_0;
  output [4:0]v1_reg_1;
  output ram_empty_fb_i_reg;
  output ram_empty_fb_i_reg_0;
  output ram_empty_fb_i_reg_1;
  output ram_empty_fb_i_reg_2;
  input [8:0]\gcc0.gc1.gsym.count_d2_reg[8] ;
  input [7:0]\gcc0.gc1.gsym.count_d1_reg[7] ;
  input [8:0]\gcc0.gc1.gsym.count_reg[8] ;
  input [0:0]E;
  input s_axi_aclk;
  input [0:0]\ngwrdrst.grst.g7serrst.rd_rst_reg_reg[2] ;

  wire [0:0]E;
  wire [8:0]Q;
  wire \gc0.count[8]_i_2_n_0 ;
  wire [7:0]\gcc0.gc1.gsym.count_d1_reg[7] ;
  wire [8:0]\gcc0.gc1.gsym.count_d2_reg[8] ;
  wire [8:0]\gcc0.gc1.gsym.count_reg[8] ;
  wire [0:0]\ngwrdrst.grst.g7serrst.rd_rst_reg_reg[2] ;
  wire [8:0]plusOp;
  wire ram_empty_fb_i_reg;
  wire ram_empty_fb_i_reg_0;
  wire ram_empty_fb_i_reg_1;
  wire ram_empty_fb_i_reg_2;
  wire [8:0]rd_pntr_plus1;
  wire s_axi_aclk;
  wire [3:0]v1_reg;
  wire [3:0]v1_reg_0;
  wire [4:0]v1_reg_1;
  wire [4:0]v1_reg_2;

  LUT1 #(
    .INIT(2'h1)) 
    \gc0.count[0]_i_1 
       (.I0(rd_pntr_plus1[0]),
        .O(plusOp[0]));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \gc0.count[1]_i_1 
       (.I0(rd_pntr_plus1[0]),
        .I1(rd_pntr_plus1[1]),
        .O(plusOp[1]));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \gc0.count[2]_i_1 
       (.I0(rd_pntr_plus1[0]),
        .I1(rd_pntr_plus1[1]),
        .I2(rd_pntr_plus1[2]),
        .O(plusOp[2]));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \gc0.count[3]_i_1 
       (.I0(rd_pntr_plus1[1]),
        .I1(rd_pntr_plus1[0]),
        .I2(rd_pntr_plus1[2]),
        .I3(rd_pntr_plus1[3]),
        .O(plusOp[3]));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \gc0.count[4]_i_1 
       (.I0(rd_pntr_plus1[2]),
        .I1(rd_pntr_plus1[0]),
        .I2(rd_pntr_plus1[1]),
        .I3(rd_pntr_plus1[3]),
        .I4(rd_pntr_plus1[4]),
        .O(plusOp[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \gc0.count[5]_i_1 
       (.I0(rd_pntr_plus1[3]),
        .I1(rd_pntr_plus1[1]),
        .I2(rd_pntr_plus1[0]),
        .I3(rd_pntr_plus1[2]),
        .I4(rd_pntr_plus1[4]),
        .I5(rd_pntr_plus1[5]),
        .O(plusOp[5]));
  LUT2 #(
    .INIT(4'h6)) 
    \gc0.count[6]_i_1 
       (.I0(\gc0.count[8]_i_2_n_0 ),
        .I1(rd_pntr_plus1[6]),
        .O(plusOp[6]));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \gc0.count[7]_i_1 
       (.I0(\gc0.count[8]_i_2_n_0 ),
        .I1(rd_pntr_plus1[6]),
        .I2(rd_pntr_plus1[7]),
        .O(plusOp[7]));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \gc0.count[8]_i_1 
       (.I0(rd_pntr_plus1[6]),
        .I1(\gc0.count[8]_i_2_n_0 ),
        .I2(rd_pntr_plus1[7]),
        .I3(rd_pntr_plus1[8]),
        .O(plusOp[8]));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \gc0.count[8]_i_2 
       (.I0(rd_pntr_plus1[5]),
        .I1(rd_pntr_plus1[3]),
        .I2(rd_pntr_plus1[1]),
        .I3(rd_pntr_plus1[0]),
        .I4(rd_pntr_plus1[2]),
        .I5(rd_pntr_plus1[4]),
        .O(\gc0.count[8]_i_2_n_0 ));
  FDCE #(
    .INIT(1'b0)) 
    \gc0.count_d1_reg[0] 
       (.C(s_axi_aclk),
        .CE(E),
        .CLR(\ngwrdrst.grst.g7serrst.rd_rst_reg_reg[2] ),
        .D(rd_pntr_plus1[0]),
        .Q(Q[0]));
  FDCE #(
    .INIT(1'b0)) 
    \gc0.count_d1_reg[1] 
       (.C(s_axi_aclk),
        .CE(E),
        .CLR(\ngwrdrst.grst.g7serrst.rd_rst_reg_reg[2] ),
        .D(rd_pntr_plus1[1]),
        .Q(Q[1]));
  FDCE #(
    .INIT(1'b0)) 
    \gc0.count_d1_reg[2] 
       (.C(s_axi_aclk),
        .CE(E),
        .CLR(\ngwrdrst.grst.g7serrst.rd_rst_reg_reg[2] ),
        .D(rd_pntr_plus1[2]),
        .Q(Q[2]));
  FDCE #(
    .INIT(1'b0)) 
    \gc0.count_d1_reg[3] 
       (.C(s_axi_aclk),
        .CE(E),
        .CLR(\ngwrdrst.grst.g7serrst.rd_rst_reg_reg[2] ),
        .D(rd_pntr_plus1[3]),
        .Q(Q[3]));
  FDCE #(
    .INIT(1'b0)) 
    \gc0.count_d1_reg[4] 
       (.C(s_axi_aclk),
        .CE(E),
        .CLR(\ngwrdrst.grst.g7serrst.rd_rst_reg_reg[2] ),
        .D(rd_pntr_plus1[4]),
        .Q(Q[4]));
  FDCE #(
    .INIT(1'b0)) 
    \gc0.count_d1_reg[5] 
       (.C(s_axi_aclk),
        .CE(E),
        .CLR(\ngwrdrst.grst.g7serrst.rd_rst_reg_reg[2] ),
        .D(rd_pntr_plus1[5]),
        .Q(Q[5]));
  FDCE #(
    .INIT(1'b0)) 
    \gc0.count_d1_reg[6] 
       (.C(s_axi_aclk),
        .CE(E),
        .CLR(\ngwrdrst.grst.g7serrst.rd_rst_reg_reg[2] ),
        .D(rd_pntr_plus1[6]),
        .Q(Q[6]));
  FDCE #(
    .INIT(1'b0)) 
    \gc0.count_d1_reg[7] 
       (.C(s_axi_aclk),
        .CE(E),
        .CLR(\ngwrdrst.grst.g7serrst.rd_rst_reg_reg[2] ),
        .D(rd_pntr_plus1[7]),
        .Q(Q[7]));
  FDCE #(
    .INIT(1'b0)) 
    \gc0.count_d1_reg[8] 
       (.C(s_axi_aclk),
        .CE(E),
        .CLR(\ngwrdrst.grst.g7serrst.rd_rst_reg_reg[2] ),
        .D(rd_pntr_plus1[8]),
        .Q(Q[8]));
  FDPE #(
    .INIT(1'b1)) 
    \gc0.count_reg[0] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(plusOp[0]),
        .PRE(\ngwrdrst.grst.g7serrst.rd_rst_reg_reg[2] ),
        .Q(rd_pntr_plus1[0]));
  FDCE #(
    .INIT(1'b0)) 
    \gc0.count_reg[1] 
       (.C(s_axi_aclk),
        .CE(E),
        .CLR(\ngwrdrst.grst.g7serrst.rd_rst_reg_reg[2] ),
        .D(plusOp[1]),
        .Q(rd_pntr_plus1[1]));
  FDCE #(
    .INIT(1'b0)) 
    \gc0.count_reg[2] 
       (.C(s_axi_aclk),
        .CE(E),
        .CLR(\ngwrdrst.grst.g7serrst.rd_rst_reg_reg[2] ),
        .D(plusOp[2]),
        .Q(rd_pntr_plus1[2]));
  FDCE #(
    .INIT(1'b0)) 
    \gc0.count_reg[3] 
       (.C(s_axi_aclk),
        .CE(E),
        .CLR(\ngwrdrst.grst.g7serrst.rd_rst_reg_reg[2] ),
        .D(plusOp[3]),
        .Q(rd_pntr_plus1[3]));
  FDCE #(
    .INIT(1'b0)) 
    \gc0.count_reg[4] 
       (.C(s_axi_aclk),
        .CE(E),
        .CLR(\ngwrdrst.grst.g7serrst.rd_rst_reg_reg[2] ),
        .D(plusOp[4]),
        .Q(rd_pntr_plus1[4]));
  FDCE #(
    .INIT(1'b0)) 
    \gc0.count_reg[5] 
       (.C(s_axi_aclk),
        .CE(E),
        .CLR(\ngwrdrst.grst.g7serrst.rd_rst_reg_reg[2] ),
        .D(plusOp[5]),
        .Q(rd_pntr_plus1[5]));
  FDCE #(
    .INIT(1'b0)) 
    \gc0.count_reg[6] 
       (.C(s_axi_aclk),
        .CE(E),
        .CLR(\ngwrdrst.grst.g7serrst.rd_rst_reg_reg[2] ),
        .D(plusOp[6]),
        .Q(rd_pntr_plus1[6]));
  FDCE #(
    .INIT(1'b0)) 
    \gc0.count_reg[7] 
       (.C(s_axi_aclk),
        .CE(E),
        .CLR(\ngwrdrst.grst.g7serrst.rd_rst_reg_reg[2] ),
        .D(plusOp[7]),
        .Q(rd_pntr_plus1[7]));
  FDCE #(
    .INIT(1'b0)) 
    \gc0.count_reg[8] 
       (.C(s_axi_aclk),
        .CE(E),
        .CLR(\ngwrdrst.grst.g7serrst.rd_rst_reg_reg[2] ),
        .D(plusOp[8]),
        .Q(rd_pntr_plus1[8]));
  LUT4 #(
    .INIT(16'h9009)) 
    \gmux.gm[0].gm1.m1_i_1__0 
       (.I0(Q[0]),
        .I1(\gcc0.gc1.gsym.count_d2_reg[8] [0]),
        .I2(Q[1]),
        .I3(\gcc0.gc1.gsym.count_d2_reg[8] [1]),
        .O(v1_reg[0]));
  LUT4 #(
    .INIT(16'h9009)) 
    \gmux.gm[0].gm1.m1_i_1__1 
       (.I0(rd_pntr_plus1[0]),
        .I1(\gcc0.gc1.gsym.count_d2_reg[8] [0]),
        .I2(\gcc0.gc1.gsym.count_d2_reg[8] [1]),
        .I3(rd_pntr_plus1[1]),
        .O(v1_reg_2[0]));
  LUT4 #(
    .INIT(16'h9009)) 
    \gmux.gm[0].gm1.m1_i_1__2 
       (.I0(Q[0]),
        .I1(\gcc0.gc1.gsym.count_d1_reg[7] [0]),
        .I2(Q[1]),
        .I3(\gcc0.gc1.gsym.count_d1_reg[7] [1]),
        .O(v1_reg_0[0]));
  LUT4 #(
    .INIT(16'h9009)) 
    \gmux.gm[0].gm1.m1_i_1__3 
       (.I0(Q[0]),
        .I1(\gcc0.gc1.gsym.count_reg[8] [0]),
        .I2(Q[1]),
        .I3(\gcc0.gc1.gsym.count_reg[8] [1]),
        .O(v1_reg_1[0]));
  LUT4 #(
    .INIT(16'h9009)) 
    \gmux.gm[0].gm1.m1_i_1__6 
       (.I0(Q[0]),
        .I1(\gcc0.gc1.gsym.count_d2_reg[8] [0]),
        .I2(Q[1]),
        .I3(\gcc0.gc1.gsym.count_d2_reg[8] [1]),
        .O(ram_empty_fb_i_reg));
  LUT4 #(
    .INIT(16'h9009)) 
    \gmux.gm[1].gms.ms_i_1__0 
       (.I0(Q[2]),
        .I1(\gcc0.gc1.gsym.count_d2_reg[8] [2]),
        .I2(Q[3]),
        .I3(\gcc0.gc1.gsym.count_d2_reg[8] [3]),
        .O(v1_reg[1]));
  LUT4 #(
    .INIT(16'h9009)) 
    \gmux.gm[1].gms.ms_i_1__1 
       (.I0(rd_pntr_plus1[2]),
        .I1(\gcc0.gc1.gsym.count_d2_reg[8] [2]),
        .I2(\gcc0.gc1.gsym.count_d2_reg[8] [3]),
        .I3(rd_pntr_plus1[3]),
        .O(v1_reg_2[1]));
  LUT4 #(
    .INIT(16'h9009)) 
    \gmux.gm[1].gms.ms_i_1__2 
       (.I0(Q[2]),
        .I1(\gcc0.gc1.gsym.count_d1_reg[7] [2]),
        .I2(Q[3]),
        .I3(\gcc0.gc1.gsym.count_d1_reg[7] [3]),
        .O(v1_reg_0[1]));
  LUT4 #(
    .INIT(16'h9009)) 
    \gmux.gm[1].gms.ms_i_1__3 
       (.I0(Q[2]),
        .I1(\gcc0.gc1.gsym.count_reg[8] [2]),
        .I2(Q[3]),
        .I3(\gcc0.gc1.gsym.count_reg[8] [3]),
        .O(v1_reg_1[1]));
  LUT4 #(
    .INIT(16'h9009)) 
    \gmux.gm[1].gms.ms_i_1__6 
       (.I0(Q[2]),
        .I1(\gcc0.gc1.gsym.count_d2_reg[8] [2]),
        .I2(Q[3]),
        .I3(\gcc0.gc1.gsym.count_d2_reg[8] [3]),
        .O(ram_empty_fb_i_reg_0));
  LUT4 #(
    .INIT(16'h9009)) 
    \gmux.gm[2].gms.ms_i_1__0 
       (.I0(Q[4]),
        .I1(\gcc0.gc1.gsym.count_d2_reg[8] [4]),
        .I2(Q[5]),
        .I3(\gcc0.gc1.gsym.count_d2_reg[8] [5]),
        .O(v1_reg[2]));
  LUT4 #(
    .INIT(16'h9009)) 
    \gmux.gm[2].gms.ms_i_1__1 
       (.I0(rd_pntr_plus1[4]),
        .I1(\gcc0.gc1.gsym.count_d2_reg[8] [4]),
        .I2(\gcc0.gc1.gsym.count_d2_reg[8] [5]),
        .I3(rd_pntr_plus1[5]),
        .O(v1_reg_2[2]));
  LUT4 #(
    .INIT(16'h9009)) 
    \gmux.gm[2].gms.ms_i_1__2 
       (.I0(Q[4]),
        .I1(\gcc0.gc1.gsym.count_d1_reg[7] [4]),
        .I2(Q[5]),
        .I3(\gcc0.gc1.gsym.count_d1_reg[7] [5]),
        .O(v1_reg_0[2]));
  LUT4 #(
    .INIT(16'h9009)) 
    \gmux.gm[2].gms.ms_i_1__3 
       (.I0(Q[4]),
        .I1(\gcc0.gc1.gsym.count_reg[8] [4]),
        .I2(Q[5]),
        .I3(\gcc0.gc1.gsym.count_reg[8] [5]),
        .O(v1_reg_1[2]));
  LUT4 #(
    .INIT(16'h9009)) 
    \gmux.gm[2].gms.ms_i_1__6 
       (.I0(Q[4]),
        .I1(\gcc0.gc1.gsym.count_d2_reg[8] [4]),
        .I2(Q[5]),
        .I3(\gcc0.gc1.gsym.count_d2_reg[8] [5]),
        .O(ram_empty_fb_i_reg_1));
  LUT4 #(
    .INIT(16'h9009)) 
    \gmux.gm[3].gms.ms_i_1__0 
       (.I0(Q[6]),
        .I1(\gcc0.gc1.gsym.count_d2_reg[8] [6]),
        .I2(Q[7]),
        .I3(\gcc0.gc1.gsym.count_d2_reg[8] [7]),
        .O(v1_reg[3]));
  LUT4 #(
    .INIT(16'h9009)) 
    \gmux.gm[3].gms.ms_i_1__1 
       (.I0(rd_pntr_plus1[6]),
        .I1(\gcc0.gc1.gsym.count_d2_reg[8] [6]),
        .I2(\gcc0.gc1.gsym.count_d2_reg[8] [7]),
        .I3(rd_pntr_plus1[7]),
        .O(v1_reg_2[3]));
  LUT4 #(
    .INIT(16'h9009)) 
    \gmux.gm[3].gms.ms_i_1__2 
       (.I0(Q[6]),
        .I1(\gcc0.gc1.gsym.count_d1_reg[7] [6]),
        .I2(Q[7]),
        .I3(\gcc0.gc1.gsym.count_d1_reg[7] [7]),
        .O(v1_reg_0[3]));
  LUT4 #(
    .INIT(16'h9009)) 
    \gmux.gm[3].gms.ms_i_1__3 
       (.I0(Q[6]),
        .I1(\gcc0.gc1.gsym.count_reg[8] [6]),
        .I2(Q[7]),
        .I3(\gcc0.gc1.gsym.count_reg[8] [7]),
        .O(v1_reg_1[3]));
  LUT4 #(
    .INIT(16'h9009)) 
    \gmux.gm[3].gms.ms_i_1__6 
       (.I0(Q[6]),
        .I1(\gcc0.gc1.gsym.count_d2_reg[8] [6]),
        .I2(Q[7]),
        .I3(\gcc0.gc1.gsym.count_d2_reg[8] [7]),
        .O(ram_empty_fb_i_reg_2));
  LUT2 #(
    .INIT(4'h9)) 
    \gmux.gm[4].gms.ms_i_1__3 
       (.I0(rd_pntr_plus1[8]),
        .I1(\gcc0.gc1.gsym.count_d2_reg[8] [8]),
        .O(v1_reg_2[4]));
  LUT2 #(
    .INIT(4'h9)) 
    \gmux.gm[4].gms.ms_i_1__4 
       (.I0(Q[8]),
        .I1(\gcc0.gc1.gsym.count_reg[8] [8]),
        .O(v1_reg_1[4]));
endmodule

(* ORIG_REF_NAME = "fifo_generator_v13_0_5_rd_bin_cntr" *) 
module axi_fifo_mm_s_0_fifo_generator_v13_0_5_rd_bin_cntr_19
   (v1_reg,
    \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram ,
    v1_reg_0,
    v1_reg_1,
    \gdiff.gcry_1_sym.diff_pntr_pad_reg[9] ,
    ram_empty_fb_i_reg,
    ram_empty_fb_i_reg_0,
    ram_empty_fb_i_reg_1,
    ram_empty_fb_i_reg_2,
    \gcc0.gc0.count_reg[8] ,
    \gcc0.gc0.count_d1_reg[8] ,
    E,
    s_axi_aclk,
    Q);
  output [4:0]v1_reg;
  output [8:0]\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram ;
  output [3:0]v1_reg_0;
  output [4:0]v1_reg_1;
  output [0:0]\gdiff.gcry_1_sym.diff_pntr_pad_reg[9] ;
  output ram_empty_fb_i_reg;
  output ram_empty_fb_i_reg_0;
  output ram_empty_fb_i_reg_1;
  output ram_empty_fb_i_reg_2;
  input [8:0]\gcc0.gc0.count_reg[8] ;
  input [8:0]\gcc0.gc0.count_d1_reg[8] ;
  input [0:0]E;
  input s_axi_aclk;
  input [0:0]Q;

  wire [8:0]\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram ;
  wire [0:0]E;
  wire [0:0]Q;
  wire \gc0.count[8]_i_2__0_n_0 ;
  wire [8:0]\gcc0.gc0.count_d1_reg[8] ;
  wire [8:0]\gcc0.gc0.count_reg[8] ;
  wire [0:0]\gdiff.gcry_1_sym.diff_pntr_pad_reg[9] ;
  wire [8:0]plusOp__1;
  wire ram_empty_fb_i_reg;
  wire ram_empty_fb_i_reg_0;
  wire ram_empty_fb_i_reg_1;
  wire ram_empty_fb_i_reg_2;
  wire [8:0]rd_pntr_plus1;
  wire s_axi_aclk;
  wire [4:0]v1_reg;
  wire [3:0]v1_reg_0;
  wire [4:0]v1_reg_1;

  LUT1 #(
    .INIT(2'h1)) 
    \gc0.count[0]_i_1__0 
       (.I0(rd_pntr_plus1[0]),
        .O(plusOp__1[0]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \gc0.count[1]_i_1__0 
       (.I0(rd_pntr_plus1[0]),
        .I1(rd_pntr_plus1[1]),
        .O(plusOp__1[1]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \gc0.count[2]_i_1__0 
       (.I0(rd_pntr_plus1[0]),
        .I1(rd_pntr_plus1[1]),
        .I2(rd_pntr_plus1[2]),
        .O(plusOp__1[2]));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \gc0.count[3]_i_1__0 
       (.I0(rd_pntr_plus1[1]),
        .I1(rd_pntr_plus1[0]),
        .I2(rd_pntr_plus1[2]),
        .I3(rd_pntr_plus1[3]),
        .O(plusOp__1[3]));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \gc0.count[4]_i_1__0 
       (.I0(rd_pntr_plus1[2]),
        .I1(rd_pntr_plus1[0]),
        .I2(rd_pntr_plus1[1]),
        .I3(rd_pntr_plus1[3]),
        .I4(rd_pntr_plus1[4]),
        .O(plusOp__1[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \gc0.count[5]_i_1__0 
       (.I0(rd_pntr_plus1[3]),
        .I1(rd_pntr_plus1[1]),
        .I2(rd_pntr_plus1[0]),
        .I3(rd_pntr_plus1[2]),
        .I4(rd_pntr_plus1[4]),
        .I5(rd_pntr_plus1[5]),
        .O(plusOp__1[5]));
  LUT2 #(
    .INIT(4'h6)) 
    \gc0.count[6]_i_1__0 
       (.I0(\gc0.count[8]_i_2__0_n_0 ),
        .I1(rd_pntr_plus1[6]),
        .O(plusOp__1[6]));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \gc0.count[7]_i_1__0 
       (.I0(\gc0.count[8]_i_2__0_n_0 ),
        .I1(rd_pntr_plus1[6]),
        .I2(rd_pntr_plus1[7]),
        .O(plusOp__1[7]));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \gc0.count[8]_i_1__0 
       (.I0(rd_pntr_plus1[6]),
        .I1(\gc0.count[8]_i_2__0_n_0 ),
        .I2(rd_pntr_plus1[7]),
        .I3(rd_pntr_plus1[8]),
        .O(plusOp__1[8]));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \gc0.count[8]_i_2__0 
       (.I0(rd_pntr_plus1[5]),
        .I1(rd_pntr_plus1[3]),
        .I2(rd_pntr_plus1[1]),
        .I3(rd_pntr_plus1[0]),
        .I4(rd_pntr_plus1[2]),
        .I5(rd_pntr_plus1[4]),
        .O(\gc0.count[8]_i_2__0_n_0 ));
  FDCE #(
    .INIT(1'b0)) 
    \gc0.count_d1_reg[0] 
       (.C(s_axi_aclk),
        .CE(E),
        .CLR(Q),
        .D(rd_pntr_plus1[0]),
        .Q(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram [0]));
  FDCE #(
    .INIT(1'b0)) 
    \gc0.count_d1_reg[1] 
       (.C(s_axi_aclk),
        .CE(E),
        .CLR(Q),
        .D(rd_pntr_plus1[1]),
        .Q(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram [1]));
  FDCE #(
    .INIT(1'b0)) 
    \gc0.count_d1_reg[2] 
       (.C(s_axi_aclk),
        .CE(E),
        .CLR(Q),
        .D(rd_pntr_plus1[2]),
        .Q(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram [2]));
  FDCE #(
    .INIT(1'b0)) 
    \gc0.count_d1_reg[3] 
       (.C(s_axi_aclk),
        .CE(E),
        .CLR(Q),
        .D(rd_pntr_plus1[3]),
        .Q(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram [3]));
  FDCE #(
    .INIT(1'b0)) 
    \gc0.count_d1_reg[4] 
       (.C(s_axi_aclk),
        .CE(E),
        .CLR(Q),
        .D(rd_pntr_plus1[4]),
        .Q(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram [4]));
  FDCE #(
    .INIT(1'b0)) 
    \gc0.count_d1_reg[5] 
       (.C(s_axi_aclk),
        .CE(E),
        .CLR(Q),
        .D(rd_pntr_plus1[5]),
        .Q(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram [5]));
  FDCE #(
    .INIT(1'b0)) 
    \gc0.count_d1_reg[6] 
       (.C(s_axi_aclk),
        .CE(E),
        .CLR(Q),
        .D(rd_pntr_plus1[6]),
        .Q(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram [6]));
  FDCE #(
    .INIT(1'b0)) 
    \gc0.count_d1_reg[7] 
       (.C(s_axi_aclk),
        .CE(E),
        .CLR(Q),
        .D(rd_pntr_plus1[7]),
        .Q(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram [7]));
  FDCE #(
    .INIT(1'b0)) 
    \gc0.count_d1_reg[8] 
       (.C(s_axi_aclk),
        .CE(E),
        .CLR(Q),
        .D(rd_pntr_plus1[8]),
        .Q(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram [8]));
  FDPE #(
    .INIT(1'b1)) 
    \gc0.count_reg[0] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(plusOp__1[0]),
        .PRE(Q),
        .Q(rd_pntr_plus1[0]));
  FDCE #(
    .INIT(1'b0)) 
    \gc0.count_reg[1] 
       (.C(s_axi_aclk),
        .CE(E),
        .CLR(Q),
        .D(plusOp__1[1]),
        .Q(rd_pntr_plus1[1]));
  FDCE #(
    .INIT(1'b0)) 
    \gc0.count_reg[2] 
       (.C(s_axi_aclk),
        .CE(E),
        .CLR(Q),
        .D(plusOp__1[2]),
        .Q(rd_pntr_plus1[2]));
  FDCE #(
    .INIT(1'b0)) 
    \gc0.count_reg[3] 
       (.C(s_axi_aclk),
        .CE(E),
        .CLR(Q),
        .D(plusOp__1[3]),
        .Q(rd_pntr_plus1[3]));
  FDCE #(
    .INIT(1'b0)) 
    \gc0.count_reg[4] 
       (.C(s_axi_aclk),
        .CE(E),
        .CLR(Q),
        .D(plusOp__1[4]),
        .Q(rd_pntr_plus1[4]));
  FDCE #(
    .INIT(1'b0)) 
    \gc0.count_reg[5] 
       (.C(s_axi_aclk),
        .CE(E),
        .CLR(Q),
        .D(plusOp__1[5]),
        .Q(rd_pntr_plus1[5]));
  FDCE #(
    .INIT(1'b0)) 
    \gc0.count_reg[6] 
       (.C(s_axi_aclk),
        .CE(E),
        .CLR(Q),
        .D(plusOp__1[6]),
        .Q(rd_pntr_plus1[6]));
  FDCE #(
    .INIT(1'b0)) 
    \gc0.count_reg[7] 
       (.C(s_axi_aclk),
        .CE(E),
        .CLR(Q),
        .D(plusOp__1[7]),
        .Q(rd_pntr_plus1[7]));
  FDCE #(
    .INIT(1'b0)) 
    \gc0.count_reg[8] 
       (.C(s_axi_aclk),
        .CE(E),
        .CLR(Q),
        .D(plusOp__1[8]),
        .Q(rd_pntr_plus1[8]));
  LUT4 #(
    .INIT(16'h9009)) 
    \gmux.gm[0].gm1.m1_i_1 
       (.I0(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram [0]),
        .I1(\gcc0.gc0.count_d1_reg[8] [0]),
        .I2(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram [1]),
        .I3(\gcc0.gc0.count_d1_reg[8] [1]),
        .O(v1_reg_0[0]));
  LUT4 #(
    .INIT(16'h9009)) 
    \gmux.gm[0].gm1.m1_i_1__4 
       (.I0(rd_pntr_plus1[0]),
        .I1(\gcc0.gc0.count_d1_reg[8] [0]),
        .I2(\gcc0.gc0.count_d1_reg[8] [1]),
        .I3(rd_pntr_plus1[1]),
        .O(v1_reg_1[0]));
  LUT4 #(
    .INIT(16'h9009)) 
    \gmux.gm[0].gm1.m1_i_1__5 
       (.I0(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram [0]),
        .I1(\gcc0.gc0.count_reg[8] [0]),
        .I2(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram [1]),
        .I3(\gcc0.gc0.count_reg[8] [1]),
        .O(v1_reg[0]));
  LUT4 #(
    .INIT(16'h9009)) 
    \gmux.gm[0].gm1.m1_i_1__7 
       (.I0(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram [0]),
        .I1(\gcc0.gc0.count_d1_reg[8] [0]),
        .I2(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram [1]),
        .I3(\gcc0.gc0.count_d1_reg[8] [1]),
        .O(ram_empty_fb_i_reg));
  LUT4 #(
    .INIT(16'h9009)) 
    \gmux.gm[1].gms.ms_i_1 
       (.I0(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram [2]),
        .I1(\gcc0.gc0.count_d1_reg[8] [2]),
        .I2(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram [3]),
        .I3(\gcc0.gc0.count_d1_reg[8] [3]),
        .O(v1_reg_0[1]));
  LUT4 #(
    .INIT(16'h9009)) 
    \gmux.gm[1].gms.ms_i_1__4 
       (.I0(rd_pntr_plus1[2]),
        .I1(\gcc0.gc0.count_d1_reg[8] [2]),
        .I2(\gcc0.gc0.count_d1_reg[8] [3]),
        .I3(rd_pntr_plus1[3]),
        .O(v1_reg_1[1]));
  LUT4 #(
    .INIT(16'h9009)) 
    \gmux.gm[1].gms.ms_i_1__5 
       (.I0(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram [2]),
        .I1(\gcc0.gc0.count_reg[8] [2]),
        .I2(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram [3]),
        .I3(\gcc0.gc0.count_reg[8] [3]),
        .O(v1_reg[1]));
  LUT4 #(
    .INIT(16'h9009)) 
    \gmux.gm[1].gms.ms_i_1__7 
       (.I0(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram [2]),
        .I1(\gcc0.gc0.count_d1_reg[8] [2]),
        .I2(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram [3]),
        .I3(\gcc0.gc0.count_d1_reg[8] [3]),
        .O(ram_empty_fb_i_reg_0));
  LUT4 #(
    .INIT(16'h9009)) 
    \gmux.gm[2].gms.ms_i_1 
       (.I0(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram [4]),
        .I1(\gcc0.gc0.count_d1_reg[8] [4]),
        .I2(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram [5]),
        .I3(\gcc0.gc0.count_d1_reg[8] [5]),
        .O(v1_reg_0[2]));
  LUT4 #(
    .INIT(16'h9009)) 
    \gmux.gm[2].gms.ms_i_1__4 
       (.I0(rd_pntr_plus1[4]),
        .I1(\gcc0.gc0.count_d1_reg[8] [4]),
        .I2(\gcc0.gc0.count_d1_reg[8] [5]),
        .I3(rd_pntr_plus1[5]),
        .O(v1_reg_1[2]));
  LUT4 #(
    .INIT(16'h9009)) 
    \gmux.gm[2].gms.ms_i_1__5 
       (.I0(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram [4]),
        .I1(\gcc0.gc0.count_reg[8] [4]),
        .I2(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram [5]),
        .I3(\gcc0.gc0.count_reg[8] [5]),
        .O(v1_reg[2]));
  LUT4 #(
    .INIT(16'h9009)) 
    \gmux.gm[2].gms.ms_i_1__7 
       (.I0(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram [4]),
        .I1(\gcc0.gc0.count_d1_reg[8] [4]),
        .I2(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram [5]),
        .I3(\gcc0.gc0.count_d1_reg[8] [5]),
        .O(ram_empty_fb_i_reg_1));
  LUT4 #(
    .INIT(16'h9009)) 
    \gmux.gm[3].gms.ms_i_1 
       (.I0(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram [6]),
        .I1(\gcc0.gc0.count_d1_reg[8] [6]),
        .I2(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram [7]),
        .I3(\gcc0.gc0.count_d1_reg[8] [7]),
        .O(v1_reg_0[3]));
  LUT4 #(
    .INIT(16'h9009)) 
    \gmux.gm[3].gms.ms_i_1__4 
       (.I0(rd_pntr_plus1[6]),
        .I1(\gcc0.gc0.count_d1_reg[8] [6]),
        .I2(\gcc0.gc0.count_d1_reg[8] [7]),
        .I3(rd_pntr_plus1[7]),
        .O(v1_reg_1[3]));
  LUT4 #(
    .INIT(16'h9009)) 
    \gmux.gm[3].gms.ms_i_1__5 
       (.I0(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram [6]),
        .I1(\gcc0.gc0.count_reg[8] [6]),
        .I2(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram [7]),
        .I3(\gcc0.gc0.count_reg[8] [7]),
        .O(v1_reg[3]));
  LUT4 #(
    .INIT(16'h9009)) 
    \gmux.gm[3].gms.ms_i_1__7 
       (.I0(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram [6]),
        .I1(\gcc0.gc0.count_d1_reg[8] [6]),
        .I2(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram [7]),
        .I3(\gcc0.gc0.count_d1_reg[8] [7]),
        .O(ram_empty_fb_i_reg_2));
  LUT2 #(
    .INIT(4'h9)) 
    \gmux.gm[4].gms.ms_i_1__2 
       (.I0(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram [8]),
        .I1(\gcc0.gc0.count_reg[8] [8]),
        .O(v1_reg[4]));
  LUT2 #(
    .INIT(4'h9)) 
    \gmux.gm[4].gms.ms_i_1__5 
       (.I0(rd_pntr_plus1[8]),
        .I1(\gcc0.gc0.count_d1_reg[8] [8]),
        .O(v1_reg_1[4]));
  LUT2 #(
    .INIT(4'h9)) 
    plusOp_carry__1_i_1__2
       (.I0(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram [8]),
        .I1(\gcc0.gc0.count_reg[8] [8]),
        .O(\gdiff.gcry_1_sym.diff_pntr_pad_reg[9] ));
endmodule

(* ORIG_REF_NAME = "fifo_generator_v13_0_5_rd_fwft" *) 
module axi_fifo_mm_s_0_fifo_generator_v13_0_5_rd_fwft
   (\gpregsm1.curr_fwft_state_reg[0]_0 ,
    ram_rd_en_i,
    E,
    \goreg_bm.dout_i_reg[40] ,
    DI,
    axis_rd_en__0,
    axi_str_txd_tvalid,
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_reg ,
    \gtxc.txc_str_Valid_reg ,
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[4] ,
    s_axi_aclk,
    Q,
    rd_en_int_sync,
    rst_int_sync_1,
    rst_int_sync,
    p_2_out,
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_reg_0 ,
    axi_str_txd_tready,
    \goreg_bm.dout_i_reg[0] ,
    axis_wr_eop_d1,
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[3] ,
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[5] ,
    p_0_in__10,
    axi_str_txc_tready,
    \gtxc.TXC_STATE_reg[0] ,
    \gtxc.TXC_STATE_reg[1] ,
    axi_str_txc_tvalid,
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[1] ,
    ram_full_i_reg,
    start_wr,
    DIADI,
    txd_wr_en);
  output \gpregsm1.curr_fwft_state_reg[0]_0 ;
  output ram_rd_en_i;
  output [0:0]E;
  output [0:0]\goreg_bm.dout_i_reg[40] ;
  output [0:0]DI;
  output axis_rd_en__0;
  output axi_str_txd_tvalid;
  output \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_reg ;
  output \gtxc.txc_str_Valid_reg ;
  output [0:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[4] ;
  input s_axi_aclk;
  input [1:0]Q;
  input rd_en_int_sync;
  input rst_int_sync_1;
  input rst_int_sync;
  input p_2_out;
  input \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_reg_0 ;
  input axi_str_txd_tready;
  input [0:0]\goreg_bm.dout_i_reg[0] ;
  input axis_wr_eop_d1;
  input [3:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[3] ;
  input \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[5] ;
  input p_0_in__10;
  input axi_str_txc_tready;
  input \gtxc.TXC_STATE_reg[0] ;
  input \gtxc.TXC_STATE_reg[1] ;
  input axi_str_txc_tvalid;
  input [0:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[1] ;
  input ram_full_i_reg;
  input start_wr;
  input [0:0]DIADI;
  input txd_wr_en;

  wire [0:0]DI;
  wire [0:0]DIADI;
  wire [0:0]E;
  wire [1:0]Q;
  wire axi_str_txc_tready;
  wire axi_str_txc_tvalid;
  wire axi_str_txd_tready;
  wire axi_str_txd_tvalid;
  wire axis_rd_en__0;
  wire axis_wr_eop_d1;
  wire [0:0]curr_fwft_state;
  wire empty_fwft_fb;
  wire empty_fwft_i0;
  wire [3:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[3] ;
  wire \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[5] ;
  wire \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_i_5_n_0 ;
  wire \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_reg ;
  wire \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_reg_0 ;
  wire [0:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[1] ;
  wire [0:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[4] ;
  wire [0:0]\goreg_bm.dout_i_reg[0] ;
  wire [0:0]\goreg_bm.dout_i_reg[40] ;
  wire \gpregsm1.curr_fwft_state_reg[0]_0 ;
  wire \gpregsm1.curr_fwft_state_reg_n_0_[1] ;
  wire \gtxc.TXC_STATE_reg[0] ;
  wire \gtxc.TXC_STATE_reg[1] ;
  wire \gtxc.txc_str_Valid_reg ;
  wire [1:0]next_fwft_state;
  wire p_0_in__10;
  wire p_14_out__5;
  wire p_2_out;
  wire ram_full_i_reg;
  wire ram_rd_en_i;
  wire rd_en_int_sync;
  wire rst_int_sync;
  wire rst_int_sync_1;
  wire s_axi_aclk;
  wire start_wr;
  wire txd_wr_en;

  LUT6 #(
    .INIT(64'h00000000AAAAAAAC)) 
    \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_i_1 
       (.I0(rd_en_int_sync),
        .I1(E),
        .I2(rst_int_sync_1),
        .I3(Q[0]),
        .I4(rst_int_sync),
        .I5(p_2_out),
        .O(ram_rd_en_i));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT2 #(
    .INIT(4'h2)) 
    axi_str_txd_tvalid_INST_0
       (.I0(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_reg_0 ),
        .I1(\gpregsm1.curr_fwft_state_reg[0]_0 ),
        .O(axi_str_txd_tvalid));
  LUT6 #(
    .INIT(64'hFF000000FF20FF00)) 
    empty_fwft_fb_i_1
       (.I0(axi_str_txd_tready),
        .I1(\gpregsm1.curr_fwft_state_reg[0]_0 ),
        .I2(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_reg_0 ),
        .I3(empty_fwft_fb),
        .I4(curr_fwft_state),
        .I5(\gpregsm1.curr_fwft_state_reg_n_0_[1] ),
        .O(empty_fwft_i0));
  (* equivalent_register_removal = "no" *) 
  FDPE #(
    .INIT(1'b1)) 
    empty_fwft_fb_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(empty_fwft_i0),
        .PRE(Q[1]),
        .Q(empty_fwft_fb));
  (* equivalent_register_removal = "no" *) 
  FDPE #(
    .INIT(1'b1)) 
    empty_fwft_i_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(empty_fwft_i0),
        .PRE(Q[1]),
        .Q(\gpregsm1.curr_fwft_state_reg[0]_0 ));
  LUT3 #(
    .INIT(8'h54)) 
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_i_1 
       (.I0(p_14_out__5),
        .I1(p_0_in__10),
        .I2(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_reg_0 ),
        .O(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_reg ));
  LUT6 #(
    .INIT(64'h0004000000000000)) 
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_i_2 
       (.I0(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[3] [1]),
        .I1(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[3] [0]),
        .I2(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[3] [2]),
        .I3(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[3] [3]),
        .I4(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[5] ),
        .I5(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_i_5_n_0 ),
        .O(p_14_out__5));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT5 #(
    .INIT(32'h00002000)) 
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_i_5 
       (.I0(axi_str_txd_tready),
        .I1(\gpregsm1.curr_fwft_state_reg[0]_0 ),
        .I2(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_reg_0 ),
        .I3(\goreg_bm.dout_i_reg[0] ),
        .I4(axis_wr_eop_d1),
        .O(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'h20)) 
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo[9]_i_2 
       (.I0(axi_str_txd_tready),
        .I1(\gpregsm1.curr_fwft_state_reg[0]_0 ),
        .I2(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_reg_0 ),
        .O(axis_rd_en__0));
  LUT6 #(
    .INIT(64'h002000FF00000000)) 
    \goreg_bm.dout_i[0]_i_1 
       (.I0(axi_str_txd_tready),
        .I1(\gpregsm1.curr_fwft_state_reg[0]_0 ),
        .I2(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_reg_0 ),
        .I3(rst_int_sync),
        .I4(curr_fwft_state),
        .I5(\gpregsm1.curr_fwft_state_reg_n_0_[1] ),
        .O(\goreg_bm.dout_i_reg[40] ));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT5 #(
    .INIT(32'hFFFFDF00)) 
    \gpregsm1.curr_fwft_state[0]_i_1 
       (.I0(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_reg_0 ),
        .I1(\gpregsm1.curr_fwft_state_reg[0]_0 ),
        .I2(axi_str_txd_tready),
        .I3(curr_fwft_state),
        .I4(\gpregsm1.curr_fwft_state_reg_n_0_[1] ),
        .O(next_fwft_state[0]));
  LUT6 #(
    .INIT(64'hDF000000FFFFFFFF)) 
    \gpregsm1.curr_fwft_state[1]_i_1 
       (.I0(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_reg_0 ),
        .I1(\gpregsm1.curr_fwft_state_reg[0]_0 ),
        .I2(axi_str_txd_tready),
        .I3(\gpregsm1.curr_fwft_state_reg_n_0_[1] ),
        .I4(curr_fwft_state),
        .I5(p_2_out),
        .O(next_fwft_state[1]));
  (* equivalent_register_removal = "no" *) 
  FDCE #(
    .INIT(1'b0)) 
    \gpregsm1.curr_fwft_state_reg[0] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .CLR(Q[1]),
        .D(next_fwft_state[0]),
        .Q(curr_fwft_state));
  (* equivalent_register_removal = "no" *) 
  FDCE #(
    .INIT(1'b0)) 
    \gpregsm1.curr_fwft_state_reg[1] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .CLR(Q[1]),
        .D(next_fwft_state[1]),
        .Q(\gpregsm1.curr_fwft_state_reg_n_0_[1] ));
  LUT6 #(
    .INIT(64'h0000000020FFFFFF)) 
    \greg.ram_rd_en_i_i_1 
       (.I0(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_reg_0 ),
        .I1(\gpregsm1.curr_fwft_state_reg[0]_0 ),
        .I2(axi_str_txd_tready),
        .I3(curr_fwft_state),
        .I4(\gpregsm1.curr_fwft_state_reg_n_0_[1] ),
        .I5(p_2_out),
        .O(E));
  LUT6 #(
    .INIT(64'hFF55FFFF0000000C)) 
    \gtxc.txc_str_Valid_i_2 
       (.I0(axi_str_txc_tready),
        .I1(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_reg_0 ),
        .I2(\gpregsm1.curr_fwft_state_reg[0]_0 ),
        .I3(\gtxc.TXC_STATE_reg[0] ),
        .I4(\gtxc.TXC_STATE_reg[1] ),
        .I5(axi_str_txc_tvalid),
        .O(\gtxc.txc_str_Valid_reg ));
  LUT6 #(
    .INIT(64'h5655565556555555)) 
    p_0_out__24_carry_i_5
       (.I0(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[1] ),
        .I1(axis_rd_en__0),
        .I2(ram_full_i_reg),
        .I3(start_wr),
        .I4(DIADI),
        .I5(txd_wr_en),
        .O(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[4] ));
  LUT5 #(
    .INIT(32'hDFFF0000)) 
    p_0_out_carry_i_1
       (.I0(axi_str_txd_tready),
        .I1(\gpregsm1.curr_fwft_state_reg[0]_0 ),
        .I2(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_reg_0 ),
        .I3(\goreg_bm.dout_i_reg[0] ),
        .I4(axis_wr_eop_d1),
        .O(DI));
endmodule

(* ORIG_REF_NAME = "fifo_generator_v13_0_5_rd_fwft" *) 
module axi_fifo_mm_s_0_fifo_generator_v13_0_5_rd_fwft_16
   (valid_fwft,
    empty_fwft_i,
    E,
    ram_rd_en_i,
    \gc0.count_reg[0] ,
    \gc0.count_reg[0]_0 ,
    \gc0.count_reg[0]_1 ,
    s_axi_aclk,
    Q,
    axis_rd_en__0,
    p_2_out,
    rst_int_sync,
    rd_en_int_sync,
    rst_int_sync_1,
    \gaxi_full_sm.r_valid_r_reg ,
    \gaxi_full_sm.r_last_r_reg ,
    sig_axi_rd_en,
    \s_axi4_araddr[21] ,
    \s_axi4_araddr[29] ,
    s_axi4_araddr);
  output valid_fwft;
  output empty_fwft_i;
  output [0:0]E;
  output ram_rd_en_i;
  output [0:0]\gc0.count_reg[0] ;
  output \gc0.count_reg[0]_0 ;
  output \gc0.count_reg[0]_1 ;
  input s_axi_aclk;
  input [1:0]Q;
  input axis_rd_en__0;
  input p_2_out;
  input rst_int_sync;
  input rd_en_int_sync;
  input rst_int_sync_1;
  input \gaxi_full_sm.r_valid_r_reg ;
  input \gaxi_full_sm.r_last_r_reg ;
  input sig_axi_rd_en;
  input \s_axi4_araddr[21] ;
  input \s_axi4_araddr[29] ;
  input [11:0]s_axi4_araddr;

  wire [0:0]E;
  wire [1:0]Q;
  wire axis_rd_en__0;
  wire [0:0]curr_fwft_state;
  wire empty_fwft_fb;
  wire empty_fwft_i;
  wire empty_fwft_i0;
  wire \gaxi_full_sm.r_last_r_reg ;
  wire \gaxi_full_sm.r_valid_r_reg ;
  wire [0:0]\gc0.count_reg[0] ;
  wire \gc0.count_reg[0]_0 ;
  wire \gc0.count_reg[0]_1 ;
  wire \gpregsm1.curr_fwft_state_reg_n_0_[1] ;
  wire \greg.ram_rd_en_i_i_4_n_0 ;
  wire \greg.ram_rd_en_i_i_8_n_0 ;
  wire \greg.ram_rd_en_i_i_9_n_0 ;
  wire [1:0]next_fwft_state;
  wire p_2_out;
  wire ram_rd_en_i;
  wire rd_en_int_sync;
  wire rst_int_sync;
  wire rst_int_sync_1;
  wire [11:0]s_axi4_araddr;
  wire \s_axi4_araddr[21] ;
  wire \s_axi4_araddr[29] ;
  wire s_axi_aclk;
  wire sig_axi_rd_en;
  wire valid_fwft;

  LUT6 #(
    .INIT(64'h00000000AAAAAAAC)) 
    \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_i_1__0 
       (.I0(rd_en_int_sync),
        .I1(\gc0.count_reg[0] ),
        .I2(rst_int_sync_1),
        .I3(Q[0]),
        .I4(rst_int_sync),
        .I5(p_2_out),
        .O(ram_rd_en_i));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'hA0EA)) 
    empty_fwft_fb_i_1__0
       (.I0(empty_fwft_fb),
        .I1(axis_rd_en__0),
        .I2(curr_fwft_state),
        .I3(\gpregsm1.curr_fwft_state_reg_n_0_[1] ),
        .O(empty_fwft_i0));
  (* equivalent_register_removal = "no" *) 
  FDPE #(
    .INIT(1'b1)) 
    empty_fwft_fb_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(empty_fwft_i0),
        .PRE(Q[1]),
        .Q(empty_fwft_fb));
  (* equivalent_register_removal = "no" *) 
  FDPE #(
    .INIT(1'b1)) 
    empty_fwft_i_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(empty_fwft_i0),
        .PRE(Q[1]),
        .Q(empty_fwft_i));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h4404)) 
    \goreg_bm.dout_i[40]_i_1 
       (.I0(rst_int_sync),
        .I1(\gpregsm1.curr_fwft_state_reg_n_0_[1] ),
        .I2(curr_fwft_state),
        .I3(axis_rd_en__0),
        .O(E));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'hF4)) 
    \gpregsm1.curr_fwft_state[0]_i_1__0 
       (.I0(axis_rd_en__0),
        .I1(curr_fwft_state),
        .I2(\gpregsm1.curr_fwft_state_reg_n_0_[1] ),
        .O(next_fwft_state[0]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'h40FF)) 
    \gpregsm1.curr_fwft_state[1]_i_1__0 
       (.I0(axis_rd_en__0),
        .I1(\gpregsm1.curr_fwft_state_reg_n_0_[1] ),
        .I2(curr_fwft_state),
        .I3(p_2_out),
        .O(next_fwft_state[1]));
  (* equivalent_register_removal = "no" *) 
  FDCE #(
    .INIT(1'b0)) 
    \gpregsm1.curr_fwft_state_reg[0] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .CLR(Q[1]),
        .D(next_fwft_state[0]),
        .Q(curr_fwft_state));
  (* equivalent_register_removal = "no" *) 
  FDCE #(
    .INIT(1'b0)) 
    \gpregsm1.curr_fwft_state_reg[1] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .CLR(Q[1]),
        .D(next_fwft_state[1]),
        .Q(\gpregsm1.curr_fwft_state_reg_n_0_[1] ));
  (* equivalent_register_removal = "no" *) 
  FDCE #(
    .INIT(1'b0)) 
    \gpregsm1.user_valid_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .CLR(Q[1]),
        .D(next_fwft_state[0]),
        .Q(valid_fwft));
  LUT6 #(
    .INIT(64'h000000004440FFFF)) 
    \greg.ram_rd_en_i_i_1__0 
       (.I0(empty_fwft_i),
        .I1(\gaxi_full_sm.r_valid_r_reg ),
        .I2(\gaxi_full_sm.r_last_r_reg ),
        .I3(sig_axi_rd_en),
        .I4(\greg.ram_rd_en_i_i_4_n_0 ),
        .I5(p_2_out),
        .O(\gc0.count_reg[0] ));
  LUT2 #(
    .INIT(4'h8)) 
    \greg.ram_rd_en_i_i_4 
       (.I0(curr_fwft_state),
        .I1(\gpregsm1.curr_fwft_state_reg_n_0_[1] ),
        .O(\greg.ram_rd_en_i_i_4_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \greg.ram_rd_en_i_i_6 
       (.I0(\s_axi4_araddr[21] ),
        .I1(\greg.ram_rd_en_i_i_8_n_0 ),
        .I2(\s_axi4_araddr[29] ),
        .I3(\greg.ram_rd_en_i_i_9_n_0 ),
        .O(\gc0.count_reg[0]_0 ));
  LUT4 #(
    .INIT(16'h0001)) 
    \greg.ram_rd_en_i_i_7 
       (.I0(s_axi4_araddr[3]),
        .I1(s_axi4_araddr[2]),
        .I2(s_axi4_araddr[1]),
        .I3(s_axi4_araddr[0]),
        .O(\gc0.count_reg[0]_1 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \greg.ram_rd_en_i_i_8 
       (.I0(s_axi4_araddr[4]),
        .I1(s_axi4_araddr[5]),
        .I2(s_axi4_araddr[6]),
        .I3(s_axi4_araddr[7]),
        .O(\greg.ram_rd_en_i_i_8_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \greg.ram_rd_en_i_i_9 
       (.I0(s_axi4_araddr[8]),
        .I1(s_axi4_araddr[9]),
        .I2(s_axi4_araddr[10]),
        .I3(s_axi4_araddr[11]),
        .O(\greg.ram_rd_en_i_i_9_n_0 ));
endmodule

(* ORIG_REF_NAME = "fifo_generator_v13_0_5_rd_logic" *) 
module axi_fifo_mm_s_0_fifo_generator_v13_0_5_rd_logic
   (p_8_out,
    p_6_out,
    sig_txd_prog_empty,
    ram_rd_en_i,
    E,
    DI,
    axis_rd_en__0,
    axi_str_txd_tvalid,
    v1_reg,
    \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram ,
    v1_reg_0,
    v1_reg_1,
    sig_txd_pe_event__1,
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_reg ,
    \gtxc.txc_str_Valid_reg ,
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[4] ,
    \gcc0.gc1.gsym.count_d2_reg[8] ,
    s_axi_aclk,
    Q,
    p_16_out,
    rd_pntr_inv_pad,
    \gcc0.gc1.gsym.count_d2_reg[8]_0 ,
    S,
    \gcc0.gc1.gsym.count_d2_reg[7] ,
    \gcc0.gc1.gsym.count_d2_reg[8]_1 ,
    rd_en_int_sync,
    rst_int_sync_1,
    rst_int_sync,
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_reg_0 ,
    axi_str_txd_tready,
    \goreg_bm.dout_i_reg[0] ,
    axis_wr_eop_d1,
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[3] ,
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[5] ,
    \gcc0.gc1.gsym.count_d1_reg[7] ,
    \gcc0.gc1.gsym.count_reg[8] ,
    sig_txd_prog_empty_d1,
    p_0_in__10,
    axi_str_txc_tready,
    \gtxc.TXC_STATE_reg[0] ,
    \gtxc.TXC_STATE_reg[1] ,
    axi_str_txc_tvalid,
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[1] ,
    ram_full_i_reg,
    start_wr,
    DIADI,
    txd_wr_en);
  output p_8_out;
  output p_6_out;
  output sig_txd_prog_empty;
  output ram_rd_en_i;
  output [0:0]E;
  output [0:0]DI;
  output axis_rd_en__0;
  output axi_str_txd_tvalid;
  output [3:0]v1_reg;
  output [8:0]\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram ;
  output [3:0]v1_reg_0;
  output [4:0]v1_reg_1;
  output sig_txd_pe_event__1;
  output \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_reg ;
  output \gtxc.txc_str_Valid_reg ;
  output [0:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[4] ;
  input \gcc0.gc1.gsym.count_d2_reg[8] ;
  input s_axi_aclk;
  input [1:0]Q;
  input p_16_out;
  input [0:0]rd_pntr_inv_pad;
  input [8:0]\gcc0.gc1.gsym.count_d2_reg[8]_0 ;
  input [3:0]S;
  input [3:0]\gcc0.gc1.gsym.count_d2_reg[7] ;
  input [0:0]\gcc0.gc1.gsym.count_d2_reg[8]_1 ;
  input rd_en_int_sync;
  input rst_int_sync_1;
  input rst_int_sync;
  input \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_reg_0 ;
  input axi_str_txd_tready;
  input [0:0]\goreg_bm.dout_i_reg[0] ;
  input axis_wr_eop_d1;
  input [3:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[3] ;
  input \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[5] ;
  input [7:0]\gcc0.gc1.gsym.count_d1_reg[7] ;
  input [8:0]\gcc0.gc1.gsym.count_reg[8] ;
  input sig_txd_prog_empty_d1;
  input p_0_in__10;
  input axi_str_txc_tready;
  input \gtxc.TXC_STATE_reg[0] ;
  input \gtxc.TXC_STATE_reg[1] ;
  input axi_str_txc_tvalid;
  input [0:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[1] ;
  input ram_full_i_reg;
  input start_wr;
  input [0:0]DIADI;
  input txd_wr_en;

  wire [8:0]\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram ;
  wire [0:0]DI;
  wire [0:0]DIADI;
  wire [0:0]E;
  wire [1:0]Q;
  wire [3:0]S;
  wire axi_str_txc_tready;
  wire axi_str_txc_tvalid;
  wire axi_str_txd_tready;
  wire axi_str_txd_tvalid;
  wire axis_rd_en__0;
  wire axis_wr_eop_d1;
  wire [4:0]\c2/v1_reg ;
  wire [3:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[3] ;
  wire \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[5] ;
  wire \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_reg ;
  wire \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_reg_0 ;
  wire [0:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[1] ;
  wire [0:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[4] ;
  wire [7:0]\gcc0.gc1.gsym.count_d1_reg[7] ;
  wire [3:0]\gcc0.gc1.gsym.count_d2_reg[7] ;
  wire \gcc0.gc1.gsym.count_d2_reg[8] ;
  wire [8:0]\gcc0.gc1.gsym.count_d2_reg[8]_0 ;
  wire [0:0]\gcc0.gc1.gsym.count_d2_reg[8]_1 ;
  wire [8:0]\gcc0.gc1.gsym.count_reg[8] ;
  wire [0:0]\goreg_bm.dout_i_reg[0] ;
  wire \gtxc.TXC_STATE_reg[0] ;
  wire \gtxc.TXC_STATE_reg[1] ;
  wire \gtxc.txc_str_Valid_reg ;
  wire p_0_in__10;
  wire p_16_out;
  wire p_2_out;
  wire p_6_out;
  wire p_8_out;
  wire ram_full_i_reg;
  wire ram_rd_en_i;
  wire rd_en_int_sync;
  wire [0:0]rd_pntr_inv_pad;
  wire rpntr_n_27;
  wire rpntr_n_28;
  wire rpntr_n_29;
  wire rpntr_n_30;
  wire rst_int_sync;
  wire rst_int_sync_1;
  wire s_axi_aclk;
  wire sig_txd_pe_event__1;
  wire sig_txd_prog_empty;
  wire sig_txd_prog_empty_d1;
  wire start_wr;
  wire txd_wr_en;
  wire [3:0]v1_reg;
  wire [3:0]v1_reg_0;
  wire [4:0]v1_reg_1;

  axi_fifo_mm_s_0_fifo_generator_v13_0_5_rd_fwft \gr1.rfwft 
       (.DI(DI),
        .DIADI(DIADI),
        .E(p_6_out),
        .Q(Q),
        .axi_str_txc_tready(axi_str_txc_tready),
        .axi_str_txc_tvalid(axi_str_txc_tvalid),
        .axi_str_txd_tready(axi_str_txd_tready),
        .axi_str_txd_tvalid(axi_str_txd_tvalid),
        .axis_rd_en__0(axis_rd_en__0),
        .axis_wr_eop_d1(axis_wr_eop_d1),
        .\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[3] (\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[3] ),
        .\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[5] (\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[5] ),
        .\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_reg (\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_reg ),
        .\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_reg_0 (\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_reg_0 ),
        .\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[1] (\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[1] ),
        .\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[4] (\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[4] ),
        .\goreg_bm.dout_i_reg[0] (\goreg_bm.dout_i_reg[0] ),
        .\goreg_bm.dout_i_reg[40] (E),
        .\gpregsm1.curr_fwft_state_reg[0]_0 (p_8_out),
        .\gtxc.TXC_STATE_reg[0] (\gtxc.TXC_STATE_reg[0] ),
        .\gtxc.TXC_STATE_reg[1] (\gtxc.TXC_STATE_reg[1] ),
        .\gtxc.txc_str_Valid_reg (\gtxc.txc_str_Valid_reg ),
        .p_0_in__10(p_0_in__10),
        .p_2_out(p_2_out),
        .ram_full_i_reg(ram_full_i_reg),
        .ram_rd_en_i(ram_rd_en_i),
        .rd_en_int_sync(rd_en_int_sync),
        .rst_int_sync(rst_int_sync),
        .rst_int_sync_1(rst_int_sync_1),
        .s_axi_aclk(s_axi_aclk),
        .start_wr(start_wr),
        .txd_wr_en(txd_wr_en));
  axi_fifo_mm_s_0_fifo_generator_v13_0_5_rd_pe_ss \grss.gpe.rdpe 
       (.E(p_6_out),
        .Q(Q[1]),
        .S(S),
        .\gcc0.gc1.gsym.count_d2_reg[7] (\gcc0.gc1.gsym.count_d2_reg[8]_0 [7:0]),
        .\gcc0.gc1.gsym.count_d2_reg[7]_0 (\gcc0.gc1.gsym.count_d2_reg[7] ),
        .\gcc0.gc1.gsym.count_d2_reg[8] (\gcc0.gc1.gsym.count_d2_reg[8]_1 ),
        .p_16_out(p_16_out),
        .rd_pntr_inv_pad(rd_pntr_inv_pad),
        .s_axi_aclk(s_axi_aclk),
        .sig_txd_pe_event__1(sig_txd_pe_event__1),
        .sig_txd_prog_empty(sig_txd_prog_empty),
        .sig_txd_prog_empty_d1(sig_txd_prog_empty_d1));
  axi_fifo_mm_s_0_fifo_generator_v13_0_5_rd_status_flags_ss \grss.rsts 
       (.E(p_6_out),
        .Q(Q[1]),
        .\gc0.count_d1_reg[0] (rpntr_n_27),
        .\gc0.count_d1_reg[2] (rpntr_n_28),
        .\gc0.count_d1_reg[4] (rpntr_n_29),
        .\gc0.count_d1_reg[6] (rpntr_n_30),
        .\gcc0.gc1.gsym.count_d2_reg[8] (\gcc0.gc1.gsym.count_d2_reg[8] ),
        .p_16_out(p_16_out),
        .p_2_out(p_2_out),
        .s_axi_aclk(s_axi_aclk),
        .v1_reg(\c2/v1_reg ));
  axi_fifo_mm_s_0_fifo_generator_v13_0_5_rd_bin_cntr rpntr
       (.E(p_6_out),
        .Q(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram ),
        .\gcc0.gc1.gsym.count_d1_reg[7] (\gcc0.gc1.gsym.count_d1_reg[7] ),
        .\gcc0.gc1.gsym.count_d2_reg[8] (\gcc0.gc1.gsym.count_d2_reg[8]_0 ),
        .\gcc0.gc1.gsym.count_reg[8] (\gcc0.gc1.gsym.count_reg[8] ),
        .\ngwrdrst.grst.g7serrst.rd_rst_reg_reg[2] (Q[1]),
        .ram_empty_fb_i_reg(rpntr_n_27),
        .ram_empty_fb_i_reg_0(rpntr_n_28),
        .ram_empty_fb_i_reg_1(rpntr_n_29),
        .ram_empty_fb_i_reg_2(rpntr_n_30),
        .s_axi_aclk(s_axi_aclk),
        .v1_reg(v1_reg),
        .v1_reg_0(v1_reg_0),
        .v1_reg_1(v1_reg_1),
        .v1_reg_2(\c2/v1_reg ));
endmodule

(* ORIG_REF_NAME = "fifo_generator_v13_0_5_rd_logic" *) 
module axi_fifo_mm_s_0_fifo_generator_v13_0_5_rd_logic_5
   (valid_fwft,
    p_6_out,
    empty_fwft_i,
    p_9_out,
    E,
    ram_rd_en_i,
    \gc0.count_reg[0] ,
    \gc0.count_reg[0]_0 ,
    v1_reg,
    \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram ,
    DI,
    \sig_ip2bus_data_reg[20] ,
    v1_reg_0,
    \sig_register_array_reg[0][2] ,
    \sig_register_array_reg[0][1] ,
    \sig_register_array_reg[0][2]_0 ,
    sig_rxd_pe_event__1,
    \gdiff.gcry_1_sym.diff_pntr_pad_reg[9] ,
    \count_reg[9] ,
    \count_reg[4] ,
    \count_reg[8] ,
    \gcc0.gc0.count_d1_reg[8] ,
    s_axi_aclk,
    Q,
    p_16_out,
    rd_pntr_inv_pad,
    \gcc0.gc0.count_d1_reg[8]_0 ,
    S,
    \gcc0.gc0.count_d1_reg[7] ,
    \gcc0.gc0.count_d1_reg[8]_1 ,
    axis_rd_en__0,
    rst_int_sync,
    rd_en_int_sync,
    rst_int_sync_1,
    \gaxi_full_sm.r_valid_r_reg ,
    \gaxi_full_sm.r_last_r_reg ,
    sig_axi_rd_en,
    \s_axi4_araddr[21] ,
    \s_axi4_araddr[29] ,
    s_axi4_araddr,
    \gcc0.gc0.count_reg[8] ,
    sig_rx_channel_reset_reg,
    sig_rxd_rd_data,
    sig_rxd_prog_empty_d1,
    \gpregsm1.user_valid_reg ,
    D);
  output valid_fwft;
  output p_6_out;
  output empty_fwft_i;
  output p_9_out;
  output [0:0]E;
  output ram_rd_en_i;
  output \gc0.count_reg[0] ;
  output \gc0.count_reg[0]_0 ;
  output [4:0]v1_reg;
  output [8:0]\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram ;
  output [3:0]DI;
  output [6:0]\sig_ip2bus_data_reg[20] ;
  output [3:0]v1_reg_0;
  output \sig_register_array_reg[0][2] ;
  output \sig_register_array_reg[0][1] ;
  output \sig_register_array_reg[0][2]_0 ;
  output sig_rxd_pe_event__1;
  output [0:0]\gdiff.gcry_1_sym.diff_pntr_pad_reg[9] ;
  output [0:0]\count_reg[9] ;
  output [3:0]\count_reg[4] ;
  output [3:0]\count_reg[8] ;
  input \gcc0.gc0.count_d1_reg[8] ;
  input s_axi_aclk;
  input [1:0]Q;
  input p_16_out;
  input [0:0]rd_pntr_inv_pad;
  input [8:0]\gcc0.gc0.count_d1_reg[8]_0 ;
  input [3:0]S;
  input [3:0]\gcc0.gc0.count_d1_reg[7] ;
  input [0:0]\gcc0.gc0.count_d1_reg[8]_1 ;
  input axis_rd_en__0;
  input rst_int_sync;
  input rd_en_int_sync;
  input rst_int_sync_1;
  input \gaxi_full_sm.r_valid_r_reg ;
  input \gaxi_full_sm.r_last_r_reg ;
  input sig_axi_rd_en;
  input \s_axi4_araddr[21] ;
  input \s_axi4_araddr[29] ;
  input [11:0]s_axi4_araddr;
  input [8:0]\gcc0.gc0.count_reg[8] ;
  input sig_rx_channel_reset_reg;
  input [0:0]sig_rxd_rd_data;
  input sig_rxd_prog_empty_d1;
  input [0:0]\gpregsm1.user_valid_reg ;
  input [8:0]D;

  wire [8:0]D;
  wire [8:0]\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram ;
  wire [3:0]DI;
  wire [0:0]E;
  wire [1:0]Q;
  wire [3:0]S;
  wire axis_rd_en__0;
  wire [4:0]\c2/v1_reg ;
  wire [3:0]\count_reg[4] ;
  wire [3:0]\count_reg[8] ;
  wire [0:0]\count_reg[9] ;
  wire empty_fwft_i;
  wire \gaxi_full_sm.r_last_r_reg ;
  wire \gaxi_full_sm.r_valid_r_reg ;
  wire \gc0.count_reg[0] ;
  wire \gc0.count_reg[0]_0 ;
  wire [3:0]\gcc0.gc0.count_d1_reg[7] ;
  wire \gcc0.gc0.count_d1_reg[8] ;
  wire [8:0]\gcc0.gc0.count_d1_reg[8]_0 ;
  wire [0:0]\gcc0.gc0.count_d1_reg[8]_1 ;
  wire [8:0]\gcc0.gc0.count_reg[8] ;
  wire [0:0]\gdiff.gcry_1_sym.diff_pntr_pad_reg[9] ;
  wire [0:0]\gpregsm1.user_valid_reg ;
  wire p_16_out;
  wire p_2_out;
  wire p_6_out;
  wire p_9_out;
  wire ram_rd_en_i;
  wire rd_en_int_sync;
  wire [0:0]rd_pntr_inv_pad;
  wire rpntr_n_24;
  wire rpntr_n_25;
  wire rpntr_n_26;
  wire rpntr_n_27;
  wire rst_int_sync;
  wire rst_int_sync_1;
  wire [11:0]s_axi4_araddr;
  wire \s_axi4_araddr[21] ;
  wire \s_axi4_araddr[29] ;
  wire s_axi_aclk;
  wire sig_axi_rd_en;
  wire [6:0]\sig_ip2bus_data_reg[20] ;
  wire \sig_register_array_reg[0][1] ;
  wire \sig_register_array_reg[0][2] ;
  wire \sig_register_array_reg[0][2]_0 ;
  wire sig_rx_channel_reset_reg;
  wire sig_rxd_pe_event__1;
  wire sig_rxd_prog_empty_d1;
  wire [0:0]sig_rxd_rd_data;
  wire [4:0]v1_reg;
  wire [3:0]v1_reg_0;
  wire valid_fwft;

  axi_fifo_mm_s_0_fifo_generator_v13_0_5_dc_ss_fwft \gr1.gdcf.dc 
       (.D(D),
        .DI(DI),
        .Q(Q[1]),
        .axis_rd_en__0(axis_rd_en__0),
        .\count_reg[4] (\count_reg[4] ),
        .\count_reg[8] (\count_reg[8] ),
        .\count_reg[9] (\count_reg[9] ),
        .\gpregsm1.user_valid_reg (\gpregsm1.user_valid_reg ),
        .s_axi_aclk(s_axi_aclk),
        .\sig_ip2bus_data_reg[20] (\sig_ip2bus_data_reg[20] ),
        .\sig_register_array_reg[0][1] (\sig_register_array_reg[0][1] ),
        .\sig_register_array_reg[0][2] (\sig_register_array_reg[0][2] ),
        .\sig_register_array_reg[0][2]_0 (\sig_register_array_reg[0][2]_0 ),
        .sig_rx_channel_reset_reg(sig_rx_channel_reset_reg),
        .sig_rxd_rd_data(sig_rxd_rd_data),
        .valid_fwft(valid_fwft));
  axi_fifo_mm_s_0_fifo_generator_v13_0_5_rd_fwft_16 \gr1.rfwft 
       (.E(E),
        .Q(Q),
        .axis_rd_en__0(axis_rd_en__0),
        .empty_fwft_i(empty_fwft_i),
        .\gaxi_full_sm.r_last_r_reg (\gaxi_full_sm.r_last_r_reg ),
        .\gaxi_full_sm.r_valid_r_reg (\gaxi_full_sm.r_valid_r_reg ),
        .\gc0.count_reg[0] (p_6_out),
        .\gc0.count_reg[0]_0 (\gc0.count_reg[0] ),
        .\gc0.count_reg[0]_1 (\gc0.count_reg[0]_0 ),
        .p_2_out(p_2_out),
        .ram_rd_en_i(ram_rd_en_i),
        .rd_en_int_sync(rd_en_int_sync),
        .rst_int_sync(rst_int_sync),
        .rst_int_sync_1(rst_int_sync_1),
        .s_axi4_araddr(s_axi4_araddr),
        .\s_axi4_araddr[21] (\s_axi4_araddr[21] ),
        .\s_axi4_araddr[29] (\s_axi4_araddr[29] ),
        .s_axi_aclk(s_axi_aclk),
        .sig_axi_rd_en(sig_axi_rd_en),
        .valid_fwft(valid_fwft));
  axi_fifo_mm_s_0_fifo_generator_v13_0_5_rd_pe_ss_17 \grss.gpe.rdpe 
       (.E(p_6_out),
        .Q(Q[1]),
        .S(S),
        .\gcc0.gc0.count_d1_reg[7] (\gcc0.gc0.count_d1_reg[8]_0 [7:0]),
        .\gcc0.gc0.count_d1_reg[7]_0 (\gcc0.gc0.count_d1_reg[7] ),
        .\gcc0.gc0.count_d1_reg[8] (\gcc0.gc0.count_d1_reg[8]_1 ),
        .p_16_out(p_16_out),
        .p_9_out(p_9_out),
        .rd_pntr_inv_pad(rd_pntr_inv_pad),
        .s_axi_aclk(s_axi_aclk),
        .sig_rxd_pe_event__1(sig_rxd_pe_event__1),
        .sig_rxd_prog_empty_d1(sig_rxd_prog_empty_d1));
  axi_fifo_mm_s_0_fifo_generator_v13_0_5_rd_status_flags_ss_18 \grss.rsts 
       (.E(p_6_out),
        .Q(Q[1]),
        .\gc0.count_d1_reg[0] (rpntr_n_24),
        .\gc0.count_d1_reg[2] (rpntr_n_25),
        .\gc0.count_d1_reg[4] (rpntr_n_26),
        .\gc0.count_d1_reg[6] (rpntr_n_27),
        .\gcc0.gc0.count_d1_reg[8] (\gcc0.gc0.count_d1_reg[8] ),
        .p_16_out(p_16_out),
        .p_2_out(p_2_out),
        .s_axi_aclk(s_axi_aclk),
        .v1_reg(\c2/v1_reg ));
  axi_fifo_mm_s_0_fifo_generator_v13_0_5_rd_bin_cntr_19 rpntr
       (.\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram (\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram ),
        .E(p_6_out),
        .Q(Q[1]),
        .\gcc0.gc0.count_d1_reg[8] (\gcc0.gc0.count_d1_reg[8]_0 ),
        .\gcc0.gc0.count_reg[8] (\gcc0.gc0.count_reg[8] ),
        .\gdiff.gcry_1_sym.diff_pntr_pad_reg[9] (\gdiff.gcry_1_sym.diff_pntr_pad_reg[9] ),
        .ram_empty_fb_i_reg(rpntr_n_24),
        .ram_empty_fb_i_reg_0(rpntr_n_25),
        .ram_empty_fb_i_reg_1(rpntr_n_26),
        .ram_empty_fb_i_reg_2(rpntr_n_27),
        .s_axi_aclk(s_axi_aclk),
        .v1_reg(v1_reg),
        .v1_reg_0(v1_reg_0),
        .v1_reg_1(\c2/v1_reg ));
endmodule

(* ORIG_REF_NAME = "fifo_generator_v13_0_5_rd_pe_ss" *) 
module axi_fifo_mm_s_0_fifo_generator_v13_0_5_rd_pe_ss
   (sig_txd_prog_empty,
    sig_txd_pe_event__1,
    p_16_out,
    s_axi_aclk,
    Q,
    E,
    rd_pntr_inv_pad,
    \gcc0.gc1.gsym.count_d2_reg[7] ,
    S,
    \gcc0.gc1.gsym.count_d2_reg[7]_0 ,
    \gcc0.gc1.gsym.count_d2_reg[8] ,
    sig_txd_prog_empty_d1);
  output sig_txd_prog_empty;
  output sig_txd_pe_event__1;
  input p_16_out;
  input s_axi_aclk;
  input [0:0]Q;
  input [0:0]E;
  input [0:0]rd_pntr_inv_pad;
  input [7:0]\gcc0.gc1.gsym.count_d2_reg[7] ;
  input [3:0]S;
  input [3:0]\gcc0.gc1.gsym.count_d2_reg[7]_0 ;
  input [0:0]\gcc0.gc1.gsym.count_d2_reg[8] ;
  input sig_txd_prog_empty_d1;

  wire [0:0]E;
  wire [0:0]Q;
  wire [3:0]S;
  wire [9:1]diff_pntr_pad;
  wire [7:0]\gcc0.gc1.gsym.count_d2_reg[7] ;
  wire [3:0]\gcc0.gc1.gsym.count_d2_reg[7]_0 ;
  wire [0:0]\gcc0.gc1.gsym.count_d2_reg[8] ;
  wire \gpes.prog_empty_i_i_1_n_0 ;
  wire \gpes.prog_empty_i_i_2_n_0 ;
  wire \gpes.prog_empty_i_i_3_n_0 ;
  wire p_16_out;
  wire plusOp_carry__0_n_0;
  wire plusOp_carry__0_n_1;
  wire plusOp_carry__0_n_2;
  wire plusOp_carry__0_n_3;
  wire plusOp_carry__0_n_4;
  wire plusOp_carry__0_n_5;
  wire plusOp_carry__0_n_6;
  wire plusOp_carry__0_n_7;
  wire plusOp_carry__1_n_7;
  wire plusOp_carry_n_0;
  wire plusOp_carry_n_1;
  wire plusOp_carry_n_2;
  wire plusOp_carry_n_3;
  wire plusOp_carry_n_4;
  wire plusOp_carry_n_5;
  wire plusOp_carry_n_6;
  wire plusOp_carry_n_7;
  wire ram_rd_en_i;
  wire ram_wr_en_i;
  wire [0:0]rd_pntr_inv_pad;
  wire s_axi_aclk;
  wire sig_txd_pe_event__1;
  wire sig_txd_prog_empty;
  wire sig_txd_prog_empty_d1;
  wire [3:0]NLW_plusOp_carry__1_CO_UNCONNECTED;
  wire [3:1]NLW_plusOp_carry__1_O_UNCONNECTED;

  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT4 #(
    .INIT(16'hBF08)) 
    \gpes.prog_empty_i_i_1 
       (.I0(ram_rd_en_i),
        .I1(\gpes.prog_empty_i_i_2_n_0 ),
        .I2(ram_wr_en_i),
        .I3(sig_txd_prog_empty),
        .O(\gpes.prog_empty_i_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h0004)) 
    \gpes.prog_empty_i_i_2 
       (.I0(diff_pntr_pad[8]),
        .I1(\gpes.prog_empty_i_i_3_n_0 ),
        .I2(diff_pntr_pad[7]),
        .I3(diff_pntr_pad[9]),
        .O(\gpes.prog_empty_i_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    \gpes.prog_empty_i_i_3 
       (.I0(diff_pntr_pad[4]),
        .I1(diff_pntr_pad[3]),
        .I2(diff_pntr_pad[2]),
        .I3(diff_pntr_pad[1]),
        .I4(diff_pntr_pad[5]),
        .I5(diff_pntr_pad[6]),
        .O(\gpes.prog_empty_i_i_3_n_0 ));
  FDPE #(
    .INIT(1'b1)) 
    \gpes.prog_empty_i_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(\gpes.prog_empty_i_i_1_n_0 ),
        .PRE(Q),
        .Q(sig_txd_prog_empty));
  FDCE #(
    .INIT(1'b0)) 
    \greg.gpcry_sym.diff_pntr_pad_reg[1] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .CLR(Q),
        .D(plusOp_carry_n_7),
        .Q(diff_pntr_pad[1]));
  FDCE #(
    .INIT(1'b0)) 
    \greg.gpcry_sym.diff_pntr_pad_reg[2] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .CLR(Q),
        .D(plusOp_carry_n_6),
        .Q(diff_pntr_pad[2]));
  FDCE #(
    .INIT(1'b0)) 
    \greg.gpcry_sym.diff_pntr_pad_reg[3] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .CLR(Q),
        .D(plusOp_carry_n_5),
        .Q(diff_pntr_pad[3]));
  FDCE #(
    .INIT(1'b0)) 
    \greg.gpcry_sym.diff_pntr_pad_reg[4] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .CLR(Q),
        .D(plusOp_carry_n_4),
        .Q(diff_pntr_pad[4]));
  FDCE #(
    .INIT(1'b0)) 
    \greg.gpcry_sym.diff_pntr_pad_reg[5] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .CLR(Q),
        .D(plusOp_carry__0_n_7),
        .Q(diff_pntr_pad[5]));
  FDCE #(
    .INIT(1'b0)) 
    \greg.gpcry_sym.diff_pntr_pad_reg[6] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .CLR(Q),
        .D(plusOp_carry__0_n_6),
        .Q(diff_pntr_pad[6]));
  FDCE #(
    .INIT(1'b0)) 
    \greg.gpcry_sym.diff_pntr_pad_reg[7] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .CLR(Q),
        .D(plusOp_carry__0_n_5),
        .Q(diff_pntr_pad[7]));
  FDCE #(
    .INIT(1'b0)) 
    \greg.gpcry_sym.diff_pntr_pad_reg[8] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .CLR(Q),
        .D(plusOp_carry__0_n_4),
        .Q(diff_pntr_pad[8]));
  FDCE #(
    .INIT(1'b0)) 
    \greg.gpcry_sym.diff_pntr_pad_reg[9] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .CLR(Q),
        .D(plusOp_carry__1_n_7),
        .Q(diff_pntr_pad[9]));
  FDCE #(
    .INIT(1'b0)) 
    \greg.ram_rd_en_i_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .CLR(Q),
        .D(E),
        .Q(ram_rd_en_i));
  FDCE #(
    .INIT(1'b0)) 
    \greg.ram_wr_en_i_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .CLR(Q),
        .D(p_16_out),
        .Q(ram_wr_en_i));
  CARRY4 plusOp_carry
       (.CI(1'b0),
        .CO({plusOp_carry_n_0,plusOp_carry_n_1,plusOp_carry_n_2,plusOp_carry_n_3}),
        .CYINIT(rd_pntr_inv_pad),
        .DI(\gcc0.gc1.gsym.count_d2_reg[7] [3:0]),
        .O({plusOp_carry_n_4,plusOp_carry_n_5,plusOp_carry_n_6,plusOp_carry_n_7}),
        .S(S));
  CARRY4 plusOp_carry__0
       (.CI(plusOp_carry_n_0),
        .CO({plusOp_carry__0_n_0,plusOp_carry__0_n_1,plusOp_carry__0_n_2,plusOp_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI(\gcc0.gc1.gsym.count_d2_reg[7] [7:4]),
        .O({plusOp_carry__0_n_4,plusOp_carry__0_n_5,plusOp_carry__0_n_6,plusOp_carry__0_n_7}),
        .S(\gcc0.gc1.gsym.count_d2_reg[7]_0 ));
  CARRY4 plusOp_carry__1
       (.CI(plusOp_carry__0_n_0),
        .CO(NLW_plusOp_carry__1_CO_UNCONNECTED[3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({NLW_plusOp_carry__1_O_UNCONNECTED[3:1],plusOp_carry__1_n_7}),
        .S({1'b0,1'b0,1'b0,\gcc0.gc1.gsym.count_d2_reg[8] }));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \sig_register_array[0][10]_i_2 
       (.I0(sig_txd_prog_empty),
        .I1(sig_txd_prog_empty_d1),
        .O(sig_txd_pe_event__1));
endmodule

(* ORIG_REF_NAME = "fifo_generator_v13_0_5_rd_pe_ss" *) 
module axi_fifo_mm_s_0_fifo_generator_v13_0_5_rd_pe_ss_17
   (p_9_out,
    sig_rxd_pe_event__1,
    p_16_out,
    s_axi_aclk,
    Q,
    E,
    rd_pntr_inv_pad,
    \gcc0.gc0.count_d1_reg[7] ,
    S,
    \gcc0.gc0.count_d1_reg[7]_0 ,
    \gcc0.gc0.count_d1_reg[8] ,
    sig_rxd_prog_empty_d1);
  output p_9_out;
  output sig_rxd_pe_event__1;
  input p_16_out;
  input s_axi_aclk;
  input [0:0]Q;
  input [0:0]E;
  input [0:0]rd_pntr_inv_pad;
  input [7:0]\gcc0.gc0.count_d1_reg[7] ;
  input [3:0]S;
  input [3:0]\gcc0.gc0.count_d1_reg[7]_0 ;
  input [0:0]\gcc0.gc0.count_d1_reg[8] ;
  input sig_rxd_prog_empty_d1;

  wire [0:0]E;
  wire [0:0]Q;
  wire [3:0]S;
  wire [7:0]\gcc0.gc0.count_d1_reg[7] ;
  wire [3:0]\gcc0.gc0.count_d1_reg[7]_0 ;
  wire [0:0]\gcc0.gc0.count_d1_reg[8] ;
  wire \gpes.prog_empty_i_i_1__0_n_0 ;
  wire \gpes.prog_empty_i_i_2__0_n_0 ;
  wire \gpes.prog_empty_i_i_3__0_n_0 ;
  wire \greg.gpcry_sym.diff_pntr_pad_reg_n_0_[1] ;
  wire \greg.gpcry_sym.diff_pntr_pad_reg_n_0_[2] ;
  wire \greg.gpcry_sym.diff_pntr_pad_reg_n_0_[3] ;
  wire \greg.gpcry_sym.diff_pntr_pad_reg_n_0_[4] ;
  wire \greg.gpcry_sym.diff_pntr_pad_reg_n_0_[5] ;
  wire \greg.gpcry_sym.diff_pntr_pad_reg_n_0_[6] ;
  wire \greg.gpcry_sym.diff_pntr_pad_reg_n_0_[7] ;
  wire \greg.gpcry_sym.diff_pntr_pad_reg_n_0_[8] ;
  wire \greg.gpcry_sym.diff_pntr_pad_reg_n_0_[9] ;
  wire p_16_out;
  wire p_9_out;
  wire plusOp_carry__0_n_0;
  wire plusOp_carry__0_n_1;
  wire plusOp_carry__0_n_2;
  wire plusOp_carry__0_n_3;
  wire plusOp_carry__0_n_4;
  wire plusOp_carry__0_n_5;
  wire plusOp_carry__0_n_6;
  wire plusOp_carry__0_n_7;
  wire plusOp_carry__1_n_7;
  wire plusOp_carry_n_0;
  wire plusOp_carry_n_1;
  wire plusOp_carry_n_2;
  wire plusOp_carry_n_3;
  wire plusOp_carry_n_4;
  wire plusOp_carry_n_5;
  wire plusOp_carry_n_6;
  wire plusOp_carry_n_7;
  wire ram_rd_en_i;
  wire ram_wr_en_i;
  wire [0:0]rd_pntr_inv_pad;
  wire s_axi_aclk;
  wire sig_rxd_pe_event__1;
  wire sig_rxd_prog_empty_d1;
  wire [3:0]NLW_plusOp_carry__1_CO_UNCONNECTED;
  wire [3:1]NLW_plusOp_carry__1_O_UNCONNECTED;

  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT4 #(
    .INIT(16'hBF08)) 
    \gpes.prog_empty_i_i_1__0 
       (.I0(ram_rd_en_i),
        .I1(\gpes.prog_empty_i_i_2__0_n_0 ),
        .I2(ram_wr_en_i),
        .I3(p_9_out),
        .O(\gpes.prog_empty_i_i_1__0_n_0 ));
  LUT4 #(
    .INIT(16'h0004)) 
    \gpes.prog_empty_i_i_2__0 
       (.I0(\greg.gpcry_sym.diff_pntr_pad_reg_n_0_[8] ),
        .I1(\gpes.prog_empty_i_i_3__0_n_0 ),
        .I2(\greg.gpcry_sym.diff_pntr_pad_reg_n_0_[7] ),
        .I3(\greg.gpcry_sym.diff_pntr_pad_reg_n_0_[9] ),
        .O(\gpes.prog_empty_i_i_2__0_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    \gpes.prog_empty_i_i_3__0 
       (.I0(\greg.gpcry_sym.diff_pntr_pad_reg_n_0_[4] ),
        .I1(\greg.gpcry_sym.diff_pntr_pad_reg_n_0_[3] ),
        .I2(\greg.gpcry_sym.diff_pntr_pad_reg_n_0_[2] ),
        .I3(\greg.gpcry_sym.diff_pntr_pad_reg_n_0_[1] ),
        .I4(\greg.gpcry_sym.diff_pntr_pad_reg_n_0_[5] ),
        .I5(\greg.gpcry_sym.diff_pntr_pad_reg_n_0_[6] ),
        .O(\gpes.prog_empty_i_i_3__0_n_0 ));
  FDPE #(
    .INIT(1'b1)) 
    \gpes.prog_empty_i_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(\gpes.prog_empty_i_i_1__0_n_0 ),
        .PRE(Q),
        .Q(p_9_out));
  FDCE #(
    .INIT(1'b0)) 
    \greg.gpcry_sym.diff_pntr_pad_reg[1] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .CLR(Q),
        .D(plusOp_carry_n_7),
        .Q(\greg.gpcry_sym.diff_pntr_pad_reg_n_0_[1] ));
  FDCE #(
    .INIT(1'b0)) 
    \greg.gpcry_sym.diff_pntr_pad_reg[2] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .CLR(Q),
        .D(plusOp_carry_n_6),
        .Q(\greg.gpcry_sym.diff_pntr_pad_reg_n_0_[2] ));
  FDCE #(
    .INIT(1'b0)) 
    \greg.gpcry_sym.diff_pntr_pad_reg[3] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .CLR(Q),
        .D(plusOp_carry_n_5),
        .Q(\greg.gpcry_sym.diff_pntr_pad_reg_n_0_[3] ));
  FDCE #(
    .INIT(1'b0)) 
    \greg.gpcry_sym.diff_pntr_pad_reg[4] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .CLR(Q),
        .D(plusOp_carry_n_4),
        .Q(\greg.gpcry_sym.diff_pntr_pad_reg_n_0_[4] ));
  FDCE #(
    .INIT(1'b0)) 
    \greg.gpcry_sym.diff_pntr_pad_reg[5] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .CLR(Q),
        .D(plusOp_carry__0_n_7),
        .Q(\greg.gpcry_sym.diff_pntr_pad_reg_n_0_[5] ));
  FDCE #(
    .INIT(1'b0)) 
    \greg.gpcry_sym.diff_pntr_pad_reg[6] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .CLR(Q),
        .D(plusOp_carry__0_n_6),
        .Q(\greg.gpcry_sym.diff_pntr_pad_reg_n_0_[6] ));
  FDCE #(
    .INIT(1'b0)) 
    \greg.gpcry_sym.diff_pntr_pad_reg[7] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .CLR(Q),
        .D(plusOp_carry__0_n_5),
        .Q(\greg.gpcry_sym.diff_pntr_pad_reg_n_0_[7] ));
  FDCE #(
    .INIT(1'b0)) 
    \greg.gpcry_sym.diff_pntr_pad_reg[8] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .CLR(Q),
        .D(plusOp_carry__0_n_4),
        .Q(\greg.gpcry_sym.diff_pntr_pad_reg_n_0_[8] ));
  FDCE #(
    .INIT(1'b0)) 
    \greg.gpcry_sym.diff_pntr_pad_reg[9] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .CLR(Q),
        .D(plusOp_carry__1_n_7),
        .Q(\greg.gpcry_sym.diff_pntr_pad_reg_n_0_[9] ));
  FDCE #(
    .INIT(1'b0)) 
    \greg.ram_rd_en_i_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .CLR(Q),
        .D(E),
        .Q(ram_rd_en_i));
  FDCE #(
    .INIT(1'b0)) 
    \greg.ram_wr_en_i_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .CLR(Q),
        .D(p_16_out),
        .Q(ram_wr_en_i));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 plusOp_carry
       (.CI(1'b0),
        .CO({plusOp_carry_n_0,plusOp_carry_n_1,plusOp_carry_n_2,plusOp_carry_n_3}),
        .CYINIT(rd_pntr_inv_pad),
        .DI(\gcc0.gc0.count_d1_reg[7] [3:0]),
        .O({plusOp_carry_n_4,plusOp_carry_n_5,plusOp_carry_n_6,plusOp_carry_n_7}),
        .S(S));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 plusOp_carry__0
       (.CI(plusOp_carry_n_0),
        .CO({plusOp_carry__0_n_0,plusOp_carry__0_n_1,plusOp_carry__0_n_2,plusOp_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI(\gcc0.gc0.count_d1_reg[7] [7:4]),
        .O({plusOp_carry__0_n_4,plusOp_carry__0_n_5,plusOp_carry__0_n_6,plusOp_carry__0_n_7}),
        .S(\gcc0.gc0.count_d1_reg[7]_0 ));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 plusOp_carry__1
       (.CI(plusOp_carry__0_n_0),
        .CO(NLW_plusOp_carry__1_CO_UNCONNECTED[3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({NLW_plusOp_carry__1_O_UNCONNECTED[3:1],plusOp_carry__1_n_7}),
        .S({1'b0,1'b0,1'b0,\gcc0.gc0.count_d1_reg[8] }));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \sig_register_array[0][12]_i_2 
       (.I0(p_9_out),
        .I1(sig_rxd_prog_empty_d1),
        .O(sig_rxd_pe_event__1));
endmodule

(* ORIG_REF_NAME = "fifo_generator_v13_0_5_rd_status_flags_ss" *) 
module axi_fifo_mm_s_0_fifo_generator_v13_0_5_rd_status_flags_ss
   (p_2_out,
    \gc0.count_d1_reg[0] ,
    \gc0.count_d1_reg[2] ,
    \gc0.count_d1_reg[4] ,
    \gc0.count_d1_reg[6] ,
    \gcc0.gc1.gsym.count_d2_reg[8] ,
    v1_reg,
    s_axi_aclk,
    Q,
    p_16_out,
    E);
  output p_2_out;
  input \gc0.count_d1_reg[0] ;
  input \gc0.count_d1_reg[2] ;
  input \gc0.count_d1_reg[4] ;
  input \gc0.count_d1_reg[6] ;
  input \gcc0.gc1.gsym.count_d2_reg[8] ;
  input [4:0]v1_reg;
  input s_axi_aclk;
  input [0:0]Q;
  input p_16_out;
  input [0:0]E;

  wire [0:0]E;
  wire [0:0]Q;
  wire c1_n_0;
  wire comp1;
  wire \gc0.count_d1_reg[0] ;
  wire \gc0.count_d1_reg[2] ;
  wire \gc0.count_d1_reg[4] ;
  wire \gc0.count_d1_reg[6] ;
  wire \gcc0.gc1.gsym.count_d2_reg[8] ;
  wire p_16_out;
  wire p_2_out;
  wire s_axi_aclk;
  wire [4:0]v1_reg;

  axi_fifo_mm_s_0_fifo_generator_v13_0_5_compare_2 c1
       (.E(E),
        .comp1(comp1),
        .\gc0.count_d1_reg[0] (\gc0.count_d1_reg[0] ),
        .\gc0.count_d1_reg[2] (\gc0.count_d1_reg[2] ),
        .\gc0.count_d1_reg[4] (\gc0.count_d1_reg[4] ),
        .\gc0.count_d1_reg[6] (\gc0.count_d1_reg[6] ),
        .\gcc0.gc1.gsym.count_d2_reg[8] (\gcc0.gc1.gsym.count_d2_reg[8] ),
        .p_16_out(p_16_out),
        .p_2_out(p_2_out),
        .ram_empty_fb_i_reg(c1_n_0));
  axi_fifo_mm_s_0_fifo_generator_v13_0_5_compare_3 c2
       (.comp1(comp1),
        .v1_reg(v1_reg));
  (* equivalent_register_removal = "no" *) 
  FDPE #(
    .INIT(1'b1)) 
    ram_empty_fb_i_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(c1_n_0),
        .PRE(Q),
        .Q(p_2_out));
endmodule

(* ORIG_REF_NAME = "fifo_generator_v13_0_5_rd_status_flags_ss" *) 
module axi_fifo_mm_s_0_fifo_generator_v13_0_5_rd_status_flags_ss_18
   (p_2_out,
    \gc0.count_d1_reg[0] ,
    \gc0.count_d1_reg[2] ,
    \gc0.count_d1_reg[4] ,
    \gc0.count_d1_reg[6] ,
    \gcc0.gc0.count_d1_reg[8] ,
    v1_reg,
    s_axi_aclk,
    Q,
    E,
    p_16_out);
  output p_2_out;
  input \gc0.count_d1_reg[0] ;
  input \gc0.count_d1_reg[2] ;
  input \gc0.count_d1_reg[4] ;
  input \gc0.count_d1_reg[6] ;
  input \gcc0.gc0.count_d1_reg[8] ;
  input [4:0]v1_reg;
  input s_axi_aclk;
  input [0:0]Q;
  input [0:0]E;
  input p_16_out;

  wire [0:0]E;
  wire [0:0]Q;
  wire c1_n_0;
  wire comp1;
  wire \gc0.count_d1_reg[0] ;
  wire \gc0.count_d1_reg[2] ;
  wire \gc0.count_d1_reg[4] ;
  wire \gc0.count_d1_reg[6] ;
  wire \gcc0.gc0.count_d1_reg[8] ;
  wire p_16_out;
  wire p_2_out;
  wire s_axi_aclk;
  wire [4:0]v1_reg;

  axi_fifo_mm_s_0_fifo_generator_v13_0_5_compare_20 c1
       (.E(E),
        .comp1(comp1),
        .\gc0.count_d1_reg[0] (\gc0.count_d1_reg[0] ),
        .\gc0.count_d1_reg[2] (\gc0.count_d1_reg[2] ),
        .\gc0.count_d1_reg[4] (\gc0.count_d1_reg[4] ),
        .\gc0.count_d1_reg[6] (\gc0.count_d1_reg[6] ),
        .\gcc0.gc0.count_d1_reg[8] (\gcc0.gc0.count_d1_reg[8] ),
        .p_16_out(p_16_out),
        .p_2_out(p_2_out),
        .ram_empty_fb_i_reg(c1_n_0));
  axi_fifo_mm_s_0_fifo_generator_v13_0_5_compare_21 c2
       (.comp1(comp1),
        .v1_reg(v1_reg));
  (* equivalent_register_removal = "no" *) 
  FDPE #(
    .INIT(1'b1)) 
    ram_empty_fb_i_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(c1_n_0),
        .PRE(Q),
        .Q(p_2_out));
endmodule

(* ORIG_REF_NAME = "fifo_generator_v13_0_5_reset_blk_ramfifo" *) 
module axi_fifo_mm_s_0_fifo_generator_v13_0_5_reset_blk_ramfifo
   (AR,
    s_axi_aclk,
    inverted_reset);
  output [0:0]AR;
  input s_axi_aclk;
  input inverted_reset;

  wire [0:0]AR;
  wire inverted_reset;
  wire \ngwrdrst.grst.g7serrst.wr_rst_asreg_i_1_n_0 ;
  wire \ngwrdrst.grst.g7serrst.wr_rst_reg[0]_i_1_n_0 ;
  (* async_reg = "true" *) (* msgon = "true" *) wire rst_d1;
  (* async_reg = "true" *) (* msgon = "true" *) wire rst_d2;
  (* async_reg = "true" *) (* msgon = "true" *) wire rst_d3;
  (* async_reg = "true" *) (* msgon = "true" *) wire rst_rd_reg1;
  (* async_reg = "true" *) (* msgon = "true" *) wire rst_rd_reg2;
  (* async_reg = "true" *) (* msgon = "true" *) wire rst_wr_reg1;
  (* async_reg = "true" *) (* msgon = "true" *) wire rst_wr_reg2;
  wire s_axi_aclk;
  wire wr_rst_asreg;
  wire wr_rst_asreg_d1;
  wire wr_rst_asreg_d2;

  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* msgon = "true" *) 
  FDPE #(
    .INIT(1'b1)) 
    \grstd1.grst_full.grst_f.rst_d1_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(1'b0),
        .PRE(inverted_reset),
        .Q(rst_d1));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* msgon = "true" *) 
  FDPE #(
    .INIT(1'b1)) 
    \grstd1.grst_full.grst_f.rst_d2_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(rst_d1),
        .PRE(inverted_reset),
        .Q(rst_d2));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* msgon = "true" *) 
  FDPE #(
    .INIT(1'b1)) 
    \grstd1.grst_full.grst_f.rst_d3_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(rst_d2),
        .PRE(inverted_reset),
        .Q(rst_d3));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* msgon = "true" *) 
  FDPE #(
    .INIT(1'b0)) 
    \ngwrdrst.grst.g7serrst.rst_rd_reg1_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(1'b0),
        .PRE(inverted_reset),
        .Q(rst_rd_reg1));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* msgon = "true" *) 
  FDPE #(
    .INIT(1'b0)) 
    \ngwrdrst.grst.g7serrst.rst_rd_reg2_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(rst_rd_reg1),
        .PRE(inverted_reset),
        .Q(rst_rd_reg2));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* msgon = "true" *) 
  FDPE #(
    .INIT(1'b0)) 
    \ngwrdrst.grst.g7serrst.rst_wr_reg1_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(1'b0),
        .PRE(inverted_reset),
        .Q(rst_wr_reg1));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* msgon = "true" *) 
  FDPE #(
    .INIT(1'b0)) 
    \ngwrdrst.grst.g7serrst.rst_wr_reg2_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(rst_wr_reg1),
        .PRE(inverted_reset),
        .Q(rst_wr_reg2));
  FDRE #(
    .INIT(1'b0)) 
    \ngwrdrst.grst.g7serrst.wr_rst_asreg_d1_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(wr_rst_asreg),
        .Q(wr_rst_asreg_d1),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \ngwrdrst.grst.g7serrst.wr_rst_asreg_d2_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(wr_rst_asreg_d1),
        .Q(wr_rst_asreg_d2),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h2)) 
    \ngwrdrst.grst.g7serrst.wr_rst_asreg_i_1 
       (.I0(wr_rst_asreg),
        .I1(wr_rst_asreg_d1),
        .O(\ngwrdrst.grst.g7serrst.wr_rst_asreg_i_1_n_0 ));
  FDPE #(
    .INIT(1'b1)) 
    \ngwrdrst.grst.g7serrst.wr_rst_asreg_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(\ngwrdrst.grst.g7serrst.wr_rst_asreg_i_1_n_0 ),
        .PRE(rst_wr_reg2),
        .Q(wr_rst_asreg));
  LUT2 #(
    .INIT(4'h2)) 
    \ngwrdrst.grst.g7serrst.wr_rst_reg[0]_i_1 
       (.I0(wr_rst_asreg),
        .I1(wr_rst_asreg_d2),
        .O(\ngwrdrst.grst.g7serrst.wr_rst_reg[0]_i_1_n_0 ));
  (* equivalent_register_removal = "no" *) 
  FDPE #(
    .INIT(1'b1)) 
    \ngwrdrst.grst.g7serrst.wr_rst_reg_reg[0] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(1'b0),
        .PRE(\ngwrdrst.grst.g7serrst.wr_rst_reg[0]_i_1_n_0 ),
        .Q(AR));
endmodule

(* ORIG_REF_NAME = "fifo_generator_v13_0_5_reset_blk_ramfifo" *) 
module axi_fifo_mm_s_0_fifo_generator_v13_0_5_reset_blk_ramfifo_4
   (s_axi_aclk,
    sync_areset_n_reg_inv);
  input s_axi_aclk;
  input sync_areset_n_reg_inv;

  (* async_reg = "true" *) (* msgon = "true" *) wire rst_d1;
  (* async_reg = "true" *) (* msgon = "true" *) wire rst_d2;
  (* async_reg = "true" *) (* msgon = "true" *) wire rst_d3;
  (* async_reg = "true" *) (* msgon = "true" *) wire rst_rd_reg1;
  (* async_reg = "true" *) (* msgon = "true" *) wire rst_rd_reg2;
  (* async_reg = "true" *) (* msgon = "true" *) wire rst_wr_reg1;
  (* async_reg = "true" *) (* msgon = "true" *) wire rst_wr_reg2;
  wire s_axi_aclk;
  wire sync_areset_n_reg_inv;

  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* msgon = "true" *) 
  FDPE #(
    .INIT(1'b1)) 
    \grstd1.grst_full.grst_f.rst_d1_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(1'b0),
        .PRE(sync_areset_n_reg_inv),
        .Q(rst_d1));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* msgon = "true" *) 
  FDPE #(
    .INIT(1'b1)) 
    \grstd1.grst_full.grst_f.rst_d2_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(rst_d1),
        .PRE(sync_areset_n_reg_inv),
        .Q(rst_d2));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* msgon = "true" *) 
  FDPE #(
    .INIT(1'b1)) 
    \grstd1.grst_full.grst_f.rst_d3_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(rst_d2),
        .PRE(sync_areset_n_reg_inv),
        .Q(rst_d3));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* msgon = "true" *) 
  FDPE #(
    .INIT(1'b0)) 
    \ngwrdrst.grst.g7serrst.rst_rd_reg1_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(1'b0),
        .PRE(sync_areset_n_reg_inv),
        .Q(rst_rd_reg1));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* msgon = "true" *) 
  FDPE #(
    .INIT(1'b0)) 
    \ngwrdrst.grst.g7serrst.rst_rd_reg2_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(rst_rd_reg1),
        .PRE(sync_areset_n_reg_inv),
        .Q(rst_rd_reg2));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* msgon = "true" *) 
  FDPE #(
    .INIT(1'b0)) 
    \ngwrdrst.grst.g7serrst.rst_wr_reg1_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(1'b0),
        .PRE(sync_areset_n_reg_inv),
        .Q(rst_wr_reg1));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* msgon = "true" *) 
  FDPE #(
    .INIT(1'b0)) 
    \ngwrdrst.grst.g7serrst.rst_wr_reg2_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(rst_wr_reg1),
        .PRE(sync_areset_n_reg_inv),
        .Q(rst_wr_reg2));
endmodule

(* ORIG_REF_NAME = "fifo_generator_v13_0_5_reset_blk_ramfifo" *) 
module axi_fifo_mm_s_0_fifo_generator_v13_0_5_reset_blk_ramfifo__parameterized0
   (out,
    \gaf.gaf0.ram_afull_i_reg ,
    AR,
    WEBWE,
    Q,
    s_axi_aclk,
    inverted_reset,
    wr_en_int_sync,
    rst_int_sync,
    rst_int_sync_1,
    p_16_out);
  output out;
  output \gaf.gaf0.ram_afull_i_reg ;
  output [0:0]AR;
  output [0:0]WEBWE;
  output [1:0]Q;
  input s_axi_aclk;
  input inverted_reset;
  input wr_en_int_sync;
  input rst_int_sync;
  input rst_int_sync_1;
  input p_16_out;

  wire [0:0]AR;
  wire [1:0]Q;
  wire [0:0]WEBWE;
  wire inverted_reset;
  wire \ngwrdrst.grst.g7serrst.rd_rst_asreg_d1_reg_n_0 ;
  wire \ngwrdrst.grst.g7serrst.rd_rst_asreg_i_1_n_0 ;
  wire \ngwrdrst.grst.g7serrst.rd_rst_reg[2]_i_1_n_0 ;
  wire \ngwrdrst.grst.g7serrst.wr_rst_asreg_d1_reg_n_0 ;
  wire \ngwrdrst.grst.g7serrst.wr_rst_asreg_i_1__1_n_0 ;
  wire \ngwrdrst.grst.g7serrst.wr_rst_reg[1]_i_1_n_0 ;
  wire p_16_out;
  wire rd_rst_asreg;
  wire rd_rst_asreg_d2;
  (* async_reg = "true" *) (* msgon = "true" *) wire rst_d1;
  (* async_reg = "true" *) (* msgon = "true" *) wire rst_d2;
  (* async_reg = "true" *) (* msgon = "true" *) wire rst_d3;
  (* async_reg = "true" *) (* msgon = "true" *) wire rst_d4;
  (* async_reg = "true" *) (* msgon = "true" *) wire rst_d5;
  wire rst_int_sync;
  wire rst_int_sync_1;
  (* async_reg = "true" *) (* msgon = "true" *) wire rst_rd_reg1;
  (* async_reg = "true" *) (* msgon = "true" *) wire rst_rd_reg2;
  (* async_reg = "true" *) (* msgon = "true" *) wire rst_wr_reg1;
  (* async_reg = "true" *) (* msgon = "true" *) wire rst_wr_reg2;
  wire s_axi_aclk;
  wire wr_en_int_sync;
  wire wr_rst_asreg;
  wire wr_rst_asreg_d2;

  assign \gaf.gaf0.ram_afull_i_reg  = rst_d5;
  assign out = rst_d2;
  LUT5 #(
    .INIT(32'hAAABAAA8)) 
    \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_i_2 
       (.I0(wr_en_int_sync),
        .I1(rst_int_sync),
        .I2(Q[0]),
        .I3(rst_int_sync_1),
        .I4(p_16_out),
        .O(WEBWE));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* msgon = "true" *) 
  FDPE #(
    .INIT(1'b1)) 
    \grstd1.grst_full.grst_f.rst_d1_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(1'b0),
        .PRE(inverted_reset),
        .Q(rst_d1));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* msgon = "true" *) 
  FDPE #(
    .INIT(1'b1)) 
    \grstd1.grst_full.grst_f.rst_d2_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(rst_d1),
        .PRE(inverted_reset),
        .Q(rst_d2));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* msgon = "true" *) 
  FDPE #(
    .INIT(1'b1)) 
    \grstd1.grst_full.grst_f.rst_d3_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(rst_d2),
        .PRE(inverted_reset),
        .Q(rst_d3));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* msgon = "true" *) 
  FDPE #(
    .INIT(1'b1)) 
    \grstd1.grst_full.grst_f.rst_d4_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(rst_d3),
        .PRE(inverted_reset),
        .Q(rst_d4));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* msgon = "true" *) 
  FDPE #(
    .INIT(1'b1)) 
    \grstd1.grst_full.grst_f.rst_d5_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(rst_d4),
        .PRE(inverted_reset),
        .Q(rst_d5));
  FDRE #(
    .INIT(1'b0)) 
    \ngwrdrst.grst.g7serrst.rd_rst_asreg_d1_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(rd_rst_asreg),
        .Q(\ngwrdrst.grst.g7serrst.rd_rst_asreg_d1_reg_n_0 ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \ngwrdrst.grst.g7serrst.rd_rst_asreg_d2_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(\ngwrdrst.grst.g7serrst.rd_rst_asreg_d1_reg_n_0 ),
        .Q(rd_rst_asreg_d2),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h2)) 
    \ngwrdrst.grst.g7serrst.rd_rst_asreg_i_1 
       (.I0(rd_rst_asreg),
        .I1(\ngwrdrst.grst.g7serrst.rd_rst_asreg_d1_reg_n_0 ),
        .O(\ngwrdrst.grst.g7serrst.rd_rst_asreg_i_1_n_0 ));
  FDPE #(
    .INIT(1'b1)) 
    \ngwrdrst.grst.g7serrst.rd_rst_asreg_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(\ngwrdrst.grst.g7serrst.rd_rst_asreg_i_1_n_0 ),
        .PRE(rst_rd_reg2),
        .Q(rd_rst_asreg));
  LUT2 #(
    .INIT(4'h2)) 
    \ngwrdrst.grst.g7serrst.rd_rst_reg[2]_i_1 
       (.I0(rd_rst_asreg),
        .I1(rd_rst_asreg_d2),
        .O(\ngwrdrst.grst.g7serrst.rd_rst_reg[2]_i_1_n_0 ));
  (* equivalent_register_removal = "no" *) 
  FDPE #(
    .INIT(1'b1)) 
    \ngwrdrst.grst.g7serrst.rd_rst_reg_reg[0] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(1'b0),
        .PRE(\ngwrdrst.grst.g7serrst.rd_rst_reg[2]_i_1_n_0 ),
        .Q(Q[0]));
  (* equivalent_register_removal = "no" *) 
  FDPE #(
    .INIT(1'b1)) 
    \ngwrdrst.grst.g7serrst.rd_rst_reg_reg[2] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(1'b0),
        .PRE(\ngwrdrst.grst.g7serrst.rd_rst_reg[2]_i_1_n_0 ),
        .Q(Q[1]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* msgon = "true" *) 
  FDPE #(
    .INIT(1'b0)) 
    \ngwrdrst.grst.g7serrst.rst_rd_reg1_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(1'b0),
        .PRE(inverted_reset),
        .Q(rst_rd_reg1));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* msgon = "true" *) 
  FDPE #(
    .INIT(1'b0)) 
    \ngwrdrst.grst.g7serrst.rst_rd_reg2_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(rst_rd_reg1),
        .PRE(inverted_reset),
        .Q(rst_rd_reg2));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* msgon = "true" *) 
  FDPE #(
    .INIT(1'b0)) 
    \ngwrdrst.grst.g7serrst.rst_wr_reg1_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(1'b0),
        .PRE(inverted_reset),
        .Q(rst_wr_reg1));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* msgon = "true" *) 
  FDPE #(
    .INIT(1'b0)) 
    \ngwrdrst.grst.g7serrst.rst_wr_reg2_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(rst_wr_reg1),
        .PRE(inverted_reset),
        .Q(rst_wr_reg2));
  FDRE #(
    .INIT(1'b0)) 
    \ngwrdrst.grst.g7serrst.wr_rst_asreg_d1_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(wr_rst_asreg),
        .Q(\ngwrdrst.grst.g7serrst.wr_rst_asreg_d1_reg_n_0 ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \ngwrdrst.grst.g7serrst.wr_rst_asreg_d2_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(\ngwrdrst.grst.g7serrst.wr_rst_asreg_d1_reg_n_0 ),
        .Q(wr_rst_asreg_d2),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h2)) 
    \ngwrdrst.grst.g7serrst.wr_rst_asreg_i_1__1 
       (.I0(wr_rst_asreg),
        .I1(\ngwrdrst.grst.g7serrst.wr_rst_asreg_d1_reg_n_0 ),
        .O(\ngwrdrst.grst.g7serrst.wr_rst_asreg_i_1__1_n_0 ));
  FDPE #(
    .INIT(1'b1)) 
    \ngwrdrst.grst.g7serrst.wr_rst_asreg_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(\ngwrdrst.grst.g7serrst.wr_rst_asreg_i_1__1_n_0 ),
        .PRE(rst_wr_reg2),
        .Q(wr_rst_asreg));
  LUT2 #(
    .INIT(4'h2)) 
    \ngwrdrst.grst.g7serrst.wr_rst_reg[1]_i_1 
       (.I0(wr_rst_asreg),
        .I1(wr_rst_asreg_d2),
        .O(\ngwrdrst.grst.g7serrst.wr_rst_reg[1]_i_1_n_0 ));
  (* equivalent_register_removal = "no" *) 
  FDPE #(
    .INIT(1'b1)) 
    \ngwrdrst.grst.g7serrst.wr_rst_reg_reg[1] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(1'b0),
        .PRE(\ngwrdrst.grst.g7serrst.wr_rst_reg[1]_i_1_n_0 ),
        .Q(AR));
endmodule

(* ORIG_REF_NAME = "fifo_generator_v13_0_5_reset_blk_ramfifo" *) 
module axi_fifo_mm_s_0_fifo_generator_v13_0_5_reset_blk_ramfifo__parameterized1
   (out,
    ram_full_i_reg,
    AR,
    WEBWE,
    Q,
    s_axi_aclk,
    sync_areset_n_reg_inv,
    wr_en_int_sync,
    rst_int_sync,
    rst_int_sync_1,
    p_16_out);
  output out;
  output ram_full_i_reg;
  output [0:0]AR;
  output [0:0]WEBWE;
  output [1:0]Q;
  input s_axi_aclk;
  input sync_areset_n_reg_inv;
  input wr_en_int_sync;
  input rst_int_sync;
  input rst_int_sync_1;
  input p_16_out;

  wire [0:0]AR;
  wire [1:0]Q;
  wire [0:0]WEBWE;
  wire \ngwrdrst.grst.g7serrst.rd_rst_asreg_d1_reg_n_0 ;
  wire \ngwrdrst.grst.g7serrst.rd_rst_asreg_i_1__0_n_0 ;
  wire \ngwrdrst.grst.g7serrst.rd_rst_reg[2]_i_1__0_n_0 ;
  wire \ngwrdrst.grst.g7serrst.wr_rst_asreg_d1_reg_n_0 ;
  wire \ngwrdrst.grst.g7serrst.wr_rst_asreg_i_1__0_n_0 ;
  wire \ngwrdrst.grst.g7serrst.wr_rst_reg[1]_i_1__0_n_0 ;
  wire p_16_out;
  wire rd_rst_asreg;
  wire rd_rst_asreg_d2;
  (* async_reg = "true" *) (* msgon = "true" *) wire rst_d1;
  (* async_reg = "true" *) (* msgon = "true" *) wire rst_d2;
  (* async_reg = "true" *) (* msgon = "true" *) wire rst_d3;
  (* async_reg = "true" *) (* msgon = "true" *) wire rst_d4;
  (* async_reg = "true" *) (* msgon = "true" *) wire rst_d5;
  wire rst_int_sync;
  wire rst_int_sync_1;
  (* async_reg = "true" *) (* msgon = "true" *) wire rst_rd_reg1;
  (* async_reg = "true" *) (* msgon = "true" *) wire rst_rd_reg2;
  (* async_reg = "true" *) (* msgon = "true" *) wire rst_wr_reg1;
  (* async_reg = "true" *) (* msgon = "true" *) wire rst_wr_reg2;
  wire s_axi_aclk;
  wire sync_areset_n_reg_inv;
  wire wr_en_int_sync;
  wire wr_rst_asreg;
  wire wr_rst_asreg_d2;

  assign out = rst_d2;
  assign ram_full_i_reg = rst_d5;
  LUT5 #(
    .INIT(32'hAAABAAA8)) 
    \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram_i_2__0 
       (.I0(wr_en_int_sync),
        .I1(rst_int_sync),
        .I2(Q[0]),
        .I3(rst_int_sync_1),
        .I4(p_16_out),
        .O(WEBWE));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* msgon = "true" *) 
  FDPE #(
    .INIT(1'b1)) 
    \grstd1.grst_full.grst_f.rst_d1_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(1'b0),
        .PRE(sync_areset_n_reg_inv),
        .Q(rst_d1));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* msgon = "true" *) 
  FDPE #(
    .INIT(1'b1)) 
    \grstd1.grst_full.grst_f.rst_d2_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(rst_d1),
        .PRE(sync_areset_n_reg_inv),
        .Q(rst_d2));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* msgon = "true" *) 
  FDPE #(
    .INIT(1'b1)) 
    \grstd1.grst_full.grst_f.rst_d3_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(rst_d2),
        .PRE(sync_areset_n_reg_inv),
        .Q(rst_d3));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* msgon = "true" *) 
  FDPE #(
    .INIT(1'b1)) 
    \grstd1.grst_full.grst_f.rst_d4_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(rst_d3),
        .PRE(sync_areset_n_reg_inv),
        .Q(rst_d4));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* msgon = "true" *) 
  FDPE #(
    .INIT(1'b1)) 
    \grstd1.grst_full.grst_f.rst_d5_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(rst_d4),
        .PRE(sync_areset_n_reg_inv),
        .Q(rst_d5));
  FDRE #(
    .INIT(1'b0)) 
    \ngwrdrst.grst.g7serrst.rd_rst_asreg_d1_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(rd_rst_asreg),
        .Q(\ngwrdrst.grst.g7serrst.rd_rst_asreg_d1_reg_n_0 ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \ngwrdrst.grst.g7serrst.rd_rst_asreg_d2_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(\ngwrdrst.grst.g7serrst.rd_rst_asreg_d1_reg_n_0 ),
        .Q(rd_rst_asreg_d2),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h2)) 
    \ngwrdrst.grst.g7serrst.rd_rst_asreg_i_1__0 
       (.I0(rd_rst_asreg),
        .I1(\ngwrdrst.grst.g7serrst.rd_rst_asreg_d1_reg_n_0 ),
        .O(\ngwrdrst.grst.g7serrst.rd_rst_asreg_i_1__0_n_0 ));
  FDPE #(
    .INIT(1'b1)) 
    \ngwrdrst.grst.g7serrst.rd_rst_asreg_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(\ngwrdrst.grst.g7serrst.rd_rst_asreg_i_1__0_n_0 ),
        .PRE(rst_rd_reg2),
        .Q(rd_rst_asreg));
  LUT2 #(
    .INIT(4'h2)) 
    \ngwrdrst.grst.g7serrst.rd_rst_reg[2]_i_1__0 
       (.I0(rd_rst_asreg),
        .I1(rd_rst_asreg_d2),
        .O(\ngwrdrst.grst.g7serrst.rd_rst_reg[2]_i_1__0_n_0 ));
  (* equivalent_register_removal = "no" *) 
  FDPE #(
    .INIT(1'b1)) 
    \ngwrdrst.grst.g7serrst.rd_rst_reg_reg[0] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(1'b0),
        .PRE(\ngwrdrst.grst.g7serrst.rd_rst_reg[2]_i_1__0_n_0 ),
        .Q(Q[0]));
  (* equivalent_register_removal = "no" *) 
  FDPE #(
    .INIT(1'b1)) 
    \ngwrdrst.grst.g7serrst.rd_rst_reg_reg[2] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(1'b0),
        .PRE(\ngwrdrst.grst.g7serrst.rd_rst_reg[2]_i_1__0_n_0 ),
        .Q(Q[1]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* msgon = "true" *) 
  FDPE #(
    .INIT(1'b0)) 
    \ngwrdrst.grst.g7serrst.rst_rd_reg1_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(1'b0),
        .PRE(sync_areset_n_reg_inv),
        .Q(rst_rd_reg1));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* msgon = "true" *) 
  FDPE #(
    .INIT(1'b0)) 
    \ngwrdrst.grst.g7serrst.rst_rd_reg2_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(rst_rd_reg1),
        .PRE(sync_areset_n_reg_inv),
        .Q(rst_rd_reg2));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* msgon = "true" *) 
  FDPE #(
    .INIT(1'b0)) 
    \ngwrdrst.grst.g7serrst.rst_wr_reg1_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(1'b0),
        .PRE(sync_areset_n_reg_inv),
        .Q(rst_wr_reg1));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* msgon = "true" *) 
  FDPE #(
    .INIT(1'b0)) 
    \ngwrdrst.grst.g7serrst.rst_wr_reg2_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(rst_wr_reg1),
        .PRE(sync_areset_n_reg_inv),
        .Q(rst_wr_reg2));
  FDRE #(
    .INIT(1'b0)) 
    \ngwrdrst.grst.g7serrst.wr_rst_asreg_d1_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(wr_rst_asreg),
        .Q(\ngwrdrst.grst.g7serrst.wr_rst_asreg_d1_reg_n_0 ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \ngwrdrst.grst.g7serrst.wr_rst_asreg_d2_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(\ngwrdrst.grst.g7serrst.wr_rst_asreg_d1_reg_n_0 ),
        .Q(wr_rst_asreg_d2),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h2)) 
    \ngwrdrst.grst.g7serrst.wr_rst_asreg_i_1__0 
       (.I0(wr_rst_asreg),
        .I1(\ngwrdrst.grst.g7serrst.wr_rst_asreg_d1_reg_n_0 ),
        .O(\ngwrdrst.grst.g7serrst.wr_rst_asreg_i_1__0_n_0 ));
  FDPE #(
    .INIT(1'b1)) 
    \ngwrdrst.grst.g7serrst.wr_rst_asreg_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(\ngwrdrst.grst.g7serrst.wr_rst_asreg_i_1__0_n_0 ),
        .PRE(rst_wr_reg2),
        .Q(wr_rst_asreg));
  LUT2 #(
    .INIT(4'h2)) 
    \ngwrdrst.grst.g7serrst.wr_rst_reg[1]_i_1__0 
       (.I0(wr_rst_asreg),
        .I1(wr_rst_asreg_d2),
        .O(\ngwrdrst.grst.g7serrst.wr_rst_reg[1]_i_1__0_n_0 ));
  (* equivalent_register_removal = "no" *) 
  FDPE #(
    .INIT(1'b1)) 
    \ngwrdrst.grst.g7serrst.wr_rst_reg_reg[1] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(1'b0),
        .PRE(\ngwrdrst.grst.g7serrst.wr_rst_reg[1]_i_1__0_n_0 ),
        .Q(AR));
endmodule

(* ORIG_REF_NAME = "fifo_generator_v13_0_5_synth" *) 
module axi_fifo_mm_s_0_fifo_generator_v13_0_5_synth
   (\gaxi_full_sm.w_ready_r_reg ,
    sig_txd_prog_empty,
    p_7_out,
    \axi_str_txd_tdata[31] ,
    DI,
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[8]_0 ,
    \sig_register_array_reg[0][4] ,
    axi_str_txd_tvalid,
    \gaxi_bid_gen.S_AXI_BID_reg[0] ,
    S,
    \gfifo_gen.gmm2s.vacancy_i_reg[9] ,
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[4]_0 ,
    tx_fifo_or,
    D,
    sig_txd_pf_event__1,
    sig_txd_pe_event__1,
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[9]_0 ,
    \gtxc.txc_str_Valid_reg ,
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[4]_1 ,
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[4]_0 ,
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[8]_1 ,
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[8]_0 ,
    \gtxc.TXC_STATE_reg[0] ,
    \gtxc.TXC_STATE_reg[1] ,
    s_axi_aclk,
    Q,
    DIADI,
    inverted_reset,
    txd_wr_en,
    start_wr,
    axi_str_txd_tready,
    axi_str_txc_tlast,
    \gtxc.TXC_STATE_reg[1]_0 ,
    sig_txd_prog_full_d1,
    sig_txd_prog_empty_d1,
    axi_str_txc_tready,
    \gtxc.TXC_STATE_reg[0]_0 ,
    axi_str_txc_tvalid,
    \gtxc.txc_cntr_reg[1] ,
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[7]_0 ,
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[7]_0 );
  output \gaxi_full_sm.w_ready_r_reg ;
  output sig_txd_prog_empty;
  output p_7_out;
  output [32:0]\axi_str_txd_tdata[31] ;
  output [3:0]DI;
  output [4:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[8]_0 ;
  output \sig_register_array_reg[0][4] ;
  output axi_str_txd_tvalid;
  output [0:0]\gaxi_bid_gen.S_AXI_BID_reg[0] ;
  output [0:0]S;
  output [7:0]\gfifo_gen.gmm2s.vacancy_i_reg[9] ;
  output [0:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[4]_0 ;
  output tx_fifo_or;
  output [7:0]D;
  output sig_txd_pf_event__1;
  output sig_txd_pe_event__1;
  output [0:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[9]_0 ;
  output \gtxc.txc_str_Valid_reg ;
  output [3:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[4]_1 ;
  output [3:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[4]_0 ;
  output [3:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[8]_1 ;
  output [3:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[8]_0 ;
  output \gtxc.TXC_STATE_reg[0] ;
  output \gtxc.TXC_STATE_reg[1] ;
  input s_axi_aclk;
  input [31:0]Q;
  input [4:0]DIADI;
  input inverted_reset;
  input txd_wr_en;
  input start_wr;
  input axi_str_txd_tready;
  input axi_str_txc_tlast;
  input \gtxc.TXC_STATE_reg[1]_0 ;
  input sig_txd_prog_full_d1;
  input sig_txd_prog_empty_d1;
  input axi_str_txc_tready;
  input \gtxc.TXC_STATE_reg[0]_0 ;
  input axi_str_txc_tvalid;
  input \gtxc.txc_cntr_reg[1] ;
  input [8:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[7]_0 ;
  input [8:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[7]_0 ;

  wire [7:0]D;
  wire [3:0]DI;
  wire [4:0]DIADI;
  wire [31:0]Q;
  wire [0:0]S;
  wire axi_str_txc_tlast;
  wire axi_str_txc_tready;
  wire axi_str_txc_tvalid;
  wire [32:0]\axi_str_txd_tdata[31] ;
  wire axi_str_txd_tready;
  wire axi_str_txd_tvalid;
  wire axis_wr_eop;
  wire axis_wr_eop_d1;
  wire [0:0]\gaxi_bid_gen.S_AXI_BID_reg[0] ;
  wire \gaxi_full_sm.w_ready_r_reg ;
  wire \gaxis_fifo.gaxisf.axisf_n_3 ;
  wire \gaxis_fifo.gaxisf.axisf_n_4 ;
  wire \gaxis_fifo.gaxisf.axisf_n_45 ;
  wire \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt[0]_i_1_n_0 ;
  wire [3:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[4]_0 ;
  wire [8:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[7]_0 ;
  wire [4:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[8]_0 ;
  wire [3:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[8]_1 ;
  wire [9:8]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg__0 ;
  wire \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_i_4_n_0 ;
  wire \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_i_7_n_0 ;
  wire \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_reg_n_0 ;
  wire \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo[0]_i_1_n_0 ;
  wire [0:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[4]_0 ;
  wire [3:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[4]_1 ;
  wire [8:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[7]_0 ;
  wire [3:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[8]_0 ;
  wire [0:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[9]_0 ;
  wire [9:8]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg__0 ;
  wire \gfifo_gen.gmm2s.vacancy_i[7]_i_2_n_0 ;
  wire \gfifo_gen.gmm2s.vacancy_i[9]_i_2_n_0 ;
  wire [7:0]\gfifo_gen.gmm2s.vacancy_i_reg[9] ;
  wire \gtxc.TXC_STATE_reg[0] ;
  wire \gtxc.TXC_STATE_reg[0]_0 ;
  wire \gtxc.TXC_STATE_reg[1] ;
  wire \gtxc.TXC_STATE_reg[1]_0 ;
  wire \gtxc.txc_cntr_reg[1] ;
  wire \gtxc.txc_str_Valid_reg ;
  wire inverted_reset;
  wire p_7_out;
  wire s_axi_aclk;
  wire \sig_register_array[0][3]_i_6_n_0 ;
  wire \sig_register_array_reg[0][4] ;
  wire sig_txd_pe_event__1;
  wire sig_txd_pf_event__1;
  wire sig_txd_prog_empty;
  wire sig_txd_prog_empty_d1;
  wire sig_txd_prog_full_d1;
  wire start_wr;
  wire tx_fifo_or;
  wire txd_wr_en;
  wire [0:0]wr_rst_reg;

  axi_fifo_mm_s_0_fifo_generator_v13_0_5_fifo_generator_top \gaxis_fifo.gaxisf.axisf 
       (.DI(DI[0]),
        .DIADI(DIADI),
        .E(\gaxis_fifo.gaxisf.axisf_n_3 ),
        .Q(Q),
        .axi_str_txc_tlast(axi_str_txc_tlast),
        .axi_str_txc_tready(axi_str_txc_tready),
        .axi_str_txc_tvalid(axi_str_txc_tvalid),
        .\axi_str_txd_tdata[31] (\axi_str_txd_tdata[31] ),
        .axi_str_txd_tready(axi_str_txd_tready),
        .axi_str_txd_tvalid(axi_str_txd_tvalid),
        .axis_wr_eop(axis_wr_eop),
        .axis_wr_eop_d1(axis_wr_eop_d1),
        .\gaxi_bid_gen.S_AXI_BID_reg[0] (\gaxi_bid_gen.S_AXI_BID_reg[0] ),
        .\gaxi_full_sm.w_ready_r_reg (\gaxi_full_sm.w_ready_r_reg ),
        .\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[4] (\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[4]_0 [0]),
        .\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[5] (\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_i_4_n_0 ),
        .\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[5]_0 (\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_i_7_n_0 ),
        .\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[9] (\gaxis_fifo.gaxisf.axisf_n_4 ),
        .\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[9]_0 ({\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg__0 ,\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[8]_0 [4:3],DI[3:1],\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[8]_0 [0]}),
        .\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_reg (\gaxis_fifo.gaxisf.axisf_n_45 ),
        .\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_reg_0 (\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_reg_n_0 ),
        .\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[1] (\gfifo_gen.gmm2s.vacancy_i_reg[9] [1]),
        .\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[4] (\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[4]_1 [0]),
        .\gtxc.TXC_STATE_reg[0] (\gtxc.TXC_STATE_reg[0] ),
        .\gtxc.TXC_STATE_reg[0]_0 (\gtxc.TXC_STATE_reg[0]_0 ),
        .\gtxc.TXC_STATE_reg[1] (\gtxc.TXC_STATE_reg[1] ),
        .\gtxc.TXC_STATE_reg[1]_0 (\gtxc.TXC_STATE_reg[1]_0 ),
        .\gtxc.txc_cntr_reg[1] (\gtxc.txc_cntr_reg[1] ),
        .\gtxc.txc_str_Valid_reg (\gtxc.txc_str_Valid_reg ),
        .inverted_reset(inverted_reset),
        .p_7_out(p_7_out),
        .s_axi_aclk(s_axi_aclk),
        .\sig_register_array_reg[0][4] (\sig_register_array_reg[0][4] ),
        .sig_txd_pe_event__1(sig_txd_pe_event__1),
        .sig_txd_pf_event__1(sig_txd_pf_event__1),
        .sig_txd_prog_empty(sig_txd_prog_empty),
        .sig_txd_prog_empty_d1(sig_txd_prog_empty_d1),
        .sig_txd_prog_full_d1(sig_txd_prog_full_d1),
        .start_wr(start_wr),
        .txd_wr_en(txd_wr_en));
  LUT1 #(
    .INIT(2'h1)) 
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt[0]_i_1 
       (.I0(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[8]_0 [0]),
        .O(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt[0]_i_1_n_0 ));
  FDCE #(
    .INIT(1'b0)) 
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[0] 
       (.C(s_axi_aclk),
        .CE(\gaxis_fifo.gaxisf.axisf_n_4 ),
        .CLR(wr_rst_reg),
        .D(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt[0]_i_1_n_0 ),
        .Q(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[8]_0 [0]));
  FDCE #(
    .INIT(1'b0)) 
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[1] 
       (.C(s_axi_aclk),
        .CE(\gaxis_fifo.gaxisf.axisf_n_4 ),
        .CLR(wr_rst_reg),
        .D(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[7]_0 [0]),
        .Q(DI[1]));
  FDCE #(
    .INIT(1'b0)) 
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[2] 
       (.C(s_axi_aclk),
        .CE(\gaxis_fifo.gaxisf.axisf_n_4 ),
        .CLR(wr_rst_reg),
        .D(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[7]_0 [1]),
        .Q(DI[2]));
  FDCE #(
    .INIT(1'b0)) 
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[3] 
       (.C(s_axi_aclk),
        .CE(\gaxis_fifo.gaxisf.axisf_n_4 ),
        .CLR(wr_rst_reg),
        .D(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[7]_0 [2]),
        .Q(DI[3]));
  FDCE #(
    .INIT(1'b0)) 
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[4] 
       (.C(s_axi_aclk),
        .CE(\gaxis_fifo.gaxisf.axisf_n_4 ),
        .CLR(wr_rst_reg),
        .D(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[7]_0 [3]),
        .Q(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[8]_0 [1]));
  FDCE #(
    .INIT(1'b0)) 
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[5] 
       (.C(s_axi_aclk),
        .CE(\gaxis_fifo.gaxisf.axisf_n_4 ),
        .CLR(wr_rst_reg),
        .D(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[7]_0 [4]),
        .Q(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[8]_0 [2]));
  FDCE #(
    .INIT(1'b0)) 
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[6] 
       (.C(s_axi_aclk),
        .CE(\gaxis_fifo.gaxisf.axisf_n_4 ),
        .CLR(wr_rst_reg),
        .D(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[7]_0 [5]),
        .Q(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[8]_0 [3]));
  FDCE #(
    .INIT(1'b0)) 
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[7] 
       (.C(s_axi_aclk),
        .CE(\gaxis_fifo.gaxisf.axisf_n_4 ),
        .CLR(wr_rst_reg),
        .D(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[7]_0 [6]),
        .Q(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[8]_0 [4]));
  FDCE #(
    .INIT(1'b0)) 
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[8] 
       (.C(s_axi_aclk),
        .CE(\gaxis_fifo.gaxisf.axisf_n_4 ),
        .CLR(wr_rst_reg),
        .D(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[7]_0 [7]),
        .Q(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg__0 [8]));
  FDCE #(
    .INIT(1'b0)) 
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[9] 
       (.C(s_axi_aclk),
        .CE(\gaxis_fifo.gaxisf.axisf_n_4 ),
        .CLR(wr_rst_reg),
        .D(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[7]_0 [8]),
        .Q(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg__0 [9]));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_i_4 
       (.I0(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[8]_0 [2]),
        .I1(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[8]_0 [1]),
        .I2(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[8]_0 [4]),
        .I3(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[8]_0 [3]),
        .I4(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg__0 [8]),
        .I5(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg__0 [9]),
        .O(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_i_4_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_i_7 
       (.I0(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[8]_0 [2]),
        .I1(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[8]_0 [1]),
        .O(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_i_7_n_0 ));
  FDCE #(
    .INIT(1'b0)) 
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .CLR(wr_rst_reg),
        .D(\gaxis_fifo.gaxisf.axisf_n_45 ),
        .Q(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_reg_n_0 ));
  FDCE #(
    .INIT(1'b0)) 
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_wr_eop_d1_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .CLR(wr_rst_reg),
        .D(axis_wr_eop),
        .Q(axis_wr_eop_d1));
  LUT1 #(
    .INIT(2'h1)) 
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo[0]_i_1 
       (.I0(\gfifo_gen.gmm2s.vacancy_i_reg[9] [0]),
        .O(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo[0]_i_1_n_0 ));
  FDCE #(
    .INIT(1'b0)) 
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[0] 
       (.C(s_axi_aclk),
        .CE(\gaxis_fifo.gaxisf.axisf_n_3 ),
        .CLR(wr_rst_reg),
        .D(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo[0]_i_1_n_0 ),
        .Q(\gfifo_gen.gmm2s.vacancy_i_reg[9] [0]));
  FDCE #(
    .INIT(1'b0)) 
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[1] 
       (.C(s_axi_aclk),
        .CE(\gaxis_fifo.gaxisf.axisf_n_3 ),
        .CLR(wr_rst_reg),
        .D(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[7]_0 [0]),
        .Q(\gfifo_gen.gmm2s.vacancy_i_reg[9] [1]));
  FDCE #(
    .INIT(1'b0)) 
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[2] 
       (.C(s_axi_aclk),
        .CE(\gaxis_fifo.gaxisf.axisf_n_3 ),
        .CLR(wr_rst_reg),
        .D(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[7]_0 [1]),
        .Q(\gfifo_gen.gmm2s.vacancy_i_reg[9] [2]));
  FDCE #(
    .INIT(1'b0)) 
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[3] 
       (.C(s_axi_aclk),
        .CE(\gaxis_fifo.gaxisf.axisf_n_3 ),
        .CLR(wr_rst_reg),
        .D(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[7]_0 [2]),
        .Q(\gfifo_gen.gmm2s.vacancy_i_reg[9] [3]));
  FDCE #(
    .INIT(1'b0)) 
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[4] 
       (.C(s_axi_aclk),
        .CE(\gaxis_fifo.gaxisf.axisf_n_3 ),
        .CLR(wr_rst_reg),
        .D(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[7]_0 [3]),
        .Q(\gfifo_gen.gmm2s.vacancy_i_reg[9] [4]));
  FDCE #(
    .INIT(1'b0)) 
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[5] 
       (.C(s_axi_aclk),
        .CE(\gaxis_fifo.gaxisf.axisf_n_3 ),
        .CLR(wr_rst_reg),
        .D(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[7]_0 [4]),
        .Q(\gfifo_gen.gmm2s.vacancy_i_reg[9] [5]));
  FDCE #(
    .INIT(1'b0)) 
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[6] 
       (.C(s_axi_aclk),
        .CE(\gaxis_fifo.gaxisf.axisf_n_3 ),
        .CLR(wr_rst_reg),
        .D(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[7]_0 [5]),
        .Q(\gfifo_gen.gmm2s.vacancy_i_reg[9] [6]));
  FDCE #(
    .INIT(1'b0)) 
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[7] 
       (.C(s_axi_aclk),
        .CE(\gaxis_fifo.gaxisf.axisf_n_3 ),
        .CLR(wr_rst_reg),
        .D(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[7]_0 [6]),
        .Q(\gfifo_gen.gmm2s.vacancy_i_reg[9] [7]));
  FDCE #(
    .INIT(1'b0)) 
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[8] 
       (.C(s_axi_aclk),
        .CE(\gaxis_fifo.gaxisf.axisf_n_3 ),
        .CLR(wr_rst_reg),
        .D(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[7]_0 [7]),
        .Q(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg__0 [8]));
  FDCE #(
    .INIT(1'b0)) 
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[9] 
       (.C(s_axi_aclk),
        .CE(\gaxis_fifo.gaxisf.axisf_n_3 ),
        .CLR(wr_rst_reg),
        .D(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[7]_0 [8]),
        .Q(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg__0 [9]));
  LUT2 #(
    .INIT(4'h9)) 
    \gfifo_gen.gmm2s.vacancy_i[2]_i_1 
       (.I0(\gfifo_gen.gmm2s.vacancy_i_reg[9] [2]),
        .I1(\gfifo_gen.gmm2s.vacancy_i_reg[9] [1]),
        .O(D[0]));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT3 #(
    .INIT(8'h87)) 
    \gfifo_gen.gmm2s.vacancy_i[3]_i_1 
       (.I0(\gfifo_gen.gmm2s.vacancy_i_reg[9] [2]),
        .I1(\gfifo_gen.gmm2s.vacancy_i_reg[9] [1]),
        .I2(\gfifo_gen.gmm2s.vacancy_i_reg[9] [3]),
        .O(D[1]));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT4 #(
    .INIT(16'h807F)) 
    \gfifo_gen.gmm2s.vacancy_i[4]_i_1 
       (.I0(\gfifo_gen.gmm2s.vacancy_i_reg[9] [3]),
        .I1(\gfifo_gen.gmm2s.vacancy_i_reg[9] [1]),
        .I2(\gfifo_gen.gmm2s.vacancy_i_reg[9] [2]),
        .I3(\gfifo_gen.gmm2s.vacancy_i_reg[9] [4]),
        .O(D[2]));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT5 #(
    .INIT(32'h80007FFF)) 
    \gfifo_gen.gmm2s.vacancy_i[5]_i_1 
       (.I0(\gfifo_gen.gmm2s.vacancy_i_reg[9] [4]),
        .I1(\gfifo_gen.gmm2s.vacancy_i_reg[9] [2]),
        .I2(\gfifo_gen.gmm2s.vacancy_i_reg[9] [1]),
        .I3(\gfifo_gen.gmm2s.vacancy_i_reg[9] [3]),
        .I4(\gfifo_gen.gmm2s.vacancy_i_reg[9] [5]),
        .O(D[3]));
  LUT6 #(
    .INIT(64'h800000007FFFFFFF)) 
    \gfifo_gen.gmm2s.vacancy_i[6]_i_1 
       (.I0(\gfifo_gen.gmm2s.vacancy_i_reg[9] [5]),
        .I1(\gfifo_gen.gmm2s.vacancy_i_reg[9] [3]),
        .I2(\gfifo_gen.gmm2s.vacancy_i_reg[9] [1]),
        .I3(\gfifo_gen.gmm2s.vacancy_i_reg[9] [2]),
        .I4(\gfifo_gen.gmm2s.vacancy_i_reg[9] [4]),
        .I5(\gfifo_gen.gmm2s.vacancy_i_reg[9] [6]),
        .O(D[4]));
  LUT6 #(
    .INIT(64'h08000000F7FFFFFF)) 
    \gfifo_gen.gmm2s.vacancy_i[7]_i_1 
       (.I0(\gfifo_gen.gmm2s.vacancy_i_reg[9] [6]),
        .I1(\gfifo_gen.gmm2s.vacancy_i_reg[9] [4]),
        .I2(\gfifo_gen.gmm2s.vacancy_i[7]_i_2_n_0 ),
        .I3(\gfifo_gen.gmm2s.vacancy_i_reg[9] [3]),
        .I4(\gfifo_gen.gmm2s.vacancy_i_reg[9] [5]),
        .I5(\gfifo_gen.gmm2s.vacancy_i_reg[9] [7]),
        .O(D[5]));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \gfifo_gen.gmm2s.vacancy_i[7]_i_2 
       (.I0(\gfifo_gen.gmm2s.vacancy_i_reg[9] [1]),
        .I1(\gfifo_gen.gmm2s.vacancy_i_reg[9] [2]),
        .O(\gfifo_gen.gmm2s.vacancy_i[7]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT3 #(
    .INIT(8'h2D)) 
    \gfifo_gen.gmm2s.vacancy_i[8]_i_1 
       (.I0(\gfifo_gen.gmm2s.vacancy_i_reg[9] [7]),
        .I1(\gfifo_gen.gmm2s.vacancy_i[9]_i_2_n_0 ),
        .I2(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg__0 [8]),
        .O(D[6]));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT4 #(
    .INIT(16'hDF20)) 
    \gfifo_gen.gmm2s.vacancy_i[9]_i_1 
       (.I0(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg__0 [8]),
        .I1(\gfifo_gen.gmm2s.vacancy_i[9]_i_2_n_0 ),
        .I2(\gfifo_gen.gmm2s.vacancy_i_reg[9] [7]),
        .I3(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg__0 [9]),
        .O(D[7]));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    \gfifo_gen.gmm2s.vacancy_i[9]_i_2 
       (.I0(\gfifo_gen.gmm2s.vacancy_i_reg[9] [5]),
        .I1(\gfifo_gen.gmm2s.vacancy_i_reg[9] [3]),
        .I2(\gfifo_gen.gmm2s.vacancy_i_reg[9] [1]),
        .I3(\gfifo_gen.gmm2s.vacancy_i_reg[9] [2]),
        .I4(\gfifo_gen.gmm2s.vacancy_i_reg[9] [4]),
        .I5(\gfifo_gen.gmm2s.vacancy_i_reg[9] [6]),
        .O(\gfifo_gen.gmm2s.vacancy_i[9]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    p_0_out__24_carry__0_i_1
       (.I0(\gfifo_gen.gmm2s.vacancy_i_reg[9] [7]),
        .I1(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg__0 [8]),
        .O(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[8]_0 [3]));
  LUT2 #(
    .INIT(4'h9)) 
    p_0_out__24_carry__0_i_2
       (.I0(\gfifo_gen.gmm2s.vacancy_i_reg[9] [6]),
        .I1(\gfifo_gen.gmm2s.vacancy_i_reg[9] [7]),
        .O(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[8]_0 [2]));
  LUT2 #(
    .INIT(4'h9)) 
    p_0_out__24_carry__0_i_3
       (.I0(\gfifo_gen.gmm2s.vacancy_i_reg[9] [5]),
        .I1(\gfifo_gen.gmm2s.vacancy_i_reg[9] [6]),
        .O(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[8]_0 [1]));
  LUT2 #(
    .INIT(4'h9)) 
    p_0_out__24_carry__0_i_4
       (.I0(\gfifo_gen.gmm2s.vacancy_i_reg[9] [4]),
        .I1(\gfifo_gen.gmm2s.vacancy_i_reg[9] [5]),
        .O(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[8]_0 [0]));
  LUT2 #(
    .INIT(4'h9)) 
    p_0_out__24_carry__1_i_1
       (.I0(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg__0 [8]),
        .I1(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg__0 [9]),
        .O(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[9]_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    p_0_out__24_carry_i_1
       (.I0(\gfifo_gen.gmm2s.vacancy_i_reg[9] [1]),
        .O(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[4]_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    p_0_out__24_carry_i_2
       (.I0(\gfifo_gen.gmm2s.vacancy_i_reg[9] [3]),
        .I1(\gfifo_gen.gmm2s.vacancy_i_reg[9] [4]),
        .O(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[4]_1 [3]));
  LUT2 #(
    .INIT(4'h9)) 
    p_0_out__24_carry_i_3
       (.I0(\gfifo_gen.gmm2s.vacancy_i_reg[9] [2]),
        .I1(\gfifo_gen.gmm2s.vacancy_i_reg[9] [3]),
        .O(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[4]_1 [2]));
  LUT2 #(
    .INIT(4'h9)) 
    p_0_out__24_carry_i_4
       (.I0(\gfifo_gen.gmm2s.vacancy_i_reg[9] [1]),
        .I1(\gfifo_gen.gmm2s.vacancy_i_reg[9] [2]),
        .O(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg[4]_1 [1]));
  LUT2 #(
    .INIT(4'h9)) 
    p_0_out_carry__0_i_1
       (.I0(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[8]_0 [4]),
        .I1(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg__0 [8]),
        .O(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[8]_1 [3]));
  LUT2 #(
    .INIT(4'h9)) 
    p_0_out_carry__0_i_2
       (.I0(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[8]_0 [3]),
        .I1(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[8]_0 [4]),
        .O(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[8]_1 [2]));
  LUT2 #(
    .INIT(4'h9)) 
    p_0_out_carry__0_i_3
       (.I0(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[8]_0 [2]),
        .I1(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[8]_0 [3]),
        .O(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[8]_1 [1]));
  LUT2 #(
    .INIT(4'h9)) 
    p_0_out_carry__0_i_4
       (.I0(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[8]_0 [1]),
        .I1(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[8]_0 [2]),
        .O(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[8]_1 [0]));
  LUT2 #(
    .INIT(4'h9)) 
    p_0_out_carry__1_i_1
       (.I0(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg__0 [8]),
        .I1(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg__0 [9]),
        .O(S));
  LUT2 #(
    .INIT(4'h9)) 
    p_0_out_carry_i_2
       (.I0(DI[3]),
        .I1(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[8]_0 [1]),
        .O(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[4]_0 [3]));
  LUT2 #(
    .INIT(4'h9)) 
    p_0_out_carry_i_3
       (.I0(DI[2]),
        .I1(DI[3]),
        .O(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[4]_0 [2]));
  LUT2 #(
    .INIT(4'h9)) 
    p_0_out_carry_i_4
       (.I0(DI[1]),
        .I1(DI[2]),
        .O(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[4]_0 [1]));
  axi_fifo_mm_s_0_fifo_generator_v13_0_5_reset_blk_ramfifo \reset_gen_cc.rstblk_cc 
       (.AR(wr_rst_reg),
        .inverted_reset(inverted_reset),
        .s_axi_aclk(s_axi_aclk));
  LUT6 #(
    .INIT(64'hFFFFFFFF80000000)) 
    \sig_register_array[0][3]_i_3 
       (.I0(\sig_register_array[0][3]_i_6_n_0 ),
        .I1(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg__0 [8]),
        .I2(\gfifo_gen.gmm2s.vacancy_i_reg[9] [7]),
        .I3(\gfifo_gen.gmm2s.vacancy_i_reg[9] [2]),
        .I4(\gfifo_gen.gmm2s.vacancy_i_reg[9] [1]),
        .I5(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo_reg__0 [9]),
        .O(tx_fifo_or));
  LUT4 #(
    .INIT(16'h8000)) 
    \sig_register_array[0][3]_i_6 
       (.I0(\gfifo_gen.gmm2s.vacancy_i_reg[9] [4]),
        .I1(\gfifo_gen.gmm2s.vacancy_i_reg[9] [3]),
        .I2(\gfifo_gen.gmm2s.vacancy_i_reg[9] [6]),
        .I3(\gfifo_gen.gmm2s.vacancy_i_reg[9] [5]),
        .O(\sig_register_array[0][3]_i_6_n_0 ));
endmodule

(* ORIG_REF_NAME = "fifo_generator_v13_0_5_synth" *) 
module axi_fifo_mm_s_0_fifo_generator_v13_0_5_synth__parameterized0
   (\grxd.fg_rxd_wr_length_reg[1] ,
    empty_fwft_i,
    p_9_out,
    p_8_out,
    \grxd.fg_rxd_wr_length_reg[21] ,
    rx_complete,
    rx_str_wr_en,
    \grxd.sig_rxd_rd_data_reg[32] ,
    \gc0.count_reg[0] ,
    \gc0.count_reg[0]_0 ,
    axi_str_rxd_tready,
    DI,
    Q,
    \sig_register_array_reg[0][2] ,
    \sig_register_array_reg[0][1] ,
    \sig_register_array_reg[0][2]_0 ,
    sig_rxd_pf_event__1,
    sig_rxd_pe_event__1,
    S,
    \count_reg[4] ,
    \count_reg[8] ,
    \grxd.fg_rxd_wr_length_reg[2] ,
    s_axi4_rdata,
    s_axi_aclk,
    axi_str_rxd_tdata,
    axi_str_rxd_tlast,
    sync_areset_n_reg_inv,
    axis_rd_en__0,
    axi_str_rxd_tvalid,
    rx_len_wr_en,
    s_axi_aresetn,
    sig_str_rst_reg,
    sig_rxd_rd_data,
    axi4_fifo_rd_en_i,
    sig_rd_rlen_reg,
    \gaxi_full_sm.r_valid_r_reg ,
    \gaxi_full_sm.r_last_r_reg ,
    sig_axi_rd_en,
    \s_axi4_araddr[21] ,
    \s_axi4_araddr[29] ,
    s_axi4_araddr,
    sig_rx_channel_reset_reg,
    sig_rxd_prog_full_d1,
    sig_rxd_prog_empty_d1,
    O,
    \grxd.fg_rxd_wr_length_reg[2]_0 ,
    fg_rxd_wr_length,
    D);
  output \grxd.fg_rxd_wr_length_reg[1] ;
  output empty_fwft_i;
  output p_9_out;
  output p_8_out;
  output \grxd.fg_rxd_wr_length_reg[21] ;
  output rx_complete;
  output rx_str_wr_en;
  output \grxd.sig_rxd_rd_data_reg[32] ;
  output \gc0.count_reg[0] ;
  output \gc0.count_reg[0]_0 ;
  output axi_str_rxd_tready;
  output [3:0]DI;
  output [6:0]Q;
  output \sig_register_array_reg[0][2] ;
  output \sig_register_array_reg[0][1] ;
  output \sig_register_array_reg[0][2]_0 ;
  output sig_rxd_pf_event__1;
  output sig_rxd_pe_event__1;
  output [0:0]S;
  output [3:0]\count_reg[4] ;
  output [3:0]\count_reg[8] ;
  output \grxd.fg_rxd_wr_length_reg[2] ;
  output [31:0]s_axi4_rdata;
  input s_axi_aclk;
  input [31:0]axi_str_rxd_tdata;
  input axi_str_rxd_tlast;
  input sync_areset_n_reg_inv;
  input axis_rd_en__0;
  input axi_str_rxd_tvalid;
  input rx_len_wr_en;
  input s_axi_aresetn;
  input sig_str_rst_reg;
  input [0:0]sig_rxd_rd_data;
  input axi4_fifo_rd_en_i;
  input sig_rd_rlen_reg;
  input \gaxi_full_sm.r_valid_r_reg ;
  input \gaxi_full_sm.r_last_r_reg ;
  input sig_axi_rd_en;
  input \s_axi4_araddr[21] ;
  input \s_axi4_araddr[29] ;
  input [11:0]s_axi4_araddr;
  input sig_rx_channel_reset_reg;
  input sig_rxd_prog_full_d1;
  input sig_rxd_prog_empty_d1;
  input [0:0]O;
  input [0:0]\grxd.fg_rxd_wr_length_reg[2]_0 ;
  input [0:0]fg_rxd_wr_length;
  input [8:0]D;

  wire [8:0]D;
  wire [3:0]DI;
  wire [0:0]O;
  wire [6:0]Q;
  wire [0:0]S;
  wire axi4_fifo_rd_en_i;
  wire [31:0]axi_str_rxd_tdata;
  wire axi_str_rxd_tlast;
  wire axi_str_rxd_tready;
  wire axi_str_rxd_tvalid;
  wire axis_rd_en__0;
  wire [3:0]\count_reg[4] ;
  wire [3:0]\count_reg[8] ;
  wire empty_fwft_i;
  wire [0:0]fg_rxd_wr_length;
  wire \gaxi_full_sm.r_last_r_reg ;
  wire \gaxi_full_sm.r_valid_r_reg ;
  wire \gc0.count_reg[0] ;
  wire \gc0.count_reg[0]_0 ;
  wire \grxd.fg_rxd_wr_length_reg[1] ;
  wire \grxd.fg_rxd_wr_length_reg[21] ;
  wire \grxd.fg_rxd_wr_length_reg[2] ;
  wire [0:0]\grxd.fg_rxd_wr_length_reg[2]_0 ;
  wire \grxd.sig_rxd_rd_data_reg[32] ;
  wire p_8_out;
  wire p_9_out;
  wire rx_complete;
  wire rx_len_wr_en;
  wire rx_str_wr_en;
  wire [11:0]s_axi4_araddr;
  wire \s_axi4_araddr[21] ;
  wire \s_axi4_araddr[29] ;
  wire [31:0]s_axi4_rdata;
  wire s_axi_aclk;
  wire s_axi_aresetn;
  wire sig_axi_rd_en;
  wire sig_rd_rlen_reg;
  wire \sig_register_array_reg[0][1] ;
  wire \sig_register_array_reg[0][2] ;
  wire \sig_register_array_reg[0][2]_0 ;
  wire sig_rx_channel_reset_reg;
  wire sig_rxd_pe_event__1;
  wire sig_rxd_pf_event__1;
  wire sig_rxd_prog_empty_d1;
  wire sig_rxd_prog_full_d1;
  wire [0:0]sig_rxd_rd_data;
  wire sig_str_rst_reg;
  wire sync_areset_n_reg_inv;

  axi_fifo_mm_s_0_fifo_generator_v13_0_5_fifo_generator_top__parameterized0 \gaxis_fifo.gaxisf.axisf 
       (.D(D),
        .DI(DI),
        .O(O),
        .Q(Q),
        .S(S),
        .axi4_fifo_rd_en_i(axi4_fifo_rd_en_i),
        .axi_str_rxd_tdata(axi_str_rxd_tdata),
        .axi_str_rxd_tlast(axi_str_rxd_tlast),
        .axi_str_rxd_tready(axi_str_rxd_tready),
        .axi_str_rxd_tvalid(axi_str_rxd_tvalid),
        .axis_rd_en__0(axis_rd_en__0),
        .\count_reg[4] (\count_reg[4] ),
        .\count_reg[8] (\count_reg[8] ),
        .empty_fwft_i(empty_fwft_i),
        .fg_rxd_wr_length(fg_rxd_wr_length),
        .\gaxi_full_sm.r_last_r_reg (\gaxi_full_sm.r_last_r_reg ),
        .\gaxi_full_sm.r_valid_r_reg (\gaxi_full_sm.r_valid_r_reg ),
        .\gc0.count_reg[0] (\gc0.count_reg[0] ),
        .\gc0.count_reg[0]_0 (\gc0.count_reg[0]_0 ),
        .\grxd.fg_rxd_wr_length_reg[1] (\grxd.fg_rxd_wr_length_reg[1] ),
        .\grxd.fg_rxd_wr_length_reg[21] (\grxd.fg_rxd_wr_length_reg[21] ),
        .\grxd.fg_rxd_wr_length_reg[2] (\grxd.fg_rxd_wr_length_reg[2] ),
        .\grxd.fg_rxd_wr_length_reg[2]_0 (\grxd.fg_rxd_wr_length_reg[2]_0 ),
        .\grxd.sig_rxd_rd_data_reg[32] (\grxd.sig_rxd_rd_data_reg[32] ),
        .p_8_out(p_8_out),
        .p_9_out(p_9_out),
        .rx_complete(rx_complete),
        .rx_len_wr_en(rx_len_wr_en),
        .rx_str_wr_en(rx_str_wr_en),
        .s_axi4_araddr(s_axi4_araddr),
        .\s_axi4_araddr[21] (\s_axi4_araddr[21] ),
        .\s_axi4_araddr[29] (\s_axi4_araddr[29] ),
        .s_axi4_rdata(s_axi4_rdata),
        .s_axi_aclk(s_axi_aclk),
        .s_axi_aresetn(s_axi_aresetn),
        .sig_axi_rd_en(sig_axi_rd_en),
        .sig_rd_rlen_reg(sig_rd_rlen_reg),
        .\sig_register_array_reg[0][1] (\sig_register_array_reg[0][1] ),
        .\sig_register_array_reg[0][2] (\sig_register_array_reg[0][2] ),
        .\sig_register_array_reg[0][2]_0 (\sig_register_array_reg[0][2]_0 ),
        .sig_rx_channel_reset_reg(sig_rx_channel_reset_reg),
        .sig_rxd_pe_event__1(sig_rxd_pe_event__1),
        .sig_rxd_pf_event__1(sig_rxd_pf_event__1),
        .sig_rxd_prog_empty_d1(sig_rxd_prog_empty_d1),
        .sig_rxd_prog_full_d1(sig_rxd_prog_full_d1),
        .sig_rxd_rd_data(sig_rxd_rd_data),
        .sig_str_rst_reg(sig_str_rst_reg),
        .sync_areset_n_reg_inv(sync_areset_n_reg_inv));
  axi_fifo_mm_s_0_fifo_generator_v13_0_5_reset_blk_ramfifo_4 \reset_gen_cc.rstblk_cc 
       (.s_axi_aclk(s_axi_aclk),
        .sync_areset_n_reg_inv(sync_areset_n_reg_inv));
endmodule

(* ORIG_REF_NAME = "fifo_generator_v13_0_5_updn_cntr" *) 
module axi_fifo_mm_s_0_fifo_generator_v13_0_5_updn_cntr
   (DI,
    \sig_ip2bus_data_reg[20] ,
    \sig_register_array_reg[0][2] ,
    \sig_register_array_reg[0][1] ,
    \sig_register_array_reg[0][2]_0 ,
    \count_reg[9]_0 ,
    \count_reg[4]_0 ,
    \count_reg[8]_0 ,
    sig_rx_channel_reset_reg,
    sig_rxd_rd_data,
    valid_fwft,
    axis_rd_en__0,
    \gpregsm1.user_valid_reg ,
    D,
    s_axi_aclk,
    Q);
  output [3:0]DI;
  output [6:0]\sig_ip2bus_data_reg[20] ;
  output \sig_register_array_reg[0][2] ;
  output \sig_register_array_reg[0][1] ;
  output \sig_register_array_reg[0][2]_0 ;
  output [0:0]\count_reg[9]_0 ;
  output [3:0]\count_reg[4]_0 ;
  output [3:0]\count_reg[8]_0 ;
  input sig_rx_channel_reset_reg;
  input [0:0]sig_rxd_rd_data;
  input valid_fwft;
  input axis_rd_en__0;
  input [0:0]\gpregsm1.user_valid_reg ;
  input [8:0]D;
  input s_axi_aclk;
  input [0:0]Q;

  wire [8:0]D;
  wire [3:0]DI;
  wire [0:0]Q;
  wire axis_rd_en__0;
  wire \count[0]_i_1_n_0 ;
  wire [3:0]\count_reg[4]_0 ;
  wire [3:0]\count_reg[8]_0 ;
  wire [0:0]\count_reg[9]_0 ;
  wire [0:0]\gpregsm1.user_valid_reg ;
  wire s_axi_aclk;
  wire [6:0]\sig_ip2bus_data_reg[20] ;
  wire \sig_register_array[0][2]_i_8_n_0 ;
  wire \sig_register_array_reg[0][1] ;
  wire \sig_register_array_reg[0][2] ;
  wire \sig_register_array_reg[0][2]_0 ;
  wire sig_rx_channel_reset_reg;
  wire [0:0]sig_rxd_rd_data;
  wire valid_fwft;

  LUT1 #(
    .INIT(2'h1)) 
    \count[0]_i_1 
       (.I0(\sig_ip2bus_data_reg[20] [0]),
        .O(\count[0]_i_1_n_0 ));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[0] 
       (.C(s_axi_aclk),
        .CE(\gpregsm1.user_valid_reg ),
        .CLR(Q),
        .D(\count[0]_i_1_n_0 ),
        .Q(\sig_ip2bus_data_reg[20] [0]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[1] 
       (.C(s_axi_aclk),
        .CE(\gpregsm1.user_valid_reg ),
        .CLR(Q),
        .D(D[0]),
        .Q(DI[1]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[2] 
       (.C(s_axi_aclk),
        .CE(\gpregsm1.user_valid_reg ),
        .CLR(Q),
        .D(D[1]),
        .Q(DI[2]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[3] 
       (.C(s_axi_aclk),
        .CE(\gpregsm1.user_valid_reg ),
        .CLR(Q),
        .D(D[2]),
        .Q(DI[3]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[4] 
       (.C(s_axi_aclk),
        .CE(\gpregsm1.user_valid_reg ),
        .CLR(Q),
        .D(D[3]),
        .Q(\sig_ip2bus_data_reg[20] [1]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[5] 
       (.C(s_axi_aclk),
        .CE(\gpregsm1.user_valid_reg ),
        .CLR(Q),
        .D(D[4]),
        .Q(\sig_ip2bus_data_reg[20] [2]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[6] 
       (.C(s_axi_aclk),
        .CE(\gpregsm1.user_valid_reg ),
        .CLR(Q),
        .D(D[5]),
        .Q(\sig_ip2bus_data_reg[20] [3]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[7] 
       (.C(s_axi_aclk),
        .CE(\gpregsm1.user_valid_reg ),
        .CLR(Q),
        .D(D[6]),
        .Q(\sig_ip2bus_data_reg[20] [4]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[8] 
       (.C(s_axi_aclk),
        .CE(\gpregsm1.user_valid_reg ),
        .CLR(Q),
        .D(D[7]),
        .Q(\sig_ip2bus_data_reg[20] [5]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[9] 
       (.C(s_axi_aclk),
        .CE(\gpregsm1.user_valid_reg ),
        .CLR(Q),
        .D(D[8]),
        .Q(\sig_ip2bus_data_reg[20] [6]));
  LUT2 #(
    .INIT(4'h9)) 
    p_0_out__49_carry__0_i_1
       (.I0(\sig_ip2bus_data_reg[20] [4]),
        .I1(\sig_ip2bus_data_reg[20] [5]),
        .O(\count_reg[8]_0 [3]));
  LUT2 #(
    .INIT(4'h9)) 
    p_0_out__49_carry__0_i_2
       (.I0(\sig_ip2bus_data_reg[20] [3]),
        .I1(\sig_ip2bus_data_reg[20] [4]),
        .O(\count_reg[8]_0 [2]));
  LUT2 #(
    .INIT(4'h9)) 
    p_0_out__49_carry__0_i_3
       (.I0(\sig_ip2bus_data_reg[20] [2]),
        .I1(\sig_ip2bus_data_reg[20] [3]),
        .O(\count_reg[8]_0 [1]));
  LUT2 #(
    .INIT(4'h9)) 
    p_0_out__49_carry__0_i_4
       (.I0(\sig_ip2bus_data_reg[20] [1]),
        .I1(\sig_ip2bus_data_reg[20] [2]),
        .O(\count_reg[8]_0 [0]));
  LUT2 #(
    .INIT(4'h9)) 
    p_0_out__49_carry__1_i_1
       (.I0(\sig_ip2bus_data_reg[20] [5]),
        .I1(\sig_ip2bus_data_reg[20] [6]),
        .O(\count_reg[9]_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    p_0_out__49_carry_i_1
       (.I0(DI[1]),
        .O(DI[0]));
  LUT2 #(
    .INIT(4'h9)) 
    p_0_out__49_carry_i_2
       (.I0(DI[3]),
        .I1(\sig_ip2bus_data_reg[20] [1]),
        .O(\count_reg[4]_0 [3]));
  LUT2 #(
    .INIT(4'h9)) 
    p_0_out__49_carry_i_3
       (.I0(DI[2]),
        .I1(DI[3]),
        .O(\count_reg[4]_0 [2]));
  LUT2 #(
    .INIT(4'h9)) 
    p_0_out__49_carry_i_4
       (.I0(DI[1]),
        .I1(DI[2]),
        .O(\count_reg[4]_0 [1]));
  LUT3 #(
    .INIT(8'h6A)) 
    p_0_out__49_carry_i_5
       (.I0(DI[1]),
        .I1(valid_fwft),
        .I2(axis_rd_en__0),
        .O(\count_reg[4]_0 [0]));
  LUT3 #(
    .INIT(8'h10)) 
    \sig_register_array[0][1]_i_7 
       (.I0(sig_rx_channel_reset_reg),
        .I1(sig_rxd_rd_data),
        .I2(\sig_register_array_reg[0][2]_0 ),
        .O(\sig_register_array_reg[0][1] ));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    \sig_register_array[0][2]_i_5 
       (.I0(\sig_ip2bus_data_reg[20] [6]),
        .I1(\sig_ip2bus_data_reg[20] [3]),
        .I2(DI[2]),
        .I3(\sig_ip2bus_data_reg[20] [5]),
        .I4(\sig_register_array[0][2]_i_8_n_0 ),
        .I5(sig_rx_channel_reset_reg),
        .O(\sig_register_array_reg[0][2] ));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \sig_register_array[0][2]_i_7 
       (.I0(\sig_register_array[0][2]_i_8_n_0 ),
        .I1(\sig_ip2bus_data_reg[20] [5]),
        .I2(DI[2]),
        .I3(\sig_ip2bus_data_reg[20] [3]),
        .I4(\sig_ip2bus_data_reg[20] [6]),
        .O(\sig_register_array_reg[0][2]_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \sig_register_array[0][2]_i_8 
       (.I0(DI[3]),
        .I1(\sig_ip2bus_data_reg[20] [1]),
        .I2(\sig_ip2bus_data_reg[20] [0]),
        .I3(DI[1]),
        .I4(\sig_ip2bus_data_reg[20] [2]),
        .I5(\sig_ip2bus_data_reg[20] [4]),
        .O(\sig_register_array[0][2]_i_8_n_0 ));
endmodule

(* ORIG_REF_NAME = "fifo_generator_v13_0_5_wr_bin_cntr" *) 
module axi_fifo_mm_s_0_fifo_generator_v13_0_5_wr_bin_cntr
   (Q,
    v1_reg,
    \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram ,
    v1_reg_0,
    \greg.gpcry_sym.diff_pntr_pad_reg[9] ,
    \gdiff.gcry_1_sym.diff_pntr_pad_reg[9] ,
    ram_empty_fb_i_reg,
    \greg.gpcry_sym.diff_pntr_pad_reg[8] ,
    S,
    \gdiff.gcry_1_sym.diff_pntr_pad_reg[8] ,
    \gcc0.gc1.gsym.count_d2_reg[7]_0 ,
    \gdiff.gcry_1_sym.diff_pntr_pad_reg[4] ,
    \gc0.count_d1_reg[8] ,
    E,
    s_axi_aclk,
    AR);
  output [8:0]Q;
  output [0:0]v1_reg;
  output [8:0]\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram ;
  output [0:0]v1_reg_0;
  output [0:0]\greg.gpcry_sym.diff_pntr_pad_reg[9] ;
  output [0:0]\gdiff.gcry_1_sym.diff_pntr_pad_reg[9] ;
  output ram_empty_fb_i_reg;
  output [3:0]\greg.gpcry_sym.diff_pntr_pad_reg[8] ;
  output [3:0]S;
  output [3:0]\gdiff.gcry_1_sym.diff_pntr_pad_reg[8] ;
  output [7:0]\gcc0.gc1.gsym.count_d2_reg[7]_0 ;
  output [3:0]\gdiff.gcry_1_sym.diff_pntr_pad_reg[4] ;
  input [8:0]\gc0.count_d1_reg[8] ;
  input [0:0]E;
  input s_axi_aclk;
  input [0:0]AR;

  wire [0:0]AR;
  wire [8:0]\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram ;
  wire [0:0]E;
  wire [8:0]Q;
  wire [3:0]S;
  wire [8:0]\gc0.count_d1_reg[8] ;
  wire \gcc0.gc1.gsym.count[8]_i_2_n_0 ;
  wire [7:0]\gcc0.gc1.gsym.count_d2_reg[7]_0 ;
  wire [3:0]\gdiff.gcry_1_sym.diff_pntr_pad_reg[4] ;
  wire [3:0]\gdiff.gcry_1_sym.diff_pntr_pad_reg[8] ;
  wire [0:0]\gdiff.gcry_1_sym.diff_pntr_pad_reg[9] ;
  wire [3:0]\greg.gpcry_sym.diff_pntr_pad_reg[8] ;
  wire [0:0]\greg.gpcry_sym.diff_pntr_pad_reg[9] ;
  wire [8:8]p_11_out;
  wire [8:0]plusOp__0;
  wire ram_empty_fb_i_reg;
  wire s_axi_aclk;
  wire [0:0]v1_reg;
  wire [0:0]v1_reg_0;

  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \gcc0.gc1.gsym.count[0]_i_1 
       (.I0(Q[0]),
        .O(plusOp__0[0]));
  LUT2 #(
    .INIT(4'h6)) 
    \gcc0.gc1.gsym.count[1]_i_1 
       (.I0(Q[0]),
        .I1(Q[1]),
        .O(plusOp__0[1]));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \gcc0.gc1.gsym.count[2]_i_1 
       (.I0(Q[0]),
        .I1(Q[1]),
        .I2(Q[2]),
        .O(plusOp__0[2]));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \gcc0.gc1.gsym.count[3]_i_1 
       (.I0(Q[1]),
        .I1(Q[0]),
        .I2(Q[2]),
        .I3(Q[3]),
        .O(plusOp__0[3]));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \gcc0.gc1.gsym.count[4]_i_1 
       (.I0(Q[2]),
        .I1(Q[0]),
        .I2(Q[1]),
        .I3(Q[3]),
        .I4(Q[4]),
        .O(plusOp__0[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \gcc0.gc1.gsym.count[5]_i_1 
       (.I0(Q[3]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(Q[2]),
        .I4(Q[4]),
        .I5(Q[5]),
        .O(plusOp__0[5]));
  LUT2 #(
    .INIT(4'h6)) 
    \gcc0.gc1.gsym.count[6]_i_1 
       (.I0(\gcc0.gc1.gsym.count[8]_i_2_n_0 ),
        .I1(Q[6]),
        .O(plusOp__0[6]));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \gcc0.gc1.gsym.count[7]_i_1 
       (.I0(\gcc0.gc1.gsym.count[8]_i_2_n_0 ),
        .I1(Q[6]),
        .I2(Q[7]),
        .O(plusOp__0[7]));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \gcc0.gc1.gsym.count[8]_i_1 
       (.I0(Q[6]),
        .I1(\gcc0.gc1.gsym.count[8]_i_2_n_0 ),
        .I2(Q[7]),
        .I3(Q[8]),
        .O(plusOp__0[8]));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \gcc0.gc1.gsym.count[8]_i_2 
       (.I0(Q[5]),
        .I1(Q[3]),
        .I2(Q[1]),
        .I3(Q[0]),
        .I4(Q[2]),
        .I5(Q[4]),
        .O(\gcc0.gc1.gsym.count[8]_i_2_n_0 ));
  FDPE #(
    .INIT(1'b1)) 
    \gcc0.gc1.gsym.count_d1_reg[0] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(Q[0]),
        .PRE(AR),
        .Q(\gcc0.gc1.gsym.count_d2_reg[7]_0 [0]));
  FDCE #(
    .INIT(1'b0)) 
    \gcc0.gc1.gsym.count_d1_reg[1] 
       (.C(s_axi_aclk),
        .CE(E),
        .CLR(AR),
        .D(Q[1]),
        .Q(\gcc0.gc1.gsym.count_d2_reg[7]_0 [1]));
  FDCE #(
    .INIT(1'b0)) 
    \gcc0.gc1.gsym.count_d1_reg[2] 
       (.C(s_axi_aclk),
        .CE(E),
        .CLR(AR),
        .D(Q[2]),
        .Q(\gcc0.gc1.gsym.count_d2_reg[7]_0 [2]));
  FDCE #(
    .INIT(1'b0)) 
    \gcc0.gc1.gsym.count_d1_reg[3] 
       (.C(s_axi_aclk),
        .CE(E),
        .CLR(AR),
        .D(Q[3]),
        .Q(\gcc0.gc1.gsym.count_d2_reg[7]_0 [3]));
  FDCE #(
    .INIT(1'b0)) 
    \gcc0.gc1.gsym.count_d1_reg[4] 
       (.C(s_axi_aclk),
        .CE(E),
        .CLR(AR),
        .D(Q[4]),
        .Q(\gcc0.gc1.gsym.count_d2_reg[7]_0 [4]));
  FDCE #(
    .INIT(1'b0)) 
    \gcc0.gc1.gsym.count_d1_reg[5] 
       (.C(s_axi_aclk),
        .CE(E),
        .CLR(AR),
        .D(Q[5]),
        .Q(\gcc0.gc1.gsym.count_d2_reg[7]_0 [5]));
  FDCE #(
    .INIT(1'b0)) 
    \gcc0.gc1.gsym.count_d1_reg[6] 
       (.C(s_axi_aclk),
        .CE(E),
        .CLR(AR),
        .D(Q[6]),
        .Q(\gcc0.gc1.gsym.count_d2_reg[7]_0 [6]));
  FDCE #(
    .INIT(1'b0)) 
    \gcc0.gc1.gsym.count_d1_reg[7] 
       (.C(s_axi_aclk),
        .CE(E),
        .CLR(AR),
        .D(Q[7]),
        .Q(\gcc0.gc1.gsym.count_d2_reg[7]_0 [7]));
  FDCE #(
    .INIT(1'b0)) 
    \gcc0.gc1.gsym.count_d1_reg[8] 
       (.C(s_axi_aclk),
        .CE(E),
        .CLR(AR),
        .D(Q[8]),
        .Q(p_11_out));
  FDCE #(
    .INIT(1'b0)) 
    \gcc0.gc1.gsym.count_d2_reg[0] 
       (.C(s_axi_aclk),
        .CE(E),
        .CLR(AR),
        .D(\gcc0.gc1.gsym.count_d2_reg[7]_0 [0]),
        .Q(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram [0]));
  FDCE #(
    .INIT(1'b0)) 
    \gcc0.gc1.gsym.count_d2_reg[1] 
       (.C(s_axi_aclk),
        .CE(E),
        .CLR(AR),
        .D(\gcc0.gc1.gsym.count_d2_reg[7]_0 [1]),
        .Q(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram [1]));
  FDCE #(
    .INIT(1'b0)) 
    \gcc0.gc1.gsym.count_d2_reg[2] 
       (.C(s_axi_aclk),
        .CE(E),
        .CLR(AR),
        .D(\gcc0.gc1.gsym.count_d2_reg[7]_0 [2]),
        .Q(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram [2]));
  FDCE #(
    .INIT(1'b0)) 
    \gcc0.gc1.gsym.count_d2_reg[3] 
       (.C(s_axi_aclk),
        .CE(E),
        .CLR(AR),
        .D(\gcc0.gc1.gsym.count_d2_reg[7]_0 [3]),
        .Q(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram [3]));
  FDCE #(
    .INIT(1'b0)) 
    \gcc0.gc1.gsym.count_d2_reg[4] 
       (.C(s_axi_aclk),
        .CE(E),
        .CLR(AR),
        .D(\gcc0.gc1.gsym.count_d2_reg[7]_0 [4]),
        .Q(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram [4]));
  FDCE #(
    .INIT(1'b0)) 
    \gcc0.gc1.gsym.count_d2_reg[5] 
       (.C(s_axi_aclk),
        .CE(E),
        .CLR(AR),
        .D(\gcc0.gc1.gsym.count_d2_reg[7]_0 [5]),
        .Q(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram [5]));
  FDCE #(
    .INIT(1'b0)) 
    \gcc0.gc1.gsym.count_d2_reg[6] 
       (.C(s_axi_aclk),
        .CE(E),
        .CLR(AR),
        .D(\gcc0.gc1.gsym.count_d2_reg[7]_0 [6]),
        .Q(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram [6]));
  FDCE #(
    .INIT(1'b0)) 
    \gcc0.gc1.gsym.count_d2_reg[7] 
       (.C(s_axi_aclk),
        .CE(E),
        .CLR(AR),
        .D(\gcc0.gc1.gsym.count_d2_reg[7]_0 [7]),
        .Q(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram [7]));
  FDCE #(
    .INIT(1'b0)) 
    \gcc0.gc1.gsym.count_d2_reg[8] 
       (.C(s_axi_aclk),
        .CE(E),
        .CLR(AR),
        .D(p_11_out),
        .Q(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram [8]));
  FDCE #(
    .INIT(1'b0)) 
    \gcc0.gc1.gsym.count_reg[0] 
       (.C(s_axi_aclk),
        .CE(E),
        .CLR(AR),
        .D(plusOp__0[0]),
        .Q(Q[0]));
  FDPE #(
    .INIT(1'b1)) 
    \gcc0.gc1.gsym.count_reg[1] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(plusOp__0[1]),
        .PRE(AR),
        .Q(Q[1]));
  FDCE #(
    .INIT(1'b0)) 
    \gcc0.gc1.gsym.count_reg[2] 
       (.C(s_axi_aclk),
        .CE(E),
        .CLR(AR),
        .D(plusOp__0[2]),
        .Q(Q[2]));
  FDCE #(
    .INIT(1'b0)) 
    \gcc0.gc1.gsym.count_reg[3] 
       (.C(s_axi_aclk),
        .CE(E),
        .CLR(AR),
        .D(plusOp__0[3]),
        .Q(Q[3]));
  FDCE #(
    .INIT(1'b0)) 
    \gcc0.gc1.gsym.count_reg[4] 
       (.C(s_axi_aclk),
        .CE(E),
        .CLR(AR),
        .D(plusOp__0[4]),
        .Q(Q[4]));
  FDCE #(
    .INIT(1'b0)) 
    \gcc0.gc1.gsym.count_reg[5] 
       (.C(s_axi_aclk),
        .CE(E),
        .CLR(AR),
        .D(plusOp__0[5]),
        .Q(Q[5]));
  FDCE #(
    .INIT(1'b0)) 
    \gcc0.gc1.gsym.count_reg[6] 
       (.C(s_axi_aclk),
        .CE(E),
        .CLR(AR),
        .D(plusOp__0[6]),
        .Q(Q[6]));
  FDCE #(
    .INIT(1'b0)) 
    \gcc0.gc1.gsym.count_reg[7] 
       (.C(s_axi_aclk),
        .CE(E),
        .CLR(AR),
        .D(plusOp__0[7]),
        .Q(Q[7]));
  FDCE #(
    .INIT(1'b0)) 
    \gcc0.gc1.gsym.count_reg[8] 
       (.C(s_axi_aclk),
        .CE(E),
        .CLR(AR),
        .D(plusOp__0[8]),
        .Q(Q[8]));
  LUT2 #(
    .INIT(4'h9)) 
    \gmux.gm[4].gms.ms_i_1 
       (.I0(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram [8]),
        .I1(\gc0.count_d1_reg[8] [8]),
        .O(v1_reg));
  LUT2 #(
    .INIT(4'h9)) 
    \gmux.gm[4].gms.ms_i_1__0 
       (.I0(p_11_out),
        .I1(\gc0.count_d1_reg[8] [8]),
        .O(v1_reg_0));
  LUT2 #(
    .INIT(4'h9)) 
    \gmux.gm[4].gms.ms_i_1__6 
       (.I0(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram [8]),
        .I1(\gc0.count_d1_reg[8] [8]),
        .O(ram_empty_fb_i_reg));
  LUT2 #(
    .INIT(4'h9)) 
    plusOp_carry__0_i_1
       (.I0(\gcc0.gc1.gsym.count_d2_reg[7]_0 [7]),
        .I1(\gc0.count_d1_reg[8] [7]),
        .O(\gdiff.gcry_1_sym.diff_pntr_pad_reg[8] [3]));
  LUT2 #(
    .INIT(4'h9)) 
    plusOp_carry__0_i_1__0
       (.I0(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram [7]),
        .I1(\gc0.count_d1_reg[8] [7]),
        .O(\greg.gpcry_sym.diff_pntr_pad_reg[8] [3]));
  LUT2 #(
    .INIT(4'h9)) 
    plusOp_carry__0_i_2__0
       (.I0(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram [6]),
        .I1(\gc0.count_d1_reg[8] [6]),
        .O(\greg.gpcry_sym.diff_pntr_pad_reg[8] [2]));
  LUT2 #(
    .INIT(4'h9)) 
    plusOp_carry__0_i_2__1
       (.I0(\gcc0.gc1.gsym.count_d2_reg[7]_0 [6]),
        .I1(\gc0.count_d1_reg[8] [6]),
        .O(\gdiff.gcry_1_sym.diff_pntr_pad_reg[8] [2]));
  LUT2 #(
    .INIT(4'h9)) 
    plusOp_carry__0_i_3
       (.I0(\gcc0.gc1.gsym.count_d2_reg[7]_0 [5]),
        .I1(\gc0.count_d1_reg[8] [5]),
        .O(\gdiff.gcry_1_sym.diff_pntr_pad_reg[8] [1]));
  LUT2 #(
    .INIT(4'h9)) 
    plusOp_carry__0_i_3__0
       (.I0(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram [5]),
        .I1(\gc0.count_d1_reg[8] [5]),
        .O(\greg.gpcry_sym.diff_pntr_pad_reg[8] [1]));
  LUT2 #(
    .INIT(4'h9)) 
    plusOp_carry__0_i_4__0
       (.I0(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram [4]),
        .I1(\gc0.count_d1_reg[8] [4]),
        .O(\greg.gpcry_sym.diff_pntr_pad_reg[8] [0]));
  LUT2 #(
    .INIT(4'h9)) 
    plusOp_carry__0_i_4__1
       (.I0(\gcc0.gc1.gsym.count_d2_reg[7]_0 [4]),
        .I1(\gc0.count_d1_reg[8] [4]),
        .O(\gdiff.gcry_1_sym.diff_pntr_pad_reg[8] [0]));
  LUT2 #(
    .INIT(4'h9)) 
    plusOp_carry__1_i_1
       (.I0(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram [8]),
        .I1(\gc0.count_d1_reg[8] [8]),
        .O(\greg.gpcry_sym.diff_pntr_pad_reg[9] ));
  LUT2 #(
    .INIT(4'h9)) 
    plusOp_carry__1_i_1__0
       (.I0(p_11_out),
        .I1(\gc0.count_d1_reg[8] [8]),
        .O(\gdiff.gcry_1_sym.diff_pntr_pad_reg[9] ));
  LUT2 #(
    .INIT(4'h9)) 
    plusOp_carry_i_2
       (.I0(\gcc0.gc1.gsym.count_d2_reg[7]_0 [3]),
        .I1(\gc0.count_d1_reg[8] [3]),
        .O(\gdiff.gcry_1_sym.diff_pntr_pad_reg[4] [3]));
  LUT2 #(
    .INIT(4'h9)) 
    plusOp_carry_i_2__0
       (.I0(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram [3]),
        .I1(\gc0.count_d1_reg[8] [3]),
        .O(S[3]));
  LUT2 #(
    .INIT(4'h9)) 
    plusOp_carry_i_3__1
       (.I0(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram [2]),
        .I1(\gc0.count_d1_reg[8] [2]),
        .O(S[2]));
  LUT2 #(
    .INIT(4'h9)) 
    plusOp_carry_i_3__2
       (.I0(\gcc0.gc1.gsym.count_d2_reg[7]_0 [2]),
        .I1(\gc0.count_d1_reg[8] [2]),
        .O(\gdiff.gcry_1_sym.diff_pntr_pad_reg[4] [2]));
  LUT2 #(
    .INIT(4'h9)) 
    plusOp_carry_i_4
       (.I0(\gcc0.gc1.gsym.count_d2_reg[7]_0 [1]),
        .I1(\gc0.count_d1_reg[8] [1]),
        .O(\gdiff.gcry_1_sym.diff_pntr_pad_reg[4] [1]));
  LUT2 #(
    .INIT(4'h9)) 
    plusOp_carry_i_4__0
       (.I0(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram [1]),
        .I1(\gc0.count_d1_reg[8] [1]),
        .O(S[1]));
  LUT2 #(
    .INIT(4'h9)) 
    plusOp_carry_i_5__0
       (.I0(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram [0]),
        .I1(\gc0.count_d1_reg[8] [0]),
        .O(S[0]));
  LUT2 #(
    .INIT(4'h9)) 
    plusOp_carry_i_5__1
       (.I0(\gcc0.gc1.gsym.count_d2_reg[7]_0 [0]),
        .I1(\gc0.count_d1_reg[8] [0]),
        .O(\gdiff.gcry_1_sym.diff_pntr_pad_reg[4] [0]));
endmodule

(* ORIG_REF_NAME = "fifo_generator_v13_0_5_wr_bin_cntr" *) 
module axi_fifo_mm_s_0_fifo_generator_v13_0_5_wr_bin_cntr__parameterized0
   (Q,
    v1_reg,
    \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram ,
    \greg.gpcry_sym.diff_pntr_pad_reg[9] ,
    ram_empty_fb_i_reg,
    \greg.gpcry_sym.diff_pntr_pad_reg[8] ,
    S,
    \gdiff.gcry_1_sym.diff_pntr_pad_reg[8] ,
    \gdiff.gcry_1_sym.diff_pntr_pad_reg[4] ,
    \gc0.count_d1_reg[8] ,
    E,
    s_axi_aclk,
    AR);
  output [8:0]Q;
  output [0:0]v1_reg;
  output [8:0]\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram ;
  output [0:0]\greg.gpcry_sym.diff_pntr_pad_reg[9] ;
  output ram_empty_fb_i_reg;
  output [3:0]\greg.gpcry_sym.diff_pntr_pad_reg[8] ;
  output [3:0]S;
  output [3:0]\gdiff.gcry_1_sym.diff_pntr_pad_reg[8] ;
  output [3:0]\gdiff.gcry_1_sym.diff_pntr_pad_reg[4] ;
  input [8:0]\gc0.count_d1_reg[8] ;
  input [0:0]E;
  input s_axi_aclk;
  input [0:0]AR;

  wire [0:0]AR;
  wire [8:0]\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram ;
  wire [0:0]E;
  wire [8:0]Q;
  wire [3:0]S;
  wire [8:0]\gc0.count_d1_reg[8] ;
  wire \gcc0.gc0.count[8]_i_2_n_0 ;
  wire [3:0]\gdiff.gcry_1_sym.diff_pntr_pad_reg[4] ;
  wire [3:0]\gdiff.gcry_1_sym.diff_pntr_pad_reg[8] ;
  wire [3:0]\greg.gpcry_sym.diff_pntr_pad_reg[8] ;
  wire [0:0]\greg.gpcry_sym.diff_pntr_pad_reg[9] ;
  wire [8:0]plusOp__2;
  wire ram_empty_fb_i_reg;
  wire s_axi_aclk;
  wire [0:0]v1_reg;

  LUT1 #(
    .INIT(2'h1)) 
    \gcc0.gc0.count[0]_i_1 
       (.I0(Q[0]),
        .O(plusOp__2[0]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \gcc0.gc0.count[1]_i_1 
       (.I0(Q[0]),
        .I1(Q[1]),
        .O(plusOp__2[1]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \gcc0.gc0.count[2]_i_1 
       (.I0(Q[0]),
        .I1(Q[1]),
        .I2(Q[2]),
        .O(plusOp__2[2]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \gcc0.gc0.count[3]_i_1 
       (.I0(Q[1]),
        .I1(Q[0]),
        .I2(Q[2]),
        .I3(Q[3]),
        .O(plusOp__2[3]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \gcc0.gc0.count[4]_i_1 
       (.I0(Q[2]),
        .I1(Q[0]),
        .I2(Q[1]),
        .I3(Q[3]),
        .I4(Q[4]),
        .O(plusOp__2[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \gcc0.gc0.count[5]_i_1 
       (.I0(Q[3]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(Q[2]),
        .I4(Q[4]),
        .I5(Q[5]),
        .O(plusOp__2[5]));
  LUT2 #(
    .INIT(4'h6)) 
    \gcc0.gc0.count[6]_i_1 
       (.I0(\gcc0.gc0.count[8]_i_2_n_0 ),
        .I1(Q[6]),
        .O(plusOp__2[6]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \gcc0.gc0.count[7]_i_1 
       (.I0(\gcc0.gc0.count[8]_i_2_n_0 ),
        .I1(Q[6]),
        .I2(Q[7]),
        .O(plusOp__2[7]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \gcc0.gc0.count[8]_i_1 
       (.I0(Q[6]),
        .I1(\gcc0.gc0.count[8]_i_2_n_0 ),
        .I2(Q[7]),
        .I3(Q[8]),
        .O(plusOp__2[8]));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \gcc0.gc0.count[8]_i_2 
       (.I0(Q[5]),
        .I1(Q[3]),
        .I2(Q[1]),
        .I3(Q[0]),
        .I4(Q[2]),
        .I5(Q[4]),
        .O(\gcc0.gc0.count[8]_i_2_n_0 ));
  FDCE #(
    .INIT(1'b0)) 
    \gcc0.gc0.count_d1_reg[0] 
       (.C(s_axi_aclk),
        .CE(E),
        .CLR(AR),
        .D(Q[0]),
        .Q(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram [0]));
  FDCE #(
    .INIT(1'b0)) 
    \gcc0.gc0.count_d1_reg[1] 
       (.C(s_axi_aclk),
        .CE(E),
        .CLR(AR),
        .D(Q[1]),
        .Q(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram [1]));
  FDCE #(
    .INIT(1'b0)) 
    \gcc0.gc0.count_d1_reg[2] 
       (.C(s_axi_aclk),
        .CE(E),
        .CLR(AR),
        .D(Q[2]),
        .Q(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram [2]));
  FDCE #(
    .INIT(1'b0)) 
    \gcc0.gc0.count_d1_reg[3] 
       (.C(s_axi_aclk),
        .CE(E),
        .CLR(AR),
        .D(Q[3]),
        .Q(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram [3]));
  FDCE #(
    .INIT(1'b0)) 
    \gcc0.gc0.count_d1_reg[4] 
       (.C(s_axi_aclk),
        .CE(E),
        .CLR(AR),
        .D(Q[4]),
        .Q(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram [4]));
  FDCE #(
    .INIT(1'b0)) 
    \gcc0.gc0.count_d1_reg[5] 
       (.C(s_axi_aclk),
        .CE(E),
        .CLR(AR),
        .D(Q[5]),
        .Q(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram [5]));
  FDCE #(
    .INIT(1'b0)) 
    \gcc0.gc0.count_d1_reg[6] 
       (.C(s_axi_aclk),
        .CE(E),
        .CLR(AR),
        .D(Q[6]),
        .Q(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram [6]));
  FDCE #(
    .INIT(1'b0)) 
    \gcc0.gc0.count_d1_reg[7] 
       (.C(s_axi_aclk),
        .CE(E),
        .CLR(AR),
        .D(Q[7]),
        .Q(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram [7]));
  FDCE #(
    .INIT(1'b0)) 
    \gcc0.gc0.count_d1_reg[8] 
       (.C(s_axi_aclk),
        .CE(E),
        .CLR(AR),
        .D(Q[8]),
        .Q(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram [8]));
  FDPE #(
    .INIT(1'b1)) 
    \gcc0.gc0.count_reg[0] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(plusOp__2[0]),
        .PRE(AR),
        .Q(Q[0]));
  FDCE #(
    .INIT(1'b0)) 
    \gcc0.gc0.count_reg[1] 
       (.C(s_axi_aclk),
        .CE(E),
        .CLR(AR),
        .D(plusOp__2[1]),
        .Q(Q[1]));
  FDCE #(
    .INIT(1'b0)) 
    \gcc0.gc0.count_reg[2] 
       (.C(s_axi_aclk),
        .CE(E),
        .CLR(AR),
        .D(plusOp__2[2]),
        .Q(Q[2]));
  FDCE #(
    .INIT(1'b0)) 
    \gcc0.gc0.count_reg[3] 
       (.C(s_axi_aclk),
        .CE(E),
        .CLR(AR),
        .D(plusOp__2[3]),
        .Q(Q[3]));
  FDCE #(
    .INIT(1'b0)) 
    \gcc0.gc0.count_reg[4] 
       (.C(s_axi_aclk),
        .CE(E),
        .CLR(AR),
        .D(plusOp__2[4]),
        .Q(Q[4]));
  FDCE #(
    .INIT(1'b0)) 
    \gcc0.gc0.count_reg[5] 
       (.C(s_axi_aclk),
        .CE(E),
        .CLR(AR),
        .D(plusOp__2[5]),
        .Q(Q[5]));
  FDCE #(
    .INIT(1'b0)) 
    \gcc0.gc0.count_reg[6] 
       (.C(s_axi_aclk),
        .CE(E),
        .CLR(AR),
        .D(plusOp__2[6]),
        .Q(Q[6]));
  FDCE #(
    .INIT(1'b0)) 
    \gcc0.gc0.count_reg[7] 
       (.C(s_axi_aclk),
        .CE(E),
        .CLR(AR),
        .D(plusOp__2[7]),
        .Q(Q[7]));
  FDCE #(
    .INIT(1'b0)) 
    \gcc0.gc0.count_reg[8] 
       (.C(s_axi_aclk),
        .CE(E),
        .CLR(AR),
        .D(plusOp__2[8]),
        .Q(Q[8]));
  LUT2 #(
    .INIT(4'h9)) 
    \gmux.gm[4].gms.ms_i_1__1 
       (.I0(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram [8]),
        .I1(\gc0.count_d1_reg[8] [8]),
        .O(v1_reg));
  LUT2 #(
    .INIT(4'h9)) 
    \gmux.gm[4].gms.ms_i_1__7 
       (.I0(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram [8]),
        .I1(\gc0.count_d1_reg[8] [8]),
        .O(ram_empty_fb_i_reg));
  LUT2 #(
    .INIT(4'h9)) 
    plusOp_carry__0_i_1__1
       (.I0(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram [7]),
        .I1(\gc0.count_d1_reg[8] [7]),
        .O(\greg.gpcry_sym.diff_pntr_pad_reg[8] [3]));
  LUT2 #(
    .INIT(4'h9)) 
    plusOp_carry__0_i_1__2
       (.I0(Q[7]),
        .I1(\gc0.count_d1_reg[8] [7]),
        .O(\gdiff.gcry_1_sym.diff_pntr_pad_reg[8] [3]));
  LUT2 #(
    .INIT(4'h9)) 
    plusOp_carry__0_i_2
       (.I0(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram [6]),
        .I1(\gc0.count_d1_reg[8] [6]),
        .O(\greg.gpcry_sym.diff_pntr_pad_reg[8] [2]));
  LUT2 #(
    .INIT(4'h9)) 
    plusOp_carry__0_i_2__2
       (.I0(Q[6]),
        .I1(\gc0.count_d1_reg[8] [6]),
        .O(\gdiff.gcry_1_sym.diff_pntr_pad_reg[8] [2]));
  LUT2 #(
    .INIT(4'h9)) 
    plusOp_carry__0_i_3__1
       (.I0(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram [5]),
        .I1(\gc0.count_d1_reg[8] [5]),
        .O(\greg.gpcry_sym.diff_pntr_pad_reg[8] [1]));
  LUT2 #(
    .INIT(4'h9)) 
    plusOp_carry__0_i_3__2
       (.I0(Q[5]),
        .I1(\gc0.count_d1_reg[8] [5]),
        .O(\gdiff.gcry_1_sym.diff_pntr_pad_reg[8] [1]));
  LUT2 #(
    .INIT(4'h9)) 
    plusOp_carry__0_i_4
       (.I0(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram [4]),
        .I1(\gc0.count_d1_reg[8] [4]),
        .O(\greg.gpcry_sym.diff_pntr_pad_reg[8] [0]));
  LUT2 #(
    .INIT(4'h9)) 
    plusOp_carry__0_i_4__2
       (.I0(Q[4]),
        .I1(\gc0.count_d1_reg[8] [4]),
        .O(\gdiff.gcry_1_sym.diff_pntr_pad_reg[8] [0]));
  LUT2 #(
    .INIT(4'h9)) 
    plusOp_carry__1_i_1__1
       (.I0(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram [8]),
        .I1(\gc0.count_d1_reg[8] [8]),
        .O(\greg.gpcry_sym.diff_pntr_pad_reg[9] ));
  LUT2 #(
    .INIT(4'h9)) 
    plusOp_carry_i_2__1
       (.I0(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram [3]),
        .I1(\gc0.count_d1_reg[8] [3]),
        .O(S[3]));
  LUT2 #(
    .INIT(4'h9)) 
    plusOp_carry_i_2__2
       (.I0(Q[3]),
        .I1(\gc0.count_d1_reg[8] [3]),
        .O(\gdiff.gcry_1_sym.diff_pntr_pad_reg[4] [3]));
  LUT2 #(
    .INIT(4'h9)) 
    plusOp_carry_i_3__0
       (.I0(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram [2]),
        .I1(\gc0.count_d1_reg[8] [2]),
        .O(S[2]));
  LUT2 #(
    .INIT(4'h9)) 
    plusOp_carry_i_3__3
       (.I0(Q[2]),
        .I1(\gc0.count_d1_reg[8] [2]),
        .O(\gdiff.gcry_1_sym.diff_pntr_pad_reg[4] [2]));
  LUT2 #(
    .INIT(4'h9)) 
    plusOp_carry_i_4__1
       (.I0(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram [1]),
        .I1(\gc0.count_d1_reg[8] [1]),
        .O(S[1]));
  LUT2 #(
    .INIT(4'h9)) 
    plusOp_carry_i_4__2
       (.I0(Q[1]),
        .I1(\gc0.count_d1_reg[8] [1]),
        .O(\gdiff.gcry_1_sym.diff_pntr_pad_reg[4] [1]));
  LUT2 #(
    .INIT(4'h9)) 
    plusOp_carry_i_5
       (.I0(\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram [0]),
        .I1(\gc0.count_d1_reg[8] [0]),
        .O(S[0]));
  LUT2 #(
    .INIT(4'h9)) 
    plusOp_carry_i_5__2
       (.I0(Q[0]),
        .I1(\gc0.count_d1_reg[8] [0]),
        .O(\gdiff.gcry_1_sym.diff_pntr_pad_reg[4] [0]));
endmodule

(* ORIG_REF_NAME = "fifo_generator_v13_0_5_wr_logic" *) 
module axi_fifo_mm_s_0_fifo_generator_v13_0_5_wr_logic
   (p_16_out,
    \gaxi_full_sm.w_ready_r_reg ,
    Q,
    p_7_out,
    E,
    rd_pntr_inv_pad,
    \gaxi_bid_gen.S_AXI_BID_reg[0] ,
    p_0_in__10,
    \gcc0.gc1.gsym.count_d1_reg[8] ,
    \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram ,
    sig_txd_pf_event__1,
    axis_wr_eop,
    \greg.gpcry_sym.diff_pntr_pad_reg[9] ,
    ram_empty_fb_i_reg,
    \greg.gpcry_sym.diff_pntr_pad_reg[8] ,
    S,
    \gc0.count_d1_reg[6] ,
    \gc0.count_d1_reg[6]_0 ,
    v1_reg,
    s_axi_aclk,
    out,
    p_6_out,
    AR,
    txd_wr_en,
    DIADI,
    start_wr,
    axis_rd_en__0,
    \grstd1.grst_full.grst_f.rst_d5_reg ,
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[9] ,
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[5] ,
    p_8_out,
    \gc0.count_d1_reg[8] ,
    sig_txd_prog_full_d1);
  output p_16_out;
  output \gaxi_full_sm.w_ready_r_reg ;
  output [7:0]Q;
  output p_7_out;
  output [0:0]E;
  output [0:0]rd_pntr_inv_pad;
  output [0:0]\gaxi_bid_gen.S_AXI_BID_reg[0] ;
  output p_0_in__10;
  output [8:0]\gcc0.gc1.gsym.count_d1_reg[8] ;
  output [8:0]\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram ;
  output sig_txd_pf_event__1;
  output axis_wr_eop;
  output [0:0]\greg.gpcry_sym.diff_pntr_pad_reg[9] ;
  output ram_empty_fb_i_reg;
  output [3:0]\greg.gpcry_sym.diff_pntr_pad_reg[8] ;
  output [3:0]S;
  input [3:0]\gc0.count_d1_reg[6] ;
  input [3:0]\gc0.count_d1_reg[6]_0 ;
  input [4:0]v1_reg;
  input s_axi_aclk;
  input out;
  input p_6_out;
  input [0:0]AR;
  input txd_wr_en;
  input [0:0]DIADI;
  input start_wr;
  input axis_rd_en__0;
  input \grstd1.grst_full.grst_f.rst_d5_reg ;
  input [7:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[9] ;
  input \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[5] ;
  input p_8_out;
  input [8:0]\gc0.count_d1_reg[8] ;
  input sig_txd_prog_full_d1;

  wire [0:0]AR;
  wire [8:0]\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram ;
  wire [0:0]DIADI;
  wire [0:0]E;
  wire [7:0]Q;
  wire [3:0]S;
  wire axis_rd_en__0;
  wire axis_wr_eop;
  wire [4:4]\c0/v1_reg ;
  wire [4:4]\c1/v1_reg ;
  wire [0:0]\gaxi_bid_gen.S_AXI_BID_reg[0] ;
  wire \gaxi_full_sm.w_ready_r_reg ;
  wire \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[5] ;
  wire [7:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[9] ;
  wire [3:0]\gc0.count_d1_reg[6] ;
  wire [3:0]\gc0.count_d1_reg[6]_0 ;
  wire [8:0]\gc0.count_d1_reg[8] ;
  wire [8:0]\gcc0.gc1.gsym.count_d1_reg[8] ;
  wire [3:0]\greg.gpcry_sym.diff_pntr_pad_reg[8] ;
  wire [0:0]\greg.gpcry_sym.diff_pntr_pad_reg[9] ;
  wire \grstd1.grst_full.grst_f.rst_d5_reg ;
  wire \gwss.wsts_n_3 ;
  wire out;
  wire p_0_in__10;
  wire p_16_out;
  wire p_6_out;
  wire p_7_out;
  wire p_8_out;
  wire ram_empty_fb_i_reg;
  wire [0:0]rd_pntr_inv_pad;
  wire s_axi_aclk;
  wire sig_txd_pf_event__1;
  wire sig_txd_prog_full_d1;
  wire start_wr;
  wire txd_wr_en;
  wire [4:0]v1_reg;
  wire wpntr_n_21;
  wire wpntr_n_31;
  wire wpntr_n_32;
  wire wpntr_n_33;
  wire wpntr_n_34;
  wire wpntr_n_43;
  wire wpntr_n_44;
  wire wpntr_n_45;
  wire wpntr_n_46;

  axi_fifo_mm_s_0_fifo_generator_v13_0_5_wr_pf_ss \gwss.gpf.wrpf 
       (.AR(AR),
        .E(p_16_out),
        .Q(Q),
        .S({wpntr_n_43,wpntr_n_44,wpntr_n_45,wpntr_n_46}),
        .\gcc0.gc1.gsym.count_d1_reg[7] ({wpntr_n_31,wpntr_n_32,wpntr_n_33,wpntr_n_34}),
        .\gcc0.gc1.gsym.count_d1_reg[8] (wpntr_n_21),
        .\grstd1.grst_full.grst_f.rst_d5_reg (\grstd1.grst_full.grst_f.rst_d5_reg ),
        .out(out),
        .p_6_out(p_6_out),
        .p_7_out(p_7_out),
        .ram_full_fb_i_reg(\gwss.wsts_n_3 ),
        .s_axi_aclk(s_axi_aclk),
        .sig_txd_pf_event__1(sig_txd_pf_event__1),
        .sig_txd_prog_full_d1(sig_txd_prog_full_d1));
  axi_fifo_mm_s_0_fifo_generator_v13_0_5_wr_status_flags_ss \gwss.wsts 
       (.DIADI(DIADI),
        .E(E),
        .axis_rd_en__0(axis_rd_en__0),
        .axis_wr_eop(axis_wr_eop),
        .\gaxi_bid_gen.S_AXI_BID_reg[0] (\gaxi_bid_gen.S_AXI_BID_reg[0] ),
        .\gaxi_full_sm.w_ready_r_reg (\gaxi_full_sm.w_ready_r_reg ),
        .\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[5] (\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[5] ),
        .\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[9] (\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[9] ),
        .\gc0.count_d1_reg[6] (\gc0.count_d1_reg[6] ),
        .\gc0.count_d1_reg[6]_0 (\gc0.count_d1_reg[6]_0 ),
        .\gcc0.gc1.gsym.count_reg[0] (p_16_out),
        .\gdiff.gcry_1_sym.diff_pntr_pad_reg[4] (\gwss.wsts_n_3 ),
        .\grstd1.grst_full.grst_f.rst_d5_reg (\grstd1.grst_full.grst_f.rst_d5_reg ),
        .out(out),
        .p_0_in__10(p_0_in__10),
        .p_6_out(p_6_out),
        .p_8_out(p_8_out),
        .rd_pntr_inv_pad(rd_pntr_inv_pad),
        .s_axi_aclk(s_axi_aclk),
        .start_wr(start_wr),
        .txd_wr_en(txd_wr_en),
        .v1_reg(v1_reg),
        .v1_reg_0(\c0/v1_reg ),
        .v1_reg_1(\c1/v1_reg ));
  axi_fifo_mm_s_0_fifo_generator_v13_0_5_wr_bin_cntr wpntr
       (.AR(AR),
        .\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram (\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram ),
        .E(p_16_out),
        .Q(\gcc0.gc1.gsym.count_d1_reg[8] ),
        .S(S),
        .\gc0.count_d1_reg[8] (\gc0.count_d1_reg[8] ),
        .\gcc0.gc1.gsym.count_d2_reg[7]_0 (Q),
        .\gdiff.gcry_1_sym.diff_pntr_pad_reg[4] ({wpntr_n_43,wpntr_n_44,wpntr_n_45,wpntr_n_46}),
        .\gdiff.gcry_1_sym.diff_pntr_pad_reg[8] ({wpntr_n_31,wpntr_n_32,wpntr_n_33,wpntr_n_34}),
        .\gdiff.gcry_1_sym.diff_pntr_pad_reg[9] (wpntr_n_21),
        .\greg.gpcry_sym.diff_pntr_pad_reg[8] (\greg.gpcry_sym.diff_pntr_pad_reg[8] ),
        .\greg.gpcry_sym.diff_pntr_pad_reg[9] (\greg.gpcry_sym.diff_pntr_pad_reg[9] ),
        .ram_empty_fb_i_reg(ram_empty_fb_i_reg),
        .s_axi_aclk(s_axi_aclk),
        .v1_reg(\c0/v1_reg ),
        .v1_reg_0(\c1/v1_reg ));
endmodule

(* ORIG_REF_NAME = "fifo_generator_v13_0_5_wr_logic" *) 
module axi_fifo_mm_s_0_fifo_generator_v13_0_5_wr_logic__parameterized0
   (p_16_out,
    \grxd.fg_rxd_wr_length_reg[1] ,
    Q,
    p_8_out,
    \count_reg[9] ,
    rd_pntr_inv_pad,
    \grxd.fg_rxd_wr_length_reg[21] ,
    rx_complete,
    rx_str_wr_en,
    axi_str_rxd_tready,
    \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram ,
    sig_rxd_pf_event__1,
    \greg.gpcry_sym.diff_pntr_pad_reg[9] ,
    ram_empty_fb_i_reg,
    \greg.gpcry_sym.diff_pntr_pad_reg[8] ,
    S,
    \grxd.fg_rxd_wr_length_reg[2] ,
    \gc0.count_d1_reg[6] ,
    v1_reg,
    s_axi_aclk,
    out,
    p_6_out,
    AR,
    \gc0.count_d1_reg[8] ,
    valid_fwft,
    axis_rd_en__0,
    axi_str_rxd_tvalid,
    \grstd1.grst_full.grst_f.rst_d5_reg ,
    axi_str_rxd_tlast,
    rx_len_wr_en,
    s_axi_aresetn,
    sig_str_rst_reg,
    \gc0.count_d1_reg[8]_0 ,
    sig_rxd_prog_full_d1,
    O,
    \grxd.fg_rxd_wr_length_reg[2]_0 ,
    fg_rxd_wr_length);
  output p_16_out;
  output \grxd.fg_rxd_wr_length_reg[1] ;
  output [8:0]Q;
  output p_8_out;
  output [0:0]\count_reg[9] ;
  output [0:0]rd_pntr_inv_pad;
  output \grxd.fg_rxd_wr_length_reg[21] ;
  output rx_complete;
  output rx_str_wr_en;
  output axi_str_rxd_tready;
  output [8:0]\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram ;
  output sig_rxd_pf_event__1;
  output [0:0]\greg.gpcry_sym.diff_pntr_pad_reg[9] ;
  output ram_empty_fb_i_reg;
  output [3:0]\greg.gpcry_sym.diff_pntr_pad_reg[8] ;
  output [3:0]S;
  output \grxd.fg_rxd_wr_length_reg[2] ;
  input [3:0]\gc0.count_d1_reg[6] ;
  input [4:0]v1_reg;
  input s_axi_aclk;
  input out;
  input p_6_out;
  input [0:0]AR;
  input [0:0]\gc0.count_d1_reg[8] ;
  input valid_fwft;
  input axis_rd_en__0;
  input axi_str_rxd_tvalid;
  input \grstd1.grst_full.grst_f.rst_d5_reg ;
  input axi_str_rxd_tlast;
  input rx_len_wr_en;
  input s_axi_aresetn;
  input sig_str_rst_reg;
  input [8:0]\gc0.count_d1_reg[8]_0 ;
  input sig_rxd_prog_full_d1;
  input [0:0]O;
  input [0:0]\grxd.fg_rxd_wr_length_reg[2]_0 ;
  input [0:0]fg_rxd_wr_length;

  wire [0:0]AR;
  wire [8:0]\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram ;
  wire [0:0]O;
  wire [8:0]Q;
  wire [3:0]S;
  wire axi_str_rxd_tlast;
  wire axi_str_rxd_tready;
  wire axi_str_rxd_tvalid;
  wire axis_rd_en__0;
  wire [4:4]\c0/v1_reg ;
  wire [0:0]\count_reg[9] ;
  wire [0:0]fg_rxd_wr_length;
  wire [3:0]\gc0.count_d1_reg[6] ;
  wire [0:0]\gc0.count_d1_reg[8] ;
  wire [8:0]\gc0.count_d1_reg[8]_0 ;
  wire [3:0]\greg.gpcry_sym.diff_pntr_pad_reg[8] ;
  wire [0:0]\greg.gpcry_sym.diff_pntr_pad_reg[9] ;
  wire \grstd1.grst_full.grst_f.rst_d5_reg ;
  wire \grxd.fg_rxd_wr_length_reg[1] ;
  wire \grxd.fg_rxd_wr_length_reg[21] ;
  wire \grxd.fg_rxd_wr_length_reg[2] ;
  wire [0:0]\grxd.fg_rxd_wr_length_reg[2]_0 ;
  wire out;
  wire p_16_out;
  wire p_3_out;
  wire p_6_out;
  wire p_8_out;
  wire ram_empty_fb_i_reg;
  wire [0:0]rd_pntr_inv_pad;
  wire rx_complete;
  wire rx_len_wr_en;
  wire rx_str_wr_en;
  wire s_axi_aclk;
  wire s_axi_aresetn;
  wire sig_rxd_pf_event__1;
  wire sig_rxd_prog_full_d1;
  wire sig_str_rst_reg;
  wire [4:0]v1_reg;
  wire valid_fwft;
  wire wpntr_n_29;
  wire wpntr_n_30;
  wire wpntr_n_31;
  wire wpntr_n_32;
  wire wpntr_n_33;
  wire wpntr_n_34;
  wire wpntr_n_35;
  wire wpntr_n_36;

  axi_fifo_mm_s_0_fifo_generator_v13_0_5_wr_pf_ss_13 \gwss.gpf.wrpf 
       (.AR(AR),
        .E(p_16_out),
        .Q(Q[7:0]),
        .S({wpntr_n_33,wpntr_n_34,wpntr_n_35,wpntr_n_36}),
        .\gc0.count_d1_reg[8] (\gc0.count_d1_reg[8] ),
        .\gcc0.gc0.count_reg[7] ({wpntr_n_29,wpntr_n_30,wpntr_n_31,wpntr_n_32}),
        .\grstd1.grst_full.grst_f.rst_d5_reg (\grstd1.grst_full.grst_f.rst_d5_reg ),
        .out(out),
        .p_3_out(p_3_out),
        .p_6_out(p_6_out),
        .p_8_out(p_8_out),
        .s_axi_aclk(s_axi_aclk),
        .sig_rxd_pf_event__1(sig_rxd_pf_event__1),
        .sig_rxd_prog_full_d1(sig_rxd_prog_full_d1));
  axi_fifo_mm_s_0_fifo_generator_v13_0_5_wr_status_flags_ss__parameterized0 \gwss.wsts 
       (.E(p_16_out),
        .O(O),
        .axi_str_rxd_tlast(axi_str_rxd_tlast),
        .axi_str_rxd_tready(axi_str_rxd_tready),
        .axi_str_rxd_tvalid(axi_str_rxd_tvalid),
        .axis_rd_en__0(axis_rd_en__0),
        .\count_reg[9] (\count_reg[9] ),
        .fg_rxd_wr_length(fg_rxd_wr_length),
        .\gc0.count_d1_reg[6] (\gc0.count_d1_reg[6] ),
        .\grstd1.grst_full.grst_f.rst_d5_reg (\grstd1.grst_full.grst_f.rst_d5_reg ),
        .\grxd.fg_rxd_wr_length_reg[1] (\grxd.fg_rxd_wr_length_reg[1] ),
        .\grxd.fg_rxd_wr_length_reg[21] (\grxd.fg_rxd_wr_length_reg[21] ),
        .\grxd.fg_rxd_wr_length_reg[2] (\grxd.fg_rxd_wr_length_reg[2] ),
        .\grxd.fg_rxd_wr_length_reg[2]_0 (\grxd.fg_rxd_wr_length_reg[2]_0 ),
        .out(out),
        .p_3_out(p_3_out),
        .p_6_out(p_6_out),
        .rd_pntr_inv_pad(rd_pntr_inv_pad),
        .rx_complete(rx_complete),
        .rx_len_wr_en(rx_len_wr_en),
        .rx_str_wr_en(rx_str_wr_en),
        .s_axi_aclk(s_axi_aclk),
        .s_axi_aresetn(s_axi_aresetn),
        .sig_str_rst_reg(sig_str_rst_reg),
        .v1_reg(v1_reg),
        .v1_reg_0(\c0/v1_reg ),
        .valid_fwft(valid_fwft));
  axi_fifo_mm_s_0_fifo_generator_v13_0_5_wr_bin_cntr__parameterized0 wpntr
       (.AR(AR),
        .\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram (\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram ),
        .E(p_16_out),
        .Q(Q),
        .S(S),
        .\gc0.count_d1_reg[8] (\gc0.count_d1_reg[8]_0 ),
        .\gdiff.gcry_1_sym.diff_pntr_pad_reg[4] ({wpntr_n_33,wpntr_n_34,wpntr_n_35,wpntr_n_36}),
        .\gdiff.gcry_1_sym.diff_pntr_pad_reg[8] ({wpntr_n_29,wpntr_n_30,wpntr_n_31,wpntr_n_32}),
        .\greg.gpcry_sym.diff_pntr_pad_reg[8] (\greg.gpcry_sym.diff_pntr_pad_reg[8] ),
        .\greg.gpcry_sym.diff_pntr_pad_reg[9] (\greg.gpcry_sym.diff_pntr_pad_reg[9] ),
        .ram_empty_fb_i_reg(ram_empty_fb_i_reg),
        .s_axi_aclk(s_axi_aclk),
        .v1_reg(\c0/v1_reg ));
endmodule

(* ORIG_REF_NAME = "fifo_generator_v13_0_5_wr_pf_ss" *) 
module axi_fifo_mm_s_0_fifo_generator_v13_0_5_wr_pf_ss
   (p_7_out,
    sig_txd_pf_event__1,
    p_6_out,
    s_axi_aclk,
    AR,
    E,
    ram_full_fb_i_reg,
    Q,
    S,
    \gcc0.gc1.gsym.count_d1_reg[7] ,
    \gcc0.gc1.gsym.count_d1_reg[8] ,
    out,
    sig_txd_prog_full_d1,
    \grstd1.grst_full.grst_f.rst_d5_reg );
  output p_7_out;
  output sig_txd_pf_event__1;
  input p_6_out;
  input s_axi_aclk;
  input [0:0]AR;
  input [0:0]E;
  input ram_full_fb_i_reg;
  input [7:0]Q;
  input [3:0]S;
  input [3:0]\gcc0.gc1.gsym.count_d1_reg[7] ;
  input [0:0]\gcc0.gc1.gsym.count_d1_reg[8] ;
  input out;
  input sig_txd_prog_full_d1;
  input \grstd1.grst_full.grst_f.rst_d5_reg ;

  wire [0:0]AR;
  wire [0:0]E;
  wire [7:0]Q;
  wire [3:0]S;
  wire eqOp__7;
  wire [3:0]\gcc0.gc1.gsym.count_d1_reg[7] ;
  wire [0:0]\gcc0.gc1.gsym.count_d1_reg[8] ;
  wire \gdiff.gcry_1_sym.diff_pntr_pad_reg_n_0_[1] ;
  wire \gdiff.gcry_1_sym.diff_pntr_pad_reg_n_0_[2] ;
  wire \gdiff.gcry_1_sym.diff_pntr_pad_reg_n_0_[3] ;
  wire \gdiff.gcry_1_sym.diff_pntr_pad_reg_n_0_[4] ;
  wire \gdiff.gcry_1_sym.diff_pntr_pad_reg_n_0_[5] ;
  wire \gdiff.gcry_1_sym.diff_pntr_pad_reg_n_0_[6] ;
  wire \gdiff.gcry_1_sym.diff_pntr_pad_reg_n_0_[7] ;
  wire \gdiff.gcry_1_sym.diff_pntr_pad_reg_n_0_[8] ;
  wire \gdiff.gcry_1_sym.diff_pntr_pad_reg_n_0_[9] ;
  wire \gpfs.prog_full_i_i_1_n_0 ;
  wire \gpfs.prog_full_i_i_3_n_0 ;
  wire \grstd1.grst_full.grst_f.rst_d5_reg ;
  wire out;
  wire p_6_out;
  wire p_7_out;
  wire plusOp_carry__0_n_0;
  wire plusOp_carry__0_n_1;
  wire plusOp_carry__0_n_2;
  wire plusOp_carry__0_n_3;
  wire plusOp_carry__0_n_4;
  wire plusOp_carry__0_n_5;
  wire plusOp_carry__0_n_6;
  wire plusOp_carry__0_n_7;
  wire plusOp_carry__1_n_7;
  wire plusOp_carry_n_0;
  wire plusOp_carry_n_1;
  wire plusOp_carry_n_2;
  wire plusOp_carry_n_3;
  wire plusOp_carry_n_4;
  wire plusOp_carry_n_5;
  wire plusOp_carry_n_6;
  wire plusOp_carry_n_7;
  wire ram_full_fb_i_reg;
  wire ram_rd_en_i;
  wire ram_wr_en_i;
  wire s_axi_aclk;
  wire sig_txd_pf_event__1;
  wire sig_txd_prog_full_d1;
  wire [3:0]NLW_plusOp_carry__1_CO_UNCONNECTED;
  wire [3:1]NLW_plusOp_carry__1_O_UNCONNECTED;

  FDCE #(
    .INIT(1'b0)) 
    \gdiff.gcry_1_sym.diff_pntr_pad_reg[1] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .CLR(AR),
        .D(plusOp_carry_n_7),
        .Q(\gdiff.gcry_1_sym.diff_pntr_pad_reg_n_0_[1] ));
  FDCE #(
    .INIT(1'b0)) 
    \gdiff.gcry_1_sym.diff_pntr_pad_reg[2] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .CLR(AR),
        .D(plusOp_carry_n_6),
        .Q(\gdiff.gcry_1_sym.diff_pntr_pad_reg_n_0_[2] ));
  FDCE #(
    .INIT(1'b0)) 
    \gdiff.gcry_1_sym.diff_pntr_pad_reg[3] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .CLR(AR),
        .D(plusOp_carry_n_5),
        .Q(\gdiff.gcry_1_sym.diff_pntr_pad_reg_n_0_[3] ));
  FDCE #(
    .INIT(1'b0)) 
    \gdiff.gcry_1_sym.diff_pntr_pad_reg[4] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .CLR(AR),
        .D(plusOp_carry_n_4),
        .Q(\gdiff.gcry_1_sym.diff_pntr_pad_reg_n_0_[4] ));
  FDCE #(
    .INIT(1'b0)) 
    \gdiff.gcry_1_sym.diff_pntr_pad_reg[5] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .CLR(AR),
        .D(plusOp_carry__0_n_7),
        .Q(\gdiff.gcry_1_sym.diff_pntr_pad_reg_n_0_[5] ));
  FDCE #(
    .INIT(1'b0)) 
    \gdiff.gcry_1_sym.diff_pntr_pad_reg[6] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .CLR(AR),
        .D(plusOp_carry__0_n_6),
        .Q(\gdiff.gcry_1_sym.diff_pntr_pad_reg_n_0_[6] ));
  FDCE #(
    .INIT(1'b0)) 
    \gdiff.gcry_1_sym.diff_pntr_pad_reg[7] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .CLR(AR),
        .D(plusOp_carry__0_n_5),
        .Q(\gdiff.gcry_1_sym.diff_pntr_pad_reg_n_0_[7] ));
  FDCE #(
    .INIT(1'b0)) 
    \gdiff.gcry_1_sym.diff_pntr_pad_reg[8] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .CLR(AR),
        .D(plusOp_carry__0_n_4),
        .Q(\gdiff.gcry_1_sym.diff_pntr_pad_reg_n_0_[8] ));
  FDCE #(
    .INIT(1'b0)) 
    \gdiff.gcry_1_sym.diff_pntr_pad_reg[9] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .CLR(AR),
        .D(plusOp_carry__1_n_7),
        .Q(\gdiff.gcry_1_sym.diff_pntr_pad_reg_n_0_[9] ));
  LUT5 #(
    .INIT(32'h55150400)) 
    \gpfs.prog_full_i_i_1 
       (.I0(\grstd1.grst_full.grst_f.rst_d5_reg ),
        .I1(eqOp__7),
        .I2(ram_rd_en_i),
        .I3(ram_wr_en_i),
        .I4(p_7_out),
        .O(\gpfs.prog_full_i_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h0008)) 
    \gpfs.prog_full_i_i_2 
       (.I0(\gpfs.prog_full_i_i_3_n_0 ),
        .I1(\gdiff.gcry_1_sym.diff_pntr_pad_reg_n_0_[1] ),
        .I2(\gdiff.gcry_1_sym.diff_pntr_pad_reg_n_0_[2] ),
        .I3(\gdiff.gcry_1_sym.diff_pntr_pad_reg_n_0_[3] ),
        .O(eqOp__7));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \gpfs.prog_full_i_i_3 
       (.I0(\gdiff.gcry_1_sym.diff_pntr_pad_reg_n_0_[4] ),
        .I1(\gdiff.gcry_1_sym.diff_pntr_pad_reg_n_0_[5] ),
        .I2(\gdiff.gcry_1_sym.diff_pntr_pad_reg_n_0_[6] ),
        .I3(\gdiff.gcry_1_sym.diff_pntr_pad_reg_n_0_[7] ),
        .I4(\gdiff.gcry_1_sym.diff_pntr_pad_reg_n_0_[9] ),
        .I5(\gdiff.gcry_1_sym.diff_pntr_pad_reg_n_0_[8] ),
        .O(\gpfs.prog_full_i_i_3_n_0 ));
  FDPE #(
    .INIT(1'b1)) 
    \gpfs.prog_full_i_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(\gpfs.prog_full_i_i_1_n_0 ),
        .PRE(out),
        .Q(p_7_out));
  FDCE #(
    .INIT(1'b0)) 
    \greg.ram_rd_en_i_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .CLR(AR),
        .D(p_6_out),
        .Q(ram_rd_en_i));
  FDCE #(
    .INIT(1'b0)) 
    \greg.ram_wr_en_i_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .CLR(AR),
        .D(E),
        .Q(ram_wr_en_i));
  CARRY4 plusOp_carry
       (.CI(1'b0),
        .CO({plusOp_carry_n_0,plusOp_carry_n_1,plusOp_carry_n_2,plusOp_carry_n_3}),
        .CYINIT(ram_full_fb_i_reg),
        .DI(Q[3:0]),
        .O({plusOp_carry_n_4,plusOp_carry_n_5,plusOp_carry_n_6,plusOp_carry_n_7}),
        .S(S));
  CARRY4 plusOp_carry__0
       (.CI(plusOp_carry_n_0),
        .CO({plusOp_carry__0_n_0,plusOp_carry__0_n_1,plusOp_carry__0_n_2,plusOp_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI(Q[7:4]),
        .O({plusOp_carry__0_n_4,plusOp_carry__0_n_5,plusOp_carry__0_n_6,plusOp_carry__0_n_7}),
        .S(\gcc0.gc1.gsym.count_d1_reg[7] ));
  CARRY4 plusOp_carry__1
       (.CI(plusOp_carry__0_n_0),
        .CO(NLW_plusOp_carry__1_CO_UNCONNECTED[3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({NLW_plusOp_carry__1_O_UNCONNECTED[3:1],plusOp_carry__1_n_7}),
        .S({1'b0,1'b0,1'b0,\gcc0.gc1.gsym.count_d1_reg[8] }));
  LUT2 #(
    .INIT(4'h2)) 
    \sig_register_array[0][9]_i_2 
       (.I0(p_7_out),
        .I1(sig_txd_prog_full_d1),
        .O(sig_txd_pf_event__1));
endmodule

(* ORIG_REF_NAME = "fifo_generator_v13_0_5_wr_pf_ss" *) 
module axi_fifo_mm_s_0_fifo_generator_v13_0_5_wr_pf_ss_13
   (p_8_out,
    sig_rxd_pf_event__1,
    p_6_out,
    s_axi_aclk,
    AR,
    E,
    p_3_out,
    Q,
    S,
    \gcc0.gc0.count_reg[7] ,
    \gc0.count_d1_reg[8] ,
    out,
    sig_rxd_prog_full_d1,
    \grstd1.grst_full.grst_f.rst_d5_reg );
  output p_8_out;
  output sig_rxd_pf_event__1;
  input p_6_out;
  input s_axi_aclk;
  input [0:0]AR;
  input [0:0]E;
  input p_3_out;
  input [7:0]Q;
  input [3:0]S;
  input [3:0]\gcc0.gc0.count_reg[7] ;
  input [0:0]\gc0.count_d1_reg[8] ;
  input out;
  input sig_rxd_prog_full_d1;
  input \grstd1.grst_full.grst_f.rst_d5_reg ;

  wire [0:0]AR;
  wire [0:0]E;
  wire [7:0]Q;
  wire [3:0]S;
  wire eqOp__7;
  wire [0:0]\gc0.count_d1_reg[8] ;
  wire [3:0]\gcc0.gc0.count_reg[7] ;
  wire \gdiff.gcry_1_sym.diff_pntr_pad_reg_n_0_[1] ;
  wire \gdiff.gcry_1_sym.diff_pntr_pad_reg_n_0_[2] ;
  wire \gdiff.gcry_1_sym.diff_pntr_pad_reg_n_0_[3] ;
  wire \gdiff.gcry_1_sym.diff_pntr_pad_reg_n_0_[4] ;
  wire \gdiff.gcry_1_sym.diff_pntr_pad_reg_n_0_[5] ;
  wire \gdiff.gcry_1_sym.diff_pntr_pad_reg_n_0_[6] ;
  wire \gdiff.gcry_1_sym.diff_pntr_pad_reg_n_0_[7] ;
  wire \gdiff.gcry_1_sym.diff_pntr_pad_reg_n_0_[8] ;
  wire \gdiff.gcry_1_sym.diff_pntr_pad_reg_n_0_[9] ;
  wire \gpfs.prog_full_i_i_1__0_n_0 ;
  wire \gpfs.prog_full_i_i_3__0_n_0 ;
  wire \grstd1.grst_full.grst_f.rst_d5_reg ;
  wire out;
  wire p_3_out;
  wire p_6_out;
  wire p_8_out;
  wire plusOp_carry__0_n_0;
  wire plusOp_carry__0_n_1;
  wire plusOp_carry__0_n_2;
  wire plusOp_carry__0_n_3;
  wire plusOp_carry__0_n_4;
  wire plusOp_carry__0_n_5;
  wire plusOp_carry__0_n_6;
  wire plusOp_carry__0_n_7;
  wire plusOp_carry__1_n_7;
  wire plusOp_carry_n_0;
  wire plusOp_carry_n_1;
  wire plusOp_carry_n_2;
  wire plusOp_carry_n_3;
  wire plusOp_carry_n_4;
  wire plusOp_carry_n_5;
  wire plusOp_carry_n_6;
  wire plusOp_carry_n_7;
  wire ram_rd_en_i;
  wire ram_wr_en_i;
  wire s_axi_aclk;
  wire sig_rxd_pf_event__1;
  wire sig_rxd_prog_full_d1;
  wire [3:0]NLW_plusOp_carry__1_CO_UNCONNECTED;
  wire [3:1]NLW_plusOp_carry__1_O_UNCONNECTED;

  FDCE #(
    .INIT(1'b0)) 
    \gdiff.gcry_1_sym.diff_pntr_pad_reg[1] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .CLR(AR),
        .D(plusOp_carry_n_7),
        .Q(\gdiff.gcry_1_sym.diff_pntr_pad_reg_n_0_[1] ));
  FDCE #(
    .INIT(1'b0)) 
    \gdiff.gcry_1_sym.diff_pntr_pad_reg[2] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .CLR(AR),
        .D(plusOp_carry_n_6),
        .Q(\gdiff.gcry_1_sym.diff_pntr_pad_reg_n_0_[2] ));
  FDCE #(
    .INIT(1'b0)) 
    \gdiff.gcry_1_sym.diff_pntr_pad_reg[3] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .CLR(AR),
        .D(plusOp_carry_n_5),
        .Q(\gdiff.gcry_1_sym.diff_pntr_pad_reg_n_0_[3] ));
  FDCE #(
    .INIT(1'b0)) 
    \gdiff.gcry_1_sym.diff_pntr_pad_reg[4] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .CLR(AR),
        .D(plusOp_carry_n_4),
        .Q(\gdiff.gcry_1_sym.diff_pntr_pad_reg_n_0_[4] ));
  FDCE #(
    .INIT(1'b0)) 
    \gdiff.gcry_1_sym.diff_pntr_pad_reg[5] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .CLR(AR),
        .D(plusOp_carry__0_n_7),
        .Q(\gdiff.gcry_1_sym.diff_pntr_pad_reg_n_0_[5] ));
  FDCE #(
    .INIT(1'b0)) 
    \gdiff.gcry_1_sym.diff_pntr_pad_reg[6] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .CLR(AR),
        .D(plusOp_carry__0_n_6),
        .Q(\gdiff.gcry_1_sym.diff_pntr_pad_reg_n_0_[6] ));
  FDCE #(
    .INIT(1'b0)) 
    \gdiff.gcry_1_sym.diff_pntr_pad_reg[7] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .CLR(AR),
        .D(plusOp_carry__0_n_5),
        .Q(\gdiff.gcry_1_sym.diff_pntr_pad_reg_n_0_[7] ));
  FDCE #(
    .INIT(1'b0)) 
    \gdiff.gcry_1_sym.diff_pntr_pad_reg[8] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .CLR(AR),
        .D(plusOp_carry__0_n_4),
        .Q(\gdiff.gcry_1_sym.diff_pntr_pad_reg_n_0_[8] ));
  FDCE #(
    .INIT(1'b0)) 
    \gdiff.gcry_1_sym.diff_pntr_pad_reg[9] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .CLR(AR),
        .D(plusOp_carry__1_n_7),
        .Q(\gdiff.gcry_1_sym.diff_pntr_pad_reg_n_0_[9] ));
  LUT5 #(
    .INIT(32'h55150400)) 
    \gpfs.prog_full_i_i_1__0 
       (.I0(\grstd1.grst_full.grst_f.rst_d5_reg ),
        .I1(eqOp__7),
        .I2(ram_rd_en_i),
        .I3(ram_wr_en_i),
        .I4(p_8_out),
        .O(\gpfs.prog_full_i_i_1__0_n_0 ));
  LUT4 #(
    .INIT(16'h0008)) 
    \gpfs.prog_full_i_i_2__0 
       (.I0(\gpfs.prog_full_i_i_3__0_n_0 ),
        .I1(\gdiff.gcry_1_sym.diff_pntr_pad_reg_n_0_[1] ),
        .I2(\gdiff.gcry_1_sym.diff_pntr_pad_reg_n_0_[2] ),
        .I3(\gdiff.gcry_1_sym.diff_pntr_pad_reg_n_0_[3] ),
        .O(eqOp__7));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \gpfs.prog_full_i_i_3__0 
       (.I0(\gdiff.gcry_1_sym.diff_pntr_pad_reg_n_0_[4] ),
        .I1(\gdiff.gcry_1_sym.diff_pntr_pad_reg_n_0_[5] ),
        .I2(\gdiff.gcry_1_sym.diff_pntr_pad_reg_n_0_[6] ),
        .I3(\gdiff.gcry_1_sym.diff_pntr_pad_reg_n_0_[7] ),
        .I4(\gdiff.gcry_1_sym.diff_pntr_pad_reg_n_0_[9] ),
        .I5(\gdiff.gcry_1_sym.diff_pntr_pad_reg_n_0_[8] ),
        .O(\gpfs.prog_full_i_i_3__0_n_0 ));
  FDPE #(
    .INIT(1'b1)) 
    \gpfs.prog_full_i_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(\gpfs.prog_full_i_i_1__0_n_0 ),
        .PRE(out),
        .Q(p_8_out));
  FDCE #(
    .INIT(1'b0)) 
    \greg.ram_rd_en_i_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .CLR(AR),
        .D(p_6_out),
        .Q(ram_rd_en_i));
  FDCE #(
    .INIT(1'b0)) 
    \greg.ram_wr_en_i_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .CLR(AR),
        .D(E),
        .Q(ram_wr_en_i));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 plusOp_carry
       (.CI(1'b0),
        .CO({plusOp_carry_n_0,plusOp_carry_n_1,plusOp_carry_n_2,plusOp_carry_n_3}),
        .CYINIT(p_3_out),
        .DI(Q[3:0]),
        .O({plusOp_carry_n_4,plusOp_carry_n_5,plusOp_carry_n_6,plusOp_carry_n_7}),
        .S(S));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 plusOp_carry__0
       (.CI(plusOp_carry_n_0),
        .CO({plusOp_carry__0_n_0,plusOp_carry__0_n_1,plusOp_carry__0_n_2,plusOp_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI(Q[7:4]),
        .O({plusOp_carry__0_n_4,plusOp_carry__0_n_5,plusOp_carry__0_n_6,plusOp_carry__0_n_7}),
        .S(\gcc0.gc0.count_reg[7] ));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 plusOp_carry__1
       (.CI(plusOp_carry__0_n_0),
        .CO(NLW_plusOp_carry__1_CO_UNCONNECTED[3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({NLW_plusOp_carry__1_O_UNCONNECTED[3:1],plusOp_carry__1_n_7}),
        .S({1'b0,1'b0,1'b0,\gc0.count_d1_reg[8] }));
  LUT2 #(
    .INIT(4'h2)) 
    \sig_register_array[0][11]_i_2 
       (.I0(p_8_out),
        .I1(sig_rxd_prog_full_d1),
        .O(sig_rxd_pf_event__1));
endmodule

(* ORIG_REF_NAME = "fifo_generator_v13_0_5_wr_status_flags_ss" *) 
module axi_fifo_mm_s_0_fifo_generator_v13_0_5_wr_status_flags_ss
   (\gaxi_full_sm.w_ready_r_reg ,
    E,
    \gcc0.gc1.gsym.count_reg[0] ,
    \gdiff.gcry_1_sym.diff_pntr_pad_reg[4] ,
    rd_pntr_inv_pad,
    \gaxi_bid_gen.S_AXI_BID_reg[0] ,
    p_0_in__10,
    axis_wr_eop,
    \gc0.count_d1_reg[6] ,
    v1_reg_0,
    \gc0.count_d1_reg[6]_0 ,
    v1_reg_1,
    v1_reg,
    s_axi_aclk,
    out,
    txd_wr_en,
    DIADI,
    start_wr,
    axis_rd_en__0,
    \grstd1.grst_full.grst_f.rst_d5_reg ,
    p_6_out,
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[9] ,
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[5] ,
    p_8_out);
  output \gaxi_full_sm.w_ready_r_reg ;
  output [0:0]E;
  output [0:0]\gcc0.gc1.gsym.count_reg[0] ;
  output \gdiff.gcry_1_sym.diff_pntr_pad_reg[4] ;
  output [0:0]rd_pntr_inv_pad;
  output [0:0]\gaxi_bid_gen.S_AXI_BID_reg[0] ;
  output p_0_in__10;
  output axis_wr_eop;
  input [3:0]\gc0.count_d1_reg[6] ;
  input [0:0]v1_reg_0;
  input [3:0]\gc0.count_d1_reg[6]_0 ;
  input [0:0]v1_reg_1;
  input [4:0]v1_reg;
  input s_axi_aclk;
  input out;
  input txd_wr_en;
  input [0:0]DIADI;
  input start_wr;
  input axis_rd_en__0;
  input \grstd1.grst_full.grst_f.rst_d5_reg ;
  input p_6_out;
  input [7:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[9] ;
  input \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[5] ;
  input p_8_out;

  wire [0:0]DIADI;
  wire [0:0]E;
  wire axis_almost_full;
  wire axis_rd_en__0;
  wire axis_wr_eop;
  wire comp1;
  wire [0:0]\gaxi_bid_gen.S_AXI_BID_reg[0] ;
  wire \gaxi_full_sm.w_ready_r_reg ;
  wire \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[5] ;
  wire [7:0]\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[9] ;
  wire \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_i_6_n_0 ;
  wire [3:0]\gc0.count_d1_reg[6] ;
  wire [3:0]\gc0.count_d1_reg[6]_0 ;
  wire [0:0]\gcc0.gc1.gsym.count_reg[0] ;
  wire \gdiff.gcry_1_sym.diff_pntr_pad_reg[4] ;
  wire \grstd1.grst_full.grst_f.rst_d5_reg ;
  wire out;
  wire p_0_in;
  wire p_0_in__10;
  wire p_2_out;
  wire p_3_out;
  wire p_6_out;
  wire p_8_out;
  wire ram_full_comb;
  wire [0:0]rd_pntr_inv_pad;
  wire s_axi_aclk;
  wire start_wr;
  wire txd_wr_en;
  wire [4:0]v1_reg;
  wire [0:0]v1_reg_0;
  wire [0:0]v1_reg_1;

  axi_fifo_mm_s_0_fifo_generator_v13_0_5_compare c0
       (.comp1(comp1),
        .\gc0.count_d1_reg[6] (\gc0.count_d1_reg[6] ),
        .\grstd1.grst_full.grst_f.rst_d5_reg (\grstd1.grst_full.grst_f.rst_d5_reg ),
        .p_3_out(p_3_out),
        .p_6_out(p_6_out),
        .ram_full_comb(ram_full_comb),
        .sig_txd_sb_wr_en_reg(\gcc0.gc1.gsym.count_reg[0] ),
        .v1_reg_0(v1_reg_0));
  axi_fifo_mm_s_0_fifo_generator_v13_0_5_compare_0 c1
       (.axis_almost_full(axis_almost_full),
        .comp1(comp1),
        .\gc0.count_d1_reg[6] (\gc0.count_d1_reg[6]_0 ),
        .\grstd1.grst_full.grst_f.rst_d5_reg (\grstd1.grst_full.grst_f.rst_d5_reg ),
        .p_0_in(p_0_in),
        .p_2_out(p_2_out),
        .p_6_out(p_6_out),
        .sig_txd_sb_wr_en_reg(\gcc0.gc1.gsym.count_reg[0] ),
        .v1_reg_1(v1_reg_1));
  axi_fifo_mm_s_0_fifo_generator_v13_0_5_compare_1 \gaf.c2 
       (.p_0_in(p_0_in),
        .v1_reg(v1_reg));
  (* equivalent_register_removal = "no" *) 
  FDPE #(
    .INIT(1'b1)) 
    \gaf.gaf0.ram_afull_i_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(p_2_out),
        .PRE(out),
        .Q(axis_almost_full));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \gaxi_bid_gen.S_AXI_BID[3]_i_1 
       (.I0(\gaxi_full_sm.w_ready_r_reg ),
        .O(\gaxi_bid_gen.S_AXI_BID_reg[0] ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_i_3 
       (.I0(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_i_6_n_0 ),
        .I1(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[9] [0]),
        .I2(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[9] [6]),
        .I3(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[9] [7]),
        .I4(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[9] [5]),
        .I5(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[9] [4]),
        .O(p_0_in__10));
  LUT6 #(
    .INIT(64'hFFFEFFFEFFFFFFFE)) 
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_i_6 
       (.I0(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[5] ),
        .I1(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[9] [3]),
        .I2(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[9] [2]),
        .I3(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_cnt_reg[9] [1]),
        .I4(axis_almost_full),
        .I5(p_8_out),
        .O(\gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_pkt_read_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.axis_wr_eop_d1_i_1 
       (.I0(DIADI),
        .I1(start_wr),
        .I2(\gaxi_full_sm.w_ready_r_reg ),
        .O(axis_wr_eop));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT5 #(
    .INIT(32'hFF1F00E0)) 
    \gaxis_fifo.gaxisf.gaxis_pkt_fifo_cc.gdc_pkt.axis_dc_pkt_fifo[9]_i_1 
       (.I0(txd_wr_en),
        .I1(DIADI),
        .I2(start_wr),
        .I3(\gaxi_full_sm.w_ready_r_reg ),
        .I4(axis_rd_en__0),
        .O(E));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT5 #(
    .INIT(32'h000000E0)) 
    \greg.ram_wr_en_i_i_1 
       (.I0(txd_wr_en),
        .I1(DIADI),
        .I2(start_wr),
        .I3(\gaxi_full_sm.w_ready_r_reg ),
        .I4(p_3_out),
        .O(\gcc0.gc1.gsym.count_reg[0] ));
  LUT6 #(
    .INIT(64'h0000000010101000)) 
    plusOp_carry_i_1
       (.I0(p_3_out),
        .I1(\gaxi_full_sm.w_ready_r_reg ),
        .I2(start_wr),
        .I3(DIADI),
        .I4(txd_wr_en),
        .I5(p_6_out),
        .O(\gdiff.gcry_1_sym.diff_pntr_pad_reg[4] ));
  LUT6 #(
    .INIT(64'h10101000FFFFFFFF)) 
    plusOp_carry_i_1__0
       (.I0(p_3_out),
        .I1(\gaxi_full_sm.w_ready_r_reg ),
        .I2(start_wr),
        .I3(DIADI),
        .I4(txd_wr_en),
        .I5(p_6_out),
        .O(rd_pntr_inv_pad));
  (* equivalent_register_removal = "no" *) 
  FDPE #(
    .INIT(1'b1)) 
    ram_full_fb_i_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(ram_full_comb),
        .PRE(out),
        .Q(p_3_out));
  (* equivalent_register_removal = "no" *) 
  FDPE #(
    .INIT(1'b1)) 
    ram_full_i_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(ram_full_comb),
        .PRE(out),
        .Q(\gaxi_full_sm.w_ready_r_reg ));
endmodule

(* ORIG_REF_NAME = "fifo_generator_v13_0_5_wr_status_flags_ss" *) 
module axi_fifo_mm_s_0_fifo_generator_v13_0_5_wr_status_flags_ss__parameterized0
   (\grxd.fg_rxd_wr_length_reg[1] ,
    \count_reg[9] ,
    rd_pntr_inv_pad,
    E,
    p_3_out,
    \grxd.fg_rxd_wr_length_reg[21] ,
    rx_complete,
    rx_str_wr_en,
    axi_str_rxd_tready,
    \grxd.fg_rxd_wr_length_reg[2] ,
    \gc0.count_d1_reg[6] ,
    v1_reg_0,
    v1_reg,
    s_axi_aclk,
    out,
    valid_fwft,
    axis_rd_en__0,
    axi_str_rxd_tvalid,
    p_6_out,
    \grstd1.grst_full.grst_f.rst_d5_reg ,
    axi_str_rxd_tlast,
    rx_len_wr_en,
    s_axi_aresetn,
    sig_str_rst_reg,
    O,
    \grxd.fg_rxd_wr_length_reg[2]_0 ,
    fg_rxd_wr_length);
  output \grxd.fg_rxd_wr_length_reg[1] ;
  output [0:0]\count_reg[9] ;
  output [0:0]rd_pntr_inv_pad;
  output [0:0]E;
  output p_3_out;
  output \grxd.fg_rxd_wr_length_reg[21] ;
  output rx_complete;
  output rx_str_wr_en;
  output axi_str_rxd_tready;
  output \grxd.fg_rxd_wr_length_reg[2] ;
  input [3:0]\gc0.count_d1_reg[6] ;
  input [0:0]v1_reg_0;
  input [4:0]v1_reg;
  input s_axi_aclk;
  input out;
  input valid_fwft;
  input axis_rd_en__0;
  input axi_str_rxd_tvalid;
  input p_6_out;
  input \grstd1.grst_full.grst_f.rst_d5_reg ;
  input axi_str_rxd_tlast;
  input rx_len_wr_en;
  input s_axi_aresetn;
  input sig_str_rst_reg;
  input [0:0]O;
  input [0:0]\grxd.fg_rxd_wr_length_reg[2]_0 ;
  input [0:0]fg_rxd_wr_length;

  wire [0:0]E;
  wire [0:0]O;
  wire axi_str_rxd_tlast;
  wire axi_str_rxd_tready;
  wire axi_str_rxd_tvalid;
  wire axis_rd_en__0;
  wire comp0;
  wire [0:0]\count_reg[9] ;
  wire [0:0]fg_rxd_wr_length;
  wire [3:0]\gc0.count_d1_reg[6] ;
  wire \grstd1.grst_full.grst_f.rst_d5_reg ;
  wire \grxd.fg_rxd_wr_length_reg[1] ;
  wire \grxd.fg_rxd_wr_length_reg[21] ;
  wire \grxd.fg_rxd_wr_length_reg[2] ;
  wire [0:0]\grxd.fg_rxd_wr_length_reg[2]_0 ;
  wire out;
  wire p_2_out;
  wire p_3_out;
  wire p_6_out;
  wire ram_full_comb;
  wire [0:0]rd_pntr_inv_pad;
  wire rx_complete;
  wire rx_len_wr_en;
  wire rx_str_wr_en;
  wire s_axi_aclk;
  wire s_axi_aresetn;
  wire sig_str_rst_reg;
  wire [4:0]v1_reg;
  wire [0:0]v1_reg_0;
  wire valid_fwft;

  LUT1 #(
    .INIT(2'h1)) 
    axi_str_rxd_tready_INST_0
       (.I0(\grxd.fg_rxd_wr_length_reg[1] ),
        .O(axi_str_rxd_tready));
  axi_fifo_mm_s_0_fifo_generator_v13_0_5_compare_14 c0
       (.comp0(comp0),
        .\gc0.count_d1_reg[6] (\gc0.count_d1_reg[6] ),
        .v1_reg_0(v1_reg_0));
  axi_fifo_mm_s_0_fifo_generator_v13_0_5_compare_15 c1
       (.E(E),
        .comp0(comp0),
        .\grstd1.grst_full.grst_f.rst_d5_reg (\grstd1.grst_full.grst_f.rst_d5_reg ),
        .p_2_out(p_2_out),
        .p_6_out(p_6_out),
        .ram_full_comb(ram_full_comb),
        .v1_reg(v1_reg));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT5 #(
    .INIT(32'h88888878)) 
    \count[9]_i_1 
       (.I0(valid_fwft),
        .I1(axis_rd_en__0),
        .I2(axi_str_rxd_tvalid),
        .I3(\grxd.fg_rxd_wr_length_reg[1] ),
        .I4(p_2_out),
        .O(\count_reg[9] ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'h10)) 
    \greg.ram_wr_en_i_i_1__0 
       (.I0(p_2_out),
        .I1(\grxd.fg_rxd_wr_length_reg[1] ),
        .I2(axi_str_rxd_tvalid),
        .O(E));
  LUT6 #(
    .INIT(64'hFFFFFFFFBF00FFFF)) 
    \grxd.fg_rxd_wr_length[21]_i_1 
       (.I0(\grxd.fg_rxd_wr_length_reg[1] ),
        .I1(axi_str_rxd_tvalid),
        .I2(axi_str_rxd_tlast),
        .I3(rx_len_wr_en),
        .I4(s_axi_aresetn),
        .I5(sig_str_rst_reg),
        .O(\grxd.fg_rxd_wr_length_reg[21] ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \grxd.fg_rxd_wr_length[21]_i_2 
       (.I0(axi_str_rxd_tvalid),
        .I1(\grxd.fg_rxd_wr_length_reg[1] ),
        .O(rx_str_wr_en));
  LUT6 #(
    .INIT(64'hBB00B8FFBB00B800)) 
    \grxd.fg_rxd_wr_length[2]_i_1 
       (.I0(O),
        .I1(axi_str_rxd_tlast),
        .I2(\grxd.fg_rxd_wr_length_reg[2]_0 ),
        .I3(rx_str_wr_en),
        .I4(rx_len_wr_en),
        .I5(fg_rxd_wr_length),
        .O(\grxd.fg_rxd_wr_length_reg[2] ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \grxd.rx_len_wr_en_i_1 
       (.I0(\grxd.fg_rxd_wr_length_reg[1] ),
        .I1(axi_str_rxd_tvalid),
        .I2(axi_str_rxd_tlast),
        .O(rx_complete));
  LUT4 #(
    .INIT(16'h02FF)) 
    plusOp_carry_i_1__1
       (.I0(axi_str_rxd_tvalid),
        .I1(\grxd.fg_rxd_wr_length_reg[1] ),
        .I2(p_2_out),
        .I3(p_6_out),
        .O(rd_pntr_inv_pad));
  LUT4 #(
    .INIT(16'h0002)) 
    plusOp_carry_i_1__2
       (.I0(axi_str_rxd_tvalid),
        .I1(\grxd.fg_rxd_wr_length_reg[1] ),
        .I2(p_2_out),
        .I3(p_6_out),
        .O(p_3_out));
  (* equivalent_register_removal = "no" *) 
  FDPE #(
    .INIT(1'b1)) 
    ram_full_fb_i_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(ram_full_comb),
        .PRE(out),
        .Q(p_2_out));
  (* equivalent_register_removal = "no" *) 
  FDPE #(
    .INIT(1'b1)) 
    ram_full_i_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(ram_full_comb),
        .PRE(out),
        .Q(\grxd.fg_rxd_wr_length_reg[1] ));
endmodule

(* ORIG_REF_NAME = "fifo_generator_v13_1_3" *) 
module axi_fifo_mm_s_0_fifo_generator_v13_1_3
   (out,
    \gcc0.gc0.count_d1_reg[6] ,
    \gpr1.dout_i_reg[0] ,
    \gpr1.dout_i_reg[0]_0 ,
    \gpr1.dout_i_reg[1] ,
    \gpr1.dout_i_reg[2] ,
    \gpr1.dout_i_reg[0]_1 ,
    \gpr1.dout_i_reg[1]_0 ,
    \gpr1.dout_i_reg[2]_0 ,
    \gpr1.dout_i_reg[3] ,
    \gpr1.dout_i_reg[4] ,
    \gpr1.dout_i_reg[5] ,
    \gpr1.dout_i_reg[3]_0 ,
    \gpr1.dout_i_reg[4]_0 ,
    \gpr1.dout_i_reg[5]_0 ,
    \gpr1.dout_i_reg[6] ,
    \gpr1.dout_i_reg[7] ,
    \gpr1.dout_i_reg[8] ,
    \gpr1.dout_i_reg[6]_0 ,
    \gpr1.dout_i_reg[7]_0 ,
    \gpr1.dout_i_reg[8]_0 ,
    \gpr1.dout_i_reg[9] ,
    \gpr1.dout_i_reg[10] ,
    \gpr1.dout_i_reg[11] ,
    \gpr1.dout_i_reg[9]_0 ,
    \gpr1.dout_i_reg[10]_0 ,
    \gpr1.dout_i_reg[11]_0 ,
    \gpr1.dout_i_reg[12] ,
    \gpr1.dout_i_reg[13] ,
    \gpr1.dout_i_reg[14] ,
    \gpr1.dout_i_reg[12]_0 ,
    \gpr1.dout_i_reg[13]_0 ,
    \gpr1.dout_i_reg[14]_0 ,
    \gpr1.dout_i_reg[15] ,
    \gpr1.dout_i_reg[16] ,
    \gpr1.dout_i_reg[17] ,
    \gpr1.dout_i_reg[15]_0 ,
    \gpr1.dout_i_reg[16]_0 ,
    \gpr1.dout_i_reg[17]_0 ,
    \gpr1.dout_i_reg[18] ,
    \gpr1.dout_i_reg[19] ,
    \gpr1.dout_i_reg[20] ,
    \gpr1.dout_i_reg[18]_0 ,
    \gpr1.dout_i_reg[19]_0 ,
    \gpr1.dout_i_reg[20]_0 ,
    \gpr1.dout_i_reg[21] ,
    \gpr1.dout_i_reg[21]_0 ,
    p_13_in,
    Q,
    \gpr1.dout_i_reg[21]_1 ,
    \sig_ip2bus_data_reg[10] ,
    s_axi_aclk,
    fg_rxd_wr_length,
    ram_full_fb_i_reg,
    rx_fg_len_empty_d1,
    ram_full_i_reg,
    axi_str_rxd_tvalid,
    axi_str_rxd_tlast,
    rx_len_wr_en,
    sig_rd_rlen_reg,
    s_axi_aresetn,
    sig_str_rst_reg,
    sig_rx_channel_reset_reg,
    D);
  output out;
  output \gcc0.gc0.count_d1_reg[6] ;
  output \gpr1.dout_i_reg[0] ;
  output \gpr1.dout_i_reg[0]_0 ;
  output \gpr1.dout_i_reg[1] ;
  output \gpr1.dout_i_reg[2] ;
  output \gpr1.dout_i_reg[0]_1 ;
  output \gpr1.dout_i_reg[1]_0 ;
  output \gpr1.dout_i_reg[2]_0 ;
  output \gpr1.dout_i_reg[3] ;
  output \gpr1.dout_i_reg[4] ;
  output \gpr1.dout_i_reg[5] ;
  output \gpr1.dout_i_reg[3]_0 ;
  output \gpr1.dout_i_reg[4]_0 ;
  output \gpr1.dout_i_reg[5]_0 ;
  output \gpr1.dout_i_reg[6] ;
  output \gpr1.dout_i_reg[7] ;
  output \gpr1.dout_i_reg[8] ;
  output \gpr1.dout_i_reg[6]_0 ;
  output \gpr1.dout_i_reg[7]_0 ;
  output \gpr1.dout_i_reg[8]_0 ;
  output \gpr1.dout_i_reg[9] ;
  output \gpr1.dout_i_reg[10] ;
  output \gpr1.dout_i_reg[11] ;
  output \gpr1.dout_i_reg[9]_0 ;
  output \gpr1.dout_i_reg[10]_0 ;
  output \gpr1.dout_i_reg[11]_0 ;
  output \gpr1.dout_i_reg[12] ;
  output \gpr1.dout_i_reg[13] ;
  output \gpr1.dout_i_reg[14] ;
  output \gpr1.dout_i_reg[12]_0 ;
  output \gpr1.dout_i_reg[13]_0 ;
  output \gpr1.dout_i_reg[14]_0 ;
  output \gpr1.dout_i_reg[15] ;
  output \gpr1.dout_i_reg[16] ;
  output \gpr1.dout_i_reg[17] ;
  output \gpr1.dout_i_reg[15]_0 ;
  output \gpr1.dout_i_reg[16]_0 ;
  output \gpr1.dout_i_reg[17]_0 ;
  output \gpr1.dout_i_reg[18] ;
  output \gpr1.dout_i_reg[19] ;
  output \gpr1.dout_i_reg[20] ;
  output \gpr1.dout_i_reg[18]_0 ;
  output \gpr1.dout_i_reg[19]_0 ;
  output \gpr1.dout_i_reg[20]_0 ;
  output \gpr1.dout_i_reg[21] ;
  output \gpr1.dout_i_reg[21]_0 ;
  output p_13_in;
  output [0:0]Q;
  output [0:0]\gpr1.dout_i_reg[21]_1 ;
  output [21:0]\sig_ip2bus_data_reg[10] ;
  input s_axi_aclk;
  input [20:0]fg_rxd_wr_length;
  input ram_full_fb_i_reg;
  input rx_fg_len_empty_d1;
  input ram_full_i_reg;
  input axi_str_rxd_tvalid;
  input axi_str_rxd_tlast;
  input rx_len_wr_en;
  input sig_rd_rlen_reg;
  input s_axi_aresetn;
  input sig_str_rst_reg;
  input sig_rx_channel_reset_reg;
  input [21:0]D;

  wire [21:0]D;
  wire [0:0]Q;
  wire axi_str_rxd_tlast;
  wire axi_str_rxd_tvalid;
  wire [20:0]fg_rxd_wr_length;
  wire \gcc0.gc0.count_d1_reg[6] ;
  wire \gpr1.dout_i_reg[0] ;
  wire \gpr1.dout_i_reg[0]_0 ;
  wire \gpr1.dout_i_reg[0]_1 ;
  wire \gpr1.dout_i_reg[10] ;
  wire \gpr1.dout_i_reg[10]_0 ;
  wire \gpr1.dout_i_reg[11] ;
  wire \gpr1.dout_i_reg[11]_0 ;
  wire \gpr1.dout_i_reg[12] ;
  wire \gpr1.dout_i_reg[12]_0 ;
  wire \gpr1.dout_i_reg[13] ;
  wire \gpr1.dout_i_reg[13]_0 ;
  wire \gpr1.dout_i_reg[14] ;
  wire \gpr1.dout_i_reg[14]_0 ;
  wire \gpr1.dout_i_reg[15] ;
  wire \gpr1.dout_i_reg[15]_0 ;
  wire \gpr1.dout_i_reg[16] ;
  wire \gpr1.dout_i_reg[16]_0 ;
  wire \gpr1.dout_i_reg[17] ;
  wire \gpr1.dout_i_reg[17]_0 ;
  wire \gpr1.dout_i_reg[18] ;
  wire \gpr1.dout_i_reg[18]_0 ;
  wire \gpr1.dout_i_reg[19] ;
  wire \gpr1.dout_i_reg[19]_0 ;
  wire \gpr1.dout_i_reg[1] ;
  wire \gpr1.dout_i_reg[1]_0 ;
  wire \gpr1.dout_i_reg[20] ;
  wire \gpr1.dout_i_reg[20]_0 ;
  wire \gpr1.dout_i_reg[21] ;
  wire \gpr1.dout_i_reg[21]_0 ;
  wire [0:0]\gpr1.dout_i_reg[21]_1 ;
  wire \gpr1.dout_i_reg[2] ;
  wire \gpr1.dout_i_reg[2]_0 ;
  wire \gpr1.dout_i_reg[3] ;
  wire \gpr1.dout_i_reg[3]_0 ;
  wire \gpr1.dout_i_reg[4] ;
  wire \gpr1.dout_i_reg[4]_0 ;
  wire \gpr1.dout_i_reg[5] ;
  wire \gpr1.dout_i_reg[5]_0 ;
  wire \gpr1.dout_i_reg[6] ;
  wire \gpr1.dout_i_reg[6]_0 ;
  wire \gpr1.dout_i_reg[7] ;
  wire \gpr1.dout_i_reg[7]_0 ;
  wire \gpr1.dout_i_reg[8] ;
  wire \gpr1.dout_i_reg[8]_0 ;
  wire \gpr1.dout_i_reg[9] ;
  wire \gpr1.dout_i_reg[9]_0 ;
  wire out;
  wire p_13_in;
  wire ram_full_fb_i_reg;
  wire ram_full_i_reg;
  wire rx_fg_len_empty_d1;
  wire rx_len_wr_en;
  wire s_axi_aclk;
  wire s_axi_aresetn;
  wire [21:0]\sig_ip2bus_data_reg[10] ;
  wire sig_rd_rlen_reg;
  wire sig_rx_channel_reset_reg;
  wire sig_str_rst_reg;

  axi_fifo_mm_s_0_fifo_generator_v13_1_3_synth inst_fifo_gen
       (.D(D),
        .Q(Q),
        .SR(\gpr1.dout_i_reg[0] ),
        .axi_str_rxd_tlast(axi_str_rxd_tlast),
        .axi_str_rxd_tvalid(axi_str_rxd_tvalid),
        .fg_rxd_wr_length(fg_rxd_wr_length),
        .\gcc0.gc0.count_d1_reg[6] (\gcc0.gc0.count_d1_reg[6] ),
        .\gpr1.dout_i_reg[0] (\gpr1.dout_i_reg[0]_0 ),
        .\gpr1.dout_i_reg[0]_0 (\gpr1.dout_i_reg[0]_1 ),
        .\gpr1.dout_i_reg[10] (\gpr1.dout_i_reg[10] ),
        .\gpr1.dout_i_reg[10]_0 (\gpr1.dout_i_reg[10]_0 ),
        .\gpr1.dout_i_reg[11] (\gpr1.dout_i_reg[11] ),
        .\gpr1.dout_i_reg[11]_0 (\gpr1.dout_i_reg[11]_0 ),
        .\gpr1.dout_i_reg[12] (\gpr1.dout_i_reg[12] ),
        .\gpr1.dout_i_reg[12]_0 (\gpr1.dout_i_reg[12]_0 ),
        .\gpr1.dout_i_reg[13] (\gpr1.dout_i_reg[13] ),
        .\gpr1.dout_i_reg[13]_0 (\gpr1.dout_i_reg[13]_0 ),
        .\gpr1.dout_i_reg[14] (\gpr1.dout_i_reg[14] ),
        .\gpr1.dout_i_reg[14]_0 (\gpr1.dout_i_reg[14]_0 ),
        .\gpr1.dout_i_reg[15] (\gpr1.dout_i_reg[15] ),
        .\gpr1.dout_i_reg[15]_0 (\gpr1.dout_i_reg[15]_0 ),
        .\gpr1.dout_i_reg[16] (\gpr1.dout_i_reg[16] ),
        .\gpr1.dout_i_reg[16]_0 (\gpr1.dout_i_reg[16]_0 ),
        .\gpr1.dout_i_reg[17] (\gpr1.dout_i_reg[17] ),
        .\gpr1.dout_i_reg[17]_0 (\gpr1.dout_i_reg[17]_0 ),
        .\gpr1.dout_i_reg[18] (\gpr1.dout_i_reg[18] ),
        .\gpr1.dout_i_reg[18]_0 (\gpr1.dout_i_reg[18]_0 ),
        .\gpr1.dout_i_reg[19] (\gpr1.dout_i_reg[19] ),
        .\gpr1.dout_i_reg[19]_0 (\gpr1.dout_i_reg[19]_0 ),
        .\gpr1.dout_i_reg[1] (\gpr1.dout_i_reg[1] ),
        .\gpr1.dout_i_reg[1]_0 (\gpr1.dout_i_reg[1]_0 ),
        .\gpr1.dout_i_reg[20] (\gpr1.dout_i_reg[20] ),
        .\gpr1.dout_i_reg[20]_0 (\gpr1.dout_i_reg[20]_0 ),
        .\gpr1.dout_i_reg[21] (\gpr1.dout_i_reg[21] ),
        .\gpr1.dout_i_reg[21]_0 (\gpr1.dout_i_reg[21]_0 ),
        .\gpr1.dout_i_reg[21]_1 (\gpr1.dout_i_reg[21]_1 ),
        .\gpr1.dout_i_reg[2] (\gpr1.dout_i_reg[2] ),
        .\gpr1.dout_i_reg[2]_0 (\gpr1.dout_i_reg[2]_0 ),
        .\gpr1.dout_i_reg[3] (\gpr1.dout_i_reg[3] ),
        .\gpr1.dout_i_reg[3]_0 (\gpr1.dout_i_reg[3]_0 ),
        .\gpr1.dout_i_reg[4] (\gpr1.dout_i_reg[4] ),
        .\gpr1.dout_i_reg[4]_0 (\gpr1.dout_i_reg[4]_0 ),
        .\gpr1.dout_i_reg[5] (\gpr1.dout_i_reg[5] ),
        .\gpr1.dout_i_reg[5]_0 (\gpr1.dout_i_reg[5]_0 ),
        .\gpr1.dout_i_reg[6] (\gpr1.dout_i_reg[6] ),
        .\gpr1.dout_i_reg[6]_0 (\gpr1.dout_i_reg[6]_0 ),
        .\gpr1.dout_i_reg[7] (\gpr1.dout_i_reg[7] ),
        .\gpr1.dout_i_reg[7]_0 (\gpr1.dout_i_reg[7]_0 ),
        .\gpr1.dout_i_reg[8] (\gpr1.dout_i_reg[8] ),
        .\gpr1.dout_i_reg[8]_0 (\gpr1.dout_i_reg[8]_0 ),
        .\gpr1.dout_i_reg[9] (\gpr1.dout_i_reg[9] ),
        .\gpr1.dout_i_reg[9]_0 (\gpr1.dout_i_reg[9]_0 ),
        .out(out),
        .p_13_in(p_13_in),
        .ram_full_fb_i_reg(ram_full_fb_i_reg),
        .ram_full_i_reg(ram_full_i_reg),
        .rx_fg_len_empty_d1(rx_fg_len_empty_d1),
        .rx_len_wr_en(rx_len_wr_en),
        .s_axi_aclk(s_axi_aclk),
        .s_axi_aresetn(s_axi_aresetn),
        .\sig_ip2bus_data_reg[10] (\sig_ip2bus_data_reg[10] ),
        .sig_rd_rlen_reg(sig_rd_rlen_reg),
        .sig_rx_channel_reset_reg(sig_rx_channel_reset_reg),
        .sig_str_rst_reg(sig_str_rst_reg));
endmodule

(* ORIG_REF_NAME = "fifo_generator_v13_1_3_dmem" *) 
module axi_fifo_mm_s_0_fifo_generator_v13_1_3_dmem
   (\gpr1.dout_i_reg[0]_0 ,
    \gpr1.dout_i_reg[1]_0 ,
    \gpr1.dout_i_reg[2]_0 ,
    \gpr1.dout_i_reg[0]_1 ,
    \gpr1.dout_i_reg[1]_1 ,
    \gpr1.dout_i_reg[2]_1 ,
    \gpr1.dout_i_reg[3]_0 ,
    \gpr1.dout_i_reg[4]_0 ,
    \gpr1.dout_i_reg[5]_0 ,
    \gpr1.dout_i_reg[3]_1 ,
    \gpr1.dout_i_reg[4]_1 ,
    \gpr1.dout_i_reg[5]_1 ,
    \gpr1.dout_i_reg[6]_0 ,
    \gpr1.dout_i_reg[7]_0 ,
    \gpr1.dout_i_reg[8]_0 ,
    \gpr1.dout_i_reg[6]_1 ,
    \gpr1.dout_i_reg[7]_1 ,
    \gpr1.dout_i_reg[8]_1 ,
    \gpr1.dout_i_reg[9]_0 ,
    \gpr1.dout_i_reg[10]_0 ,
    \gpr1.dout_i_reg[11]_0 ,
    \gpr1.dout_i_reg[9]_1 ,
    \gpr1.dout_i_reg[10]_1 ,
    \gpr1.dout_i_reg[11]_1 ,
    \gpr1.dout_i_reg[12]_0 ,
    \gpr1.dout_i_reg[13]_0 ,
    \gpr1.dout_i_reg[14]_0 ,
    \gpr1.dout_i_reg[12]_1 ,
    \gpr1.dout_i_reg[13]_1 ,
    \gpr1.dout_i_reg[14]_1 ,
    \gpr1.dout_i_reg[15]_0 ,
    \gpr1.dout_i_reg[16]_0 ,
    \gpr1.dout_i_reg[17]_0 ,
    \gpr1.dout_i_reg[15]_1 ,
    \gpr1.dout_i_reg[16]_1 ,
    \gpr1.dout_i_reg[17]_1 ,
    \gpr1.dout_i_reg[18]_0 ,
    \gpr1.dout_i_reg[19]_0 ,
    \gpr1.dout_i_reg[20]_0 ,
    \gpr1.dout_i_reg[18]_1 ,
    \gpr1.dout_i_reg[19]_1 ,
    \gpr1.dout_i_reg[20]_1 ,
    \gpr1.dout_i_reg[21]_0 ,
    \gpr1.dout_i_reg[21]_1 ,
    SR,
    \goreg_dm.dout_i_reg[21] ,
    s_axi_aclk,
    fg_rxd_wr_length,
    ram_full_fb_i_reg,
    \gc1.count_d2_reg[5] ,
    Q,
    ram_full_fb_i_reg_0,
    s_axi_aresetn,
    sig_str_rst_reg,
    sig_rx_channel_reset_reg,
    E,
    D);
  output \gpr1.dout_i_reg[0]_0 ;
  output \gpr1.dout_i_reg[1]_0 ;
  output \gpr1.dout_i_reg[2]_0 ;
  output \gpr1.dout_i_reg[0]_1 ;
  output \gpr1.dout_i_reg[1]_1 ;
  output \gpr1.dout_i_reg[2]_1 ;
  output \gpr1.dout_i_reg[3]_0 ;
  output \gpr1.dout_i_reg[4]_0 ;
  output \gpr1.dout_i_reg[5]_0 ;
  output \gpr1.dout_i_reg[3]_1 ;
  output \gpr1.dout_i_reg[4]_1 ;
  output \gpr1.dout_i_reg[5]_1 ;
  output \gpr1.dout_i_reg[6]_0 ;
  output \gpr1.dout_i_reg[7]_0 ;
  output \gpr1.dout_i_reg[8]_0 ;
  output \gpr1.dout_i_reg[6]_1 ;
  output \gpr1.dout_i_reg[7]_1 ;
  output \gpr1.dout_i_reg[8]_1 ;
  output \gpr1.dout_i_reg[9]_0 ;
  output \gpr1.dout_i_reg[10]_0 ;
  output \gpr1.dout_i_reg[11]_0 ;
  output \gpr1.dout_i_reg[9]_1 ;
  output \gpr1.dout_i_reg[10]_1 ;
  output \gpr1.dout_i_reg[11]_1 ;
  output \gpr1.dout_i_reg[12]_0 ;
  output \gpr1.dout_i_reg[13]_0 ;
  output \gpr1.dout_i_reg[14]_0 ;
  output \gpr1.dout_i_reg[12]_1 ;
  output \gpr1.dout_i_reg[13]_1 ;
  output \gpr1.dout_i_reg[14]_1 ;
  output \gpr1.dout_i_reg[15]_0 ;
  output \gpr1.dout_i_reg[16]_0 ;
  output \gpr1.dout_i_reg[17]_0 ;
  output \gpr1.dout_i_reg[15]_1 ;
  output \gpr1.dout_i_reg[16]_1 ;
  output \gpr1.dout_i_reg[17]_1 ;
  output \gpr1.dout_i_reg[18]_0 ;
  output \gpr1.dout_i_reg[19]_0 ;
  output \gpr1.dout_i_reg[20]_0 ;
  output \gpr1.dout_i_reg[18]_1 ;
  output \gpr1.dout_i_reg[19]_1 ;
  output \gpr1.dout_i_reg[20]_1 ;
  output \gpr1.dout_i_reg[21]_0 ;
  output \gpr1.dout_i_reg[21]_1 ;
  output [0:0]SR;
  output [21:0]\goreg_dm.dout_i_reg[21] ;
  input s_axi_aclk;
  input [20:0]fg_rxd_wr_length;
  input ram_full_fb_i_reg;
  input [5:0]\gc1.count_d2_reg[5] ;
  input [5:0]Q;
  input ram_full_fb_i_reg_0;
  input s_axi_aresetn;
  input sig_str_rst_reg;
  input sig_rx_channel_reset_reg;
  input [0:0]E;
  input [21:0]D;

  wire [21:0]D;
  wire [0:0]E;
  wire [5:0]Q;
  wire [0:0]SR;
  wire [20:0]fg_rxd_wr_length;
  wire [5:0]\gc1.count_d2_reg[5] ;
  wire [21:0]\goreg_dm.dout_i_reg[21] ;
  wire \gpr1.dout_i_reg[0]_0 ;
  wire \gpr1.dout_i_reg[0]_1 ;
  wire \gpr1.dout_i_reg[10]_0 ;
  wire \gpr1.dout_i_reg[10]_1 ;
  wire \gpr1.dout_i_reg[11]_0 ;
  wire \gpr1.dout_i_reg[11]_1 ;
  wire \gpr1.dout_i_reg[12]_0 ;
  wire \gpr1.dout_i_reg[12]_1 ;
  wire \gpr1.dout_i_reg[13]_0 ;
  wire \gpr1.dout_i_reg[13]_1 ;
  wire \gpr1.dout_i_reg[14]_0 ;
  wire \gpr1.dout_i_reg[14]_1 ;
  wire \gpr1.dout_i_reg[15]_0 ;
  wire \gpr1.dout_i_reg[15]_1 ;
  wire \gpr1.dout_i_reg[16]_0 ;
  wire \gpr1.dout_i_reg[16]_1 ;
  wire \gpr1.dout_i_reg[17]_0 ;
  wire \gpr1.dout_i_reg[17]_1 ;
  wire \gpr1.dout_i_reg[18]_0 ;
  wire \gpr1.dout_i_reg[18]_1 ;
  wire \gpr1.dout_i_reg[19]_0 ;
  wire \gpr1.dout_i_reg[19]_1 ;
  wire \gpr1.dout_i_reg[1]_0 ;
  wire \gpr1.dout_i_reg[1]_1 ;
  wire \gpr1.dout_i_reg[20]_0 ;
  wire \gpr1.dout_i_reg[20]_1 ;
  wire \gpr1.dout_i_reg[21]_0 ;
  wire \gpr1.dout_i_reg[21]_1 ;
  wire \gpr1.dout_i_reg[2]_0 ;
  wire \gpr1.dout_i_reg[2]_1 ;
  wire \gpr1.dout_i_reg[3]_0 ;
  wire \gpr1.dout_i_reg[3]_1 ;
  wire \gpr1.dout_i_reg[4]_0 ;
  wire \gpr1.dout_i_reg[4]_1 ;
  wire \gpr1.dout_i_reg[5]_0 ;
  wire \gpr1.dout_i_reg[5]_1 ;
  wire \gpr1.dout_i_reg[6]_0 ;
  wire \gpr1.dout_i_reg[6]_1 ;
  wire \gpr1.dout_i_reg[7]_0 ;
  wire \gpr1.dout_i_reg[7]_1 ;
  wire \gpr1.dout_i_reg[8]_0 ;
  wire \gpr1.dout_i_reg[8]_1 ;
  wire \gpr1.dout_i_reg[9]_0 ;
  wire \gpr1.dout_i_reg[9]_1 ;
  wire ram_full_fb_i_reg;
  wire ram_full_fb_i_reg_0;
  wire s_axi_aclk;
  wire s_axi_aresetn;
  wire sig_rx_channel_reset_reg;
  wire sig_str_rst_reg;
  wire NLW_RAM_reg_0_63_0_2_DOD_UNCONNECTED;
  wire NLW_RAM_reg_0_63_12_14_DOD_UNCONNECTED;
  wire NLW_RAM_reg_0_63_15_17_DOD_UNCONNECTED;
  wire NLW_RAM_reg_0_63_18_20_DOD_UNCONNECTED;
  wire NLW_RAM_reg_0_63_21_21_SPO_UNCONNECTED;
  wire NLW_RAM_reg_0_63_3_5_DOD_UNCONNECTED;
  wire NLW_RAM_reg_0_63_6_8_DOD_UNCONNECTED;
  wire NLW_RAM_reg_0_63_9_11_DOD_UNCONNECTED;
  wire NLW_RAM_reg_64_127_0_2_DOD_UNCONNECTED;
  wire NLW_RAM_reg_64_127_12_14_DOD_UNCONNECTED;
  wire NLW_RAM_reg_64_127_15_17_DOD_UNCONNECTED;
  wire NLW_RAM_reg_64_127_18_20_DOD_UNCONNECTED;
  wire NLW_RAM_reg_64_127_21_21_SPO_UNCONNECTED;
  wire NLW_RAM_reg_64_127_3_5_DOD_UNCONNECTED;
  wire NLW_RAM_reg_64_127_6_8_DOD_UNCONNECTED;
  wire NLW_RAM_reg_64_127_9_11_DOD_UNCONNECTED;

  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M RAM_reg_0_63_0_2
       (.ADDRA(\gc1.count_d2_reg[5] ),
        .ADDRB(\gc1.count_d2_reg[5] ),
        .ADDRC(\gc1.count_d2_reg[5] ),
        .ADDRD(Q),
        .DIA(1'b0),
        .DIB(fg_rxd_wr_length[0]),
        .DIC(fg_rxd_wr_length[1]),
        .DID(1'b0),
        .DOA(\gpr1.dout_i_reg[0]_0 ),
        .DOB(\gpr1.dout_i_reg[1]_0 ),
        .DOC(\gpr1.dout_i_reg[2]_0 ),
        .DOD(NLW_RAM_reg_0_63_0_2_DOD_UNCONNECTED),
        .WCLK(s_axi_aclk),
        .WE(ram_full_fb_i_reg));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M RAM_reg_0_63_12_14
       (.ADDRA(\gc1.count_d2_reg[5] ),
        .ADDRB(\gc1.count_d2_reg[5] ),
        .ADDRC(\gc1.count_d2_reg[5] ),
        .ADDRD(Q),
        .DIA(fg_rxd_wr_length[11]),
        .DIB(fg_rxd_wr_length[12]),
        .DIC(fg_rxd_wr_length[13]),
        .DID(1'b0),
        .DOA(\gpr1.dout_i_reg[12]_0 ),
        .DOB(\gpr1.dout_i_reg[13]_0 ),
        .DOC(\gpr1.dout_i_reg[14]_0 ),
        .DOD(NLW_RAM_reg_0_63_12_14_DOD_UNCONNECTED),
        .WCLK(s_axi_aclk),
        .WE(ram_full_fb_i_reg));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M RAM_reg_0_63_15_17
       (.ADDRA(\gc1.count_d2_reg[5] ),
        .ADDRB(\gc1.count_d2_reg[5] ),
        .ADDRC(\gc1.count_d2_reg[5] ),
        .ADDRD(Q),
        .DIA(fg_rxd_wr_length[14]),
        .DIB(fg_rxd_wr_length[15]),
        .DIC(fg_rxd_wr_length[16]),
        .DID(1'b0),
        .DOA(\gpr1.dout_i_reg[15]_0 ),
        .DOB(\gpr1.dout_i_reg[16]_0 ),
        .DOC(\gpr1.dout_i_reg[17]_0 ),
        .DOD(NLW_RAM_reg_0_63_15_17_DOD_UNCONNECTED),
        .WCLK(s_axi_aclk),
        .WE(ram_full_fb_i_reg));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M RAM_reg_0_63_18_20
       (.ADDRA(\gc1.count_d2_reg[5] ),
        .ADDRB(\gc1.count_d2_reg[5] ),
        .ADDRC(\gc1.count_d2_reg[5] ),
        .ADDRD(Q),
        .DIA(fg_rxd_wr_length[17]),
        .DIB(fg_rxd_wr_length[18]),
        .DIC(fg_rxd_wr_length[19]),
        .DID(1'b0),
        .DOA(\gpr1.dout_i_reg[18]_0 ),
        .DOB(\gpr1.dout_i_reg[19]_0 ),
        .DOC(\gpr1.dout_i_reg[20]_0 ),
        .DOD(NLW_RAM_reg_0_63_18_20_DOD_UNCONNECTED),
        .WCLK(s_axi_aclk),
        .WE(ram_full_fb_i_reg));
  RAM64X1D RAM_reg_0_63_21_21
       (.A0(Q[0]),
        .A1(Q[1]),
        .A2(Q[2]),
        .A3(Q[3]),
        .A4(Q[4]),
        .A5(Q[5]),
        .D(fg_rxd_wr_length[20]),
        .DPO(\gpr1.dout_i_reg[21]_0 ),
        .DPRA0(\gc1.count_d2_reg[5] [0]),
        .DPRA1(\gc1.count_d2_reg[5] [1]),
        .DPRA2(\gc1.count_d2_reg[5] [2]),
        .DPRA3(\gc1.count_d2_reg[5] [3]),
        .DPRA4(\gc1.count_d2_reg[5] [4]),
        .DPRA5(\gc1.count_d2_reg[5] [5]),
        .SPO(NLW_RAM_reg_0_63_21_21_SPO_UNCONNECTED),
        .WCLK(s_axi_aclk),
        .WE(ram_full_fb_i_reg));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M RAM_reg_0_63_3_5
       (.ADDRA(\gc1.count_d2_reg[5] ),
        .ADDRB(\gc1.count_d2_reg[5] ),
        .ADDRC(\gc1.count_d2_reg[5] ),
        .ADDRD(Q),
        .DIA(fg_rxd_wr_length[2]),
        .DIB(fg_rxd_wr_length[3]),
        .DIC(fg_rxd_wr_length[4]),
        .DID(1'b0),
        .DOA(\gpr1.dout_i_reg[3]_0 ),
        .DOB(\gpr1.dout_i_reg[4]_0 ),
        .DOC(\gpr1.dout_i_reg[5]_0 ),
        .DOD(NLW_RAM_reg_0_63_3_5_DOD_UNCONNECTED),
        .WCLK(s_axi_aclk),
        .WE(ram_full_fb_i_reg));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M RAM_reg_0_63_6_8
       (.ADDRA(\gc1.count_d2_reg[5] ),
        .ADDRB(\gc1.count_d2_reg[5] ),
        .ADDRC(\gc1.count_d2_reg[5] ),
        .ADDRD(Q),
        .DIA(fg_rxd_wr_length[5]),
        .DIB(fg_rxd_wr_length[6]),
        .DIC(fg_rxd_wr_length[7]),
        .DID(1'b0),
        .DOA(\gpr1.dout_i_reg[6]_0 ),
        .DOB(\gpr1.dout_i_reg[7]_0 ),
        .DOC(\gpr1.dout_i_reg[8]_0 ),
        .DOD(NLW_RAM_reg_0_63_6_8_DOD_UNCONNECTED),
        .WCLK(s_axi_aclk),
        .WE(ram_full_fb_i_reg));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M RAM_reg_0_63_9_11
       (.ADDRA(\gc1.count_d2_reg[5] ),
        .ADDRB(\gc1.count_d2_reg[5] ),
        .ADDRC(\gc1.count_d2_reg[5] ),
        .ADDRD(Q),
        .DIA(fg_rxd_wr_length[8]),
        .DIB(fg_rxd_wr_length[9]),
        .DIC(fg_rxd_wr_length[10]),
        .DID(1'b0),
        .DOA(\gpr1.dout_i_reg[9]_0 ),
        .DOB(\gpr1.dout_i_reg[10]_0 ),
        .DOC(\gpr1.dout_i_reg[11]_0 ),
        .DOD(NLW_RAM_reg_0_63_9_11_DOD_UNCONNECTED),
        .WCLK(s_axi_aclk),
        .WE(ram_full_fb_i_reg));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M RAM_reg_64_127_0_2
       (.ADDRA(\gc1.count_d2_reg[5] ),
        .ADDRB(\gc1.count_d2_reg[5] ),
        .ADDRC(\gc1.count_d2_reg[5] ),
        .ADDRD(Q),
        .DIA(1'b0),
        .DIB(fg_rxd_wr_length[0]),
        .DIC(fg_rxd_wr_length[1]),
        .DID(1'b0),
        .DOA(\gpr1.dout_i_reg[0]_1 ),
        .DOB(\gpr1.dout_i_reg[1]_1 ),
        .DOC(\gpr1.dout_i_reg[2]_1 ),
        .DOD(NLW_RAM_reg_64_127_0_2_DOD_UNCONNECTED),
        .WCLK(s_axi_aclk),
        .WE(ram_full_fb_i_reg_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M RAM_reg_64_127_12_14
       (.ADDRA(\gc1.count_d2_reg[5] ),
        .ADDRB(\gc1.count_d2_reg[5] ),
        .ADDRC(\gc1.count_d2_reg[5] ),
        .ADDRD(Q),
        .DIA(fg_rxd_wr_length[11]),
        .DIB(fg_rxd_wr_length[12]),
        .DIC(fg_rxd_wr_length[13]),
        .DID(1'b0),
        .DOA(\gpr1.dout_i_reg[12]_1 ),
        .DOB(\gpr1.dout_i_reg[13]_1 ),
        .DOC(\gpr1.dout_i_reg[14]_1 ),
        .DOD(NLW_RAM_reg_64_127_12_14_DOD_UNCONNECTED),
        .WCLK(s_axi_aclk),
        .WE(ram_full_fb_i_reg_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M RAM_reg_64_127_15_17
       (.ADDRA(\gc1.count_d2_reg[5] ),
        .ADDRB(\gc1.count_d2_reg[5] ),
        .ADDRC(\gc1.count_d2_reg[5] ),
        .ADDRD(Q),
        .DIA(fg_rxd_wr_length[14]),
        .DIB(fg_rxd_wr_length[15]),
        .DIC(fg_rxd_wr_length[16]),
        .DID(1'b0),
        .DOA(\gpr1.dout_i_reg[15]_1 ),
        .DOB(\gpr1.dout_i_reg[16]_1 ),
        .DOC(\gpr1.dout_i_reg[17]_1 ),
        .DOD(NLW_RAM_reg_64_127_15_17_DOD_UNCONNECTED),
        .WCLK(s_axi_aclk),
        .WE(ram_full_fb_i_reg_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M RAM_reg_64_127_18_20
       (.ADDRA(\gc1.count_d2_reg[5] ),
        .ADDRB(\gc1.count_d2_reg[5] ),
        .ADDRC(\gc1.count_d2_reg[5] ),
        .ADDRD(Q),
        .DIA(fg_rxd_wr_length[17]),
        .DIB(fg_rxd_wr_length[18]),
        .DIC(fg_rxd_wr_length[19]),
        .DID(1'b0),
        .DOA(\gpr1.dout_i_reg[18]_1 ),
        .DOB(\gpr1.dout_i_reg[19]_1 ),
        .DOC(\gpr1.dout_i_reg[20]_1 ),
        .DOD(NLW_RAM_reg_64_127_18_20_DOD_UNCONNECTED),
        .WCLK(s_axi_aclk),
        .WE(ram_full_fb_i_reg_0));
  RAM64X1D RAM_reg_64_127_21_21
       (.A0(Q[0]),
        .A1(Q[1]),
        .A2(Q[2]),
        .A3(Q[3]),
        .A4(Q[4]),
        .A5(Q[5]),
        .D(fg_rxd_wr_length[20]),
        .DPO(\gpr1.dout_i_reg[21]_1 ),
        .DPRA0(\gc1.count_d2_reg[5] [0]),
        .DPRA1(\gc1.count_d2_reg[5] [1]),
        .DPRA2(\gc1.count_d2_reg[5] [2]),
        .DPRA3(\gc1.count_d2_reg[5] [3]),
        .DPRA4(\gc1.count_d2_reg[5] [4]),
        .DPRA5(\gc1.count_d2_reg[5] [5]),
        .SPO(NLW_RAM_reg_64_127_21_21_SPO_UNCONNECTED),
        .WCLK(s_axi_aclk),
        .WE(ram_full_fb_i_reg_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M RAM_reg_64_127_3_5
       (.ADDRA(\gc1.count_d2_reg[5] ),
        .ADDRB(\gc1.count_d2_reg[5] ),
        .ADDRC(\gc1.count_d2_reg[5] ),
        .ADDRD(Q),
        .DIA(fg_rxd_wr_length[2]),
        .DIB(fg_rxd_wr_length[3]),
        .DIC(fg_rxd_wr_length[4]),
        .DID(1'b0),
        .DOA(\gpr1.dout_i_reg[3]_1 ),
        .DOB(\gpr1.dout_i_reg[4]_1 ),
        .DOC(\gpr1.dout_i_reg[5]_1 ),
        .DOD(NLW_RAM_reg_64_127_3_5_DOD_UNCONNECTED),
        .WCLK(s_axi_aclk),
        .WE(ram_full_fb_i_reg_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M RAM_reg_64_127_6_8
       (.ADDRA(\gc1.count_d2_reg[5] ),
        .ADDRB(\gc1.count_d2_reg[5] ),
        .ADDRC(\gc1.count_d2_reg[5] ),
        .ADDRD(Q),
        .DIA(fg_rxd_wr_length[5]),
        .DIB(fg_rxd_wr_length[6]),
        .DIC(fg_rxd_wr_length[7]),
        .DID(1'b0),
        .DOA(\gpr1.dout_i_reg[6]_1 ),
        .DOB(\gpr1.dout_i_reg[7]_1 ),
        .DOC(\gpr1.dout_i_reg[8]_1 ),
        .DOD(NLW_RAM_reg_64_127_6_8_DOD_UNCONNECTED),
        .WCLK(s_axi_aclk),
        .WE(ram_full_fb_i_reg_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M RAM_reg_64_127_9_11
       (.ADDRA(\gc1.count_d2_reg[5] ),
        .ADDRB(\gc1.count_d2_reg[5] ),
        .ADDRC(\gc1.count_d2_reg[5] ),
        .ADDRD(Q),
        .DIA(fg_rxd_wr_length[8]),
        .DIB(fg_rxd_wr_length[9]),
        .DIC(fg_rxd_wr_length[10]),
        .DID(1'b0),
        .DOA(\gpr1.dout_i_reg[9]_1 ),
        .DOB(\gpr1.dout_i_reg[10]_1 ),
        .DOC(\gpr1.dout_i_reg[11]_1 ),
        .DOD(NLW_RAM_reg_64_127_9_11_DOD_UNCONNECTED),
        .WCLK(s_axi_aclk),
        .WE(ram_full_fb_i_reg_0));
  FDRE #(
    .INIT(1'b0)) 
    \gpr1.dout_i_reg[0] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(D[0]),
        .Q(\goreg_dm.dout_i_reg[21] [0]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \gpr1.dout_i_reg[10] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(D[10]),
        .Q(\goreg_dm.dout_i_reg[21] [10]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \gpr1.dout_i_reg[11] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(D[11]),
        .Q(\goreg_dm.dout_i_reg[21] [11]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \gpr1.dout_i_reg[12] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(D[12]),
        .Q(\goreg_dm.dout_i_reg[21] [12]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \gpr1.dout_i_reg[13] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(D[13]),
        .Q(\goreg_dm.dout_i_reg[21] [13]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \gpr1.dout_i_reg[14] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(D[14]),
        .Q(\goreg_dm.dout_i_reg[21] [14]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \gpr1.dout_i_reg[15] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(D[15]),
        .Q(\goreg_dm.dout_i_reg[21] [15]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \gpr1.dout_i_reg[16] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(D[16]),
        .Q(\goreg_dm.dout_i_reg[21] [16]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \gpr1.dout_i_reg[17] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(D[17]),
        .Q(\goreg_dm.dout_i_reg[21] [17]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \gpr1.dout_i_reg[18] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(D[18]),
        .Q(\goreg_dm.dout_i_reg[21] [18]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \gpr1.dout_i_reg[19] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(D[19]),
        .Q(\goreg_dm.dout_i_reg[21] [19]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \gpr1.dout_i_reg[1] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(D[1]),
        .Q(\goreg_dm.dout_i_reg[21] [1]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \gpr1.dout_i_reg[20] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(D[20]),
        .Q(\goreg_dm.dout_i_reg[21] [20]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \gpr1.dout_i_reg[21] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(D[21]),
        .Q(\goreg_dm.dout_i_reg[21] [21]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \gpr1.dout_i_reg[2] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(D[2]),
        .Q(\goreg_dm.dout_i_reg[21] [2]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \gpr1.dout_i_reg[3] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(D[3]),
        .Q(\goreg_dm.dout_i_reg[21] [3]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \gpr1.dout_i_reg[4] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(D[4]),
        .Q(\goreg_dm.dout_i_reg[21] [4]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \gpr1.dout_i_reg[5] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(D[5]),
        .Q(\goreg_dm.dout_i_reg[21] [5]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \gpr1.dout_i_reg[6] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(D[6]),
        .Q(\goreg_dm.dout_i_reg[21] [6]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \gpr1.dout_i_reg[7] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(D[7]),
        .Q(\goreg_dm.dout_i_reg[21] [7]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \gpr1.dout_i_reg[8] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(D[8]),
        .Q(\goreg_dm.dout_i_reg[21] [8]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \gpr1.dout_i_reg[9] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(D[9]),
        .Q(\goreg_dm.dout_i_reg[21] [9]),
        .R(SR));
  LUT3 #(
    .INIT(8'hFD)) 
    \gpregsm1.curr_fwft_state[1]_i_1__1 
       (.I0(s_axi_aresetn),
        .I1(sig_str_rst_reg),
        .I2(sig_rx_channel_reset_reg),
        .O(SR));
endmodule

(* ORIG_REF_NAME = "fifo_generator_v13_1_3_fifo_generator_ramfifo" *) 
module axi_fifo_mm_s_0_fifo_generator_v13_1_3_fifo_generator_ramfifo
   (out,
    \gcc0.gc0.count_d1_reg[6] ,
    SR,
    \gpr1.dout_i_reg[0] ,
    \gpr1.dout_i_reg[1] ,
    \gpr1.dout_i_reg[2] ,
    \gpr1.dout_i_reg[0]_0 ,
    \gpr1.dout_i_reg[1]_0 ,
    \gpr1.dout_i_reg[2]_0 ,
    \gpr1.dout_i_reg[3] ,
    \gpr1.dout_i_reg[4] ,
    \gpr1.dout_i_reg[5] ,
    \gpr1.dout_i_reg[3]_0 ,
    \gpr1.dout_i_reg[4]_0 ,
    \gpr1.dout_i_reg[5]_0 ,
    \gpr1.dout_i_reg[6] ,
    \gpr1.dout_i_reg[7] ,
    \gpr1.dout_i_reg[8] ,
    \gpr1.dout_i_reg[6]_0 ,
    \gpr1.dout_i_reg[7]_0 ,
    \gpr1.dout_i_reg[8]_0 ,
    \gpr1.dout_i_reg[9] ,
    \gpr1.dout_i_reg[10] ,
    \gpr1.dout_i_reg[11] ,
    \gpr1.dout_i_reg[9]_0 ,
    \gpr1.dout_i_reg[10]_0 ,
    \gpr1.dout_i_reg[11]_0 ,
    \gpr1.dout_i_reg[12] ,
    \gpr1.dout_i_reg[13] ,
    \gpr1.dout_i_reg[14] ,
    \gpr1.dout_i_reg[12]_0 ,
    \gpr1.dout_i_reg[13]_0 ,
    \gpr1.dout_i_reg[14]_0 ,
    \gpr1.dout_i_reg[15] ,
    \gpr1.dout_i_reg[16] ,
    \gpr1.dout_i_reg[17] ,
    \gpr1.dout_i_reg[15]_0 ,
    \gpr1.dout_i_reg[16]_0 ,
    \gpr1.dout_i_reg[17]_0 ,
    \gpr1.dout_i_reg[18] ,
    \gpr1.dout_i_reg[19] ,
    \gpr1.dout_i_reg[20] ,
    \gpr1.dout_i_reg[18]_0 ,
    \gpr1.dout_i_reg[19]_0 ,
    \gpr1.dout_i_reg[20]_0 ,
    \gpr1.dout_i_reg[21] ,
    \gpr1.dout_i_reg[21]_0 ,
    p_13_in,
    Q,
    \gpr1.dout_i_reg[21]_1 ,
    \sig_ip2bus_data_reg[10] ,
    s_axi_aclk,
    fg_rxd_wr_length,
    ram_full_fb_i_reg,
    rx_fg_len_empty_d1,
    ram_full_i_reg,
    axi_str_rxd_tvalid,
    axi_str_rxd_tlast,
    rx_len_wr_en,
    sig_rd_rlen_reg,
    s_axi_aresetn,
    sig_str_rst_reg,
    sig_rx_channel_reset_reg,
    D);
  output out;
  output \gcc0.gc0.count_d1_reg[6] ;
  output [0:0]SR;
  output \gpr1.dout_i_reg[0] ;
  output \gpr1.dout_i_reg[1] ;
  output \gpr1.dout_i_reg[2] ;
  output \gpr1.dout_i_reg[0]_0 ;
  output \gpr1.dout_i_reg[1]_0 ;
  output \gpr1.dout_i_reg[2]_0 ;
  output \gpr1.dout_i_reg[3] ;
  output \gpr1.dout_i_reg[4] ;
  output \gpr1.dout_i_reg[5] ;
  output \gpr1.dout_i_reg[3]_0 ;
  output \gpr1.dout_i_reg[4]_0 ;
  output \gpr1.dout_i_reg[5]_0 ;
  output \gpr1.dout_i_reg[6] ;
  output \gpr1.dout_i_reg[7] ;
  output \gpr1.dout_i_reg[8] ;
  output \gpr1.dout_i_reg[6]_0 ;
  output \gpr1.dout_i_reg[7]_0 ;
  output \gpr1.dout_i_reg[8]_0 ;
  output \gpr1.dout_i_reg[9] ;
  output \gpr1.dout_i_reg[10] ;
  output \gpr1.dout_i_reg[11] ;
  output \gpr1.dout_i_reg[9]_0 ;
  output \gpr1.dout_i_reg[10]_0 ;
  output \gpr1.dout_i_reg[11]_0 ;
  output \gpr1.dout_i_reg[12] ;
  output \gpr1.dout_i_reg[13] ;
  output \gpr1.dout_i_reg[14] ;
  output \gpr1.dout_i_reg[12]_0 ;
  output \gpr1.dout_i_reg[13]_0 ;
  output \gpr1.dout_i_reg[14]_0 ;
  output \gpr1.dout_i_reg[15] ;
  output \gpr1.dout_i_reg[16] ;
  output \gpr1.dout_i_reg[17] ;
  output \gpr1.dout_i_reg[15]_0 ;
  output \gpr1.dout_i_reg[16]_0 ;
  output \gpr1.dout_i_reg[17]_0 ;
  output \gpr1.dout_i_reg[18] ;
  output \gpr1.dout_i_reg[19] ;
  output \gpr1.dout_i_reg[20] ;
  output \gpr1.dout_i_reg[18]_0 ;
  output \gpr1.dout_i_reg[19]_0 ;
  output \gpr1.dout_i_reg[20]_0 ;
  output \gpr1.dout_i_reg[21] ;
  output \gpr1.dout_i_reg[21]_0 ;
  output p_13_in;
  output [0:0]Q;
  output [0:0]\gpr1.dout_i_reg[21]_1 ;
  output [21:0]\sig_ip2bus_data_reg[10] ;
  input s_axi_aclk;
  input [20:0]fg_rxd_wr_length;
  input ram_full_fb_i_reg;
  input rx_fg_len_empty_d1;
  input ram_full_i_reg;
  input axi_str_rxd_tvalid;
  input axi_str_rxd_tlast;
  input rx_len_wr_en;
  input sig_rd_rlen_reg;
  input s_axi_aresetn;
  input sig_str_rst_reg;
  input sig_rx_channel_reset_reg;
  input [21:0]D;

  wire [21:0]D;
  wire [0:0]Q;
  wire [0:0]SR;
  wire axi_str_rxd_tlast;
  wire axi_str_rxd_tvalid;
  wire [20:0]fg_rxd_wr_length;
  wire \gcc0.gc0.count_d1_reg[6] ;
  wire \gntv_or_sync_fifo.gl0.rd_n_17 ;
  wire \gntv_or_sync_fifo.gl0.rd_n_2 ;
  wire \gntv_or_sync_fifo.gl0.wr_n_1 ;
  wire \gntv_or_sync_fifo.gl0.wr_n_13 ;
  wire \gntv_or_sync_fifo.gl0.wr_n_14 ;
  wire \gntv_or_sync_fifo.gl0.wr_n_9 ;
  wire \gpr1.dout_i_reg[0] ;
  wire \gpr1.dout_i_reg[0]_0 ;
  wire \gpr1.dout_i_reg[10] ;
  wire \gpr1.dout_i_reg[10]_0 ;
  wire \gpr1.dout_i_reg[11] ;
  wire \gpr1.dout_i_reg[11]_0 ;
  wire \gpr1.dout_i_reg[12] ;
  wire \gpr1.dout_i_reg[12]_0 ;
  wire \gpr1.dout_i_reg[13] ;
  wire \gpr1.dout_i_reg[13]_0 ;
  wire \gpr1.dout_i_reg[14] ;
  wire \gpr1.dout_i_reg[14]_0 ;
  wire \gpr1.dout_i_reg[15] ;
  wire \gpr1.dout_i_reg[15]_0 ;
  wire \gpr1.dout_i_reg[16] ;
  wire \gpr1.dout_i_reg[16]_0 ;
  wire \gpr1.dout_i_reg[17] ;
  wire \gpr1.dout_i_reg[17]_0 ;
  wire \gpr1.dout_i_reg[18] ;
  wire \gpr1.dout_i_reg[18]_0 ;
  wire \gpr1.dout_i_reg[19] ;
  wire \gpr1.dout_i_reg[19]_0 ;
  wire \gpr1.dout_i_reg[1] ;
  wire \gpr1.dout_i_reg[1]_0 ;
  wire \gpr1.dout_i_reg[20] ;
  wire \gpr1.dout_i_reg[20]_0 ;
  wire \gpr1.dout_i_reg[21] ;
  wire \gpr1.dout_i_reg[21]_0 ;
  wire [0:0]\gpr1.dout_i_reg[21]_1 ;
  wire \gpr1.dout_i_reg[2] ;
  wire \gpr1.dout_i_reg[2]_0 ;
  wire \gpr1.dout_i_reg[3] ;
  wire \gpr1.dout_i_reg[3]_0 ;
  wire \gpr1.dout_i_reg[4] ;
  wire \gpr1.dout_i_reg[4]_0 ;
  wire \gpr1.dout_i_reg[5] ;
  wire \gpr1.dout_i_reg[5]_0 ;
  wire \gpr1.dout_i_reg[6] ;
  wire \gpr1.dout_i_reg[6]_0 ;
  wire \gpr1.dout_i_reg[7] ;
  wire \gpr1.dout_i_reg[7]_0 ;
  wire \gpr1.dout_i_reg[8] ;
  wire \gpr1.dout_i_reg[8]_0 ;
  wire \gpr1.dout_i_reg[9] ;
  wire \gpr1.dout_i_reg[9]_0 ;
  wire out;
  wire [5:0]p_0_out;
  wire [5:0]p_11_out;
  wire [6:4]p_12_out;
  wire p_13_in;
  wire p_5_out;
  wire p_7_out;
  wire ram_full_fb_i_reg;
  wire ram_full_i_reg;
  wire ram_rd_en_i;
  wire [3:0]rd_pntr_plus1;
  wire rx_fg_len_empty_d1;
  wire rx_len_wr_en;
  wire s_axi_aclk;
  wire s_axi_aresetn;
  wire [21:0]\sig_ip2bus_data_reg[10] ;
  wire sig_rd_rlen_reg;
  wire sig_rx_channel_reset_reg;
  wire sig_str_rst_reg;

  axi_fifo_mm_s_0_fifo_generator_v13_1_3_rd_logic \gntv_or_sync_fifo.gl0.rd 
       (.E(ram_rd_en_i),
        .Q(rd_pntr_plus1),
        .axi_str_rxd_tlast(axi_str_rxd_tlast),
        .axi_str_rxd_tvalid(axi_str_rxd_tvalid),
        .\gc1.count_reg[0] (p_7_out),
        .\gcc0.gc0.count_d1_reg[2] (\gntv_or_sync_fifo.gl0.wr_n_13 ),
        .\gcc0.gc0.count_d1_reg[6] ({Q,p_11_out[5:4],p_11_out[1:0]}),
        .\gcc0.gc0.count_reg[0] (\gntv_or_sync_fifo.gl0.wr_n_9 ),
        .\gcc0.gc0.count_reg[6] (p_12_out),
        .\goreg_dm.dout_i_reg[21] (p_5_out),
        .\gpr1.dout_i_reg[21] ({\gpr1.dout_i_reg[21]_1 ,p_0_out}),
        .out(out),
        .p_13_in(p_13_in),
        .ram_empty_i_reg(\gntv_or_sync_fifo.gl0.rd_n_2 ),
        .ram_full_fb_i_reg(\gcc0.gc0.count_d1_reg[6] ),
        .ram_full_fb_i_reg_0(\gntv_or_sync_fifo.gl0.wr_n_1 ),
        .ram_full_i_reg(\gntv_or_sync_fifo.gl0.rd_n_17 ),
        .ram_full_i_reg_0(ram_full_i_reg),
        .rx_fg_len_empty_d1(rx_fg_len_empty_d1),
        .rx_len_wr_en(rx_len_wr_en),
        .s_axi_aclk(s_axi_aclk),
        .sig_rd_rlen_reg(sig_rd_rlen_reg),
        .sig_str_rst_reg(SR));
  axi_fifo_mm_s_0_fifo_generator_v13_1_3_wr_logic \gntv_or_sync_fifo.gl0.wr 
       (.Q({Q,p_11_out}),
        .SR(SR),
        .\gc1.count_d1_reg[3] (rd_pntr_plus1),
        .\gc1.count_d1_reg[5] (\gntv_or_sync_fifo.gl0.rd_n_2 ),
        .\gc1.count_d2_reg[3] (p_0_out[3:0]),
        .\gcc0.gc0.count_d1_reg[6] (p_12_out),
        .\gpr1.dout_i_reg[0] (\gntv_or_sync_fifo.gl0.wr_n_14 ),
        .out(\gcc0.gc0.count_d1_reg[6] ),
        .ram_empty_i_reg(\gntv_or_sync_fifo.gl0.wr_n_1 ),
        .ram_full_fb_i_reg(\gntv_or_sync_fifo.gl0.rd_n_17 ),
        .ram_full_i_reg(\gntv_or_sync_fifo.gl0.wr_n_9 ),
        .ram_full_i_reg_0(\gntv_or_sync_fifo.gl0.wr_n_13 ),
        .rx_len_wr_en(rx_len_wr_en),
        .s_axi_aclk(s_axi_aclk),
        .sig_rd_rlen_reg(p_7_out));
  axi_fifo_mm_s_0_fifo_generator_v13_1_3_memory \gntv_or_sync_fifo.mem 
       (.D(D),
        .E(ram_rd_en_i),
        .Q(p_11_out),
        .SR(SR),
        .fg_rxd_wr_length(fg_rxd_wr_length),
        .\gc1.count_d2_reg[5] (p_0_out),
        .\gpr1.dout_i_reg[0] (\gpr1.dout_i_reg[0] ),
        .\gpr1.dout_i_reg[0]_0 (\gpr1.dout_i_reg[0]_0 ),
        .\gpr1.dout_i_reg[10] (\gpr1.dout_i_reg[10] ),
        .\gpr1.dout_i_reg[10]_0 (\gpr1.dout_i_reg[10]_0 ),
        .\gpr1.dout_i_reg[11] (\gpr1.dout_i_reg[11] ),
        .\gpr1.dout_i_reg[11]_0 (\gpr1.dout_i_reg[11]_0 ),
        .\gpr1.dout_i_reg[12] (\gpr1.dout_i_reg[12] ),
        .\gpr1.dout_i_reg[12]_0 (\gpr1.dout_i_reg[12]_0 ),
        .\gpr1.dout_i_reg[13] (\gpr1.dout_i_reg[13] ),
        .\gpr1.dout_i_reg[13]_0 (\gpr1.dout_i_reg[13]_0 ),
        .\gpr1.dout_i_reg[14] (\gpr1.dout_i_reg[14] ),
        .\gpr1.dout_i_reg[14]_0 (\gpr1.dout_i_reg[14]_0 ),
        .\gpr1.dout_i_reg[15] (\gpr1.dout_i_reg[15] ),
        .\gpr1.dout_i_reg[15]_0 (\gpr1.dout_i_reg[15]_0 ),
        .\gpr1.dout_i_reg[16] (\gpr1.dout_i_reg[16] ),
        .\gpr1.dout_i_reg[16]_0 (\gpr1.dout_i_reg[16]_0 ),
        .\gpr1.dout_i_reg[17] (\gpr1.dout_i_reg[17] ),
        .\gpr1.dout_i_reg[17]_0 (\gpr1.dout_i_reg[17]_0 ),
        .\gpr1.dout_i_reg[18] (\gpr1.dout_i_reg[18] ),
        .\gpr1.dout_i_reg[18]_0 (\gpr1.dout_i_reg[18]_0 ),
        .\gpr1.dout_i_reg[19] (\gpr1.dout_i_reg[19] ),
        .\gpr1.dout_i_reg[19]_0 (\gpr1.dout_i_reg[19]_0 ),
        .\gpr1.dout_i_reg[1] (\gpr1.dout_i_reg[1] ),
        .\gpr1.dout_i_reg[1]_0 (\gpr1.dout_i_reg[1]_0 ),
        .\gpr1.dout_i_reg[20] (\gpr1.dout_i_reg[20] ),
        .\gpr1.dout_i_reg[20]_0 (\gpr1.dout_i_reg[20]_0 ),
        .\gpr1.dout_i_reg[21] (\gpr1.dout_i_reg[21] ),
        .\gpr1.dout_i_reg[21]_0 (\gpr1.dout_i_reg[21]_0 ),
        .\gpr1.dout_i_reg[2] (\gpr1.dout_i_reg[2] ),
        .\gpr1.dout_i_reg[2]_0 (\gpr1.dout_i_reg[2]_0 ),
        .\gpr1.dout_i_reg[3] (\gpr1.dout_i_reg[3] ),
        .\gpr1.dout_i_reg[3]_0 (\gpr1.dout_i_reg[3]_0 ),
        .\gpr1.dout_i_reg[4] (\gpr1.dout_i_reg[4] ),
        .\gpr1.dout_i_reg[4]_0 (\gpr1.dout_i_reg[4]_0 ),
        .\gpr1.dout_i_reg[5] (\gpr1.dout_i_reg[5] ),
        .\gpr1.dout_i_reg[5]_0 (\gpr1.dout_i_reg[5]_0 ),
        .\gpr1.dout_i_reg[6] (\gpr1.dout_i_reg[6] ),
        .\gpr1.dout_i_reg[6]_0 (\gpr1.dout_i_reg[6]_0 ),
        .\gpr1.dout_i_reg[7] (\gpr1.dout_i_reg[7] ),
        .\gpr1.dout_i_reg[7]_0 (\gpr1.dout_i_reg[7]_0 ),
        .\gpr1.dout_i_reg[8] (\gpr1.dout_i_reg[8] ),
        .\gpr1.dout_i_reg[8]_0 (\gpr1.dout_i_reg[8]_0 ),
        .\gpr1.dout_i_reg[9] (\gpr1.dout_i_reg[9] ),
        .\gpr1.dout_i_reg[9]_0 (\gpr1.dout_i_reg[9]_0 ),
        .ram_full_fb_i_reg(\gntv_or_sync_fifo.gl0.wr_n_14 ),
        .ram_full_fb_i_reg_0(ram_full_fb_i_reg),
        .s_axi_aclk(s_axi_aclk),
        .s_axi_aresetn(s_axi_aresetn),
        .\sig_ip2bus_data_reg[10] (\sig_ip2bus_data_reg[10] ),
        .sig_rd_rlen_reg(p_5_out),
        .sig_rx_channel_reset_reg(sig_rx_channel_reset_reg),
        .sig_str_rst_reg(sig_str_rst_reg));
endmodule

(* ORIG_REF_NAME = "fifo_generator_v13_1_3_fifo_generator_top" *) 
module axi_fifo_mm_s_0_fifo_generator_v13_1_3_fifo_generator_top
   (out,
    \gcc0.gc0.count_d1_reg[6] ,
    \gpr1.dout_i_reg[0] ,
    \gpr1.dout_i_reg[0]_0 ,
    \gpr1.dout_i_reg[1] ,
    \gpr1.dout_i_reg[2] ,
    \gpr1.dout_i_reg[0]_1 ,
    \gpr1.dout_i_reg[1]_0 ,
    \gpr1.dout_i_reg[2]_0 ,
    \gpr1.dout_i_reg[3] ,
    \gpr1.dout_i_reg[4] ,
    \gpr1.dout_i_reg[5] ,
    \gpr1.dout_i_reg[3]_0 ,
    \gpr1.dout_i_reg[4]_0 ,
    \gpr1.dout_i_reg[5]_0 ,
    \gpr1.dout_i_reg[6] ,
    \gpr1.dout_i_reg[7] ,
    \gpr1.dout_i_reg[8] ,
    \gpr1.dout_i_reg[6]_0 ,
    \gpr1.dout_i_reg[7]_0 ,
    \gpr1.dout_i_reg[8]_0 ,
    \gpr1.dout_i_reg[9] ,
    \gpr1.dout_i_reg[10] ,
    \gpr1.dout_i_reg[11] ,
    \gpr1.dout_i_reg[9]_0 ,
    \gpr1.dout_i_reg[10]_0 ,
    \gpr1.dout_i_reg[11]_0 ,
    \gpr1.dout_i_reg[12] ,
    \gpr1.dout_i_reg[13] ,
    \gpr1.dout_i_reg[14] ,
    \gpr1.dout_i_reg[12]_0 ,
    \gpr1.dout_i_reg[13]_0 ,
    \gpr1.dout_i_reg[14]_0 ,
    \gpr1.dout_i_reg[15] ,
    \gpr1.dout_i_reg[16] ,
    \gpr1.dout_i_reg[17] ,
    \gpr1.dout_i_reg[15]_0 ,
    \gpr1.dout_i_reg[16]_0 ,
    \gpr1.dout_i_reg[17]_0 ,
    \gpr1.dout_i_reg[18] ,
    \gpr1.dout_i_reg[19] ,
    \gpr1.dout_i_reg[20] ,
    \gpr1.dout_i_reg[18]_0 ,
    \gpr1.dout_i_reg[19]_0 ,
    \gpr1.dout_i_reg[20]_0 ,
    \gpr1.dout_i_reg[21] ,
    \gpr1.dout_i_reg[21]_0 ,
    p_13_in,
    Q,
    \gpr1.dout_i_reg[21]_1 ,
    \sig_ip2bus_data_reg[10] ,
    s_axi_aclk,
    fg_rxd_wr_length,
    ram_full_fb_i_reg,
    rx_fg_len_empty_d1,
    ram_full_i_reg,
    axi_str_rxd_tvalid,
    axi_str_rxd_tlast,
    rx_len_wr_en,
    sig_rd_rlen_reg,
    s_axi_aresetn,
    sig_str_rst_reg,
    sig_rx_channel_reset_reg,
    D);
  output out;
  output \gcc0.gc0.count_d1_reg[6] ;
  output \gpr1.dout_i_reg[0] ;
  output \gpr1.dout_i_reg[0]_0 ;
  output \gpr1.dout_i_reg[1] ;
  output \gpr1.dout_i_reg[2] ;
  output \gpr1.dout_i_reg[0]_1 ;
  output \gpr1.dout_i_reg[1]_0 ;
  output \gpr1.dout_i_reg[2]_0 ;
  output \gpr1.dout_i_reg[3] ;
  output \gpr1.dout_i_reg[4] ;
  output \gpr1.dout_i_reg[5] ;
  output \gpr1.dout_i_reg[3]_0 ;
  output \gpr1.dout_i_reg[4]_0 ;
  output \gpr1.dout_i_reg[5]_0 ;
  output \gpr1.dout_i_reg[6] ;
  output \gpr1.dout_i_reg[7] ;
  output \gpr1.dout_i_reg[8] ;
  output \gpr1.dout_i_reg[6]_0 ;
  output \gpr1.dout_i_reg[7]_0 ;
  output \gpr1.dout_i_reg[8]_0 ;
  output \gpr1.dout_i_reg[9] ;
  output \gpr1.dout_i_reg[10] ;
  output \gpr1.dout_i_reg[11] ;
  output \gpr1.dout_i_reg[9]_0 ;
  output \gpr1.dout_i_reg[10]_0 ;
  output \gpr1.dout_i_reg[11]_0 ;
  output \gpr1.dout_i_reg[12] ;
  output \gpr1.dout_i_reg[13] ;
  output \gpr1.dout_i_reg[14] ;
  output \gpr1.dout_i_reg[12]_0 ;
  output \gpr1.dout_i_reg[13]_0 ;
  output \gpr1.dout_i_reg[14]_0 ;
  output \gpr1.dout_i_reg[15] ;
  output \gpr1.dout_i_reg[16] ;
  output \gpr1.dout_i_reg[17] ;
  output \gpr1.dout_i_reg[15]_0 ;
  output \gpr1.dout_i_reg[16]_0 ;
  output \gpr1.dout_i_reg[17]_0 ;
  output \gpr1.dout_i_reg[18] ;
  output \gpr1.dout_i_reg[19] ;
  output \gpr1.dout_i_reg[20] ;
  output \gpr1.dout_i_reg[18]_0 ;
  output \gpr1.dout_i_reg[19]_0 ;
  output \gpr1.dout_i_reg[20]_0 ;
  output \gpr1.dout_i_reg[21] ;
  output \gpr1.dout_i_reg[21]_0 ;
  output p_13_in;
  output [0:0]Q;
  output [0:0]\gpr1.dout_i_reg[21]_1 ;
  output [21:0]\sig_ip2bus_data_reg[10] ;
  input s_axi_aclk;
  input [20:0]fg_rxd_wr_length;
  input ram_full_fb_i_reg;
  input rx_fg_len_empty_d1;
  input ram_full_i_reg;
  input axi_str_rxd_tvalid;
  input axi_str_rxd_tlast;
  input rx_len_wr_en;
  input sig_rd_rlen_reg;
  input s_axi_aresetn;
  input sig_str_rst_reg;
  input sig_rx_channel_reset_reg;
  input [21:0]D;

  wire [21:0]D;
  wire [0:0]Q;
  wire axi_str_rxd_tlast;
  wire axi_str_rxd_tvalid;
  wire [20:0]fg_rxd_wr_length;
  wire \gcc0.gc0.count_d1_reg[6] ;
  wire \gpr1.dout_i_reg[0] ;
  wire \gpr1.dout_i_reg[0]_0 ;
  wire \gpr1.dout_i_reg[0]_1 ;
  wire \gpr1.dout_i_reg[10] ;
  wire \gpr1.dout_i_reg[10]_0 ;
  wire \gpr1.dout_i_reg[11] ;
  wire \gpr1.dout_i_reg[11]_0 ;
  wire \gpr1.dout_i_reg[12] ;
  wire \gpr1.dout_i_reg[12]_0 ;
  wire \gpr1.dout_i_reg[13] ;
  wire \gpr1.dout_i_reg[13]_0 ;
  wire \gpr1.dout_i_reg[14] ;
  wire \gpr1.dout_i_reg[14]_0 ;
  wire \gpr1.dout_i_reg[15] ;
  wire \gpr1.dout_i_reg[15]_0 ;
  wire \gpr1.dout_i_reg[16] ;
  wire \gpr1.dout_i_reg[16]_0 ;
  wire \gpr1.dout_i_reg[17] ;
  wire \gpr1.dout_i_reg[17]_0 ;
  wire \gpr1.dout_i_reg[18] ;
  wire \gpr1.dout_i_reg[18]_0 ;
  wire \gpr1.dout_i_reg[19] ;
  wire \gpr1.dout_i_reg[19]_0 ;
  wire \gpr1.dout_i_reg[1] ;
  wire \gpr1.dout_i_reg[1]_0 ;
  wire \gpr1.dout_i_reg[20] ;
  wire \gpr1.dout_i_reg[20]_0 ;
  wire \gpr1.dout_i_reg[21] ;
  wire \gpr1.dout_i_reg[21]_0 ;
  wire [0:0]\gpr1.dout_i_reg[21]_1 ;
  wire \gpr1.dout_i_reg[2] ;
  wire \gpr1.dout_i_reg[2]_0 ;
  wire \gpr1.dout_i_reg[3] ;
  wire \gpr1.dout_i_reg[3]_0 ;
  wire \gpr1.dout_i_reg[4] ;
  wire \gpr1.dout_i_reg[4]_0 ;
  wire \gpr1.dout_i_reg[5] ;
  wire \gpr1.dout_i_reg[5]_0 ;
  wire \gpr1.dout_i_reg[6] ;
  wire \gpr1.dout_i_reg[6]_0 ;
  wire \gpr1.dout_i_reg[7] ;
  wire \gpr1.dout_i_reg[7]_0 ;
  wire \gpr1.dout_i_reg[8] ;
  wire \gpr1.dout_i_reg[8]_0 ;
  wire \gpr1.dout_i_reg[9] ;
  wire \gpr1.dout_i_reg[9]_0 ;
  wire out;
  wire p_13_in;
  wire ram_full_fb_i_reg;
  wire ram_full_i_reg;
  wire rx_fg_len_empty_d1;
  wire rx_len_wr_en;
  wire s_axi_aclk;
  wire s_axi_aresetn;
  wire [21:0]\sig_ip2bus_data_reg[10] ;
  wire sig_rd_rlen_reg;
  wire sig_rx_channel_reset_reg;
  wire sig_str_rst_reg;

  axi_fifo_mm_s_0_fifo_generator_v13_1_3_fifo_generator_ramfifo \grf.rf 
       (.D(D),
        .Q(Q),
        .SR(\gpr1.dout_i_reg[0] ),
        .axi_str_rxd_tlast(axi_str_rxd_tlast),
        .axi_str_rxd_tvalid(axi_str_rxd_tvalid),
        .fg_rxd_wr_length(fg_rxd_wr_length),
        .\gcc0.gc0.count_d1_reg[6] (\gcc0.gc0.count_d1_reg[6] ),
        .\gpr1.dout_i_reg[0] (\gpr1.dout_i_reg[0]_0 ),
        .\gpr1.dout_i_reg[0]_0 (\gpr1.dout_i_reg[0]_1 ),
        .\gpr1.dout_i_reg[10] (\gpr1.dout_i_reg[10] ),
        .\gpr1.dout_i_reg[10]_0 (\gpr1.dout_i_reg[10]_0 ),
        .\gpr1.dout_i_reg[11] (\gpr1.dout_i_reg[11] ),
        .\gpr1.dout_i_reg[11]_0 (\gpr1.dout_i_reg[11]_0 ),
        .\gpr1.dout_i_reg[12] (\gpr1.dout_i_reg[12] ),
        .\gpr1.dout_i_reg[12]_0 (\gpr1.dout_i_reg[12]_0 ),
        .\gpr1.dout_i_reg[13] (\gpr1.dout_i_reg[13] ),
        .\gpr1.dout_i_reg[13]_0 (\gpr1.dout_i_reg[13]_0 ),
        .\gpr1.dout_i_reg[14] (\gpr1.dout_i_reg[14] ),
        .\gpr1.dout_i_reg[14]_0 (\gpr1.dout_i_reg[14]_0 ),
        .\gpr1.dout_i_reg[15] (\gpr1.dout_i_reg[15] ),
        .\gpr1.dout_i_reg[15]_0 (\gpr1.dout_i_reg[15]_0 ),
        .\gpr1.dout_i_reg[16] (\gpr1.dout_i_reg[16] ),
        .\gpr1.dout_i_reg[16]_0 (\gpr1.dout_i_reg[16]_0 ),
        .\gpr1.dout_i_reg[17] (\gpr1.dout_i_reg[17] ),
        .\gpr1.dout_i_reg[17]_0 (\gpr1.dout_i_reg[17]_0 ),
        .\gpr1.dout_i_reg[18] (\gpr1.dout_i_reg[18] ),
        .\gpr1.dout_i_reg[18]_0 (\gpr1.dout_i_reg[18]_0 ),
        .\gpr1.dout_i_reg[19] (\gpr1.dout_i_reg[19] ),
        .\gpr1.dout_i_reg[19]_0 (\gpr1.dout_i_reg[19]_0 ),
        .\gpr1.dout_i_reg[1] (\gpr1.dout_i_reg[1] ),
        .\gpr1.dout_i_reg[1]_0 (\gpr1.dout_i_reg[1]_0 ),
        .\gpr1.dout_i_reg[20] (\gpr1.dout_i_reg[20] ),
        .\gpr1.dout_i_reg[20]_0 (\gpr1.dout_i_reg[20]_0 ),
        .\gpr1.dout_i_reg[21] (\gpr1.dout_i_reg[21] ),
        .\gpr1.dout_i_reg[21]_0 (\gpr1.dout_i_reg[21]_0 ),
        .\gpr1.dout_i_reg[21]_1 (\gpr1.dout_i_reg[21]_1 ),
        .\gpr1.dout_i_reg[2] (\gpr1.dout_i_reg[2] ),
        .\gpr1.dout_i_reg[2]_0 (\gpr1.dout_i_reg[2]_0 ),
        .\gpr1.dout_i_reg[3] (\gpr1.dout_i_reg[3] ),
        .\gpr1.dout_i_reg[3]_0 (\gpr1.dout_i_reg[3]_0 ),
        .\gpr1.dout_i_reg[4] (\gpr1.dout_i_reg[4] ),
        .\gpr1.dout_i_reg[4]_0 (\gpr1.dout_i_reg[4]_0 ),
        .\gpr1.dout_i_reg[5] (\gpr1.dout_i_reg[5] ),
        .\gpr1.dout_i_reg[5]_0 (\gpr1.dout_i_reg[5]_0 ),
        .\gpr1.dout_i_reg[6] (\gpr1.dout_i_reg[6] ),
        .\gpr1.dout_i_reg[6]_0 (\gpr1.dout_i_reg[6]_0 ),
        .\gpr1.dout_i_reg[7] (\gpr1.dout_i_reg[7] ),
        .\gpr1.dout_i_reg[7]_0 (\gpr1.dout_i_reg[7]_0 ),
        .\gpr1.dout_i_reg[8] (\gpr1.dout_i_reg[8] ),
        .\gpr1.dout_i_reg[8]_0 (\gpr1.dout_i_reg[8]_0 ),
        .\gpr1.dout_i_reg[9] (\gpr1.dout_i_reg[9] ),
        .\gpr1.dout_i_reg[9]_0 (\gpr1.dout_i_reg[9]_0 ),
        .out(out),
        .p_13_in(p_13_in),
        .ram_full_fb_i_reg(ram_full_fb_i_reg),
        .ram_full_i_reg(ram_full_i_reg),
        .rx_fg_len_empty_d1(rx_fg_len_empty_d1),
        .rx_len_wr_en(rx_len_wr_en),
        .s_axi_aclk(s_axi_aclk),
        .s_axi_aresetn(s_axi_aresetn),
        .\sig_ip2bus_data_reg[10] (\sig_ip2bus_data_reg[10] ),
        .sig_rd_rlen_reg(sig_rd_rlen_reg),
        .sig_rx_channel_reset_reg(sig_rx_channel_reset_reg),
        .sig_str_rst_reg(sig_str_rst_reg));
endmodule

(* ORIG_REF_NAME = "fifo_generator_v13_1_3_memory" *) 
module axi_fifo_mm_s_0_fifo_generator_v13_1_3_memory
   (\gpr1.dout_i_reg[0] ,
    \gpr1.dout_i_reg[1] ,
    \gpr1.dout_i_reg[2] ,
    \gpr1.dout_i_reg[0]_0 ,
    \gpr1.dout_i_reg[1]_0 ,
    \gpr1.dout_i_reg[2]_0 ,
    \gpr1.dout_i_reg[3] ,
    \gpr1.dout_i_reg[4] ,
    \gpr1.dout_i_reg[5] ,
    \gpr1.dout_i_reg[3]_0 ,
    \gpr1.dout_i_reg[4]_0 ,
    \gpr1.dout_i_reg[5]_0 ,
    \gpr1.dout_i_reg[6] ,
    \gpr1.dout_i_reg[7] ,
    \gpr1.dout_i_reg[8] ,
    \gpr1.dout_i_reg[6]_0 ,
    \gpr1.dout_i_reg[7]_0 ,
    \gpr1.dout_i_reg[8]_0 ,
    \gpr1.dout_i_reg[9] ,
    \gpr1.dout_i_reg[10] ,
    \gpr1.dout_i_reg[11] ,
    \gpr1.dout_i_reg[9]_0 ,
    \gpr1.dout_i_reg[10]_0 ,
    \gpr1.dout_i_reg[11]_0 ,
    \gpr1.dout_i_reg[12] ,
    \gpr1.dout_i_reg[13] ,
    \gpr1.dout_i_reg[14] ,
    \gpr1.dout_i_reg[12]_0 ,
    \gpr1.dout_i_reg[13]_0 ,
    \gpr1.dout_i_reg[14]_0 ,
    \gpr1.dout_i_reg[15] ,
    \gpr1.dout_i_reg[16] ,
    \gpr1.dout_i_reg[17] ,
    \gpr1.dout_i_reg[15]_0 ,
    \gpr1.dout_i_reg[16]_0 ,
    \gpr1.dout_i_reg[17]_0 ,
    \gpr1.dout_i_reg[18] ,
    \gpr1.dout_i_reg[19] ,
    \gpr1.dout_i_reg[20] ,
    \gpr1.dout_i_reg[18]_0 ,
    \gpr1.dout_i_reg[19]_0 ,
    \gpr1.dout_i_reg[20]_0 ,
    \gpr1.dout_i_reg[21] ,
    \gpr1.dout_i_reg[21]_0 ,
    SR,
    \sig_ip2bus_data_reg[10] ,
    s_axi_aclk,
    fg_rxd_wr_length,
    ram_full_fb_i_reg,
    \gc1.count_d2_reg[5] ,
    Q,
    ram_full_fb_i_reg_0,
    s_axi_aresetn,
    sig_str_rst_reg,
    sig_rx_channel_reset_reg,
    E,
    D,
    sig_rd_rlen_reg);
  output \gpr1.dout_i_reg[0] ;
  output \gpr1.dout_i_reg[1] ;
  output \gpr1.dout_i_reg[2] ;
  output \gpr1.dout_i_reg[0]_0 ;
  output \gpr1.dout_i_reg[1]_0 ;
  output \gpr1.dout_i_reg[2]_0 ;
  output \gpr1.dout_i_reg[3] ;
  output \gpr1.dout_i_reg[4] ;
  output \gpr1.dout_i_reg[5] ;
  output \gpr1.dout_i_reg[3]_0 ;
  output \gpr1.dout_i_reg[4]_0 ;
  output \gpr1.dout_i_reg[5]_0 ;
  output \gpr1.dout_i_reg[6] ;
  output \gpr1.dout_i_reg[7] ;
  output \gpr1.dout_i_reg[8] ;
  output \gpr1.dout_i_reg[6]_0 ;
  output \gpr1.dout_i_reg[7]_0 ;
  output \gpr1.dout_i_reg[8]_0 ;
  output \gpr1.dout_i_reg[9] ;
  output \gpr1.dout_i_reg[10] ;
  output \gpr1.dout_i_reg[11] ;
  output \gpr1.dout_i_reg[9]_0 ;
  output \gpr1.dout_i_reg[10]_0 ;
  output \gpr1.dout_i_reg[11]_0 ;
  output \gpr1.dout_i_reg[12] ;
  output \gpr1.dout_i_reg[13] ;
  output \gpr1.dout_i_reg[14] ;
  output \gpr1.dout_i_reg[12]_0 ;
  output \gpr1.dout_i_reg[13]_0 ;
  output \gpr1.dout_i_reg[14]_0 ;
  output \gpr1.dout_i_reg[15] ;
  output \gpr1.dout_i_reg[16] ;
  output \gpr1.dout_i_reg[17] ;
  output \gpr1.dout_i_reg[15]_0 ;
  output \gpr1.dout_i_reg[16]_0 ;
  output \gpr1.dout_i_reg[17]_0 ;
  output \gpr1.dout_i_reg[18] ;
  output \gpr1.dout_i_reg[19] ;
  output \gpr1.dout_i_reg[20] ;
  output \gpr1.dout_i_reg[18]_0 ;
  output \gpr1.dout_i_reg[19]_0 ;
  output \gpr1.dout_i_reg[20]_0 ;
  output \gpr1.dout_i_reg[21] ;
  output \gpr1.dout_i_reg[21]_0 ;
  output [0:0]SR;
  output [21:0]\sig_ip2bus_data_reg[10] ;
  input s_axi_aclk;
  input [20:0]fg_rxd_wr_length;
  input ram_full_fb_i_reg;
  input [5:0]\gc1.count_d2_reg[5] ;
  input [5:0]Q;
  input ram_full_fb_i_reg_0;
  input s_axi_aresetn;
  input sig_str_rst_reg;
  input sig_rx_channel_reset_reg;
  input [0:0]E;
  input [21:0]D;
  input [0:0]sig_rd_rlen_reg;

  wire [21:0]D;
  wire [0:0]E;
  wire [5:0]Q;
  wire [0:0]SR;
  wire [21:0]dout_i;
  wire [20:0]fg_rxd_wr_length;
  wire [5:0]\gc1.count_d2_reg[5] ;
  wire \gpr1.dout_i_reg[0] ;
  wire \gpr1.dout_i_reg[0]_0 ;
  wire \gpr1.dout_i_reg[10] ;
  wire \gpr1.dout_i_reg[10]_0 ;
  wire \gpr1.dout_i_reg[11] ;
  wire \gpr1.dout_i_reg[11]_0 ;
  wire \gpr1.dout_i_reg[12] ;
  wire \gpr1.dout_i_reg[12]_0 ;
  wire \gpr1.dout_i_reg[13] ;
  wire \gpr1.dout_i_reg[13]_0 ;
  wire \gpr1.dout_i_reg[14] ;
  wire \gpr1.dout_i_reg[14]_0 ;
  wire \gpr1.dout_i_reg[15] ;
  wire \gpr1.dout_i_reg[15]_0 ;
  wire \gpr1.dout_i_reg[16] ;
  wire \gpr1.dout_i_reg[16]_0 ;
  wire \gpr1.dout_i_reg[17] ;
  wire \gpr1.dout_i_reg[17]_0 ;
  wire \gpr1.dout_i_reg[18] ;
  wire \gpr1.dout_i_reg[18]_0 ;
  wire \gpr1.dout_i_reg[19] ;
  wire \gpr1.dout_i_reg[19]_0 ;
  wire \gpr1.dout_i_reg[1] ;
  wire \gpr1.dout_i_reg[1]_0 ;
  wire \gpr1.dout_i_reg[20] ;
  wire \gpr1.dout_i_reg[20]_0 ;
  wire \gpr1.dout_i_reg[21] ;
  wire \gpr1.dout_i_reg[21]_0 ;
  wire \gpr1.dout_i_reg[2] ;
  wire \gpr1.dout_i_reg[2]_0 ;
  wire \gpr1.dout_i_reg[3] ;
  wire \gpr1.dout_i_reg[3]_0 ;
  wire \gpr1.dout_i_reg[4] ;
  wire \gpr1.dout_i_reg[4]_0 ;
  wire \gpr1.dout_i_reg[5] ;
  wire \gpr1.dout_i_reg[5]_0 ;
  wire \gpr1.dout_i_reg[6] ;
  wire \gpr1.dout_i_reg[6]_0 ;
  wire \gpr1.dout_i_reg[7] ;
  wire \gpr1.dout_i_reg[7]_0 ;
  wire \gpr1.dout_i_reg[8] ;
  wire \gpr1.dout_i_reg[8]_0 ;
  wire \gpr1.dout_i_reg[9] ;
  wire \gpr1.dout_i_reg[9]_0 ;
  wire ram_full_fb_i_reg;
  wire ram_full_fb_i_reg_0;
  wire s_axi_aclk;
  wire s_axi_aresetn;
  wire [21:0]\sig_ip2bus_data_reg[10] ;
  wire [0:0]sig_rd_rlen_reg;
  wire sig_rx_channel_reset_reg;
  wire sig_str_rst_reg;

  axi_fifo_mm_s_0_fifo_generator_v13_1_3_dmem \gdm.dm_gen.dm 
       (.D(D),
        .E(E),
        .Q(Q),
        .SR(SR),
        .fg_rxd_wr_length(fg_rxd_wr_length),
        .\gc1.count_d2_reg[5] (\gc1.count_d2_reg[5] ),
        .\goreg_dm.dout_i_reg[21] (dout_i),
        .\gpr1.dout_i_reg[0]_0 (\gpr1.dout_i_reg[0] ),
        .\gpr1.dout_i_reg[0]_1 (\gpr1.dout_i_reg[0]_0 ),
        .\gpr1.dout_i_reg[10]_0 (\gpr1.dout_i_reg[10] ),
        .\gpr1.dout_i_reg[10]_1 (\gpr1.dout_i_reg[10]_0 ),
        .\gpr1.dout_i_reg[11]_0 (\gpr1.dout_i_reg[11] ),
        .\gpr1.dout_i_reg[11]_1 (\gpr1.dout_i_reg[11]_0 ),
        .\gpr1.dout_i_reg[12]_0 (\gpr1.dout_i_reg[12] ),
        .\gpr1.dout_i_reg[12]_1 (\gpr1.dout_i_reg[12]_0 ),
        .\gpr1.dout_i_reg[13]_0 (\gpr1.dout_i_reg[13] ),
        .\gpr1.dout_i_reg[13]_1 (\gpr1.dout_i_reg[13]_0 ),
        .\gpr1.dout_i_reg[14]_0 (\gpr1.dout_i_reg[14] ),
        .\gpr1.dout_i_reg[14]_1 (\gpr1.dout_i_reg[14]_0 ),
        .\gpr1.dout_i_reg[15]_0 (\gpr1.dout_i_reg[15] ),
        .\gpr1.dout_i_reg[15]_1 (\gpr1.dout_i_reg[15]_0 ),
        .\gpr1.dout_i_reg[16]_0 (\gpr1.dout_i_reg[16] ),
        .\gpr1.dout_i_reg[16]_1 (\gpr1.dout_i_reg[16]_0 ),
        .\gpr1.dout_i_reg[17]_0 (\gpr1.dout_i_reg[17] ),
        .\gpr1.dout_i_reg[17]_1 (\gpr1.dout_i_reg[17]_0 ),
        .\gpr1.dout_i_reg[18]_0 (\gpr1.dout_i_reg[18] ),
        .\gpr1.dout_i_reg[18]_1 (\gpr1.dout_i_reg[18]_0 ),
        .\gpr1.dout_i_reg[19]_0 (\gpr1.dout_i_reg[19] ),
        .\gpr1.dout_i_reg[19]_1 (\gpr1.dout_i_reg[19]_0 ),
        .\gpr1.dout_i_reg[1]_0 (\gpr1.dout_i_reg[1] ),
        .\gpr1.dout_i_reg[1]_1 (\gpr1.dout_i_reg[1]_0 ),
        .\gpr1.dout_i_reg[20]_0 (\gpr1.dout_i_reg[20] ),
        .\gpr1.dout_i_reg[20]_1 (\gpr1.dout_i_reg[20]_0 ),
        .\gpr1.dout_i_reg[21]_0 (\gpr1.dout_i_reg[21] ),
        .\gpr1.dout_i_reg[21]_1 (\gpr1.dout_i_reg[21]_0 ),
        .\gpr1.dout_i_reg[2]_0 (\gpr1.dout_i_reg[2] ),
        .\gpr1.dout_i_reg[2]_1 (\gpr1.dout_i_reg[2]_0 ),
        .\gpr1.dout_i_reg[3]_0 (\gpr1.dout_i_reg[3] ),
        .\gpr1.dout_i_reg[3]_1 (\gpr1.dout_i_reg[3]_0 ),
        .\gpr1.dout_i_reg[4]_0 (\gpr1.dout_i_reg[4] ),
        .\gpr1.dout_i_reg[4]_1 (\gpr1.dout_i_reg[4]_0 ),
        .\gpr1.dout_i_reg[5]_0 (\gpr1.dout_i_reg[5] ),
        .\gpr1.dout_i_reg[5]_1 (\gpr1.dout_i_reg[5]_0 ),
        .\gpr1.dout_i_reg[6]_0 (\gpr1.dout_i_reg[6] ),
        .\gpr1.dout_i_reg[6]_1 (\gpr1.dout_i_reg[6]_0 ),
        .\gpr1.dout_i_reg[7]_0 (\gpr1.dout_i_reg[7] ),
        .\gpr1.dout_i_reg[7]_1 (\gpr1.dout_i_reg[7]_0 ),
        .\gpr1.dout_i_reg[8]_0 (\gpr1.dout_i_reg[8] ),
        .\gpr1.dout_i_reg[8]_1 (\gpr1.dout_i_reg[8]_0 ),
        .\gpr1.dout_i_reg[9]_0 (\gpr1.dout_i_reg[9] ),
        .\gpr1.dout_i_reg[9]_1 (\gpr1.dout_i_reg[9]_0 ),
        .ram_full_fb_i_reg(ram_full_fb_i_reg),
        .ram_full_fb_i_reg_0(ram_full_fb_i_reg_0),
        .s_axi_aclk(s_axi_aclk),
        .s_axi_aresetn(s_axi_aresetn),
        .sig_rx_channel_reset_reg(sig_rx_channel_reset_reg),
        .sig_str_rst_reg(sig_str_rst_reg));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_dm.dout_i_reg[0] 
       (.C(s_axi_aclk),
        .CE(sig_rd_rlen_reg),
        .D(dout_i[0]),
        .Q(\sig_ip2bus_data_reg[10] [0]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_dm.dout_i_reg[10] 
       (.C(s_axi_aclk),
        .CE(sig_rd_rlen_reg),
        .D(dout_i[10]),
        .Q(\sig_ip2bus_data_reg[10] [10]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_dm.dout_i_reg[11] 
       (.C(s_axi_aclk),
        .CE(sig_rd_rlen_reg),
        .D(dout_i[11]),
        .Q(\sig_ip2bus_data_reg[10] [11]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_dm.dout_i_reg[12] 
       (.C(s_axi_aclk),
        .CE(sig_rd_rlen_reg),
        .D(dout_i[12]),
        .Q(\sig_ip2bus_data_reg[10] [12]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_dm.dout_i_reg[13] 
       (.C(s_axi_aclk),
        .CE(sig_rd_rlen_reg),
        .D(dout_i[13]),
        .Q(\sig_ip2bus_data_reg[10] [13]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_dm.dout_i_reg[14] 
       (.C(s_axi_aclk),
        .CE(sig_rd_rlen_reg),
        .D(dout_i[14]),
        .Q(\sig_ip2bus_data_reg[10] [14]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_dm.dout_i_reg[15] 
       (.C(s_axi_aclk),
        .CE(sig_rd_rlen_reg),
        .D(dout_i[15]),
        .Q(\sig_ip2bus_data_reg[10] [15]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_dm.dout_i_reg[16] 
       (.C(s_axi_aclk),
        .CE(sig_rd_rlen_reg),
        .D(dout_i[16]),
        .Q(\sig_ip2bus_data_reg[10] [16]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_dm.dout_i_reg[17] 
       (.C(s_axi_aclk),
        .CE(sig_rd_rlen_reg),
        .D(dout_i[17]),
        .Q(\sig_ip2bus_data_reg[10] [17]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_dm.dout_i_reg[18] 
       (.C(s_axi_aclk),
        .CE(sig_rd_rlen_reg),
        .D(dout_i[18]),
        .Q(\sig_ip2bus_data_reg[10] [18]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_dm.dout_i_reg[19] 
       (.C(s_axi_aclk),
        .CE(sig_rd_rlen_reg),
        .D(dout_i[19]),
        .Q(\sig_ip2bus_data_reg[10] [19]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_dm.dout_i_reg[1] 
       (.C(s_axi_aclk),
        .CE(sig_rd_rlen_reg),
        .D(dout_i[1]),
        .Q(\sig_ip2bus_data_reg[10] [1]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_dm.dout_i_reg[20] 
       (.C(s_axi_aclk),
        .CE(sig_rd_rlen_reg),
        .D(dout_i[20]),
        .Q(\sig_ip2bus_data_reg[10] [20]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_dm.dout_i_reg[21] 
       (.C(s_axi_aclk),
        .CE(sig_rd_rlen_reg),
        .D(dout_i[21]),
        .Q(\sig_ip2bus_data_reg[10] [21]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_dm.dout_i_reg[2] 
       (.C(s_axi_aclk),
        .CE(sig_rd_rlen_reg),
        .D(dout_i[2]),
        .Q(\sig_ip2bus_data_reg[10] [2]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_dm.dout_i_reg[3] 
       (.C(s_axi_aclk),
        .CE(sig_rd_rlen_reg),
        .D(dout_i[3]),
        .Q(\sig_ip2bus_data_reg[10] [3]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_dm.dout_i_reg[4] 
       (.C(s_axi_aclk),
        .CE(sig_rd_rlen_reg),
        .D(dout_i[4]),
        .Q(\sig_ip2bus_data_reg[10] [4]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_dm.dout_i_reg[5] 
       (.C(s_axi_aclk),
        .CE(sig_rd_rlen_reg),
        .D(dout_i[5]),
        .Q(\sig_ip2bus_data_reg[10] [5]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_dm.dout_i_reg[6] 
       (.C(s_axi_aclk),
        .CE(sig_rd_rlen_reg),
        .D(dout_i[6]),
        .Q(\sig_ip2bus_data_reg[10] [6]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_dm.dout_i_reg[7] 
       (.C(s_axi_aclk),
        .CE(sig_rd_rlen_reg),
        .D(dout_i[7]),
        .Q(\sig_ip2bus_data_reg[10] [7]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_dm.dout_i_reg[8] 
       (.C(s_axi_aclk),
        .CE(sig_rd_rlen_reg),
        .D(dout_i[8]),
        .Q(\sig_ip2bus_data_reg[10] [8]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \goreg_dm.dout_i_reg[9] 
       (.C(s_axi_aclk),
        .CE(sig_rd_rlen_reg),
        .D(dout_i[9]),
        .Q(\sig_ip2bus_data_reg[10] [9]),
        .R(SR));
endmodule

(* ORIG_REF_NAME = "fifo_generator_v13_1_3_rd_bin_cntr" *) 
module axi_fifo_mm_s_0_fifo_generator_v13_1_3_rd_bin_cntr
   (ram_empty_i_reg,
    comp0,
    \gpr1.dout_i_reg[21] ,
    ram_full_i_reg,
    Q,
    \gcc0.gc0.count_d1_reg[6] ,
    \gcc0.gc0.count_d1_reg[2] ,
    \gcc0.gc0.count_reg[6] ,
    ram_full_fb_i_reg,
    sig_str_rst_reg,
    E,
    \gcc0.gc0.count_reg[0] ,
    s_axi_aclk);
  output ram_empty_i_reg;
  output comp0;
  output [6:0]\gpr1.dout_i_reg[21] ;
  output ram_full_i_reg;
  output [3:0]Q;
  input [4:0]\gcc0.gc0.count_d1_reg[6] ;
  input \gcc0.gc0.count_d1_reg[2] ;
  input [2:0]\gcc0.gc0.count_reg[6] ;
  input ram_full_fb_i_reg;
  input sig_str_rst_reg;
  input [0:0]E;
  input \gcc0.gc0.count_reg[0] ;
  input s_axi_aclk;

  wire [0:0]E;
  wire [3:0]Q;
  wire comp0;
  wire \gc1.count[6]_i_2_n_0 ;
  wire \gcc0.gc0.count_d1_reg[2] ;
  wire [4:0]\gcc0.gc0.count_d1_reg[6] ;
  wire \gcc0.gc0.count_reg[0] ;
  wire [2:0]\gcc0.gc0.count_reg[6] ;
  wire [6:0]\gpr1.dout_i_reg[21] ;
  wire [6:0]plusOp__3;
  wire ram_empty_fb_i_i_4_n_0;
  wire ram_empty_i_reg;
  wire ram_full_fb_i_i_2_n_0;
  wire ram_full_fb_i_reg;
  wire ram_full_i_reg;
  wire [6:4]rd_pntr_plus1;
  wire [6:0]rd_pntr_plus2;
  wire s_axi_aclk;
  wire sig_str_rst_reg;

  LUT1 #(
    .INIT(2'h1)) 
    \gc1.count[0]_i_1 
       (.I0(rd_pntr_plus2[0]),
        .O(plusOp__3[0]));
  LUT2 #(
    .INIT(4'h6)) 
    \gc1.count[1]_i_1 
       (.I0(rd_pntr_plus2[0]),
        .I1(rd_pntr_plus2[1]),
        .O(plusOp__3[1]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \gc1.count[2]_i_1 
       (.I0(rd_pntr_plus2[0]),
        .I1(rd_pntr_plus2[1]),
        .I2(rd_pntr_plus2[2]),
        .O(plusOp__3[2]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \gc1.count[3]_i_1 
       (.I0(rd_pntr_plus2[1]),
        .I1(rd_pntr_plus2[0]),
        .I2(rd_pntr_plus2[2]),
        .I3(rd_pntr_plus2[3]),
        .O(plusOp__3[3]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \gc1.count[4]_i_1 
       (.I0(rd_pntr_plus2[2]),
        .I1(rd_pntr_plus2[0]),
        .I2(rd_pntr_plus2[1]),
        .I3(rd_pntr_plus2[3]),
        .I4(rd_pntr_plus2[4]),
        .O(plusOp__3[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \gc1.count[5]_i_1 
       (.I0(rd_pntr_plus2[3]),
        .I1(rd_pntr_plus2[1]),
        .I2(rd_pntr_plus2[0]),
        .I3(rd_pntr_plus2[2]),
        .I4(rd_pntr_plus2[4]),
        .I5(rd_pntr_plus2[5]),
        .O(plusOp__3[5]));
  LUT3 #(
    .INIT(8'h78)) 
    \gc1.count[6]_i_1 
       (.I0(\gc1.count[6]_i_2_n_0 ),
        .I1(rd_pntr_plus2[5]),
        .I2(rd_pntr_plus2[6]),
        .O(plusOp__3[6]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT5 #(
    .INIT(32'h80000000)) 
    \gc1.count[6]_i_2 
       (.I0(rd_pntr_plus2[4]),
        .I1(rd_pntr_plus2[2]),
        .I2(rd_pntr_plus2[0]),
        .I3(rd_pntr_plus2[1]),
        .I4(rd_pntr_plus2[3]),
        .O(\gc1.count[6]_i_2_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \gc1.count_d1_reg[0] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(rd_pntr_plus2[0]),
        .Q(Q[0]),
        .S(sig_str_rst_reg));
  FDRE #(
    .INIT(1'b0)) 
    \gc1.count_d1_reg[1] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(rd_pntr_plus2[1]),
        .Q(Q[1]),
        .R(sig_str_rst_reg));
  FDRE #(
    .INIT(1'b0)) 
    \gc1.count_d1_reg[2] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(rd_pntr_plus2[2]),
        .Q(Q[2]),
        .R(sig_str_rst_reg));
  FDRE #(
    .INIT(1'b0)) 
    \gc1.count_d1_reg[3] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(rd_pntr_plus2[3]),
        .Q(Q[3]),
        .R(sig_str_rst_reg));
  FDRE #(
    .INIT(1'b0)) 
    \gc1.count_d1_reg[4] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(rd_pntr_plus2[4]),
        .Q(rd_pntr_plus1[4]),
        .R(sig_str_rst_reg));
  FDRE #(
    .INIT(1'b0)) 
    \gc1.count_d1_reg[5] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(rd_pntr_plus2[5]),
        .Q(rd_pntr_plus1[5]),
        .R(sig_str_rst_reg));
  FDRE #(
    .INIT(1'b0)) 
    \gc1.count_d1_reg[6] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(rd_pntr_plus2[6]),
        .Q(rd_pntr_plus1[6]),
        .R(sig_str_rst_reg));
  FDRE #(
    .INIT(1'b0)) 
    \gc1.count_d2_reg[0] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(Q[0]),
        .Q(\gpr1.dout_i_reg[21] [0]),
        .R(sig_str_rst_reg));
  FDRE #(
    .INIT(1'b0)) 
    \gc1.count_d2_reg[1] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(Q[1]),
        .Q(\gpr1.dout_i_reg[21] [1]),
        .R(sig_str_rst_reg));
  FDRE #(
    .INIT(1'b0)) 
    \gc1.count_d2_reg[2] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(Q[2]),
        .Q(\gpr1.dout_i_reg[21] [2]),
        .R(sig_str_rst_reg));
  FDRE #(
    .INIT(1'b0)) 
    \gc1.count_d2_reg[3] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(Q[3]),
        .Q(\gpr1.dout_i_reg[21] [3]),
        .R(sig_str_rst_reg));
  FDRE #(
    .INIT(1'b0)) 
    \gc1.count_d2_reg[4] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(rd_pntr_plus1[4]),
        .Q(\gpr1.dout_i_reg[21] [4]),
        .R(sig_str_rst_reg));
  FDRE #(
    .INIT(1'b0)) 
    \gc1.count_d2_reg[5] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(rd_pntr_plus1[5]),
        .Q(\gpr1.dout_i_reg[21] [5]),
        .R(sig_str_rst_reg));
  FDRE #(
    .INIT(1'b0)) 
    \gc1.count_d2_reg[6] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(rd_pntr_plus1[6]),
        .Q(\gpr1.dout_i_reg[21] [6]),
        .R(sig_str_rst_reg));
  FDRE #(
    .INIT(1'b0)) 
    \gc1.count_reg[0] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(plusOp__3[0]),
        .Q(rd_pntr_plus2[0]),
        .R(sig_str_rst_reg));
  FDSE #(
    .INIT(1'b1)) 
    \gc1.count_reg[1] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(plusOp__3[1]),
        .Q(rd_pntr_plus2[1]),
        .S(sig_str_rst_reg));
  FDRE #(
    .INIT(1'b0)) 
    \gc1.count_reg[2] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(plusOp__3[2]),
        .Q(rd_pntr_plus2[2]),
        .R(sig_str_rst_reg));
  FDRE #(
    .INIT(1'b0)) 
    \gc1.count_reg[3] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(plusOp__3[3]),
        .Q(rd_pntr_plus2[3]),
        .R(sig_str_rst_reg));
  FDRE #(
    .INIT(1'b0)) 
    \gc1.count_reg[4] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(plusOp__3[4]),
        .Q(rd_pntr_plus2[4]),
        .R(sig_str_rst_reg));
  FDRE #(
    .INIT(1'b0)) 
    \gc1.count_reg[5] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(plusOp__3[5]),
        .Q(rd_pntr_plus2[5]),
        .R(sig_str_rst_reg));
  FDRE #(
    .INIT(1'b0)) 
    \gc1.count_reg[6] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(plusOp__3[6]),
        .Q(rd_pntr_plus2[6]),
        .R(sig_str_rst_reg));
  LUT6 #(
    .INIT(64'h1001000000001001)) 
    ram_empty_fb_i_i_2
       (.I0(ram_empty_fb_i_i_4_n_0),
        .I1(\gcc0.gc0.count_d1_reg[2] ),
        .I2(\gcc0.gc0.count_d1_reg[6] [0]),
        .I3(\gpr1.dout_i_reg[21] [0]),
        .I4(\gcc0.gc0.count_d1_reg[6] [1]),
        .I5(\gpr1.dout_i_reg[21] [1]),
        .O(comp0));
  LUT6 #(
    .INIT(64'h6FF6FFFFFFFF6FF6)) 
    ram_empty_fb_i_i_4
       (.I0(\gpr1.dout_i_reg[21] [5]),
        .I1(\gcc0.gc0.count_d1_reg[6] [3]),
        .I2(\gpr1.dout_i_reg[21] [6]),
        .I3(\gcc0.gc0.count_d1_reg[6] [4]),
        .I4(\gcc0.gc0.count_d1_reg[6] [2]),
        .I5(\gpr1.dout_i_reg[21] [4]),
        .O(ram_empty_fb_i_i_4_n_0));
  LUT6 #(
    .INIT(64'h6FF6FFFFFFFF6FF6)) 
    ram_empty_fb_i_i_8
       (.I0(rd_pntr_plus1[5]),
        .I1(\gcc0.gc0.count_d1_reg[6] [3]),
        .I2(rd_pntr_plus1[6]),
        .I3(\gcc0.gc0.count_d1_reg[6] [4]),
        .I4(\gcc0.gc0.count_d1_reg[6] [2]),
        .I5(rd_pntr_plus1[4]),
        .O(ram_empty_i_reg));
  LUT6 #(
    .INIT(64'h020A020F020A020A)) 
    ram_full_fb_i_i_1__1
       (.I0(ram_full_fb_i_reg),
        .I1(comp0),
        .I2(sig_str_rst_reg),
        .I3(E),
        .I4(ram_full_fb_i_i_2_n_0),
        .I5(\gcc0.gc0.count_reg[0] ),
        .O(ram_full_i_reg));
  LUT6 #(
    .INIT(64'h6FF6FFFFFFFF6FF6)) 
    ram_full_fb_i_i_2
       (.I0(\gpr1.dout_i_reg[21] [5]),
        .I1(\gcc0.gc0.count_reg[6] [1]),
        .I2(\gpr1.dout_i_reg[21] [6]),
        .I3(\gcc0.gc0.count_reg[6] [2]),
        .I4(\gcc0.gc0.count_reg[6] [0]),
        .I5(\gpr1.dout_i_reg[21] [4]),
        .O(ram_full_fb_i_i_2_n_0));
endmodule

(* ORIG_REF_NAME = "fifo_generator_v13_1_3_rd_fwft" *) 
module axi_fifo_mm_s_0_fifo_generator_v13_1_3_rd_fwft
   (out,
    p_13_in,
    E,
    \gc1.count_reg[0] ,
    \goreg_dm.dout_i_reg[21] ,
    s_axi_aclk,
    sig_str_rst_reg,
    rx_fg_len_empty_d1,
    ram_full_i_reg,
    axi_str_rxd_tvalid,
    axi_str_rxd_tlast,
    sig_rd_rlen_reg,
    ram_empty_fb_i_reg);
  output out;
  output p_13_in;
  output [0:0]E;
  output [0:0]\gc1.count_reg[0] ;
  output [0:0]\goreg_dm.dout_i_reg[21] ;
  input s_axi_aclk;
  input sig_str_rst_reg;
  input rx_fg_len_empty_d1;
  input ram_full_i_reg;
  input axi_str_rxd_tvalid;
  input axi_str_rxd_tlast;
  input sig_rd_rlen_reg;
  input ram_empty_fb_i_reg;

  wire [0:0]E;
  (* DONT_TOUCH *) wire aempty_fwft_fb_i;
  wire aempty_fwft_fb_i_i_1_n_0;
  (* DONT_TOUCH *) wire aempty_fwft_i;
  wire axi_str_rxd_tlast;
  wire axi_str_rxd_tvalid;
  (* DONT_TOUCH *) wire [1:0]curr_fwft_state;
  (* DONT_TOUCH *) wire empty_fwft_fb_i;
  wire empty_fwft_fb_i_i_1_n_0;
  (* DONT_TOUCH *) wire empty_fwft_fb_o_i;
  wire empty_fwft_fb_o_i_reg0;
  (* DONT_TOUCH *) wire empty_fwft_i;
  wire [0:0]\gc1.count_reg[0] ;
  wire [0:0]\goreg_dm.dout_i_reg[21] ;
  wire [1:0]next_fwft_state;
  wire p_13_in;
  wire ram_empty_fb_i_reg;
  wire ram_full_i_reg;
  wire rx_fg_len_empty_d1;
  wire s_axi_aclk;
  wire sig_rd_rlen_reg;
  wire sig_str_rst_reg;
  (* DONT_TOUCH *) wire user_valid;

  assign out = empty_fwft_i;
  LUT6 #(
    .INIT(64'hFFFEFFAFEAAAAAAA)) 
    aempty_fwft_fb_i_i_1
       (.I0(sig_str_rst_reg),
        .I1(sig_rd_rlen_reg),
        .I2(curr_fwft_state[1]),
        .I3(ram_empty_fb_i_reg),
        .I4(curr_fwft_state[0]),
        .I5(aempty_fwft_fb_i),
        .O(aempty_fwft_fb_i_i_1_n_0));
  (* DONT_TOUCH *) 
  (* KEEP = "yes" *) 
  (* equivalent_register_removal = "no" *) 
  FDRE #(
    .INIT(1'b1)) 
    aempty_fwft_fb_i_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(aempty_fwft_fb_i_i_1_n_0),
        .Q(aempty_fwft_fb_i),
        .R(1'b0));
  (* DONT_TOUCH *) 
  (* KEEP = "yes" *) 
  (* equivalent_register_removal = "no" *) 
  FDRE #(
    .INIT(1'b1)) 
    aempty_fwft_i_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(aempty_fwft_fb_i_i_1_n_0),
        .Q(aempty_fwft_i),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hFFCFCECC)) 
    empty_fwft_fb_i_i_1
       (.I0(sig_rd_rlen_reg),
        .I1(sig_str_rst_reg),
        .I2(curr_fwft_state[1]),
        .I3(curr_fwft_state[0]),
        .I4(empty_fwft_fb_i),
        .O(empty_fwft_fb_i_i_1_n_0));
  (* DONT_TOUCH *) 
  (* KEEP = "yes" *) 
  (* equivalent_register_removal = "no" *) 
  FDRE #(
    .INIT(1'b1)) 
    empty_fwft_fb_i_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(empty_fwft_fb_i_i_1_n_0),
        .Q(empty_fwft_fb_i),
        .R(1'b0));
  LUT4 #(
    .INIT(16'h88EA)) 
    empty_fwft_fb_o_i_i_1
       (.I0(empty_fwft_fb_o_i),
        .I1(curr_fwft_state[0]),
        .I2(sig_rd_rlen_reg),
        .I3(curr_fwft_state[1]),
        .O(empty_fwft_fb_o_i_reg0));
  (* DONT_TOUCH *) 
  (* KEEP = "yes" *) 
  (* equivalent_register_removal = "no" *) 
  FDSE #(
    .INIT(1'b1)) 
    empty_fwft_fb_o_i_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(empty_fwft_fb_o_i_reg0),
        .Q(empty_fwft_fb_o_i),
        .S(sig_str_rst_reg));
  (* DONT_TOUCH *) 
  (* KEEP = "yes" *) 
  (* equivalent_register_removal = "no" *) 
  FDRE #(
    .INIT(1'b1)) 
    empty_fwft_i_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(empty_fwft_fb_i_i_1_n_0),
        .Q(empty_fwft_i),
        .R(1'b0));
  LUT4 #(
    .INIT(16'h00BF)) 
    \gc1.count_d1[6]_i_1 
       (.I0(sig_rd_rlen_reg),
        .I1(curr_fwft_state[0]),
        .I2(curr_fwft_state[1]),
        .I3(ram_empty_fb_i_reg),
        .O(\gc1.count_reg[0] ));
  LUT3 #(
    .INIT(8'hB0)) 
    \goreg_dm.dout_i[21]_i_1 
       (.I0(sig_rd_rlen_reg),
        .I1(curr_fwft_state[0]),
        .I2(curr_fwft_state[1]),
        .O(\goreg_dm.dout_i_reg[21] ));
  LUT4 #(
    .INIT(16'h00F7)) 
    \gpr1.dout_i[21]_i_1 
       (.I0(curr_fwft_state[1]),
        .I1(curr_fwft_state[0]),
        .I2(sig_rd_rlen_reg),
        .I3(ram_empty_fb_i_reg),
        .O(E));
  LUT3 #(
    .INIT(8'hF4)) 
    \gpregsm1.curr_fwft_state[0]_i_1__1 
       (.I0(sig_rd_rlen_reg),
        .I1(curr_fwft_state[0]),
        .I2(curr_fwft_state[1]),
        .O(next_fwft_state[0]));
  LUT4 #(
    .INIT(16'h40FF)) 
    \gpregsm1.curr_fwft_state[1]_i_2 
       (.I0(sig_rd_rlen_reg),
        .I1(curr_fwft_state[1]),
        .I2(curr_fwft_state[0]),
        .I3(ram_empty_fb_i_reg),
        .O(next_fwft_state[1]));
  (* DONT_TOUCH *) 
  (* KEEP = "yes" *) 
  (* equivalent_register_removal = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    \gpregsm1.curr_fwft_state_reg[0] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(next_fwft_state[0]),
        .Q(curr_fwft_state[0]),
        .R(sig_str_rst_reg));
  (* DONT_TOUCH *) 
  (* KEEP = "yes" *) 
  (* equivalent_register_removal = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    \gpregsm1.curr_fwft_state_reg[1] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(next_fwft_state[1]),
        .Q(curr_fwft_state[1]),
        .R(sig_str_rst_reg));
  (* DONT_TOUCH *) 
  (* KEEP = "yes" *) 
  (* equivalent_register_removal = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    \gpregsm1.user_valid_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(next_fwft_state[0]),
        .Q(user_valid),
        .R(sig_str_rst_reg));
  LUT5 #(
    .INIT(32'h23222222)) 
    \sig_register_array[0][5]_i_2 
       (.I0(rx_fg_len_empty_d1),
        .I1(empty_fwft_i),
        .I2(ram_full_i_reg),
        .I3(axi_str_rxd_tvalid),
        .I4(axi_str_rxd_tlast),
        .O(p_13_in));
endmodule

(* ORIG_REF_NAME = "fifo_generator_v13_1_3_rd_logic" *) 
module axi_fifo_mm_s_0_fifo_generator_v13_1_3_rd_logic
   (out,
    p_13_in,
    ram_empty_i_reg,
    Q,
    E,
    \gc1.count_reg[0] ,
    \gpr1.dout_i_reg[21] ,
    \goreg_dm.dout_i_reg[21] ,
    ram_full_i_reg,
    s_axi_aclk,
    sig_str_rst_reg,
    rx_fg_len_empty_d1,
    ram_full_i_reg_0,
    axi_str_rxd_tvalid,
    axi_str_rxd_tlast,
    rx_len_wr_en,
    ram_full_fb_i_reg,
    ram_full_fb_i_reg_0,
    \gcc0.gc0.count_d1_reg[6] ,
    sig_rd_rlen_reg,
    \gcc0.gc0.count_d1_reg[2] ,
    \gcc0.gc0.count_reg[6] ,
    \gcc0.gc0.count_reg[0] );
  output out;
  output p_13_in;
  output ram_empty_i_reg;
  output [3:0]Q;
  output [0:0]E;
  output [0:0]\gc1.count_reg[0] ;
  output [6:0]\gpr1.dout_i_reg[21] ;
  output [0:0]\goreg_dm.dout_i_reg[21] ;
  output ram_full_i_reg;
  input s_axi_aclk;
  input sig_str_rst_reg;
  input rx_fg_len_empty_d1;
  input ram_full_i_reg_0;
  input axi_str_rxd_tvalid;
  input axi_str_rxd_tlast;
  input rx_len_wr_en;
  input ram_full_fb_i_reg;
  input ram_full_fb_i_reg_0;
  input [4:0]\gcc0.gc0.count_d1_reg[6] ;
  input sig_rd_rlen_reg;
  input \gcc0.gc0.count_d1_reg[2] ;
  input [2:0]\gcc0.gc0.count_reg[6] ;
  input \gcc0.gc0.count_reg[0] ;

  wire [0:0]E;
  wire [3:0]Q;
  wire axi_str_rxd_tlast;
  wire axi_str_rxd_tvalid;
  wire comp0;
  wire [0:0]\gc1.count_reg[0] ;
  wire \gcc0.gc0.count_d1_reg[2] ;
  wire [4:0]\gcc0.gc0.count_d1_reg[6] ;
  wire \gcc0.gc0.count_reg[0] ;
  wire [2:0]\gcc0.gc0.count_reg[6] ;
  wire [0:0]\goreg_dm.dout_i_reg[21] ;
  wire [6:0]\gpr1.dout_i_reg[21] ;
  wire out;
  wire p_13_in;
  wire p_2_out;
  wire ram_empty_i_reg;
  wire ram_full_fb_i_reg;
  wire ram_full_fb_i_reg_0;
  wire ram_full_i_reg;
  wire ram_full_i_reg_0;
  wire rx_fg_len_empty_d1;
  wire rx_len_wr_en;
  wire s_axi_aclk;
  wire sig_rd_rlen_reg;
  wire sig_str_rst_reg;

  axi_fifo_mm_s_0_fifo_generator_v13_1_3_rd_fwft \gr1.gr1_int.rfwft 
       (.E(E),
        .axi_str_rxd_tlast(axi_str_rxd_tlast),
        .axi_str_rxd_tvalid(axi_str_rxd_tvalid),
        .\gc1.count_reg[0] (\gc1.count_reg[0] ),
        .\goreg_dm.dout_i_reg[21] (\goreg_dm.dout_i_reg[21] ),
        .out(out),
        .p_13_in(p_13_in),
        .ram_empty_fb_i_reg(p_2_out),
        .ram_full_i_reg(ram_full_i_reg_0),
        .rx_fg_len_empty_d1(rx_fg_len_empty_d1),
        .s_axi_aclk(s_axi_aclk),
        .sig_rd_rlen_reg(sig_rd_rlen_reg),
        .sig_str_rst_reg(sig_str_rst_reg));
  axi_fifo_mm_s_0_fifo_generator_v13_1_3_rd_status_flags_ss \grss.rsts 
       (.comp0(comp0),
        .out(p_2_out),
        .ram_full_fb_i_reg(ram_full_fb_i_reg),
        .ram_full_fb_i_reg_0(ram_full_fb_i_reg_0),
        .rx_len_wr_en(rx_len_wr_en),
        .s_axi_aclk(s_axi_aclk),
        .sig_str_rst_reg(sig_str_rst_reg));
  axi_fifo_mm_s_0_fifo_generator_v13_1_3_rd_bin_cntr rpntr
       (.E(\gc1.count_reg[0] ),
        .Q(Q),
        .comp0(comp0),
        .\gcc0.gc0.count_d1_reg[2] (\gcc0.gc0.count_d1_reg[2] ),
        .\gcc0.gc0.count_d1_reg[6] (\gcc0.gc0.count_d1_reg[6] ),
        .\gcc0.gc0.count_reg[0] (\gcc0.gc0.count_reg[0] ),
        .\gcc0.gc0.count_reg[6] (\gcc0.gc0.count_reg[6] ),
        .\gpr1.dout_i_reg[21] (\gpr1.dout_i_reg[21] ),
        .ram_empty_i_reg(ram_empty_i_reg),
        .ram_full_fb_i_reg(ram_full_fb_i_reg),
        .ram_full_i_reg(ram_full_i_reg),
        .s_axi_aclk(s_axi_aclk),
        .sig_str_rst_reg(sig_str_rst_reg));
endmodule

(* ORIG_REF_NAME = "fifo_generator_v13_1_3_rd_status_flags_ss" *) 
module axi_fifo_mm_s_0_fifo_generator_v13_1_3_rd_status_flags_ss
   (out,
    s_axi_aclk,
    comp0,
    rx_len_wr_en,
    ram_full_fb_i_reg,
    sig_str_rst_reg,
    ram_full_fb_i_reg_0);
  output out;
  input s_axi_aclk;
  input comp0;
  input rx_len_wr_en;
  input ram_full_fb_i_reg;
  input sig_str_rst_reg;
  input ram_full_fb_i_reg_0;

  wire comp0;
  (* DONT_TOUCH *) wire ram_empty_fb_i;
  wire ram_empty_fb_i_i_1__1_n_0;
  (* DONT_TOUCH *) wire ram_empty_i;
  wire ram_full_fb_i_reg;
  wire ram_full_fb_i_reg_0;
  wire rx_len_wr_en;
  wire s_axi_aclk;
  wire sig_str_rst_reg;

  assign out = ram_empty_fb_i;
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFAA2A)) 
    ram_empty_fb_i_i_1__1
       (.I0(ram_empty_fb_i),
        .I1(comp0),
        .I2(rx_len_wr_en),
        .I3(ram_full_fb_i_reg),
        .I4(sig_str_rst_reg),
        .I5(ram_full_fb_i_reg_0),
        .O(ram_empty_fb_i_i_1__1_n_0));
  (* DONT_TOUCH *) 
  (* KEEP = "yes" *) 
  (* equivalent_register_removal = "no" *) 
  FDRE #(
    .INIT(1'b1)) 
    ram_empty_fb_i_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(ram_empty_fb_i_i_1__1_n_0),
        .Q(ram_empty_fb_i),
        .R(1'b0));
  (* DONT_TOUCH *) 
  (* KEEP = "yes" *) 
  (* equivalent_register_removal = "no" *) 
  FDRE #(
    .INIT(1'b1)) 
    ram_empty_i_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(ram_empty_fb_i_i_1__1_n_0),
        .Q(ram_empty_i),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "fifo_generator_v13_1_3_synth" *) 
module axi_fifo_mm_s_0_fifo_generator_v13_1_3_synth
   (out,
    \gcc0.gc0.count_d1_reg[6] ,
    SR,
    \gpr1.dout_i_reg[0] ,
    \gpr1.dout_i_reg[1] ,
    \gpr1.dout_i_reg[2] ,
    \gpr1.dout_i_reg[0]_0 ,
    \gpr1.dout_i_reg[1]_0 ,
    \gpr1.dout_i_reg[2]_0 ,
    \gpr1.dout_i_reg[3] ,
    \gpr1.dout_i_reg[4] ,
    \gpr1.dout_i_reg[5] ,
    \gpr1.dout_i_reg[3]_0 ,
    \gpr1.dout_i_reg[4]_0 ,
    \gpr1.dout_i_reg[5]_0 ,
    \gpr1.dout_i_reg[6] ,
    \gpr1.dout_i_reg[7] ,
    \gpr1.dout_i_reg[8] ,
    \gpr1.dout_i_reg[6]_0 ,
    \gpr1.dout_i_reg[7]_0 ,
    \gpr1.dout_i_reg[8]_0 ,
    \gpr1.dout_i_reg[9] ,
    \gpr1.dout_i_reg[10] ,
    \gpr1.dout_i_reg[11] ,
    \gpr1.dout_i_reg[9]_0 ,
    \gpr1.dout_i_reg[10]_0 ,
    \gpr1.dout_i_reg[11]_0 ,
    \gpr1.dout_i_reg[12] ,
    \gpr1.dout_i_reg[13] ,
    \gpr1.dout_i_reg[14] ,
    \gpr1.dout_i_reg[12]_0 ,
    \gpr1.dout_i_reg[13]_0 ,
    \gpr1.dout_i_reg[14]_0 ,
    \gpr1.dout_i_reg[15] ,
    \gpr1.dout_i_reg[16] ,
    \gpr1.dout_i_reg[17] ,
    \gpr1.dout_i_reg[15]_0 ,
    \gpr1.dout_i_reg[16]_0 ,
    \gpr1.dout_i_reg[17]_0 ,
    \gpr1.dout_i_reg[18] ,
    \gpr1.dout_i_reg[19] ,
    \gpr1.dout_i_reg[20] ,
    \gpr1.dout_i_reg[18]_0 ,
    \gpr1.dout_i_reg[19]_0 ,
    \gpr1.dout_i_reg[20]_0 ,
    \gpr1.dout_i_reg[21] ,
    \gpr1.dout_i_reg[21]_0 ,
    p_13_in,
    Q,
    \gpr1.dout_i_reg[21]_1 ,
    \sig_ip2bus_data_reg[10] ,
    s_axi_aclk,
    fg_rxd_wr_length,
    ram_full_fb_i_reg,
    rx_fg_len_empty_d1,
    ram_full_i_reg,
    axi_str_rxd_tvalid,
    axi_str_rxd_tlast,
    rx_len_wr_en,
    sig_rd_rlen_reg,
    s_axi_aresetn,
    sig_str_rst_reg,
    sig_rx_channel_reset_reg,
    D);
  output out;
  output \gcc0.gc0.count_d1_reg[6] ;
  output [0:0]SR;
  output \gpr1.dout_i_reg[0] ;
  output \gpr1.dout_i_reg[1] ;
  output \gpr1.dout_i_reg[2] ;
  output \gpr1.dout_i_reg[0]_0 ;
  output \gpr1.dout_i_reg[1]_0 ;
  output \gpr1.dout_i_reg[2]_0 ;
  output \gpr1.dout_i_reg[3] ;
  output \gpr1.dout_i_reg[4] ;
  output \gpr1.dout_i_reg[5] ;
  output \gpr1.dout_i_reg[3]_0 ;
  output \gpr1.dout_i_reg[4]_0 ;
  output \gpr1.dout_i_reg[5]_0 ;
  output \gpr1.dout_i_reg[6] ;
  output \gpr1.dout_i_reg[7] ;
  output \gpr1.dout_i_reg[8] ;
  output \gpr1.dout_i_reg[6]_0 ;
  output \gpr1.dout_i_reg[7]_0 ;
  output \gpr1.dout_i_reg[8]_0 ;
  output \gpr1.dout_i_reg[9] ;
  output \gpr1.dout_i_reg[10] ;
  output \gpr1.dout_i_reg[11] ;
  output \gpr1.dout_i_reg[9]_0 ;
  output \gpr1.dout_i_reg[10]_0 ;
  output \gpr1.dout_i_reg[11]_0 ;
  output \gpr1.dout_i_reg[12] ;
  output \gpr1.dout_i_reg[13] ;
  output \gpr1.dout_i_reg[14] ;
  output \gpr1.dout_i_reg[12]_0 ;
  output \gpr1.dout_i_reg[13]_0 ;
  output \gpr1.dout_i_reg[14]_0 ;
  output \gpr1.dout_i_reg[15] ;
  output \gpr1.dout_i_reg[16] ;
  output \gpr1.dout_i_reg[17] ;
  output \gpr1.dout_i_reg[15]_0 ;
  output \gpr1.dout_i_reg[16]_0 ;
  output \gpr1.dout_i_reg[17]_0 ;
  output \gpr1.dout_i_reg[18] ;
  output \gpr1.dout_i_reg[19] ;
  output \gpr1.dout_i_reg[20] ;
  output \gpr1.dout_i_reg[18]_0 ;
  output \gpr1.dout_i_reg[19]_0 ;
  output \gpr1.dout_i_reg[20]_0 ;
  output \gpr1.dout_i_reg[21] ;
  output \gpr1.dout_i_reg[21]_0 ;
  output p_13_in;
  output [0:0]Q;
  output [0:0]\gpr1.dout_i_reg[21]_1 ;
  output [21:0]\sig_ip2bus_data_reg[10] ;
  input s_axi_aclk;
  input [20:0]fg_rxd_wr_length;
  input ram_full_fb_i_reg;
  input rx_fg_len_empty_d1;
  input ram_full_i_reg;
  input axi_str_rxd_tvalid;
  input axi_str_rxd_tlast;
  input rx_len_wr_en;
  input sig_rd_rlen_reg;
  input s_axi_aresetn;
  input sig_str_rst_reg;
  input sig_rx_channel_reset_reg;
  input [21:0]D;

  wire [21:0]D;
  wire [0:0]Q;
  wire [0:0]SR;
  wire axi_str_rxd_tlast;
  wire axi_str_rxd_tvalid;
  wire [20:0]fg_rxd_wr_length;
  wire \gcc0.gc0.count_d1_reg[6] ;
  wire \gpr1.dout_i_reg[0] ;
  wire \gpr1.dout_i_reg[0]_0 ;
  wire \gpr1.dout_i_reg[10] ;
  wire \gpr1.dout_i_reg[10]_0 ;
  wire \gpr1.dout_i_reg[11] ;
  wire \gpr1.dout_i_reg[11]_0 ;
  wire \gpr1.dout_i_reg[12] ;
  wire \gpr1.dout_i_reg[12]_0 ;
  wire \gpr1.dout_i_reg[13] ;
  wire \gpr1.dout_i_reg[13]_0 ;
  wire \gpr1.dout_i_reg[14] ;
  wire \gpr1.dout_i_reg[14]_0 ;
  wire \gpr1.dout_i_reg[15] ;
  wire \gpr1.dout_i_reg[15]_0 ;
  wire \gpr1.dout_i_reg[16] ;
  wire \gpr1.dout_i_reg[16]_0 ;
  wire \gpr1.dout_i_reg[17] ;
  wire \gpr1.dout_i_reg[17]_0 ;
  wire \gpr1.dout_i_reg[18] ;
  wire \gpr1.dout_i_reg[18]_0 ;
  wire \gpr1.dout_i_reg[19] ;
  wire \gpr1.dout_i_reg[19]_0 ;
  wire \gpr1.dout_i_reg[1] ;
  wire \gpr1.dout_i_reg[1]_0 ;
  wire \gpr1.dout_i_reg[20] ;
  wire \gpr1.dout_i_reg[20]_0 ;
  wire \gpr1.dout_i_reg[21] ;
  wire \gpr1.dout_i_reg[21]_0 ;
  wire [0:0]\gpr1.dout_i_reg[21]_1 ;
  wire \gpr1.dout_i_reg[2] ;
  wire \gpr1.dout_i_reg[2]_0 ;
  wire \gpr1.dout_i_reg[3] ;
  wire \gpr1.dout_i_reg[3]_0 ;
  wire \gpr1.dout_i_reg[4] ;
  wire \gpr1.dout_i_reg[4]_0 ;
  wire \gpr1.dout_i_reg[5] ;
  wire \gpr1.dout_i_reg[5]_0 ;
  wire \gpr1.dout_i_reg[6] ;
  wire \gpr1.dout_i_reg[6]_0 ;
  wire \gpr1.dout_i_reg[7] ;
  wire \gpr1.dout_i_reg[7]_0 ;
  wire \gpr1.dout_i_reg[8] ;
  wire \gpr1.dout_i_reg[8]_0 ;
  wire \gpr1.dout_i_reg[9] ;
  wire \gpr1.dout_i_reg[9]_0 ;
  wire out;
  wire p_13_in;
  wire ram_full_fb_i_reg;
  wire ram_full_i_reg;
  wire rx_fg_len_empty_d1;
  wire rx_len_wr_en;
  wire s_axi_aclk;
  wire s_axi_aresetn;
  wire [21:0]\sig_ip2bus_data_reg[10] ;
  wire sig_rd_rlen_reg;
  wire sig_rx_channel_reset_reg;
  wire sig_str_rst_reg;

  axi_fifo_mm_s_0_fifo_generator_v13_1_3_fifo_generator_top \gconvfifo.rf 
       (.D(D),
        .Q(Q),
        .axi_str_rxd_tlast(axi_str_rxd_tlast),
        .axi_str_rxd_tvalid(axi_str_rxd_tvalid),
        .fg_rxd_wr_length(fg_rxd_wr_length),
        .\gcc0.gc0.count_d1_reg[6] (\gcc0.gc0.count_d1_reg[6] ),
        .\gpr1.dout_i_reg[0] (SR),
        .\gpr1.dout_i_reg[0]_0 (\gpr1.dout_i_reg[0] ),
        .\gpr1.dout_i_reg[0]_1 (\gpr1.dout_i_reg[0]_0 ),
        .\gpr1.dout_i_reg[10] (\gpr1.dout_i_reg[10] ),
        .\gpr1.dout_i_reg[10]_0 (\gpr1.dout_i_reg[10]_0 ),
        .\gpr1.dout_i_reg[11] (\gpr1.dout_i_reg[11] ),
        .\gpr1.dout_i_reg[11]_0 (\gpr1.dout_i_reg[11]_0 ),
        .\gpr1.dout_i_reg[12] (\gpr1.dout_i_reg[12] ),
        .\gpr1.dout_i_reg[12]_0 (\gpr1.dout_i_reg[12]_0 ),
        .\gpr1.dout_i_reg[13] (\gpr1.dout_i_reg[13] ),
        .\gpr1.dout_i_reg[13]_0 (\gpr1.dout_i_reg[13]_0 ),
        .\gpr1.dout_i_reg[14] (\gpr1.dout_i_reg[14] ),
        .\gpr1.dout_i_reg[14]_0 (\gpr1.dout_i_reg[14]_0 ),
        .\gpr1.dout_i_reg[15] (\gpr1.dout_i_reg[15] ),
        .\gpr1.dout_i_reg[15]_0 (\gpr1.dout_i_reg[15]_0 ),
        .\gpr1.dout_i_reg[16] (\gpr1.dout_i_reg[16] ),
        .\gpr1.dout_i_reg[16]_0 (\gpr1.dout_i_reg[16]_0 ),
        .\gpr1.dout_i_reg[17] (\gpr1.dout_i_reg[17] ),
        .\gpr1.dout_i_reg[17]_0 (\gpr1.dout_i_reg[17]_0 ),
        .\gpr1.dout_i_reg[18] (\gpr1.dout_i_reg[18] ),
        .\gpr1.dout_i_reg[18]_0 (\gpr1.dout_i_reg[18]_0 ),
        .\gpr1.dout_i_reg[19] (\gpr1.dout_i_reg[19] ),
        .\gpr1.dout_i_reg[19]_0 (\gpr1.dout_i_reg[19]_0 ),
        .\gpr1.dout_i_reg[1] (\gpr1.dout_i_reg[1] ),
        .\gpr1.dout_i_reg[1]_0 (\gpr1.dout_i_reg[1]_0 ),
        .\gpr1.dout_i_reg[20] (\gpr1.dout_i_reg[20] ),
        .\gpr1.dout_i_reg[20]_0 (\gpr1.dout_i_reg[20]_0 ),
        .\gpr1.dout_i_reg[21] (\gpr1.dout_i_reg[21] ),
        .\gpr1.dout_i_reg[21]_0 (\gpr1.dout_i_reg[21]_0 ),
        .\gpr1.dout_i_reg[21]_1 (\gpr1.dout_i_reg[21]_1 ),
        .\gpr1.dout_i_reg[2] (\gpr1.dout_i_reg[2] ),
        .\gpr1.dout_i_reg[2]_0 (\gpr1.dout_i_reg[2]_0 ),
        .\gpr1.dout_i_reg[3] (\gpr1.dout_i_reg[3] ),
        .\gpr1.dout_i_reg[3]_0 (\gpr1.dout_i_reg[3]_0 ),
        .\gpr1.dout_i_reg[4] (\gpr1.dout_i_reg[4] ),
        .\gpr1.dout_i_reg[4]_0 (\gpr1.dout_i_reg[4]_0 ),
        .\gpr1.dout_i_reg[5] (\gpr1.dout_i_reg[5] ),
        .\gpr1.dout_i_reg[5]_0 (\gpr1.dout_i_reg[5]_0 ),
        .\gpr1.dout_i_reg[6] (\gpr1.dout_i_reg[6] ),
        .\gpr1.dout_i_reg[6]_0 (\gpr1.dout_i_reg[6]_0 ),
        .\gpr1.dout_i_reg[7] (\gpr1.dout_i_reg[7] ),
        .\gpr1.dout_i_reg[7]_0 (\gpr1.dout_i_reg[7]_0 ),
        .\gpr1.dout_i_reg[8] (\gpr1.dout_i_reg[8] ),
        .\gpr1.dout_i_reg[8]_0 (\gpr1.dout_i_reg[8]_0 ),
        .\gpr1.dout_i_reg[9] (\gpr1.dout_i_reg[9] ),
        .\gpr1.dout_i_reg[9]_0 (\gpr1.dout_i_reg[9]_0 ),
        .out(out),
        .p_13_in(p_13_in),
        .ram_full_fb_i_reg(ram_full_fb_i_reg),
        .ram_full_i_reg(ram_full_i_reg),
        .rx_fg_len_empty_d1(rx_fg_len_empty_d1),
        .rx_len_wr_en(rx_len_wr_en),
        .s_axi_aclk(s_axi_aclk),
        .s_axi_aresetn(s_axi_aresetn),
        .\sig_ip2bus_data_reg[10] (\sig_ip2bus_data_reg[10] ),
        .sig_rd_rlen_reg(sig_rd_rlen_reg),
        .sig_rx_channel_reset_reg(sig_rx_channel_reset_reg),
        .sig_str_rst_reg(sig_str_rst_reg));
endmodule

(* ORIG_REF_NAME = "fifo_generator_v13_1_3_wr_bin_cntr" *) 
module axi_fifo_mm_s_0_fifo_generator_v13_1_3_wr_bin_cntr
   (ram_empty_i_reg,
    Q,
    ram_full_i_reg,
    ram_full_i_reg_0,
    \gcc0.gc0.count_d1_reg[6]_0 ,
    sig_rd_rlen_reg,
    out,
    rx_len_wr_en,
    \gc1.count_d1_reg[5] ,
    \gc1.count_d1_reg[3] ,
    E,
    \gc1.count_d2_reg[3] ,
    SR,
    s_axi_aclk);
  output ram_empty_i_reg;
  output [6:0]Q;
  output ram_full_i_reg;
  output ram_full_i_reg_0;
  output [2:0]\gcc0.gc0.count_d1_reg[6]_0 ;
  input [0:0]sig_rd_rlen_reg;
  input out;
  input rx_len_wr_en;
  input \gc1.count_d1_reg[5] ;
  input [3:0]\gc1.count_d1_reg[3] ;
  input [0:0]E;
  input [3:0]\gc1.count_d2_reg[3] ;
  input [0:0]SR;
  input s_axi_aclk;

  wire [0:0]E;
  wire [6:0]Q;
  wire [0:0]SR;
  wire [3:0]\gc1.count_d1_reg[3] ;
  wire \gc1.count_d1_reg[5] ;
  wire [3:0]\gc1.count_d2_reg[3] ;
  wire \gcc0.gc0.count[6]_i_2_n_0 ;
  wire [2:0]\gcc0.gc0.count_d1_reg[6]_0 ;
  wire out;
  wire [3:0]p_12_out;
  wire [6:0]plusOp__4;
  wire ram_empty_fb_i_i_6_n_0;
  wire ram_empty_fb_i_i_7_n_0;
  wire ram_empty_i_reg;
  wire ram_full_fb_i_i_4_n_0;
  wire ram_full_i_reg;
  wire ram_full_i_reg_0;
  wire rx_len_wr_en;
  wire s_axi_aclk;
  wire [0:0]sig_rd_rlen_reg;

  LUT1 #(
    .INIT(2'h1)) 
    \gcc0.gc0.count[0]_i_1__0 
       (.I0(p_12_out[0]),
        .O(plusOp__4[0]));
  LUT2 #(
    .INIT(4'h6)) 
    \gcc0.gc0.count[1]_i_1__0 
       (.I0(p_12_out[0]),
        .I1(p_12_out[1]),
        .O(plusOp__4[1]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \gcc0.gc0.count[2]_i_1__0 
       (.I0(p_12_out[0]),
        .I1(p_12_out[1]),
        .I2(p_12_out[2]),
        .O(plusOp__4[2]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \gcc0.gc0.count[3]_i_1__0 
       (.I0(p_12_out[1]),
        .I1(p_12_out[0]),
        .I2(p_12_out[2]),
        .I3(p_12_out[3]),
        .O(plusOp__4[3]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \gcc0.gc0.count[4]_i_1__0 
       (.I0(p_12_out[2]),
        .I1(p_12_out[0]),
        .I2(p_12_out[1]),
        .I3(p_12_out[3]),
        .I4(\gcc0.gc0.count_d1_reg[6]_0 [0]),
        .O(plusOp__4[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \gcc0.gc0.count[5]_i_1__0 
       (.I0(p_12_out[3]),
        .I1(p_12_out[1]),
        .I2(p_12_out[0]),
        .I3(p_12_out[2]),
        .I4(\gcc0.gc0.count_d1_reg[6]_0 [0]),
        .I5(\gcc0.gc0.count_d1_reg[6]_0 [1]),
        .O(plusOp__4[5]));
  LUT3 #(
    .INIT(8'h78)) 
    \gcc0.gc0.count[6]_i_1__0 
       (.I0(\gcc0.gc0.count[6]_i_2_n_0 ),
        .I1(\gcc0.gc0.count_d1_reg[6]_0 [1]),
        .I2(\gcc0.gc0.count_d1_reg[6]_0 [2]),
        .O(plusOp__4[6]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT5 #(
    .INIT(32'h80000000)) 
    \gcc0.gc0.count[6]_i_2 
       (.I0(\gcc0.gc0.count_d1_reg[6]_0 [0]),
        .I1(p_12_out[2]),
        .I2(p_12_out[0]),
        .I3(p_12_out[1]),
        .I4(p_12_out[3]),
        .O(\gcc0.gc0.count[6]_i_2_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \gcc0.gc0.count_d1_reg[0] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(p_12_out[0]),
        .Q(Q[0]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \gcc0.gc0.count_d1_reg[1] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(p_12_out[1]),
        .Q(Q[1]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \gcc0.gc0.count_d1_reg[2] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(p_12_out[2]),
        .Q(Q[2]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \gcc0.gc0.count_d1_reg[3] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(p_12_out[3]),
        .Q(Q[3]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \gcc0.gc0.count_d1_reg[4] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(\gcc0.gc0.count_d1_reg[6]_0 [0]),
        .Q(Q[4]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \gcc0.gc0.count_d1_reg[5] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(\gcc0.gc0.count_d1_reg[6]_0 [1]),
        .Q(Q[5]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \gcc0.gc0.count_d1_reg[6] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(\gcc0.gc0.count_d1_reg[6]_0 [2]),
        .Q(Q[6]),
        .R(SR));
  FDSE #(
    .INIT(1'b1)) 
    \gcc0.gc0.count_reg[0] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(plusOp__4[0]),
        .Q(p_12_out[0]),
        .S(SR));
  FDRE #(
    .INIT(1'b0)) 
    \gcc0.gc0.count_reg[1] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(plusOp__4[1]),
        .Q(p_12_out[1]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \gcc0.gc0.count_reg[2] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(plusOp__4[2]),
        .Q(p_12_out[2]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \gcc0.gc0.count_reg[3] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(plusOp__4[3]),
        .Q(p_12_out[3]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \gcc0.gc0.count_reg[4] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(plusOp__4[4]),
        .Q(\gcc0.gc0.count_d1_reg[6]_0 [0]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \gcc0.gc0.count_reg[5] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(plusOp__4[5]),
        .Q(\gcc0.gc0.count_d1_reg[6]_0 [1]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \gcc0.gc0.count_reg[6] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(plusOp__4[6]),
        .Q(\gcc0.gc0.count_d1_reg[6]_0 [2]),
        .R(SR));
  LUT6 #(
    .INIT(64'h0000000010001010)) 
    ram_empty_fb_i_i_3
       (.I0(ram_empty_fb_i_i_6_n_0),
        .I1(ram_empty_fb_i_i_7_n_0),
        .I2(sig_rd_rlen_reg),
        .I3(out),
        .I4(rx_len_wr_en),
        .I5(\gc1.count_d1_reg[5] ),
        .O(ram_empty_i_reg));
  LUT4 #(
    .INIT(16'h6FF6)) 
    ram_empty_fb_i_i_5
       (.I0(Q[2]),
        .I1(\gc1.count_d2_reg[3] [2]),
        .I2(Q[3]),
        .I3(\gc1.count_d2_reg[3] [3]),
        .O(ram_full_i_reg_0));
  LUT4 #(
    .INIT(16'h6FF6)) 
    ram_empty_fb_i_i_6
       (.I0(Q[0]),
        .I1(\gc1.count_d1_reg[3] [0]),
        .I2(Q[1]),
        .I3(\gc1.count_d1_reg[3] [1]),
        .O(ram_empty_fb_i_i_6_n_0));
  LUT4 #(
    .INIT(16'h6FF6)) 
    ram_empty_fb_i_i_7
       (.I0(Q[2]),
        .I1(\gc1.count_d1_reg[3] [2]),
        .I2(Q[3]),
        .I3(\gc1.count_d1_reg[3] [3]),
        .O(ram_empty_fb_i_i_7_n_0));
  LUT6 #(
    .INIT(64'h2002000000002002)) 
    ram_full_fb_i_i_3
       (.I0(E),
        .I1(ram_full_fb_i_i_4_n_0),
        .I2(p_12_out[0]),
        .I3(\gc1.count_d2_reg[3] [0]),
        .I4(p_12_out[1]),
        .I5(\gc1.count_d2_reg[3] [1]),
        .O(ram_full_i_reg));
  LUT4 #(
    .INIT(16'h6FF6)) 
    ram_full_fb_i_i_4
       (.I0(p_12_out[2]),
        .I1(\gc1.count_d2_reg[3] [2]),
        .I2(p_12_out[3]),
        .I3(\gc1.count_d2_reg[3] [3]),
        .O(ram_full_fb_i_i_4_n_0));
endmodule

(* ORIG_REF_NAME = "fifo_generator_v13_1_3_wr_logic" *) 
module axi_fifo_mm_s_0_fifo_generator_v13_1_3_wr_logic
   (out,
    ram_empty_i_reg,
    Q,
    ram_full_i_reg,
    \gcc0.gc0.count_d1_reg[6] ,
    ram_full_i_reg_0,
    \gpr1.dout_i_reg[0] ,
    ram_full_fb_i_reg,
    s_axi_aclk,
    sig_rd_rlen_reg,
    rx_len_wr_en,
    \gc1.count_d1_reg[5] ,
    \gc1.count_d1_reg[3] ,
    \gc1.count_d2_reg[3] ,
    SR);
  output out;
  output ram_empty_i_reg;
  output [6:0]Q;
  output ram_full_i_reg;
  output [2:0]\gcc0.gc0.count_d1_reg[6] ;
  output ram_full_i_reg_0;
  output \gpr1.dout_i_reg[0] ;
  input ram_full_fb_i_reg;
  input s_axi_aclk;
  input [0:0]sig_rd_rlen_reg;
  input rx_len_wr_en;
  input \gc1.count_d1_reg[5] ;
  input [3:0]\gc1.count_d1_reg[3] ;
  input [3:0]\gc1.count_d2_reg[3] ;
  input [0:0]SR;

  wire [6:0]Q;
  wire [0:0]SR;
  wire [3:0]\gc1.count_d1_reg[3] ;
  wire \gc1.count_d1_reg[5] ;
  wire [3:0]\gc1.count_d2_reg[3] ;
  wire [2:0]\gcc0.gc0.count_d1_reg[6] ;
  wire \gpr1.dout_i_reg[0] ;
  wire out;
  wire p_17_out;
  wire ram_empty_i_reg;
  wire ram_full_fb_i_reg;
  wire ram_full_i_reg;
  wire ram_full_i_reg_0;
  wire rx_len_wr_en;
  wire s_axi_aclk;
  wire [0:0]sig_rd_rlen_reg;

  axi_fifo_mm_s_0_fifo_generator_v13_1_3_wr_status_flags_ss \gwss.wsts 
       (.E(p_17_out),
        .Q(Q[6]),
        .\gpr1.dout_i_reg[0] (\gpr1.dout_i_reg[0] ),
        .out(out),
        .ram_full_fb_i_reg_0(ram_full_fb_i_reg),
        .rx_len_wr_en(rx_len_wr_en),
        .s_axi_aclk(s_axi_aclk));
  axi_fifo_mm_s_0_fifo_generator_v13_1_3_wr_bin_cntr wpntr
       (.E(p_17_out),
        .Q(Q),
        .SR(SR),
        .\gc1.count_d1_reg[3] (\gc1.count_d1_reg[3] ),
        .\gc1.count_d1_reg[5] (\gc1.count_d1_reg[5] ),
        .\gc1.count_d2_reg[3] (\gc1.count_d2_reg[3] ),
        .\gcc0.gc0.count_d1_reg[6]_0 (\gcc0.gc0.count_d1_reg[6] ),
        .out(out),
        .ram_empty_i_reg(ram_empty_i_reg),
        .ram_full_i_reg(ram_full_i_reg),
        .ram_full_i_reg_0(ram_full_i_reg_0),
        .rx_len_wr_en(rx_len_wr_en),
        .s_axi_aclk(s_axi_aclk),
        .sig_rd_rlen_reg(sig_rd_rlen_reg));
endmodule

(* ORIG_REF_NAME = "fifo_generator_v13_1_3_wr_status_flags_ss" *) 
module axi_fifo_mm_s_0_fifo_generator_v13_1_3_wr_status_flags_ss
   (out,
    E,
    \gpr1.dout_i_reg[0] ,
    ram_full_fb_i_reg_0,
    s_axi_aclk,
    rx_len_wr_en,
    Q);
  output out;
  output [0:0]E;
  output \gpr1.dout_i_reg[0] ;
  input ram_full_fb_i_reg_0;
  input s_axi_aclk;
  input rx_len_wr_en;
  input [0:0]Q;

  wire [0:0]E;
  wire [0:0]Q;
  wire \gpr1.dout_i_reg[0] ;
  (* DONT_TOUCH *) wire ram_afull_fb;
  (* DONT_TOUCH *) wire ram_afull_i;
  (* DONT_TOUCH *) wire ram_full_fb_i;
  wire ram_full_fb_i_reg_0;
  (* DONT_TOUCH *) wire ram_full_i;
  wire rx_len_wr_en;
  wire s_axi_aclk;

  assign out = ram_full_fb_i;
  LUT3 #(
    .INIT(8'h04)) 
    RAM_reg_0_63_0_2_i_1
       (.I0(ram_full_fb_i),
        .I1(rx_len_wr_en),
        .I2(Q),
        .O(\gpr1.dout_i_reg[0] ));
  LUT2 #(
    .INIT(4'h2)) 
    \gcc0.gc0.count_d1[6]_i_1 
       (.I0(rx_len_wr_en),
        .I1(ram_full_fb_i),
        .O(E));
  LUT1 #(
    .INIT(2'h2)) 
    i_0
       (.I0(1'b0),
        .O(ram_afull_i));
  LUT1 #(
    .INIT(2'h2)) 
    i_1
       (.I0(1'b0),
        .O(ram_afull_fb));
  (* DONT_TOUCH *) 
  (* KEEP = "yes" *) 
  (* equivalent_register_removal = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    ram_full_fb_i_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(ram_full_fb_i_reg_0),
        .Q(ram_full_fb_i),
        .R(1'b0));
  (* DONT_TOUCH *) 
  (* KEEP = "yes" *) 
  (* equivalent_register_removal = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    ram_full_i_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(ram_full_fb_i_reg_0),
        .Q(ram_full_i),
        .R(1'b0));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (weak1, weak0) GSR = GSR_int;
    assign (weak1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
