`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: University of Idaho
// Sponsor: Thorlabs
// Engineer: Alexis Wilson
// Project: FPGA Data Acquisition and Control
//
// Create Date: 03/28/2020 04:16:10 PM
// Design Name: 
// Module Name: ping_pong_out
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module ping_pong_out(
    input clk,
    input [15:0] in,
    input write_buffer,
    
    output cpu_ready,
    output [15:0] cpu_out
    );
  
// slave logic
logic reset = 0;
logic [15:0] data_in = 0;
logic s_axis_tready = 0;

// master logic
logic m_ready = 0;
logic axis_overflow = 0;

logic [3:0] in_count = 0;

always_ff @(posedge clk)
begin
    if(in == 0)
    begin
        reset = 1;
        m_ready = 0;
    end
    else if (write_buffer == 1 && in_count < 7)
    begin
        reset = 0;
        m_ready = 0;
        data_in = in;
        in_count++;
    end
    else if(write_buffer == 1 && in_count >= 7)
    begin
        reset = 0;
        m_ready = 1;
        data_in = in;
        in_count++;
    end
    else if(write_buffer == 1 && in_count == 15)
    begin
        reset = 1;
        in_count = 0;
        m_ready = 1;
        data_in = in;
    end
end

// FIFO Interface
fifo_generator_0 fifo_generator_01 (
  .s_aclk(clk),                // input wire s_aclk
  .s_aresetn(reset),          // input wire s_aresetn
  .s_axis_tvalid(write_buffer),  // input wire s_axis_tvalid
  .s_axis_tdata(data_in),    // input wire [15 : 0] s_axis_tdata
  .s_axis_tready(s_axis_tready),  // output wire s_axis_tready

  .m_axis_tready(m_ready),  // input wire m_axis_tready
  .m_axis_tvalid(cpu_ready),  // output wire m_axis_tvalid
  .m_axis_tdata(cpu_out)    // output wire [15 : 0] m_axis_tdata
);

endmodule