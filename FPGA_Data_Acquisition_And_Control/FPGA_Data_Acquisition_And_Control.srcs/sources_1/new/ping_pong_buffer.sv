`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: University of Idaho
// Sponsor: Thorlabs
// Engineer: Alexis Wilson
// Project: FPGA Data Acquisition and Control
//
// Create Date: 03/03/2020 03:37:49 PM
// Design Name: 
// Module Name: ping_pong_buffer
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module ping_pong_buffer(
    input clk,
    input [15:0] in1,
    input [15:0] in2,
    
    input dec_out_flag1,
    input dec_out_flag2,
    
    output cpu_ready1,
    output cpu_ready2,
    output [15:0] cpu_out1,
    output [15:0] cpu_out2
    );


// dec_out1 output to cpu
ping_pong_out ping_pong_out1 (
    .clk(clk),
    .in(in1),
    .write_buffer(dec_out_flag1),
    .cpu_ready(cpu_ready1),
    .cpu_out(cpu_out1)
);

// dec_out2 output to cpu
ping_pong_out ping_pong_out2 (
    .clk(clk),
    .in(in2),
    .write_buffer(dec_out_flag2),
    .cpu_ready(cpu_ready2),
    .cpu_out(cpu_out2)
);

endmodule
