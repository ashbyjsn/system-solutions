`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: University of Idaho
// Sponsor: Thorlabs
// Engineer: Alexis Wilson
// Project: FPGA Data Acquisition and Control
// 
// Create Date: 03/03/2020 03:33:48 PM
// Design Name: 
// Module Name: cpu_out
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module cpu_out(
    input clk,
    input [15:0] dec_out1,
    input [15:0] dec_out2,
    input dec_out_flag1,
    input dec_out_flag2,
    
    output cpu_ready1,
    output cpu_ready2,
    output [15:0] cpu_out1,
    output [15:0] cpu_out2
    );
    
FIFO FIFO1 (
    .clk(clk),
    .in(dec_out1),
    .write_fifo(dec_out_flag1),
    .read_fifo(cpu_ready1),
    .cpu_out(cpu_out1)
);

/*FIFO FIFO2 (
    .clk(clk),
    .in(dec_out2),
    .write_fifo(dec_out_flag2),
    .read_fifo(cpu_ready2),
    .cpu_out(cpu_out2)
);*/
    
endmodule
