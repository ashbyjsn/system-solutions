`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: University of Idaho
// Sponsor: Thorlabs
// Engineer: Alexis Wilson
// Project: FPGA Data Acquisition and Control
// 
// Create Date: 02/25/2020 08:39:34 AM
// Design Name: 
// Module Name: adc_to_dac_wrapper
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module adc_to_dac_wrapper(
    // adc inputs and dac outputs
    input adc_clk_i,
    input [13:0] adc_dat_a,
    input [13:0] adc_dat_b,

    //output [13:0] dac_dat_o, // dont need this rn
    
    // AXI interface inputs and outputs
    input s_axis_clk, // need axi clock for data transfer
    input s_axis_resetn, // using axi reset
    input s_axis_tready1,
    
    output s_axis_tvalid1,
    output [15:0] s_axis_tdata1
    
    //input s_axis_tready2,
    //output s_axis_tvalid2,
    //output [15:0] s_axis_tdata2
);

logic dec_out_flag1 = 0;
logic dec_out_flag2 = 0;
logic [15:0] dec_out1;
logic [15:0] dec_out2;

// decimator instantiation
decimator decimator1 (
     .clk(adc_clk_i),
     .in1(adc_dat_a),
     .in2(adc_dat_b),
     .dec_out_flag1(dec_out_flag1),
     .dec_out_flag2(dec_out_flag2),
     .dec_out1(dec_out1),
     .dec_out2(dec_out2)
);

logic [15:0] fom_calc;
logic fom_calc_flag = 0;
// FOM calculation instantiation
fig_of_merit fig_of_merit1(
    .clk(adc_clk_i),
    .in1(dec_out1),
    .in2(dec_out2),
    .dec_out_flag1(dec_out_flag1),
    .dec_out_flag2(dec_out_flag2),
    .fom_calc_flag(fom_calc_flag),
    .fom_calc(fom_calc)
    //.dac_dat_o(dac_dac_o)
);

// declare internal signals equivalent to output
logic s_axis_tdata_1 = 0;
logic s_axis_tready_1 = 0;
logic ready1 = 0;
logic s_axis_tvalid_1 = 0;
logic valid1 = 0;

//logic s_axis_tdata_2;
//logic s_axis_tready_2;
//logic s_axis_tvalid_2;
//logic ready2;
//logic valid2;

// Synchronization portion to seperate clock domain aka FIFO
always_ff @ (posedge adc_clk_i)
begin
    if(s_axis_resetn)
    begin
        s_axis_tdata_1 <= 0;
        valid1 <= 0;
        //s_axis_tdata_2 <= 0;
        //valid2 <= 0;
    end
    else
    begin
        // fom calculation data
        if(fom_calc_flag == 1)
        begin
            s_axis_tdata_1 <= fom_calc;
        end
        
        valid1 = valid1 ^ s_axis_tready1;
        
        // dec_out1 data
        /*if(dec_out_flag1)
        begin
           s_axis_tdata_1 <= dec_out1  
        end*/
        
        // dec_out2 data
        /*if(dec_out_flag2)
        begin
            s_axis_tdata_2 <= dec_out2;
        end*/
        
        // sync slave ready signals
        //valid1 = valid1 ^ s_axis_tready1;
        //valid2 = valid2 ^ s_axis_tready2;
    end
end

logic [1:0] valid_reg_sync1;
logic valid_ff_reg1;
/*logic [1:0] valid_reg_sync2;
logic valid_ff_reg2;*/

always_ff @(posedge s_axis_clk)
begin
    if(valid1)
    begin
        valid_reg_sync1[0] <= valid1;
        valid_reg_sync1[1] <= valid_reg_sync1[0];
        valid_ff_reg1 <= valid_reg_sync1[1];
        s_axis_tvalid_1 <= valid_ff_reg1 ^ valid_reg_sync1[1];
    end
    
    /*if(valid2)
    begin
        valid_reg_sync2[0] <= valid2;
        valid_reg_sync2[1] <= valid_reg_sync2[0];
        valid_ff_reg2 <= valid_reg_sync2[1];
        s_axis_tvalid_2 <= valid_ff_reg2 ^ valid_reg_sync2[1];
    end*/
end

assign s_axis_tdata1 = s_axis_tdata_1;
assign s_axis_tvalid1 = s_axis_tvalid_1;

//assign s_axis_tdata2 = s_axis_tdata_2;
//assign s_axis_tvalid2 = s_axis_tvalid_2;

endmodule