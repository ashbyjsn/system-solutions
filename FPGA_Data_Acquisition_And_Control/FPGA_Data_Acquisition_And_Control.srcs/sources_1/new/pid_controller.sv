`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: University of Idaho
// Sponsor: Thorlabs
// Engineer: Alexis Wilson
// Project: FPGA Data Acquisition and Control
// 
// Create Date: 02/25/2020 08:39:34 AM
// Design Name: 
// Module Name: pid_controller
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module pid_controller(
        input clk,
        input [15:0] fom_calc,
        output [13:0] dac_dat_o
    );

endmodule