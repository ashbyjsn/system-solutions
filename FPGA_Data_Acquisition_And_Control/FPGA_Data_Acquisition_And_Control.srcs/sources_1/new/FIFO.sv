`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: University of Idaho
// Sponsor: Thorlabs
// Engineer: Alexis Wilson
// Project: FPGA Data Acquisition and Control
//
// Create Date: 04/07/2020 12:07:49 PM
// Design Name: 
// Module Name: FIFO
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module FIFO(
    input clk,
    input [15:0] in,
    input write_fifo,
    output read_fifo,
    output [15:0] cpu_out
    );
    
logic [15:0] data_in = 0;
logic reset;
logic s_axis_tready;
logic m_ready = 0;
logic [4:0] count = 0;

typedef enum logic[2:0]
{
    s0,s1,s2,s3,s4
} statetype;

statetype state;

always_ff @(posedge clk)
begin
    case(state)
        s0:
        begin
            if(write_fifo != 0)
            begin
                count = count + 1;
                data_in = in;
                state <= s1;
            end
            else
                state <= s0;
        end
        s1:
        begin
            if(write_fifo == 1)
            begin
                count = count + 1;
                data_in = in;
                state <= s2;
            end
            else
                state <= s1;
        end
        s2:
        begin
            if(count != 5 && write_fifo == 1)
            begin
                count = count + 1;
                data_in = in;
                state <= s2;
            end
            else if(count == 5 && write_fifo == 1)
            begin
                count = count + 1;
                data_in = in;
                state <= s3;
            end
            else 
                state <= s2;
        end
        s3:
        begin
            if(count != 10 && write_fifo == 1)
            begin
                count = count + 1;
                data_in = in;
                state <= s3;
            end
            else if (count == 10 && write_fifo == 1)
            begin
                count = count + 1;
                data_in = in;
                state <= s4;
            end
            else
                state <= s3;
        end
        s4:
        begin
            if(count != 15 && write_fifo == 1)
            begin
                count = count + 1;
                data_in = in;
                state <= s4;
            end
            else if (count == 15 && write_fifo == 1)
            begin
                count = 0;
                data_in = in;
                state <= s4;
            end
            else
                state <= s4;
        end
        
        default: 
            state <= s0;
    endcase
end

always_comb
begin
    reset = 0;
    m_ready = 0;
    
    
    case(state)
        s0: 
        if(in != 0)
        begin
            reset = 0;
        end
        else 
        begin
            reset = 1;
        end
        
        s1:
        m_ready = 0;
        
        s2: 
        if(count == 5)
        begin
            m_ready = 1;
        end
        else 
        begin
            m_ready = 0;
        end
        
        s3: m_ready = 1;
        
        s4: m_ready = 1;
    endcase
end
    
// FIFO Interface
fifo_generator_0 fifo_generator_01 (
  .s_aclk(clk),                // input wire s_aclk
  .s_aresetn(reset),          // input wire s_aresetn
  .s_axis_tvalid(write_fifo),  // input wire s_axis_tvalid
  .s_axis_tdata(data_in),    // input wire [15 : 0] s_axis_tdata
  .s_axis_tready(s_axis_tready),  // output wire s_axis_tready

  .m_axis_tready(m_ready),  // input wire m_axis_tready
  .m_axis_tvalid(read_fifo),  // output wire m_axis_tvalid
  .m_axis_tdata(cpu_out)    // output wire [15 : 0] m_axis_tdata
);

endmodule