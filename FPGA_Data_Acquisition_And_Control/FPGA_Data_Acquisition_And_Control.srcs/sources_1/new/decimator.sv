`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: University of Idaho
// Sponsor: Thorlabs
// Engineer: Alexis Wilson
// Project: FPGA Data Acquisition and Control
// 
// Create Date: 02/25/2020 08:39:34 AM
// Design Name: 
// Module Name: decimator
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module decimator(
    input clk,
    input [13:0] in1,
    input [13:0] in2,
    output dec_out_flag1,
    output dec_out_flag2,
    output [15:0] dec_out1,
    output [15:0] dec_out2
    );

logic [3:0] count1 = 0;
logic [19:0] accum_in1 = 0;
logic [19:0] accum_out1 = 0;
logic [15:0] dec_calc1 = 0;
logic flag1 = 0;

// Accumulator for Input 1
always_ff @(posedge clk)
begin
    // waits for an initial value to come into the system
    if(count1 == 0 && in1 != 0)
    begin
        accum_in1 = in1;
        count1++;
        flag1 = 0;
    end
    // As long as only 15 values have gone in aka countonly reaches 14 it enters this loop
    else if (count1 != 15 && in1 != 0)
    begin
        accum_in1 = accum_in1 + in1;
        count1++;
        flag1 = 0;
    end
    /* means this is the 16th value to be inputted
       assign the new accumulated value to output of accumulator
       and then reset */
    else if(count1 == 15)
    begin
        accum_out1 = accum_in1 + in1;
        count1++;
        accum_in1 = 0;
        flag1 = 1;
    end
    else
    begin
        flag1 = 0;
    end
end

//Decimation for Input 1
always_ff @(posedge clk)
begin
    if(flag1 == 1)
    begin
        dec_calc1 = accum_out1 <<< 4;
    end
end
// how dec_out is assigned until decimation is written
assign dec_out1 = dec_calc1;
assign dec_out_flag1 = flag1;

logic [3:0] count2 = 0;
logic [19:0] accum_in2 = 0;
logic [19:0] accum_out2 = 0;
logic [15:0] dec_calc2 = 0;
logic flag2 = 0;

// Accumulator for Input 2
always_ff @(posedge clk)
begin
    // waits for an initial value to come into the system
    if(count2 == 0 && in2 != 0)
    begin
        accum_in2 = in2;
        count2++;
        flag2 = 0;
    end
    // As long as only 15 values have gone in aka countonly reaches 14 it enters this loop
    else if (count2 != 15 && in2 != 0)
    begin
        accum_in2 = accum_in2 + in2;
        count2++;
        flag2 = 0;
    end
    /* means this is the 16th value to be inputted
       assign the new accumulated value to output of accumulator
       and then reset */
    else if(count2 == 15)
    begin
        accum_out2 = accum_in2 + in2;
        count2++;
        accum_in2 = 0;
        flag2 = 1;
    end
    else
    begin
        flag2 = 0;
    end
end

// Decimation for Input 2
always_ff @(posedge clk)
begin
    if(flag2 == 1)
    begin
        dec_calc2 = accum_out2 <<< 4;
    end
end
// how dec_out is assigned until decimation is written
assign dec_out2 = dec_calc2;
assign dec_out_flag2 = flag2;

endmodule