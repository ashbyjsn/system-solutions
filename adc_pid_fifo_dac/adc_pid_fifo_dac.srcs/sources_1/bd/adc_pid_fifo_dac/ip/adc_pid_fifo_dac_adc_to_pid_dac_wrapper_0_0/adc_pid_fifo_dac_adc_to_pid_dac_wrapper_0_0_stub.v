// Copyright 1986-2016 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2016.4 (win64) Build 1756540 Mon Jan 23 19:11:23 MST 2017
// Date        : Thu Apr 30 17:12:00 2020
// Host        : DESKTOP-0KQQTOC running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub {d:/ECE/CapstoneDesign/ECE
//               483/Code/adc_pid_fifo_dac/adc_pid_fifo_dac.srcs/sources_1/bd/adc_pid_fifo_dac/ip/adc_pid_fifo_dac_adc_to_pid_dac_wrapper_0_0/adc_pid_fifo_dac_adc_to_pid_dac_wrapper_0_0_stub.v}
// Design      : adc_pid_fifo_dac_adc_to_pid_dac_wrapper_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z010clg400-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "adc_to_dac_wrapper,Vivado 2016.4" *)
module adc_pid_fifo_dac_adc_to_pid_dac_wrapper_0_0(adc_clk_i, adc_dat_a, adc_dat_b, dac_dat_o, 
  s_axis_clk, s_axis_resetn, s_axis_tready1, s_axis_tvalid1, s_axis_tdata1)
/* synthesis syn_black_box black_box_pad_pin="adc_clk_i,adc_dat_a[13:0],adc_dat_b[13:0],dac_dat_o[13:0],s_axis_clk,s_axis_resetn,s_axis_tready1,s_axis_tvalid1,s_axis_tdata1[15:0]" */;
  input adc_clk_i;
  input [13:0]adc_dat_a;
  input [13:0]adc_dat_b;
  output [13:0]dac_dat_o;
  input s_axis_clk;
  input s_axis_resetn;
  input s_axis_tready1;
  output s_axis_tvalid1;
  output [15:0]s_axis_tdata1;
endmodule
