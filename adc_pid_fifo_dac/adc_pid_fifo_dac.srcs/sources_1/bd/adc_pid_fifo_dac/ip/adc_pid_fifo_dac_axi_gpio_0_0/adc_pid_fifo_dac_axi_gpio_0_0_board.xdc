#--------------------Physical Constraints-----------------

set_property BOARD_PIN {exp_p_io[0]} [get_ports gpio_io_t[0]]

set_property BOARD_PIN {exp_p_io[1]} [get_ports gpio_io_t[1]]

set_property BOARD_PIN {exp_p_io[2]} [get_ports gpio_io_t[2]]

set_property BOARD_PIN {exp_p_io[3]} [get_ports gpio_io_t[3]]

set_property BOARD_PIN {exp_p_io[4]} [get_ports gpio_io_t[4]]

set_property BOARD_PIN {exp_p_io[5]} [get_ports gpio_io_t[5]]

set_property BOARD_PIN {exp_p_io[6]} [get_ports gpio_io_t[6]]

set_property BOARD_PIN {exp_p_io[7]} [get_ports gpio_io_t[7]]

set_property BOARD_PIN {exp_n_io[0]} [get_ports gpio_io_t[8]]

set_property BOARD_PIN {exp_n_io[1]} [get_ports gpio_io_t[9]]

set_property BOARD_PIN {exp_n_io[2]} [get_ports gpio_io_t[10]]

set_property BOARD_PIN {exp_n_io[3]} [get_ports gpio_io_t[11]]

set_property BOARD_PIN {exp_n_io[4]} [get_ports gpio_io_t[12]]

set_property BOARD_PIN {exp_n_io[5]} [get_ports gpio_io_t[13]]

set_property BOARD_PIN {exp_n_io[6]} [get_ports gpio_io_t[14]]

set_property BOARD_PIN {exp_n_io[7]} [get_ports gpio_io_t[15]]

