`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: University of Idaho
// Sponsor: Thorlabs
// Engineer: Alexis Wilson
// Project: FPGA Data Acquisition and Control
// 
// Create Date: 03/05/2020 01:18:38 PM
// Design Name: 
// Module Name: fig_of_merit
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module fig_of_merit(
    input clk,
    input [15:0] in1,
    input [15:0] in2,
    input dec_out_flag1,
    input dec_out_flag2,
    output fom_calc_flag,
    output [15:0] fom_calc
    //output [13:0] dac_dat_o
    );
    
//sets some base values to save calculation to
logic fom_flag;
logic [15:0] fom = 0;
    
always_ff @(posedge clk)
begin
    // check there is an input and that there was no previous FOM calcultaion
    if(dec_out_flag1 == 1 && dec_out_flag2 == 1)
    begin
        fom = in2/in1;
        fom_flag = 1;
    end
    else
    begin
        fom_flag = 0;
    end
end

assign fom_calc = fom;
assign fom_calc_flag = fom_flag;

endmodule
