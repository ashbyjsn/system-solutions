`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: University of Idaho
// Sponsor: Thorlabs
// Engineer: Alexis Wilson
// Project: FPGA Data Acquisition and Control
// 
// Create Date: 02/25/2020 08:39:34 AM
// Design Name: 
// Module Name: adc_to_dac_wrapper
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module adc_to_dac_wrapper(
    // adc inputs
    input adc_clk_i,
    input [13:0] adc_dat_a,
    input [13:0] adc_dat_b,
    // dac output 
    output [13:0] dac_dat_o,
    
    // AXI interface inputs and outputs
    input s_axis_clk, // need axi clock for data transfer
    input s_axis_resetn, // using axi reset
    input s_axis_tready1,
    
    output s_axis_tvalid1,
    output [15:0] s_axis_tdata1
);

logic dec_out_flag1 = 0;
logic dec_out_flag2 = 0;
logic [15:0] dec_out1;
logic [15:0] dec_out2;

// decimator instantiation
decimator decimator1 (
     .clk(adc_clk_i),
     .in1(adc_dat_a),
     .in2(adc_dat_b),
     .dec_out_flag1(dec_out_flag1),
     .dec_out_flag2(dec_out_flag2),
     .dec_out1(dec_out1),
     .dec_out2(dec_out2)
);

logic [15:0] fom_calc;
logic fom_calc_flag = 0;
// FOM calculation instantiation
fig_of_merit fig_of_merit1(
    .clk(adc_clk_i),
    .in1(dec_out1),
    .in2(dec_out2),
    .dec_out_flag1(dec_out_flag1),
    .dec_out_flag2(dec_out_flag2),
    .fom_calc_flag(fom_calc_flag),
    .fom_calc(fom_calc)
);

logic [15:0] fom_in = 0;
logic [15:0] pid_calc = 0;
logic error_flag = 0;
logic pid_flag = 0;

// pid instantiation
pid pid1(
    .clk(adc_clk_i),
    .reset(s_axis_resetn),
    .fom_calc_flag(fom_calc_flag),
    .fom_in(fom_calc),
    .error_flag(pid_flag),
    .error_calc(pid_calc)
);

logic [15:0] pid_out = 0;
always @(posedge adc_clk_i)
begin
    if(pid_flag)
    begin
        pid_out = pid_calc;
    end
end

assign dac_dat_o = pid_out;

// declare internal signals equivalent to output
logic s_axis_tdata_1 = 0;
logic s_axis_tready_1 = 0;
logic ready1 = 0;
logic s_axis_tvalid_1 = 0;
logic valid1 = 0;

// Synchronization portion to seperate clock domain aka FIFO
always_ff @ (posedge adc_clk_i)
begin
    if(s_axis_resetn)
    begin
        s_axis_tdata_1 <= 0;
        valid1 <= 0;
    end
    else
    begin
        // fom calculation data
        if(fom_calc_flag == 1)
        begin
            s_axis_tdata_1 <= fom_calc;
        end
    end
end

logic [1:0] valid_reg_sync1;
logic valid_ff_reg1;

always_ff @(posedge s_axis_clk)
begin
    if(valid1)
    begin
        valid_reg_sync1[0] <= valid1;
        valid_reg_sync1[1] <= valid_reg_sync1[0];
        valid_ff_reg1 <= valid_reg_sync1[1];
        s_axis_tvalid_1 <= valid_ff_reg1 ^ valid_reg_sync1[1];
    end
end

assign s_axis_tdata1 = s_axis_tdata_1;
assign s_axis_tvalid1 = s_axis_tvalid_1;

endmodule