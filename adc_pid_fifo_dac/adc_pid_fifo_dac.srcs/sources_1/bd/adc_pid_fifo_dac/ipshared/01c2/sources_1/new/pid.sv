`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: University of Idaho
// Sponsor: Thorlabs
// Engineer: Alexis Wilson
// Project: FPGA Data Acquisition and Control
//
// Create Date: 04/30/2020 01:49:52 PM
// Design Name: 
// Module Name: pid
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module pid(
    input clk,
    input reset,
    input fom_calc_flag,
    input [15:0] fom_in,
    
    output error_flag,
    output [15:0] error_calc
    );

logic [3:0] k1 = 4'b0100;
logic [3:0] k2  = 4'b0110;
logic [3:0] k3 = 4'b0111;

logic pid_flag =0;

logic [15:0] error_calc_prev = 0;
logic [15:0] error_in1 = 0;
logic [15:0] error_in2 = 0;

assign error_calc = k1*error_calc_prev + (k2*fom_in - k2*error_in1) + k3*error_in2;

always @(posedge clk)
begin
	if(reset == 1)
    begin
        pid_flag <= 0;
        error_calc_prev <= 0;
        error_in1 <= 0;
		error_in2 <= 0;
	end

	if(fom_calc_flag == 1)
	begin
	   pid_flag <= 1;
	   error_in2 <= error_in1;
	   error_in1 <= fom_in;
	   error_calc_prev <= error_calc;
	end
	else
	begin
	   pid_flag <= 0;
       error_in2 <= error_in2;
       error_in1 <= error_in1;
       error_calc_prev <= error_calc_prev;
	end
end

assign error_flag = pid_flag;

endmodule
