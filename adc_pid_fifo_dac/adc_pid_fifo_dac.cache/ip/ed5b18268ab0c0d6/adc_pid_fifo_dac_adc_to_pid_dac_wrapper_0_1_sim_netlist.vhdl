-- Copyright 1986-2016 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2016.4 (win64) Build 1756540 Mon Jan 23 19:11:23 MST 2017
-- Date        : Thu Apr 30 20:16:31 2020
-- Host        : DESKTOP-0KQQTOC running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ adc_pid_fifo_dac_adc_to_pid_dac_wrapper_0_1_sim_netlist.vhdl
-- Design      : adc_pid_fifo_dac_adc_to_pid_dac_wrapper_0_1
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z010clg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_adc_to_dac_wrapper is
  port (
    adc_clk_i : in STD_LOGIC;
    adc_dat_a : in STD_LOGIC_VECTOR ( 13 downto 0 );
    adc_dat_b : in STD_LOGIC_VECTOR ( 13 downto 0 );
    dac_dat_o : out STD_LOGIC_VECTOR ( 13 downto 0 );
    s_axis_clk : in STD_LOGIC;
    s_axis_resetn : in STD_LOGIC;
    s_axis_tready1 : in STD_LOGIC;
    s_axis_tvalid1 : out STD_LOGIC;
    s_axis_tdata1 : out STD_LOGIC_VECTOR ( 15 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_adc_to_dac_wrapper;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_adc_to_dac_wrapper is
  signal \<const0>\ : STD_LOGIC;
begin
  dac_dat_o(13) <= \<const0>\;
  dac_dat_o(12) <= \<const0>\;
  dac_dat_o(11) <= \<const0>\;
  dac_dat_o(10) <= \<const0>\;
  dac_dat_o(9) <= \<const0>\;
  dac_dat_o(8) <= \<const0>\;
  dac_dat_o(7) <= \<const0>\;
  dac_dat_o(6) <= \<const0>\;
  dac_dat_o(5) <= \<const0>\;
  dac_dat_o(4) <= \<const0>\;
  dac_dat_o(3) <= \<const0>\;
  dac_dat_o(2) <= \<const0>\;
  dac_dat_o(1) <= \<const0>\;
  dac_dat_o(0) <= \<const0>\;
  s_axis_tdata1(15) <= \<const0>\;
  s_axis_tdata1(14) <= \<const0>\;
  s_axis_tdata1(13) <= \<const0>\;
  s_axis_tdata1(12) <= \<const0>\;
  s_axis_tdata1(11) <= \<const0>\;
  s_axis_tdata1(10) <= \<const0>\;
  s_axis_tdata1(9) <= \<const0>\;
  s_axis_tdata1(8) <= \<const0>\;
  s_axis_tdata1(7) <= \<const0>\;
  s_axis_tdata1(6) <= \<const0>\;
  s_axis_tdata1(5) <= \<const0>\;
  s_axis_tdata1(4) <= \<const0>\;
  s_axis_tdata1(3) <= \<const0>\;
  s_axis_tdata1(2) <= \<const0>\;
  s_axis_tdata1(1) <= \<const0>\;
  s_axis_tdata1(0) <= \<const0>\;
  s_axis_tvalid1 <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    adc_clk_i : in STD_LOGIC;
    adc_dat_a : in STD_LOGIC_VECTOR ( 13 downto 0 );
    adc_dat_b : in STD_LOGIC_VECTOR ( 13 downto 0 );
    dac_dat_o : out STD_LOGIC_VECTOR ( 13 downto 0 );
    s_axis_clk : in STD_LOGIC;
    s_axis_resetn : in STD_LOGIC;
    s_axis_tready1 : in STD_LOGIC;
    s_axis_tvalid1 : out STD_LOGIC;
    s_axis_tdata1 : out STD_LOGIC_VECTOR ( 15 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "adc_pid_fifo_dac_adc_to_pid_dac_wrapper_0_1,adc_to_dac_wrapper,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "adc_to_dac_wrapper,Vivado 2016.4";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
begin
inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_adc_to_dac_wrapper
     port map (
      adc_clk_i => adc_clk_i,
      adc_dat_a(13 downto 0) => adc_dat_a(13 downto 0),
      adc_dat_b(13 downto 0) => adc_dat_b(13 downto 0),
      dac_dat_o(13 downto 0) => dac_dat_o(13 downto 0),
      s_axis_clk => s_axis_clk,
      s_axis_resetn => s_axis_resetn,
      s_axis_tdata1(15 downto 0) => s_axis_tdata1(15 downto 0),
      s_axis_tready1 => s_axis_tready1,
      s_axis_tvalid1 => s_axis_tvalid1
    );
end STRUCTURE;
